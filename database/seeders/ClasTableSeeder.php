<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Clas;

class ClasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
//        Clas::truncate();
        Clas::create([
            'school_id' => 2,
            'grade_id' => 1,
            'kumi_id' => 1,
        ]);
        Clas::create([
            'school_id' => 2,
            'grade_id' => 2,
            'kumi_id' => 1,
        ]);
        Clas::create([
            'school_id' => 2,
            'grade_id' => 3,
            'kumi_id' => 1,
        ]);
        Clas::create([
            'school_id' => 2,
            'grade_id' => 4,
            'kumi_id' => 1,
        ]);
        Clas::create([
            'school_id' => 2,
            'grade_id' => 5,
            'kumi_id' => 1,
        ]);
        Clas::create([
            'school_id' => 2,
            'grade_id' => 6,
            'kumi_id' => 1,
        ]);
        Clas::create([
            'school_id' => 1,
            'kumi_id' => 11,
        ]);
        Clas::create([
            'school_id' => 10,
            'kumi_id' => 13,
        ]);
    }
}
