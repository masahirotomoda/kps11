<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\IdNumber;

class IdNumberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 10101; $i < 100000; $i++){
            IdNumber::create([
                'school_int_id' => 1,
                'admin_teacher_id' => sprintf('t'.'%05d', $i),
                'user_id' => sprintf('%05d', $i),
            ]);
        }
    }
}
