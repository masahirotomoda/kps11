<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Word;

class WordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Word::create([
            'word_mana' => "サルも木から落ちる",
            'word_kana' => "さるもきからおちる",
        ]);
        Word::create([
            'word_mana' => "チャレンジ精神",
            'word_kana' => "ちゃれんじせいしん",
        ]);
        Word::create([
            'word_mana' => "ノンアルコールビール",
            'word_kana' => "のんあるこーるびーる",
        ]);
        Word::create([
            'word_mana' => "産業革命",
            'word_kana' => "さんぎょうかくめい",
        ]);
        Word::create([
            'word_mana' => "クラシック音楽",
            'word_kana' => "くらしっくおんがく",
        ]);
        Word::create([
            'word_mana' => "人生の分岐点",
            'word_kana' => "じんせいのぶんきてん",
        ]);
        Word::create([
            'word_mana' => "未知の生命体",
            'word_kana' => "みちのせいめいたい",
        ]);
        Word::create([
            'word_mana' => "ドレミファソラシド",
            'word_kana' => "どれみふぁそらしど",
        ]);
        Word::create([
            'word_mana' => "マヤ文明の遺跡",
            'word_kana' => "まやぶんめいのいせき",
        ]);
        Word::create([
            'word_mana' => "クリスマスパーティー",
            'word_kana' => "くりすますぱーてぃー",
        ]);
        Word::create([
            'word_mana' => "リンゴ",
            'word_kana' => "りんご",
            'display_order' => 10,
            'pronunciation_file_name' => '0002085ce642e0803c3.mp3'

        ]);
        Word::create([
            'word_mana' => "パイナップル",
            'word_kana' => "ぱいなっぷる",
            'display_order' => 20,
        ]);
        Word::create([
            'word_mana' => "abcde fghij klmno pqrst uvwxy zzzzz abcde fghij klmno pqrst uvwxy zzzzz abcde fghij klmno pqrst uvwxy zzzzz abcde fghij klmno pqrst uvwxy zzzzz",
            'display_order' => 10,
        ]);
    }
}
