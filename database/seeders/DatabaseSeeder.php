<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(SchoolTableSeeder::class);
        $this->call(KumiTableSeeder::class);
        $this->call(GradeTableSeeder::class);
        $this->call(ClasTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ConfigTableSeeder::class);
        $this->call(CourseTableSeeder::class);
        $this->call(CourseWordTableSeeder::class);
        $this->call(InformationTableSeeder::class);
        $this->call(ScoreHistorySeeder::class);
        $this->call(WordTableSeeder::class);
    }
}
