<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => '市川 葉子',
            'school_int_id' => 1,
            'school_id' => 'abc12345',
            'login_account' => 'a10101',
            'role' => 'システム管理者',
            'class_id' => 1,
            'password' => bcrypt('zaq12wsx'),
        ]);
        User::create([
            'name' => '友田 雅宏',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => 'a10101',
            'role' => '学校管理者',
            'class_id' => 1,
            'password' => bcrypt('zaq12wsx'),
        ]);
        User::create([
            'name' => '鈴木 一郎',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => 't10101',
            'role' => '先生',
            'class_id' => 1,
            'password' => bcrypt('zaq12wsx'),
        ]);
        User::create([
            'name' => 'Alistair Overeem',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => 't10102',
            'role' => '先生',
            'class_id' => 1,
            'password' => bcrypt('zaq12wsx'),
        ]);
        User::create([
            'name' => '森内 俊之',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => 't10103',
            'role' => '先生',
            'class_id' => 1,
            'password' => bcrypt('zaq12wsx'),
        ]);
        User::create([
            'name' => '相葉 雅紀',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => '10101',
            'attendance_no' => 1,
            'role' => '生徒',
            'class_id' => 1,
            'password' => '1111',
        ]);
        User::create([
            'name' => '大野 智',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => '10102',
            'attendance_no' => 2,
            'role' => '生徒',
            'class_id' => 1,
            'password' => '1111',
        ]);
        User::create([
            'name' => '二宮 和也',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => '10103',
            'attendance_no' => 3,
            'role' => '生徒',
            'class_id' => 1,
            'password' => '1111',
        ]);
        User::create([
            'name' => '城島 茂',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => '10104',
            'attendance_no' => 1,
            'role' => '生徒',
            'class_id' => 2,
            'password' => '1111',
        ]);
        User::create([
            'name' => '松岡 昌宏',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => '10105',
            'attendance_no' => 2,
            'role' => '生徒',
            'class_id' => 2,
            'password' => '1111',
        ]);
        User::create([
            'name' => '岡田 准一',
            'school_int_id' => 3,
            'school_id' => 'abc22345',
            'login_account' => '10106',
            'attendance_no' => 1,
            'role' => '生徒',
            'class_id' => 3,
            'password' => '1111',
        ]);
        User::create([
            'name' => '森田 剛',
            'school_int_id' => 2,
            'school_id' => 'abc22345',
            'login_account' => '10107',
            'attendance_no' => 2,
            'role' => '生徒',
            'class_id' => 3,
            'password' => '1111',
        ]);
        User::create([
            'name' => '岩田 剛典',
            'school_int_id' => 1,
            'school_id' => 'abc12345',
            'login_account' => '10101',
            'role' => '生徒',
            'class_id' => 7,
            'attendance_no' => 3,
            'password' => '1111',
        ]);
        User::create([
            'name' => 'NAOTO',
            'school_int_id' => 1,
            'school_id' => 'abc12345',
            'login_account' => '10102',
            'role' => '生徒',
            'class_id' => 8,
            'attendance_no' => 2,
            'password' => '1111',
        ]);
        User::create([
            'name' => '登坂 広臣',
            'school_int_id' => 1,
            'school_id' => 'abc12345',
            'login_account' => '10103',
            'role' => '生徒',
            'class_id' => 7,
            'attendance_no' => 4,
            'password' => '1111',
        ]);
        User::create([
            'name' => 'ELLY',
            'school_int_id' => 1,
            'school_id' => 'abc12345',
            'login_account' => '10104',
            'role' => '生徒',
            'class_id' => 7,
            'attendance_no' => 1,
            'password' => '1111',
        ]);
        User::create([
            'name' => '中居 正広',
            'school_int_id' => 3,
            'school_id' => 'abc32345',
            'login_account' => '11111',
            'role' => '生徒',
            'class_id' => 2,
            'attendance_no' => 1,
            'password' => '1111',
        ]);
    }
}
