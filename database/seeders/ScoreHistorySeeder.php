<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ScoreHistory;

class ScoreHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ScoreHistory::truncate();
        ScoreHistory::create([
            'user_id' => 8,
            'course_id' => 1,
            'course_name' => 'てすと',
            'point' => 99,
            'time' => 100,
            'typing_number' => 10,
            'misstyping_number' => 1,
        ]);
        ScoreHistory::create([
            'user_id' => 9,
            'course_id' => 1,
            'course_name' => 'てすと２',
            'point' => 120,
            'time' => 90,
            'typing_number' => 100,
            'misstyping_number' => 6,
        ]);
    }
}
