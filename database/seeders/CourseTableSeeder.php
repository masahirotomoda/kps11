<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Course;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Course::create([
            'course_level' => '★',
            'course_category' => 'ホームポジション',
            'course_name' => '上段',
            'course_type' => '組込',
            'display_order' => '10',
            'test_time' => '600',
        ]);
        Course::create([
            'course_level' => '★',
            'course_category' => 'ホームポジション',
            'course_name' => '下段',
            'course_type' => '組込',
            'display_order' => '20',
            'test_time' => '600',
        ]);
        Course::create([
            'course_level' => '★',
            'course_category' => 'ホームポジション',
            'course_name' => 'アルファベット',
            'course_type' => '組込',
            'display_order' => '30',
            'test_time' => '600',
        ]);
        Course::create([
            'course_level' => '★',
            'course_category' => 'ホームポジション',
            'course_name' => 'ローマ字あ行～か行',
            'course_type' => '組込',
            'display_order' => '40',
            'test_time' => '600',
        ]);
        Course::create([
            'course_level' => '★',
            'course_category' => 'ホームポジション',
            'course_name' => 'ローマ字さ行～た行',
            'course_type' => '組込',
            'display_order' => '50',
            'test_time' => '600',
        ]);
        Course::create([
            'course_level' => '★★',
            'course_category' => 'ホームポジション',
            'course_name' => 'ローマ字な行～は行',
            'course_type' => '組込',
            'display_order' => '60',
            'test_time' => '600',
        ]);
        Course::create([
            'course_level' => '★',
            'course_category' => '今月のタイピング',
            'course_name' => 'はるやすみ',
            'course_type' => '組込',
            'display_order' => '10',
            'test_time' => '600',
        ]);
        Course::create([
            'course_level' => '★',
            'course_category' => 'その他',
            'course_name' => '経堂小学校追加１',
            'course_type' => '追加',
            'display_order' => '10',
            'school_id' => '2',
        ]);
        Course::create([
            'course_level' => '★',
            'invalid' => true,
            'course_category' => 'その他',
            'course_name' => '経堂小学校追加２',
            'course_type' => '追加',
            'display_order' => '20',
            'school_id' => '2',
        ]);
        Course::create([
            'course_level' => '★',
            'course_category' => 'テスト',
            'course_name' => '標準テスト1',
            'course_type' => 'テスト',
            'display_order' => '10',
            'test_time' => '600',
        ]);

    }
}
