<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Grade;

class GradeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Grade::create([
            'name' => '1年',
            'school_id' => 2,
        ]);
        Grade::create([
            'name' => '2年',
            'school_id' => 2,
        ]);
        Grade::create([
            'name' => '3年',
            'school_id' => 2,
        ]);
        Grade::create([
            'name' => '4年',
            'school_id' => 2,
        ]);
        Grade::create([
            'name' => '5年',
            'school_id' => 2,
        ]);
        Grade::create([
            'name' => '6年',
            'school_id' => 2,
        ]);
        Grade::create([
            'name' => '1年',
            'school_id' => 3,
        ]);
        Grade::create([
            'name' => '2年',
            'school_id' => 3,
        ]);
        Grade::create([
            'name' => '3年',
            'school_id' => 3,
        ]);
        Grade::create([
            'name' => '4年',
            'school_id' => 3,
        ]);
        Grade::create([
            'name' => '5年',
            'school_id' => 3,
        ]);
        Grade::create([
            'name' => '6年',
            'school_id' => 3,
        ]);
    }
}
