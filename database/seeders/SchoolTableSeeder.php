<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\School;

class SchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        School::truncate();
        School::create([
            'name' => 'ナレッジ・プログラミングスクール',
            'school_id' => 'abc12345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => '経堂小学校',
            'school_id' => 'abc22345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => '〇１小学校',
            'school_id' => 'abc32345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'invalid' => true,
            'mall'  => 'test@test.com',
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => '〇２小学校',
            'school_id' => 'abc42345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => '〇３小学校',
            'school_id' => 'abc52345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => '〇４小学校',
            'school_id' => 'abc62345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => '〇１中学校',
            'school_id' => 'abc72345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => '〇２中学校',
            'school_id' => 'abc82345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => 'あああ学習塾',
            'school_id' => 'abc92345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'trial' => true,
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => 'いいい学習塾',
            'school_id' => 'abc02345',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'user_number'  => 600,
            'course_number'  => 10,
            'word_number'  => 1100,
        ]);
        School::create([
            'name' => 'トライアル学校1',
            'school_id' => 'tli00001',
            'address'  => '東京都世田谷区',
            'tel'  => '031234567',
            'mall'  => 'test@test.com',
            'trial'  => true,
            'user_number'  => 8000,
            'course_number'  =>  0,
            'word_number'  => 1100,
            'trial_school_num' => 1,
        ]);
    }
}
