<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CourseWord;

class CourseWordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 1,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 2,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 3,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 4,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 5,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 6,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 7,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 8,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 9,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 10,
        ]);
        CourseWord::create([
            'course_id' => 1,
            'word_id' => 10,
        ]);
        CourseWord::create([
            'course_id' => 7,
            'word_id' => 11,
        ]);
        CourseWord::create([
            'course_id' => 7,
            'word_id' => 12,
        ]);
        CourseWord::create([
            'course_id' => 10,
            'word_id' => 13,
        ]);
    }
}
