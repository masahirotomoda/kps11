<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Clas;
use App\Models\Config;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Config::truncate();
        Config::create([
            'ip_adress' => '183.77.254.227',
            'session_time' => 360,
            'logo' => 'aaa',
            'copyright' => '@know ledge programming school 2022',
            'course_level' => '★,★★,★★★',
            'course_category' => 'ローマ字基本,アルファベット,ことわざ,テスト,その他',
            'course_type' => '組み込み、テスト、追加',
            'pagenation' => 200,
            'important' =>'重要,一般' ,
            'school_category' => '小,中,高',
            'school_trial' => 12,
            'private_school_trial' => 6,
            'trial_school_num' => 1,
        ]);

    }
}
