<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BestScore;

class BestScoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        BestScore::truncate();
        BestScore::create([
            'course_id'=>1,
            'best_point'=>100,
            'user_id'=>8,
            'school_id'=>2,
            'best_point_created_at'=>'2022/1/1 0:0:0'
        ]);
    }
}
