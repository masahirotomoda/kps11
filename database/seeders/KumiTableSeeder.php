<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kumi;

class KumiTableSeeder extends Seeder
{

    public function run()
    {
        Kumi::create([
            'name' => '1組',
            'school_id' => 2,
        ]);
        Kumi::create([
            'name' => '1組',
            'school_id' => 3,
        ]);
        Kumi::create([
            'name' => '1組',
            'school_id' => 4,
        ]);
        Kumi::create([
            'name' => '1組',
            'school_id' => 5,
        ]);
        Kumi::create([
            'name' => '1組',
            'school_id' => 6,
        ]);
        Kumi::create([
            'name' => '1組',
            'school_id' => 7,
        ]);
        Kumi::create([
            'name' => '1組',
            'school_id' => 8,
        ]);
        Kumi::create([
            'name' => 'PC入門クラス',
            'school_id' => 9,
        ]);
        Kumi::create([
            'name' => 'PC入門クラス',
            'school_id' => 10,
        ]);
        Kumi::create([
            'name' => 'プレコース',
            'school_id' => 1,
        ]);
        Kumi::create([
            'name' => 'ベーシックコース',
            'school_id' => 1,
        ]);
        Kumi::create([
            'name' => 'ミドルコース',
            'school_id' => 1,
        ]);
        Kumi::create([
            'name' => 'アドバンスコース',
            'school_id' => 1,
        ]);

    }
}
