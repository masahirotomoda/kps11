<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Information;

class InformationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Information::truncate();
        Information::create([
            'title' => '記事テスト1',
            'article' => '記事テスト記事テスト記事テスト記事テスト記事テスト記事テスト記事テスト',
            'importance'  => '一般',
        ]);
        Information::create([
            'title' => '記事テスト2',
            'article' => '記事テスト記事テスト記事テスト記事テスト記事テスト記事テスト記事テスト',
            'importance'  => '重要',
        ]);
    }
}
