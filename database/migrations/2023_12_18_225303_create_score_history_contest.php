<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoreHistoryContest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('score_history_contest', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('conuser_id');
            $table->unsignedBigInteger('course_id');
            $table->text('course_name',50);
            $table->unsignedInteger('point');
            $table->unsignedInteger('time')->nullable();
            $table->unsignedInteger('typing_number');
            $table->unsignedInteger('misstyping_number');
            $table->string('rank',10)->nullable();
            $table->timestamps();

            $table->foreign('conuser_id')
                ->references('id')
                ->on('conusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('score_history_contest');
    }
}
