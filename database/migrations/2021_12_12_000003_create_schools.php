<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->id();
            $table->string('school_id',50);
            $table->string('name',50);
            $table->string('address',50);
            $table->string('tel',50)->nullable();
            $table->string('mall',50)->nullable();
            $table->integer('user_number');
            $table->integer('course_number');
            $table->integer('word_number');
            $table->boolean('invalid')->nullable()->default(false);
            $table->boolean('trial')->nullable()->default(false);
            $table->integer('trial_school_num')->nullable();
            $table->string('password_school_init',50)->default('Abcdef891');
            $table->string('password_teacher_init',50)->default('abc123');
            $table->string('password_student_init',50)->default('1111');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
