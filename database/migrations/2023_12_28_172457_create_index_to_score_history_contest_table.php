<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndexToScoreHistoryContestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('score_history_contest', function (Blueprint $table) {
            $table->index('school_int_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('score_history_contest', function (Blueprint $table) {
            $table->dropIndex('score_history_contest_school_int_id_index');
        });
    }
}
