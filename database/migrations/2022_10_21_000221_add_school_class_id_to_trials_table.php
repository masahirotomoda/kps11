<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSchoolClassIdToTrialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trials', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('school_id')->notnull();  //カラム追加
            $table->unsignedBigInteger('class_id')->notnull();  //カラム追加

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trials', function (Blueprint $table) {
            //
            $table->dropColumn('school_id');  //カラム削除
            $table->dropColumn('class_id');  //カラム削除
        });
    }
}
