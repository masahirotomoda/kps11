<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourses extends Migration
{
    /**
     * @version 2021.12.13.01
     * @author Masahiro.Tomoda
     * @return void
     * @desc ●組込み（既定のコース一覧にあるコースで削除不可）、オリジナル（学校で追加で作ったコースで追加、編集、削除できる）
     *        ●コースは学校に属する（クラスでない）→オリジナルのコースは、その学校の全学年が使える。
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('course_level',50);
            $table->string('course_category',50);
            $table->string('course_name',50);
            $table->string('course_type',10);
            $table->boolean('is_free_course')->default(true);
            $table->boolean('random')->default(true);
            $table->integer('display_order');
            $table->integer('test_time')->nullable();
            $table->bigInteger('school_id')->nullable();
            $table->boolean('invalid')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
