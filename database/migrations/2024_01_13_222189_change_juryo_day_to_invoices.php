<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeJuryoDayToInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->date('juryo_day')->nullable(true)->change();
            $table->date('nyukin_day')->nullable(true)->change();
            $table->boolean('show')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->date('juryo_day')->nullable(false)->change();
            $table->date('nyukin_day')->nullable(false)->change();
            $table->dropColumn('show');
        });
    }
}
