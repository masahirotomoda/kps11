<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyBattlescoreHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('battlescore_history', function (Blueprint $table) {
            $table->dropColumn('period'); // 「period」カラムを削除
            $table->Integer('user_id')->before('nickname'); // 「nickname」カラムの前に「user_id」カラムを追加
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('battlescore_history', function (Blueprint $table) {
            $table->string('period'); // 「period」カラムを再追加
            $table->dropColumn('user_id'); // 「user_id」カラムを削除
        });
    }
}
