<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->string('role',50)->nullable;
            $table->unsignedBigInteger('class_id')->nullable();
            $table->unsignedInteger('attendance_no')->nullable();
            $table->string('login_account',50);
            $table->string('password',256);
            $table->string('remember_token',100)->nullable();
            $table->unsignedBigInteger('school_int_id');
            $table->string('school_id',50);
            $table->timestamps();
            $table->unique(['login_account', 'school_id'], 'users_login_account_school_id_unique');

            $table->foreign('class_id')
                ->references('id')
                ->on('classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
