<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWords extends Migration
{
    /**
     * @version 2022.02.20.01
     * @author Hikaru.Asai
     * @return void
     * @desc ●1つのコースに単語が１０個設定できる。
     *       ●1つの単語に対して正解が２，３ある場合がある→ジャムの答えはjamu  と　zyamu　これはプログラム側で実装する。
     *
     */
    public function up()
    {
        Schema::create('words', function (Blueprint $table) {
            $table->id();
            $table->text('word_mana');
            $table->text('word_kana')->nullable();
            $table->string('pronunciation_file_name', 50)->nullable();
            $table->integer('display_order')->nullable();
            $table->string('remark', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('words');
    }
}
