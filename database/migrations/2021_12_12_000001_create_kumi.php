<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKumi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kumi', function (Blueprint $table) {
            $table->id();
            $table->string('name',10);
            $table->unsignedBigInteger('school_id');
            $table->timestamps();
            $table->unique(['name', 'school_id'], 'kumi_name_school_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kumi');
    }
}
