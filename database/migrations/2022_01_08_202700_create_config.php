<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config', function (Blueprint $table) {
            $table->string('ip_adress',50);
            $table->string('session_time',50);
            $table->string('logo',50);
            $table->string('copyright',50);
            $table->string('course_level',1024);
            $table->string('course_category',1024);
            $table->string('course_type',1024);
            $table->integer('pagenation');
            $table->string('important',1024);
            $table->string('school_category',1024);
            $table->integer('school_trial');
            $table->integer('private_school_trial');
            $table->integer('trial_school_num');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config');
    }
}
