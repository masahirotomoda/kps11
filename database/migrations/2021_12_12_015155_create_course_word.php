<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseWord extends Migration
{
    /**
     * @version 2021.12.09.01
     * @author Masahiro.Tomoda
     * @return void
     * @desc ●コースには、10個（数値は決まってない）の単語が登録されている。
     * @return void
     */
    public function up()
    {
        Schema::create('course_word', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('course_id');
            $table->unsignedBigInteger('word_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_word');
    }
}
