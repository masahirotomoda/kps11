<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trials', function (Blueprint $table) {
            $table->id();
            $table->string('category',50)->nullable();
            $table->string('industry',50)->nullable();
            $table->string('name',256)->notnull();
            $table->string('grade',256)->nullable();
            $table->string('kumi',256)->nullable();
            $table->string('teachers_name',256)->notnull();
            $table->string('mail',256)->notnull();
            $table->string('address',256)->notnull();
            $table->string('tel',256)->notnull();
            $table->date('end_of_trial');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trials');
    }
}
