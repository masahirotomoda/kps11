<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoginUserIdLoginSchoolIntIdOnLoginLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('login_logs', function (Blueprint $table) {
            $table->boolean('login_user_id')->nullable();  //カラム追加
            $table->boolean('login_school_int_id')->nullable();  //カラム追加
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('login_logs', function (Blueprint $table) {
            $table->dropColumn('login_user_id');  //カラム削除
            $table->dropColumn('login_school_int_id');  //カラム削除
        });
    }
}
