<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSuccessNumToCourseWithBestpointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_with_bestpoint', function (Blueprint $table) {
            $table->unsignedInteger('success_num')->nullable()->after('best_point');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_with_bestpoint', function (Blueprint $table) {
            $table->dropColumn('success_num');
        });
    }
}
