<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('grade_id')->nullable();
            $table->unsignedBigInteger('kumi_id')->nullable();
            $table->timestamps();
            $table->unique(['school_id', 'grade_id','kumi_id'], 'users_school_grade_kumi_unique');

            $table->foreign('id')
            ->references('id')
            ->on('schools');

            $table->foreign('grade_id')
            ->references('id')
            ->onDelete('cascade')
            ->on('grade');

            $table->foreign('kumi_id')
            ->references('id')
            ->onDelete('cascade')
            ->on('kumi');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
