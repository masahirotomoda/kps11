<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBattlescoreHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battlescore_history', function (Blueprint $table) {

            $table->id();
            $table->string('nickname',50); 
            $table->string('period',50)->nullable();
            $table->integer('course_id');
            $table->string('course_name',100);
            $table->integer('battle_score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battlescore_history');
    }
}
