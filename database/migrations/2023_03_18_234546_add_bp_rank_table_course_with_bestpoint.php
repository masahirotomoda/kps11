<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBpRankTableCourseWithBestpoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_with_bestpoint', function (Blueprint $table) {
            $table->unsignedSmallInteger('bp_rank')->default(0)->notnull();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_with_bestpoint', function (Blueprint $table) {
            $table->dropColumn('bp_rank');
        });
    }
}
