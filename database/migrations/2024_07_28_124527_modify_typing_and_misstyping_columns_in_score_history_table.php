<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyTypingAndMisstypingColumnsInScoreHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('score_history', function (Blueprint $table) {
            $table->unsignedInteger('typing_number')->nullable()->change();
            $table->unsignedInteger('misstyping_number')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('score_history', function (Blueprint $table) {
            $table->integer('typing_number', 10)->unsigned()->nullable(false)->change();
            $table->integer('misstyping_number', 10)->unsigned()->nullable(false)->change();
        });
    }
}
