<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoInfoHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_info_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('information_id');
            $table->unsignedBigInteger('information_history_id');
            $table->timestamps();

            $table->foreign('information_id')
            ->references('id')
            ->on('information')
            ->onDelete('cascade');
            $table->foreign('information_history_id')
            ->references('id')
            ->on('information_history')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_info_history');
    }
}
