<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conusers', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->string('role',50);
            $table->string('grade')->nullable();
            $table->string('class')->nullable();
            $table->string('attendance_no')->nullable();
            $table->string('account',50)->unique()->nullable();
            $table->string('pass',50)->nullable();
            $table->Integer('school_int_id')->unsigned();
            $table->string('school_id',50);
            $table->timestamps();
            $table->foreign('school_int_id')
                ->references('id')
                ->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conusers');
    }
}
