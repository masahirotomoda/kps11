<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseWithBestpoint extends Migration
{
    /**
     * @version 2021.12.13.01
     * @author Masahiro.Tomoda
     * @return void
     * @desc ●表のタイピングメニューにつかう元テーブル。本来は、スコア履歴から、最高点を算出すればよいが、年度更新でタイピング履歴を削除するため。ここでベストスコア10を記録し、その中の1位を表示。
     *        ●タイピング画面でコースのスコアTOP１０がでる。TOP１０もこのテーブルからデータを取得する。→コースごとに上位スコア１０レコードを保存する
     *        ●タイピングをして、スコアがでたとき、もし、このテーブルにそのユーザーのこのコースのレコードがなければ（初めてこのコースを使う）新規作成してスコアを記録する。
     *        ●タイピングのスコアがでた時に、ベスト10より下のスコアなら何もしない、ベスト10以内なら、そのスコアを新規追加して、そのかわりに11位のレコードを削除する。
     * @return void
     */
    public function up()
    {
        Schema::create('course_with_bestpoint', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('course_id');
            $table->unsignedBigInteger('user_id');
            $table->integer('best_point');
            $table->timestamp('best_point_created_at');
            $table->unsignedBigInteger('school_id');
            $table->string('rank',10)->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('school_id')
                ->references('id')
                ->on('schools')
                ->onDelete('cascade');

            $table->foreign('course_id')
                ->references('id')
                ->on('courses')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_with_bestpoint');
    }
}
