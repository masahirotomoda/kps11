<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBestPointCreatedAtTableCourseWithBestpoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_with_bestpoint', function (Blueprint $table) {
            $table->timestamp('best_point_created_at')->useCurrent()->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_with_bestpoint', function (Blueprint $table) {
            $table->dropColumn('best_point_created_at');
        });
    }
}
