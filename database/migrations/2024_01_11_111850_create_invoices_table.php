<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();    
            $table->date('seikyu_day');
            $table->string('detaill',300)->nullable();
            $table->string('detaill_term',300)->nullable();
            $table->integer('tanka');
            $table->integer('count');
            $table->integer('price');
            $table->integer('total_price');
            $table->integer('tax');
            $table->date('kigen_day');
            $table->date('juryo_day');
            $table->string('howpay',20)->nullable();
            $table->string('other',300)->nullable();
            $table->string('memo',500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
