<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[App\Http\Controllers\Free\MenuController::class, 'index'])->name('freeIndex');
//◎トライアル　＞　申し込みフォーム
Route::get('/trial',
    [App\Http\Controllers\trial\TrialController::class, 'index'])->name('trialindex');
//◎トライアル　＞　申し込みフォーム送信
Route::post('/trial-submit',[App\Http\Controllers\trial\TrialController::class, 'submit'])->name('trialApplication');

//Freeトップ
Route::match(['get', 'post'],'/free-top',[App\Http\Controllers\Free\MenuController::class, 'index'])->name('freeIndex');
Route::get('/free-allinfo',[App\Http\Controllers\Free\MenuController::class, 'allInfo'])->name('freeAllInfo');

//Freeタイピング画面 URLを変えるのはSEOのため
Route::match(['get', 'post'],'free/course', [App\Http\Controllers\Free\TypingController::class, 'index'])->name('freetyping');
Route::match(['get', 'post'],'free/course-keytouch', [App\Http\Controllers\Free\TypingController::class, 'index'])->name('freeKEYTOUCH');
Route::match(['get', 'post'],'free/course-test', [App\Http\Controllers\Free\TypingController::class, 'index'])->name('freeTEST');
Route::match(['get', 'post'],'free/course-map', [App\Http\Controllers\Free\TypingController::class, 'index'])->name('freeMAP');
Route::match(['get', 'post'],'free/course-isan', [App\Http\Controllers\Free\TypingController::class, 'index'])->name('freeISAN');
Route::match(['get', 'post'],'free/course-english', [App\Http\Controllers\Free\TypingController::class, 'index'])->name('freeEnglish');

//以前　n-typing.com/free/typing-map/422　など、コースIDで案内していたのでしばらく404でないようにリダイレクト※いずれは削除すること
Route::get('/free/typing-map/422', function () {
    return redirect()->route('freeMAP');
});
Route::get('/free/typing/1', function () {
    return redirect()->route('freetyping');
});
Route::get('/free/typing-test/93', function () {
    return redirect()->route('freeTEST');
});
Route::get('/free/typing-test/121', function () {
    return redirect()->route('freeKEYTOUCH');
});
//2024/1/27　リリース前の検索で古いURLがでるのでリダイレクト対策　※後で削除
Route::get('/free/typing-map/{CourseID}', function ($CourseID) {
    return redirect()->route('freeIndex');
})->whereNumber('CourseID');
Route::get('/free/typing/{CourseID}', function ($CourseID) {
    return redirect()->route('freeIndex');
})->whereNumber('CourseID');
Route::get('/free/typing-test/{CourseID}', function ($CourseID) {
    return redirect()->route('freeIndex');
})->whereNumber('CourseID');

//トライアル
Route::get('/trial/terms', function () {
    return view('trial.terms');
})->name('trialTerms');
Route::get('/terms', function () {
    return view('terms');
})->name('terms');
Route::get('/terms-free', function () {
    return view('termsfree');
})->name('freeTerms');

Auth::routes();
Route::get('logout', function ()
{
    Auth::logout();
    return Redirect::to('/login');
})->name('logout');

Route::get('school/logout', function ()
{
    Auth::logout();
    Session()->flush();
    return Redirect::to('/login');
})->name('logout');

Route::get('logout-school', function ()
{
    Auth::logout();
    Session()->flush();
    return Redirect::to('/login');
})->name('logout-school');

//コンテスト
Route::get('{schoolId}/contest', [App\Http\Controllers\Contest\MenuController::class, 'index'])->where('schoolId', '[a-zA-Z0-9]{16,16}')->name('contestMenu');
Route::post('{schoolId}/contest-course', [App\Http\Controllers\Contest\TypingController::class, 'index'])->where('schoolId', '[a-zA-Z0-9]{16,16}')->name('contestTyping');
Route::post('{schoolId}/contest/typing', [App\Http\Controllers\Contest\TypingController::class, 'practiceTyping'])->where('schoolId', '[a-zA-Z0-9]{16,16}')->where('course', '[0-9]+');
Route::post('{schoolId}/contest/exam-start', [App\Http\Controllers\Contest\MenuController::class, 'newStudent'])->where('schoolId', '[a-zA-Z0-9]{16,16}');
Route::post('{schoolId}/contest/store', [App\Http\Controllers\Contest\TypingController::class, 'store'])->where('schoolId', '[a-zA-Z0-9]{16,16}');
//デモ用スコア一覧
Route::get('{schoolId}/scorelist', [App\Http\Controllers\Contest\MenuController::class, 'demoscorelist'])->where('schoolId', '[a-zA-Z0-9]{16,16}');
//コンテストデモページのリダイレクト　n-typing.com/contet ⇒n-typing.com/contest2pramode2/contest
Route::get('/contest1', function () {
    return redirect()->route('contestMenu', ['schoolId' => 'contest2pramode2']);
});
Route::get('/contest2', function () {
    return redirect()->route('contestMenu', ['schoolId' => 'contest1tesmode1']);
});
//コンテストデモページ　リダイレクト ⇒各練習コースはpostパラメータなので、数時間置くとエラーとなる。その際、コースメニューに戻らせる
Route::get('/contest2pramode2/contest-course', function () {
    return redirect()->route('contestMenu', ['schoolId' => 'contest2pramode2']);
});
Route::get('/contest1tesmode1/contest-course', function () {
    return redirect()->route('contestMenu', ['schoolId' => 'contest1tesmode1']);
});

//ログインフォーム
Route::get('/system-login',[LoginController::class, 'showLoginForm'])->name('systemLoginForm');
Route::get('/school-login',[LoginController::class, 'showSchoolLoginForm'])->name('schoolLoginForm');
Route::get('/login',[LoginController::class, 'showUserLoginForm'])->name('userLoginForm');

//ログイン
Route::post('system-login',[LoginController::class, 'systemLogin'])->name('systemLogin');
Route::post('school-login',[LoginController::class, 'schoolLogin'])->name('schoolLogin');
Route::post('user-login',[LoginController::class, 'userLogin'])->name('userLogin');

//権限ログイン不可
Route::get('/not-permit', function () {
    abort(401);
});

//Error
//Route::get('/errors/403', function () {
//    return view('errors.403');
//})->name('403');

//Route::get('/errors/404', function () {
//    return view('errors.404');
//})->name('404');

Route::group(['middleware' => ['auth']], function () {
// この中はログインされている場合のみルーティングされる

//ユーザー（生徒・先生系）
//★TOPページ　タイピングコース一覧
Route::match(['get', 'post'],'user-menu', [App\Http\Controllers\User\MenuController::class, 'index'])->name('userMenu');
//★全コースタイピング画面
Route::post('school-typingcourse', [App\Http\Controllers\User\TypingController::class, 'index']);
//★全コーススコア記録スコア記録　 Ajax通信
//※【重要】tokenがうまく引き継げないため、tokunを例外にしている⇒ここで設定　app\Http\Middleware\VerifyCsrfToken.php
Route::post('typing/typing-store', [App\Http\Controllers\User\TypingController::class, 'ajax'])->name('typing.ajax');
Route::post('typing/typing-store-battle', [App\Http\Controllers\User\TypingController::class, 'ajax_battle'])->name('typing.ajax_battle');


//【コンテスト】学校管理　＞　スコア履歴
    Route::get('school/contest-scorelist', [App\Http\Controllers\Contest\ScorelistSchoolController::class, 'index']);
    Route::match(['get', 'post'],'school/contest-scorelist-search', [App\Http\Controllers\Contest\ScorelistSchoolController::class, 'searchAndExcel']);
    Route::post('/contest/transcript-print-pdf', [App\Http\Controllers\Contest\ScorelistSchoolController::class, 'pdf']);
//【コンテスト】学校管理　＞　生徒
    Route::get('school/contest-studentlist', [App\Http\Controllers\Contest\StudentlistSchoolController::class, 'index']);
    Route::match(['get', 'post'],'school/contest-studentlist-search', [App\Http\Controllers\Contest\StudentlistSchoolController::class, 'search']);
//【コンテスト】システム　＞　生徒
    Route::get('contest-studentlist', [App\Http\Controllers\Contest\StudentlistSystemController::class, 'index']);
    Route::match(['get', 'post'],'contest-studentlist-search', [App\Http\Controllers\Contest\StudentlistSystemController::class, 'search']);
    Route::post('contest-studentlist-delete', [App\Http\Controllers\Contest\StudentlistSystemController::class, 'delete'])->name('contestStudentDel');
//【コンテスト】システム　＞　スコア
    Route::get('contest-scorelist', [App\Http\Controllers\Contest\ScorelistSystemController::class, 'index']);
    Route::match(['get', 'post'],'contest-scorelist-search', [App\Http\Controllers\Contest\ScorelistSystemController::class, 'search']);
    Route::post('contest-scorelist-delete', [App\Http\Controllers\Contest\ScorelistSystemController::class, 'delete'])->name('contestScoreDel');

  

//【システム管理メニュー】メインメニュー
    //メインメニュー
    Route::get('system-menu', [App\Http\Controllers\System\MenuController::class, 'index'])->name('systemMenu');
    //すべてのおしらせ
    Route::get('menu-allinfo', [App\Http\Controllers\System\MenuController::class, 'allinfo'])->name('systemAllInfo');
    //すべてのトライアル
    Route::get('menu-alltrial', [App\Http\Controllers\System\MenuController::class, 'alltrial'])->name('systemAllTrial');

//【システム管理メニュー】トライアル管理
    //トライアル管理　※すべてのトライアルと同じメソッド
    Route::get('trialinfo', [App\Http\Controllers\System\TrialinfoController::class, 'index'])->name('systemTrialInfo');
    //トライアル管理　＞　検索
    Route::match(['get', 'post'],'trialinfo-search', [App\Http\Controllers\System\TrialinfoController::class, 'search'])->name('systemTrialInfoSearch');
    //トライアル管理　＞　編集
    Route::post('trialinfo-edit', [App\Http\Controllers\System\TrialinfoController::class, 'edit'])->name('systemTrialInfoEdit');
    //トライアル管理　＞　更新
    Route::post('trialinfo-update', [App\Http\Controllers\System\TrialinfoController::class, 'update'])->name('systemTrialInfoUpdate');
    //トライアル管理　＞　削除
    Route::post('trialinfo-delete', [App\Http\Controllers\System\TrialinfoController::class, 'delete'])->name('systemTrialInfoDelete');
    //トライアル管理　＞　ユーザー管理※コントローラーが異なる！
    Route::match(['get', 'post'],'trialinfo-edituser', [App\Http\Controllers\System\TrialinfouserController::class, 'index'])->name('systemTrialInfoUser');
    //トライアル管理　＞　ユーザー管理　＞　一括更新　※コントローラーが異なる！
    Route::match(['get', 'post'],'trialinfo-allupdate', [App\Http\Controllers\System\TrialinfouserController::class, 'update'])->name('systemTrialInfoUserUpdate');
    //トライアル管理　＞　ユーザー管理　＞　一括削除　※コントローラーが異なる！
    Route::match(['get', 'post'],'trialinfo-userdelete', [App\Http\Controllers\System\TrialinfouserController::class, 'delete'])->name('systemTrialInfoUserDelete');
//【システム管理メニュー】システム管理者情報
    //システム管理者情報
    Route::get('profile', [App\Http\Controllers\System\ProfileController::class, 'index'])->name('systemProfile');
    //システム管理者情報　＞　名前変更
    Route::post('profile-update', [App\Http\Controllers\System\ProfileController::class, 'update'])->name('systemProfileUpdate');
    //システム管理者情報　＞　パスワードリセット
    Route::post('password-change', [App\Http\Controllers\System\ProfileController::class, 'changePassword'])->name('systemPasswordChange');
//【システム管理メニュー】学校情報
    //学校情報
    Route::get('school-info', [App\Http\Controllers\System\SchoolInfoController::class, 'index'])->name('schoolInfo');
    //学校情報　＞　検索
    Route::match(['get', 'post'],'school-info-search', [App\Http\Controllers\System\SchoolInfoController::class, 'search'])->name('schoolInfoSearch');
    //学校情報　＞　新規
    Route::post('school-add', [App\Http\Controllers\System\SchoolInfoController::class, 'add'])->name('schoolCreate');
    //学校情報　＞　詳細・編集
    Route::post('school-info-edit', [App\Http\Controllers\System\SchoolInfoController::class, 'edit'])->name('schoolInfoEdit');
    //学校情報　＞　削除
    Route::post('school-delete', [App\Http\Controllers\System\SchoolInfoController::class, 'delete'])->name('schoolDelete');
    //学校情報　＞　更新
    Route::post('school-info-update', [App\Http\Controllers\System\SchoolInfoController::class, 'update'])->name('schoolInfoUpdate');

//【システム管理メニュー】クラス管理(1)
    //クラス一覧
    Route::get('class', [App\Http\Controllers\System\ClassInfoController::class, 'index'])->name('classInfo');
    //検索
    Route::match(['get', 'post'],'class-search', [App\Http\Controllers\System\ClassInfoController::class, 'search'])->name('classInfoSearch');
    //新規＆更新 return view paginationあり　★getあり　要検討
    Route::match(['get', 'post'],'class-add', [App\Http\Controllers\System\ClassInfoController::class, 'save'])->name('classInfoAdd');
    //削除　paginationなし
    Route::post('class-delete', [App\Http\Controllers\System\ClassInfoController::class, 'delete'])->name('classInfoDelete');
    //各クラスの所属生徒（別ページ）paginationなし
    Route::post('class-student', [App\Http\Controllers\System\ClassInfoController::class, 'studentinfo'])->name('classInfoUpdate');
    //ドロップダウンで学校を選択　paginationあり
    Route::match(['get', 'post'],'class-select', [App\Http\Controllers\System\ClassInfoController::class, 'schoolSelect'])->name('classInfoUpdate');

//【システム管理メニュー】クラス管理(2)　※学校別　ページネーションなし
    //◆学校別/ドロップダウンで学校を選択 (メインメニューから遷移）
    Route::get('class2-school-select', [App\Http\Controllers\System\ClassInfo2Controller::class, 'schoolSelect'])->name('classInfo2Update');
    //◆学校別/クラス一覧
    Route::post('class2', [App\Http\Controllers\System\ClassInfo2Controller::class, 'index'])->name('classInfo2');
    //◆学校別/クラス一覧　＞　検索
    Route::post('class2-search', [App\Http\Controllers\System\ClassInfo2Controller::class, 'search'])->name('classInfo2Search');
    //◆学校別/クラス一覧　＞　新規＆更新
    Route::post('class2-add', [App\Http\Controllers\System\ClassInfo2Controller::class, 'save'])->name('classInfo2Add');
    //◆学校別/クラス一覧　＞　削除　paginationなし
    Route::post('class2-delete', [App\Http\Controllers\System\ClassInfo2Controller::class, 'delete'])->name('classInfo2Delete');
    //◆学校別/クラス一覧　＞　各クラスの所属生徒（別ページ）
    Route::post('class2-student', [App\Http\Controllers\System\ClassInfo2Controller::class, 'studentinfo'])->name('classInfo2Update');

//【システム管理メニュー】学年管理(1)
    //◆学年一覧
    Route::get('grade', [App\Http\Controllers\System\GradeController::class, 'index'])->name('systemGrade');
    //◆学年一覧　＞　検索
    Route::match(['get', 'post'],'grade-search', [App\Http\Controllers\System\GradeController::class, 'search'])->name('systemGradeSearch');
    //◆学年一覧　＞　ドロップダウンで学校を選択
    Route::match(['get', 'post'],'grade-select', [App\Http\Controllers\System\GradeController::class, 'schoolSelect'])->name('systemGradeSelect');
    //◆学年一覧　＞　新規　　redirect　postでOK
    Route::match(['get', 'post'],'grade-add', [App\Http\Controllers\System\GradeController::class, 'save'])->name('systemGradeAdd');
    //◆学年一覧　＞　更新　　★getあり　要検討
    Route::match(['get', 'post'],'grade-save', [App\Http\Controllers\System\GradeController::class, 'save'])->name('systemGradeupdate');
    //◆学年一覧　＞　削除　reidirect　postでOK
    Route::match(['post'],'grade-delete', [App\Http\Controllers\System\GradeController::class, 'delete'])->name('systemGradeDelete');

//【システム管理メニュー】学年管理(2) 学校別
    //◆システム管理メニュー　＞　学校別/学校を選択
    Route::get('grade2-school-select', [App\Http\Controllers\System\Grade2Controller::class, 'schoolSelect'])

        ->name('systemGrade2Select');
    //◆システム管理メニュー　＞　学校別/学年一覧
    Route::post('grade2', [App\Http\Controllers\System\Grade2Controller::class, 'index'])->name('systemGrade2');
    //◆システム管理メニュー　＞　学校別/学年一覧　＞　新規＆更新
    Route::post('grade2-save', [App\Http\Controllers\System\Grade2Controller::class, 'save'])->name('systemGrade2update');
    //◆システム管理メニュー　＞　学校別/学年一覧　＞　削除
    Route::post('grade2-delete', [App\Http\Controllers\System\Grade2Controller::class, 'delete'])->name('systemGrade2Delete');

//【システム管理メニュー】組管理(1)
    //◆システム管理メニュー　＞　組一覧
    Route::get('kumi', [App\Http\Controllers\System\KumiController::class, 'index'])->name('systemKumi');
    //◆システム管理メニュー　＞　組一覧　＞　検索
    Route::match(['get', 'post'],'kumi-search', [App\Http\Controllers\System\KumiController::class, 'search'])->name('systemKumiSearch');
    //◆システム管理メニュー　＞　組一覧　＞　検索ドロップダウンで学校選択　paginationあり
    Route::match(['get', 'post'],'kumi-select', [App\Http\Controllers\System\KumiController::class, 'schoolSelect'])->name('systemKumiSelect');
    //◆システム管理メニュー　＞　組一覧　＞　新規＆更新　paginationあり　★getあり　要検討
    Route::match(['get', 'post'],'kumi-save', [App\Http\Controllers\System\KumiController::class, 'save'])
        ->middleware('web')
        ->name('systemKumiSave');
    //◆システム管理メニュー　＞　組一覧　＞　削除　paginationなし　redirect　postのみ
    Route::post('kumi-delete', [App\Http\Controllers\System\KumiController::class, 'delete'])->name('systemKumiDelete');
//【システム管理メニュー】組管理(2) 学校別
    //◆学校別/組一覧　＞　で学校選択
    Route::get('kumi2-school-select', [App\Http\Controllers\System\Kumi2Controller::class, 'schoolSelect'])->name('systemKumi2Select');
    //◆組一覧
    Route::post('kumi2', [App\Http\Controllers\System\Kumi2Controller::class, 'index'])->name('systemKumi2');
    //◆学校別/組一覧　＞　新規＆更新
    Route::post('kumi2-save', [App\Http\Controllers\System\Kumi2Controller::class, 'save'])
        ->middleware('web')
        ->name('systemKumi2Save');
    //◆学校別/組一覧　＞　削除
    Route::post('kumi2-delete', [App\Http\Controllers\System\Kumi2Controller::class, 'delete'])->name('systemKumi2Delete');

//【システム管理メニュー】ユーザ管理(1)
    //◆ユーザ一覧
    Route::get('user', [App\Http\Controllers\System\UserInfoController::class, 'index'])->name('systemUserInfo');
    //◆ドロップダウンで学校選択
    Route::match(['get', 'post'],'user-select', [App\Http\Controllers\System\UserInfoController::class, 'schoolSelect'])->name('systemUserSelect');
    //◆検索
    Route::match(['get', 'post'],'user-search', [App\Http\Controllers\System\UserInfoController::class, 'search'])->name('systemUserInfoSearch');
    //◆新規＆更新
    Route::match(['get', 'post'],'user-save', [App\Http\Controllers\System\UserInfoController::class, 'save'])->name('systemUserInfoSave');
    //◆エクセル一括登録
    Route::match(['get', 'post'],'user-import', [App\Http\Controllers\System\UserInfoController::class, 'importCsv'])->name('systemUserImportCsv');
    //◆削除
    Route::post('user-delete', [App\Http\Controllers\System\UserInfoController::class, 'delete'])->name('systemUserDelete');
    //◆エクセルテンプレートダウンロード
    Route::get('system_user_exceltemplate.xlsx', [App\Http\Controllers\System\UserInfoController::class, 'download'])->name('systemUserDownload');

//【システム管理メニュー】ユーザ管理(2) ※学校別
    //◆学校別/ユーザ一覧
    Route::match(['get', 'post'],'user2', [App\Http\Controllers\System\UserInfo2Controller::class, 'index'])->name('systemUserInfo2');
    //◆学校別/ユーザ一覧　＞　ドロップダウンで学校選択
    Route::match(['get', 'post'],'user2-school-select', [App\Http\Controllers\System\UserInfo2Controller::class, 'schoolSelect'])->name('systemUser2Select');
    //◆学校別/ユーザ一覧　＞　検索＆エクセル出力＆すべて表示
    Route::match(['get', 'post'],'user2-search', [App\Http\Controllers\System\UserInfo2Controller::class, 'search'])->name('systemUserInfo2Search');
    //◆学校別/ユーザ一覧　＞　新規＆更新＆パスワードリセット
    Route::match(['get', 'post'],'user2-save', [App\Http\Controllers\System\UserInfo2Controller::class, 'save'])->name('systemUserInfo2Save');
    //◆学校別/ユーザ一覧　＞　削除
    Route::post('user2-delete', [App\Http\Controllers\System\UserInfo2Controller::class, 'save'])->name('systemUser2Delete');
    //◆学校別/ユーザ一覧　＞　特別学校アカウント登録（ナレッジ用）へ遷移
    Route::post('user2-addschoolaccount', [App\Http\Controllers\System\UserInfo2Controller::class, 'addschoolaccount'])
        ->where('schoolId', '[a-zA-Z0-9]{8,8}')->name('systemUser2AddSchoolAccount');
    //◆学校別/ユーザ一覧　＞　特別学校アカウントを新規登録する
    Route::post('user2-saveschoolaccount', [App\Http\Controllers\System\UserInfo2Controller::class, 'saveschoolaccount'])
        ->where('schoolId', '[a-zA-Z0-9]{8,8}')->name('systemUser2SaveSchoolAccount');
    //◆学校別/ユーザ一覧　＞　ユーザー編集画面へ遷移
    Route::post('user2-edituser', [App\Http\Controllers\System\UserInfo2Controller::class, 'useredit'])
        ->where('schoolId', '[a-zA-Z0-9]{8,8}')->name('systemUser2UserEdit');
    //◆学校別/ユーザ一覧　＞　ユーザー編集、保存
    Route::post('user2-saveuser', [App\Http\Controllers\System\UserInfo2Controller::class, 'usersave'])->where('schoolId', '[a-zA-Z0-9]{8,8}')->name('systemUser2UserSave');


//【システム管理メニュー】コース管理
    //◆コース一覧
    Route::get('course', [App\Http\Controllers\System\CourseController::class, 'index'])->name('systemCourseAdmin');
    //◆検索
    Route::match(['get', 'post'],'course-search', [App\Http\Controllers\System\CourseController::class, 'searchCourse'])->name('systemCourseAdminSearch');
    //◆新規＆更新
    Route::match(['get', 'post'],'course-save', [App\Http\Controllers\System\CourseController::class, 'saveCourse'])->name('systemSaveCourse');
    //◆コース削除
    Route::post('course-delete', [App\Http\Controllers\System\CourseController::class, 'deleteCourse'])->name('systemCourseDelete');
    //◆単語一覧
    Route::post('course-addword', [App\Http\Controllers\System\CourseController::class, 'addWord'])->name('systemCourseAddWord');
    //◆【管理用】単語一覧
    Route::match(['get', 'post'],'course-wordlist', [App\Http\Controllers\System\CourseController::class, 'wordList'])->name('systemCourseWordList');
    //◆単語一覧　＞　新規
    Route::post('add-word', [App\Http\Controllers\System\CourseController::class, 'addWordAdd'])->name('systemCourseAddWord');
    //◆単語一覧　＞　テストモードの新規
    Route::post('wordtest', [App\Http\Controllers\System\CourseController::class, 'wordTest'])->name('systemCourseWordTest');
    //◆単語一覧　＞　単語の削除
    Route::post('addword-delete', [App\Http\Controllers\System\CourseController::class, 'deleteWord'])->name('systemCourseAddWordDelete');

//【システム管理メニュー】スコア履歴
    //◆システム管理メニュー　＞　スコア履歴一覧
    Route::get('typing-history', [App\Http\Controllers\System\TranscriptController::class, 'index'])
        ->name('systemTypingHistory');
    //◆システム管理メニュー　＞　スコア履歴一覧　＞　検索＆エクセル出力
    Route::match(['get', 'post'],'typing-history-search', [App\Http\Controllers\System\TranscriptController::class, 'search'])
        ->name('systemTypingHistorySearch');
    //◆システム管理メニュー　＞　スコア履歴一覧　＞　PDF出力
    Route::post('typing-history-pdf', [App\Http\Controllers\System\TranscriptController::class, 'pdf'])
        ->name('systemTypingHistoryPdf');

//【システム管理メニュー】バトルスコア履歴
    //◆システム管理メニュー　＞　バトルスコア履歴一覧
    Route::get('battle', [App\Http\Controllers\System\BattleController::class, 'index'])
        ->name('systemBattle');
    //◆システム管理メニュー　＞　スコア履歴一覧　＞　検索
    Route::match(['get', 'post'],'battle-search', [App\Http\Controllers\System\BattleController::class, 'search'])
        ->name('systemBattleSearch');
    //◆システム管理メニュー　＞　スコア履歴一覧　＞　削除
    Route::post('battle-delete', [App\Http\Controllers\System\BattleController::class, 'delete'])
        ->name('systemBattleDelete');

//【システム管理メニュー】ベストスコア
    //◆システム管理メニュー　＞　ベストスコア一覧
    Route::get('bestscore', [App\Http\Controllers\System\BestScoreController::class, 'index'])
        ->name('systemBestscore');
    //◆システム管理メニュー　＞　ベストスコア一覧　＞　検索＆エクセル出力
    Route::match(['get', 'post'],'bestscore-search', [App\Http\Controllers\System\BestScoreController::class, 'search'])
        ->name('systemBestscoreSearch');
    //◆システム管理メニュー　＞　ベストスコア一覧　＞　PDF出力
    Route::post('bestscore-pdf', [App\Http\Controllers\System\BestScoreController::class, 'pdf'])
        ->name('systemBestscorePdf');

 //【システム管理メニュー】バレッド専用　ICTタイピング検定
    Route::get('vldtest', [App\Http\Controllers\System\VldtestController::class, 'index'])
        ->name('systemBldtestIndex');
    Route::match(['get', 'post'],'vldtest-search', [App\Http\Controllers\System\VldtestController::class, 'search'])
        ->name('systemBldtestUpdate');
    Route::post('vldtest-update', [App\Http\Controllers\System\VldtestController::class, 'update'])
        ->name('systemBldtestUpdate');

//【システム管理メニュー】メンテナンス情報管理
    //◆システム管理メニュー　＞　メンテナンス情報一覧
    Route::get('maintenance-info', [App\Http\Controllers\System\MaintenanceController::class, 'index'])

        ->name('systemInfoAdmin');
    //◆システム管理メニュー　＞　メンテナンス情報一覧 ＞　検索
    Route::match(['get', 'post'],'maintenance-search', [App\Http\Controllers\System\MaintenanceController::class, 'search'])

        ->name('systemInfoSearch');
    //◆システム管理メニュー　＞　メンテナンス情報一覧 ＞　新規or編集ページへ遷移
    Route::post('maintenance-add', [App\Http\Controllers\System\MaintenanceController::class, 'add'])

        ->name('systemInfoAdd');
    //◆システム管理メニュー　＞　メンテナンス情報一覧 ＞　新規＆更新
    Route::post('maintenance-new', [App\Http\Controllers\System\MaintenanceController::class, 'new'])

        ->name('systemInfoNew');

//【システム管理メニュー】ブログ管理
    //◆システム管理メニュー　＞　ブログ一覧
    Route::get('bloglist', [App\Http\Controllers\System\BlogController::class, 'index'])

        ->name('systemBlogIndex');
    //◆システム管理メニュー　＞　ブログ一覧　＞　検索
    Route::match(['get', 'post'],'blog-search', [App\Http\Controllers\System\BlogController::class, 'search'])

        ->name('systemBlogSearch');
    //◆システム管理メニュー　＞　ブログ一覧　＞　編集画面に遷移
    Route::post('blog-edit', [App\Http\Controllers\System\BlogController::class, 'edit'])

        ->name('systemBlogEdit');
    //◆システム管理メニュー　＞　ブログ一覧　＞　新規＆更新保存＆削除　※削除も同じメソッド
    Route::post('blog-save', [App\Http\Controllers\System\BlogController::class, 'save'])

        ->name('systemBlogSave');

//【システム管理メニュー】管理画面ログイン履歴
    //◆システム管理メニュー　＞　管理画面ログイン履歴一覧
    Route::get('login-history', [App\Http\Controllers\System\LoginHistoryController::class, 'index'])

        ->name('systemLoginHistory');
    //◆システム管理メニュー　＞　管理画面ログイン履歴　＞検索＆エクセル出力
    Route::match(['get', 'post'],'login-history-search', [App\Http\Controllers\System\LoginHistoryController::class, 'search'])

        ->name('systemLoginHistorySearach');

//【システム管理メニュー】基本設定
    //◆システム管理メニュー　＞　基本設定一覧
    Route::get('setting', [App\Http\Controllers\System\SettingController::class, 'index'])

        ->name('systemSettingIndex');

    //◆システム管理メニュー　＞　更新
    Route::post('setting-update', [App\Http\Controllers\System\SettingController::class, 'update'])

        ->name('systemSettingUpdate');

//【システム管理メニュー】ノート（開発ノート）
    //◆システム管理メニュー　＞　ノート一覧
    Route::get('note', [App\Http\Controllers\System\NoteController::class, 'index'])

        ->name('systemNoteIndex');
    //◆システム管理メニュー　＞　ノート検索
    Route::match(['get', 'post'],'note-search', [App\Http\Controllers\System\NoteController::class, 'search'])

        ->name('systemNoteSearch');
    //◆システム管理メニュー　＞　ノート編集画面遷移
    Route::post('note-edit', [App\Http\Controllers\System\NoteController::class, 'edit'])

        ->name('systemNoteEdit');
    //◆システム管理メニュー　＞　ノート新規、更新、削除
    Route::post('note-update', [App\Http\Controllers\System\NoteController::class, 'update'])

        ->name('systemNoteUpdate');

//【システム管理メニュー】ファイル整理
    //◆音声ファイル一覧
    Route::get('file', [App\Http\Controllers\System\FilecleanupController::class, 'index'])->name('systemFileIndex');
    //◆削除
    Route::post('file-delete', [App\Http\Controllers\System\FilecleanupController::class, 'delete'])->name('systemFileDelete');
    //◆音声ファイルか画像ファイルか選択
    Route::post('file-select', [App\Http\Controllers\System\FilecleanupController::class, 'select'])->name('systemFileSelect');

//【システム管理メニュー】検証
    //◆検証一覧
    Route::get('check', [App\Http\Controllers\System\CheckController::class, 'index'])->name('systemCheckIndex');
    //◆エラーボタン押下
    Route::post('error', [App\Http\Controllers\System\CheckController::class, 'error'])->name('systemCheckError');
//【システム管理メニュー】地図コースの画像アップロード
    //◆単語一覧（画像ファイルつき）＆検索
    Route::match(['get', 'post'],'fileupload', [App\Http\Controllers\System\FileUploadController::class, 'index'])->name('systemFileUpload');
    //◆単語一覧（画像ファイルつき）削除
    Route::post('fileupload-delete', [App\Http\Controllers\System\FileUploadController::class, 'delete'])->name('systemFileUploadDelete');
    //◆単語一覧（画像ファイルつき）画像アップロード
    Route::post('fileupload-up', [App\Http\Controllers\System\FileUploadController::class, 'upload'])->name('systemFileUploadUpload');

//【システム管理メニュー】請求書
    //◆一覧＆検索
    Route::match(['get', 'post'],'invoice', [App\Http\Controllers\System\InvoiceController::class, 'index']);    
    //◆編集ページへ遷移
    Route::get('invoice-edit/{invoiceId}', [App\Http\Controllers\System\InvoiceController::class, 'edit']);
    //◆新規ページへ遷移
    Route::get('invoice-add/{add}', [App\Http\Controllers\System\InvoiceController::class, 'add']);
    //◆新規＆編集　保存
    Route::post('invoice-store', [App\Http\Controllers\System\InvoiceController::class, 'store']);
    //◆削除
    Route::post('invoice-delete', [App\Http\Controllers\System\InvoiceController::class, 'delete'])->name('systemInvoiceDel');
    //◆PDF請求書、受領書
    Route::match(['get', 'post'],'invoice-pdf', [App\Http\Controllers\System\InvoiceController::class, 'pdf']);  
   

//【学校管理メニュー】メインメニュー
    //●学校管理メニュー　＞　メインメニュー
    Route::get('school/menu', [App\Http\Controllers\School\MenuController::class, 'index'])
        ->name('schoolMenu');
    //●学校管理メニュー　＞　メインメニュー　＞　全てのお知らせ
    Route::get('school/menu-allinfo', [App\Http\Controllers\School\MenuController::class, 'allinfo'])
        ->name('schoolAllInfo');

//【学校管理メニュー】学校アカウント
    //●学校管理メニュー　＞　学校アカウント情報（already in useの為、admin→addmin）
    Route::get('school/profile', [App\Http\Controllers\School\ProfileController::class, 'index'])
        ->name('adminInfo');
    //●学校管理メニュー　＞　学校アカウント情報　＞　パスワードリセット
    Route::post('school/userpas-change', [App\Http\Controllers\School\ProfileController::class, 'changePassword'])
        ->name('adminChangePassword');

//【学校管理メニュー】先生情報
    //●学校管理メニュー　＞　先生一覧
    Route::get('school/teacherlist', [App\Http\Controllers\School\TeacherlistController::class, 'index'])
        ->name('teacherList');
    //●学校管理メニュー　＞　先生一覧　＞　検索
    Route::match(['get', 'post'],'school/teacher-search', [App\Http\Controllers\School\TeacherlistController::class, 'search'])
        ->name('teacherListSearch');
    //●学校管理メニュー　＞　先生一覧　＞　新規＆更新
    Route::post('school/teacher-list-new', [App\Http\Controllers\School\TeacherlistController::class, 'new'])
        ->name('teacherListNew');
    //●学校管理メニュー　＞　先生一覧　＞　削除
    Route::post('school/teacher-delete', [App\Http\Controllers\School\TeacherlistController::class, 'delete'])
        ->name('teacherDelete');
    //●学校管理メニュー　＞　先生一覧　＞　パスワードリセット
    Route::post('school/teacher-pass-reset', [App\Http\Controllers\School\TeacherlistController::class, 'reset'])
        ->name('teacherResetPassword');
    //●学校管理メニュー　＞　先生一覧　＞　エクセル一括登録　※一括登録失敗時、戻るボタン押下のケースがあるため
    Route::match(['get', 'post'],'school/teacher-import', [App\Http\Controllers\School\TeacherlistController::class, 'importCsv'])
        ->name('teacherImportTeacher');
    //●学校管理メニュー　＞　先生一覧　＞　エクセルテンプレートダウンロード
    Route::get('school-teacher-exceltemplate', [App\Http\Controllers\School\TeacherlistController::class, 'download'])
    ->name('schoolTeacherDownload');

//【学校管理メニュー】生徒情報
    //●学校管理メニュー　＞　生徒一覧※クラス一覧から学年、組のパラメータをもって遷移するケースもあるためPOSTも可
    Route::match(['get', 'post'],'school/studentlist', [App\Http\Controllers\School\StudentlistController::class, 'index'])
        ->name('studentList');
    //●学校管理メニュー　＞　生徒一覧　＞　検索
    Route::match(['get', 'post'],'school/student-search', [App\Http\Controllers\School\StudentlistController::class, 'search'])
        ->name('studentListSearch');
    //●学校管理メニュー　＞　生徒一覧　＞　新規＆更新　※ページネーションがあるため
    Route::match(['get', 'post'],'school/student-list-new', [App\Http\Controllers\School\StudentlistController::class, 'new'])
        ->name('studentListnew');
    //●学校管理メニュー　＞　生徒一覧　＞　削除
    Route::post('school/student-delete', [App\Http\Controllers\School\StudentlistController::class, 'delete'])
        ->name('studentDelete');
    //●学校管理メニュー　＞　生徒一覧　＞　パスワードリセット
    Route::post('school/student-pass-reset', [App\Http\Controllers\School\StudentlistController::class, 'resetPassword'])
        ->name('studentResetPassword');
    //●学校管理メニュー　＞　生徒一覧　＞　エクセル一括登録 ※一括登録失敗時、戻るボタン押下のケースがあるため
    Route::match(['get', 'post'],'school/student-import', [App\Http\Controllers\School\StudentlistController::class, 'import'])
        ->name('studentImportStudent');
    //●学校管理メニュー　＞　生徒一覧　＞　エクセルテンプレートダウンロード
    Route::get('school-student-exceltemplate', [App\Http\Controllers\School\StudentlistController::class, 'download'])
        ->name('schoolStudentDownload');
    //●学校管理メニュー　＞　生徒一覧　＞　本日登録した生徒一覧（インポート後の確認）
    Route::get('school/today-student', [App\Http\Controllers\School\StudentlistController::class, 'addstudent'])
        ->name('schoolAddStudent');

//【学校管理メニュー】クラス一覧
     //●学校管理メニュー　＞　クラス一覧
    Route::get('school/byclass', [App\Http\Controllers\School\ClasslistSchoolController::class, 'index'])
        ->name('byClass');
     //●学校管理メニュー　＞　クラス一覧　＞　クラス別所属生徒
    Route::match(['get', 'post'],'school/byclass-belongs', [App\Http\Controllers\School\ClasslistSchoolController::class, 'belongs'])
        ->name('byclassBelongs');

//【学校管理メニュー】スコア履歴EXCEL＆PDF
    //●学校管理メニュー　＞　スコア履歴一覧
    Route::get('school/transcript-print', [App\Http\Controllers\School\TranscriptController::class, 'index'])
        ->name('transcriptPrint');
    //●学校管理メニュー　＞　スコア履歴一覧　＞　検索＆エクセル出力
    Route::match(['get', 'post'],'school/transcript-print-bt', [App\Http\Controllers\School\TranscriptController::class, 'searchAndExcel'])
        ->name('school.transcriptSaE');
    //●学校管理メニュー　＞　スコア履歴一覧　＞　PDF出力
    Route::match(['get', 'post'],'school/transcript-print-pdf', [App\Http\Controllers\School\TranscriptController::class, 'pdf'])
        ->name('school.transcriptPdf');

//【学校管理メニュー】ランキングバトルスコア一覧
    Route::get('school/battlescore-history', [App\Http\Controllers\School\BattleScoreController::class, 'index'])
        ->name('SchoolBattleScore');
    Route::post('school/battlescore-history-search', [App\Http\Controllers\School\BattleScoreController::class, 'search'])
        ->name('SchoolBattleScoreSearch');

//【学校管理メニュー】生徒別ベストスコアEXCEL＆PDF
    //●学校管理メニュー　＞　生徒別ベストスコア一覧
    Route::get('school/bestscore', [App\Http\Controllers\School\BestScoreController::class, 'index'])
        ->name('schoolBestScoreIndex');
    //●学校管理メニュー　＞　生徒別ベストスコア一覧　＞　検索＆エクセル出力
    Route::match(['get', 'post'],'school/bestscore-search', [App\Http\Controllers\School\BestScoreController::class, 'search'])
        ->name('schoolBestScoreSearch');
    //●学校管理メニュー　＞　生徒別ベストスコア一覧　＞　PDF出力
    Route::match(['get', 'post'],'school/transcript-bestscore-print-pdf', [App\Http\Controllers\School\BestScoreController::class, 'pdf'])
        ->name('schoolBestScorePdf');

//【学校管理メニュー】タイピングコース追加
    //●学校管理メニュー　＞　タイピングコース一覧　※ページネーションなし
    Route::get('school/add-course', [App\Http\Controllers\School\AddCourseController::class, 'index'])
        ->name('school.addCourseIndex');
    //●学校管理メニュー　＞　タイピングコース一覧　＞　検索
    Route::match(['get', 'post'],'school/addcourse-search', [App\Http\Controllers\School\AddCourseController::class, 'search'])
        ->name('school.addCourseSearch');
    //●学校管理メニュー　＞　タイピングコース一覧　＞　コース新規＆更新
    Route::post('school/add-course-new', [App\Http\Controllers\School\AddCourseController::class, 'new'])
        ->name('school.addCourseNew');
    //●学校管理メニュー　＞　タイピングコース一覧　＞　コース削除
    Route::post('school/addcourse-delete', [App\Http\Controllers\School\AddCourseController::class, 'deleteCourse'])
        ->name('schoolCourseDelete');
    //●学校管理メニュー　＞　タイピングコース一覧　＞　単語一覧
    Route::post('school/addcourse-add-word', [App\Http\Controllers\School\AddCourseController::class, 'addWord'])
        ->name('school.addCourseAddWord');
    //●学校管理メニュー　＞　タイピングコース一覧　＞　単語一覧　＞　単語新規＆更新
    Route::post('school/add-word', [App\Http\Controllers\School\AddCourseController::class, 'addwordAdd'])
        ->name('school.addwordAdd');
    //●学校管理メニュー　＞　タイピングコース一覧　＞　単語一覧　＞　削除
    Route::post('school/addcourse-word-delete', [App\Http\Controllers\School\AddCourseController::class, 'deleteWord'])
        ->name('schoolWordDelete');

//【学校管理メニュー】管理画面ログイン履歴
    //●学校管理メニュー　＞　ログイン履歴一覧
    Route::get('school/login-history', [App\Http\Controllers\School\LoginhistoryController::class, 'index'])
        ->name('loginHistory');
    //●学校管理メニュー　＞　ログイン履歴一覧　＞　検索
    Route::match(['get', 'post'],'school/login-History-search', [App\Http\Controllers\School\LoginhistoryController::class, 'search'])
        ->name('loginHistorySearch');

    //メンテナンス情報管理
    Route::get('info-admin', [App\Http\Controllers\School\LoginhistoryController::class, 'index'])
    ->name('infoAdmin');

//【学校管理メニュー】請求
    Route::get('school/invoice', [App\Http\Controllers\School\InvoiceController::class, 'index']);
    Route::post('school/invoice-pdf', [App\Http\Controllers\School\InvoiceController::class, 'pdf']);

//【学校管理メニュー】バレッド専用　ICTタイピング検定
    Route::get('school/vld-test', [App\Http\Controllers\School\VldtestController::class, 'index']);
    Route::post('school/vldtest-update', [App\Http\Controllers\School\VldtestController::class, 'update']);

//【生徒メニュー】マイアカウント
    //◆生徒マイメニュー　＞　生徒情報ページ
    Route::get('student/profile', [App\Http\Controllers\User\ProfileStudentController::class, 'studentProfile'])
        ->name('studentProfile');
    //◆生徒マイメニュー　＞　トライアルの生徒情報ページ
    Route::get('student/profile-trial', [App\Http\Controllers\User\ProfileController::class, 'studentProfile'])
        ->name('studentProfileTrial');
    //◆生徒マイメニュー　＞　生徒情報ページ　パスワード変更（トライアル＆生徒で利用）
    Route::post('student/userpas-change', [App\Http\Controllers\User\ProfileStudentController::class, 'studentPasswordUpdate'])
        ->name('studentPasswordUpdate');
    //◆生徒マイメニュー　＞　生徒情報ページ　学年、組、出席番号の更新（生徒で利用）
    Route::post('student/profile-update', [App\Http\Controllers\User\ProfileStudentController::class, 'studentProfileUpdate'])
        ->name('studentProfileUpdate');
    
//【先生メニュー】メインメニュー
    //■先生メニュー　＞　メインメニュー
    Route::get('teacher/menu', [App\Http\Controllers\User\MenuTeacherController::class, 'teacherIndex'])
        ->name('teacher.menu');
    //■先生メニュー　＞　メインメニュー　＞　すべてのお知らせ
    Route::get('teacher/menu-allinfo', [App\Http\Controllers\User\MenuTeacherController::class, 'allInformation'])
        ->name('teacher.allinfo');
    //■先生メニュー　＞　メインメニュー　＞　紹介動画
    Route::get('teacher/movie-introduction', [App\Http\Controllers\User\MenuTeacherController::class, 'movie']);

//【先生メニュー】マイアカウント
    //■先生メニュー　＞　プロフィール
    Route::get('teacher/profile', [App\Http\Controllers\User\ProfileController::class, 'teacherIndex'])
        ->name('teacher.profile');
    //■先生メニュー　＞　名前の更新
    Route::post('teacher/profile-update', [App\Http\Controllers\User\ProfileController::class, 'update'])
        ->name('teacher.profileUpdate');
    //■先生メニュー　＞　パスワードの更新
    Route::post('teacher/userpas-change', [App\Http\Controllers\User\ProfileController::class, 'updateTeacherPassword'])
        ->name('teacher.passwordUpdate');

//【先生メニュー】生徒一覧
    //■先生メニュー　＞　生徒一覧
    Route::get('teacher/student-list', [App\Http\Controllers\User\StudentlistController::class, 'index'])
        ->name('teacher.studentListIndex');
    //■先生メニュー　＞　生徒一覧　＞　新規
    Route::match(['get', 'post'],'teacher/student-list-new', [App\Http\Controllers\User\StudentlistController::class, 'new'])
        ->name('teacher.studentListnew');
    //■先生メニュー　＞　生徒一覧　＞　検索
    Route::match(['get', 'post'],'teacher/student-search', [App\Http\Controllers\User\StudentlistController::class, 'search'])
        ->name('teacher.studentSearch');
    //■先生メニュー　＞　生徒一覧　＞　更新
    Route::match(['get', 'post'],'teacher/student-list-update', [App\Http\Controllers\User\StudentlistController::class, 'update'])
        ->name('teacher.studentListUpdate');
    //■先生メニュー　＞　生徒一覧　＞　パスワードリセット　POST OK
    Route::post('teacher/userpas-reset', [App\Http\Controllers\User\StudentlistController::class, 'reset'])
        ->name('teacher.studentListReset');

//【トライアル先生メニュー】トライアル生徒一覧
    //■トライアル先生メニュー　＞　トライアル生徒一覧
    Route::get('teacher/studentlists', [App\Http\Controllers\User\StudentlistTrialController::class, 'index'])
        ->name('teacher.studentListTrialIndex');
    //■トライアル先生メニュー　＞　トライアル生徒一覧　＞　トライアル検索
    Route::post('teacher/students-search', [App\Http\Controllers\User\StudentlistTrialController::class, 'search'])
        ->name('teacher.studenttrialTrialSearch');
    //■トライアル先生メニュー　＞　トライアル生徒一覧　＞　トライアル更新
    Route::post('teacher/students-update', [App\Http\Controllers\User\StudentlistTrialController::class, 'update'])
        ->name('teacher.studentListTrialUpdate');
    //■トライアル先生メニュー　＞　トライアル生徒一覧　＞　トライアルパスワードリセット
    Route::post('teacher/students-pas-reset', [App\Http\Controllers\User\StudentlistTrialController::class, 'reset'])
        ->name('teacher.studentListTrialReset');

//【先生メニュー】クラス一覧
     //●先生メニュー　＞　クラス一覧
     Route::get('teacher/byclass', [App\Http\Controllers\User\ClasslistController::class, 'index'])
        ->name('teacher.byClass');
    //●先生メニュー　＞　クラス一覧　＞　クラス別所属生徒
    Route::match(['get', 'post'],'teacher/byclass-belongs', [App\Http\Controllers\User\ClasslistController::class, 'belongs'])
        ->name('teacher.byclassBelongs');

//【先生メニュー】スコア履歴（エクセル＆PDF）
    //■先生メニュー　＞　スコア履歴一覧
    Route::get('teacher/transcript-print', [App\Http\Controllers\User\TranscriptPrintController::class, 'index'])
        ->name('teacherTranscriptPrint');
    //■先生メニュー　＞　スコア履歴　＞　検索＆エクセル出力
    Route::match(['get', 'post'],'teacher/transcript-print-bt', [App\Http\Controllers\User\TranscriptPrintController::class, 'searchAndExcel'])
        ->name('teacher.transcriptSaE');
    //■先生メニュー　＞　スコア履歴　＞　PDF(シンプルとカラフル両方とも)
    Route::match(['get', 'post'],'teacher/transcript-print-pdf', [App\Http\Controllers\User\TranscriptPrintController::class, 'pdf'])
        ->name('teacher.transcriptPdf');

//【先生メニュー】ベストスコア（エクセル＆PDF）
    //■先生メニュー　＞　ベストスコア
    Route::get('teacher/bestscore', [App\Http\Controllers\User\BestScoreController::class, 'index'])
        ->name('teacher.bestScoreIndex');
    //■先生メニュー　＞　ベストスコア　＞　検索＆エクセル出力
    Route::match(['get', 'post'],'teacher/bestscore-search', [App\Http\Controllers\User\BestScoreController::class, 'search'])
        ->name('teacher.bestScoreSearch');
    //■先生メニュー　＞　ベストスコア　＞　PDF（シンプル＆カラフル）
    Route::match(['get', 'post'],'teacher/transcript-bestscore-print-pdf', [App\Http\Controllers\User\BestScoreController::class, 'pdf'])
    ->name('teacher.transcriptBestScorePdf');

//【先生メニュー】ランキングバトルスコア一覧
    Route::get('teacher/battlescore-history', [App\Http\Controllers\User\BattleScoreController::class, 'index'])
    ->name('TeacherBattleScore');
    Route::post('teacher/battlescore-history-search', [App\Http\Controllers\User\BattleScoreController::class, 'search'])
    ->name('TeacherBattleScoreSearch');

//【先生メニュー】コース一覧＆単語一覧
    //■先生メニュー　＞　コース一覧
    Route::get('teacher/add-course', [App\Http\Controllers\User\AddCourseController::class, 'index'])
        ->name('teacher.addCourseIndex');
    //■先生メニュー　＞　コース一覧　＞　検索
    Route::match(['get', 'post'],'teacher/addcourse-search', [App\Http\Controllers\User\AddCourseController::class, 'search'])
        ->name('teacher.addCourseIndex');
    //■先生メニュー　＞　コース一覧　＞　新規＆更新
    Route::post('teacher/add-course-new', [App\Http\Controllers\User\AddCourseController::class, 'new'])
        ->name('teacher.addCourseNew');
    //■先生メニュー　＞　コース一覧　＞　削除
    Route::post('teacher/addcourse-delete', [App\Http\Controllers\User\AddCourseController::class, 'deleteCourse'])
    ->name('teacherCourseDelete');
    //■先生メニュー　＞　コース一覧　＞　単語一覧 ※ページネーションなし
    Route::post('teacher/addcourse-add-word', [App\Http\Controllers\User\AddCourseController::class, 'addWord'])
        ->name('teacher.addCourseAddWord');
    //■先生メニュー　＞　コース一覧　＞　単語一覧　＞　新規＆更新
    Route::post('teacher/add-word', [App\Http\Controllers\User\AddCourseController::class, 'addwordAdd'])
        ->name('teacher.addwordAdd');
    //■先生メニュー　＞　コース一覧　＞　単語一覧　＞　削除
    Route::post('teacher/addcourse-word-delete', [App\Http\Controllers\User\AddCourseController::class, 'deleteWord'])
        ->name('teacherWordDelete');

});  //ここまでは、ログインしている人のルーティング

