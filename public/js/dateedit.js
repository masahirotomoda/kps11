/****************************************
* assign date
*****************************************/
function dateEditFunction(dateType) {
  switch (dateType) {
    case '昨日':
      dateInputFunction('昨日', '00:00', '23:59');
      break;
    case '今日':
      dateInputFunction('今日', '00:00', '23:59');
      break;
    case 'AM':
      dateInputFunction('今日', '00:00', '11:59');
      break;
    case 'PM':
      dateInputFunction('今日', '12:00', '23:59');
      break;
    default:
  }
}
/****************************************
* rewrite the date
*****************************************/
function dateInputFunction(dateType, startTime, endTime) {
  const dateValue = new Date();
  if (dateType == '昨日') {
    dateValue.setDate(dateValue.getDate() - 1);
  }
  const dateYear = dateValue.getFullYear();
  const dateMonth = ('00' + (dateValue.getMonth()+1)).slice(-2);
  const dateDay = ('00' + dateValue.getDate()).slice(-2);

  document.getElementById('start_date').value = dateYear + '-' + dateMonth + '-' + dateDay;
  document.getElementById('inputMDEx1').value = startTime;
  document.getElementById('end_date').value = dateYear + '-' + dateMonth + '-' + dateDay;
  document.getElementById('inputMDEx2').value = endTime;
}
