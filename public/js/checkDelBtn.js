/****************************************
 * delete
 *****************************************/
document.addEventListener(
    "DOMContentLoaded",
    function (event) {
        const targetButton = document.getElementById("del");
        const triggerCheckbox = document.getElementsByName("checks[]");
        const edit = document.getElementsByName("edit");

        targetButton.disabled = true;
        targetButton.classList.add("is-inactive");
        let len_edit = edit.length;
        for (let c_edit = 0; c_edit < len_edit; c_edit++) {
            triggerCheckbox[c_edit].addEventListener(
                "change",
                function () {
                    if (this.checked) {
                        targetButton.disabled = false;
                        targetButton.classList.remove("is-inactive");
                        targetButton.classList.add("is-active");
                    } else {
                        let flg = true;
                        for (let j = 0; j < len_edit; j++) {
                            if (triggerCheckbox[j].checked == true) {
                                flg = false;
                                break;
                            }
                        }
                        if (flg == true) {
                            targetButton.disabled = true;
                            targetButton.classList.remove("is-active");
                            targetButton.classList.add("is-inactive");
                        }
                    }
                },
                false
            );
        }
    },
    false
);
