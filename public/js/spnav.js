$(function () {
    $('.navBtn').click(function () {
        $('#naviOut').toggleClass('navOpen');
    });
    $('.freePageHeadBtn li a').click(function () {
        $('#naviOut').toggleClass('navOpen');
    });
    $(window).on('load resize', function () {
        var breakpoint = 680;
        if (window.innerWidth > breakpoint) {
            $('#naviOut').removeClass('navOpen');
        }
    });
});
$(function () {
    var topBtn = $('.pagetop');
    topBtn.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
});
