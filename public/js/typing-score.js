"use strict";
let speedPoint;
let misstypePoint;
let OneMojiTime;
let scoreOneMoji;
let scoreMoji;
let scoreSpeed;
let scoreAllSpeed;
let scoreSpeedRate;
function typingscore() {
    typingTimeResult =
        Math.round((endDate.getTime() - beginDate.getTime()) / 10) / 100;
    if (scoretype == 1) {
        typingTime = typingTimeResult.toFixed(1);
        const typeRate = successType / keydownAllCnt;
        typingScore =
            (keydownAllCnt / typingTimeResult) *
            60 *
            typeRate *
            typeRate *
            typeRate;
        if (isNaN(typingScore)) {
            typingScore = 0;
        }
        resultypingScore = Math.ceil(typingScore);
    } else {
        if (courseData.tab == "battle") {
            typingTime = typingTimeResult.toFixed(1);
            speedPoint = 600;
            misstypePoint = 400;
            scoreOneMoji = misstypePoint / successType;
            if (successType - missType < 0) {
                scoreMoji = 0;
            } else {
                scoreMoji = scoreOneMoji * (successType - missType * 4);
            }
            OneMojiTime = 0.18;
            scoreAllSpeed = OneMojiTime * successType;
            scoreSpeedRate = scoreAllSpeed / typingTime;
            if (scoreSpeedRate >= 1) {
                scoreSpeed = speedPoint;
            } else {
                scoreSpeed = speedPoint * scoreSpeedRate;
            }
            typingScore = Math.ceil(scoreMoji + scoreSpeed);
            if (typingScore < 0) {
                typingScore = 0;
            }
            resultypingScore = typingScore;
        } else {
            typingTime = typingTimeResult.toFixed(1);
            if (successType > 199) {
                speedPoint = 60;
            } else if (successType > 100) {
                speedPoint = 50;
            } else {
                speedPoint = 40;
            }
            misstypePoint = 100 - speedPoint;
            scoreOneMoji = misstypePoint / successType;
            if (successType - missType < 0) {
                scoreMoji = 0;
            } else {
                scoreMoji = scoreOneMoji * (successType - missType);
            }
            if (scoretype == 0) {
                OneMojiTime = 0.25;
            } else {
                OneMojiTime = 0.28;
            }
            scoreAllSpeed = OneMojiTime * successType;
            scoreSpeedRate = scoreAllSpeed / typingTime;
            if (scoreSpeedRate >= 1) {
                scoreSpeed = speedPoint;
            } else {
                scoreSpeed = speedPoint * scoreSpeedRate;
            }
            //console.log("speed" + Math.ceil(scoreSpeed));
            //console.log("misstype" + Math.ceil(scoreMoji));
            typingScore = Math.ceil(scoreMoji + scoreSpeed);
            resultypingScore = typingScore;
            //console.log("totalscore" + typingScore);
            if (typingScore == 100) {
                perfect.textContent = "パーフェクト！";
            } else if (typingScore > 89) {
                perfect.textContent = "すばらしい！";
            } else if (typingScore > 69) {
                perfect.textContent = "Good♪さらに上を！！";
            } else if (typingScore > 59) {
                perfect.textContent = "この調子でがんばれ！";
            } else {
                perfect.textContent = "";
            }
        }
    }
}
