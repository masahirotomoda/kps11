/****************************************
 * delete confirm modal
 *****************************************/
var deleteModal = document.getElementById("deleteModal");
deleteModal.addEventListener("show.bs.modal", function (event) {
    let checks1 = document.getElementsByName("checks[]");
    let len_checks = checks1.length;
    let num_checkd = 0;
    let names = "";

    for (let c_chk = 0; c_chk < len_checks; c_chk++) {
        if (checks1[c_chk].checked == true) {
            names = names + checks1[c_chk].id + "<br>";
            console.log(names);
            num_checkd++;
        }
    }

    var title = deleteModal.querySelector(".modal-title");
    var message = deleteModal.querySelector(".modal-body");

    title.textContent = "選択した" + num_checkd + "件のデータを削除します";
    message.innerHTML = names;

    deleteexecute.disabled = false;
});
let deleteexecute = document.getElementById("delete-execute");
deleteexecute.addEventListener("click", () => {
    deleteexecute.innerHTML = "削除しています...";

    setTimeout(function () {
        deleteexecute.disabled = true;
    }, 100);
});
