/****************************************
* click the button
*****************************************/
let studentinfo_update_btn = document.getElementById("studentinfo_update_btn");
studentinfo_update_btn.addEventListener('click', () => {
    setTimeout(function () {
        studentinfo_update_btn.disabled = true;
        studentinfo_update_btn.value = '保存中...';
    }, 100);
});
let change = document.getElementById("change");
change.addEventListener('click', () => {
        setTimeout(function () {
            change.disabled = true;
            change.textContent = 'パスワード変更中...';
        }, 100);
});
