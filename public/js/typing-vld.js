"use strict";
const typingFinishBtn = document.getElementById("typingFinishBtn");
const typeField = document.getElementById("p-typingtestscreen__typesfield");
const typenumberField = document.getElementById("p-typenumber");
const timeField = document.getElementById("p-time");
const typeResultField = document.getElementById("p-typeresultfield");
const inputTypingField = document.getElementById("inputTypingField");
const startBtn = document.getElementById("p-typingscreen__start");
const perfect = document.getElementById("perfect");
const remark = document.getElementById("remark");
let typingTime = courseData.test_time;
let keywords;
let keywordsLength;
let keywordsArr;
let min;
let sec;
let successType = 0;
let typingInterval;
let translateInt = 0;
let multiplicationTimes = 1;
let resultModalTestTime;
let collectRate;
getCurrentCourseData();
min = Math.floor(typingTime / 60);
sec = typingTime % 60;
timeField.textContent = min + "分 " + sec + "秒";
typenumberField.textContent = successType;
inputTypingField.disabled = true;
remark.classList.add("hidden");
function startBtnClick() {
    multiplicationTimes = 1;
    maetugiBtnFlg == true;
    restartFlg = false;
    restartBtn.classList.add("hidden");
    mae.classList.add("hidden");
    tugi.classList.add("hidden");
    translateInt = 0;
    successType = 0;
    getCurrentWordData();

    keywords = typeWords[0].word_mana;
    keywordsArr = [...keywords];
    keywordsLength = keywords.length;

    startBtn.classList.add("hidden");
    countDown.classList.remove("hidden");
    for (let i = 0; i <= countSeconds; i++) {
        setTimeout(() => {
            countDown.textContent = countSeconds - i;
        }, i * 1000);
    }
    setTimeout(() => {
        if (typeWords[0].remark) {
            remark.textContent = typeWords[0].remark;
            remark.classList.remove("hidden");
        }
        countDown.classList.add("hidden");
        restartBtn.classList.remove("hidden");
        startTypingTimer();
        typingFlag = true;
        typingstart();
    }, countSeconds * 1000);
}

function getCurrentWordData() {
    currentWordsData = [];
    if (fromMenuFlg == true) {
        currentCourseId = courseData.id;
    }
    for (let i = 0; i <= typeWordsAll.length - 1; i++) {
        if (typeWordsAll[i]["courseId"] == currentCourseId) {
            currentWordsData.push(typeWordsAll[i]);
        }
    }
    typeWords = currentWordsData;
}

function typingstart() {
    inputTypingField.disabled = false;
    inputTypingField.focus();
    typingFinishBtn.classList.remove("hidden");
    for (let i = 0; i < keywords.length; i++) {
        var element = document.createElement("span");
        element.textContent = keywordsArr[i];
        typeField.appendChild(element);
    }
}
window.addEventListener("keydown", (e) => {
    if (e.code == "Space") {
        e.preventDefault();
    }
});
inputTypingField.addEventListener("keyup", (e) => {
    let inputTypingFieldValue = inputTypingField.value;
    successType = 0;
    for (let i = 0; i < keywordsLength; i++) {
        if (inputTypingFieldValue.substr(i, 1) == keywordsArr[i]) {
            successType++;
            if ((i + 1) % (150 * multiplicationTimes) == 0) {
                multiplicationTimes++;
                translateInt += 10;
                for (let j = 0; j < typeField.children.length; j++) {
                    typeField.children[j].style.transform =
                        "translateY(-" + translateInt + "rem)";
                }
            }
            typenumberField.textContent = successType;
            typeField.children[i].classList.add("p-typingtestscreen__typed");
        } else {
            if (
                typeField.children[i].classList.contains(
                    "p-typingtestscreen__typed"
                )
            ) {
                typeField.children[i].classList.remove(
                    "p-typingtestscreen__typed"
                );
            }
        }
    }
});

function restartBtnClick() {
    restartFlg = true;
    clearInterval(typingInterval);
    Initialization();
}
function finishType() {
    resultModalTestTime = typingTime;
    clearInterval(typingInterval);
    collectRate = successType;
    document.getElementById("p-typingresult--score").textContent = collectRate;
    document.getElementById("p-typingresult--time").textContent =
        min + "分 " + sec + "秒";
    perfect.textContent = "";
    let inputTypingFieldValue = inputTypingField.value;
    for (let i = 0; i < keywordsLength; i++) {
        const element = document.createElement("span");
        element.textContent = keywordsArr[i];
        typeResultField.appendChild(element);
        if (i < inputTypingFieldValue.length) {
            if (inputTypingFieldValue.substr(i, 1) == keywordsArr[i]) {
                typeResultField.children[i].classList.add(
                    "p-typingtestresultscreen__typed"
                );
            } else {
                typeResultField.children[i].classList.add(
                    "p-typingtestscreen__misstyped"
                );
                typeResultField.children[i].classList.add(
                    "p-typingtestresultscreen__typed"
                );
            }
        }
    }
    typingResultModal.classList.remove("hidden");
    inputTypingField.disabled = true;
    getResutAjax();
}
function Initialization() {
    remark.classList.add("hidden");
    typingFlag = false;
    inputTypingField.value = "";
    inputTypingField.disabled = true;
    translateInt = 0;
    successType = 0;
    typenumberField.textContent = successType;
    typingTime = courseData.test_time;
    showTime(timeField);
    typingFinishBtn.classList.add("hidden");
    countDown.classList.add("hidden");
    if (!maetugiBtnFlg) {
        mae.classList.add("hidden");
        tugi.classList.add("hidden");
        startBtn.classList.add("hidden");
    } else {
        mae.classList.remove("hidden");
        tugi.classList.remove("hidden");
        startBtn.classList.remove("hidden");
    }

    while (typeField.firstChild) {
        typeField.removeChild(typeField.firstChild);
    }
    while (typeResultField.firstChild) {
        typeResultField.removeChild(typeResultField.firstChild);
    }
}
function closeResultModal() {
    Initialization();
    typingResultModal.classList.add("hidden");
}
