/****************************************
 * PDF output
 *****************************************/
let pdf = document.getElementById("pdf");
const pdf1 = document.getElementById("pdf1");
const pdf2 = document.getElementById("pdf2");
const copy = document.getElementById("copy");

pdf1.addEventListener("click", () => {
    let printbtn = document.getElementsByName("checks1[]");
    let is_print = null;
    for (i = 0; i < printbtn.length; i++) {
        if (printbtn[i].checked === true) {
            is_print = "yes";
        }
    }
    if (is_print == "yes") {
        pdf.target = "_blank";
    } else {
        alert("※請求書のチェック項目に、チェックがありません。");
        pdf1.target = "_self";
        setTimeout(function () {
            pdf1.disabled = true;
            pdf1.innerHTML = "処理中...";
        }, 100);
        setTimeout(function () {
            pdf1.disabled = false;
            pdf1.value = "請求書PDF";
        }, 6000);
    }
});
pdf2.addEventListener("click", () => {
    let printbtn = document.getElementsByName("checks2[]");
    let is_print = null;
    for (i = 0; i < printbtn.length; i++) {
        if (printbtn[i].checked === true) {
            is_print = "yes";
        }
    }
    if (is_print == "yes") {
        pdf.target = "_blank";
    } else {
        pdf2.target = "_self";
        alert("■受領書のチェック項目に、チェックがありません。");
        setTimeout(function () {
            pdf2.disabled = true;
            pdf2.innerHTML = "処理中...";
        }, 100);
        setTimeout(function () {
            pdf2.disabled = false;
            pdf2.value = "受領書PDF";
        }, 6000);
    }
});
if (copy !== null) {
    copy.addEventListener("click", () => {
        let printbtn = document.getElementsByName("checks3[]");
        let is_copy = 0;
        for (i = 0; i < printbtn.length; i++) {
            if (printbtn[i].checked === true) {
                is_copy += 1;
            }
        }

        if (is_copy > 1) {
            alert("★コピーのチェックは1つのみです。");
        } else if (is_copy == 0) {
            alert("★コピーにチェックがありません。");
        }
        console.log(is_copy);
        setTimeout(function () {
            copy.disabled = true;
            copy.innerHTML = "処理中...";
        }, 100);
        setTimeout(function () {
            copy.disabled = false;
            copy.value = "請求書コピー";
        }, 6000);
    });
}
