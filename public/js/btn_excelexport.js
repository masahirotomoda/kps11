/****************************************
 * Excel download
 *****************************************/
let excelbtn = document.getElementById("excelbtn");
excelbtn.addEventListener("click", () => {
    setTimeout(function () {
        excelbtn.disabled = true;
        excelbtn.value = "ダウンロード中...";
    }, 100);
    setTimeout(function () {
        excelbtn.disabled = false;
        excelbtn.value = "データの一括ダウンロード(Excel)";
    }, 4000);
});
