"use strict";
const MapTopContainer = document.getElementById("map_top");
const MapContainer = document.getElementById("map_img");
const challengeBtnChecked = document.getElementById(
    "p-typingscreen__startscreen__challenge"
);
const optionContainer = document.getElementById(
    "p-typingscreen__startscreen__option__container"
);
const startBtn = document.getElementById("p-typingscreen__start");
let preWordId = null;
let courseId;
let courseIdImg;
let WordId;
let WordIdImg;

if (sound == "yes") {
    soundBtn.checked = true;
} else {
    nosoundBtn.checked = true;
}
getCurrentCourseData();
for (let i = 0; i < courseList.length; i++) {
    courseId = courseList[i]["id"];
    courseIdImg = document.createElement("img");
    courseIdImg.src = "/img/map/maptop" + courseId + ".png";
    courseIdImg.height = 80;
    courseIdImg.id = "maptop" + courseId;
    courseIdImg.classList.add("hidden");
    courseIdImg.classList.add("maptop");
    MapTopContainer.appendChild(courseIdImg);
}
document.getElementById("maptop" + currentCourseId).classList.remove("hidden");
function challengeBtnClick() {
    const optionSoundContainer = document.getElementById(
        "p-typingscreen__startscreen__sound__container"
    );
    if (challengeBtnChecked.checked == true) {
        optionSoundContainer.classList.add("hidden");
    } else {
        optionSoundContainer.classList.remove("hidden");
    }
}
function startBtnClick() {
    const testPronunciation = new Audio(routeLink + "audio/silence.mp3");
    testPronunciation.play();
    restartFlg = false;
    maetugiBtnFlg == true;
    restartBtn.classList.add("hidden");
    startBtn.disabled = true;
    startBtn.textContent = "スペースではじめる";
    startBtn.classList.add("p-typingscreen__start__important");
    optionContainer.classList.add("hidden");
    spaceStartFlag = true;
    mae.classList.add("hidden");
    tugi.classList.add("hidden");
    soundBtnChecked = soundBtn.checked;
    if (soundBtnChecked) {
        sound = "yes";
    } else {
        sound = "no";
    }
}
function startTypingFunc() {
    getCurrentWordData();
    startBtn.classList.add("hidden");
    countDown.classList.remove("hidden");
    sortByKeyword();
    if (
        typeWords[keywordOrder[0]].pronunciation_file_name != null &&
        sound != "no"
    ) {
        firstAudio = new Audio(
            routeLink +
                "storage/audio/" +
                typeWords[keywordOrder[0]].pronunciation_file_name
        );
        firstAudio.load();
    }
    for (let i = 0; i < typeWords.length; i++) {
        WordId = typeWords[keywordOrder[i]].id;
        WordIdImg = document.createElement("img");
        WordIdImg.src = "/img/map/" + WordId + ".png";
        WordIdImg.height = 80;
        WordIdImg.id = WordId;
        WordIdImg.classList.add("hidden");
        MapContainer.appendChild(WordIdImg);
    }
    for (let i = 0; i <= countSeconds; i++) {
        setTimeout(() => {
            countDown.textContent = countSeconds - i;
        }, i * 1000);
    }
    setTimeout(() => {
        if (restartFlg == false) {
            document
                .getElementById("maptop" + currentCourseId)
                .classList.add("hidden");
            countDown.classList.add("hidden");
            restartBtn.classList.remove("hidden");
            beforeTypingStartArea.classList.add("hidden");
            typingWordAreaBgBlack.classList.remove("hidden");
            initTypingFunc();
        }
    }, countSeconds * 1000);
}
function initTypingFunc() {
    if (challengeBtnChecked.checked == true) {
        challengeBtnCheckedFlg = true;
        document.getElementById("p-mana").classList.add("hidden");
        document.getElementById("p-kana").classList.add("hidden");
        firstAudio = null;
    } else {
        document.getElementById("p-mana").classList.remove("hidden");
        document.getElementById("p-kana").classList.remove("hidden");
    }
    keydownAllCnt = 0;
    successType = 0;
    missType = 0;
    successtypeField.textContent = "入力: " + successType;
    misstypeField.textContent = "ミス: " + missType;

    nextKeywordFunc();
    typingFlag = true;
    beginDate = new Date();
}

function nextKeywordFunc() {
    romanMap = romanMapLower;
    typingShowField.innerHTML = "";
    typedField.textContent = "";
    untypedField.textContent = "";
    untypedCnt = 0;
    let wordId = typeWords[keywordOrder[0]].id;
    if (preWordId != null) {
        document.getElementById(preWordId).remove();
    }
    document.getElementById(wordId).classList.remove("hidden");
    preWordId = wordId;
    if (
        typeWords[keywordOrder[0]].word_kana === "" ||
        typeWords[keywordOrder[0]].word_kana === null
    ) {
        kanaField.textContent = typeWords[keywordOrder[0]].word_mana;
        manaField.textContent = typeWords[keywordOrder[0]].word_mana;
        keywordArr = [...typeWords[keywordOrder[0]].word_mana];
        untypedTwoArr = [];
        typedTwoArr = [];
        for (let i = 0; i < keywordArr.length; i++) {
            untypedTwoArr.push([keywordArr[i]]);
            typedTwoArr.push([""]);
        }
    } else {
        kanaField.textContent = typeWords[keywordOrder[0]].word_kana;
        manaField.textContent = typeWords[keywordOrder[0]].word_mana;
        keywordArr = [...typeWords[keywordOrder[0]].word_kana];
        for (let i = 0; i < keywordArr.length; i++) {
            if (["ん"].indexOf(keywordArr[i]) > -1) {
                if (
                    [
                        "あ",
                        "い",
                        "う",
                        "え",
                        "お",
                        "な",
                        "に",
                        "ぬ",
                        "ね",
                        "の",
                        "っ",
                        undefined,
                    ].indexOf(keywordArr[i + 1]) < 0
                ) {
                    keywordArr[i] = "n";
                }
            }
        }
        for (let i = 0; i < keywordArr.length; i++) {
            if (["っ"].indexOf(keywordArr[i]) > -1) {
                let lower = keywordArr.slice(i, i + 1);
                let upper = keywordArr.slice(i + 1, i + 2);
                keywordArr.splice(i, 1);
                keywordArr.splice(i, 1, lower + upper);
            }
            if (
                ["ぁ", "ぃ", "ぅ", "ぇ", "ぉ", "ゃ", "ゅ", "ょ", "ゎ"].indexOf(
                    keywordArr[i]
                ) > -1
            ) {
                let lower = keywordArr.slice(i, i + 1);
                let upper = keywordArr.slice(i - 1, i);
                keywordArr.splice(i, 1);
                keywordArr.splice(i - 1, 1, upper + lower);
                i -= 1;
            }
        }
        let roman = [];
        untypedTwoArr = [];
        keywordArr.forEach((val, index) => {
            roman = [];
            if (val.length === 3) {
                if (romanMap[val] != undefined) {
                    roman = [...romanMap[val]];
                    const romanCopy = [...romanMap[val]];
                    if (keywordArr[index - 1] == "n") {
                        for (val1 of romanCopy) {
                            roman.push("n" + val1);
                        }
                    }
                }
                const idx1 = val.slice(0, 1);
                const idx2 = val.slice(1, 2);
                const idx3 = val.slice(2, 3);
                const roman1 = [...romanMap[idx1]];
                const roman2 = [...romanMap[idx2]];
                const roman3 = [...romanMap[idx3]];
                const idx23 = val.slice(1, 3);
                const roman23 = [...romanMap[idx23]];
                for (val1 of roman1) {
                    for (val2 of roman23) {
                        roman.push(val1 + val2);
                        if (keywordArr[index - 1] == "n") {
                            roman.push("n" + val1 + val2);
                        }
                    }
                }
                for (val1 of roman1) {
                    for (val2 of roman2) {
                        for (val3 of roman3) {
                            roman.push(val1 + val2 + val3);
                            if (keywordArr[index - 1] == "n") {
                                roman.push("n" + val1 + val2 + val3);
                            }
                        }
                    }
                }
                untypedTwoArr.push(roman);
            } else if (val.length === 2) {
                if (romanMap[val] != undefined) {
                    roman = [...romanMap[val]];
                    const romanCopy = [...romanMap[val]];
                    if (keywordArr[index - 1] == "n") {
                        for (val1 of romanCopy) {
                            roman.push("n" + val1);
                        }
                    }
                }
                const idx1 = val.slice(0, 1);
                const idx2 = val.slice(1, 2);
                const roman1 = [...romanMap[idx1]];
                const roman2 = [...romanMap[idx2]];
                for (val1 of roman1) {
                    for (val2 of roman2) {
                        roman.push(val1 + val2);
                        if (keywordArr[index - 1] == "n") {
                            roman.push("n" + val1 + val2);
                        }
                    }
                }
                untypedTwoArr.push(roman);
            } else {
                if (romanMap[val] != undefined) {
                    roman = [...romanMap[val]];
                    const romanCopy = [...romanMap[val]];
                    if (keywordArr[index - 1] == "n") {
                        for (val1 of romanCopy) {
                            roman.push("n" + val1);
                        }
                    }
                } else {
                    roman = [...val];
                }
                untypedTwoArr.push(roman);
            }
        });
        typedTwoArr = JSON.parse(JSON.stringify(untypedTwoArr));
        typedTwoArr.forEach((val1, idx1) => {
            typedTwoArr[idx1].forEach((val2, idx2) => {
                typedTwoArr[idx1][idx2] = "";
            });
        });
    }
    for (val of typedTwoArr) {
        typingShowField.innerHTML +=
            '<span class="typed">' + val[0] + "</span>";
        typedField.textContent += val[0];
    }
    for (val of untypedTwoArr) {
        if (alphabet != "no" && challengeBtnChecked.checked == false) {
            typingShowField.innerHTML +=
                '<span class="untyped">' + val[0] + "</span>";
        }
        untypedField.textContent += val[0];
    }
    if (oneKeyword !== "") {
    }
    oneKeyword = untypedTwoArr[0][0].substring(0, 1);
    wordSound();
}
mae.addEventListener("click", () => {
    document
        .getElementById("maptop" + currentCourseId)
        .classList.remove("hidden");
    document
        .getElementById("maptop" + courseList[currentCourseIdKey + 1]["id"])
        .classList.add("hidden");
});
tugi.addEventListener("click", () => {
    document
        .getElementById("maptop" + currentCourseId)
        .classList.remove("hidden");
    document
        .getElementById("maptop" + courseList[currentCourseIdKey - 1]["id"])
        .classList.add("hidden");
});
function restartBtnClick() {
    endFlag = false;
    sendFlag = true;
    spaceStartFlag = false;
    typingFlag = false;
    restartFlg = true;
    firstAudio = null;
    preWordId = null;
    challengeBtnCheckedFlg = false;
    if (!maetugiBtnFlg) {
        mae.classList.add("hidden");
        tugi.classList.add("hidden");
        startBtn.classList.add("hidden");
    } else {
        mae.classList.remove("hidden");
        tugi.classList.remove("hidden");
        startBtn.classList.remove("hidden");
    }
    startBtn.disabled = false;
    startBtn.textContent = "はじめる";
    startBtn.classList.remove("p-typingscreen__start__important");
    optionContainer.classList.remove("hidden");
    beforeTypingStartArea.classList.remove("hidden");
    typingWordAreaBgBlack.classList.add("hidden");
    countDown.classList.add("hidden");
    typingResultModal.classList.add("hidden");
    while (MapContainer.firstChild) {
        MapContainer.removeChild(MapContainer.firstChild);
    }
    if (isFree) {
        const imgMaptopAll = MapTopContainer.querySelectorAll(".maptop");
        imgMaptopAll.forEach((imgMaptop) => {
            imgMaptop.classList.add("hidden");
        });
    }
    document
        .getElementById("maptop" + currentCourseId)
        .classList.remove("hidden");
}
window.addEventListener(
    "keydown",
    (e) => {
        if (e.repeat) {
            return false;
        }
        argKeyCode = e.code;
        if (typingFlag && e.key === "Tab") {
            e.preventDefault();
            return false;
        }
        if (argKeyCode === "Space") {
            e.preventDefault();
        }
        if (keyValue.includes(e.key) || argKeyCode === "Space") {
            if (spaceStartFlag && !typingFlag) {
                if (e.code == "Space") {
                    spaceStartFlag = false;
                    startTypingFunc();
                }
            }
            if (typingFlag) {
                keydownAllCnt += 1;
                let arrSub = untypedTwoArr[untypedCnt].filter(
                    (element) =>
                        element.substring(0, 1).toLowerCase() ==
                        e.key.toLowerCase()
                );
                if (arrSub.length > 0) {
                    untypedTwoArr[untypedCnt] = arrSub;
                }
                let passIndex = untypedTwoArr[untypedCnt].findIndex(
                    (element) =>
                        element.substring(0, 1).toLowerCase() ==
                        e.key.toLowerCase()
                );
                untypedTwoArr[untypedCnt].forEach((val, index) => {
                    if (
                        e.key.toLowerCase() ===
                        untypedTwoArr[untypedCnt][index]
                            .substring(0, 1)
                            .toLowerCase()
                    ) {
                        typedTwoArr[untypedCnt][index] += untypedTwoArr[
                            untypedCnt
                        ][index].substring(0, 1);
                        untypedTwoArr[untypedCnt][index] =
                            untypedTwoArr[untypedCnt][index].slice(1);
                    }
                });
                if (passIndex > -1) {
                    typingShowField.innerHTML = "";
                    typedField.textContent = "";
                    untypedField.textContent = "";
                    for (val of typedTwoArr) {
                        typingShowField.innerHTML +=
                            '<span class="typed">' + val[passIndex] + "</span>";
                        typedField.textContent += val[passIndex];
                    }
                    for (val of untypedTwoArr) {
                        if (
                            alphabet != "no" &&
                            challengeBtnChecked.checked == false
                        ) {
                            typingShowField.innerHTML +=
                                '<span class="untyped">' +
                                val[passIndex] +
                                "</span>";
                        }
                        untypedField.textContent += val[passIndex];
                    }
                    if (untypedTwoArr[untypedCnt][passIndex].length == 0) {
                        untypedCnt += 1;
                    }
                    if (untypedField.textContent !== "") {
                        oneKeyword = untypedField.textContent.substring(0, 1);
                    }
                    successType += 1;
                    successtypeField.textContent = "入力: " + successType;
                    untypedField.classList.remove(
                        "p-typingscreen__romaji__untyped__miss"
                    );
                } else {
                    missType += 1;
                    misstypeField.textContent = "ミス: " + missType;
                    untypedField.classList.add(
                        "p-typingscreen__romaji__untyped__miss"
                    );
                    if (sound != "no" && challengeBtnChecked.checked == false) {
                        if (missType % 100 == 0) {
                            alert(
                                "連続で100回以上間違えると音がでなくなります。コース一覧ページにもどってください。"
                            );
                            return false;
                        }
                        missSound();
                    }
                }

                if (untypedField.textContent === "") {
                    if (keywordOrder.length !== 0) {
                        nextKeywordFunc();
                    } else {
                        finishType();
                    }
                }
            }
            if (endFlag) {
                if (e.code == "Space") {
                    restartBtnClick();
                }
            }
        }
    },
    { passive: false }
);

function finishType() {
    typingFlag = false;
    endFlag = true;
    endDate = new Date();
    typingscore();
    document.getElementById("p-typingresult--score").textContent =
        resultypingScore;
    document.getElementById("p-typingresult--time").textContent = typingTime;
    document.getElementById("p-typingresult--all").textContent = keydownAllCnt;
    document.getElementById("p-typingresult--miss").textContent = missType;
    typingResultModal.classList.remove("hidden");
    if (!isFree) {
        getResutAjax();
    }
}
