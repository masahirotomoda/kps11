/****************************************
* variable
*****************************************/
let checkall = document.getElementById("checkAll");
let checks = document.getElementsByName("checks[]");
let del = document.getElementById("del");

/****************************************
* delete check
*****************************************/
document.addEventListener("DOMContentLoaded", function(){
    checkall.addEventListener('click', () => {
    for (val of checks) {
        checkall.checked == true ? val.checked = true : val.checked = false;
    }
    if(del != null){
        if(checkall.checked == true ){
            del.disabled = false;
        }else{
            del.disabled = true;
        }
    }

    });
    checks.forEach(element => {
    element.addEventListener('click', () => {
        if (element.checked == false) {
        checkall.checked = false;
        }
        if (document.querySelectorAll(".checks:checked").length == checks.length) {
        checkall.checked = true;
        }
    });
    });
}, false);
