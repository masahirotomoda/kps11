/****************************************
 * sound and alphabet
 *****************************************/
document.getElementsByName("sound")[0].addEventListener("change", () => {
    if (document.getElementsByName("sound")[0].checked == true) {
        document.cookie = "ntyping_option_sound=yes";
    }
});
document.getElementsByName("sound")[1].addEventListener("change", () => {
    if (document.getElementsByName("sound")[1].checked == true) {
        document.cookie = "ntyping_option_sound=no";
    }
});
//document.getElementsByName("alpha")[0].addEventListener("change", () => {
//    if (document.getElementsByName("alpha")[0].checked == true) {
//        document.cookie = "ntyping_option_alphabet=yes";
//    }
//});
//document.getElementsByName("alpha")[1].addEventListener("change", () => {
//    if (document.getElementsByName("alpha")[1].checked == true) {
//        document.cookie = "ntyping_option_alphabet=no";
//    }
//});
/****************************************
 * lucky button
 *****************************************/
const luckybtn_entry = document.getElementById("luckybtn_entry");
const luckybtn_middle = document.getElementById("luckybtn_middle");
const luckybtn_advance = document.getElementById("luckybtn_advance");
const typing_form = document.getElementById("typing_form");

let randomNum;
let randomLuckyNum;

function luckyBtnEntry() {
    randomLuckyNum = Math.floor(Math.random() * nyumon.length);
    luckybtn_entry.name = nyumon[randomLuckyNum];
}
function luckyBtnMiddle() {
    randomLuckyNum = Math.floor(Math.random() * cyukyu.length);
    luckybtn_middle.name = cyukyu[randomLuckyNum];
}
function luckyBtnAdvance() {
    randomLuckyNum = Math.floor(Math.random() * jokyu.length);
    luckybtn_advance.name = jokyu[randomLuckyNum];
}

function courseListMap() {
    typing_form.action = "/free/course-map";
}
function courseListKeytouch() {
    typing_form.action = "/free/course-keytouch";
}
function courseListTest() {
    typing_form.action = "/free/course-test";
}
function courseListEnglish() {
    typing_form.action = "/free/course-english";
}
/****************************************
 * lucky button animation
 *****************************************/
const luckyBTN_Entry = document.getElementById("luckybtn_entry");
const luckyBTN_Middle = document.getElementById("luckybtn_middle");
const luckyBTN_Advance = document.getElementById("luckybtn_advance");

luckyBTN_Entry.addEventListener("click", function () {
    this.classList.add("active");
});
luckyBTN_Middle.addEventListener("click", function () {
    this.classList.add("active");
});
luckyBTN_Advance.addEventListener("click", function () {
    this.classList.add("active");
});
/****************************************
 * battle mode
 *****************************************/
document
    .getElementById("battle_Btn")
    .addEventListener("click", function (event) {
        if (!userNickname) {
            event.preventDefault();
            alert(
                "■画面上にある「ログアウト」ボタンの左にある「マイメニュー」でニックネームを先に登録してください。※スコアとニックネームがランキングに公開されます。"
            );
        }
    });
