/****************************************
* password reset
*****************************************/
let btn = document.getElementById("modal_reset_btn");
btn.addEventListener('click', () => {
    setTimeout(function () {
        btn.disabled = true;
        btn.textContent = 'パスワードリセット中...';
    }, 100);
});

