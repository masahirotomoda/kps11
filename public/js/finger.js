/**
 * @constract
 * @type {Object} fingerMap 指(色変更)マップ
 */
 const fingerMap = {
  // ===== 1段目
  '1': ['p-fingers__finger--llittle'], // 左小指
  '2': ['p-fingers__finger--lthird'],  // 左薬指
  '3': ['p-fingers__finger--lsecond'], // 左中指
  '4': ['p-fingers__finger--lfirst'],  // 左人差し指
  '5': ['p-fingers__finger--lfirst'],  // 左人差し指
  '6': ['p-fingers__finger--rfirst'],  // 右人差し指
  '7': ['p-fingers__finger--rfirst'],  // 右人差し指
  '8': ['p-fingers__finger--rsecond'], // 右中指
  '9': ['p-fingers__finger--rthird'],  // 右薬指
  '0': ['p-fingers__finger--rlittle'], // 右小指
  '-': ['p-fingers__finger--rlittle'], // 右小指
  '^': ['p-fingers__finger--rlittle'], // 右小指
  '¥': ['p-fingers__finger--rlittle'], // 右小指
  // ===== 2段目
  'q': ['p-fingers__finger--llittle'], // 左小指
  'w': ['p-fingers__finger--lthird'],  // 左薬指
  'e': ['p-fingers__finger--lsecond'], // 左中指
  'r': ['p-fingers__finger--lfirst'],  // 左人差し指
  't': ['p-fingers__finger--lfirst'],  // 左人差し指
  'y': ['p-fingers__finger--rfirst'],  // 右人差し指
  'u': ['p-fingers__finger--rfirst'],  // 右人差し指
  'i': ['p-fingers__finger--rsecond'], // 右中指
  'o': ['p-fingers__finger--rthird'],  // 右薬指
  'p': ['p-fingers__finger--rlittle'], // 右小指
  '@': ['p-fingers__finger--rlittle'], // 右小指
  '[': ['p-fingers__finger--rlittle'], // 右小指
  // ===== 3段目
  'a': ['p-fingers__finger--llittle'], // 左小指
  's': ['p-fingers__finger--lthird'],  // 左薬指
  'd': ['p-fingers__finger--lsecond'], // 左中指
  'f': ['p-fingers__finger--lfirst'],  // 左人差し指
  'g': ['p-fingers__finger--lfirst'],  // 左人差し指
  'h': ['p-fingers__finger--rfirst'],  // 右人差し指
  'j': ['p-fingers__finger--rfirst'],  // 右人差し指
  'k': ['p-fingers__finger--rsecond'], // 右中指
  'l': ['p-fingers__finger--rthird'],  // 右薬指
  ';': ['p-fingers__finger--rlittle'], // 右小指
  ':': ['p-fingers__finger--rlittle'], // 右小指
  ']': ['p-fingers__finger--rlittle'], // 右小指
  // ===== 4段目
  'z': ['p-fingers__finger--llittle'], // 左小指
  'x': ['p-fingers__finger--lthird'],  // 左薬指
  'c': ['p-fingers__finger--lsecond'], // 左中指
  'v': ['p-fingers__finger--lfirst'],  // 左人差し指
  'b': ['p-fingers__finger--lfirst'],  // 左人差し指
  'n': ['p-fingers__finger--rfirst'],  // 右人差し指
  'm': ['p-fingers__finger--rfirst'],  // 右人差し指
  ',': ['p-fingers__finger--rsecond'], // 右中指
  '.': ['p-fingers__finger--rthird'],  // 右薬指
  '/': ['p-fingers__finger--rlittle'], // 右小指
  '_': ['p-fingers__finger--rlittle'], // 右小指
  // ===== 5段目
  ' ': ['p-fingers__finger--lthumb', 'p-fingers__finger--rthumb'], // 右小指
  // ========== Shiftキー押しながら
  // ===== 1段目
  '!': ['p-fingers__finger--llittle'], // 左小指
  '"': ['p-fingers__finger--lthird'],  // 左薬指
  '#': ['p-fingers__finger--lsecond'], // 左中指
  '$': ['p-fingers__finger--lfirst'],  // 左人差し指
  '%': ['p-fingers__finger--lfirst'],  // 左人差し指
  '&': ['p-fingers__finger--rfirst'],  // 右人差し指
  "'": ['p-fingers__finger--rfirst'],  // 右人差し指
  '(': ['p-fingers__finger--rsecond'], // 右中指
  ')': ['p-fingers__finger--rthird'],  // 右薬指
  '=': ['p-fingers__finger--rlittle'], // 右小指
  '~': ['p-fingers__finger--rlittle'], // 右小指
  '|': ['p-fingers__finger--rlittle'], // 右小指
  // ===== 2段目
  'Q': ['p-fingers__finger--llittle'], // 左小指
  'W': ['p-fingers__finger--lthird'],  // 左薬指
  'E': ['p-fingers__finger--lsecond'], // 左中指
  'R': ['p-fingers__finger--lfirst'],  // 左人差し指
  'T': ['p-fingers__finger--lfirst'],  // 左人差し指
  'Y': ['p-fingers__finger--rfirst'],  // 右人差し指
  'U': ['p-fingers__finger--rfirst'],  // 右人差し指
  'I': ['p-fingers__finger--rsecond'], // 右中指
  'O': ['p-fingers__finger--rthird'],  // 右薬指
  'P': ['p-fingers__finger--rlittle'], // 右小指
  '`': ['p-fingers__finger--rlittle'], // 右小指
  '{': ['p-fingers__finger--rlittle'], // 右小指
  // ===== 3段目
  'A': ['p-fingers__finger--llittle'], // 左小指
  'S': ['p-fingers__finger--lthird'],  // 左薬指
  'D': ['p-fingers__finger--lsecond'], // 左中指
  'F': ['p-fingers__finger--lfirst'],  // 左人差し指
  'G': ['p-fingers__finger--lfirst'],  // 左人差し指
  'H': ['p-fingers__finger--rfirst'],  // 右人差し指
  'J': ['p-fingers__finger--rfirst'],  // 右人差し指
  'K': ['p-fingers__finger--rsecond'], // 右中指
  'L': ['p-fingers__finger--rthird'],  // 右薬指
  '+': ['p-fingers__finger--rlittle'], // 右小指
  '*': ['p-fingers__finger--rlittle'], // 右小指
  '}': ['p-fingers__finger--rlittle'], // 右小指
  // ===== 4段目
  'Z': ['p-fingers__finger--llittle'], // 左小指
  'X': ['p-fingers__finger--lthird'],  // 左薬指
  'C': ['p-fingers__finger--lsecond'], // 左中指
  'V': ['p-fingers__finger--lfirst'],  // 左人差し指
  'B': ['p-fingers__finger--lfirst'],  // 左人差し指
  'N': ['p-fingers__finger--rfirst'],  // 右人差し指
  'M': ['p-fingers__finger--rfirst'],  // 右人差し指
  '<': ['p-fingers__finger--rsecond'], // 右中指
  '>': ['p-fingers__finger--rthird'],  // 右薬指
  '?': ['p-fingers__finger--rlittle'], // 右小指
  // ===== 5段目
}