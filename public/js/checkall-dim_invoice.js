/****************************************
 * variable
 *****************************************/
let checkall1 = document.getElementById("checkAll1");
let checkall2 = document.getElementById("checkAll2");
let checks1 = document.getElementsByName("checks1[]");
let checks2 = document.getElementsByName("checks2[]");

/****************************************
 * delete check
 *****************************************/
document.addEventListener(
    "DOMContentLoaded",
    function () {
        checkall1.addEventListener("click", () => {
            for (val of checks1) {
                checkall1.checked == true
                    ? (val.checked = true)
                    : (val.checked = false);
            }
        });
        checks1.forEach((element) => {
            element.addEventListener("click", () => {
                if (element.checked == false) {
                    checkall1.checked = false;
                }
                if (
                    document.querySelectorAll(".checks:checked").length ==
                    checks1.length
                ) {
                    checkall1.checked = true;
                }
            });
        });
        checkall2.addEventListener("click", () => {
            for (val of checks2) {
                checkall2.checked == true
                    ? (val.checked = true)
                    : (val.checked = false);
            }
        });
        checks2.forEach((element) => {
            element.addEventListener("click", () => {
                if (element.checked == false) {
                    checkall1.checked = false;
                }
                if (
                    document.querySelectorAll(".checks:checked").length ==
                    checks2.length
                ) {
                    checkall2.checked = true;
                }
            });
        });
    },
    false
);
