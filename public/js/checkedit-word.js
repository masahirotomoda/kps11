/****************************************
* 変数定義
*****************************************/
let edit = document.getElementsByName("edit");
let display_order = document.getElementsByName("display_order");
let word_kana = document.getElementsByName("word_kana");
let word_mana = document.getElementsByName("word_mana");
let s_btn = document.getElementsByName("save");
let edit_cancel = document.getElementsByName("editcancel");
let del_checks = document.getElementsByName("checks[]");

/****************************************
* 編集ボタン押下、キャンセルボタン押下、保存ボタン押下
*****************************************/
document.addEventListener("DOMContentLoaded", function(){
    let len_edit = edit.length;
    for (let c_edit = 0; c_edit < len_edit; c_edit++) {
        edit[c_edit].addEventListener('click', () => {
            for(let j = 0; j < len_edit; j++){
                display_order[j].readOnly = true;
                del_checks[j].disabled = false;
                word_kana[j].readOnly = true;
                word_mana[j].readOnly = true;
                s_btn[j].disabled = true;
                edit_cancel[j].disabled = true;
                edit[j].disabled = false;
            }
            display_order[c_edit].readOnly = false;
            word_kana[c_edit].readOnly = false;
            word_mana[c_edit].readOnly = false;
            s_btn[c_edit].disabled = false;
            edit[c_edit].disabled = true;
            edit_cancel[c_edit].disabled = false;
            del_checks[c_edit].checked = false;
            del_checks[c_edit].disabled = true;
            editbefore_display_order =display_order[c_edit].value;
            editbefore_word_mana =word_mana[c_edit].value;
            editbefore_word_kana =word_kana[c_edit].value;

        });
        edit_cancel[c_edit].addEventListener('click', () => {
            display_order[c_edit].value = editbefore_display_order;
            word_mana[c_edit].value = editbefore_word_mana;
            word_kana[c_edit].value = editbefore_word_kana;

            s_btn[c_edit].disabled = true;
            edit_cancel[c_edit].disabled = true;
            edit[c_edit].disabled = false;
            del_checks[c_edit].disabled = false;
        
            display_order[c_edit].readOnly = true;
            word_kana[c_edit].readOnly = true;
            word_mana[c_edit].readOnly = true;
        });
        s_btn[c_edit].addEventListener('click', () => {
            edit_cancel[c_edit].disabled = true;
            s_btn[c_edit].value = '保存中...';   
            setTimeout(function () {
                s_btn[c_edit].disabled = true; 
            }, 100);
        });
    }
}, false);
