/****************************************
* 名前変更ボタンボタン押下
*****************************************/
let update_btn = document.getElementById("update_btn");
update_btn.addEventListener('click', () => {
        setTimeout(function () {
            update_btn.disabled = true;
            update_btn.textContent = '名前を更新中...';
        }, 100);
    });

