/****************************************
* variable
*****************************************/
let edit = document.getElementsByName("edit");
let s_kumi = document.getElementsByName("kumi_id");
let s_grade = document.getElementsByName("grade_id");
let s_user = document.getElementsByName("user_name");
let s_attendance = document.getElementsByName("attendance_no");
let s_btn = document.getElementsByName("save");
let edit_cancel = document.getElementsByName("editcancel");

/****************************************
* edit_button  cancel_button save_button
*****************************************/
document.addEventListener("DOMContentLoaded", function(){
    let len_edit = edit.length;
    for (let c_edit = 0; c_edit < len_edit; c_edit++) {
        edit[c_edit].addEventListener('click', () => {
            for(let j = 0; j < len_edit; j++){
                s_kumi[j].disabled = true;
                s_grade[j].disabled = true;
                s_attendance[j].readOnly =true;
                s_user[j].readOnly = true;
                s_btn[j].disabled = true;
                edit_cancel[j].disabled = true;
                edit[j].disabled = false;
            }
            s_kumi[c_edit].disabled = false;
            s_grade[c_edit].disabled = false;
            s_attendance[c_edit].readOnly = false;
            s_user[c_edit].readOnly = false;
            s_btn[c_edit].disabled = false;
            edit_cancel[c_edit].disabled = false;
            edit[c_edit].disabled = true;

            editbefore_s_user =s_user[c_edit].value;
            editbefore_s_attendance =s_attendance[c_edit].value;
            editbefore_s_grade =s_grade[c_edit].value;
            editbefore_s_kumi =s_kumi[c_edit].value;
        });
        edit_cancel[c_edit].addEventListener('click', () => {
            s_user[c_edit].value = editbefore_s_user;
            s_attendance[c_edit].value = editbefore_s_attendance;
            s_grade[c_edit].value = editbefore_s_grade;
            s_kumi[c_edit].value = editbefore_s_kumi;

            s_btn[c_edit].disabled = true;
            edit_cancel[c_edit].disabled = true;
            edit[c_edit].disabled = false;

            s_kumi[c_edit].disabled = true;
            s_grade[c_edit].disabled = true;
            s_attendance[c_edit].readOnly = true;
            s_user[c_edit].readOnly = true;
 
        });
        s_btn[c_edit].addEventListener('click', () => {
            edit_cancel[c_edit].disabled = true;
            s_btn[c_edit].value = '保存中...';
        
            setTimeout(function () {
                s_btn[c_edit].disabled = true; 
            }, 100);
        
        });
    }
}, false);
