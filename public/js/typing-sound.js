function wordSound(){
  if (firstAudio != null && keywordOrder.length === typeWords.length) {
    firstAudio.play();    
  } else if (typeWords[keywordOrder[0]].pronunciation_file_name != null && sound != 'no' && challengeBtnCheckedFlg==false) {
  const audioContext = new AudioContext();
  const audioSource = audioContext.createBufferSource();
  const audioRequest = new XMLHttpRequest();
  audioRequest.open('GET', routeLink+'storage/audio/' +  typeWords[keywordOrder[0]].pronunciation_file_name, true);
  audioRequest.responseType = 'arraybuffer';
  audioRequest.send();
  audioRequest.onload = function () {
    var res = audioRequest.response;
    audioContext.decodeAudioData(res, function (buf) {
      audioSource.buffer = buf;
    });
  audioSource.connect(audioContext.destination);
  audioSource.start(0);
  }
}
  keywordOrder.shift();
}
function missSound(){
  const context  = new AudioContext();  
          const oscillator = context.createOscillator();
          oscillator.type = "sine"; 
          oscillator.frequency.value = 880;
          const gainNode = context.createGain();
          gainNode.gain.value = 0.1; 
          oscillator.connect(gainNode).connect(context.destination);
          const startTime = context.currentTime;
          oscillator.start(startTime);
          oscillator.stop(startTime+0.03);
}
