/****************************************
 * file
 *****************************************/
document.addEventListener("DOMContentLoaded", function () {
    const formInputs = document.getElementsByClassName("inputFile");
    const length = formInputs.length;
    for (let i = 0; i < length; i++) {
        formInputs[i].onchange = function () {
            const file = this.files[0].name;
            const label = this.nextElementSibling;
            if (!label.classList.contains("changed")) {
                const span = document.createElement("span");
                span.className = "filename";
                this.parentNode.appendChild(span);
                label.classList.add("changed");
            }
            label.nextElementSibling.innerHTML = file;
        };
    }
});
/****************************************
 * smartphone menu
 *****************************************/
$(function () {
    $(".navBtn").click(function () {
        $("#naviOut").toggleClass("navOpen");
    });
    $(".freePageHeadBtn li a").click(function () {
        $("#naviOut").toggleClass("navOpen");
    });
    $(window).on("load resize", function () {
        var breakpoint = 680;
        if (window.innerWidth > breakpoint) {
            $("#naviOut").removeClass("navOpen");
        }
    });
});
/****************************************
 * accordion menu
 *****************************************/
$(function () {
    $(".questionBtn").click(function () {
        $(this).next(".answerBox").fadeToggle("fast");
        $(this).next(".answerBox").toggleClass("is-open");
        $(".questionBtn").not($(this)).next(".answerBox").fadeOut("fast");
        $(".questionBtn")
            .not($(this))
            .next(".answerBox")
            .removeClass("is-open");
    });
});

/****************************************
 * page top
 *****************************************/
$(function () {
    var topBtn = $(".pagetop");
    topBtn.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
});
$(function () {
    $('a[href^="#top"]').click(function () {
        var adjust = 0;
        var speed = 400;
        var href = $(this).attr("href");
        var target = $(href == "#top" || href == "" ? "html" : href);
        var position = target.offset().top + adjust;
        $("body,html").animate(
            {
                scrollTop: position,
            },
            speed,
            "swing"
        );
        return false;
    });
});
