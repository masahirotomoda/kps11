window.addEventListener('DOMContentLoaded', function() {
  let btn_passview = document.getElementById("btn_passview");
  let input_pass = document.getElementById("password");

  btn_passview.addEventListener("click", (e) => {
      e.preventDefault();
      if (input_pass.type === 'password') {
          input_pass.type = 'text';
          btn_passview.textContent = '非表示';
btn_passview.classList.add('passviewbtn');
      } else {
          input_pass.type = 'password';
          btn_passview.textContent = '表示';
btn_passview.classList.remove('passviewbtn');
      }
  });
});