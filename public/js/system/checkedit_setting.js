/****************************************
* 変数定義
*****************************************/
let edit = document.getElementById("edit");
let update = document.getElementById("update");

let PAGENATION = document.getElementById("PAGENATION");
let SESSION_TIME = document.getElementById("SESSION_TIME");
let COPYRIGHT = document.getElementById("COPYRIGHT");
let TRIAL_PERIOD_SCHOOL = document.getElementById("TRIAL_PERIOD_SCHOOL");
let TRIAL_PERIOD_CRAM = document.getElementById("TRIAL_PERIOD_CRAM");
let META_TITLE = document.getElementById("META_TITLE");
let TRIAL_SCHOOL_NO = document.getElementById("TRIAL_SCHOOL_NO");
let DOMAIN_NAME = document.getElementById("DOMAIN_NAME");
let ADMIN_ADDRESS = document.getElementById("ADMIN_ADDRESS");
let ADMIN_ZIP = document.getElementById("ADMIN_ZIP");
let ADMIN_TEL = document.getElementById("ADMIN_TEL");
let ADMIN_MAIL = document.getElementById("ADMIN_MAIL");
let ADMIN_IPADDRESS = document.getElementById("ADMIN_IPADDRESS");
let ADMIN_SCHOOL = document.getElementById("ADMIN_SCHOOL");

/****************************************
* 編集ボタン押下時、 キャンセル押下時、保存ボタン押下時
*****************************************/
document.addEventListener("DOMContentLoaded", function(){
    edit.disabled = false;
    update.disabled = true;

        edit.addEventListener('click', () => {
            console.log('editmode');
            edit.disabled = true;
            update.disabled = false;

            PAGENATION.disabled = false;
            SESSION_TIME.disabled = false;
            COPYRIGHT.disabled = false;
            TRIAL_PERIOD_SCHOOL.disabled = false;
            TRIAL_PERIOD_CRAM.disabled = false;
            META_TITLE.disabled = false;
            TRIAL_SCHOOL_NO.disabled = false;
            DOMAIN_NAME.disabled = false;
            ADMIN_ADDRESS.disabled = false;
            ADMIN_ZIP.disabled = false;
            ADMIN_TEL.disabled = false;
            ADMIN_MAIL.disabled = false;
            ADMIN_IPADDRESS.disabled = false;
            ADMIN_SCHOOL.disabled = false;

        });
        update.addEventListener('click', () => {
            console.log('updatemode');
            setTimeout(function () {
                update.disabled = true;
                edit.disabled = false;
            }, 100);


        });


}, false);
