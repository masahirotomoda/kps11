/****************************************
* 変数定義
*****************************************/
let edit = document.getElementsByName("edit");
let s_cancel = document.getElementsByName("editcancel");
let s_kumi = document.getElementsByName("kumi_id");
let s_grade = document.getElementsByName("grade_id");
let s_btn = document.getElementsByName("save");
let checks1 = document.getElementsByName("checks[]");

/****************************************
*  編集ボタン押下時、入力カラム活性化(削除チェックは非活性)
*****************************************/
document.addEventListener("DOMContentLoaded", function(){
    let len_edit = edit.length;
    for (let c_edit = 0; c_edit < len_edit; c_edit++) {
        edit[c_edit].addEventListener('click', () => {
            for(let j = 0; j < len_edit; j++){
                s_kumi[j].disabled = true;
                s_grade[j].disabled = true;
                s_btn[j].disabled = true;
                s_cancel[j].disabled = true;
                checks1[j].disabled = false;
            }
            s_kumi[c_edit].disabled = false;
            s_grade[c_edit].disabled = false;
            s_btn[c_edit].disabled = false;
            s_cancel[c_edit].disabled = false;
            checks1[c_edit].checked = false;
            checks1[c_edit].disabled = true;

            editbefore_s_grade =s_grade[c_edit].value;
            editbefore_s_kumi =s_kumi[c_edit].value;
        });
        s_cancel[c_edit].addEventListener('click', () => {
            s_grade[c_edit].value = editbefore_s_grade;
            s_kumi[c_edit].value = editbefore_s_kumi;

            edit[c_edit].disabled = false;
            s_btn[c_edit].disabled = true;
            s_cancel[c_edit].disabled = true;
            s_grade[c_edit].disabled = true;
            s_kumi[c_edit].disabled = true;
        });
    }
}, false);

/****************************************
* 学校選択ボタン押下、クラス作成ボタンが活性化する
*****************************************/
document.addEventListener('DOMContentLoaded', (event) => {

    let selected = document.getElementById("selected");
    let mode = document.getElementById('SearchOrEditMode');
    let add = document.getElementById("add");
    let selected_school = document.getElementById("school_new_selection");

    if(selected.value == '1' && selected_school.value !=='' ){
        add.disabled = false;
        for(i = 0; i < edit.length; i++) {
            edit[i].disabled = false;
        }
    } else {
    }

}, false);
