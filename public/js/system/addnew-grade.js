/****************************************
* 新規ボタン押下で1行追加
*(1)表の1番上に1行つくり、そこに要素を属す
*(2)追加された行にある「保存」ボタンの連打での多重送信防止
*(3)追加された行にある「キャンセル」ボタンを押すと、追加した行を削除
*****************************************/
let add = document.getElementById("add");
document.addEventListener("DOMContentLoaded", function(){
    add.addEventListener('click', () => {
        let insert_btn = document.getElementById("insert_btn");
        if(insert_btn == null){

            let edit = document.getElementsByName("edit");
            let s_btn = document.getElementsByName("save");
            let del_checks = document.getElementsByName("checks[]");
            let edit_cancel = document.getElementsByName("editcancel");
            let s_grade_name = document.getElementsByName("grade_name");
            for(i = 0; i < edit.length; i++) {
                edit[i].disabled = true;
                s_btn[i].disabled = true;
                del_checks[i].disabled = true;
                edit_cancel[i].disabled = true;
            }

            var tableElem = document.getElementById('mainTable');
            var csrf = document.querySelector('meta[name="csrf-token"]').content;
            var trElem = tableElem.insertRow(1);
            trElem.id = 'add_new_tr';
            trElem.style.backgroundColor = "#DDDDFF";
            var form = document.createElement('form');
            form.action = './grade-add';
            form.id = 'insert_btn';
            form.method = 'POST';
            form.enctype = 'multipart/form-data';
            trElem.append(form);

            let csrfinput = document.createElement('input');
            csrfinput.setAttribute('name','_token');
            csrfinput.setAttribute('value',csrf);
            csrfinput.setAttribute('type','hidden');
            form.appendChild(csrfinput);

            let hdn_school = document.getElementById('school_id');
            var qi = document.createElement('input');
            qi.setAttribute('form','insert_btn');
            qi.type = 'hidden';
            qi.name = 'school_id';
            qi.value = hdn_school.value;
            form.appendChild(qi);

            var cellElem1 = trElem.insertCell(0);
            var cellElem2 = trElem.insertCell(0);
            var cellElem3 = trElem.insertCell(0);
            var cellElem4 = trElem.insertCell(0);
            var cellElem5 = trElem.insertCell(0);
            cellElem1.style = "border: 1px solid white";
            cellElem2.style = "border: 1px solid white";
            cellElem3.style = "border: 1px solid white";
            cellElem4.style = "border: 1px solid white";
            cellElem5.style = "border: 1px solid white";

            cellElem1.appendChild(document.createTextNode(''));

            let button = document.createElement('input');
            button.setAttribute('type','submit');
            button.setAttribute('name','update');
            button.setAttribute('id','update_id');
            button.setAttribute('form','insert_btn');
            button.setAttribute('value','保存');
            button.setAttribute('class','btn btn-primary btn-block');
            cellElem1.appendChild(button);

            let button_cancel = document.createElement('input');
            button_cancel.setAttribute('type','button');
            button_cancel.setAttribute('name','cancel');
            button_cancel.setAttribute('id','cancel_id');
            button_cancel.setAttribute('form','insert_btn');
            button_cancel.setAttribute('value','ｷｬﾝｾﾙ');
            button_cancel.setAttribute('class','btn btn-primary btn-block');
            cellElem1.appendChild(button_cancel);

            var input1 = document.createElement('input');
            input1.name = 'grade_name_new';
            input1.type = 'text';
            input1.className = 'form-control col-xs-2';
            input1.maxLength = 10;
            input1.setAttribute('form','insert_btn');
            cellElem2.appendChild(input1);

            let select2 = document.getElementById('school_new_selection');
            var cloneElement2 = select2.cloneNode(true);
            cellElem3.appendChild(cloneElement2);

            let new_cancel = document.getElementById("cancel_id");
            let new_save = document.getElementById("update_id");
            new_save.addEventListener('click', () => {
                new_cancel.disabled = true;
                new_save.value = '保存中...';
                setTimeout(function () {
                    new_save.disabled = true;
                }, 100);
            })

            let add_new_tr = document.getElementById('add_new_tr');
            new_cancel.addEventListener('click', () => {
            add_new_tr.remove();
            for(i = 0; i < edit.length; i++) {
                edit[i].disabled = false
            }
            })

        }
    });
}, false);
