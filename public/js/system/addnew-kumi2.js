/****************************************
* 新規ボタン押下で1行追加
*(1)表の1番上に1行つくり、そこに要素を属す
*(2)追加された行にある「保存」ボタンの連打での多重送信防止
*(3)追加された行にある「キャンセル」ボタンを押すと、追加した行を削除
*****************************************/
let add = document.getElementById("add");
// ページロード時
document.addEventListener("DOMContentLoaded", function(){
    //「新規」ボタン押下　※コンソールでaddがnullとエラーがでるが、学校選択してからでないと現れないので、このままでOK
    add.addEventListener('click', () => {

        //新規ボタン連打防止　「insert_btn」は新規フォームのid   
        let insert_btn = document.getElementById("insert_btn");
        if(insert_btn == null){

            //キャンセルボタン押下用に変数定義
            let edit = document.getElementsByName("edit");
            let s_btn = document.getElementsByName("save");
            let del_checks = document.getElementsByName("checks[]");
            let edit_cancel = document.getElementsByName("editcancel");
            let s_grade_name = document.getElementsByName("grade_name");
            //他のすべての行の編集ボタンを非活性にする
            //編集ボタンが押せると、バグが生じるため
            for(i = 0; i < edit.length; i++) {
                edit[i].disabled = true;
                s_btn[i].disabled = true;
                del_checks[i].disabled = true;
                edit_cancel[i].disabled = true;
                //新規ボタン押下後、キャンセルボタン押下の後に、編集ボタンを押すと、組入力欄が活性化しないため
                //s_kumi_name[i].disabled = true;
            }
        
            // table要素を取得
            var tableElem = document.getElementById('mainTable');
            var csrf = document.querySelector('meta[name="csrf-token"]').content;
            
            // tbody要素にtr要素（行）を追加、行のスタイル、idは必要
            var trElem = tableElem.insertRow(1);
            trElem.id = 'add_new_tr';
            trElem.style.backgroundColor = "#DDDDFF";
            
            //form要素取得
            var form = document.createElement('form');
            form.action = './kumi2-save';
            form.id = 'insert_btn';
            form.method = 'POST';
            form.enctype = 'multipart/form-data';
            trElem.append(form);

            //hidden要素に、tokunをつける（base.bladeのmetaでtoken発行しており、そこと連動
            let csrfinput = document.createElement('input');
            csrfinput.setAttribute('name','_token');
            csrfinput.setAttribute('value',csrf);
            csrfinput.setAttribute('type','hidden');
            form.appendChild(csrfinput);

            //inputに要素を追加（hidden 学校ID）（必須）
            //form内のhiddenで取得すると、新規登録で1件目を入力するとき、要素が存在しないため、学校IDが取得できない
            // bladeファイルの下部のhiddenのここから取得⇒{{ Form::text($school->id,$school->id,['id' => 'get_school_int_id']) }}
            let hidden_school_id = document.getElementById('get_school_int_id');
            let school_idinput = document.createElement('input');
            school_idinput.setAttribute('name','hidden_school_id');
            school_idinput.setAttribute('value',hidden_school_id.value);
            school_idinput.setAttribute('type','hidden');
            form.appendChild(school_idinput);
            
            // td要素を追加　とスタイルの設定
            var cellElem1 = trElem.insertCell(0);
            var cellElem2 = trElem.insertCell(0);
            var cellElem3 = trElem.insertCell(0);
            var cellElem4 = trElem.insertCell(0);
            var cellElem5 = trElem.insertCell(0);
            cellElem1.style = "border: 1px solid white";
            cellElem2.style = "border: 1px solid white";
            cellElem3.style = "border: 1px solid white";
            cellElem4.style = "border: 1px solid white";
            cellElem5.style = "border: 1px solid white";

            //右から1つ目のセルはテキストボックスで空欄
            cellElem1.appendChild(document.createTextNode(''));

            //右から1つ目の空欄のセルにsubmitボタン（保存）を設置、IDは必須
            let button = document.createElement('input');
            button.setAttribute('type','submit');
            button.setAttribute('name','update');
            button.setAttribute('id','update_id');
            button.setAttribute('form','insert_btn');
            button.setAttribute('value','保存');
            button.setAttribute('class','btn btn-primary btn-block');
            cellElem1.appendChild(button);

            //右から1つ目の空欄のセルにキャンセルボタンを設置
            //キャンセルボタンの挙動は checkedit-grade.js で設定
            let button_cancel = document.createElement('input');
            button_cancel.setAttribute('type','button');
            button_cancel.setAttribute('name','cancel');
            button_cancel.setAttribute('id','cancel_id');
            button_cancel.setAttribute('form','insert_btn');
            button_cancel.setAttribute('value','ｷｬﾝｾﾙ');
            button_cancel.setAttribute('class','btn btn-primary btn-block');
            cellElem1.appendChild(button_cancel); 

            //右から2つ目のセルに学年を入力するテキストボックス　最大文字10字
            var input1 = document.createElement('input');
            input1.name = 'kumi_name_new';
            input1.type = 'text';
            input1.className = 'form-control col-xs-2';
            input1.maxLength = 10;
            input1.setAttribute('form','insert_btn');
            cellElem2.appendChild(input1);

             /////保存ボタンの連続押下対策のための変数定義
             let new_cancel = document.getElementById("cancel_id");
             let new_save = document.getElementById("update_id");
             //保存ボタン押下後、隣にあるキャンセルボタンを非活性、保存ボタンの文字列を「保存中...」にして、100ミリ秒後に保存ボタンを非活性。
             //保存ボタンの非活性を100ミリ秒後にする理由⇒これを入れないと、送信する前に非活性になり、保存ボタンが押下できないから。
             new_save.addEventListener('click', () => {
                 new_cancel.disabled = true;
                 new_save.value = '保存中...';
                 setTimeout(function () {
                     new_save.disabled = true; 
                 }, 100);
             })
             
             /////新規の登録中、キャンセルボタン押下後、新規行は削除され、削除後、他の行の編集ボタンがすべて活性化（リロード初期状態）
             let add_new_tr = document.getElementById('add_new_tr');
             new_cancel.addEventListener('click', () => {
             add_new_tr.remove();
             for(i = 0; i < edit.length; i++) {
                 edit[i].disabled = false
             }
             })
 
         }
     });
 }, false);
