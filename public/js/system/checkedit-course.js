/****************************************
 * 変数定義
 *****************************************/
let edit = document.getElementsByName("edit");
let s_btn = document.getElementsByName("save");
let del_checks = document.getElementsByName("checks[]");
let edit_cancel = document.getElementsByName("editcancel");

let s_course_type = document.getElementsByName("course_type");
let s_course_tab = document.getElementsByName("course_tab");
let s_course_category = document.getElementsByName("course_category");
let s_course_level = document.getElementsByName("course_level");
let s_display_order = document.getElementsByName("display_order");
let s_course_name = document.getElementsByName("course_name");
let s_invalid = document.getElementsByName("invalid");
let s_is_random = document.getElementsByName("is_random");
let s_test_time = document.getElementsByName("test_time");
let s_is_free_course = document.getElementsByName("is_free_course");
let s_school_id_selected = document.getElementsByName("school_id_selected");
let s_contest = document.getElementsByName("contest");

/****************************************
 * (1)編集ボタン押下時
 * (2)キャンセル押下時
 * (3)保存ボタン押下時
 *****************************************/
document.addEventListener(
    "DOMContentLoaded",
    function () {
        let len_edit = edit.length;
        for (let c_edit = 0; c_edit < len_edit; c_edit++) {
            edit[c_edit].addEventListener("click", () => {
                for (let j = 0; j < len_edit; j++) {
                    s_course_type[j].disabled = true;
                    s_course_tab[j].disabled = true;
                    s_course_category[j].disabled = true;
                    s_course_level[j].disabled = true;
                    s_display_order[j].readOnly = true;
                    s_contest[j].readOnly = true;
                    s_course_name[j].readOnly = true;
                    s_invalid[j].disabled = true;
                    s_is_random[j].disabled = true;
                    s_test_time[j].readOnly = true;
                    s_btn[j].disabled = true;
                    s_is_free_course[j].disabled = true;
                    del_checks[j].disabled = false;
                    s_school_id_selected[j].disabled = true;
                }
                del_checks[c_edit].checked = false;
                del_checks[c_edit].disabled = true;
                s_btn[c_edit].disabled = false;
                edit_cancel[c_edit].disabled = false;
                edit[c_edit].disabled = true;
                s_course_type[c_edit].disabled = false;
                s_course_tab[c_edit].disabled = false;
                s_course_category[c_edit].disabled = false;
                s_course_level[c_edit].disabled = false;
                s_display_order[c_edit].readOnly = false;
                s_contest[c_edit].readOnly = false;
                s_course_name[c_edit].readOnly = false;
                s_invalid[c_edit].disabled = false;
                s_is_random[c_edit].disabled = false;
                s_test_time[c_edit].readOnly = false;
                s_is_free_course[c_edit].disabled = false;
                s_school_id_selected[c_edit].disabled = false;

                if (s_invalid[c_edit].checked) {
                    editbefore_s_invalid = true;
                } else {
                    editbefore_s_invalid = false;
                }
                if (s_is_random[c_edit].checked) {
                    editbefore_s_is_random = true;
                } else {
                    editbefore_s_is_random = false;
                }
                if (s_is_free_course[c_edit].checked) {
                    editbefore_s_is_free_course = true;
                } else {
                    editbefore_s_is_free_course = false;
                }

                editbefore_s_invalid = s_invalid[c_edit].checked;
                editbefore_s_display_order = s_display_order[c_edit].value;
                editbefore_s_contest = s_contest[c_edit].value;
                editbefore_s_course_name = s_course_name[c_edit].value;
                editbefore_s_is_random = s_is_random[c_edit].checked;
                editbefore_s_is_free_course = s_is_free_course[c_edit].checked;
                editbefore_s_test_time = s_test_time[c_edit].value;
                editbefore_s_course_level = s_course_level[c_edit].value;
                editbefore_s_course_category = s_course_category[c_edit].value;
                editbefore_s_course_type = s_course_type[c_edit].value;
                editbefore_s_course_tab = s_course_tab[c_edit].value;
            });
            edit_cancel[c_edit].addEventListener("click", () => {
                s_invalid[c_edit].checked = editbefore_s_invalid;
                s_display_order[c_edit].value = editbefore_s_display_order;
                s_contest[c_edit].value = editbefore_s_contest;
                s_course_name[c_edit].value = editbefore_s_course_name;
                s_is_random[c_edit].checked = editbefore_s_is_random;
                s_is_free_course[c_edit].checked = editbefore_s_is_free_course;
                s_test_time[c_edit].value = editbefore_s_test_time;
                s_course_level[c_edit].value = editbefore_s_course_level;
                s_course_category[c_edit].value = editbefore_s_course_category;
                s_course_type[c_edit].value = editbefore_s_course_type;
                s_course_tab[c_edit].value = editbefore_s_course_tab;

                del_checks[c_edit].disabled = false;
                s_btn[c_edit].disabled = true;
                edit_cancel[c_edit].disabled = true;
                edit[c_edit].disabled = false;

                s_invalid[c_edit].disabled = true;
                s_display_order[c_edit].readOnly = true;
                s_contest[c_edit].readOnly = true;
                s_course_name[c_edit].readOnly = true;
                s_is_random[c_edit].disabled = true;
                s_is_free_course[c_edit].disabled = true;
                s_test_time[c_edit].readOnly = true;
                s_course_level[c_edit].disabled = true;
                s_course_category[c_edit].disabled = true;
                s_course_type[c_edit].disabled = true;
                s_course_tab[c_edit].disabled = true;
            });
            s_btn[c_edit].addEventListener("click", () => {
                edit_cancel[c_edit].disabled = true;
                s_btn[c_edit].value = "保存中...";
                setTimeout(function () {
                    s_btn[c_edit].disabled = true;
                }, 100);
            });
        }
    },
    false
);
