/****************************************
* 変数定義
* 表内の要素はforeachでまわすためidでなく、nameで取得「getElementsByName」（複数）
*****************************************/

let edit = document.getElementsByName("edit");
let s_cancel = document.getElementsByName("cancel");
let s_save = document.getElementsByName("save");
let s_user_name = document.getElementsByName("user_name");
let s_grade_id = document.getElementsByName("grade_id");
let s_kumi_id = document.getElementsByName("kumi_id");
let s_attendance_no = document.getElementsByName("attendance_no");
let s_role = document.getElementsByName("role");
let checks1 = document.getElementsByName("checks");
let del_checks = document.getElementsByName("checks[]");
let modal_password_reset_btn = document.getElementsByName("modal_password_reset_btn");

/****************************************
* 編集ボタン押下時、キャンセルボタン押下時(削除チェックは非活性)
*****************************************/
document.addEventListener("DOMContentLoaded", function(){
    let len_edit = edit.length;
    for (let c_edit = 0; c_edit < len_edit; c_edit++) {
        edit[c_edit].addEventListener('click', () => {
            for(let j = 0; j < len_edit; j++){
                s_user_name[j].readOnly = true;
                s_attendance_no[j].readOnly = true;
                s_grade_id[j].disabled = true;
                s_kumi_id[j].disabled = true;
                s_save[j].disabled = true;
                s_cancel[j].disabled = true;
                edit[j].disabled = true;
            }
            del_checks[c_edit].checked = false;
            del_checks[c_edit].disabled = true;
            s_user_name[c_edit].readOnly = false;
            s_attendance_no[c_edit].readOnly = false;
            s_grade_id[c_edit].disabled = false;
            s_kumi_id[c_edit].disabled = false;
            s_save[c_edit].disabled = false;
            s_cancel[c_edit].disabled = false;
            edit[c_edit].disabled = false;

            editbefore_s_user_name =s_user_name[c_edit].value;
            editbefore_s_attendance_no =s_attendance_no[c_edit].value;
            editbefore_s_grade_id =s_grade_id[c_edit].value;
            editbefore_s_kumi_id =s_kumi_id[c_edit].value;
        });
        s_cancel[c_edit].addEventListener('click', () => {
            s_user_name[c_edit].value = editbefore_s_user_name;
            s_attendance_no[c_edit].value = editbefore_s_attendance_no;
            s_grade_id[c_edit].value = editbefore_s_grade_id;
            s_kumi_id[c_edit].value = editbefore_s_kumi_id;
            edit[c_edit].disabled = false;
            s_save[c_edit].disabled = true;
            s_cancel[c_edit].disabled = true;
            s_user_name[c_edit].readOnly = true;
            s_grade_id[c_edit].disabled = true;
            s_kumi_id[c_edit].disabled = true;
            s_attendance_no[c_edit].readOnly = true;
        });
        s_save[c_edit].addEventListener('click', () => {
        s_cancel[c_edit].disabled = true;
        s_save[c_edit].textContent = '保存中...';

        setTimeout(function () {
            s_save[c_edit].disabled = true;
        }, 100);
        });
        modal_password_reset_btn[c_edit].addEventListener('click', () => {
            modal_password_reset_btn[c_edit].textContent = 'パスワードリセット中...';

            setTimeout(function () {
                modal_password_reset_btn[c_edit].disabled = true;
            }, 100);
        });
    }
}, false);

