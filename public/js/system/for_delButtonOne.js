/****************************************
* コンポーネント「delButtonOne」削除確認モーダルのボタン
* ボタン押下時、非活性、文字列変更　⇒連打防止
*****************************************/
let deleteexecute = document.getElementById("delete-execute");
deleteexecute.addEventListener('click', () => {
    deleteexecute.innerHTML='削除しています...'

    setTimeout(function () {
      deleteexecute.disabled = true;
  }, 100);


  });
