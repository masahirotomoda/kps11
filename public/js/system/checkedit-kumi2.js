/****************************************
* 変数定義
*****************************************/
let edit = document.getElementsByName("edit");
let s_kumi_name = document.getElementsByName("kumi_name");
let s_btn = document.getElementsByName("save");
let edit_cancel = document.getElementsByName("editcancel");
let del_checks = document.getElementsByName("checks[]");

/****************************************
* 編集ボタン押下時
*キャンセル押下時
*保存ボタン押下時
*****************************************/
//ページロード時
document.addEventListener("DOMContentLoaded", function(){
    //編集ボタンの数だけ、forループ
    let len_edit = edit.length;
    for (let c_edit = 0; c_edit < len_edit; c_edit++) {
        edit[c_edit].addEventListener('click', () => {
            for(let j = 0; j < len_edit; j++){
                //各行のカラム初期値
                s_kumi_name[j].readOnly = true;//組の名前
                s_btn[j].disabled = true; //保存ボタン
                edit_cancel[j].disabled = true; //キャンセルボタン
                edit[j].disabled = false;  //編集ボタン
                del_checks[j].disabled = false; //削除ボタン
            }
            //クリックした行を活性化
            s_kumi_name[c_edit].readOnly = false;
            del_checks[c_edit].checked = false; //チェックして、編集ボタンを押すと非活性化になるため、編集ボタン押下で、チェックをはずす。
            del_checks[c_edit].disabled = true;
            s_btn[c_edit].disabled = false;
            edit_cancel[c_edit].disabled = false;
            edit[c_edit].disabled = true;

            //キャンセル時、元のデータに戻すための準備
            editbefore_s_kumi_name = s_kumi_name[c_edit].value;

    });
    //キャンセルボタン押下時
    edit_cancel[c_edit].addEventListener('click', () => {
        //編集ボタン押下時の学年データにもどす
        s_kumi_name[c_edit].value = editbefore_s_kumi_name;
        
        del_checks[c_edit].disabled = false; //削除ボタン活性化
        s_btn[c_edit].disabled = true; //保存ボタン非活性
        edit_cancel[c_edit].disabled = true; //キャンセルボタン非活性
        edit[c_edit].disabled = false; //編集ボタン活性化

        s_kumi_name[c_edit].readOnly = true; //学年名入力ボックス非活性
    });
    //保存ボタン押下時⇒連打で多重送信防止
    s_btn[c_edit].addEventListener('click', () => {
        edit_cancel[c_edit].disabled = true;
        s_btn[c_edit].value = '保存中...';
    
        setTimeout(function () {
            s_btn[c_edit].disabled = true; 
        }, 100);
    
    });
}
}, false);
