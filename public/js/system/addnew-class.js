/****************************************
* 新規ボタン押下で1行追加
*(1)表の1番上に1行つくり、そこに要素を属す
*(2)追加された行にある「保存」ボタンの連打での多重送信防止
*(3)追加された行にある「キャンセル」ボタンを押すと、追加した行を削除
*****************************************/
let add = document.getElementById("add");
document.addEventListener("DOMContentLoaded", function(){
    add.addEventListener('click', () => {

        let insert_btn = document.getElementById("insert_btn");
        if(insert_btn == null){

            let edit = document.getElementsByName("edit");
            let s_btn = document.getElementsByName("save");
            let del_checks = document.getElementsByName("checks[]");
            let edit_cancel = document.getElementsByName("editcancel");
            let grade_id = document.getElementsByName("grade_id");
            let kumi_id = document.getElementsByName("kumi_id");

            for(i = 0; i < edit.length; i++) {
                edit[i].disabled = true;
                s_btn[i].disabled = true;
                del_checks[i].disabled = true;
                edit_cancel[i].disabled = true;
                grade_id[i].disabled = true;
                kumi_id[i].disabled = true;
            }

            var tableElem = document.getElementById('mainTable');
            var csrf = document.querySelector('meta[name="csrf-token"]').content;
            var trElem = tableElem.insertRow(1);
            trElem.id = 'add_new_tr';
            trElem.style.backgroundColor = "#DDDDFF";

            var form = document.createElement('form');
            form.action = './class-add';
            form.id = 'insert_btn';
            form.method = 'POST';
            trElem.append(form);

            let csrfinput = document.createElement('input');
            csrfinput.setAttribute('name','_token');
            csrfinput.setAttribute('value',csrf);
            csrfinput.setAttribute('type','hidden');
            form.appendChild(csrfinput);

            var cellElem1 = trElem.insertCell(0);
            var cellElem2 = trElem.insertCell(0);
            var cellElem3 = trElem.insertCell(0);
            var cellElem4 = trElem.insertCell(0);
            var cellElem5 = trElem.insertCell(0);
            var cellElem6 = trElem.insertCell(0);
            cellElem1.style = "border: 1px solid white";
            cellElem2.style = "border: 1px solid white";
            cellElem3.style = "border: 1px solid white";
            cellElem4.style = "border: 1px solid white";
            cellElem5.style = "border: 1px solid white";
            cellElem6.style = "border: 1px solid white";

            let button = document.createElement('input');
            button.setAttribute('type','submit');
            button.setAttribute('id','update_id');
            button.setAttribute('name','update');
            button.setAttribute('form','insert_btn');
            button.setAttribute('value','保存');
            button.setAttribute('class','btn btn-secondary btn-sm');
            cellElem1.appendChild(button);

            cellElem1.appendChild(document.createTextNode(''));

            let select1 = document.getElementById('kumi_new_selection');
            select1.setAttribute('name','kumi_id');
            select1.setAttribute('id','new_kumi_id');
            select1.setAttribute('form','insert_btn');
            var cloneElement1 = select1.cloneNode(true);
            cloneElement1.hidden = false;
            cellElem2.appendChild(cloneElement1);

            let select2 = document.getElementById('grade_new_selection');
            select2.setAttribute('name','grade_id');
            select2.setAttribute('id','new_grade_id');
            select2.setAttribute('form','insert_btn');
            var cloneElement2 = select2.cloneNode(true);
            cloneElement2.hidden = false;
            cellElem3.appendChild(cloneElement2);

            let select3 = document.getElementById('school_new_selection');
            const num = select3.selectedIndex;
            const val = select3.options[num].value;
            var q = document.createElement('input');
            q.type = 'hidden';
            q.name = 'school_id';
            q.value = val;
            form.appendChild(q);

            cellElem4.appendChild(document.createTextNode(''));

            var cloneElement3 = select3.cloneNode(true);
            cloneElement3.setAttribute('name','school_id');
            cloneElement3.setAttribute('id','new_school_id');
            cloneElement3.setAttribute('form','insert_btn');
            cloneElement3.disabled = true;

            cellElem5.appendChild(cloneElement3);

            cellElem6.appendChild(document.createTextNode(''));

            let new_cancel = document.getElementById("cancel_id");
            let new_save = document.getElementById("update_id");
            new_save.addEventListener('click', () => {
                new_cancel.disabled = true;
                new_save.value = '保存中...';
                setTimeout(function () {
                    new_save.disabled = true;
                }, 100);
            })
        };

    });
}, false);

