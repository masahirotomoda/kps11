/****************************************
* 新規ボタン押下で1行追加
*(1)表の1番上に1行つくり、そこに要素を属す
*(2)追加された行にある「保存」ボタンの連打での多重送信防止
*(3)追加された行にある「キャンセル」ボタンを押すと、追加した行を削除
*****************************************/
let add = document.getElementById("add");
// ページロード時
document.addEventListener("DOMContentLoaded", function(){
    //「新規」ボタン押下　※コンソールでaddがnullとエラーがでるが、学校選択してからでないと現れないので、このままでOK
    add.addEventListener('click', () => {

        //id「insert_btn」は新規フォームのID、新規フォームがない時だけ、新規フォームをつくる
        let insert_btn = document.getElementById("insert_btn");
        if(insert_btn == null){

            //キャンセルボタン押下用に変数定義
            let edit = document.getElementsByName("edit");
            let s_btn = document.getElementsByName("save");
            let del_checks = document.getElementsByName("checks[]");
            let edit_cancel = document.getElementsByName("editcancel");
            let grade_id = document.getElementsByName("grade_id");
            let kumi_id = document.getElementsByName("kumi_id");
            
            for(i = 0; i < edit.length; i++) {
                edit[i].disabled = true;
                s_btn[i].disabled = true;
                del_checks[i].disabled = true;
                edit_cancel[i].disabled = true;
                //新規ボタン押下後、キャンセルボタン押下の後に、編集ボタンを押すと、学年,組入力欄が活性化しないため
                grade_id[i].disabled = true;
                kumi_id[i].disabled = true;
            }

            //table要素を取得（トークンはbase.bladeファイルのヘッダーのmetaから取得）
            var tableElem = document.getElementById('mainTable');
            var csrf = document.querySelector('meta[name="csrf-token"]').content;
            var trElem = tableElem.insertRow(1);
            trElem.id = 'add_new_tr';
            trElem.style.backgroundColor = "#DDDDFF";
            
            //form要素取得
            var form = document.createElement('form');
            form.action = './class2-add';
            form.id = 'insert_btn';
            form.method = 'POST';
            trElem.append(form);

            //hidden要素に、tokunをつける
            let csrfinput = document.createElement('input');
            csrfinput.setAttribute('name','_token');
            csrfinput.setAttribute('value',csrf);
            csrfinput.setAttribute('type','hidden');
            form.appendChild(csrfinput);

            // td要素を追加　とスタイルの設定
            var cellElem1 = trElem.insertCell(0);
            var cellElem2 = trElem.insertCell(0);
            var cellElem3 = trElem.insertCell(0);
            var cellElem4 = trElem.insertCell(0);
            var cellElem5 = trElem.insertCell(0);
            var cellElem6 = trElem.insertCell(0);
            cellElem1.style = "border: 1px solid white";
            cellElem2.style = "border: 1px solid white";
            cellElem3.style = "border: 1px solid white";
            cellElem4.style = "border: 1px solid white";
            cellElem5.style = "border: 1px solid white";
            cellElem6.style = "border: 1px solid white";

            //右から1つ目の空欄のセルにsubmitボタン（保存）を設置、IDは必須
            let button = document.createElement('input');
            button.setAttribute('type','submit');
            button.setAttribute('id','update_id');
            button.setAttribute('name','update');
            button.setAttribute('form','insert_btn');
            button.setAttribute('value','保存');
            button.setAttribute('class','btn btn-secondary btn-sm');
            cellElem1.appendChild(button);

            //右から1つ目セルはテキストボックス
            cellElem1.appendChild(document.createTextNode(''));

            /*
            ★ｷｬﾝｾﾙボタンはなしに変更★【理由】ｷｬﾝｾﾙした後に、再度新規ボタン押下で、学年、組のドロップダウンが表示されないバグ発生のため。
            右から1つ目の空欄のセルにキャンセルボタン
            2行目の編集ボタン押下で3行目がアクティブになる　これを防ぐためにリロードする
            let button_cancel = document.createElement('input');
            button_cancel.setAttribute('type','button');
            button_cancel.setAttribute('name','cancel');
            button_cancel.setAttribute('id','cancel_id');
            button_cancel.setAttribute('form','insert_btn');
            button_cancel.setAttribute('value','ｷｬﾝｾﾙ');
            button_cancel.setAttribute('class','btn btn-secondary btn-sm');
            cellElem1.appendChild(button_cancel); 
            */

            //右から3つ目セルに組のセレクトボックス　IDが「kumi」のセレクトボックスをコピー
            let select1 = document.getElementById('s_kumi');
            select1.setAttribute('name','kumi_id');
            select1.setAttribute('id','new_kumi_id');
            select1.setAttribute('form','insert_btn');
            var cloneElement1 = select1.cloneNode(true);
            cloneElement1.hidden = false;
            cellElem3.appendChild(cloneElement1);

            //右から4つ目セルに学年のセレクトボックス　IDが「grade」のセレクトボックスをコピー           
            let select2 = document.getElementById('s_grade');
            select2.setAttribute('name','grade_id');
            select2.setAttribute('id','new_grade_id');
            select2.setAttribute('form','insert_btn');
            var cloneElement2 = select2.cloneNode(true);
            cloneElement2.hidden = false;
            cellElem4.appendChild(cloneElement2);

            //inputに要素を追加（hidden 学校ID）
            //form内のhiddenで取得すると、新規登録で1件目を入力するとき、要素が存在しないため、学校IDが取得できない
            // bladeファイルの下部のhiddenのここから取得⇒{{ Form::text($school->id,$school->id,['id' => 'get_school_int_id']) }}
            let hidden_school_id = document.getElementById('get_school_int_id');
            let school_idinput = document.createElement('input');
            school_idinput.setAttribute('name','hidden_school_id');
            school_idinput.setAttribute('value',hidden_school_id.value);
            school_idinput.setAttribute('type','hidden');
            form.appendChild(school_idinput);

            //右から5つ目セルはテキストボックス
            cellElem5.appendChild(document.createTextNode(''));

            //右から6つ目セルはテキストボックス
            cellElem6.appendChild(document.createTextNode(''));
           


            /////ここから、新規行の保存ボタン押下処理
            let new_cancel = document.getElementById("cancel_id");
            let new_save = document.getElementById("update_id");
            new_save.addEventListener('click', () => {
                new_cancel.disabled = true;
                new_save.value = '保存中...';
                setTimeout(function () {
                    new_save.disabled = true; 
                }, 100);
            })

            /*
            ここから、新規行のｷｬﾝｾﾙボタン押下処理　★★キャンセルボタンなしに変更★★
            【理由】キャンセル後、新規作成ボタンを押すと、学年、組の表示されないバグ発生。ページロード時にのみ、学年と組のドロップダウンのコピーができるため。
            let add_new_tr = document.getElementById('add_new_tr');
            new_cancel.addEventListener('click', () => {
            追加した行を削除
            add_new_tr.remove();
            for(i = 0; i < edit.length; i++) {
               edit[i].disabled = false
            }
            })
            */
        };
        
    });
}, false);

