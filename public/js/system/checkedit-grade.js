/****************************************
* 変数定義
*****************************************/
let edit = document.getElementsByName("edit");
let s_grade_name = document.getElementsByName("grade_name");
let s_btn = document.getElementsByName("save");
let edit_cancel = document.getElementsByName("editcancel");
let del_checks = document.getElementsByName("checks[]");

/****************************************
* 編集ボタン押下時
*キャンセル押下時
*保存ボタン押下時
*****************************************/
document.addEventListener("DOMContentLoaded", function(){
    let len_edit = edit.length;
    for (let c_edit = 0; c_edit < len_edit; c_edit++) {
        edit[c_edit].addEventListener('click', () => {
        for(let j = 0; j < len_edit; j++){
            s_grade_name[j].readOnly = true;
            s_btn[j].disabled = true;
            edit_cancel[j].disabled = true;
            edit[j].disabled = false;
            del_checks[j].disabled = false;
        }
        s_grade_name[c_edit].readOnly = false;
        del_checks[c_edit].checked = false;
        del_checks[c_edit].disabled = true;
        s_btn[c_edit].disabled = false;
        edit_cancel[c_edit].disabled = false;
        edit[c_edit].disabled = true;
        editbefore_s_grade_name = s_grade_name[c_edit].value;
    });
    edit_cancel[c_edit].addEventListener('click', () => {
        s_grade_name[c_edit].value = editbefore_s_grade_name;

        del_checks[c_edit].disabled = false;
        s_btn[c_edit].disabled = true;
        edit_cancel[c_edit].disabled = true;
        edit[c_edit].disabled = false;

        s_grade_name[c_edit].readOnly = true;
    });
    s_btn[c_edit].addEventListener('click', () => {
        edit_cancel[c_edit].disabled = true;
        s_btn[c_edit].value = '保存中...';

        setTimeout(function () {
            s_btn[c_edit].disabled = true;
        }, 100);
    });
}
}, false);
