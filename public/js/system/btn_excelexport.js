/****************************************
* Excel出力
*****************************************/
let excelbtn = document.getElementById("excelbtn");
excelbtn.addEventListener('click', () => {
    setTimeout(function () {
        excelbtn.disabled = true;
        excelbtn.value = '処理中・・・';
    }, 100);            
    setTimeout(function () {
        excelbtn.disabled = false;
        excelbtn.value = 'エクセル出力';
    }, 4000);
});
