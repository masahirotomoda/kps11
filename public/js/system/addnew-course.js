/****************************************
 * 新規ボタン押下で1行追加
 *(1)表の1番上に1行追加、そこにformを作り、その中に、要素を作る
 *(2)新規行にある「保存」ボタンの連打での多重送信防止
 *(3)新規行にある「キャンセル」ボタン押下で、行を削除して他行の初期化
 *****************************************/
let add = document.getElementById("add");
document.addEventListener(
    "DOMContentLoaded",
    function () {
        add.addEventListener("click", () => {
            let insert_btn = document.getElementById("insert_btn");
            if (insert_btn == null) {
                let edit = document.getElementsByName("edit");
                let s_btn = document.getElementsByName("save");
                let edit_cancel = document.getElementsByName("editcancel");
                let del_checks = document.getElementsByName("checks[]");
                let s_course_type = document.getElementsByName("course_type");
                let s_course_tab = document.getElementsByName("course_tab");
                let s_course_category =
                    document.getElementsByName("course_category");
                let s_course_level = document.getElementsByName("course_level");
                let s_display_order =
                    document.getElementsByName("display_order");
                let s_course_name = document.getElementsByName("course_name");
                let s_invalid = document.getElementsByName("invalid");
                let s_is_random = document.getElementsByName("is_random");
                let s_test_time = document.getElementsByName("test_time");
                let s_is_free_course =
                    document.getElementsByName("is_free_course");
                let s_contest = document.getElementsByName("contest");

                for (i = 0; i < edit.length; i++) {
                    edit[i].disabled = true;
                    s_btn[i].disabled = true;
                    del_checks[i].disabled = true;
                    edit_cancel[i].disabled = true;
                    s_invalid[i].disabled = true;
                    s_display_order[i].disabled = true;
                    s_course_name[i].disabled = true;
                    s_is_random[i].disabled = true;
                    s_course_type[i].disabled = true;
                    s_course_tab[i].disabled = true;
                    s_course_category[i].disabled = true;
                    s_course_level[i].disabled = true;
                    s_test_time[i].disabled = true;
                    s_is_free_course[i].disabled = true;
                    s_contest[i].disabled = true;
                }

                var tableElem = document.getElementById("mainTable");

                var csrf = document.querySelector(
                    'meta[name="csrf-token"]'
                ).content;

                var trElem = tableElem.insertRow(1);
                trElem.style.backgroundColor = "#DDDDFF";
                trElem.id = "add_new_tr";

                var form = document.createElement("form");
                form.action = "./course-save";
                form.id = "insert_btn";
                form.method = "POST";
                form.enctype = "multipart/form-data";
                trElem.append(form);

                let csrfinput = document.createElement("input");
                csrfinput.setAttribute("name", "_token");
                csrfinput.setAttribute("value", csrf);
                csrfinput.setAttribute("type", "hidden");
                form.appendChild(csrfinput);

                var cellElem1 = trElem.insertCell(0);
                var cellElem2 = trElem.insertCell(0);
                var cellElem3 = trElem.insertCell(0);
                var cellElem4 = trElem.insertCell(0);
                var cellElem5 = trElem.insertCell(0);
                var cellElem6 = trElem.insertCell(0);
                var cellElem7 = trElem.insertCell(0);
                var cellElem8 = trElem.insertCell(0);
                var cellElem9 = trElem.insertCell(0);
                var cellElem10 = trElem.insertCell(0);
                var cellElem11 = trElem.insertCell(0);
                cellElem1.style = "border: 1px solid white";
                cellElem2.style = "border: 1px solid white";
                cellElem3.style = "border: 1px solid white";
                cellElem4.style = "border: 1px solid white";
                cellElem5.style = "border: 1px solid white";
                cellElem6.style = "border: 1px solid white";
                cellElem7.style = "border: 1px solid white";
                cellElem8.style = "border: 1px solid white";
                cellElem9.style = "border: 1px solid white";
                cellElem10.style = "border: 1px solid white";
                cellElem11.style = "border: 1px solid white";

                let button = document.createElement("input");
                button.setAttribute("type", "submit");
                button.setAttribute("name", "update");
                button.setAttribute("id", "update_id");
                button.setAttribute("form", "insert_btn");
                button.setAttribute("value", "保存");
                button.setAttribute("class", "btn btn-primary btn-block");
                cellElem3.appendChild(button);

                let button_cancel = document.createElement("input");
                button_cancel.setAttribute("type", "button");
                button_cancel.setAttribute("name", "cancel");
                button_cancel.setAttribute("id", "cancel_id");
                button_cancel.setAttribute("form", "insert_btn");
                button_cancel.setAttribute("value", "ｷｬﾝｾﾙ");
                button_cancel.setAttribute(
                    "class",
                    "btn btn-primary btn-block"
                );
                cellElem3.appendChild(button_cancel);

                let checkbox1 = document.createElement("input");
                checkbox1.setAttribute("type", "checkbox");
                checkbox1.setAttribute("name", "is_random");
                checkbox1.setAttribute("value", 1);
                checkbox1.setAttribute("form", "insert_btn");
                checkbox1.className = "form-check-input";
                cellElem4.appendChild(checkbox1);

                let select_tab = document.getElementById("nd_tab_selection");
                let c_select_tab = select_tab.cloneNode(true);
                c_select_tab.setAttribute("name", "course_tab");
                c_select_tab.setAttribute("form", "insert_btn");
                cellElem4.appendChild(c_select_tab);

                let select_level =
                    document.getElementById("nd_level_selection");
                let c_select_level = select_level.cloneNode(true);
                c_select_level.setAttribute("name", "course_level");
                c_select_level.setAttribute("form", "insert_btn");
                cellElem5.appendChild(c_select_level);

                var input_test_time = document.createElement("input");
                input_test_time.name = "test_time";
                input_test_time.type = "text";
                input_test_time.maxLength = 4;
                input_test_time.className = "form-control col-xs-2";
                input_test_time.setAttribute("form", "insert_btn");
                cellElem5.appendChild(input_test_time);

                let select_type = document.getElementById("nd_type_selection");
                let c_select_type = select_type.cloneNode(true);
                c_select_type.setAttribute("name", "course_type");
                c_select_type.setAttribute("form", "insert_btn");
                cellElem6.appendChild(c_select_type);

                let select_category = document.getElementById(
                    "nd_category_selection"
                );
                let c_select_category = select_category.cloneNode(true);
                c_select_category.setAttribute("name", "course_category");
                c_select_category.setAttribute("form", "insert_btn");
                cellElem6.appendChild(c_select_category);

                let select_school = document.getElementById(
                    "nd_school_selection"
                );
                let c_select_school = select_school.cloneNode(true);
                c_select_school.setAttribute("name", "school_id_selected");
                c_select_school.setAttribute("form", "insert_btn");
                cellElem7.appendChild(c_select_school);

                var input_course_name = document.createElement("input");
                input_course_name.name = "course_name";
                input_course_name.type = "text";
                input_course_name.maxLength = 30;
                input_course_name.required = true;
                input_course_name.className = "form-control col-xs-2";
                input_course_name.setAttribute("form", "insert_btn");
                cellElem7.appendChild(input_course_name);

                var input_display_order = document.createElement("input");
                input_display_order.name = "display_order";
                input_display_order.type = "text";
                input_display_order.maxLength = 4;
                input_display_order.className = "form-control col-xs-2";
                input_display_order.setAttribute("form", "insert_btn");
                cellElem8.appendChild(input_display_order);

                let checkbox2 = document.createElement("input");
                checkbox2.setAttribute("type", "checkbox");
                checkbox2.setAttribute("name", "is_free_course");
                checkbox2.setAttribute("value", 1);
                checkbox2.setAttribute("form", "insert_btn");
                checkbox2.className = "form-check-input";
                cellElem9.appendChild(checkbox2);

                let contest_txtbox = document.createElement("input");
                contest_txtbox.setAttribute("type", "text");
                contest_txtbox.setAttribute("name", "contest");
                contest_txtbox.maxLength = 4;
                contest_txtbox.className = "form-control col-xs-2";
                contest_txtbox.setAttribute("form", "insert_btn");
                cellElem9.appendChild(contest_txtbox);

                let checkbox3 = document.createElement("input");
                checkbox3.setAttribute("type", "checkbox");
                checkbox3.setAttribute("name", "invalid");
                checkbox3.setAttribute("value", 1);
                checkbox3.setAttribute("form", "insert_btn");
                checkbox3.className = "form-check-input";
                cellElem10.appendChild(checkbox3);

                let new_cancel = document.getElementById("cancel_id");
                let new_save = document.getElementById("update_id");
                new_save.addEventListener("click", () => {
                    new_cancel.disabled = true;
                    new_save.value = "保存中...";
                    setTimeout(function () {
                        new_save.disabled = true;
                    }, 100);
                });
                let add_new_tr = document.getElementById("add_new_tr");
                new_cancel.addEventListener("click", () => {
                    add_new_tr.remove();
                    for (i = 0; i < edit.length; i++) {
                        edit[i].disabled = false;
                    }
                });
            }
        });
    },
    false
);
