"use strict";
const romanBtn = document.getElementById("p-typingscreen__startscreen__roman");
const noromanBtn = document.getElementById(
    "p-typingscreen__startscreen__noroman"
);
const upperBtn = document.getElementById("p-typingscreen__startscreen__upper");
const lowerBtn = document.getElementById("p-typingscreen__startscreen__lower");
const optionContainer = document.getElementById(
    "p-typingscreen__startscreen__upper__container"
);
const keyboardSpace = document.getElementById("p-keyboard__key--space");
const keybordArea = document.getElementById("keyboard-area");
const keybordAreaInitImg = document.getElementById("keyboard-area-init");
const startBtn = document.getElementById("p-typingscreen__start");
const resultModalSpaceKey = document.getElementsByClassName("mb-4");
const perfect = document.getElementById("perfect");
let upperBtnChecked;
let noromanBtnChecked;

if (alphabet == "yes") {
    romanBtn.checked = true;
    noromanBtn.checked = false;
} else {
    noromanBtn.checked = true;
    romanBtn.checked = false;
}
if (sound == "yes") {
    soundBtn.checked = true;
} else {
    nosoundBtn.checked = true;
}
if (upperBtn.value == "★") {
    upperBtn.checked = true;
} else {
    lowerBtn.checked = true;
}
getCurrentCourseData();
function startBtnClick() {
    const testPronunciation = new Audio(routeLink + "audio/silence.mp3");
    testPronunciation.play();
    maetugiBtnFlg == true;
    restartFlg = false;
    keybordArea.classList.remove("hidden");
    keybordAreaInitImg.classList.add("hidden");
    startBtn.disabled = true;
    startBtn.textContent = "スペースではじめる";
    startBtn.classList.add("p-typingscreen__start__important");
    keyboardSpace.classList.add("p-keyboard__active");
    optionContainer.classList.add("hidden");
    spaceStartFlag = true;
    restartBtn.classList.add("hidden");
    mae.classList.add("hidden");
    tugi.classList.add("hidden");
    upperBtnChecked = upperBtn.checked;
    if (upperBtnChecked) {
        romanMap = romanMapUpper;
    } else {
        romanMap = romanMapLower;
    }
    soundBtnChecked = soundBtn.checked;
    if (honban == true) {
        sound = "no";
    } else {
        if (soundBtnChecked) {
            sound = "yes";
        } else {
            sound = "no";
        }
    }

    if (romanBtn.checked == true) {
        alphabet = "yes";
    } else {
        alphabet = "no";
    }
}

function startTypingFunc() {
    getCurrentWordData();
    keyboardSpace.classList.remove("p-keyboard__active");
    startBtn.classList.add("hidden");
    countDown.classList.remove("hidden");
    sortByKeyword();
    if (
        typeWords[keywordOrder[0]].pronunciation_file_name != null &&
        sound != "no"
    ) {
        firstAudio = new Audio(
            routeLink +
                "storage/audio/" +
                typeWords[keywordOrder[0]].pronunciation_file_name
        );
        firstAudio.load();
    }
    for (let i = 0; i <= countSeconds; i++) {
        setTimeout(() => {
            countDown.textContent = countSeconds - i;
        }, i * 1000);
    }
    setTimeout(() => {
        if (restartFlg == false) {
            countDown.classList.add("hidden");
            beforeTypingStartArea.classList.add("hidden");
            typingWordAreaBgBlack.classList.remove("hidden");
            initTypingFunc();
        }
    }, countSeconds * 1000);
}
function initTypingFunc() {
    keydownAllCnt = 0;
    successType = 0;
    missType = 0;
    if (honban == false) {
        restartBtn.classList.remove("hidden");
    }
    successtypeField.textContent = "入力: " + successType;
    misstypeField.textContent = "ミス: " + missType;
    nextKeywordFunc();
    typingFlag = true;
    beginDate = new Date();
}
function nextKeywordFunc() {
    typingShowField.innerHTML = "";
    typedField.textContent = "";
    untypedField.textContent = "";
    untypedCnt = 0;
    if (
        typeWords[keywordOrder[0]].word_kana === "" ||
        typeWords[keywordOrder[0]].word_kana === null
    ) {
        kanaField.textContent = typeWords[keywordOrder[0]].word_mana;
        manaField.textContent = typeWords[keywordOrder[0]].word_mana;

        keywordArr = [...typeWords[keywordOrder[0]].word_mana];
        untypedTwoArr = [];
        typedTwoArr = [];
        for (let i = 0; i < keywordArr.length; i++) {
            untypedTwoArr.push([keywordArr[i]]);
            typedTwoArr.push([""]);
        }
    } else {
        if (
            courseData.course_category == "ローマ字50音" ||
            courseData.course_category == "ホームポジション" ||
            courseData.course_category == "英単語"
        ) {
            manaField.textContent = typeWords[keywordOrder[0]].word_mana;
            kanaField.textContent = typeWords[keywordOrder[0]].null;
        } else {
            kanaField.textContent = typeWords[keywordOrder[0]].word_kana;
            manaField.textContent = typeWords[keywordOrder[0]].word_mana;
        }
        keywordArr = [...typeWords[keywordOrder[0]].word_kana];
        for (let i = 0; i < keywordArr.length; i++) {
            if (["ん"].indexOf(keywordArr[i]) > -1) {
                if (
                    [
                        "あ",
                        "い",
                        "う",
                        "え",
                        "お",
                        "な",
                        "に",
                        "ぬ",
                        "ね",
                        "の",
                        "っ",
                        undefined,
                    ].indexOf(keywordArr[i + 1]) < 0
                ) {
                    if (upperBtnChecked) {
                        keywordArr[i] = "N";
                    } else {
                        keywordArr[i] = "n";
                    }
                }
            }
        }
        for (let i = 0; i < keywordArr.length; i++) {
            if (["っ"].indexOf(keywordArr[i]) > -1) {
                let lower = keywordArr.slice(i, i + 1);
                let upper = keywordArr.slice(i + 1, i + 2);
                keywordArr.splice(i, 1);
                keywordArr.splice(i, 1, lower + upper);
            }
            if (
                ["ぁ", "ぃ", "ぅ", "ぇ", "ぉ", "ゃ", "ゅ", "ょ", "ゎ"].indexOf(
                    keywordArr[i]
                ) > -1
            ) {
                let lower = keywordArr.slice(i, i + 1);
                let upper = keywordArr.slice(i - 1, i);
                keywordArr.splice(i, 1);
                keywordArr.splice(i - 1, 1, upper + lower);
                i -= 1;
            }
        }
        let roman = [];
        untypedTwoArr = [];
        keywordArr.forEach((val, index) => {
            roman = [];
            if (val.length === 3) {
                if (romanMap[val] != undefined) {
                    roman = [...romanMap[val]];
                    const romanCopy = [...romanMap[val]];

                    if (upperBtnChecked) {
                        if (keywordArr[index - 1] == "N") {
                            for (val1 of romanCopy) {
                                roman.push("N" + val1);
                            }
                        }
                    } else {
                        if (keywordArr[index - 1] == "n") {
                            for (val1 of romanCopy) {
                                roman.push("n" + val1);
                            }
                        }
                    }
                }
                const idx1 = val.slice(0, 1);
                const idx2 = val.slice(1, 2);
                const idx3 = val.slice(2, 3);
                const roman1 = [...romanMap[idx1]];
                const roman2 = [...romanMap[idx2]];
                const roman3 = [...romanMap[idx3]];
                const idx23 = val.slice(1, 3);
                const roman23 = [...romanMap[idx23]];
                for (val1 of roman1) {
                    for (val2 of roman23) {
                        roman.push(val1 + val2);
                        if (upperBtnChecked) {
                            if (keywordArr[index - 1] == "N") {
                                roman.push("N" + val1 + val2);
                            }
                        } else {
                            if (keywordArr[index - 1] == "n") {
                                roman.push("n" + val1 + val2);
                            }
                        }
                    }
                }
                for (val1 of roman1) {
                    for (val2 of roman2) {
                        for (val3 of roman3) {
                            roman.push(val1 + val2 + val3);
                            if (upperBtnChecked) {
                                if (keywordArr[index - 1] == "N") {
                                    roman.push("N" + val1 + val2 + val3);
                                }
                            } else {
                                if (keywordArr[index - 1] == "n") {
                                    roman.push("n" + val1 + val2 + val3);
                                }
                            }
                        }
                    }
                }
                untypedTwoArr.push(roman);
            } else if (val.length === 2) {
                if (romanMap[val] != undefined) {
                    roman = [...romanMap[val]];
                    const romanCopy = [...romanMap[val]];

                    if (upperBtnChecked) {
                        if (keywordArr[index - 1] == "N") {
                            for (val1 of romanCopy) {
                                roman.push("N" + val1);
                            }
                        }
                    } else {
                        if (keywordArr[index - 1] == "n") {
                            for (val1 of romanCopy) {
                                roman.push("n" + val1);
                            }
                        }
                    }
                }
                const idx1 = val.slice(0, 1);
                const idx2 = val.slice(1, 2);
                const roman1 = [...romanMap[idx1]];
                const roman2 = [...romanMap[idx2]];
                for (val1 of roman1) {
                    for (val2 of roman2) {
                        roman.push(val1 + val2);
                        if (upperBtnChecked) {
                            if (keywordArr[index - 1] == "N") {
                                roman.push("N" + val1 + val2);
                            }
                        } else {
                            if (keywordArr[index - 1] == "n") {
                                roman.push("n" + val1 + val2);
                            }
                        }
                    }
                }
                untypedTwoArr.push(roman);
            } else {
                if (romanMap[val] != undefined) {
                    roman = [...romanMap[val]];
                    const romanCopy = [...romanMap[val]];

                    if (upperBtnChecked) {
                        if (keywordArr[index - 1] == "N") {
                            for (val1 of romanCopy) {
                                roman.push("N" + val1);
                            }
                        }
                    } else {
                        if (keywordArr[index - 1] == "n") {
                            for (val1 of romanCopy) {
                                roman.push("n" + val1);
                            }
                        }
                    }
                } else {
                    roman = [...val];
                }
                untypedTwoArr.push(roman);
            }
        });
        typedTwoArr = JSON.parse(JSON.stringify(untypedTwoArr));
        typedTwoArr.forEach((val1, idx1) => {
            typedTwoArr[idx1].forEach((val2, idx2) => {
                typedTwoArr[idx1][idx2] = "";
            });
        });
    }
    for (val of typedTwoArr) {
        typingShowField.innerHTML +=
            '<span class="typed">' + val[0] + "</span>";
        typedField.textContent += val[0];
    }
    for (val of untypedTwoArr) {
        if (alphabet == "yes") {
            typingShowField.innerHTML +=
                '<span class="untyped">' + val[0] + "</span>";
        }
        untypedField.textContent += val[0];
    }

    if (oneKeyword !== "") {
        for (const key of keyboardMap[oneKeyword]) {
            if (key !== "p-keyboard__key--lshift") {
                document
                    .getElementById(key)
                    .classList.remove("p-keyboard__active");
            }
        }
        for (const key of fingerMap[oneKeyword]) {
            document.getElementById(key).classList.remove("p-finger__active");
        }
    }
    oneKeyword = untypedTwoArr[0][0].substring(0, 1);
    for (const key of keyboardMap[oneKeyword]) {
        if (key !== "p-keyboard__key--lshift") {
            document.getElementById(key).classList.add("p-keyboard__active");
        }
    }
    for (const key of fingerMap[oneKeyword]) {
        document.getElementById(key).classList.add("p-finger__active");
    }
    wordSound();
}
function restartBtnClick() {
    endFlag = false;
    spaceStartFlag = false;
    typingFlag = false;
    restartFlg = true;
    if (!maetugiBtnFlg) {
        mae.classList.add("hidden");
        tugi.classList.add("hidden");
        startBtn.classList.add("hidden");
    } else {
        mae.classList.remove("hidden");
        tugi.classList.remove("hidden");
        startBtn.classList.remove("hidden");
    }
    firstAudio = null;
    startBtn.disabled = false;
    startBtn.textContent = "はじめる";
    startBtn.classList.remove("p-typingscreen__start__important");
    keyboardSpace.classList.remove("p-keyboard__active");
    optionContainer.classList.remove("hidden");
    beforeTypingStartArea.classList.remove("hidden");
    typingWordAreaBgBlack.classList.add("hidden");
    countDown.classList.add("hidden");
    typingResultModal.classList.add("hidden");
    const fingerActive = document.querySelector(".p-finger__active");
    if (fingerActive) {
        fingerActive.classList.remove("p-finger__active");
    }
    const keyboardActive = document.querySelector(".p-keyboard__active");
    if (keyboardActive) {
        keyboardActive.classList.remove("p-keyboard__active");
    }
}
window.addEventListener(
    "keydown",
    (e) => {
        if (e.repeat) {
            return false;
        }
        argKeyCode = e.code;
        if (typingFlag && e.key === "Tab") {
            e.preventDefault();
            return false;
        }
        if (argKeyCode === "Space") {
            e.preventDefault();
        }
        if (keyValue.includes(e.key) || argKeyCode === "Space") {
            if (spaceStartFlag && !typingFlag) {
                if (e.code == "Space") {
                    spaceStartFlag = false;
                    startTypingFunc();
                }
            }
            if (typingFlag) {
                keydownAllCnt += 1;
                let arrSub = untypedTwoArr[untypedCnt].filter(
                    (element) =>
                        element.substring(0, 1).toLowerCase() ==
                        e.key.toLowerCase()
                );
                if (arrSub.length > 0) {
                    untypedTwoArr[untypedCnt] = arrSub;
                }
                let passIndex = untypedTwoArr[untypedCnt].findIndex(
                    (element) =>
                        element.substring(0, 1).toLowerCase() ==
                        e.key.toLowerCase()
                );
                untypedTwoArr[untypedCnt].forEach((val, index) => {
                    if (
                        e.key.toLowerCase() ===
                        untypedTwoArr[untypedCnt][index]
                            .substring(0, 1)
                            .toLowerCase()
                    ) {
                        typedTwoArr[untypedCnt][index] += untypedTwoArr[
                            untypedCnt
                        ][index].substring(0, 1);
                        untypedTwoArr[untypedCnt][index] =
                            untypedTwoArr[untypedCnt][index].slice(1);
                    }
                });
                if (passIndex > -1) {
                    typingShowField.innerHTML = "";
                    typedField.textContent = "";
                    untypedField.textContent = "";
                    for (val of typedTwoArr) {
                        typingShowField.innerHTML +=
                            '<span class="typed">' + val[passIndex] + "</span>";
                        typedField.textContent += val[passIndex];
                    }
                    for (val of untypedTwoArr) {
                        if (alphabet == "yes") {
                            typingShowField.innerHTML +=
                                '<span class="untyped">' +
                                val[passIndex] +
                                "</span>";
                        }
                        untypedField.textContent += val[passIndex];
                    }
                    if (untypedTwoArr[untypedCnt][passIndex].length == 0) {
                        untypedCnt += 1;
                    }
                    if (untypedField.textContent !== "") {
                        for (const key of keyboardMap[oneKeyword]) {
                            if (key !== "p-keyboard__key--lshift") {
                                document
                                    .getElementById(key)
                                    .classList.remove("p-keyboard__active");
                            }
                        }
                        for (const key of fingerMap[oneKeyword]) {
                            document
                                .getElementById(key)
                                .classList.remove("p-finger__active");
                        }
                        oneKeyword = untypedField.textContent.substring(0, 1);
                        for (const key of keyboardMap[oneKeyword]) {
                            if (key !== "p-keyboard__key--lshift") {
                                document
                                    .getElementById(key)
                                    .classList.add("p-keyboard__active");
                            }
                        }
                        for (const key of fingerMap[oneKeyword]) {
                            document
                                .getElementById(key)
                                .classList.add("p-finger__active");
                        }
                    }
                    successType += 1;
                    successtypeField.textContent = "入力: " + successType;
                    untypedField.classList.remove(
                        "p-typingscreen__romaji__untyped__miss"
                    );
                } else {
                    missType += 1;
                    misstypeField.textContent = "ミス: " + missType;
                    untypedField.classList.add(
                        "p-typingscreen__romaji__untyped__miss"
                    );
                    if (sound !== "no") {
                        if (missType % 100 == 0) {
                            alert(
                                "連続で100回以上間違えると音がでなくなります。コース一覧ページにもどってください。"
                            );
                            return false;
                        }
                        missSound();
                    }
                }
                if (untypedField.textContent === "") {
                    if (keywordOrder.length !== 0) {
                        nextKeywordFunc();
                    } else {
                        finishType();
                    }
                }
            }
            if (
                endFlag == true &&
                e.code == "Space" &&
                typeof isContest == "undefined"
            ) {
                restartBtnClick();
            }
        }
    },
    { passive: false }
);
function finishType() {
    typingFlag = false;
    endFlag = true;
    endDate = new Date();
    typingscore();
    document.getElementById("p-typingresult--score").textContent =
        resultypingScore;
    document.getElementById("p-typingresult--time").textContent = typingTime;
    document.getElementById("p-typingresult--all").textContent = keydownAllCnt;
    document.getElementById("p-typingresult--miss").textContent = missType;
    if (honban) {
        document.querySelector('input[name="typingresult--score"]').value =
            resultypingScore;
        document.querySelector('input[name="typingresult--time"]').value =
            typingTime;
        //document.querySelector('input[name="typingresult--all"]').value =
        keydownAllCnt;
        //document.querySelector('input[name="typingresult--miss"]').value =
        missType;
        document.querySelector('input[name="course_name"]').value =
            document.querySelector(".active").textContent;
        document.querySelector('input[name="course_id"]').value =
            currentCourseId;
    }
    typingResultModal.classList.remove("hidden");
}
