/****************************************
* ボタン押下
*****************************************/
let btn = document.getElementById("all_btn");
btn.addEventListener('click', () => {
    setTimeout(function () {
        btn.disabled = true;
        btn.value = 'データ取得中...';
    }, 100);
});
let btn2 = document.getElementById("all_reset_btn");
btn2.addEventListener('click', () => {
    setTimeout(function () {
        btn2.disabled = true;
        btn2.value = 'キャンセル中...';
    }, 100);
});

