"use strict";
document.addEventListener("DOMContentLoaded", function () {
    let contest_typingtest_caution_btn = document.getElementById(
        "contest_typingtest_caution_btn"
    );
    let contest_btn_event = new Event("click", { bubbles: true });
    contest_typingtest_caution_btn.dispatchEvent(contest_btn_event);
});
const userinfo_cxl_btn = document.getElementById("userinfo_cxl_btn");
userinfo_cxl_btn.addEventListener("click", () => {
    userinfo_cxl_btn.disabled = true;
    userinfo_cxl_btn.textContent = "やりなおし中...";
    setTimeout(function () {
        userinfo_cxl_btn.disabled = false;
    }, 5000);
});
const start_cxl_btn = document.getElementById("start_cxl_btn");
start_cxl_btn.addEventListener("click", () => {
    start_cxl_btn.disabled = true;
    start_cxl_btn.textContent = "やりなおし中...";
    setTimeout(function () {
        start_cxl_btn.disabled = false;
    }, 5000);
});
const btnarrow = document.getElementById("btnarrow");
let okbtn = document.getElementById("contest_typingtest_ok");
okbtn.addEventListener("click", () => {
    for (let i = 0; i < 6; i++) {
        setTimeout(function () {
            btnarrow.classList.toggle("hidden");
        }, 400 * i);
    }
});
const result_close_btn = document.getElementById("typing_store_close");
result_close_btn.addEventListener("click", () => {
    result_close_btn.textContent = "保存中...";
    setTimeout(function () {
        result_close_btn.disabled = true;
    }, 200);
});
