"use strict";
const beforeTypingStartArea = document.getElementById(
    "p-typingscreen__startscreen"
);
const typingWordAreaBgBlack = document.getElementById("p-typingscreen");
const soundBtn = document.getElementById("p-typingscreen__startscreen__sound");
const nosoundBtn = document.getElementById(
    "p-typingscreen__startscreen__nosound"
);
const typedField = document.getElementById("p-typed");
const untypedField = document.getElementById("p-untyped");
const typingShowField = document.getElementById("p-typing-show");
const kanaField = document.getElementById("p-kana");
const manaField = document.getElementById("p-mana");
const successtypeField = document.getElementById("p-successtype");
const misstypeField = document.getElementById("p-misstype");

let endFlag = false;
let inputTypeKey = "";
let typingTime = 0;
let romanMap;
let val;
let val1;
let val2;
let val3;
let typingScore;
let resultypingScore;
let soundBtnChecked;
let keywordOrder = [];
let keyword = "";
let keywordArr = [];
let untypedTwoArr = [];
let typedTwoArr = [];
let untypedCnt = 0;
let keydownAllCnt = 0;
let successType = 0;
let missType = 0;
let oneKeyword = "";
let beginDate;
let endDate;
let typingTimeResult = 0.0;
let sendFlag = true;
let argKeyCode = "";
let challengeBtnCheckedFlg = false;
let firstAudio = null;
let spaceStartFlag = false;
const keyValue = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0",
    "-",
    "^",
    "¥",
    "!",
    '"',
    "#",
    "$",
    "%",
    "&",
    "'",
    "(",
    ")",
    "=",
    "~",
    "|",
    "q",
    "w",
    "e",
    "r",
    "t",
    "y",
    "u",
    "i",
    "o",
    "p",
    "@",
    "[",
    "Q",
    "W",
    "E",
    "R",
    "T",
    "Y",
    "U",
    "I",
    "O",
    "P",
    "`",
    "{",
    "a",
    "s",
    "d",
    "f",
    "g",
    "h",
    "j",
    "k",
    "l",
    ";",
    ":",
    "]",
    "A",
    "S",
    "D",
    "F",
    "G",
    "H",
    "J",
    "K",
    "L",
    "+",
    "*",
    "}",
    "z",
    "x",
    "c",
    "v",
    "b",
    "n",
    "m",
    ",",
    ".",
    "/",
    "_",
    "Z",
    "X",
    "C",
    "V",
    "B",
    "N",
    "M",
    "<",
    ">",
    "?",
];
