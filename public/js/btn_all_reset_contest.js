/****************************************
 * click the button
 *****************************************/
const all_btn = document.getElementById("all_btn");
all_btn.addEventListener("click", () => {
    setTimeout(function () {
        all_btn.disabled = true;
        all_btn.textContent = "処理中……";
    }, 100);
});
if (document.getElementById("cancel_btn") !== null) {
    let all_reset_btn = document.getElementById("cancel_btn");
    all_reset_btn.addEventListener("click", () => {
        setTimeout(function () {
            all_reset_btn.disabled = true;
            all_reset_btn.textContent = "キャンセル中……";
        }, 100);
    });
}
const excelbtn = document.getElementById("excelbtn");
excelbtn.addEventListener("click", () => {
    setTimeout(function () {
        excelbtn.disabled = true;
        excelbtn.value = "ダウンロードの準備中..........";
    }, 100);
    setTimeout(function () {
        excelbtn.disabled = false;
        excelbtn.value = "データの一括ダウンロード(Excel)";
    }, 4000);
});
