"use strict";
const typeField = document.getElementById("p-typefield");
const typenumberField = document.getElementById("p-typenumber");
const timeField = document.getElementById("p-time");
const timeField_init = document.getElementById("p-initial-time");
const optionBtnContainer = document.getElementById(
    "p-typingscreen__startscreen__optionbtn__container"
);
const typeResultField = document.getElementById("p-typeresultfield");
const optionToggleBtn = document.getElementById(
    "p-typingscreen__startscreen__lower"
);
const typingArea = document.getElementById("p-typingtestscreen");
const typingAreForStart = document.getElementById(
    "p-typingtestscreen__startscreen"
);
const keytouchRankInfoBtn = document.getElementById("keytouchRankInfoBtn");
const tab_course_index = document.getElementsByName("tab_course_index");
const keytouchCutionBtn = document.getElementById("keytouchCutionBtn");
const startBtn = document.getElementById("p-typingtestscreen__start");
let contest_typingtest_ok = document.getElementById("contest_typingtest_ok");

let countDownSetTimeout;
let afterthreeMinStartSetTimeout;
let typingInterval;
let typingTime = courseData.test_time;
let resultModalTestTime = courseData.test_time;
let keywords;
let keywordArr = [];
let typeOrderArr = 0;
let keyword = "";
let successType = 0;
const codeSpace = { Space: "&nbsp;" };
let translateInt = 0;
let optionBtnChecked;
let inputTypeKey = "";
let textContent = "";
let argKeyCode = "";
let min;
let sec;
let scoreRank = null;
const perfect = document.getElementById("perfect");
const keyValue = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0",
    "-",
    "^",
    "¥",
    "!",
    '"',
    "#",
    "$",
    "%",
    "&",
    "'",
    "(",
    ")",
    "=",
    "~",
    "|",
    "q",
    "w",
    "e",
    "r",
    "t",
    "y",
    "u",
    "i",
    "o",
    "p",
    "@",
    "[",
    "Q",
    "W",
    "E",
    "R",
    "T",
    "Y",
    "U",
    "I",
    "O",
    "P",
    "`",
    "{",
    "a",
    "s",
    "d",
    "f",
    "g",
    "h",
    "j",
    "k",
    "l",
    ";",
    ":",
    "]",
    "A",
    "S",
    "D",
    "F",
    "G",
    "H",
    "J",
    "K",
    "L",
    "+",
    "*",
    "}",
    "z",
    "x",
    "c",
    "v",
    "b",
    "n",
    "m",
    ",",
    ".",
    "/",
    "_",
    "Z",
    "X",
    "C",
    "V",
    "B",
    "N",
    "M",
    "<",
    ">",
    "?",
];
getCurrentCourseData();
min = Math.floor(typingTime / 60);
sec = typingTime % 60;
timeField_init.textContent = min + "分 " + sec + "秒";
function startTypingFunc() {
    maetugiBtnFlg == true;
    restartFlg = false;
    restartBtn.classList.add("hidden");
    mae.classList.add("hidden");
    tugi.classList.add("hidden");

    typeOrderArr = 0;
    translateInt = 0;
    successType = 0;
    getCurrentWordData();
    keywords = typeWords[0].word_mana;
    startBtn.classList.add("hidden");
    countDown.classList.remove("hidden");
    optionBtnContainer.classList.add("hidden");
    optionBtnChecked = optionToggleBtn.checked;
    if (!optionBtnChecked) {
        keywords = keywords.toUpperCase();
    }
    for (let i = 0; i <= countSeconds; i++) {
        countDownSetTimeout = setTimeout(() => {
            countDown.textContent = countSeconds - i;
        }, i * 1000);
    }
    afterthreeMinStartSetTimeout = setTimeout(() => {
        typingAreForStart.classList.add("hidden");
        typingArea.classList.remove("hidden");
        if (honban == false) {
            restartBtn.classList.remove("hidden");
        }
        initTypingFunc();
    }, countSeconds * 1000);
}
function initTypingFunc() {
    typenumberField.textContent = successType;
    showTime(timeField);
    nextKeywordFunc();
    typingFlag = true;
    startTypingTimer();
}
function startTypingTimer() {
    typingInterval = setInterval(() => {
        typingTime--;
        showTime(timeField);
        if (typingTime == 0) {
            finishType();
        }
    }, 1000);
}
function nextKeywordFunc() {
    typeField.textContent = "";
    typeResultField.textContent = "";
    keywordArr = [...keywords];
    for (let i = 0; i < keywordArr.length; i++) {
        let element = document.createElement("span");
        let resultElement = document.createElement("span");
        if (keywordArr[i] == " ") {
            element.innerHTML = "&nbsp;";
            resultElement.innerHTML = "&nbsp;";
        } else {
            element.textContent = keywordArr[i];
            resultElement.textContent = keywordArr[i];
        }
        typeField.appendChild(element);
        typeResultField.appendChild(resultElement);
    }
    typeField.children[typeOrderArr].classList.add(
        "p-typingtestscreen__untyped"
    );
}
window.addEventListener(
    "keydown",
    (e) => {
        argKeyCode = e.code;
        if (typingFlag && e.key === "Tab") {
            e.preventDefault();
            return false;
        }
        if (argKeyCode === "Space") {
            e.preventDefault();
        }
        if (keyValue.includes(e.key) || argKeyCode === "Space") {
            if (typingFlag) {
                textContent =
                    typeField.children[typeOrderArr].textContent.toLowerCase();
                inputTypeKey = e.key.toLowerCase();
                if (
                    inputTypeKey === textContent ||
                    codeSpace[argKeyCode] ==
                        typeField.children[typeOrderArr].innerHTML
                ) {
                    if (
                        typeField.children[typeOrderArr].classList.contains(
                            "p-typingtestscreen__untyped"
                        )
                    ) {
                        typeField.children[typeOrderArr].classList.remove(
                            "p-typingtestscreen__untyped"
                        );
                    } else {
                        typeField.children[typeOrderArr].classList.remove(
                            "p-typingtestscreen__misstyped"
                        );
                    }
                    typeField.children[typeOrderArr].classList.add(
                        "p-typingtestscreen__typed"
                    );
                    typeResultField.children[typeOrderArr].classList.add(
                        "p-typingtestresultscreen__typed"
                    );
                    successType += 1;
                    typeOrderArr += 1;
                    typenumberField.textContent = successType;
                    if (typeOrderArr % 30 == 0) {
                        translateInt += 2;
                        for (let i = 0; i < typeField.children.length; i++) {
                            typeField.children[i].style.transform =
                                "translateY(-" + translateInt + "rem)";
                        }
                    }

                    if (
                        document.querySelectorAll(".p-typingtestscreen__typed")
                            .length === keywordArr.length
                    ) {
                        finishType();
                    } else {
                        typeField.children[typeOrderArr].classList.add(
                            "p-typingtestscreen__untyped"
                        );
                    }
                } else {
                    if (
                        typeField.children[typeOrderArr].classList.contains(
                            "p-typingtestscreen__untyped"
                        )
                    ) {
                        typeField.children[typeOrderArr].classList.remove(
                            "p-typingtestscreen__untyped"
                        );
                        typeField.children[typeOrderArr].classList.add(
                            "p-typingtestscreen__misstyped"
                        );
                        typeResultField.children[typeOrderArr].classList.add(
                            "p-typingtestscreen__misstyped"
                        );
                    }
                }
            }
        }
    },
    { passive: false }
);

function restartBtnClick() {
    optionBtnContainer.classList.remove("hidden");
    if (!maetugiBtnFlg) {
        mae.classList.add("hidden");
        tugi.classList.add("hidden");
        startBtn.classList.add("hidden");
    } else {
        mae.classList.remove("hidden");
        tugi.classList.remove("hidden");
        startBtn.classList.remove("hidden");
    }
    typingFlag = false;
    restartFlg = true;
    clearInterval(typingInterval);
    clearTimeout(countDownSetTimeout);
    clearTimeout(afterthreeMinStartSetTimeout);
    typingAreForStart.classList.remove("hidden");
    typingArea.classList.add("hidden");
    countDown.classList.add("hidden");
    typingResultModal.classList.add("hidden");
    typingTime = courseList[currentCourseIdKey]["test_time"];
    startBtn.classList.remove("hidden");
    showTime(timeField_init);
}

function resultMessage(CURRENTCOURSEID, SUCCESSTYPE1, SUCCESSTYPE2) {
    if (currentCourseId == CURRENTCOURSEID) {
        if (successType == SUCCESSTYPE1) {
            perfect.textContent = "パーフェクト！";
        } else if (successType > SUCCESSTYPE2) {
            perfect.textContent = "すばらしい！";
        }
    }
}
function finishType() {
    typingFlag = false;
    clearInterval(typingInterval);
    resultModalTestTime = courseData.test_time;
    document.getElementById("p-typingresult--score").textContent = successType;
    perfect.textContent = "";
    resultMessage(619, 7, 5);
    resultMessage(320, 300, 199);
    resultMessage(321, 300, 199);
    resultMessage(322, 350, 249);
    resultMessage(323, 400, 299);
    resultMessage(324, 500, 399);
    resultMessage(325, 500, 399);
    if (resultModalTestTime == 300) {
        if (successType == 500) {
            perfect.textContent = "パーフェクト！";
        } else if (successType > 399) {
            perfect.textContent = "すばらしい！";
        }
    }
    if (resultModalTestTime == 600) {
        if (successType == 2000) {
            perfect.textContent = "あなたは神レベル！";
        } else if (successType > 1499) {
            perfect.textContent = "タイピングマスター！";
        }
    }
    min = Math.floor(typingTime / 60);
    sec = typingTime % 60;
    document.getElementById("p-typingresult--time").textContent =
        min + "分 " + sec + "秒";
    if (honban) {
        document.querySelector('input[name="typingresult--score"]').value =
            successType;

        document.querySelector('input[name="typingresult--time"]').value =
            resultModalTestTime;
        document.querySelector('input[name="course_name"]').value =
            document.querySelector(".active").textContent;
        document.querySelector('input[name="course_id"]').value =
            currentCourseId;
    }
    typingResultModal.classList.remove("hidden");
}
if (contest_typingtest_ok !== null) {
    contest_typingtest_ok.addEventListener("click", () => {
        mae.classList.add("hidden");
        tugi.classList.add("hidden");
    });
}
