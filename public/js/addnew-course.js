/****************************************
* 新規ボタン押下で1行追加
*****************************************/
let add = document.getElementById("add");

document.addEventListener("DOMContentLoaded", function(){
    add.addEventListener('click', () => {
        let insert_btn = document.getElementById("insert_btn");
        if(insert_btn == null){
            let insert_btn = document.getElementById("insert_btn");
            let editbtn = document.getElementsByName("edit");
            let s_btn = document.getElementsByName("save");
            let del_checks = document.getElementsByName("checks[]");
            let edit_cancel = document.getElementsByName("editcancel");
            let s_invalid = document.getElementsByName("invalid");
            let s_display_order = document.getElementsByName("display_order");
            let s_course_name = document.getElementsByName("course_name");
            let s_is_random = document.getElementsByName("is_random");
        
            for(i = 0; i < editbtn.length; i++) {
                editbtn[i].disabled = true;
                s_btn[i].disabled = true;
                del_checks[i].disabled = true;
                edit_cancel[i].disabled = true;
                s_invalid[i].disabled = true;
                s_is_random[i].disabled = true;
            }
            var tableElem = document.getElementById('mainTable');
            var csrf = document.querySelector('meta[name="csrf-token"]').content;
            var trElem = tableElem.insertRow(1);
            trElem.style.backgroundColor = "#DDDDFF";
            trElem.id = 'add_new_tr';
            var form = document.createElement('form');
            form.action = './add-course-new';
            form.id = 'insert_btn';
            form.method = 'POST';
            form.enctype = 'multipart/form-data';
            trElem.append(form);

            let csrfinput = document.createElement('input');
            csrfinput.setAttribute('name','_token');
            csrfinput.setAttribute('value',csrf);
            csrfinput.setAttribute('type','hidden');
            form.appendChild(csrfinput);

            let hdn_school = document.getElementById('school_id');
            var qi = document.createElement('input');
            qi.type = 'hidden';
            qi.name = 'school_id';
            form.appendChild(qi);

            let hdn_course = document.getElementById('course_id');
            var qi2 = document.createElement('input');
            qi2.type = 'hidden';
            qi2.name = 'course_id';
            form.appendChild(qi2);

            var cellElem1 = trElem.insertCell(0);
            var cellElem2 = trElem.insertCell(0);
            var cellElem3 = trElem.insertCell(0);
            var cellElem4 = trElem.insertCell(0);
            var cellElem5 = trElem.insertCell(0);
            var cellElem6 = trElem.insertCell(0);
            var cellElem7 = trElem.insertCell(0);
            var cellElem8 = trElem.insertCell(0);
            cellElem1.style = "border: 1px solid white";
            cellElem2.style = "border: 1px solid white";
            cellElem3.style = "border: 1px solid white";
            cellElem4.style = "border: 1px solid white";
            cellElem5.style = "border: 1px solid white";
            cellElem6.style = "border: 1px solid white";
            cellElem7.style = "border: 1px solid white";
            cellElem8.style = "border: 1px solid white";

            cellElem1.appendChild(document.createTextNode(''));
            cellElem2.appendChild(document.createTextNode(''));

            let button = document.createElement('input');
            button.setAttribute('type','submit');
            button.setAttribute('name','update');
            button.setAttribute('id','update_id');
            button.setAttribute('form','insert_btn');
            button.setAttribute('value','保存');
            button.setAttribute('class','btn btn-secondary btn-sm');
            button.setAttribute('onclick','wait_for()');
            cellElem3.appendChild(button);

            let button_cancel = document.createElement('input');
            button_cancel.setAttribute('type','button');
            button_cancel.setAttribute('name','cancel');
            button_cancel.setAttribute('id','cancel_id');
            button_cancel.setAttribute('form','insert_btn');
            button_cancel.setAttribute('value','ｷｬﾝｾﾙ');
            button_cancel.setAttribute('class','btn btn-secondary btn-sm');
            cellElem3.appendChild(button_cancel); 

            let checkbox1 = document.createElement('input');
            checkbox1.setAttribute('type','checkbox');
            checkbox1.setAttribute('name','is_random');
            checkbox1.setAttribute('value',1);
            checkbox1.setAttribute('form','insert_btn');
            checkbox1.className = 'form-check-input';
            cellElem4.appendChild(checkbox1);

            var input1 = document.createElement('input');
            input1.name = 'course_name';
            input1.type = 'text';
            input1.className = 'form-control col-xs-2';
            input1.maxLength = '30';
            input1.setAttribute('form','insert_btn');
            cellElem5.appendChild(input1);

            var input2 = document.createElement('input');
            input2.name = 'display_order';
            input2.type = 'text';
            input2.className = 'form-control col-xs-2';
            input2.maxLength = '4';
            input2.setAttribute('form','insert_btn');
            cellElem6.appendChild(input2);

            let checkbox2 = document.createElement('input');
            checkbox2.setAttribute('type','checkbox');
            checkbox2.setAttribute('name','invalid');
            checkbox2.setAttribute('value',1);
            checkbox2.setAttribute('form','insert_btn');
            checkbox2.className = 'form-check-input';
            cellElem7.appendChild(checkbox2);
            
            let new_cancel = document.getElementById("cancel_id");
            let new_save = document.getElementById("update_id");
            new_save.addEventListener('click', () => {
                new_cancel.disabled = true;
                new_save.value = '保存中...';
                setTimeout(function () {
                    new_save.disabled = true; 
                }, 100);
            })
    
            let add_new_tr = document.getElementById('add_new_tr');
            new_cancel.addEventListener('click', () => {
            add_new_tr.remove();
            for(i = 0; i < editbtn.length; i++) {
                editbtn[i].disabled = false
            }
            })
        };
        
    });
}, false);