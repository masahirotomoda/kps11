/****************************************
* flash message
*****************************************/
window.onload=function(){
    let flashmessage=document.getElementById('flash_message');
    if( flashmessage != null){
    setTimeout("document.getElementById('flash_message').style.visibility ='hidden'",6000);
}}

/****************************************
* validation modal error message
*****************************************/
document.addEventListener('DOMContentLoaded', (event) => {
    var alertmodal = document.getElementById('alertmodal') ;
    var alertbtn = document.getElementById('alertbtn') ;
    var is_error=   document.getElementById('is_error') ;
    if(is_error.value == 'yes'){
        let event = new Event("click", {bubbles: true});
        alertbtn.dispatchEvent(event);
        alertmodal.style.display ="block"; 
    }
});



