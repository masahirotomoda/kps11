"use strict";

function returnRank(successType) {
  switch (true) {
    case successType < 50:
      return "20級"
    case successType < 100:
      return "19級"
    case successType < 150:
      return "18級"
    case successType < 200:
      return "17級"
    case successType < 250:
      return "16級"
    case successType < 300:
      return "15級"
    case successType < 350:
      return "14級"
    case successType < 400:
      return "13級"
    case successType < 450:
      return "12級"
    case successType < 500:
      return "11級"
    case successType < 550:
      return "10級"
    case successType < 600:
      return "9級"
    case successType < 650:
      return "8級"
    case successType < 700:
      return "7級"
    case successType < 750:
      return "6級"
    case successType < 800:
      return "5級"
    case successType < 850:
      return "4級"
    case successType < 900:
      return "3級"
    case successType < 950:
      return "2級"
    case successType < 1000:
      return "1級"
    case successType < 1050:
      return "1段"
    case successType < 1100:
      return "2段"
    case successType < 1150:
      return "3段"
    case successType < 1200:
      return "4段"
    case successType < 1250:
      return "5段"
    case successType < 1300:
      return "6段"
    case successType < 1350:
      return "7段"
    case successType < 1400:
      return "8段"
    case successType < 1450:
      return "9段"
    case successType < 1500:
      return "10段"
    case successType < 1550:
      return "11段"
    case successType < 1600:
      return "12段"
    case successType < 1650:
      return "13段"
    case successType < 1700:
      return "14段"
    case successType < 1750:
      return "15段"
    case successType < 1800:
      return "16段"
    case successType < 1850:
      return "17段"
    case successType < 1900:
      return "18段"
    case successType < 1950:
      return "19段"
    case successType < 2000:
      return "20段"
    case 2000 <= successType:
      return "マスター"
    default:
      return "20級"
  }
}
function clearRankBg() {
  document.getElementById('20級').classList.remove('p-keyboard__active');
  document.getElementById('19級').classList.remove('p-keyboard__active');
  document.getElementById('18級').classList.remove('p-keyboard__active');
  document.getElementById('17級').classList.remove('p-keyboard__active');
  document.getElementById('16級').classList.remove('p-keyboard__active');
  document.getElementById('15級').classList.remove('p-keyboard__active');
  document.getElementById('14級').classList.remove('p-keyboard__active');
  document.getElementById('13級').classList.remove('p-keyboard__active');
  document.getElementById('12級').classList.remove('p-keyboard__active');
  document.getElementById('11級').classList.remove('p-keyboard__active');
  document.getElementById('10級').classList.remove('p-keyboard__active');
  document.getElementById('9級').classList.remove('p-keyboard__active');
  document.getElementById('8級').classList.remove('p-keyboard__active');
  document.getElementById('7級').classList.remove('p-keyboard__active');
  document.getElementById('6級').classList.remove('p-keyboard__active');
  document.getElementById('5級').classList.remove('p-keyboard__active');
  document.getElementById('4級').classList.remove('p-keyboard__active');
  document.getElementById('3級').classList.remove('p-keyboard__active');
  document.getElementById('2級').classList.remove('p-keyboard__active');
  document.getElementById('1級').classList.remove('p-keyboard__active');
  document.getElementById('1段').classList.remove('p-keyboard__active');
  document.getElementById('2段').classList.remove('p-keyboard__active');
  document.getElementById('3段').classList.remove('p-keyboard__active');
  document.getElementById('4段').classList.remove('p-keyboard__active');
  document.getElementById('5段').classList.remove('p-keyboard__active');
  document.getElementById('6段').classList.remove('p-keyboard__active');
  document.getElementById('7段').classList.remove('p-keyboard__active');
  document.getElementById('8段').classList.remove('p-keyboard__active');
  document.getElementById('9段').classList.remove('p-keyboard__active');
  document.getElementById('10段').classList.remove('p-keyboard__active');
  document.getElementById('11段').classList.remove('p-keyboard__active');
  document.getElementById('12段').classList.remove('p-keyboard__active');
  document.getElementById('13段').classList.remove('p-keyboard__active');
  document.getElementById('14段').classList.remove('p-keyboard__active');
  document.getElementById('15段').classList.remove('p-keyboard__active');
  document.getElementById('16段').classList.remove('p-keyboard__active');
  document.getElementById('17段').classList.remove('p-keyboard__active');
  document.getElementById('18段').classList.remove('p-keyboard__active');
  document.getElementById('19段').classList.remove('p-keyboard__active');
  document.getElementById('20段').classList.remove('p-keyboard__active');
  document.getElementById('マスター').classList.remove('p-keyboard__active');
}