/****************************************
 * PDF output
 *****************************************/
let pdf = document.getElementById("pdf");
const pdf_basic = document.getElementById("pdf_basic");
const pdf_yusyou = document.getElementById("pdf_yusyou");
const pdf_junyusyou = document.getElementById("pdf_junyusyou");
const pdf_kin = document.getElementById("pdf_kin");
const pdf_gin = document.getElementById("pdf_gin");
const pdf_dou = document.getElementById("pdf_dou");
const pdf_ganbatta = document.getElementById("pdf_ganbatta");
const pdf_1 = document.getElementById("pdf_1");
const pdf_2 = document.getElementById("pdf_2");
const pdf_3 = document.getElementById("pdf_3");
const pdf_4 = document.getElementById("pdf_4");
const pdf_5 = document.getElementById("pdf_5");
const pdf_6 = document.getElementById("pdf_6");
const pdf_7 = document.getElementById("pdf_7");
const pdf_8 = document.getElementById("pdf_8");
const pdf_9 = document.getElementById("pdf_9");
const pdf_10 = document.getElementById("pdf_10");

function pdfoutput($target) {
    let printbtn = document.getElementsByName("checks[]");
    let is_print = null;
    for (i = 0; i < printbtn.length; i++) {
        if (printbtn[i].checked === true) {
            is_print = "yes";
        }
    }
    if (is_print == "yes") {
        pdf.target = "_blank";
    } else {
        pdf.target = "_self";
        setTimeout(function () {
            $target.disabled = true;
            $target.innerHTML = "PDFプリント準備中...";
        }, 100);
        setTimeout(function () {
            $target.disabled = false;
            $target.value = "金メダル";
        }, 6000);
    }
}
pdf_basic.addEventListener("click", () => {
    $target = pdf_basic;
    pdfoutput($target);
});
pdf_yusyou.addEventListener("click", () => {
    $target = pdf_yusyou;
    pdfoutput($target);
});
pdf_junyusyou.addEventListener("click", () => {
    $target = pdf_junyusyou;
    pdfoutput($target);
});
pdf_kin.addEventListener("click", () => {
    $target = pdf_kin;
    pdfoutput($target);
});
pdf_gin.addEventListener("click", () => {
    $target = pdf_gin;
    pdfoutput($target);
});
pdf_dou.addEventListener("click", () => {
    $target = pdf_dou;
    pdfoutput($target);
});
pdf_ganbatta.addEventListener("click", () => {
    $target = pdf_ganbatta;
    pdfoutput($target);
});
pdf_1.addEventListener("click", () => {
    $target = pdf_1;
    pdfoutput($target);
});
pdf_2.addEventListener("click", () => {
    $target = pdf_2;
    pdfoutput($target);
});
pdf_3.addEventListener("click", () => {
    $target = pdf_3;
    pdfoutput($target);
});
pdf_4.addEventListener("click", () => {
    $target = pdf_4;
    pdfoutput($target);
});
pdf_5.addEventListener("click", () => {
    $target = pdf_5;
    pdfoutput($target);
});
pdf_6.addEventListener("click", () => {
    $target = pdf_6;
    pdfoutput($target);
});
pdf_7.addEventListener("click", () => {
    $target = pdf_7;
    pdfoutput($target);
});
pdf_8.addEventListener("click", () => {
    $target = pdf_8;
    pdfoutput($target);
});
pdf_9.addEventListener("click", () => {
    $target = pdf_9;
    pdfoutput($target);
});
pdf_10.addEventListener("click", () => {
    $target = pdf_10;
    pdfoutput($target);
});
