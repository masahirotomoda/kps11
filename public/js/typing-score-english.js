"use strict";
let speedPoint;
let misstypePoint;
let OneMojiTime;
let scoreOneMoji;
let scoreMoji;
let scoreSpeed;
let scoreAllSpeed;
let scoreSpeedRate;
function typingscore() {
    typingTimeResult =
        Math.round((endDate.getTime() - beginDate.getTime()) / 10) / 100;

    typingTime = typingTimeResult.toFixed(1);

    if (courseData.tab == "battle") {
        typingTime = typingTimeResult.toFixed(1);
        speedPoint = 500;
        misstypePoint = 500;
        scoreOneMoji = misstypePoint / successType;
        if (successType - missType < 0) {
            scoreMoji = 0;
        } else {
            scoreMoji = scoreOneMoji * (successType - missType);
        }
        OneMojiTime = 0.225;
        scoreAllSpeed = OneMojiTime * successType;
        scoreSpeedRate = scoreAllSpeed / typingTime;
        if (scoreSpeedRate >= 1) {
            scoreSpeed = speedPoint;
        } else {
            scoreSpeed = speedPoint * scoreSpeedRate;
        }
        typingScore = Math.ceil(scoreMoji + scoreSpeed);
        resultypingScore = typingScore;
    } else {
        if (successType > 199) {
            speedPoint = 60;
        } else if (successType > 100) {
            speedPoint = 50;
        } else {
            speedPoint = 40;
        }
        misstypePoint = 100 - speedPoint;
        scoreOneMoji = misstypePoint / successType;
        if (successType - missType < 0) {
            scoreMoji = 0;
        } else {
            scoreMoji = scoreOneMoji * (successType - missType);
        }
        if (scoretype == 0 || scoretype == 1) {
            OneMojiTime = 0.3;
        } else {
            OneMojiTime = 0.34;
        }
        scoreAllSpeed = OneMojiTime * successType;
        scoreSpeedRate = scoreAllSpeed / typingTime;
        if (scoreSpeedRate >= 1) {
            scoreSpeed = speedPoint;
        } else {
            scoreSpeed = speedPoint * scoreSpeedRate;
        }
        typingScore = Math.ceil(scoreMoji + scoreSpeed);
        resultypingScore = typingScore;
        if (typingScore == 100) {
            perfect.textContent = "パーフェクト！英語の神様！";
        } else if (typingScore > 89) {
            perfect.textContent = "すばらしい！英語の達人";
        } else if (typingScore > 69) {
            perfect.textContent = "Good♪さらに上を！！";
        } else if (typingScore > 59) {
            perfect.textContent = "この調子でがんばれ！";
        } else {
            perfect.textContent = "";
        }
    }
}
