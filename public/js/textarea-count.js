document.addEventListener(
    "DOMContentLoaded",
    function () {
        let counter = document.getElementById("word-count");
        let textarea = document.getElementById("word_mana");
        textarea.addEventListener("keyup", () => {
            counter.textContent = `残り${2000 - textarea.value.length}文字`;
        });
    },
    false
);
