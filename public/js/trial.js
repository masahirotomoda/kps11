/****************************************
 * trial account
 *****************************************/
let category = document.getElementById("category");
let industry = document.getElementById("industry");
let SubmitButton = document.getElementById("SubmitButton");
let confirm = document.getElementById("confirm");

document.addEventListener(
    "DOMContentLoaded",
    function () {
        if (category.value == "その他") {
            industry.disabled = false;
        } else {
            industry.disabled = true;
        }
        category.addEventListener("change", function () {
            if (category.value == "その他") {
                industry.disabled = false;
            } else {
                industry.disabled = true;
            }
        });
        confirm.addEventListener("click", () => {
            if (confirm.checked == true) {
                SubmitButton.disabled = false;
            } else {
                SubmitButton.disabled = true;
            }
        });
        SubmitButton.addEventListener("click", function () {
            SubmitButton.innerHTML = "トライアル情報作成中...";
            setTimeout(function () {
                SubmitButton.disabled = true;
            }, 100);
        });
    },
    false
);
