/****************************************
* student add button
*****************************************/
let add = document.getElementById("add");

document.addEventListener("DOMContentLoaded", function(){
  add.addEventListener('click', () => {
    let insert_btn = document.getElementById("insert_btn");
    if(insert_btn == null){
        let edit = document.getElementsByName("edit");
        let s_kumi = document.getElementsByName("kumi_id");
        let s_grade = document.getElementsByName("grade_id");
        let s_user = document.getElementsByName("student_name");
        let s_attendance = document.getElementsByName("attendance_no");
        let s_btn = document.getElementsByName("save");
        let edit_cancel = document.getElementsByName("editcancel");
        let del_checks = document.getElementsByName("checks[]");

        for(i = 0; i < edit.length; i++) {
            edit[i].disabled = true;
            s_kumi[i].disabled = true;
            s_grade[i].disabled = true;
            s_btn[i].disabled = true;
            edit_cancel[i].disabled = true;
        }

        var tableElem = document.getElementById('mainTable');
        var csrf = document.querySelector('meta[name="csrf-token"]').content;
        var trElem = tableElem.insertRow(1);
        trElem.style.backgroundColor = "#DDDDFF";
        trElem.id = 'add_new_tr';
        var form = document.createElement('form');
        form.action = './student-list-new';
        form.id = 'insert_btn';
        form.method = 'POST';
        trElem.append(form);

        let csrfinput = document.createElement('input');
        csrfinput.setAttribute('name','_token');
        csrfinput.setAttribute('value',csrf);
        csrfinput.setAttribute('type','hidden');
        form.appendChild(csrfinput);

        var cellElem0 = trElem.insertCell(0);
        var cellElem1 = trElem.insertCell(0);
        var cellElem2 = trElem.insertCell(0);
        var cellElem3 = trElem.insertCell(0);
        var cellElem4 = trElem.insertCell(0);
        var cellElem5 = trElem.insertCell(0);
        var cellElem6 = trElem.insertCell(0);
        var cellElem7 = trElem.insertCell(0);
        var cellElem8 = trElem.insertCell(0);
       
        cellElem0.style = "border: 1px solid white";
        cellElem1.style = "border: 1px solid white";
        cellElem2.style = "border: 1px solid white";
        cellElem3.style = "border: 1px solid white";
        cellElem4.style = "border: 1px solid white";
        cellElem5.style = "border: 1px solid white";
        cellElem6.style = "border: 1px solid white";
        cellElem7.style = "border: 1px solid white";
        cellElem8.style = "border: 1px solid white";

        cellElem0.appendChild(document.createTextNode(''));
        cellElem1.appendChild(document.createTextNode(''));
        cellElem2.appendChild(document.createTextNode(''));

        let button = document.createElement('input');
        button.setAttribute('type','submit');
        button.setAttribute('name','update');
        button.setAttribute('id','update_id');
        button.setAttribute('form','insert_btn');
        button.setAttribute('value','保存');
        button.setAttribute('class','btn btn-secondary btn-sm');
        cellElem3.appendChild(button);
    
        let button_cancel = document.createElement('input');
        button_cancel.setAttribute('type','button');
        button_cancel.setAttribute('name','cancel');
        button_cancel.setAttribute('id','cancel_id');
        button_cancel.setAttribute('form','insert_btn');
        button_cancel.setAttribute('value','ｷｬﾝｾﾙ');
        button_cancel.setAttribute('class','btn btn-secondary btn-sm');
        cellElem3.appendChild(button_cancel);        

        var input1 = document.createElement('input');
        input1.name = 'student_name';
        input1.type = 'text';
        input1.className = 'form-control col-xs-2';
        input1.maxLength = 20;
        input1.setAttribute('form','insert_btn');
        cellElem4.appendChild(input1);

        var input2 = document.createElement('input');
        input2.name = 'attendance_no';
        input2.type = 'text';
        input2.className = 'form-control col-xs-2';
        input2.maxLength = 4;
        input2.setAttribute('form','insert_btn');
        cellElem5.appendChild(input2);

        let select1 = document.getElementById('nd_kumi_selection');
        let select_insert1 = select1.cloneNode(true);
        select_insert1.setAttribute('name','kumi_id');
        select_insert1.setAttribute('form','insert_btn');
        cellElem6.appendChild(select_insert1);

        let select2 = document.getElementById('nd_grade_selection');
        let select_insert2 = select2.cloneNode(true);
        select_insert2.setAttribute('name','grade_id');
        select_insert2.setAttribute('form','insert_btn');
        cellElem7.appendChild(select_insert2);

        let editbtn = document.getElementsByName("edit");            
        for(i = 0; i < editbtn.length; i++) {
            editbtn[i].disabled = true
        }
        let new_cancel = document.getElementById("cancel_id");
        let new_save = document.getElementById("update_id");
        new_save.addEventListener('click', () => {
            new_cancel.disabled = true;
            new_save.value = '保存中...';
            setTimeout(function () {
                new_save.disabled = true; 
            }, 100);
        })

        let add_new_tr = document.getElementById('add_new_tr');
        new_cancel.addEventListener('click', () => {
        add_new_tr.remove();
        for(i = 0; i < edit.length; i++) {
            edit[i].disabled = false
        }
        })
    }
    
});
}, false);
