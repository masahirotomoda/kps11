/****************************************
* variable
*****************************************/
let edit = document.getElementsByName("edit");
let s_user = document.getElementsByName("name");
let s_btn = document.getElementsByName("save");
let edit_cancel = document.getElementsByName("editcancel");
let del_checks = document.getElementsByName("checks[]");
let downloadbtn = document.getElementById("downloadbtn");
let excelbtn = document.getElementById("excelbtn");
let importbtn = document.getElementById("importbtn");

/****************************************
* edit_button  cancel_button save_button
*****************************************/
document.addEventListener("DOMContentLoaded", function(){
    
    let len_edit = edit.length;    
    for (let c_edit = 0; c_edit < len_edit; c_edit++) {
        edit[c_edit].addEventListener('click', () => {
            for(let j = 0; j < len_edit; j++){
                s_user[j].readOnly = true;
                s_btn[j].disabled = true;
                edit_cancel[j].disabled = true;
                edit[j].disabled = false;
                del_checks[j].disabled = false;
            }
            s_user[c_edit].readOnly = false;
            del_checks[c_edit].checked = false;
            del_checks[c_edit].disabled = true;
            s_btn[c_edit].disabled = false;
            edit_cancel[c_edit].disabled = false;
            edit[c_edit].disabled = true;

            editbefore_s_user = s_user[c_edit].value;

        });
        edit_cancel[c_edit].addEventListener('click', () => {
            s_user[c_edit].value = editbefore_s_user;

            del_checks[c_edit].disabled = false;
            s_btn[c_edit].disabled = true;
            edit_cancel[c_edit].disabled = true;
            edit[c_edit].disabled = false;

            s_user[c_edit].readOnly = true;
        });
        s_btn[c_edit].addEventListener('click', () => {
            edit_cancel[c_edit].disabled = true;
            s_btn[c_edit].value = '保存中...';
        
            setTimeout(function () {
                s_btn[c_edit].disabled = true; 
            }, 100);
        
        });
    }

    downloadbtn.addEventListener('click', () => {
            setTimeout(function () {
                downloadbtn.disabled = true;
                downloadbtn.value = 'ダウンロード中...';
            }, 100);            
            setTimeout(function () {
                downloadbtn.disabled = false;
                downloadbtn.value = '入力用ひな形入手';
            }, 4000);
    });
    excelbtn.addEventListener('click', () => {
        setTimeout(function () {
            excelbtn.disabled = true;
            excelbtn.value = 'ダウンロード中...';
        }, 100);            
        setTimeout(function () {
            excelbtn.disabled = false;
            excelbtn.value = 'データの一括ダウンロード(Excel)';
        }, 4000);
    });
    importbtn.addEventListener('click', () => {
        let selectedFile = document.getElementById('file').files[0];

        if(selectedFile != null == false){
            alert('先に、エクセルファイルをアップロードしてください。、')
            
        } else {
            setTimeout(function () {
                importbtn.disabled = true;
                importbtn.value = '⇒アップロード中...';
            }, 100);            
            setTimeout(function () {
                importbtn.disabled = false;
                importbtn.value = '⇒③先生データのアップロード(Excel)';
            }, 100000);
        }        
    });
}, false);
