/****************************************
* 変数定義
*****************************************/
let edit = document.getElementsByName("edit");
let s_invalid = document.getElementsByName("invalid");
let s_display_order = document.getElementsByName("display_order");
let s_course_name = document.getElementsByName("course_name");
let s_is_random = document.getElementsByName("is_random");
let s_btn = document.getElementsByName("save");
let edit_cancel = document.getElementsByName("editcancel");
let del_checks = document.getElementsByName("checks[]");

/****************************************
* 編集ボタン押下時、 キャンセル押下時、保存ボタン押下時
*****************************************/
document.addEventListener("DOMContentLoaded", function(){
        let len_edit = edit.length;
        for (let c_edit = 0; c_edit < len_edit; c_edit++) {
            edit[c_edit].addEventListener('click', () => {
                for(let j = 0; j < len_edit; j++){
                s_invalid[j].disabled = true;
                s_display_order[j].readOnly = true;
                s_course_name[j].readOnly = true;
                s_is_random[j].disabled = true;
                s_btn[j].disabled = true;
                edit_cancel[j].disabled = true;
                edit[j].disabled = false;
                del_checks[j].disabled = false;    
            }
            del_checks[c_edit].checked = false;
            del_checks[c_edit].disabled = true;
            s_invalid[c_edit].disabled = false;
            s_display_order[c_edit].readOnly = false;
            s_course_name[c_edit].readOnly = false;
            s_is_random[c_edit].disabled = false;
            s_btn[c_edit].disabled = false;
            edit_cancel[c_edit].disabled = false;
            edit[c_edit].disabled = true;

            if(s_invalid[c_edit].checked){
                editbefore_s_invalid = true;
            } else {
                editbefore_s_invalid = false;
            }
            if(s_is_random[c_edit].checked){
                editbefore_s_is_random = true;
            } else {
                editbefore_s_is_random = false;
            }
            
            editbefore_s_invalid = s_invalid[c_edit].checked;
            editbefore_s_display_order =s_display_order[c_edit].value;
            editbefore_s_course_name = s_course_name[c_edit].value;
            editbefore_s_is_random =s_is_random[c_edit].checked;
        });
        edit_cancel[c_edit].addEventListener('click', () => {
            s_invalid[c_edit].checked = editbefore_s_invalid;
            s_display_order[c_edit].value = editbefore_s_display_order;
            s_course_name[c_edit].value = editbefore_s_course_name;
            s_is_random[c_edit].checked = editbefore_s_is_random;
            del_checks[c_edit].disabled = false;

            s_btn[c_edit].disabled = true;
            edit_cancel[c_edit].disabled = true;
            edit[c_edit].disabled = false;

            s_invalid[c_edit].disabled = true;
            s_display_order[c_edit].readOnly = true;
            s_course_name[c_edit].readOnly = true;
            s_is_random[c_edit].disabled = true;
        });
        s_btn[c_edit].addEventListener('click', () => {
            edit_cancel[c_edit].disabled = true;
            s_btn[c_edit].value = '保存中...';
        
            setTimeout(function () {
                s_btn[c_edit].disabled = true; 
            }, 100);
        
        });
    }
}, false);
