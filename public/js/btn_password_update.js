/****************************************
* when change password
*****************************************/
let btn = document.getElementById("change");
btn.addEventListener('click', () => {
    setTimeout(function () {
        btn.disabled = true;
        btn.textContent = 'パスワード更新中...';
    }, 100);
});
