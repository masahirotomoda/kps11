'use strict';
const restartBtn=document.getElementById('restart_Btn');
const countDown=document.getElementById('p-typingscreen__count');
const typingResultModal=document.getElementById('p-typingresult');
const mae = document.getElementById('mae');
const tugi = document.getElementById('tugi');
const BackToTopmenuBtn = document.getElementById("BackToTopmenuBtn");
const countSeconds = 3;
let typingFlag = false;
let fromMenuFlg=true;
let restartFlg=false;
let maetugiBtnFlg=true;
let maeCourseIdKey;
let tugiCourseIdKey;
let currentCourseIdKey;
let currentCourseId;
let currentWordsData=[];
let typeWords;
function getCurrentCourseData(){
  if(fromMenuFlg == true){
    currentCourseId = courseData.id;
  }
    for(let i=0;i <=courseList.length-1; i++) {
      if(courseList[i]['id'] == currentCourseId){
        currentCourseIdKey = i;
        if(i==0){
          maeCourseIdKey = null;
          mae.disabled=true;
        } else {
          maeCourseIdKey = i-1;
          mae.disabled=false;
        }
        if(i==courseList.length-1){
          tugiCourseIdKey = null;
          tugi.disabled=true;
        } else {
          tugiCourseIdKey = i+1;
          tugi.disabled=false;
        }
      }
    }
  if(isFree){
    let current_Course_Id_Key=currentCourseIdKey+1;
    const tab_course_index=document.getElementsByName('tab_course_index');
    tab_course_index.forEach(function(course_index) {
      if(course_index.innerHTML==current_Course_Id_Key){
        course_index.classList.add('tabCourseIndexBackground');
      } else {
        course_index.classList.remove('tabCourseIndexBackground');
      }
    })
  }
}
function getCurrentWordData(){
  currentWordsData=[];
  if(fromMenuFlg == true){
    currentCourseId=courseData.id;
  }
  for (let i = 0; i <= typeWordsAll.length-1; i++) {
    if(typeWordsAll[i]['course_id'] == currentCourseId){
      currentWordsData.push(typeWordsAll[i])
    }
  }
  typeWords=currentWordsData;
}

function showTime(timeField){
    min = Math.floor(typingTime / 60);
    sec = typingTime % 60;
    timeField.textContent = min + '分 ' + sec + '秒';
 }
function sortByKeyword() {  
  keywordOrder = [];
  for (let i = 0; i < typeWords.length; i++) { keywordOrder.push(i); }
  if (courseList[currentCourseIdKey]['random'] === 1) {
    for (let i = keywordOrder.length - 1; i >= 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [keywordOrder[i], keywordOrder[j]] = [keywordOrder[j], keywordOrder[i]];
    }
  }
}
function startTypingTimer() {
  typingInterval = setInterval(() => {
    typingTime--;
    min = Math.floor(typingTime / 60);
    sec = typingTime % 60;
    timeField.textContent=min+'分 '+sec+'秒';
    if (typingTime == 0) {
      finishType();
    }
  }, 1000);
}
function courseTitleAnimation(){
  document.querySelector(".active").classList.add("fades");
  setTimeout(() => {
    document.querySelector(".active").classList.remove("fades");
    }, 300);
 }

function getTabCourseLink(courseIndex){
    fromMenuFlg=false;
    currentCourseId = courseList[courseIndex]['id']
    typingTime = courseList[courseIndex]['test_time']
    document.querySelector(".active").textContent = courseList[courseIndex]['course_name'];
    courseTitleAnimation()
    getCurrentCourseData();
    restartBtnClick();
};
function AjaxInit(){
  if(maetugiBtnFlg==true){
    maetugiBtnFlg=false;
  }
  BackToTopmenuBtn.classList.add('hidden');
  mae.classList.add('hidden');
  tugi.classList.add('hidden');
  startBtn.classList.add('hidden');
}
function AjaxEnd(){
      BackToTopmenuBtn.classList.remove('hidden');
      maetugiBtnFlg=true;
      mae.classList.remove('hidden');
      tugi.classList.remove('hidden');
      startBtn.classList.remove('hidden');
}
mae.addEventListener('click', () => {
  fromMenuFlg=false;
  currentCourseIdKey = maeCourseIdKey;
  currentCourseId = courseList[currentCourseIdKey]['id']
  document.querySelector(".active").textContent = courseList[currentCourseIdKey]['course_name'];
  courseTitleAnimation()
  courseData= courseList[currentCourseIdKey];
  getCurrentCourseData();
  restartBtnClick();
  if(!isFree){ 
      maetugiBtnGetScoreData();
  }
});
tugi.addEventListener('click', () => {
  fromMenuFlg=false;
  currentCourseIdKey = tugiCourseIdKey;
  currentCourseId = courseList[currentCourseIdKey]['id']
  document.querySelector(".active").textContent = courseList[currentCourseIdKey]['course_name'];
  courseTitleAnimation()
  courseData= courseList[currentCourseIdKey];
  getCurrentCourseData();
  restartBtnClick();
  if(!isFree){
      maetugiBtnGetScoreData()
  }
});
function toggleModalFunc(modalID) {
  document.getElementById(modalID).classList.toggle("hidden");
}
