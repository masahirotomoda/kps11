/****************************************
 * PDF output
 *****************************************/
let pdf = document.getElementById("pdf");
let pdf1 = document.getElementById("pdf1");
let pdf2 = document.getElementById("pdf2");

pdf1.addEventListener("click", () => {
    let printbtn = document.getElementsByName("checks[]");
    let is_print = null;
    for (i = 0; i < printbtn.length; i++) {
        if (printbtn[i].checked === true) {
            is_print = "yes";
        }
    }
    if (is_print == "yes") {
        pdf.target = "_blank";
    } else {
        pdf.target = "_self";
        setTimeout(function () {
            pdf1.disabled = true;
            pdf1.innerHTML = "PDFプリント準備中...";
        }, 100);
        setTimeout(function () {
            pdf1.disabled = false;
            pdf1.value = "記録証PDFを表示（シンプル）";
        }, 6000);
    }
});
pdf2.addEventListener("click", () => {
    let printbtn = document.getElementsByName("checks[]");
    let is_print = null;
    for (i = 0; i < printbtn.length; i++) {
        if (printbtn[i].checked === true) {
            is_print = "yes";
        }
    }
    if (is_print == "yes") {
        pdf.target = "_blank";
    } else {
        pdf.target = "_self";
        setTimeout(function () {
            pdf2.disabled = true;
            pdf2.innerHTML = "PDFプリント準備中...";
        }, 100);
        setTimeout(function () {
            pdf2.disabled = false;
            pdf2.value = "記録証PDFを表示（カラフル）";
        }, 6000);
    }
});
