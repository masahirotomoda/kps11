/****************************************
* 新規ボタン押下で1行追加
*****************************************/
let add = document.getElementById("add");

document.addEventListener("DOMContentLoaded", function(){
    add.addEventListener('click', () => {
        let insert_btn = document.getElementById("insert_btn");
        if(insert_btn == null){
            let edit = document.getElementsByName("edit");
            let display_order = document.getElementsByName("display_order");
            let word_kana = document.getElementsByName("word_kana");
            let word_mana = document.getElementsByName("word_mana");
            let s_btn = document.getElementsByName("save");
            let edit_cancel = document.getElementsByName("editcancel");
            let del_checks = document.getElementsByName("checks[]");
            for(i = 0; i < edit.length; i++) {
                edit[i].disabled = true;
                s_btn[i].disabled = true;
                edit_cancel[i].disabled = true;
                del_checks[i].disabled = true;
            }
            var tableElem = document.getElementById('mainTableTwo');
            var csrf = document.querySelector('meta[name="csrf-token"]').content;

            var trElem = tableElem.insertRow(1);
            trElem.style.backgroundColor = "#DDDDFF";
            trElem.id = 'add_new_tr';
            var form = document.createElement('form');
            form.action = './add-word';
            form.id = 'insert_btn';
            form.method = 'POST';
            form.enctype = 'multipart/form-data';
            trElem.append(form);

            let csrfinput = document.createElement('input');
            csrfinput.setAttribute('name','_token');
            csrfinput.setAttribute('value',csrf);
            csrfinput.setAttribute('type','hidden');
            form.appendChild(csrfinput);

            let hdn_school = document.getElementById('school_id');
            var qi = document.createElement('input');
            qi.type = 'hidden';
            qi.name = 'school_id';
            qi.value = hdn_school.value;
            form.appendChild(qi);

            let hdn_course = document.getElementById('course_id');
            var qi2 = document.createElement('input');
            qi2.type = 'hidden';
            qi2.name = 'course_id';
            qi2.value = hdn_course.value;
            form.appendChild(qi2);

            var cellElem1 = trElem.insertCell(0);
            var cellElem2 = trElem.insertCell(0);
            var cellElem3 = trElem.insertCell(0);
            var cellElem4 = trElem.insertCell(0);
            var cellElem5 = trElem.insertCell(0);
            cellElem1.style = "border: 1px solid white";
            cellElem2.style = "border: 1px solid white";
            cellElem3.style = "border: 1px solid white";
            cellElem4.style = "border: 1px solid white";
            cellElem5.style = "border: 1px solid white";

            let button = document.createElement('input');
            button.setAttribute('type','submit');
            button.setAttribute('name','update');
            button.setAttribute('id','update_id');
            button.setAttribute('form','insert_btn');
            button.setAttribute('value','保存');
            button.setAttribute('class','btn btn-secondary btn-sm');
            cellElem1.appendChild(button);

            let button_cancel = document.createElement('input');
            button_cancel.setAttribute('type','button');
            button_cancel.setAttribute('name','cancel');
            button_cancel.setAttribute('id','cancel_id');
            button_cancel.setAttribute('form','insert_btn');
            button_cancel.setAttribute('value','ｷｬﾝｾﾙ');
            button_cancel.setAttribute('class','btn btn-secondary btn-sm');
            cellElem1.appendChild(button_cancel);

            var q2 = document.createElement('input');
            q2.name = 'word_kana';
            q2.type = 'text';
            q2.maxLength = 50;
            q2.className = 'form-control col-xs-2';
            q2.setAttribute('form','insert_btn');
            cellElem2.appendChild(q2);

            var q3 = document.createElement('input');
            q3.name = 'word_mana';
            q3.type = 'text';
            q3.maxLength = 50;
            q3.setAttribute('form','insert_btn');
            q3.className = 'form-control col-xs-2';
            cellElem3.appendChild(q3);

            var q4 = document.createElement('input');
            q4.name = 'display_order';
            q4.type = 'text';
            q4.maxLength = 4;
            q4.setAttribute('form','insert_btn');
            q4.className = 'form-control col-xs-2';
            cellElem4.appendChild(q4);

            let new_cancel = document.getElementById("cancel_id");
            let new_save = document.getElementById("update_id");
            new_save.addEventListener('click', () => {
                new_cancel.disabled = true;
                new_save.value = '保存中...';
                setTimeout(function () {
                    new_save.disabled = true;
                }, 100);
            })

            let add_new_tr = document.getElementById('add_new_tr');
            new_cancel.addEventListener('click', () => {
            add_new_tr.remove();
            for(i = 0; i < edit.length; i++) {
                edit[i].disabled = false
            }
            })
        }

    });
}, false);


