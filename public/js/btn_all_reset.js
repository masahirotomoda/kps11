/****************************************
* click the button
*****************************************/
if(document.getElementById("all_btn") !==null){
    let all_btn = document.getElementById("all_btn");
    all_btn.addEventListener('click', () => {
        setTimeout(function () {
            all_btn.disabled = true;
            all_btn.textContent = 'データ取得中...';
        }, 100);
    });
}
if(document.getElementById("all_reset_btn") !==null){
    let all_reset_btn = document.getElementById("all_reset_btn");
    all_reset_btn.addEventListener('click', () => {
            setTimeout(function () {
                all_reset_btn.disabled = true;
                all_reset_btn.textContent = 'キャンセル中...';
            }, 100);
        });
}
if(document.getElementById("update_btn") !==null){
    let update_btn = document.getElementById("update_btn");
    update_btn.addEventListener('click', () => {
            setTimeout(function () {
                update_btn.disabled = true;
                update_btn.textContent = '更新中...';
            }, 100);
        });
}
