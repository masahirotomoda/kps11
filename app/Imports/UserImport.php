<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class UserImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            if($row[1] !=='生徒'){
                $password = bcrypt($row[5]);
            }else{
                $password = $row[5];
            }
            User::create([
                'name' => $row[0], // 行の1列目
                'role' => $row[1], // 行の2列目
                'class_id' => $row[2], // 行の3列目
                'attendance_no' => $row[3], // 行の4列目
                'login_account' => $row[4], // 行の5列目
                'password' => $password, // 行の6列目
                'school_int_id' => $row[6], // 行の7列目
                'school_id' => $row[7], // 行の8列目
            ]);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return self::$startRow;
    }
}
