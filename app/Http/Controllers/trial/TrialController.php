<?php

namespace App\Http\Controllers\trial;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Rules\Phone;
use App\Models\Trial;
use App\Models\School;
use App\Models\IdNumber;
use App\Models\User;
use App\Models\Clas;
//gradeは使うので削除しないこと！
use App\Models\Grade;
use App\Models\Kumi;
use Exception;

/**
 * トライアルアカウント発行
 * (1)申し込みフォーム
 * (2)申し込み完了
*/
class TrialController extends Controller
{

    public function __construct()
    {
        logger()->debug('<-- TrialController@construct');
        
    }

    /**
     * ★申し込みフォーム
     * Route::get
     * @return view('trial.trialentry')
    */
    public function index()
    {
        logger()->debug('<-- TrialController@index');
        

        //カテゴリーセレクトボックス値と初期値
        $category_selection = config('configrations.ENTRY_TYPE');
        $category_select = null;

        //リターンview
        return view('trial.trialentry',
            compact('category_selection','category_select',));
    }
    /**
     * ★送信ボタン押下、完了フォーム
     * (1)入力値のバリデーション
     * (2)先生1名の作成
     * (3)生徒40名の作成
     * (4)トライアルテーブルに追加
     * Route::post
     * @return view('trial.trialresult')
    */
    public function submit(Request $request)
    {
        logger()->debug('<-- TrialController@submit');
        //dd($request->all());
        //【重要/必須】更新ボタン押下で、二重登録防止　トークンを再発行する、セッションタイムアウトにとばされる。
        $request->session()->regenerateToken();
        $ret = true;

        //dd($request->all());
        //(1)バリデーション　一番優先は「required」
        $validator = Validator::make($request->all(), [
            'name' => ['required','string','max:20'],
            'category' => ['required','string','max:20'],
            'industry' => ['required_if:category,その他','string','max:20','nullable'],
            'grade' => ['max:20'],
            'kumi' => ['max:20'],
            'teachers_name' => ['required','string','max:20'],
            'mail' => ['required', 'string', 'email', 'max:50'],
            'address' => ['required', 'string', 'max:50'],
            //'tel' => ['required', new Phone],
            'tel' => ['required','max:50'],
        ]);
        //利用規約にチェックしないと、保存ボタンが活性化しないので、下記処理は念のため
        if($request->missing ('confirm')){
            return redirect('/trial')   //ToDo 利用規約へ飛ぶ
            ->withErrors('利用規約にチェックしてください');
        }
        //dd($request->all());
        //バリデーション失敗時（oldメソッドをつかっているため、入力値を保持）
        if ($validator->fails()) {
            return redirect('/trial')
                ->withErrors($validator)
                ->withInput();
        }else{
            try{
                //バリデーション成功時　
                //(2)先生アカウント取得
                DB::beginTransaction();
                $school = School::where('schools.id','=', config('configrations.TRIAL_SCHOOL_NO')) ->first();
                $s_id = $school->id;
                logger()->debug('School get is OK'.$school->id);

                //先生1名のアカウント取得（admin_teacher_id)
                $account = IdNumber::inRandomOrder()->first();
                logger()->debug('account get is OK '.$account->admin_teacher_id);

                //組の名前取得　(例）32 ⇒数字のみ、トライアル学校内にある一番大きい数字を取得して、それに1足す
                //最初　トライアル32組、とトライアルのクラス数+1にしたが、クラスを削除するとバグが発生するため、名称を数字のみにした。
                $trial_kumino = Kumi::where('school_id',config('configrations.TRIAL_SCHOOL_NO'))
                    ->select('name')
                    ->orderByRaw('CAST(name as SIGNED) DESC')  //組の名前は文字列なので、並べ替えがうまくいかない。数値にキャストする（SQLの機能）
                    ->first();
                //dd($trial_kumino);
                //トライアル既存クラスの中の一番大きい数字を文字列から数値型に変換し、新クラス番号用に1足す
                $trial_kumino  =  intval($trial_kumino->name);
                $max_trial_kumino = $trial_kumino + 1;
                //新しい組をつくる
                $kumi = Kumi::create([
                    'school_id' => $school->id,
                    'name' => $max_trial_kumino,
                ]);

                //学年「なし」を設定
                $trial_grade = grade::where('grade.name','=','なし')
                ->where('grade.school_id','=', $school->id)
                ->select('grade.id')
                ->first();

                //クラスを作る
                $class = Clas::create([
                    'school_id' => $school->id,
                    'grade_id' => $trial_grade->id,
                    'kumi_id' => $kumi->id,
                ]);
                $c_id = $class->id;
                logger()->debug('Class create is OK');

                //先生を作る
                User::create([
                    'school_int_id' => $school->id,
                    'school_id' => $school->school_id,
                    'login_account' => $account->admin_teacher_id,
                    'name' => $request->teachers_name,
                    'role' => config('configrations.ROLE_LIST.3'),                           //ToDo change to do. from Config read
                    'class_id' => $class->id,
                    'trial_flg' => 1,
                    'attendance_no' => 0,
                    'password' => bcrypt($school->password_teacher_init),
                ]);
                //使った先生アカウントをテーブルから削除
                IdNumber::destroy($account->id);

                //(3)生徒40名の作成
                for($i=1;$i<=40;$i++){
                    //テーブルからアカウントを取得してユーザー作成　（例）生徒1（連番）
                    $account = IdNumber::inRandomOrder()->first();
                    User::create([
                        'school_int_id' => $school->id,
                        'school_id' => $school->school_id,
                        'login_account' => $account->user_id,
                        'name' => config('configrations.ROLE_LIST.4').$i,
                        'attendance_no' => $i,
                        'role' => config('configrations.ROLE_LIST.4'),                           //ToDo change to do. from Config read
                        'class_id' => $class->id,
                        'trial_flg' => 1,
                        'password' => $school->password_student_init,
                    ]);
                    //取得したアカウントをテーブルから削除
                    IdNumber::destroy($account->id);
                }
                logger()->debug('User create is OK');
                logger()->debug('IdNumber destroy is OK');

                //(4)トライアルテーブルに保存
                $trial = new Trial();
                //トライアル終了日　小中高校⇒3か月、その他⇒1か月
                if($request->input('category') === '小学校' || $request->input('category') === '中学校' || $request->input('category') === '専門学校'
                    || $request->input('category') === '高校' || $request->input('category') === '大学'){
                    $end_date =date('Y/m/d H:i:s',strtotime("+3 month"));
                }else{
                    $end_date =date('Y/m/d H:i:s',strtotime("+1 month"));
                }
                $trial->fill(array_merge($request->all(),['end_of_trial'=>$end_date,'class_id'=>$c_id,'school_id'=>$s_id]))->save();
                DB::commit();
            }catch (Exception $e){
                DB::rollBack();
                logger()->error('トライアルDBアクセス失敗 '.$e->getMessage());
                logger()->debug($e);
                $ret = false;
            }
            $teacher = null;

            if($ret === true){
                $users = User::where('class_id',$class->id)->get();
                return view('trial.trialresult',compact('trial','school','teacher','users','kumi'));
            }else{
                abort(500); //ToDo とりあえずエラーにしとく
                //return view('trial.trialresult',compact('school'));//ToDo With error　でエラーを出す
            }
        }
    }
}
