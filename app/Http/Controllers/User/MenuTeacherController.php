<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\Information;

/**
 * 先生メニュー　＞　メインメニュー※トライアルと共通
 * (1)メインメニュー表示　2件
 * (2)すべてのお知らせページ表示(bladeは学校管理、システム管理と共通) 30件
*/
$pagenation = 200;
class MenuTeacherController extends Controller
{
    public function __construct()
    {
        logger()->debug('<--  teacher/MenuController@construct');
        $this->middleware('auth');
        $this->middleware('teacher.role');
    }

    /**
     * ★メインメニュー（トライアルと共通
     * Route::get
     * @return view('user.teacher.menu')
     */
    public function teacherIndex()
    {
        logger()->debug('<--  teacher/MenuController@teacherIndex');

        //学校データ　ヘッダーとトライアルかの確認で使う
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();

        //お知らせ（無効をのぞく、ブログではなく、サーバーメンテやサーバー障害などのお知らせ）
        $information = Information::where('invalid','<>',1)
            ->limit(2) //2件のみ表示
            ->orderBy('created_at','DESC')
            ->get();

        //リターンVIEW
        return view('user.teacher.menu',compact('school','information'));
    }

    /**
     * ★すべてのお知らせ
     * Route::get
     * ※ページネーション30（configrations に依存せず、ここだけで設定）
     * @return view('user.teacher.menuinformation')
     */
    public function allInformation()
    {
        logger()->debug('<-- teacher/MenuController@allInformation');

        //学校データ　ヘッダーで使用
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();
        //お知らせ　メニューで3件表示、ここでは4件目から表示⇒bladeで設定 ⇒ページネーションでエラーしたので、全て表示
        $information = Information::where('invalid','<>',1)
            ->orderBy('created_at','DESC')
            ->paginate(30);
        //リターンVIEW
        return view('user.teacher.menuinformation',compact('school','information'));
    }
    public function movie()
    {
        logger()->debug('<-- MenuController@movie');
        //学校データ　ヘッダーで使用
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();
        return view('user.teacher.movie',compact('school'));
    }

    public function first_typing()
    {
        logger()->debug('<-- MenuController@movie');
        //学校データ　ヘッダーで使用
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();
        return view('user.teacher.first_typing.pdf',compact('school'));
    }
    public function qa()
    {
        logger()->debug('<-- MenuController@movie');
        //学校データ　ヘッダーで使用
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();
        return view('user.teacher.qa',compact('school'));
    }
}