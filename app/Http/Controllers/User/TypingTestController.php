<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Exception;
use App\Models\ScoreHistory;
use App\Models\Course;
use App\Models\School;
use App\Models\BestScore;

class TypingTestController extends Controller
{
    /**
     * 【学校版】テストモード、キータッチ2000共通
     */
    public function __construct()
    {
        logger()->debug('<-- SchoolTypingTestController@construct');
        $this->middleware('auth');
    }

    /**
     * 【学校版】タイピング画面(テストモード、キータッチ2000)を出力する
     *
     * @version 2022.02.27.01　updated at 2023.11.1 by Ichikawa Yoko
     * @author Hikaru.Asai
     * @param Request $request 学校ID,コースID
     * @return Response タイピング画面のリダイレクト画面出力
     */
    public function show(Course $course)
    {
        logger()->debug('<-- SchoolTypingTestController@show');

        $school = School::where('id',auth()->user()->school_int_id)->first();

        //【JSに渡すデータ】選んだコースのタブの全コースに紐づいてる単語（複数）⇒各コースの単語の振り分けはJSで行う
        $wordData = Course::select('words.word_mana','words.id','course_id')
            ->leftJoin('course_word', 'courses.id', '=', 'course_word.course_id')
            ->leftJoin('words', 'words.id', '=', 'course_word.word_id')
            //->where('courses.id', '=', $course->id)
            ->where('courses.invalid',false)
            ->where('course_type', 'テスト')
            ->where('course_category', $course->course_category)
            ->where(function($query) use($school){
                $query->where('courses.school_id', $school->id);
                $query->orWhere('courses.school_id', null);
                }) 
            ->get();
            //dd($wordData);

        //【JDに渡すデータ】前へボタン、次へボタン　前後のコースIDを取得するため、今選んでいるタブの全コースのコースIDを取得
        $coursesArray = Course::where('invalid',false)
            ->where('course_type', 'テスト')
            ->where('course_category', $course->course_category)
            ->where(function($query) use($school){
                $query->where('courses.school_id', $school->id);
                $query->orWhere('courses.school_id', null);
                })            
            ->select('id','course_name','random','course_category','test_time')
            ->orderBy('display_order','ASC')
            ->get()->toArray();  //配列形式にする、加工がしやすいため
            //dd($coursesArray);

        // ベストスコア10件
        $scoreData = BestScore::where('user_id', auth()->id())
            ->where('course_id',$course->id)
            ->orderBy('best_point','desc')
            ->orderBy('best_point_created_at','desc')
            ->take(10)
            ->get();
            //dd($scoreData);
        //今日のスコア
        $todayScoreData = ScoreHistory::where('user_id',auth()->id())
            ->where('course_id',$course->id)
            ->whereDate('created_at',now()->format('Y-m-d'))
            ->orderBy('created_at','desc')
            ->get();

        //リクエストで$courseの全カラムデータ取得。不要なカラムを取りたいため（JSで使うので）
        $course=Course::select('id','course_name','test_time','course_category')
            ->where('id',$course->id)
            ->first();
            //dd($course);

        if ($wordData->isEmpty()) {
            logger()->warning('不正操作? id = '.auth()->user()->id);
            return redirect()->route('403');
        } else {
            return view('user.student.typingtest', compact('course', 'scoreData', 'todayScoreData', 'wordData', 'school','coursesArray'));
        }
    }

    /**
     * 【重要】ajaxにしたため、これは使わない
     * タイピングが終了後、スコアを登録する
     *
     * @version 2022.03.21.01
     * @author Hikaru.Asai
     * @param Request $request ユーザーID,コースID,コース名,ポイント,タイム,タイプ数,ミスタイプ数
     * @return Response タイピング画面のリダイレクト画面出力

    public function store(Request $request, Course $course)
    {
        logger()->debug('<-- TypingTestController@store');

        // スコア履歴テーブルにデータ追加
        $newScore = new ScoreHistory();
        $newScore->user_id = auth()->id();
        $newScore->course_id = $course->id;
        $newScore->course_name = $course->course_name;
        $newScore->point = $request->input('typingresult--score');
        $newScore->time = $request->input('typingresult--time');
        $newScore->typing_number = $request->input('typingresult--all');
        $newScore->misstyping_number = $request->input('typingresult--miss');
        $newScore->save();

        // ベストスコアテーブルにデータ追加
        $bestScoreData = BestScore::where('user_id', '=', auth()->id())
                        ->where('course_id', '=', $course->id)
                        ->orderBy('best_point', 'desc')
                        ->take(10)
                        ->get();
        if ($bestScoreData->count() == 10) {
            // 10件だった場合更新
            if ($bestScoreData->last()->best_point < $request->input('typingresult--score')) {            
                $bestScoreData->last()->update([
                    'best_point' => $request->input('typingresult--score'),
                    'best_point_created_at' => now()
                ]);         
            }
        } else {
            // 10件ない場合追加
            $bestScore = new BestScore();
            $bestScore->user_id = auth()->id();
            $bestScore->course_id = $course->id;
            $bestScore->best_point = $request->input('typingresult--score');
            $bestScore->school_id = auth()->user()->school_int_id;
            $bestScore->save();
        }
        //順位を書き込む
        $raw = 'RANK() OVER(ORDER BY best_point DESC,best_point_created_at DESC) AS rank_result';
        $bestScoreData = BestScore::where('user_id', '=', auth()->id())
            ->where('course_id', '=', $course->id)
            ->select('*',DB::raw($raw))
            ->get();
        foreach ($bestScoreData as $dt){
            logger()->debug('$dt->rank_result = '.$dt->rank_result);

            $bestScoreData->find($dt->id)->update([
                'bp_rank' => $dt->rank_result,
            ]);
        }

        // 二重送信対策
        $request->session()->regenerateToken();

        // タイピング画面へリダイレクト
        return redirect()->route('typingtest.show', $course);
    }
    */
    /**
     * 【ajax通信】タイピングの結果を表示　
     * ※次の共通　⇒①結果モーダルを閉じた時に呼び出し　②前、次へボタン押下時に呼び出し
     * コースや単語データはJSで処理するため、ここでは呼び出さない。今日のスコアとベストポイントだけ保存＆呼び出し
     * ※tokenがうまく引き継げないため、tokunを例外にしている⇒ここで設定　app\Http\Middleware\VerifyCsrfToken.php
     * 
     */
    public function ajax(Request $request)
    {
        
        logger()->debug('<-- TypingTestController@ajax');
        //ajax通信以外はエラー
        if($request->ajax()==false){
            return redirect()->route('403');;
        }

        logger()->debug($request->ajax());
        $requestAjaxData=$request->all();
        $requestAjaxData_courseId=$requestAjaxData['courseId'];
        $requestAjaxData_typingScore=$requestAjaxData['typingScore'];
        //■■ddで変数が確認できないので、loggerでlaravel logで確認する
            logger()->debug($requestAjaxData);
            //ajaxで取得したスコアデータ
        $courseNameAjaxData=Course::find($requestAjaxData['courseId']);
            //logger()->debug($course_ajax_data);

        if($courseNameAjaxData->course_category =='キータッチ2000検定'){
            $rank=$requestAjaxData['typingRank'];
        } else {
            $rank=null;
        }
        // スコア履歴テーブルにデータ追加、次、前ボタンはスコアがないので、保存はなし。
        if($requestAjaxData_typingScore != null){
            $newScore = new ScoreHistory();
            $newScore->user_id = auth()->id();
            $newScore->course_id =$requestAjaxData_courseId;
            $newScore->course_name = $courseNameAjaxData->course_name;
            $newScore->point = $requestAjaxData_typingScore;
            $newScore->time = $requestAjaxData['typingTime'];
            $newScore->typing_number =0;
            $newScore->misstyping_number =0;
            $newScore->rank =$rank;
            $newScore->save();

            // ベストスコアテーブルにデータ追加
            $bestScoreData = BestScore::where('user_id', '=', auth()->id())
                            ->where('course_id',$requestAjaxData_courseId)
                            ->orderBy('best_point', 'desc')
                            ->take(10)
                            ->get();
            DB::beginTransaction();
            try {
                if ($bestScoreData->count() == 10) {
                    // 10件だった場合更新
                    if ($bestScoreData->last()->best_point < $requestAjaxData_typingScore) {
                        $bestScoreData->last()->update([
                            'best_point' => $requestAjaxData_typingScore,
                            'rank' => $rank,
                            'best_point_created_at' => now()
                        ]);
                    }
                } else {
                    // 10件ない場合追加
                    $bestScore = new BestScore();
                    $bestScore->user_id = auth()->id();
                    $bestScore->course_id = $requestAjaxData_courseId;
                    $bestScore->best_point = $requestAjaxData_typingScore;
                    $bestScore->rank = $rank;
                    $bestScore->best_point_created_at = now();
                    $bestScore->school_id = auth()->user()->school_int_id;
                    $bestScore->save();
                }
                //順位を書き込む
                $raw = 'RANK() OVER(ORDER BY best_point DESC,best_point_created_at DESC) AS rank_result';
                $bestScoreData = BestScore::where('user_id', '=', auth()->id())
                    ->where('course_id', '=', $requestAjaxData_courseId)
                    ->select('*',DB::raw($raw))
                    ->get();
                foreach ($bestScoreData as $dt){
                    //logger()->debug('$dt->rank_result = '.$dt->rank_result);

                    $bestScoreData->find($dt->id)->update([
                        'bp_rank' => $dt->rank_result,
                    ]);
                }

                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                logger()->error('【学校版】タイピング画面（テストモード）でエラー');
                logger()->error($e);
                abort(500);
            }
        }

        //スコア履歴
        $scoreData = BestScore::where('user_id', '=', auth()->id())
                        ->where('course_id', '=', $requestAjaxData_courseId)
                        ->orderBy('best_point', 'desc')
                        ->orderBy('best_point_created_at', 'desc')
                        ->take(10)
                        ->get();
        //今日のスコア
        $todayScoreData = ScoreHistory::where('user_id', '=', auth()->id())
                        ->where('course_id', '=',$requestAjaxData_courseId)
                        ->whereDate('created_at', now()->format('Y-m-d'))
                        ->select('point','rank')
                        ->orderBy('created_at', 'desc')
                        ->get();
        
        //取得データを送信
        return response()->json([$todayScoreData,$scoreData]);             
    }
}