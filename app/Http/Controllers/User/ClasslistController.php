<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\School;
use App\Models\Clas;

/**
 *  学校管理メニュー ＞　クラス一覧　※学校管理メニューのクラス管理と同じ　bladeファイル共有
 * 【重要】学校管理のクラス管理も、このコントローラーのクラスを使う
 * (1)クラス一覧
 * (2)クラス別所属生徒一覧
*/
class ClasslistController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- ClasslistController@construct');
        $this->middleware('auth');
        $this->middleware('teacher.role');
    }
    /**
     * //★クラス一覧（閲覧のみ、ページネーションなし）
     * 【重要】トライアルクラスが、URLを直接入力すると他のクラス一覧がみれるため、これを阻止する
     * （本来、トライアルクラスはクラス一覧機能は使えない）
     * @return view('school.classlist')
     */
    public function index()
    {
        logger()->debug('<-- teacher/ClasslistController@index');

        //学校データ
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('name','id','trial')->first();

        //念のため、トライアル先生がgetパラメータで入るのを防ぐため（直接URLを入れるとみえてしまうため）
        if($school->trial === 1){
            logger()->error('■先生メニュー、クラス一覧でトライアルクラスが強引にゲットパラメータで入ろうとした時のエラー');
            abort(403);
        }

        //上の表⇒学年別の生徒数（SQLで取得）
        $gradeinfo_toals = User::leftJoin('classes','class_id','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('grade.name as grade_name')            
            ->where('classes.school_id','=',$school->id)
            ->where('users.role','=','生徒')
            ->groupBy('grade.name')
            ->selectRaw('COUNT(users.id) AS user_id_count')
            ->selectRaw('COUNT(grade.name) AS grade_name_count')
            ->orderByRaw('CHAR_LENGTH(grade.name) ASC,grade.name ASC')
            ->get();

        //下の表⇒クラス数と生徒数(下の表は、SQLで取得せず、forで求める。classesのforの中に、usersのforが多重で入る)
        //ここで、'users.role','=','生徒'　を設定する必要はない
        $classes = Clas::leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('grade.name as grade_name','kumi.name as kumi_name','classes.id as class_id',
            'classes.school_id as school_int_id','kumi.id as kumi_id','grade.id as grade_id')
            ->where('classes.school_id','=',$school->id)
            ->orderByRaw('CHAR_LENGTH(grade.name) ASC,grade.name ASC,
                CHAR_LENGTH(kumi.name) ASC, kumi.name ASC')
            ->get();

        //下の表の生徒数カラムのデータ（forでまわして取得））
        $users = User::leftJoin('classes','users.class_id','=','classes.id')        
        ->select('users.id','users.school_id','users.role','class_id',)
        ->where('users.school_id','=',auth()->user()->school_id)
        ->where('users.role','=','生徒')
        ->get();

        //リターンview
        return view('user.teacher.classlist',compact('users','classes','school','gradeinfo_toals'));
    }
    /**
     * ★クラス別所属生徒（閲覧のみ、ページネーションなし)
     * クラス一覧の各クラスにある「所属生徒」ボタン押下時  
     * ※「abc22345/teacher/student-search」（生徒一覧の検索） に学年、組のパラメータをもたせて遷移するため、学年と組をhiddenでもたせる
     *
     * @return view('school.classlist')
     */
    public function belongs(Request $request)
    {
        logger()->debug('<-- teacher/ClasslistController@belongs');
        //学校データ
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('name','id','trial')->first();
        //念のため、トライアル先生がgetパラメータで入るのを防ぐため（直接URLを入れるとみえてしまうため）
        if($school->trial === 1){
            logger()->error('■先生メニュー、クラス一覧⇒クラス別所属生徒一覧でトライアルクラスが強引にゲットパラメータで入ろうとした時のエラー');
            abort(403);
        } 
        //hidden formからクラスID取得
        $class_id = $request->input('class_id');
        
        //生徒一覧
        $users = User::where('users.school_int_id','=',auth()->user()->school_int_id)
            ->where('users.role','=','生徒')
            ->where('users.class_id','=',$class_id)
            ->orderBy('attendance_no','ASC')
            ->get();

        //上の表の学年とクラス（学年IDとクラスIDは生徒一覧ボタン押下時の遷移でパラメータとして使う）
        $class = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
            ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('grade.name as grade_name','kumi.name as kumi_name','grade.id as grade_id','kumi.id as kumi_id')
            ->where('classes.id','=',$class_id)
            ->first();

        //上の表の生徒数
        $student_num_byclass = $users -> count();

        //リターンview
        return view('user.teacher.classbelongs',compact('users','school','class','student_num_byclass' ));
    }
}
