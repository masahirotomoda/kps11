<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use app\Models\User;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\Clas;
use Exception;

/**
* 生徒のマイメニュー　※トライアルと共通
*(1)生徒情報表示
*(2)生徒のクラス情報変更
*(3)生徒のパスワード変更
*/
class ProfileStudentController extends Controller
{
    public function __construct()
    {
        //logger()->debug('<-- ProfileStudentController@construct');
        $this->middleware('auth');
    }
    /**
    * ★生徒情報（トライアルと共通、トライアルは別VIEWに遷移）    *
    * @return　（トライアル） view('user.student.profiletrial'));  
    * @return （通常の学校）　view('user.student.profile')
    */
    public function studentProfile()
    {
        //logger()->debug('<-- ProfileController@studentProfile');
        //生徒情報（クラス情報はクラスIDから、学年、組を取得）
        $user = User::where('users.id','=',auth()->id())
            ->leftJoin('classes','users.class_id','=','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('users.name as user_name','users.id as user_id','users.school_int_id as school_int_id','users.school_id as school_id','users.user_nickname',
                'role','class_id','login_account','attendance_no','grade.id as grade_id','kumi.id as kumi_id',
                'grade.name as grade_name','kumi.name as kumi_name','users.avatar')
            ->first();
        $avatar=$user->avatar;
        
        $school=School::find(auth()->user()->school_int_id);
        //学年、組のセレクトボックス値　モデルで関数を作成して、取得（学校IDが引数）
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        $green_selected="selected";

        //生徒とトライアルはviewを変える（トライアルは学年・組・出席番号の変更ができない）    
        if($school['trial'] === 0){
            return view('user.student.profile',compact('user','school','grade_selection','kumi_selection','green_selected','avatar'));  
        } else {
            return view('user.student.profiletrial',compact('user','school'));  
        }        
    }
    /**
    * 生徒情報（学年、組、出席番号）の変更(トライアルは変更不可なので使用しない)
    *　※ユーザーにあるのはクラス（学年と組の組み合わせ）のみ、入力された学年と組からクラスIDを取得
    * @return redirect('/student/profile');
    */
    public function studentProfileUpdate (Request $request)
    {
        //logger()->debug('<-- ProfileStudentController@studentProfileUpdate ');  
        
        //学校データ
        $school = School::find(auth()->user()->school_int_id);
        //学年、組のセレクトボックス値
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        //変更した学年、組からクラスIDを取得
        $student_class = Clas::where('school_id','=',auth()->user()->school_int_id)
            ->where('grade_id','=',$request -> input('nd_grade_selection') ) 
            ->where('kumi_id','=',$request -> input('nd_kumi_selection') ) 
            -> first();

        //リクエストデータを$reqDataに格納　⇒配列形式に変換
        $reqData = $request->all();
        //dd($reqData);

        //出席番号を半角数字にして$requestに再セット　※出席番号は本来必須、わからない時はゼロをいれてもらう
        if (isset($reqData['attendance_no'])){
            $reqData['attendance_no'] =  mb_convert_kana($reqData['attendance_no'],"n");
        }        
        //クラスが存在しないと、$student_class['id']　は何も返さない（要素名が存在しない）ので、nullをセットする
        if(isset($student_class['id'])){
            $reqData['student_class_id'] = $student_class['id'];
        } else {
            $reqData['student_class_id'] = null;
        }
        //バリデーション実行(カスタムエラーメッセージにするための設定)の準備 ※クラスIDは配列形式にする       
        $validator = Validator::make($reqData, [
            'student_class_id' => ['required'],
            'attendance_no' => ['required','numeric'],
        ]);     
        
        //バリデーション失敗時　リダイレクト
        if ($validator->fails()) {
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            return redirect('/student/profile')
                ->withErrors($validator);
        }

        //編集中の生徒データ
        $user = User::find(auth()->id());

        //変更した出席番号とクラスIDをセット
        $user ->attendance_no =  $reqData['attendance_no'];
        $user ->class_id = $reqData['student_class_id'];
        $user ->user_nickname =  $reqData['user_nickname'];
        $user ->avatar =  $reqData['selected_avatar'];

        //学年名の取得（成功時フラッシュメッセージを1，2年はひらがな、それ以上は漢字にするため）
        $grade = Grade::find($request -> input('nd_grade_selection'));

        if($grade->name == '1年' || $grade ->name =='2年'){
            $flash_message = 'せいとじょうほうをほぞんしました。';
        } else {
            $flash_message = '生徒情報を保存しました。';
        }

        //保存ボタン押下＆POSTの時だけ保存実行
        if($request->has('student_name_change_btn')  && $request->isMethod('post')){
            try{
                //生徒データの更新　成功時flashメッセージ
                $user->save();
                session()->flash('flash_message', $flash_message);  
            
            }catch(Exception $e) {
                logger()->error('生徒マイメニュー、生徒データの更新でエラー');
                logger()->error($e);
                abort(500);
            } 
        } else {
            logger()->error('■生徒マイメニュー、生徒の学年、組、出席番号更新で保存ボタンをおさずPOSTしていない時のエラー');
            abort(500);
        }
      
       //リダイレクト
       return redirect('/student/profile');
    }
   /**
    * 生徒のパスワード変更 ※トライアルと、通常生徒でのパスワード変更は共通メソッド
    *  「パスワードを変える」ボタン押下で確認モーダル表示、モーダルの「パスワードを変える」ボタンでform開始
    * コンポーネントの「changePaswd」でモーダル表示
    * @return redirect('/student/profile')
    */   
    public function studentPasswordUpdate(Request $request)
    {
        logger()->debug('<-- ProfileController@studentPasswordUpdate');
        
        //学校データ
        $school = School::find(auth()->user()->school_int_id);      

        //リクエストデータを$reqDataに格納　⇒配列形式に変換
        $reqData = $request->all();

        //パスワードと確認用パスワードを半角数字にして$requestに再セット
        $reqData['student_change_password'] =  mb_convert_kana($request['change_password'],"n");
        $reqData['student_change_password_confirmation'] =  mb_convert_kana($request['change_password_confirmation'],"n");
         
        //バリデーション実行(カスタムエラーメッセージにするための設定)の準備 ※クラスIDは配列形式にする       
        $validator = Validator::make($reqData, [
            'student_change_password' => ['required','confirmed','numeric','regex:<[0-9]{4,6}>'],
        ]); 
        
        //バリデーション失敗時　リダイレクト
        if ($validator->fails()) {
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            return redirect('/student/profile')
                ->withErrors($validator);
        }
        //ログイン中の生徒情報取得
        $user = User::find(auth()->id());
            
        //更新したパスワードを$userにセット
        $user['password'] = $reqData['student_change_password'];
        //dd($request->all());

        //保存ボタン押下＆POSTの時だけ保存実行
        if($request->has('user_password_reset_btn')  && $request->isMethod('post')){
            try{
                $user->save();
                    //更新成功時、flashメッセージ
                    session()->flash('flash_message', 'パスワードを変更しました');        
            }catch(Exception $e) {
                    logger()->error('生徒マイメニュー、生徒パスワード変更でエラー');
                    logger()->error($e);
                    abort(500);
            }
        } else {
            logger()->error('■生徒マイメニュー、パスワード変更で保存ボタンをおさずPOSTしていない時のエラー');
            abort(500);
        }
        //リダイレクト　
        return redirect('/student/profile');
    } 
}