<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\BestScore;
use App\Exports\Export;
//赤線がでるが、PDFでつかうので必要
use Barryvdh\DomPDF\Facade\Pdf;
use Maatwebsite\Excel\Facades\Excel;

/**
* 先生メニュー　生徒別ベストスコア、エクセル出力、PDF印刷
*【重要】ベストスコアテーブルにはschool_id(int_id)カラムがあるが、それは使わずに、usersから学校IDを取得すること！
* ⇒トライアル学校から学校に昇格したときに、学校IDが引き継がれないため。
* ページネーション100【理由】PDFで出力で時間がかかるため、1ページ100件にする
*(1)ベストスコア一覧
*(2)検索＆エクセル出力
*(3)PDF印刷
*/
class BestScoreController extends Controller
{
    public function __construct()
    {
        //logger()->debug('<-- BestScoreController@construct');
        $this->middleware('auth');
        $this->middleware('teacher.role');
    }

    /**
     * ★ベストスコア一覧(先生の成績は表示しない)
     * ※学校とトライアルの振り分けあり（SQL内で処理）
     * ※日付書式はコントローラーでできなかったのでViewで実装
     * @return  view('user.teacher.bestscore')
     */
    public function index()
    {
        //logger()->debug('<-- (Teacher)BestScoreController@index');
        //学校データ
        $school = School::where('id','=',auth()->user()->school_int_id)->first();
        //学年、組のセレクトボックス値
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        //検索ボックスの初期値
        $user_name = null;
        $courseName = null;
        $grade = null;
        $kumi = null;
        $name = null;

        //次のSQLでクラスIDが変数で必要なので作成（auth()の形式だとエラー）
        $class_id=auth()->user()->class_id;

        //ベストスコアデータの取得 「when」で学校とトライアルの切り分け（トライアルがクラス単位）
            $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
                ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
                ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
                ->leftJoin('classes','classes.id','=','users.class_id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                //トライアルならクラスIDを取得して、クラス単位で表示
                ->when($school['trial'] === 1 ,function($q) use($class_id){
                    return $q->where('users.class_id',"=",$class_id);
                })
                ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name','success_num',
                    'course_with_bestpoint.best_point_created_at as b_created_at','course_with_bestpoint.id as bestpoint_id','course_with_bestpoint.rank as bestpoint_rank','users.*',
                    'course_with_bestpoint.best_point')
                ->groupBy('users.id')
                ->groupBy('course_with_bestpoint.course_id')
                ->where('users.role','=','生徒')
                ->where('users.school_id',$school->school_id)
                ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')
                ->paginate(100);
                
        //共通リターンview
        return view('user.teacher.bestscore',compact('school','courseName',
            'grade','kumi','name','grade_selection','kumi_selection','bestScore'));
    }
    /**
     * ★ベストスコア　検索＆エクセル出力
     * 検索項目：「開始日時」「開始時間」「終了日時」「終了時間」「コース名の一部」「学年」「組」「生徒名の一部」
     * ※学校とトライアルの振り分けあり（SQL内で処理）
     * ※エクセル出力⇒検索結果をエクセル出力するので、同じメソッド内で処理
     * @return
     */
    public function search(Request $request)
    {
        //logger()->debug('<-- (Teacher)BestScoreController@search');
        //学校データ
        $school = School::where('id','=',auth()->user()->school_int_id)->first();

        //検索入力値を変数にセット
        $startDateTime = $request->input('start-date-time');
        $endDateTime = $request->input('end-date-time');
        $courseName = $request->input('course_name');
        $user_name = $request->input('name');
        $grade = $request->input('grade');
        $kumi = $request->input('kumi');
        $name = $request->input('name');

        //学年、組のセレクトボックス値
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        //次のSQLでクラスIDが変数で必要なので作成（auth()の形式だとエラー）
        $class_id=auth()->user()->class_id;

        //エクセル出力ボタンのnameが「excel」、検索ボタン押下（search）でviewを切り分け
        //エクセル⇒ページネーションにかかわらず出力なのでget、検索⇒paginate()
        //※学校とトライアルはSQLの中のwhenで切り分けしている

        //コース名は、BestScoreテーブルから取得 クエリのみ
        $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
            ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
            ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
            ->leftJoin('classes','classes.id','=','users.class_id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            //トライアルならクラスIDを取得して、クラス単位で表示
                ->when($school['trial'] === 1 ,function($q) use($class_id){
                return $q->where('users.class_id',"=",$class_id);
                })
                ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name','success_num',
                'course_with_bestpoint.best_point_created_at as b_created_at','course_with_bestpoint.id as bestpoint_id','course_with_bestpoint.rank as bestpoint_rank','users.*',
                'course_with_bestpoint.best_point')
            ->groupBy('users.id')
            ->groupBy('course_with_bestpoint.course_id')
            ->where('users.role','=','生徒')
            ->where('users.school_id',$school->school_id)
            ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')

            ->when($grade != '' || $grade != null ,function($q) use($grade){
                return $q->where('classes.grade_id','=',$grade);
            })
            ->when($kumi != '' || $kumi != null ,function($q) use($kumi){
                return $q->where('classes.kumi_id','=',$kumi);
            })
            ->when($user_name !== null ,function($q) use($user_name){
                return $q->where('users.name','Like binary',"%$user_name%");
            })
            ->when($courseName !== null ,function($q) use($courseName){
                return $q->where('courses.course_name','Like binary',"%$courseName%");
            });

        //エクセルボタン押下時（ページネーションにかかわらずget）
        if($request->has('excel')){
            $bestScore = $bestScore->get();
            //データが何も表示されていない時(バリデーションチェック)
            if ($bestScore->isEmpty()) {
                return redirect('/teacher/bestscore')
                    ->withErrors('エクセルに出力するデータがありません。検索して、データを表示させてから「データの一括ダウンロード(Excel)」ボタンを押してください');
            } else{
            //出力するデータがあればエクセルに出力
            //★これから実装　⇒ダウンロード成功時のメッセージ
            $view = view('excel.bestscoreexcel',compact('bestScore'));
            return Excel::download(new Export($view), '生徒のタイピングベストスコア'.date('Y年m月d日H時i分s秒出力').'.xlsx');
            }
        //検索ボタン押下時
        } elseif ($request->has('search') || $request->has('page')){
            $bestScore = $bestScore->paginate(100);
            return view('user.teacher.bestscore',compact('school','courseName',
            'grade','kumi','name','grade_selection','kumi_selection','bestScore',));
        } else {
            logger()->error('■先生メニュー、生徒別ベストスコアの検索&エクセル出力でボタンをおさずエラー');
            abort(500);
        }
    }
    /**
     * ★生徒別ベストスコア一覧　記録証PDF出力
     * @param Request $request
     * @return   $pdf->stream()
     */
    public function pdf(Request $request){
        //logger()->debug('<-- BestscoreController@pdf');

        //印刷チェックがあれば（$request['checks']は、チェックしているscorehistoryのIDが配列で格納）
        if(isset($request['checks'])){
                $select = null;//??
                $school = School::where('id','=',auth()->user()->school_int_id)->first();
                $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
                    ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
                    ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
                    ->leftJoin('classes','classes.id','=','users.class_id')
                    ->leftJoin('grade','classes.grade_id','=','grade.id')
                    ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                    ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name','courses.course_category as course_category','success_num',
                        'course_with_bestpoint.rank','course_with_bestpoint.id as bestpoint_id','course_with_bestpoint.best_point_created_at as b_created_at','users.*',
                        'course_with_bestpoint.best_point')
                    ->groupBy('course_with_bestpoint.course_id')
                    ->groupBy('users.id')
                    ->where('users.role','=','生徒')
                    ->where('users.school_id',$school->school_id)
                    ->whereIn('course_with_bestpoint.id',$request->checks)
                    ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')
                    ->get();
                    //dd($bestScore);
                //pdf1（シンプル記録証）ボタン押下
                if($request->has('pdf1')){
                    $pdf = PDF::loadView('pdf.transcript_bestscore_simple',compact('bestScore','school'));

                }else{
                //pdf1（カラフル記録証）ボタン押下
                    $pdf = PDF::loadView('pdf.transcript_bestscore_colorful',compact('bestScore','school'));
                }
                return $pdf->stream();
        } else{
            //印刷チェックが1つもない場合、エラーメッセージでリダイレクト
            return redirect('/teacher/bestscore')
                ->withErrors('【印刷できません】印刷したいデータにチェック（表の一番左の項目）してからボタンを押してください');
        }
    }
}
