<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\School;
use Exception;

/**
* トライアル先生メニュー ＞　トライアル生徒一覧 ※トライアルはページネーションなし。学校版とコントローラーを分ける
*(2)トライアル生徒一覧表示
*(4)トライアル生徒検索
*(7)トライアル生徒の更新
*(8)トライアルパスワードリセット
*/
class StudentlistTrialController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- teacher/StudentlistTrialController@construct');
        $this->middleware('auth');
        $this->middleware('teacher.role');
    }

    /**
     * ★トライアル生徒一覧
     * Route::get
     * @return view('user.teacher.studentlisttrial')
     */
    public function index()
    {
        logger()->debug('<-- teacher/StudentlistTrialController@index');
        global $pagenation;
        
        //学校データ（ヘッダーで使用＆パスワードリセット）
        $school = School::find(auth()->user()->school_int_id);   
        
        //トライアルの場合
        if($school->trial === 1){            
            //検索ボックス初期値
            $user_name = null;

            //次のSQLでクラスIDが変数で必要なので作成（auth()の形式だとエラー）
            $class_id=auth()->user()->class_id;

            //トライアル生徒データ
            $users_data = User::where('users.school_int_id','=',auth()->user()->school_int_id)
                ->where('users.role','=','生徒')
                ->where('users.class_id','=',$class_id)
                ->select('users.name as user_name','users.id as user_id','password',
                'users.school_int_id as school_int_id','role','class_id','login_account')
                ->orderByRaw('CHAR_LENGTH(users.name) ASC,users.name ASC');
                
            $users = $users_data->get();            

            //トライアル生徒数
            $user_count = $users_data->count();
            //検索語のトライアル生徒数だが、indexの段階では、全データと同じ
            $aftersearch_user_count =  $user_count;
            //dd($user_count);
            //リターンview
            return view('user.teacher.studentlisttrial',compact('users','school','user_name','user_count','aftersearch_user_count'));
        } else {
        //それ以外
            logger()->error('■トライアル先生メニュー、生徒一覧でトライアル学校以外の時のエラー');
            abort(500);
        }
    }

    /**
     * ★トライアル生徒の検索「名前」 ※40名なので、ページネーションなし
     * Route::post 　トライアル学校内で、先生権限は同じなのでgetは使わない。
     * @return view('user.teacher.studentlisttrial')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- teacher/StudentlisttrialController@search');
        
        global $pagenation;
        //成功メッセージをださないため
        session()->flash('flash_message', null);
        //検索値を変数に代入
        $user_name = $request->input('username');

        $class_id=auth()->user()->class_id;
        //dd($class_id);

        //学校データ(パスワード表示、非表示の設定)
        $school = School::where('id','=',auth()->user()->school_int_id)
            ->select('name','id','trial','password_student_init','password_display')->first();
        
        //生徒数
        $user_count = User::select('id','role')
            ->where('users.class_id','=',$class_id)
            ->where('users.role','=',"生徒")
            ->count();

        //検索後の生徒データ
        $users_data = User::leftJoin('classes','users.class_id','=','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('users.name as user_name','users.id as user_id','password',
                'users.school_int_id as school_int_id',
                'role','class_id','login_account','attendance_no','classes.*','grade.id as grade_id','kumi.id as kumi_id')
            ->where('users.class_id','=',$class_id)
            ->where('users.role','=',"生徒")
            ->when($user_name != '' || $user_name != null ,function($q) use($user_name){
                return $q->where('users.name','Like binary','%'.$user_name.'%');
            })
            ->orderByRaw('CHAR_LENGTH(users.name) ASC,users.name ASC');

        //検索後の生徒情報
        $users = $users_data->get();

        //検索後の生徒数
        $aftersearch_user_count = $users_data->count();

        //リターンview
        return view('user.teacher.studentlisttrial',compact('users','school','user_name','user_count','aftersearch_user_count'));
    }

    /**
     * ★トライアル生徒情報の更新  変更できるのは名前のみ　※ページネーションなし
     * Route::post
     * @return redirect('/teacher/studentlists'
     */
    public function update(Request $request)
    {
        logger()->debug('<-- teacher/StudentlistController@update');
        //多重送信対策　更新ボタンを連打すると、419ページに遷移する。連打しても、更新が繰り返されるだけなので、今は、使わずコメントアウト
        //$request->session()->regenerateToken();
        global $pagenation;

        
            //リクエストデータを$reqDataに格納　⇒配列形式
            $reqData = $request->all();
                
            //生徒名をセット（オリジナルのバリデーションメッセージにするため、名前をかえる）
            $reqData['studentlist_user_name'] = $reqData['user_name'];
            //バリデーション実行  ※クラスIDは配列形式にする       
            $validator = Validator::make($reqData, [
                'studentlist_user_name' => ['required','max:20'],
            ]);   
                
            //バリデーション失敗時　リダイレクト
            if ($validator->fails()) {
                //成功メッセージをださないため
                session()->flash('flash_message', null);
                return redirect('/teacher/studentlists')
                    ->withErrors($validator);
            }

            //生徒更新情報をセットして保存
            $user =User::find($reqData['user_id']);
            $user -> name = $reqData['studentlist_user_name'];

            //保存ボタン押下＆POSTの時だけ保存実行
            if($request->has('save')){
                try{
                    DB::beginTransaction();
                    $user -> save();
                    session()->flash('flash_message', '「'.$user->name.'」さんの情報を更新しました');
                    DB::commit();
                }catch(Exception $e){
                    logger()->error('■先生メニュー、トライアル生徒の更新でエラー');
                    logger()->error($e);
                    DB::rollback();
                    abort(500);
                }
            } else {
                logger()->error('■先生メニュー、トライアル生徒の更新で保存ボタンをおさずエラー');
                abort(500);
            }
            //リダイレクト
            return redirect('/teacher/studentlists');
    }

    /**
     * ★トライアル生徒パスワードリセット
     * 「リセット」ボタン押下で、モーダル表示
     * Route::post
     * @return redirect('/teacher/studentlists')
     */
    public function reset(Request $request)
    {
        logger()->debug('<-- teacher/StudentlistController@reset');
        //学校データ(生徒パスワード初期値を取得)
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();
        //生徒データ
        $user = User::find($request->input('user_id'));
        //パスワード初期値にセットして保存
        $user -> password = $school -> password_student_init;

        //保存        
        try {
        $user -> save();
        session()->flash('flash_message', $user -> name.'さんのパスワードをリセットしました');
        } catch (Exception $e) {
            logger()->error('■先生メニュー、トライアル生徒のパスワードリセットでエラー');
            logger()->error($e);
            abort(500);
        }
        //リダイレクト        
        return redirect('/teacher/studentlists');
    }
}

