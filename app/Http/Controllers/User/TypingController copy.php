<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Exception;
use App\Models\ScoreHistory;
use App\Models\BattleScoreHistory;
use App\Models\Course;
use App\Models\School;
use App\Models\BestScore;
class TypingController extends Controller
{
    /**
     * 【学校版】タイピング画面（全コース共通）
     *　選んだタブのコースのすべてのコース情報、単語情報を取得。JSでコースごとに振り分ける。
     * 　（理由）タイピング画面の「次へ」「前へ」ボタンでリロードすると、レンダリングで表示時間がかかる。⇒JSで
     * 　　データだけ差し替える（画面のレンダリングはしない）その際、「ベストスコア」「スコア履歴」はAjax通信で取得。
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 【学校版】タイピング画面
     * postとgetがOK⇒基本はPOST　だがSEO的に
     * (1)n-typing.com/free/course　を直接入力すると（get）★の中段
     * (2)n-typing.com/free/course-test　を直接入力すると（get）テストの中1問目
     * (3)n-typing.com/free/course-keytouch　を直接入力すると（get）キータッチの1問目
     * (4)n-typing.com/free/course-map　を直接入力すると（get）地図の1問目
     * メニューから遷移した時だけ使用、「途中でやめる」「次へ」「前へ」ボタンの遷移はajax
     */
    public function index(Request $request)
    {
        //メニューにある音あり、アルファベットありのオプション値   yes か　noがかえる
        $soundData = $request->hasCookie('ntyping_option_sound') ? $request->cookie('ntyping_option_sound') : 'yes';
        $alphabetData = $request->hasCookie('ntyping_option_alphabet') ? $request->cookie('ntyping_option_alphabet') : 'yes';

        //dd($request->all());バトルモード
        if($request->has('battleBtn')){
            //選んだコース
            $course = Course::where('tab','battle')
                ->where('courses.invalid',0)
                ->first();
            $courseId = $course->id;
            //学校データ（ヘッダーで使用）
            $school = School::where('id',auth()->user()->school_int_id)->first();
            $scoretype=$school->scoretype;

            $battleScore=BattleScoreHistory::where('course_id',$courseId)
            ->orderBy('battle_score','desc')
            ->orderBy('created_at','asc')
            ->get();

             //【JSに渡すデータ】選んだコースのタブの全コースに紐づいてる単語（複数）⇒各コースの単語の振り分けはJSで行う             
            $wordData = Course::leftJoin('course_word','courses.id','course_word.course_id')
            ->leftJoin('words','words.id','course_word.word_id')
            ->select('words.word_mana','words.word_kana','words.id','pronunciation_file_name','words.display_order','course_id','tab','words.id as wordId','courses.id as courseId','words.remark')
            ->where('courses.invalid',0)
            ->where('courses.tab', 'battle')        
            ->orderBy('display_order','ASC')
            ->get();      
            //【JDに渡すデータ】前へボタン、次へボタン　前後のコースIDを取得するため、今選んでいるタブの全コースのコースIDを取得
            $coursesArray = Course::where('invalid',0)
            ->where('tab','battle')
            ->select('id','course_name','random','course_category','tab','test_time')
            ->orderBy('display_order','ASC')
            ->get()->toArray();  //配列形式にする、加工がしやすいため

            $user_nickname=auth()->user()->user_nickname;
             //viewの振り分け
             if ($course->course_type=='組込') {
                $ViewFile='user.student.typingbattle';
            } elseif($course->course_type=='テスト') {
                $ViewFile='user.student.typingtestbattle';
            } elseif($course->course_type=='地図') {
                $ViewFile='user.student.typingmapbattle';
            } elseif($course->course_type=='英語') {
                $ViewFile='user.student.typingenglishbattle';
            } elseif($course->course_type=='長文') {
                $ViewFile='user.student.typinglongbattle';
            }
            return view($ViewFile, compact('user_nickname','course', 'battleScore','soundData', 'alphabetData', 'school','coursesArray','scoretype','wordData'));
       
        } else {
            //courseIDを取得したいがforeach内でhiddenが繰り返し使えないので、nameにIDを入れる。4番目（token、sound,alpha,ID）なので、keyで取得
            
            $req=$request->all();
            //dd($req);
            foreach($req as $key => $value){
                $courseId =  $key;
            }
        
        //選んだコース
        $course=Course::where('courses.id',$courseId)->first();

        //学校データ（ヘッダーで使用）
        $school = School::where('id',auth()->user()->school_int_id)->first();
        $scoretype=$school->scoretype;

        
        //ページロード時の初期データ　ベストスコア10件取得、ベストスコアはbp_Rankで1~10までランクづけ
        $scoreData = BestScore::where('user_id',auth()->id())
            ->where('course_id',$course->id)
            ->orderBy('bp_rank','ASC')
            //->orderBy('success_num','desc')
            //->orderBy('best_point_created_at','desc')
            //->take(10)
            ->get();
        
        //ページロード時の初期データ　今日のスコア取得
        $todayScoreData = ScoreHistory::where('user_id', auth()->id())
                        ->where('course_id', $course->id)
                        ->whereDate('created_at', now()->format('Y-m-d'))
                        ->orderBy('created_at', 'desc')
                        ->get();
        
        //【JSに渡すデータ】選んだコースのタブの全コースに紐づいてる単語（複数）⇒各コースの単語の振り分けはJSで行う
        $wordData = Course::leftJoin('course_word','courses.id','course_word.course_id')
                ->leftJoin('words','words.id','course_word.word_id')
                ->select('words.word_mana','words.word_kana','words.id','pronunciation_file_name','words.display_order','course_id','tab','words.id as wordId','courses.id as courseId','words.remark')
                ->where('courses.invalid',0)
                ->where('courses.tab', $course->tab)
                ->where(function($query) use($school){
                    $query->where('courses.school_id', $school->id);
                    $query->orWhere('courses.school_id', null);
                })
                ->orderBy('display_order','ASC')
                ->get();        
       //【JDに渡すデータ】前へボタン、次へボタン　前後のコースIDを取得するため、今選んでいるタブの全コースのコースIDを取得
        $coursesArray = Course::where('invalid',0)
                ->where('tab',$course->tab)
                ->where(function($query) use($school){
                    $query->where('courses.school_id', $school->id);
                    $query->orWhere('courses.school_id', null);
                })
                ->select('id','course_name','random','course_category','tab','test_time')
                ->orderBy('display_order','ASC')
                ->get()->toArray();  //配列形式にする、加工がしやすいため
            
        if ($wordData->isEmpty()) {
            abort(403);
        } else {
            //viewの振り分け
            if ($course->tab=='month' || $course->tab=='star1' || $course->tab=='star2' || $course->tab=='star3' || $course->tab=='add') {
                $ViewFile='user.student.typing';
            } elseif($course->tab=='test' || $course->tab=='key') {
                $ViewFile='user.student.typingtest';
            //長文
            } elseif($course->tab=='long') {
                $ViewFile='user.student.typinglong';
            } elseif($course->tab=='map' || $course->tab=='isan') {
                $ViewFile='user.student.typingmap';
            } elseif($course->tab=='english1' || $course->tab=='english2' || $course->tab=='english3' || $course->tab=='english4' || $course->tab=='english5') {
                $ViewFile='user.student.typingenglish';
            //バレッド専用コース
            } elseif($course->tab=='vld') {
                $ViewFile='user.student.typingvld';
            } 
            return view($ViewFile, compact('course', 'scoreData', 'todayScoreData', 'wordData', 'soundData', 'alphabetData', 'school','coursesArray','scoretype'));
        }
    }
    
    }

    /**
     * 【ajax通信】タイピングの結果を表示　
     * ※共通メソッド　⇒①結果モーダルを閉じた時に呼び出し　②前、次へボタン押下時に呼び出し
     * コースや単語データはJSで処理するため、ここでは呼び出さない。今日のスコアとベストポイントだけ保存＆呼び出し
     * ※tokenがうまく引き継げないため、tokunを例外にしている⇒ここで設定　app\Http\Middleware\VerifyCsrfToken.php
     * 
     */
    public function ajax(Request $request)
    {
        //バレッドの長文も含め、このメソッドで処理する
        //logger()->debug('<-- TypingController@ajax');
        //ajax通信以外はエラー
        if($request->ajax()==false){
            return redirect()->route('403');;
        }
        //Ajax通信でのPOSTリクエストデータ
        $requestAjaxData=$request->all();
        //ddで変数が確認できないので、loggerでlaravel logで確認する
        //logger()->debug($requestAjaxData);
        //ajaxで取得したコースIDから現在のコースデータを取得
        $course_ajax_data=Course::find($requestAjaxData['courseId']);

        if($course_ajax_data->tab =='key'){
            $rank=$requestAjaxData['typingRank'];
        } else {
            $rank=null;
        }
        // スコア履歴テーブルにデータ追加　（ifの理由）「次へ」「前ボタン」の遷移時にもこのメソッドをつかう。この場合、スコア履歴の保存はないため。
        if($requestAjaxData['typingScore'] != null){
            $newScore = new ScoreHistory();
            $newScore->user_id = auth()->id();
            $newScore->course_id =$requestAjaxData['courseId'];
            $newScore->course_name = $course_ajax_data->course_name;
            $newScore->point = $requestAjaxData['typingScore'];//バレッド専用長文は、typingScoreに打数が記録されている
            $newScore->time = $requestAjaxData['typingTime'];
            //$newScore->typing_number = $requestAjaxData['keydownAllCnt'];
            //$newScore->misstyping_number = $requestAjaxData['missType'];            
            $newScore->typing_number = null;
            $newScore->misstyping_number = null;
            $newScore->rank = $rank;
            $newScore->save();

            // ベストスコアテーブルにデータ追加
            $bestScoreData = BestScore::where('user_id',auth()->id())
                            ->where('course_id',$course_ajax_data->id)
                            ->orderBy('best_point', 'desc')
                            ->take(10)
                            ->get();
            DB::beginTransaction();
            try {
                if ($bestScoreData->count() == 10) {
                    // 10件だった場合更新
                    if ($bestScoreData->last()->best_point < $requestAjaxData['typingScore']) {
                        $bestScoreData->last()->update([
                            'best_point' => $requestAjaxData['typingScore'],
                            'rank' => $rank,
                            'best_point_created_at' => now()
                        ]);
                    }
                } else {
                    // 10件ない場合追加
                    $bestScore = new BestScore();
                    $bestScore->user_id = auth()->id();
                    $bestScore->course_id = $course_ajax_data->id;
                    $bestScore->best_point = $requestAjaxData['typingScore'];
                    $bestScore->rank = $rank;
                    $bestScore->best_point_created_at = now();
                    $bestScore->school_id = auth()->user()->school_int_id;
                    $bestScore->save();
                }
                //順位を書き込む
                $raw = 'RANK() OVER(ORDER BY best_point DESC,best_point_created_at DESC) AS rank_result';
                $bestScoreData = BestScore::where('user_id',auth()->id())
                    ->where('course_id', '=', $course_ajax_data->id)
                    ->select('*',DB::raw($raw))
                    ->get();
                foreach ($bestScoreData as $dt){
                    //logger()->debug('$dt->rank_result = '.$dt->rank_result);
                    $bestScoreData->find($dt->id)->update([
                        'bp_rank' => $dt->rank_result,
                    ]);
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                //logger()->error('【学校版】タイピング画面（練習モード）でエラー');
                logger()->error($e);
                abort(500);
            }
        }

        //スコア履歴
        $scoreData = BestScore::where('user_id',auth()->id())
                        ->where('course_id',$course_ajax_data->id)
                        ->orderBy('bp_rank', 'ASC')
                        ->get();
        //今日のスコア
        $todayScoreData = ScoreHistory::where('user_id',auth()->id())
                        ->where('course_id',$course_ajax_data->id)
                        ->whereDate('created_at', now()->format('Y-m-d'))
                        ->select('point','rank','typing_number')
                        ->orderBy('created_at', 'desc')
                        ->get();
        
        //取得データを送信
        return response()->json([$todayScoreData,$scoreData]);             
    }
    /**
     * 【ajax通信】タイピングの結果を表示　
     * バトルモードでのみ使用
     * ※tokenがうまく引き継げないため、tokunを例外にしている⇒ここで設定　app\Http\Middleware\VerifyCsrfToken.php
     * 
     */
    public function ajax_battle(Request $request)
    {
        //バトルモード専用
        //logger()->debug('<-- TypingController@ajax');
        //ajax通信以外はエラー
        if($request->ajax()==false){
            return redirect()->route('403');;
        }
        //Ajax通信でのPOSTリクエストデータ
        $requestAjaxData=$request->all();
        //ddで変数が確認できないので、loggerでlaravel logで確認する
        //logger()->debug($requestAjaxData);
        //ajaxで取得したコースIDから現在のコースデータを取得
        $course_ajax_data=Course::find($requestAjaxData['courseId']);

        
        // battlescore_historyテーブルにデータ追加        
            $newScore = new BattleScoreHistory();
            //$newScore->nickname = $requestAjaxData['nickname'];
            $newScore->nickname =auth()->user()->user_nickname;
            $newScore->course_id =$requestAjaxData['courseId'];
            $newScore->course_name = $course_ajax_data->course_name;
            $newScore->battle_score = $requestAjaxData['typingScore'];
            $newScore->save();

        //スコア履歴
        $scoreData = BattleScoreHistory::where('course_id',$course_ajax_data->id)
                        ->orderBy('battle_score', 'DESC')
                        ->orderBy('created_at', 'ASC')
                        ->get();
        $myScore = $requestAjaxData['typingScore'];
        $totalScores = $scoreData->count(); // 全スコア数
        $rank = 1; // 初期順位
        $myRank = null;
        
        // 各スコアに対してループ
        foreach ($scoreData as $data) {
            if ($data->battle_score == $myScore) {
                $myRank = $rank;
                break;
            }
            $rank++;
        }
        
        //取得データを送信
        //return response()->json([$scoreData,$totalScores,$myRank]); 
        return response()->json([
            'scoreData' => $scoreData,
            'totalScores' => $totalScores,
            'myRank' => $myRank
        ]);
                    
    }
    
}
