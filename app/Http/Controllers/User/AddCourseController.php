<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\School;
use App\Models\Course;
use App\Models\Word;
use App\Models\CourseWord;
use App\Models\AudioFile;
use Exception;

/**
* 先生メニュー　コース追加　※ページネーションなし、トライアルは利用できない
* bledeファイルは学校管理メニューのコース一覧、単語一覧と共通 
* コース一覧：resources\views\layouts\include\addcourse.blade.php
* 単語一覧：resources\views\layouts\include\courseaddword.blade.php
*(1)コース一覧
*(2)コース検索
*(3)コース新規＆更新
*(4)コース削除
*(5)単語一覧
*(6)単語新規＆更新
*(7)単語削除
*/
class AddCourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher.role');
    }
    /**
     * ★コース一覧
     * Route::get  ※学校独自でコースを作れるのは「追加コース」のみ、コースに学校IDが紐づく
     * @return view('user.teacher.addcourse')
     */
    public function index()
    {
        //単語登録のフラッシュメッセージだけ消す
        $message= session('flash_message');
        //単語登録ページのフラッシュメッセージが、このページにも引き継がれてしまっている。
        //それを防ぐため、単語登録のフラッシュメッセージに含まれている「単語」があれば、メッセージを消す処理。
        if(strpos($message,'単語') !== false){
            session()->flash('flash_message', null);
        }
        //学校データ(ヘッダーで使用)
        $school = School::where('schools.id',auth()->user()->school_int_id)->select('name','id','trial')->first();
        //念のため、トライアル先生がgetパラメータで入るのを防ぐため（直接URLを入れるとみえてしまうため）
        if($school->trial === 1){
            logger()->error('■先生メニュー、コース一覧でトライアルクラスが強引にゲットパラメータで入ろうとした時のエラー');
            abort(403);
        }
        //検索ボックス初期値　※有効＆無効は「invalid」カラムを2つにわけている
        $course_name = null;
        $invalid = true; //無効　
        $valid = true; //有効

        //コースデータ　※ログインしている学校のコース　
        $courses = Course::where('school_id',$school->id)
            ->where('tab','add')
            ->orderBy('display_order','ASC')
            ->get();
        //リターンview
        return view('user.teacher.addcourse',compact('school','courses','course_name','invalid','valid'));
    }
    /**
     * ★コース検索「コース名」「有効」「無効」
     * Route::match(['get', 'post']　　ページネーションなし
     * @return view('user.teacher.addcourse')
     */
    public function search(Request $request)
    {
        //成功メッセージを残さないため
        session()->flash('flash_message', null);
        //学校データ(ヘッダーで使用)
        $school = School::where('schools.id',auth()->user()->school_int_id)->select('name','id','trial')->first();

        //念のため、トライアル先生がgetパラメータで入るのを防ぐため（直接URLを入れるとみえてしまうため）
        if($school->trial === 1){
            logger()->error('■先生メニュー、コース一覧（検索）でトライアルクラスが強引にゲットパラメータで入ろうとした時のエラー');
            abort(403);
        }
        //検索値を変数に代入(input の第二引数は、第一引数が空の時につかう値)
        $course_name = $request->input('i_course_name',null);
        $invalid = $request->input('i_invalid',null);
        $valid = $request->input('i_valid',null);

        //検索後のコースデータ
        $courses = Course::where('school_id',$school->id)
            ->where('tab','add')
            ->where('course_name','Like binary',"%$course_name%")
            ->when($valid == '有効' && $invalid == null ,function($q){
                return $q->where('courses.invalid',false);
                })
            ->when($valid == null && $invalid == '無効' ,function($q){
                return $q->where('courses.invalid',true);
            })
            //有効、無効の両方チェックがない⇒コースIDを0にして表示データをなしにする
            ->when($valid == null && $invalid == null ,function($q){
                return $q->where('courses.id',0);
            })
            ->orderBy('display_order','ASC')
            ->get();

        //リターンview
        return view('user.teacher.addcourse',compact('school','courses','course_name','invalid','valid'));
    }
/**
     * ★コースの新規＆更新　※ボタンで切り分け
     * Route::post
     * 新規⇒新しい行を作成　/js/addnew-course.js　⇒保存ボタン押下でメソッド実行
     * 更新⇒該当レコードのカラムをアクティブ　/js/checkedit-course.js　⇒保存ボタンでメソッド実行
     * @return redirect('/teacher/add-course)
     */
    public function new(Request $request)
    {
        //学校データ
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('name','id','trial')->first();
        //念のため、トライアル先生がgetパラメータで入るのを防ぐため（直接URLを入れるとみえてしまうため）
        if($school->trial === 1){
            logger()->error('■先生メニュー、クラス一覧(新規or更新）でトライアルクラスが強引にゲットパラメータで入ろうとした時のエラー');
            abort(403);
        }
        //表示順を半角数字に変換
        $reqData = $request->all();
        if (isset($reqData['display_order'])){
            $reqData['display_order'] = mb_convert_kana($reqData['display_order'], 'n');
        }
        
        //表示順の重複チェックするための準備（タイピング画面にある「次」「前」ボタンで重複があるとエラーするため
        $display_order_unique_count = Course::leftJoin('schools','courses.school_id','schools.id')
            ->where('courses.school_id','=',$school->id)
            ->where('courses.display_order','=', $reqData['display_order'])
            ->count();
        //バリデーションする$reqDataに半角数字に変換された表示順を代入
        $reqData['display_order_unique_count'] = $display_order_unique_count;

        //バリデーション ここから振り分け   
        //【ここから新規】　(新しい行にデータを入力して保存ボタン押下)
        if($request->has('update')){
            //バリデーション実行（重複はカスタムメッセージ）
            //順番重要　(1)表示順カラム　(2)コース名　(3)重複チェック       
            $validator = Validator::make($reqData, [
                'display_order' => ['required','integer','between:0,9999'],
                'course_name' => ['max:30','required'],
                //重複した表示順の数、重複していないと0
                'display_order_unique_count' =>  ['max:0','numeric'],     
            ]);
            //バリデーション失敗時　ソート⇒表示順（エラー波線あるが問題なし）
            //stopOnFirstFailure()は、エラーのあった所でストップする⇒表示順に文字列を入れてエラーした時に、重複エラーメッセージが出るのを防ぐため
            //エラーの赤波線あるが問題なし
            if ($validator->stopOnFirstFailure()->fails()) {
                //新規作成後に再度、新規作成をしてバリデーションに失敗すると、成功のフラッシュメッセージが表示されるのを防ぐため
                session()->flash('flash_message', null);
                //失敗時リダイレクト
                return redirect('/teacher/add-course')
                    ->withErrors($validator);
            }
                //多重送信対策　　保存ボタンはdisabledしているので、連打は想定しないが念のため。
                $request->session()->regenerateToken();

                //新規コースには入力値にかかわらず、コースタイプ（追加）、タブ、学校IDをセット
                $course = new Course();
                $course->tab = 'add'; 
                $course->course_type = '追加';
                $course->school_id = auth()->user()->school_int_id;

                //保存成功時のフラッシュメッセージの内容
                $flashmessage = '」コースを追加しました。次に単語を登録しましょう。';
            
            //【ここから更新】（編集後、保存ボタン押下）
        } else if($request->has('save')){
                //dd($request->all());
                //重複エラーがおこるケース(1)編集ボタン押下だが表示順は変更しない⇒もともとの表示順がすでにあるため、display_order_unique_count は「１」
                //(2)表示順を変更するケース⇒変更する表示順がすでにあるかdisplay_order_unique_count は「0」
                //変更前と、変更後の表示順を調べる
                $edit_before_display_order = Course::find($request->course_id)->display_order;

                //変更前と表示順が同じなら
                if($edit_before_display_order == $request->display_order){
                    $validator = Validator::make($reqData, [
                        'display_order' => ['required','integer','between:0,9999'],
                        'course_name' => ['max:30','required'],
                        //重複した表示順の数、重複していないと1（自分の表示順）
                        'display_order_unique_count' =>  ['max:1','numeric'], 
                    ]);
                } else {
                    //変更前と表示順が異なるなら
                    $validator = Validator::make($reqData, [
                        'display_order' => ['required','integer','between:0,9999'],
                        'course_name' => ['max:30','required'],
                        //重複した表示順の数、重複していないと0
                        'display_order_unique_count' =>  ['max:0','numeric'], 
                    ]);
                }
                //バリデーション失敗時
                if ($validator->stopOnFirstFailure()->fails()) {
                    //新規作成後に再度、新規作成をしてバリデーションに失敗すると、成功のフラッシュメッセージが表示されるのを防ぐため
                    session()->flash('flash_message', null);
                    //失敗時リダイレクト
                    return redirect('/teacher/add-course')
                        ->withErrors($validator);
                }
            //【ここから共通】

            //多重送信対策　　保存ボタンはdisabledしているので、連打は想定しないが念のため。
            //更新で連打すると419ページにとぶ。連打しても更新されるだけなので、とりあえずコメントアウト
            //$request->session()->regenerateToken();

            //編集中のコースデータを取得
            $course =Course::find($request -> input('course_id'));
            //保存成功時のフラッシュメッセージの内容
            $flashmessage = '」コースの情報を更新しました';
        } else {
            logger()->error('●学校管理メニュー、コースの新規or更新で保存ボタンをおさずエラー');
            abort(500);
        } 

        //ここから、共通処理
        $course->invalid = $request->invalid == '1' ? true : false; // 無効
        $course->display_order = $reqData['display_order']; // 表示順
        $course->course_name = $request->course_name; // コース名
        $course->is_free_course = 0; // フリー版に表示⇒表示しないのでfalse
        $course->course_category = 'その他'; // 分類
        $course->course_level = 'その他'; // レベル
        $course->tab = 'add'; // タブ
        $course->random = $request->is_random == '1' ? true : false; // ランダムか
    
        //保存
        try{
            $course->save();
        }catch(\Exception $e){
            logger()->error('●学校管理メニュー、コース新規or編集でエラー');
            logger()->error($e);
            abort(500);
        }
        //成功時のメッセージ（更新コースを表示、新規と更新でメッセージを変える）
        session()->flash('flash_message','「'. $course->course_name.$flashmessage);
        
        //リダイレクト　※リダイレクトの理由⇒新規で更新ボタン押下で多重送信されデータの多重登録がおこるため
        return redirect('/teacher/add-course');
    }
    /**
     * ★コースの削除
     * Route::post
     * 【重要】コース削除⇒紐づく中間テーブル（course_word)と単語テーブル(words)も削除される
     * 単語テーブル削除の際は、音声ファイルの情報を「sudio_files」テーブルに保存する
     * @return redirect('/teacher/add-course')
     */
    public function deleteCourse(Request $request)
    {
        //学校データ
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('name','id','trial')->first();

        //念のため、トライアル先生がgetパラメータで入るのを防ぐため（route はpostだが念のため）
        if($school->trial === 1){
            logger()->error('■先生メニュー、コース一覧（削除）でトライアルクラスが強引に入ろうとした時のエラー');
            abort(500);
        }
        //削除するコースのコースIDが[「$checks」に配列として格納
        $checks = $request->checks;

        //削除チェックを入れてから、編集ボタンを押すと、削除チェックを外し処理をJSでしている（checkedit_course)が、赤い削除ボタンは活性化したまま。
        //つまり、削除チェックなしの状態で、削除ボタンがおせる。
        if($checks === null){
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            return redirect('/teacher/add-course')
                ->withErrors('削除チェックがないため、削除に失敗しました。削除したいデータにチェックをしてから、削除を実行してください。'); 
         } else {
        
        //削除処理（音声ファイルの保存あり）
            DB::beginTransaction();
            try {
                //(1)単語を削除
                Word::leftJoin('course_word','words.id','course_word.word_id')
                    ->whereIn('course_word.course_id', $checks)->delete();
                //(2)単語とコースをつなぐ中間テーブル（course_word）を削除
                CourseWord::whereIn('course_id', $checks)->delete();
                //(3)コースを削除
                Course::whereIn('id', $checks)->delete();

                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                logger()->error('■先生メニュー、コース削除（音声ファイルの保存あり）でエラー');
                logger()->error($e);
                abort(500);
            }
            //削除成功のフラッシュメッセージ
            session()->flash('flash_message', 'コースを削除しました（コースの登録単語も削除しました)');
            
            //リダイレクト
            return redirect('/teacher/add-course');
        }
    }
////////////////////ここから単語一覧////////////////////
    /**
     * ★単語一覧　※コース一覧にある各コースの「単語の登録・編集」ボタン押下の遷移先
     * Route::post　ページネーションはないが、２階層目なので、常にコースIDを持つため
     * @return view('user.teacher.courseaddword)
     */
    public function addWord(Request $request)
    {
        //成功メッセージを残さないため
        session()->flash('flash_message', null);
        
        // コース一覧から「単語の追加・編集」ボタン押下で遷移した時、hiddenで「コースID」を取得 
        //単語一覧から「すべて表示」ボタン押下した時、hiddenで「コースID」を取得
        //どちらから遷移しても　$request->input('course_id')をもっている

        //音声ファイル削除のチェックボックス
        $file_delete = null;
       
        $course_id = $request->input('course_id');
        //単語登録するコースデータ
        $course = Course::find($course_id);
        //学校データ
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();
        $school_id = $school->id;

        //念のため、トライアル先生がgetパラメータで入るのを防ぐため（route はpostだが念のため）
        if($school->trial === 1){
            logger()->error('■先生メニュー、単語一覧でトライアルクラスが強引に入ろうとした時のエラー');
            abort(403);
        }

        //コースに登録されている単語一覧データを取得
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
            ->leftJoin('words','course_word.word_id','words.id')
            ->where('courses.school_id',$school->id)
            ->where('course_word.course_id',$course_id)
            ->orderBy('words.display_order', 'ASC')
            ->orderBy('words.word_kana', 'ASC')
            ->get();

        //リターンview
        return view('user.teacher.courseaddword',compact('school','courseWords','file_delete',
            'course','school_id','course_id'));
    }

    /**
     * ★単語の新規＆更新　※ボタンできりわけ　
     * Route::post　※ページネーションなし
     * 単語一覧は第2階層でコースIDを持ちまわるため、リダイレクト不可
     * /addnew-courseaddword.jsで表に1行追加し、入力データを$requestで受け取る（システム管理、先生メニューと共通JS）
     * 「form　open」と「submit」ボタンは　addnew-courseaddword.js　にある
     * （bladeファイルにある新規ボタンクリックは、Jsで1行追加するのみ）
     * @return view('user.teacher.courseaddword')
     */
    public function addwordAdd(Request $request)
    {
        //多重送信対策　　新規で単語追加して、リロードすると、単語が二重登録されるため、必須。
        //更新の保存ボタンを連打すると419エラーになるのを防ぐため、保存ボタンは押下後、disabled
        $request->session()->regenerateToken();
       
        //単語一覧表示に必要なデータ
        //チェックボックスはチェックなしだと、要素名も返さない　チェックすると「on」が返る
        $file_delete = $request->input('file_delete','off');
        $course_id = $request->input('course_id');
        $school_id = $request->input('school_id');
        $school = School::where('id',auth()->user()->school_int_id)->first();

        //念のため、トライアル先生がgetパラメータで入るのを防ぐため（route はpostだが念のため）
        if($school->trial === 1){
            logger()->error('■先生メニュー、単語追加（新規or更新）でトライアルクラスが強引に入ろうとした時のエラー');
            abort(403);
        }
        
        //リクエストデータを$reqDataに格納　⇒配列形式にしないとバリデーションがうまくいかない
        $reqData = $request->all();
        //表示順を半角数にして、バリデーション実行 
        if (isset($reqData['display_order'])){
                $reqData['display_order'] = mb_convert_kana($reqData['display_order'], 'n');
            }
        $validator = Validator::make($reqData, [
            'display_order' => ['nullable','integer','between:0,9999'],
            'word_mana' => ['required','string','max:40'],
            //カタカナ、漢字不可  「＝」はエラーで登録できない
            'word_kana' => ['required', 'regex:/^[ あ-ん゛゜、。,.!?%()#$-@*+><;:　っゎぁ-ぉゃ-ょーA-Za-z0-9]+$/u','max:40'],
            'audio' => ['nullable','mimes:mp3','max:300'], //音声ファイル
        ]);

        //バリデーション失敗時
        if ($validator->fails()) {
            $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
                ->leftJoin('words','course_word.word_id','words.id')
                ->where('courses.school_id',$school->id)
                ->where('course_word.course_id',$course_id)
                ->orderBy('words.display_order','ASC')
                ->orderBy('words.word_kana', 'ASC')
                ->get();
            $course = Course::find($course_id);

            //バリデーション失敗時のリターンview（コースIDを持ちまわるのでリダイレクト不可）
            return view('user.teacher.courseaddword',compact('school','courseWords',
                'course','school_id','course_id','file_delete'))
                ->withErrors($validator);
        }
        //ここから切り分け
        //新規　（保存ボタン[update」は「addnew-courseaddword.js」にある）
        if($request->has('update')) {
            $word = new Word(); 
            $flashmessage = '」の単語を登録しました';   //成功時のフラッシュメッセージの内容 
        //更新　（編集後、保存ボタン押下）
        } elseif($request->has('save')){
            $word = Word::find($request->id);
            $flashmessage = '」の単語情報を更新しました';  //成功時のフラッシュメッセージの内容
        } else {
        logger()->error('■先生メニュー、単語の新規or更新で保存ボタンをおさずエラー');
        abort(500);
        }

        //更新情報を$wordにセット        
        $word->display_order = $reqData['display_order'];
        $word->word_mana = $reqData['word_mana'];
        $word->word_kana = $reqData['word_kana'];
        
        //チェックボックスはチェックなしだとキーを返さない。
        //音声ファイル名を削除
        if($request ->has('file_delete')) {
            //音声ファイル名を削除
            $word->pronunciation_file_name = null;
        }        

        //音声ファイルの更新
        if($request->has('audio')){
            //ファイル名変換処理
            $fname = sprintf('%04d', $school_id).uniqid(bin2hex(random_bytes(1))).'.mp3';
            logger()->debug('fname = '.$fname);
            $file = request()->file('audio');
            $file->storeAs('public/audio',$fname);
            $word->pronunciation_file_name = $fname;
        }  
        //保存(course_wordテーブルとwordsテーブルを保存)
        DB::beginTransaction();
        try{
            $word->save();
            if($request->has('update')  && $request->isMethod('post')){                
                //コースと単語をつなぐcourse_wordテーブルにcourse_idとword_id をセットして保存
                $courseWord = new CourseWord();
                $courseWord->course_id = $request->course_id;
                $courseWord->word_id = $word->id;
                $courseWord->save();
            }
            DB::commit();
            session()->flash('flash_message', '「'.$word->word_mana.$flashmessage);
        }catch(Exception $e){
            logger()->error('■先生メニュー、新規単語の保存処理（wordsテーブルとcourse_wordテーブル');
            logger()->error($e);
            DB::rollback();
            abort(500);
        }
        //リターンviewに渡すデータ
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
            ->leftJoin('words','course_word.word_id','words.id')
            ->where('courses.school_id',$school->id)
            ->where('course_word.course_id',$course_id)
            ->orderBy('words.display_order', 'ASC')
            ->get();
        $course = Course::find($course_id);

        //リターンview
        return view('user.teacher.courseaddword',compact('school','courseWords','course','school_id','course_id','file_delete'));
    }
    
    /**
     * ★単語の削除　【重要】関連するcourse_wordテーブルのデータも削除（カスケードでなく、プログラムで削除）
     * bladeファイルにhiddenでスクールIDやコースIDをもっているので、削除しないこと！
     * (1)音声ファイルのファイル名を保存　(2)wordテーブルを削除　(3)course_wordテーブルを削除
     * Route::post　ページネーションなし
     * @return view('user.teacher.courseaddword')
     */
    public function deleteWord(Request $request)
    {
        //音声ファイルチェックボックス
        $file_delete = $request->input('file_delete');

        //学校情報　Jsで使うので必要
        $school_id = auth()->user()->school_int_id;
        $school = School::where('schools.id',$school_id)->first();

        //念のため、トライアル先生がgetパラメータで入るのを防ぐため（route はpostだが念のため）
        if($school->trial === 1){
            logger()->error('■先生メニュー、単語追加（削除）でトライアルクラスが強引に入ろうとした時のエラー');
            abort(403);
        }
        //コース情報ID
        $course_id = $request->input('course_id');
        $course= Course::where('courses.id',$course_id)
            -> select('courses.invalid','courses.course_name','courses.random')
            -> first();       
        
        //削除するコースのコースIDが「$checks」に配列として格納
        $checks = $request->checks;
        
        //削除チェックを入れてから、編集ボタンを押すと、削除チェックを外し処理をJSでしている（checkedit_course)が、赤い削除ボタンは活性化したまま。
        //つまり、削除チェックなしの状態で、削除ボタンがおせる。
        if($checks === null){
            //成功メッセージをださないため
            session()->flash('flash_message', null);

            //リターンview渡すデータ　ソートは表示順
            $courseWords = CourseWord::leftJoin('words','course_word.word_id','words.id')
            ->where('course_word.course_id',$course_id)           
            ->orderBy('words.display_order','ASC')
            -> get();
        
            //リターンview    ※2階層目でコースIDを持ちまわるのでリダイレクトできない
            return view('user.teacher.courseaddword',compact('courseWords','course','course_id','school','school_id','file_delete'))
                ->withErrors('削除チェックがないため、削除に失敗しました。削除したいデータにチェックをしてから、削除を実行してください。'); 
         } else {
        //削除処理（音声ファイルの保存あり）
            DB::beginTransaction();
            try {
                //(1)単語を削除
                Word::whereIn('id', $checks)->delete();

                //(2)単語とコースをつなぐ中間テーブル（course_word）を削除
                CourseWord::whereIn('word_id', $checks)->delete();
                DB::commit();
                session()->flash('flash_message', '単語を削除しました');
                
            }catch(Exception $e){
                logger()->error('■先生メニュー、単語削除（中間テーブルも削除）＆音声ファイル保存でエラー');
                logger()->error($e);
                DB::rollback();
                abort(500);
            }
        }
        //リターンview渡すデータ　ソートは表示順
        $courseWords = CourseWord::leftJoin('words','course_word.word_id','words.id')
            ->where('course_word.course_id',$course_id)           
            ->orderBy('words.display_order','ASC')
            -> get();        
        //リターンview    ※2階層目でコースIDを持ちまわるのでリダイレクトできない
        return view('user.teacher.courseaddword',compact('courseWords','course','course_id','school','school_id','file_delete'));
    }
}