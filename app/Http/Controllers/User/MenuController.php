<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\User;
use App\Models\School;
use Illuminate\Support\Facades\DB;
use App\Models\Information;
use Carbon\Carbon;
use App\Models\BestScore;
use Maatwebsite\Excel\Concerns\ToArray;

class MenuController extends Controller
{
    /**
     * 【学校版】タイピングコース一覧
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 【学校版】先生、生徒共通のコース一覧
     *  indexなのに$requestの引数をもつ理由⇒タイピングコースから戻る時にタイピングしたコースのタブをアクティブにするため
     */
    public function index(Request $request)
    {
    //dd($request->all());
       //どのタブを表示するか
       $map_tab_active = null;
       $star1_tab_active = null;
       $star2_tab_active = null;
       $star3_tab_active = null;
       $test_tab_active = null;
       $tuika_tab_active = null;
       $long_tab_active = null;
       $keytouch_tab_active = null;
       $currentmonth_tab_active = null;
       $english1_tab_active = null;
       $english2_tab_active = null;
       $english3_tab_active = null;
       $english4_tab_active = null;
       $english5_tab_active = null;
       $vld_tab_active = null;
       //$isan_tab_active = null;
       //タイピング画面から戻るとき、タイピングしていたコースのタブを表示
       if($request->isMethod('post') && $request->has('gotopbtn')){     
            if($request->tab=='map'){
                $map_tab_active = 'active';//bootstrapのタブを使っている。css のclassに「active」があると、表示される
            } elseif($request->tab=='star1'){
                $star1_tab_active = 'active';
            } elseif($request->tab=='star2'){
                $star2_tab_active = 'active';
            } elseif($request->tab=='star3'){
                $star3_tab_active = 'active';
            } elseif($request->tab=='test'){
                $test_tab_active = 'active';
            } elseif($request->tab=='key'){
                $keytouch_tab_active = 'active';
            } elseif ($request->tab=='month'){
                $currentmonth_tab_active = 'active';
            } elseif ($request->tab=='add'){
                $tuika_tab_active = 'active';
            } elseif ($request->tab=='long'){
                $long_tab_active = 'active';
            } elseif ($request->tab=='english1'){
                $english1_tab_active = 'active';
            } elseif ($request->tab=='english2'){
                $english2_tab_active = 'active';
            } elseif ($request->tab=='english3'){
                $english3_tab_active = 'active';
            } elseif ($request->tab=='english4'){
                $english4_tab_active = 'active';
            } elseif ($request->tab=='english5'){
                $english5_tab_active = 'active';
            } elseif ($request->tab=='vld'){
                $vld_tab_active = 'active';
            } elseif ($request->tab=='vldtest'){
                $vld_tab_active = 'active';
            //} elseif ($request->tab=='isan'){
            //    $isan_tab_active = 'active';
            } else {
                $star1_tab_active = 'active';
            }
        } else {//最初にTOPを開いた時は、★コースを表示
            $star1_tab_active = "active";
        }

       DB::statement("SET SQL_MODE=''");//this is the trick use it just before your query
    
       $school = School::where('id',auth()->user()->school_int_id)->first();
       $sub_query ='SELECT best_point,course_id,best_point_created_at FROM course_with_bestpoint WHERE user_id = '.auth()->user()->id .
       ' AND bp_rank = 1';

       //今月のチャレンジコース
       $courses_chg = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
            ->where('tab','chg')
            ->orderBy('display_order','ASC')
            ->take(12)
            ->get();

        //blade用　今月（当月）のチャレンジコースのコースID
        $kongetu = Carbon::now()->format('n月'); // 例: "4月"
        $kongetu_chg_course=Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
            ->where('tab','chg')
            ->where('course_level',$kongetu)
            ->first();
            //dd($kongetu_chg_course);
   
       //タブ1 今月のタイピング
       $courses1 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
            ->where('tab','month')
            ->orderBy('display_order','ASC')
            ->get();
        //タブ2 ★コース
        $courses2 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
            ->where('tab','star1')
            ->orderBy('display_order','ASC')
            ->get();
        //タブ3　★★コース
        $courses3 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
            ->where('tab','star2')
            ->orderBy('display_order','ASC')
            ->get();
        //タブ4　★★★コース
        $courses4 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','star3')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ5　テストモード
        $courses5 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','test')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ6　追加
        $courses6 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','add')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ7　キータッチ
        $courses7 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','key')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ8　長文        
            $courses8 = Course::where('invalid',0)
                ->where(function($query) use($school){
                    $query->where('school_id', $school->id);
                    $query->orWhere('school_id', null);
                })
                ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                    ->where('tab','long')
                    ->orderBy('display_order','ASC')
                    ->get();
        
        //タブ9　地図
        $courses9 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','map')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ10　英語★
        $courses10 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','english1')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ11　英語★★
        $courses11 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','english2')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ12　英語★★★
        $courses12 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','english3')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ13　バレッド専用
            //ICT試験受験者は検定コースを表示させる。Userテーブルのvldtestカラムに、検定のコースIDが入力
            //されていれば、それが表示する検定コース。それ以外、このカラムはnull。
            //nullをor条件にかけると、うまくいかないので、'なし'にして、該当なしとする。
            $user_id=User::where('id',auth()->user()->id)->first();
            $vldtest_courseId=$user_id->vldtest;
            if($vldtest_courseId==null){
                $vldtest_courseId=='なし';
            }
        //バレッドICT検定問題は、タブは「vldtest」であり、vldでない（vldにすると、全問題表示されるため）
        $courses13 = Course::where('invalid', 0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })         
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','vld')
                ->orWhere('id',$vldtest_courseId)//userテーブルのvldtestカラムにコース番号があれば表示
                ->orderBy('display_order','ASC')
                ->get();
                
        //タブ14　英語★★★★
        $courses14 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','english4')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ15　英語★★★★★
        $courses15 = Course::where('invalid',0)
            ->where(function($query) use($school){
                $query->where('school_id', $school->id);
                $query->orWhere('school_id', null);
            })
            ->leftjoinSub($sub_query,'bp', 'courses.id', 'bp.course_id')
                ->where('tab','english5')
                ->orderBy('display_order','ASC')
                ->get();
        //ランキング用変数　bladeにあるの必要　ランキングを実装しない場合、削除
        $rankCourses=null;
        $position=null;
        $userRole=auth()->user()->role;
        $userNickname=auth()->user()->user_nickname;

        //音あり、アルファベットあり
        $sound = $request->hasCookie('ntyping_option_sound') ? $request->cookie('ntyping_option_sound') : 'yes';
        $alpha = $request->hasCookie('ntyping_option_alphabet') ? $request->cookie('ntyping_option_alphabet') : 'yes';
 
        //入門ラッキーボタン　courses2のidを配列にいれ、bladeに渡す。どれを出すかはJS処理
        foreach ($courses2 as $cs2) {
            $nyumon[]=$cs2->id;
        }
        //中級ラッキーボタン　courses1,courses3,courses7のidを配列にいれ、bladeに渡す。3つをマージする
        foreach ($courses1 as $cs1) {
            $cyukyu1_ids[]=$cs1->id;
        }
        foreach ($courses3 as $cs3) {
            $cyukyu3_ids[]=$cs3->id;
        }
        foreach ($courses9 as $cs9) {
            $cyukyu9_ids[]=$cs9->id;
        }
        $cyukyu = array_merge($cyukyu1_ids, $cyukyu3_ids, $cyukyu9_ids);
        //上級ラッキーボタン　courses4,courses5のidを配列にいれ、bladeに渡す。2つをマージする
        foreach ($courses4 as $cs4) {
            $jokyu4_ids[]=$cs4->id;
        }
        foreach ($courses5 as $cs5) {
            $jokyu5_ids[]=$cs5->id;
        }
        $jokyu = array_merge($jokyu4_ids, $jokyu5_ids);

        //ページ上部の一言ニュース　【重要】blogテーブルのidが「1」の「title」をここに充てる。なので1はinvalidをかならず「１」にしておく！
        $information=Information::where('id','1')
            ->select('title')
            ->first();
        $newstitle=$information->title;

       return view('user.student.menu',compact(
            'userNickname','nyumon','cyukyu','jokyu','newstitle','userRole','rankCourses','position','school','courses1','courses2','courses3','courses4','courses5','courses6','courses7','courses8','courses9','courses10','courses11','courses12','courses13','courses14','courses15','courses_chg','kongetu_chg_course',
            'sound','alpha','map_tab_active','star1_tab_active','star2_tab_active','star3_tab_active','test_tab_active','keytouch_tab_active','currentmonth_tab_active','tuika_tab_active','long_tab_active','english1_tab_active','english2_tab_active','english3_tab_active','vld_tab_active','english4_tab_active','english5_tab_active'));
    }
   
    public function teacherIndex()
    {
        $information = Information::where('invalid',0)->limit(3)->orderBy('created_at','DESC')->get();
        return view('user.teacher.menu',compact('school','information'));
    }
    public function allInformation()
    {
        global $pagenation;
        $information = Information::where('invalid',0)->orderBy('created_at','DESC')
            ->paginate($pagenation);
        return view('user.teacher.menuinformation',compact('school','information'));
    }
}
