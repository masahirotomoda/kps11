<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ScoreHistory;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Exports\Export;
//赤線がでるが、PDFでつかうので必要
use Barryvdh\DomPDF\Facade\Pdf;
use Maatwebsite\Excel\Facades\Excel;

/**
* 先生メニュー　成績履歴一覧、エクセル出力、PDF印刷 ※トライアルとの振り分けあり
* 【重要】PDF印刷は時間がかかるため、ページネーションは100（手動設定）
*(1)スコア履歴一覧
*(2)検索＆エクセル出力
*(3)PDF印刷
*/
class TranscriptPrintController extends Controller
{
    public function __construct()
    {
        //logger()->debug('<--  teacher/TranscriptPrintController@construct');
        $this->middleware('auth');
        $this->middleware('teacher.role');
    }
    /**
     * ★スコア履歴一覧(先生の成績は表示しない)
     * //PDFで出力で時間がかかるため、1ページ100件のページネーションにしている。
     * ※学校とトライアルの振り分けあり（SQL内で処理）⇒メソッドもviewも同じ
     * Route::get
     * @return  view('user.teacher.transcriptprint')
     */
    public function index()
    {
        //logger()->debug('<-- teacher/TranscriptPrintController@index');

        $school = School::where('id','=',auth()->user()->school_int_id)->first();
        //検索ボックスの初期値
        $startDate = null;
        $startTime = null;
        $endDate = null;
        $endTime = null;
        $courseName = null;
        $user_name = null;
        $grade = null;
        $kumi = null;
        $attendanceNo = null;
        $name = null;

        //学年、組のセレクトボックス値
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        //次のSQLでクラスIDが変数で必要なので作成（auth()の形式だとエラー）
         $class_id=auth()->user()->class_id;
        
        //スコア履歴データの取得 「when」で学校とトライアルの切り分け（トライアルがクラス単位）
       
            $scoreHistory = ScoreHistory::leftjoin('users','score_history.user_id','=','users.id')
                ->leftJoin('classes','classes.id','=','users.class_id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                 //トライアルならクラスIDを取得して、クラス単位で表示
                 ->when($school['trial'] === 1 ,function($q) use($class_id){
                    return $q->where('users.class_id',"=",$class_id);
                })
                ->select('kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
                    'score_history.created_at as sh_created_at','users.*','score_history.*')
                    ->where('users.school_int_id',$school->id)
                ->where('users.role','=','生徒')
                ->orderByRaw('score_history.created_at DESC')
                ->paginate(100);       
        
        //共通リターンview　
        return view('user.teacher.transcriptprint',compact('school','startDate','startTime',
            'endDate','endTime','courseName','grade','kumi',
            'grade_selection','kumi_selection','scoreHistory','name'));        
    }

    /**
     * ★スコア履歴　検索＆エクセル出力
     * スコア履歴データの取得(ページネーション100)【理由】PDFで出力で時間がかかるため、1ページ100件にしている。
     * 検索項目：「開始日時」「開始時間」「終了日時」「終了時間」「コース名の一部」「学年」「組」「生徒名の一部」
     * ※学校とトライアルの振り分けあり（SQL内で処理）
     * ※成績履歴の エクセル出力⇒検索結果をエクセル出力するので、同じメソッド内で処理
     * ※エクセルのプロパティ（タイトルや名前など）vendor\phpoffice\phpspreadsheet\src\PhpSpreadsheet\Document\Properties.php
     * Route::match(['get', 'post']　※ページネーションあり
     * @return  view('user.teacher.transcriptprint')
     */
    public function searchAndExcel(Request $request){
        //logger()->debug('<--  teacher/TranscriptPrintController@searchAndExcel');
        //dd($request->all());

        //検索入力値を変数にセット
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        //開始時間が空欄の処理
            $startTime = $request->input('start_time');
            if($startTime === null){
                $startTime  = '00:00';
            //終了時間が空欄の処理
            }
            $endTime = $request->input('end_time');
            if($endTime === null){
                $endTime  = '23:59';
            }
        $courseName = $request->input('course_name');
        $grade = $request->input('grade');
        $kumi = $request->input('kumi');
        $name = $request->input('name');

        //viewに渡すデータ
        $school = School::where('id','=',auth()->user()->school_int_id)->first();
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);



         //次のSQLでクラスIDが変数で必要なので作成（auth()の形式だとエラー）
         $class_id=auth()->user()->class_id;

        //エクセル出力ボタンのnameが「excel」、検索ボタン押下（search）でviewを切り分け
        //エクセル⇒ページネーションにかかわらず出力なのでget、検索⇒paginate()
        //※学校とトライアルはSQLの中のwhenで切り分けしている
        
        //コース名は、scorehistoryテーブルから取得
        $scoreHistory = ScoreHistory::leftjoin('users','score_history.user_id','=','users.id')
            ->leftJoin('classes','classes.id','=','users.class_id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
                'score_history.created_at as sh_created_at','grade.id','kumi.id','users.*','score_history.*')
            //トライアルならクラスIDを取得して、クラス単位で表示
            ->when($school['trial'] === 1 ,function($q) use($class_id){
                return $q->where('users.class_id',"=",$class_id);
            })
            ->when($grade != '' || $grade != null ,function($q) use($grade){
                return $q->where('grade.id','=',$grade);
            })
            ->when($kumi != '' || $kumi != null ,function($q) use($kumi){
                    return $q->where('kumi.id','=',$kumi);
            })
            ->when($name !== null ,function($q) use($name){
                return $q->where('users.name','Like binary',"%$name%");
            })
            ->when( $courseName !== null ,function($q) use( $courseName){
                return $q->where('course_name','Like binary',"%$courseName%");
            })
            ->when($startDate !== null ,function($q) use($startDate){
            return $q->where('score_history.created_at','>',$startDate);
            })
            ->when($startDate !== null && $startTime !== null,function($q) use($startDate,$startTime){
                return $q->where('score_history.created_at','>',$startDate. ' '. $startTime);
            })
            ->when($endDate !== null && $endTime !== null,function($q) use($endDate,$endTime){
                return $q->where('score_history.created_at','<',$endDate. ' '. $endTime);
            })
            ->where('users.school_int_id',$school->id)
            ->where('users.role','生徒')
            ->orderByRaw('grade.name ASC,kumi.name ASC,users.attendance_no ASC,score_history.created_at DESC');

        //検索ボックスの開始時間と終了時間は空欄時、時間（00:00と23:59）を入力しているため、viewに渡すときに空にする
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        
        //エクセルボタン押下時
        if($request->has('excel')){
            //データが何も表示されていない時(バリデーションチェック)
            $scoreHistory= $scoreHistory->get();
            
            if ($scoreHistory->isEmpty()) {
                return redirect('/teacher/transcript-print')
                ->withErrors('エクセルに出力するデータがありません。検索して、データを表示させてから「データの一括ダウンロード(Excel)」ボタンを押してください');
            } else{
            //出力するデータがあればエクセルに出力
            $view = view('excel.transcriptexcel',compact('scoreHistory','school'));
            return Excel::download(new Export($view), '生徒のタイピング履歴データ'.date('Y年m月d日H時i分s秒出力').'.xlsx');
            }
            
        //検索ボタン押下時
        }else if($request->has('search')  || $request->has('page') ){
            $scoreHistory= $scoreHistory->paginate(100);
            return view('user.teacher.transcriptprint',compact('school','startDate','startTime',
            'endDate','endTime','courseName','grade','kumi',
            'grade_selection','kumi_selection','scoreHistory','name'));       
        } else {
            //logger()->error('■先生メニュー、スコア履歴の検索&エクセル出力でボタン押下せずエラー');
            abort(500);
        }        
    }
    /**
     * ★スコア履歴　記録証PDF出力
     * Route::match(['get', 'post']　※【重要】PDF印刷は時間がかかるため、ページネーションは50（手動設定）
     * 記録証はシンプルとカラフルの2種類
     * ライブラリ：\vendor\dompdf\dompdf\README.md　　　vendor\dompdf
     * 　　　フォント：vendor\dompdf\dompdf\lib\fonts　　CSS:vendor\dompdf\dompdf\lib\res\html.css
     * @return   $pdf->stream()
     */
    public function pdf(Request $request){
        //logger()->debug('<--  teacher/TranscriptPrintController@pdf');
        //印刷チェックがあれば（$request['checks']は、チェックしているscorehistoryのIDが配列で格納）
        if(isset($request['checks'])){
                $select = null; //何に使っているか？
                $school = School::where('id','=',auth()->user()->school_int_id)->first();
                //PDFプリントに必要なデータ
                $transcripts = ScoreHistory::leftjoin('users','score_history.user_id','=','users.id')
                    ->leftJoin('courses','courses.id','=','score_history.course_id')
                    ->leftJoin('classes','classes.id','=','users.class_id')
                    ->leftJoin('grade','classes.grade_id','=','grade.id')
                    ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                    ->select('kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
                        'score_history.created_at as sh_created_at','courses.course_category','courses.test_time','users.*','score_history.*')
                    ->where('users.school_int_id',$school->id)
                    ->whereIn('score_history.id',$request->checks)
                    ->orderByRaw('grade.name ASC,kumi.name ASC,users.attendance_no ASC,score_history.created_at DESC')
                    ->get();

                //pdf1（シンプル記録証）ボタン押下
                if($request->has('pdf1') ){
                    $pdf = PDF::loadView('pdf.transcript_scorehistory_simple',compact('transcripts','school'));

                } elseif($request->has('pdf2')){
                //pdf1（カラフル記録証）ボタン押下
                    $pdf = PDF::loadView('pdf.transcript_scorehistory_colorful',compact('transcripts','school'));
                } else {
                    //logger()->error('■先生メニュー、スコア履歴PDF出力でPDFボタン押下せずエラー');
                    abort(500);
                }
                return $pdf->stream();
        } else{
        //印刷チェックが1つもない場合、エラーメッセージでリダイレクト
            //成功フラッシュメッセージが表示されるのを防ぐ
            session()->flash('flash_message', null);
            //リダイレクト
            return redirect('/teacher/transcript-print')
            ->withErrors('「印刷」項目に1つもチェックがありません。印刷したいデータにチェックしてください（印刷項目は表の一番左です）');
        }
    }
}
