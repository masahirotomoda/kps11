<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Clas;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use Exception;

/**
* 先生メニュー ＞　生徒一覧　※トライアルの生徒一覧は別コントローラー
*(1)生徒一覧表示
*(2)検索
*(3)生徒の新規
*(4)生徒の更新
*(5)パスワードリセット
*/
$pagenation = 200;
class StudentlistController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- teacher/StudentlistController@construct');
        $this->middleware('auth');
        $this->middleware('teacher.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★生徒一覧
     * Route::get
     * @return view('user.teacher.studentlist')
     */
    public function index()
    {
        logger()->debug('<-- teacher/StudentlistController@index');
        global $pagenation;
        
        //学校データ（ヘッダーで使用＆パスワードリセット）
        $school = School::find(auth()->user()->school_int_id);   
        
        //【重要】トライアル先生がgetパラメータでトライアル校の全生徒の情報を得られないようにするため。
        if($school->trial === 0){
            //学年、組のセレクトボックス値（モデルで関数作成）
            $grade_selection = Grade::selectListById($school->id);
            $kumi_selection = Kumi::selectListById($school->id);

            //検索ボックス初期値
            $user_name = null;
            $grade_select = null;
            $kumi_select = null;

            //生徒データ
            $users_data = User::leftJoin('classes','users.class_id','classes.id')
                ->leftJoin('grade','classes.grade_id','grade.id')
                ->leftJoin('kumi','classes.kumi_id','kumi.id')
                ->select('users.name as user_name','users.id as user_id','password','users.*',
                    'classes.*','grade.id as grade_id','kumi.id as kumi_id')
                ->where('users.school_int_id','=',auth()->user()->school_int_id)
                ->where('users.role','=','生徒')            
                //whenを使うと、orderByで、分けるとエラーなので、orderByRaw
                ->orderByRaw('CHAR_LENGTH(grade.name) ASC,grade.name ASC,
                        CHAR_LENGTH(kumi.name) ASC, kumi.name ASC, attendance_no ASC');
            
            //全生徒数 【重要】先に　生徒データ（$users = $users_data -> paginate($pagenation);）を記載するとバグが生じる
            $student_num_data = $users_data->get();
            $student_num =$student_num_data->count();

            //表示する生徒データ
            $users = $users_data -> paginate($pagenation);

            //リターンview　学校のview
            return view('user.teacher.studentlist',compact('users','school','grade_selection',
                    'kumi_selection','user_name','grade_select','kumi_select','student_num'));
        
        } else {
        //それ以外
            logger()->error('■先生メニュー、生徒一覧でトライアルフラグが0以外の時のエラー');
            abort(500);
        }
    }

    /**
     * ★検索「学年」「組」「名前」
     * Route::match(['get', 'post']
     * @return view('user.teacher.studentlist')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- teacher/StudentlistController@search');        
        global $pagenation;
        //dd(auth()->user());

        //学校データ（ヘッダーで使用＆トライアルかの確認）
        $school = School::find(auth()->user()->school_int_id);   

        //【重要】トライアル先生がgetパラメータでトライアル校の全生徒の情報を得られないようにするため。
        if($school->trial === 0){
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            //検索値を変数に代入
            $user_name = $request->input('username');
            $grade_select = $request->input('grade');
            $kumi_select = $request->input('kumi');

            //学校データ(パスワード表示、非表示の設定)
            $school = School::where('id','=',auth()->user()->school_int_id)
                ->select('name','id','trial','password_student_init','password_display')->first();
            //検索ボックス、学年と組のセレクトボックス値
            $grade_selection = Grade::selectListById($school->id);
            $kumi_selection = Kumi::selectListById($school->id);

            //検索後の生徒データ
            $users_data = User::leftJoin('classes','users.class_id','=','classes.id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                ->select('users.name as user_name','users.id as user_id','password',
                    'users.school_int_id as school_int_id',
                    'role','class_id','login_account','attendance_no','classes.*','grade.id as grade_id','kumi.id as kumi_id')
                ->where('users.school_int_id','=',auth()->user()->school_int_id)
                ->when($user_name != '' || $user_name != null ,function($q) use($user_name){
                    return $q->where('users.name','Like binary','%'.$user_name.'%');
                })
                ->where('users.role','=','生徒')
                ->when($grade_select != '' || $grade_select != null ,function($q) use($grade_select){
                    return $q->where('grade.id','=',$grade_select);
                })
                ->when($kumi_select != '' || $kumi_select != null ,function($q) use($kumi_select){
                    return $q->where('kumi.id','=',$kumi_select);
                })
                ->orderByRaw('CHAR_LENGTH(grade.name) ASC,grade.name ASC,
                    CHAR_LENGTH(kumi.name) ASC, kumi.name ASC, attendance_no ASC');
                //->paginate($pagenation);

                //検索後の生徒データ
                $users = $users_data -> paginate($pagenation);

                //全生徒数（学校でのみ表示、生徒IDをカウント）※検索後の人数ではない
                $student_num =  User::where('users.school_int_id','=',auth()->user()->school_int_id)
                ->where('users.role','=','生徒') ->select('id') ->count();

                //検索結果後の生徒数
                $student_search_num =$users_data->count();

                //リターンview
                return view('user.teacher.studentlist',compact('users','school','grade_selection',
                    'kumi_selection','user_name','grade_select','kumi_select','student_num','student_search_num'));
        } else {  //それ以外
            logger()->error('■先生メニュー、生徒一覧でトライアルフラグが0以外の時のエラー');
            abort(500);
        }
    }
 
    /**
     * ★新規
     * ※teacher/addnew_student.jsで表に1行追加し、入力データを$requestで受け取る
     * 「form　open」と「submit」ボタンは　addnew_student.js　にある
     * （studentlist.bladeにある新規ボタンクリックで、Jsで1行追加する）
     * Route::match(['get', 'post']　⇒ページネーションがあるので
     * ※POST：処理後リダイレクト　GET:リダイレクト(直接URLをたたくと　/teacher/student-list　リダイレクトされる）　else：abord500
     * @return view('user.teacher.studentlist')
     */
    public function new(Request $request)
    {
        //dd($request->all());
        logger()->debug('<-- teacher/StudentlistController@new');

        //学校データ（ヘッダーで使用＆トライアルかの確認）
        $school = School::find(auth()->user()->school_int_id);
        //念のため、トライアル先生がgetパラメータで入るのを防ぐため
        if($school->trial === 1){
            logger()->error('■先生メニュー、生徒一覧の新規でトライアルフラグが1の時のエラー');
            abort(403);
        }

        //多重送信対策　新規ボタン押下後、ボタンはdisabledになるため、多重送信はされないが、念のため。
        $request->session()->regenerateToken();

        //POSTメソッドで、ページネーション（pageを含まない）していない時
        if($request->isMethod('post') && $request->missing('page')){
                //リクエストデータを$reqDataに格納　⇒配列形式にしないとバリデーションがうまくいかない
                $reqData = $request->all();

                //学校ごとの最大ユーザー数（生徒＋先生-学校管理者1名-先生1名）が超えていないかチェック（学校管理1名と先生1名は含まない）
                
                //学校に登録できるユーザー最大数
                $max_user_num = $school['user_number'];

                //学校管理＆先生＆生徒を含むユーザー数 ※ナレッジ用の隠し学校アカウントがあれば、人数に含めない
                $user_num =  User::where('school_int_id','=',auth()->user()->school_int_id)
                //ナレッジ用、学校管理隠しアカウントがあればカウントしない(隠しアカウントはconfig)
                ->where('users.login_account','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN')) 
                ->count();
                //dd( $user_num);

                // 最大登録数をオーバーしたらバリデーション（requied )チェックするための準備（学校管理1名、先生1名は含まない）
                if($user_num < $max_user_num +2){
                    $reqData['over_max_student_num'] ="OK"; //バリデーションクリア
                } else {
                    $reqData['over_max_student_num'] = null; //バリデーションにひっかかる
                }

                //学年、組のどちらか、または両方未入力なら、 $student_class['id']に'未入力'を代入
                //この処理がないと、学年が未入力の時、「学年は必須です」「学年、組の組み合わせが間違っている」と2つのエラーメッセージがでる。
                if($reqData['grade_id'] ==null || $reqData['kumi_id'] ==null){
                    $reqData['studentlist_clas'] = '未入力';
                    
                //そうでないと（学年、組の両方が入力）クラスIDを取得
                } else {
                    $student_class = Clas::where('school_id','=',auth()->user()->school_int_id)
                        ->where('grade_id',$request -> input('grade_id') ) 
                        ->where('kumi_id',$request -> input('kumi_id') ) 
                        -> first();
                //クラスが存在しないと、$student_class['id']　は何も返さない（要素名が存在しない）ので、nullをセットする　⇒バリデーションでrequiredにかけるため
                    if(isset($student_class)){
                        $reqData['studentlist_clas'] = $student_class['id'];
                    } else {
                        $reqData['studentlist_clas'] = null;
                    }                    
                }
                //dd($student_class);

                //出席番号を半角数字にして$reqDataにセット　※出席番号は本来必須、わからない時はゼロをいれてもらう
                if (isset($reqData['attendance_no'])){
                    $reqData['studentlist_attendance_no'] =  mb_convert_kana($reqData['attendance_no'],"n");
                }        
                
                //生徒名をセット
                $reqData['studentlist_user_name'] = $reqData['user_name'];
                //バリデーション実行  ※クラスIDは配列形式にする       
                $validator = Validator::make($reqData, [
                    'grade_id' => ['required'],
                    'kumi_id' => ['required'],
                    'studentlist_attendance_no' => ['required','digits_between:0,4'],
                    'studentlist_user_name' => ['required','max:20'],
                    'studentlist_clas' => ['required'], //学年＆組が存在しない組み合わせだとエラーにする
                    'over_max_student_num' => ['required'],                    
                ]);   
                
                //バリデーション失敗時　リダイレクト
                if ($validator->fails()) {
                    //成功メッセージをださないため
                    session()->flash('flash_message', null);
                    return redirect('/teacher/student-list')
                        ->withErrors($validator);
                }

                //バリデーション成功時、$newUserに新規生徒情報を追加
                $newUser = new User();
                $newUser->name = $reqData['studentlist_user_name'];  // 名前
                $newUser->role = '生徒';    // ロール
                $newUser->class_id = $reqData['studentlist_clas'];    // クラスID
                $newUser->attendance_no = $reqData['studentlist_attendance_no'];   // 出席番号
                $newUser->password = $school['password_student_init'];  // パスワード
                $newUser->school_int_id = auth()->user()->school_int_id; // 学校ID(数値)
                $newUser->school_id = auth()->user()->school_id;  // 学校ID
                //dd($request->all());
                //保存ボタン押下時、保存処理スタート
                if($request->has('update')){
                    try{
                        DB::beginTransaction();
                        //ログインIDを取得　※10101 ~ 99999 の内
                        while(true){
                            $id_no = str_pad(mt_rand(10101, 99999), 5, 0, STR_PAD_LEFT);
                            $n_no = User::where('login_account','Like',"%$id_no")
                                ->where('school_id',auth()->user()->school_id)
                                ->first();
                            if($n_no === null){
                                break;
                            }
                        }
                        $newUser->login_account = $id_no;  // ログインID        
                        //dd($request->isMethod('post'));
                        //新規生徒を保存
                        $newUser->save();
                        //unset($request->all());
                        session()->flash('flash_message', '「'.$newUser->name.'」さんを登録しました');
                        DB::commit();
                    }catch(Exception $e){
                        logger()->error('先生メニュー、生徒の新規登録でエラー');
                        logger()->error($e);
                        DB::rollback();
                        abort(500);
                    } 
                } else {
                    logger()->error('■先生メニュー、生徒の新規登録ボタンをおしていない時のエラー');
                    abort(500);
                }
            
                //リターンリダイレクト
                return redirect('/teacher/student-list');
        //GETなら        
        } elseif($request->isMethod('get')){
            return redirect('/teacher/student-list');
        //POST でもGETでもない時
        } else {
            logger()->error('■先生メニュー、生徒の新規登録でPOSTorGETどちらでもない時のエラー');
                abort(500);
        }
    }

    /**
     * ★生徒情報の更新
     * /js/user/teacher/checkedit_student.jsで編集を制御（編集ボタン押下でカラムをアクティブ
     * Route::match(['get', 'post']　⇒ページネーションがあるため。
     * @return redirect('/teacher/student-list'
     */
    public function update(Request $request)
    {
        logger()->debug('<-- teacher/StudentlistController@update');

        //学校データ（ヘッダーで使用＆トライアルかの確認）
        $school = School::find(auth()->user()->school_int_id);   
        //念のため、トライアル先生がgetパラメータで入るのを防ぐため
        if($school->trial === 1){
            logger()->error('■先生メニュー、生徒一覧の新規でトライアルフラグが1の時のエラー');
            abort(403);
        }

        //多重送信対策　更新ボタンを連打すると、419ページに遷移する。連打しても、更新が繰り返されるだけなので、今は、使わずコメントアウト
        //$request->session()->regenerateToken();
        global $pagenation;

        //POSTメソッドで、ページネーション（pageを含まない）していない時
        if($request->isMethod('post') && $request->missing('page')){

                //リクエストデータを$reqDataに格納　⇒配列形式
                $reqData = $request->all();

                //全生徒数（viewで必要）
                $student_num =  User::where('school_id','=',auth()->user()->school_id)
                        ->where('users.role','=','生徒') ->count();

                //変更した学年、組からクラスIDを取得
                $student_class = Clas::where('school_id','=',auth()->user()->school_int_id)
                    ->where('grade_id',$request -> input('grade_id') ) 
                    ->where('kumi_id',$request -> input('kumi_id') ) 
                    -> first();

                //出席番号を半角数字にして$reqDataにセット　※出席番号は本来必須、わからない時はゼロをいれてもらう
                if (isset($reqData['attendance_no'])){
                    $reqData['studentlist_attendance_no'] =  mb_convert_kana($reqData['attendance_no'],"n");
                }        
                //クラスが存在しないと、$student_class['id']　は何も返さない（要素名が存在しない）ので、nullをセットする
                if(isset($student_class['id'])){
                    $reqData['studentlist_clas'] = $student_class['id'];
                } else {
                    $reqData['studentlist_clas'] = null;
                }
            //生徒名をセット（オリジナルのバリデーションメッセージにするため、名前をかえる）
                $reqData['studentlist_user_name'] = $reqData['user_name'];
                //バリデーション実行  ※クラスIDは配列形式にする       
                $validator = Validator::make($reqData, [
                    'studentlist_attendance_no' => ['required','digits_between:0,4'],
                    'studentlist_user_name' => ['required','max:20'],
                    'studentlist_clas' => ['required'],
                ]);   
                
                //バリデーション失敗時　リダイレクト
                if ($validator->fails()) {
                    //成功メッセージをださないため
                    session()->flash('flash_message', null);
                    return redirect('/teacher/student-list')
                        ->withErrors($validator);
                }

                //生徒更新情報をセットして保存
                $user =User::find($reqData['user_id']);
                $user -> name = $reqData['studentlist_user_name'];
                $user -> class_id = $reqData['studentlist_clas'];
                $user -> attendance_no = $reqData['studentlist_attendance_no'];

                //保存ボタン押下＆POSTの時だけ保存実行
                if($request->has('save')){
                    try{
                        DB::beginTransaction();
                        $user -> save();
                        session()->flash('flash_message', '「'.$user->name.'」さんの情報を更新しました');
                        DB::commit();
                    }catch(Exception $e){
                        logger()->error('■先生メニュー、生徒の更新でエラー');
                        logger()->error($e);
                        DB::rollback();
                        abort(500);
                    }
                } else {
                    logger()->error('■先生メニュー、生徒の更新で保存ボタンをおさずエラー');
                    abort(500);
                }
                //リダイレクト
                return redirect('/teacher/student-list');

        //GETなら        
        } elseif($request->isMethod('get')){
            return redirect('/teacher/student-list');
        //POST でもGETでもない時
        } else {
            logger()->error('■先生メニュー、生徒の更新でPOSTorGETどちらでもない時のエラー');
                abort(500);
        }
    }

    /**
     * ★生徒パスワードリセット
     * 「リセット」ボタン押下で、モーダル表示
     * Route::post
     * @return redirect('/teacher/student-list')
     */
    public function reset(Request $request)
    {
        logger()->debug('<-- teacher/StudentlistController@reset');
        //学校データ(生徒パスワード初期値を取得)
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();
        //生徒データ
        $user = User::find($request->input('user_id'));
        //パスワード初期値にセットして保存
        $user -> password = $school -> password_student_init;

        //保存        
        try {
        $user -> save();
        session()->flash('flash_message', $user -> name.'さんのパスワードをリセットしました');
        } catch (Exception $e) {
            logger()->error('■先生メニュー、生徒のパスワードリセットでエラー');
            logger()->error($e);
            abort(500);
        }
        //リダイレクト        
        return redirect('/teacher/student-list');
    }
}

