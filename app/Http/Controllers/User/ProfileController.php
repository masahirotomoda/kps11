<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use app\Models\User;
use App\Models\School;
use Exception;

/**
* 先生メニュー
*(1)先生情報
*(2)先生の名前変更
*(3)先生のパスワード変更
*/
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teacher.role');
    }
    /**
     * ★先生情報
     * @return view('user.teacher.profile')
     */
    public function teacherIndex()
    {
        //先生データ
        $user = User::find(auth()->user()->id);
        $avatar=$user->avatar;
        //学校データ（ヘッダーの学校名で使用）
        $school = School::find(auth()->user()->school_int_id);
        //リターンview
        return view('user.teacher.profile',compact('user','school','avatar'));
    }

    /**
     * ★先生の名前変更（先生にはクラス情報がないので、名前とパスワードしか変更できない）
     * @return redirect('/teacher/profile')
     *
    */
    public function update(Request $request)
    {
        //学校データ
        $school = School::find(auth()->user()->school_int_id);
        //リクエストデータを$reqDataに格納　⇒配列形式に変換
        $reqData = $request->all();

        //バリデーション実行
        $validator = Validator::make($reqData, [
        'name' => ['required','max:20'],
        ]);

        //バリデーション失敗時　リダイレクト
        if ($validator->fails()) {
            //成功メッセージがsessionに残っていると失敗しても成功メッセージを出すのを防ぐため
            session()->flash('flash_message', null);
            return redirect('/teacher/profile')
                ->withErrors($validator);
        }
        //先生データ取得
        $user = User::find(auth()->user()->id);
        //先生の更新情報を$userにセット
        $user -> name = $reqData['name'];
        $user -> user_nickname = $reqData['user_nickname'];
        $user -> avatar = $reqData['selected_avatar'];
        try{
            //先生情報の更新後、flashメッセージ
            $user->save();
            session()->flash('flash_message', '先生情報を更新しました');
        } catch(Exception $e) {
            logger()->error('■先生メニュー、先生の名前更新でエラー');
            logger()->error($e);
            abort(500);
        }
        //リダイレクト(情報を保持する必要がないから)
        //return redirect('/teacher/profile');
        return redirect('/teacher/profile');

    }
    /**
    * ★パスワード変更
    *　※パスワードルール：6～20文字、英数字（アルファベットと数字が1文字以上必要）記号不可、大文字小文字はどちらもOK
    * @return redirect('/teacher/profile')
    */
     public function updateTeacherPassword(Request $request)
    {
        logger()->debug('<-- ProfileController@updateTeacherPassword');
        //先生データ
        $user = User::find(auth()->user()->id);

        //パスワードのバリデーション アルファベットと数字を含む6～20文字
        $request->validate([
            'change_password' => [
                'confirmed', //確認パスワードとの相違をチェック
                'required',
                'regex:/\A(?=.*?[a-z])(?=.*?\d)[a-z\d]{6,21}+\z/i',
            ]
        ]);
        try{
            //パスワードの更新後、flashメッセージ
            $user ->password = bcrypt($request['change_password']);
            $user->save();
            session()->flash('flash_message', 'パスワードを変更しました');
        }catch(Exception $e) {
            logger()->error('■先生メニュー、パスワード更新エラー.');
            logger()->error($e);
            abort(500);
        }
            //リダイレクト
            return redirect('/teacher/profile');
    }
}
