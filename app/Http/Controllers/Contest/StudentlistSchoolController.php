<?php

namespace App\Http\Controllers\Contest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Conuser;
use App\Models\School;
use App\Exports\Export;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Writer\Ods\Content;

/**
 * 【コンテスト】学校管理メニュー ＞　コンテスト生徒一覧　　※通常のusersではなく、conusersから取得
 * (1)コンテスト生徒一覧表示 index()
 * (2)検索
*/
class StudentlistSchoolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); 
        $this->middleware('schooladmin.role');
    }
    /**
     * ★コンテスト生徒一覧
     * Route::get
     * @return view('contest.studentlist_school')
     */
    public function index()
    {
        //検索ボックスの初期化
        $user_name =null;
        $grade_select =null;
        $kumi_select = null;
        $attendance_no_select =null;

        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();
        $contest_account=$school->school_id.$school->password_teacher_init;

        //生徒データ
        $users = Conuser::where('school_int_id','=',auth()->user()->school_int_id)
            ->orderBy('created_at','DESC')
            ->paginate(config('configrations.PAGENATION'));            
        $student_num= Conuser::where('school_int_id','=',auth()->user()->school_int_id)->count();
        //リターンview
        return view('contest.studentlist_school',compact('users','contest_account','school','user_name','grade_select','kumi_select','attendance_no_select','student_num'));
    }
    /**
     * ★検索＆エクセル出力　※ボタンで振り分け
     * Route::post　※ページネーションあり
     */
    public function search(Request $request)
    {
        $pagenation=config('configrations.PAGENATION');
        //検索値を変数に代入
        $user_name = $request->input('school_student_name');
        $grade_select = $request->input('school_grade');
        $kumi_select = $request->input('school_kumi');
        $attendance_no_select = $request->input('school_attendance_no');
        
        //学校データ
        $school = School::where('id','=',auth()->user()->school_int_id)->first();
        $contest_account=$school->school_id.$school->password_teacher_init;

        //検索後の生徒データ SQL内「when」で、エクセル出力⇒get（）　検索⇒paginete（）で切り分け
        $users = Conuser::where('school_int_id','=',auth()->user()->school_int_id)
            ->when($user_name != '' || $user_name !=null,function($q) use($user_name){
                return $q->where('name','Like binary',"%$user_name%");
            })
            ->when($grade_select != '' || $grade_select != null ,function($q) use($grade_select){
                return $q->where('grade','Like binary',"%$grade_select%");
            })
            ->when($kumi_select != '' || $kumi_select != null ,function($q) use($kumi_select){
                return $q->where('kumi','Like binary',"%$kumi_select%");
            })
            ->when($attendance_no_select != '' || $attendance_no_select != null ,function($q) use($attendance_no_select){
                return $q->where('attendance_no','=',$attendance_no_select);
            })
            ->orderBy('created_at','DESC')
            ->when($request->has('excel') ,function($q) use($kumi_select){
                return $q->get();
            })
            ->when($request->has('search') || $request->has('page') ,function($q) use($pagenation){
               return $q->paginate($pagenation);
            });

            $student_num= Conuser::where('school_int_id','=',auth()->user()->school_int_id)->count();

            //ここからviewで切り分け
            //エクセル出力ボタン押下時
            if($request->has('excel')){
                if($users->isEmpty()) {
                    return redirect('/school/contest-studentlist')
                    ->withErrors('エクセルに出力するデータが1件もありません。検索をしてデータ表示させてから、「データの一括ダウンロード（Excel）」ボタンを押してください。');
                }

                $view = view('contest.studentlist_excel_school',compact('users','school'));
                //ページを更新しないので表示されない⇒session()->flash('flash_message', 'エクセル出力に成功しました');
                return Excel::download(new Export($view), '生徒一覧'.date('Y年m月d日H時i分s秒出力').'.xlsx');
            //検索ボタン押下時
            //「page」がある理由：検索ボタンをおさずに次ページに移動する時があるため
            }elseif($request->has('search') || $request->has('page')){
                return view('contest.studentlist_school',compact('users','school','contest_account','user_name','grade_select','kumi_select','attendance_no_select','student_num'));
            //かならずelseをつける
            }else{
                    return view('contest.studentlist_school',compact('users','school','contest_account','user_name','grade_select','kumi_select','attendance_no_select','student_num'));
                }
    }
}
