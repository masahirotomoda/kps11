<?php

namespace App\Http\Controllers\contest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Exports\Export;
use App\Models\Conuser;
use App\Models\ScoreHistoryContest;
use PhpOffice\PhpSpreadsheet\Writer\Ods\Content;
use Maatwebsite\Excel\Facades\Excel;
use Exception;

    /**
     * 【コンテスト】システム管理メニュー　＞　スコアリスト管理
     * (1)ユーザー一覧表示
     * (2)検索＆エクセル出力＆学校選択
     * (3)削除
     */
$pagenation =200;
class ScorelistSystemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★コンテストスコア一覧
     * Route::get
     * @return view('contest.scorelist_system')
     */
    public function index()
    {
        global $pagenation;
        //検索ボックスの初期化
        $school_name = null;
        $school_id = null;
        $user_name =null;
        $grade_select =null;
        $kumi_select = null;
        $attendance_no_select =null;
        $school_invalid =null;
        $test_mode =null;
        $practice_mode =null;
        $sort_point_desc=null;

        //スコアデータ（登録日の降順）　※検索で学校、学年、クラスの昇順になるため
        $scoreHistory = ScoreHistoryContest::leftJoin('schools','school_int_id','schools.id')
            ->leftJoin('conusers','conuser_id','conusers.id')
            ->select('schools.school_id as sc_school_id','schools.name as sc_name','password_teacher_init','schools.contest as sc_contest','schools.invalid as sc_invalid',
                    'conusers.name as st_name','grade','kumi','attendance_no','conusers.id as st_id',
                    'score_history_contest.*')
            ->orderBy('score_history_contest.created_at','DESC')
            ->paginate($pagenation);       
        $student_num= Scorehistorycontest::count();
        //リターンview
        return view('contest.scorelist_system',compact('scoreHistory','user_name','grade_select','kumi_select','attendance_no_select','student_num','school_name','school_id','school_invalid','test_mode','practice_mode','sort_point_desc'));
    }
    /**
     * ★検索＆エクセル出力　※ボタンで振り分け
     * Route::post　※ページネーションあり
     */
    public function search(Request $request)
    {
        global $pagenation;
        //dd($request->all());
        //検索値を変数に代入
        $school_name = $request->input('school_name');
        $school_id = $request->input('school_id');
        $user_name = $request->input('school_student_name');
        $grade_select = $request->input('school_grade');
        $kumi_select = $request->input('school_kumi');
        $attendance_no_select = $request->input('school_attendance_no');
        //チェックがないと、何もかえらないため
        if($request->has('school_invalid')){
            $school_invalid='checked';
        } else {
            $school_invalid=null;
        }
        if($request->has('test_mode')){
            $test_mode='checked';
        } else {
            $test_mode=null;
        }
        if($request->has('practice_mode')){
            $practice_mode='checked';
        } else {
            $practice_mode=null;
        }
        if($request->has('sort_point_desc')){
            $sort_point_desc='checked';
        } else {
            $sort_point_desc=null;
        }
        //検索後のスコアデータ SQL内「when」で、エクセル出力⇒get（）　検索⇒paginete（）で切り分け
        $scoreHistory = ScoreHistoryContest::leftJoin('schools','school_int_id','schools.id')
            ->leftJoin('conusers','conuser_id','conusers.id')
            ->select('schools.school_id as sc_school_id','schools.name as sc_name','password_teacher_init','schools.contest as sc_contest','schools.invalid as sc_invalid','schools.id as sc_school_int_id',
                    'conusers.name as st_name','grade','kumi','attendance_no','conusers.id as st_id',
                    'score_history_contest.*')
            ->when($user_name != '' || $user_name !=null,function($q) use($user_name){
                return $q->where('conusers.name','Like binary',"%$user_name%");
            })
            ->when($grade_select != '' || $grade_select != null ,function($q) use($grade_select){
                return $q->where('grade','=',$grade_select);
            })
            ->when($kumi_select != '' || $kumi_select != null ,function($q) use($kumi_select){
                return $q->where('kumi','=',$kumi_select);
            })
            ->when($attendance_no_select != '' || $attendance_no_select != null ,function($q) use($attendance_no_select){
                return $q->where('attendance_no','=',$attendance_no_select);
            })
            ->when($school_name != '' || $school_name != null ,function($q) use($school_name){
                return $q->where('schools.name','Like binary',"%$school_name%");

            })
            ->when($school_id != '' || $school_id != null ,function($q) use($school_id){
                return $q->where('schools.school_id',$school_id);
            })
            ->when($school_invalid == 'checked' ,function($q){
                return $q->where('schools.invalid',1);
            })
            ->when($test_mode == 'checked' ,function($q){
                return $q->where('schools.contest',1);
            })
            ->when($practice_mode == 'checked' ,function($q){
                return $q->where('schools.contest',2);
            })
            ->when($sort_point_desc == 'checked' ,function($q){
                return $q->orderBy('point','DESC');
            })
            ->orderBy('score_history_contest.created_at','DESC')
            ->when($request->has('excel') ,function($q) use($kumi_select){
                return $q->get();
            })
            ->when($request->has('search') || $request->has('page') ,function($q) use($pagenation){
               return $q->paginate($pagenation);
            });

            $student_num= ScoreHistoryContest::count();
            //ここからviewで切り分け
            //エクセル出力ボタン押下時
            if($request->has('excel')){
                if($scoreHistory->isEmpty()) {
                    return redirect('/scorelist_system')
                    ->withErrors('エクセルに出力するデータが1件もありません。検索をしてデータ表示させてから、「データの一括ダウンロード（Excel）」ボタンを押してください。');
                }

                $view = view('contest.scorelist_excel_system',compact('scoreHistory'));
                //ページを更新しないので表示されない⇒session()->flash('flash_message', 'エクセル出力に成功しました');
                return Excel::download(new Export($view), '【システム管理】生徒一覧'.date('Y年m月d日H時i分s秒出力').'.xlsx');
            //検索ボタン押下時
            //「page」がある理由：検索ボタンをおさずに次ページに移動する時があるため
            }elseif($request->has('search') || $request->has('page')){
                return view('contest.scorelist_system',compact('scoreHistory','user_name','grade_select','kumi_select','attendance_no_select','student_num','school_id','school_name','school_invalid','test_mode','practice_mode','sort_point_desc'));
            //かならずelseをつける
            }else{
                    return view('contest.scorelist_system',compact('scoreHistory','user_name','grade_select','kumi_select','attendance_no_select','student_num','school_id','school_name','school_invalid','test_mode','practice_mode','sort_point_desc'));
                }
    }
    /**
     * ★削除
     * Route::post
     * @return redirect('/scorelist_system')
     */
    public function delete(Request $request)
    {
        //削除するコースのコースIDが[「$checks」に配列として格納
        $checks = $request->checks;
        if($checks !== null ){
            DB::beginTransaction();
            try {
                ScoreHistoryContest::whereIn('id', $checks)->delete();
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                abort(500);
            }
            //成功のフラッシュメッセージが表示されるのを防ぐため
            session()->flash('flash_message', '生徒と生徒のスコア履歴を削除しました。');
            //リダイレクト
        
        }
        return redirect('/contest-scorelist');
    }
}
