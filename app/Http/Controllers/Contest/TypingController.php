<?php

namespace App\Http\Controllers\Contest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\School;
use App\Models\ScoreHistoryContest;

class TypingController extends Controller
{
    /**
     * ★【コンテスト】【デモコンテスト】共通　タイピング画面【テストモード】【練習モード】共通3)スコア記録
     */
    public function __construct(){}
    
    /**
     * ★【コンテスト】【デモコンテスト】共通　タイピング画面【テストモード】【練習モード】共通
     * (1)schoolsテーブルのcontestが1⇒本番はテストモード　2⇒本番は練習モード
     * (2)courseテーブルのcontestが1⇒本番のテストモード　2⇒本番の練習モード　3⇒練習（1）練習モード　4⇒練習（2）テストモード
     * (3)小学校モード（従来）、塾教室モード（バレッド）あり。schoolsテーブルのcontestカラムのpassword_displayカラム流用。1⇒塾教室モード、学年⇒教室名、組⇒なし、出席番号⇒生徒番号に変更
     * 　※schoolsとcoursesカラム両方に「contest」カラムがあるので注意！
     */
    public function index($contest_account,request $request)
    {
        //これは、保存しない時の処理、本番で保存する処理は、MenuControllerにあり
        //dd($contest_account); //学校アカウント　（例）kaki7123
        //courseIDを取得したいがforeach内でhiddenが繰り返し使えないので、nameにIDを入れる。2番目（token、ID）なので、keyで取得
        $req=$request->all();
        foreach($req as $key => $value){
            $courseId =  $key;
        }

        //URLの学校アカウントはschoolsテーブルのschool_id と　password_teacher_init  をつなげている
        $contest_school_id = substr($contest_account, 0,8);//学校アカウント（例）kaki7123
        $school = School::where('schools.school_id',$contest_school_id)->first();
        //次の場合、400へ⇒存在しない学校アカウント、間違った学校パスワード初期値、学校が無効、コンテストにチェックなし
        if($school==null || $contest_account !== $contest_school_id.$school->password_teacher_init || $school->invalid == 1 || $school->contest ==0){
            abort(400);
        }
        //選んだコースデータ
        $course=Course::where('id',$courseId)->first();

        //【JSに渡すデータ】選んだコースのタブの全コースに紐づいてる単語（複数）⇒各コースの単語の振り分けはJSで行う
        $wordData = Course::select('words.word_mana','words.word_kana','words.id','words.pronunciation_file_name','words.display_order','course_id')
            ->leftJoin('course_word','courses.id','course_word.course_id')
            ->leftJoin('words','words.id','course_word.word_id')
            ->where('courses.invalid',0)
            ->where('courses.contest', $course->contest)
            ->where(function($query) use($school){
                $query->where('courses.school_id', $school->id);
                $query->orWhere('courses.school_id', null);
            })
            ->orderBy('display_order','ASC')
            ->get();
        //メニューから選んだコースのタブの全コースデータを取得        
        $tabCourseList = Course::where('contest',$course->contest)
            ->where('invalid',0)
            ->where(function($query) use($school){
                $query->where('courses.school_id', $school->id);
                $query->orWhere('courses.school_id', null);
            })
            ->select('id','course_name','random','course_category','test_time','tab')
            ->orderBy('display_order','ASC')
            ->get(); 

        $coursesArray = $tabCourseList->toArray();   //配列形式にする、加工がしやすいため      
        //ナレッジタイピング本体では、toggleボタンで音声ありなし、アルファベットありなしをするが、コンテストではなし。JSで使われているからここで先に設定しておく。
        $soundData='yes';//音有り、なし　yesはあり
        $alphabetData='yes';//アルファベットあり、なし　yesはあり

        //view切り分け
        if ($wordData->isEmpty()) {
            abort(404);
        } elseif($course->contest==1 || $course->contest==3 ) {
            $ViewFile='contest.typingtest';
        } else{
            $ViewFile='contest.typing';
        }        
        return view($ViewFile, compact('coursesArray','course', 'wordData', 'soundData', 'alphabetData','tabCourseList','contest_account','school'));
    }
    
    /**
     * ★【コンテスト】本番コースのみ⇒スコア記録  ※デモコンテストは記録しないのでこのメソッドは使わない。
     *  結果モーダルを閉じたら記録(score_history_contestテーブル)※通常のscore_historyテーブルと分けて、コンテスト履歴用テーブルを新規作成
     * Userもコンテスト用はconusersテーブルを新規作成 
     */
    public function store(Request $request)
    {
        $contest_account=$request->contest_account;
        $newScore = new ScoreHistoryContest();
        $newScore->school_int_id = $request->school_int_id;
        $newScore->conuser_id = $request->student_id;
        $newScore->course_id = $request->course_id;//コースIDとコース名はJS処理のため、JSから取得
        $newScore->course_name = $request->course_name;
        $newScore->point = $request->input('typingresult--score');//スコアデータもJS処理のため、JSから取得
        $newScore->time = $request->input('typingresult--time');//3分、5分10分など、タイピングで設定したタイム
        $newScore->typing_number = 0;
        $newScore->misstyping_number = 0;
        $newScore->save();     
        // タイピング画面へリダイレクト
        return redirect()->route('contestMenu', ['schoolId' => $contest_account]);
    }
}
