<?php

namespace App\Http\Controllers\Contest;

use Illuminate\Http\Request;//赤線でるが必要
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\School;
use App\Models\Conuser;

class MenuController extends Controller
{
    /**
     * 【コンテスト】＆【デモコンテスト】共通タイピングコース一覧
     * タブは3つ①本番コース（テストモードか練習モードかは学校ごとに選べる、schoolsテーブルのcontestカラムが1ならテストモード、2なら練習モード
     * 　　　　②練習コース(1)（練習モード）③練習コース(2)（テストモード）
     * 【このクラスのメソッド】(1)トップページ表示　(2)本番ボタンを押して生徒登録　(3)先生メニュー
     */
    public function __construct(){}

    /**
     * 【コンテスト】【デモコンテスト】共通コース一覧
     *  URL（コンテスト）は　https://n-tayping.com/学校アカウント＋先生初期パスワード/contest
     *  ※コンテスト用学校アカウントは16文字（学校アカウントと先生初期パスワードをつなげる）
     *  URL（デモコンテスト）は　https://n-tayping.com/contest1tesmode1/contest
     *  ※デモコンテストは、学校IDが「4」のタイピング小学校とする。⇒テストモード
     * 　【重要】デモコンテストを練習モードにするには、学校IDを「3」に変更する（ナレッジ小学校）⇒コンテストアカウントは　contest2pramode2
     *  ※塾教室（バレッドなど）モードと小学校モードあり。schoolsテーブルのpassword_displayカラムを流用している。
     * 　【重要】password_displayカラム⇒1 は塾教室モード、学年⇒教室名、クラス⇒なし、出席番号⇒生徒番号に変更している。　0は小学校モード
     */
    public function index($contest_account)
    {
        //logger()->debug('<-- ContestMenuController@index');
        //dd($contest_account);  //パラメータはコンテスト用学校アカウント（例）contest1tesmode1
        
        if($contest_account == 'contest1tesmode1'){//デモ用テストモード
            $contest_school_id = config('configrations.DEMO_CONTEST_INFO.testmode_school_id');//ナレッジ中学校　テストモード          
            $school = School::where('schools.id',$contest_school_id)->first();
        } elseif($contest_account == 'contest2pramode2'){//デモ用練習モード
            $contest_school_id = config('configrations.DEMO_CONTEST_INFO.practicemode_school_id');//ナレッジ小学校　練習モード　configrationsで設定
            $school = School::where('schools.id',$contest_school_id)->first();
        } else {//コンテスト本番
            $contest_school_id = substr($contest_account, 0,8);
            $school = School::where('schools.school_id',$contest_school_id)->first();
            //不正アドレスをはねる
            if($school==null || $contest_account !== $school->school_id.$school->password_teacher_init || $school->invalid == 1 || $school->contest ==0){
                abort(400);
            }
        }
        //本番コースタブに表示させるコースデータを取得（schoolsテーブルのcontestの値が1と2でモード振り分け 
        //教室独自のコンテスト本番問題を作りたい場合、courseテーブルのcontestカラムに、学校ID（ナレッジの場合kaki7123）を入れる。       
        $exam_courses = Course::where('courses.invalid',0)//本番はテストモード
                ->where('courses.contest',$school->contest)
                ->where(function($query) use($school){
                    $query->where('courses.school_id', $school->id);
                    $query->orWhere('courses.school_id', null);
                })
                ->orderBy('courses.display_order','ASC')
                ->get();
        //dd($school->school_id);
        //タブ2の練習コース（練習モード）
        $practice_courses = Course::where('courses.invalid',0)
            ->where('courses.contest','4')
            ->where('courses.course_type', '組込')
            ->orderBy('courses.display_order','ASC')
            ->get();
        //タブ3の練習コース（テストモード）
        $test_courses = Course::where('courses.invalid',0)
            ->where('courses.contest','3')
            ->where('courses.course_type', 'テスト')
            ->orderBy('courses.display_order','ASC')
            ->get();

        return view('contest.menu',compact(
            'school','exam_courses','practice_courses','test_courses','contest_account'));
    }
    
    /**
     * 【コンテスト】＆【デモコンテスト】共通、「本番」ボタン押下⇒生徒情報の登録　※デモコンテストは登録しない
     *  URLは　https://n-tayping.com/学校アカウント＋先生初期パスワード/contest
     *  ※コンテスト用学校アカウントは16文字（先生アカウントと学校初期パスワードをつなげる）
     */
    public function newStudent(request $request){
        //学校情報
        $contest_account=$request->contest_account;
        $contest_school_id = substr($contest_account, 0,8);
        $school = School::where('schools.school_id',$contest_school_id)->first();

        //不正アドレスをはねる処理はなし⇒ postだから
        //バリデーション実行
        if($request->grade == null || $request->kumi == null || $request->attendance_no == null ||$request->student_name == null){
            return redirect('/'.$contest_account.'/contest')
                ->withErrors('「本番スタート」ボタンをおして、もう一度、入力しましょう！');
        }            
        //バリデーション成功時、生徒のインスタンス作成
        $student = new Conuser();
        $student->name = $request->student_name;
        $student->role = '生徒'; 
        $student->grade = $request->grade;
        $student->kumi = $request->kumi;
        $student->attendance_no = $request->attendance_no;
        $student->account = null; 
        $student->pass = null;
        $student->school_int_id = $school->id;
        $student->school_id = $school->school_id;
        //本番コンテストのみ記録（デモコースは記録しない）
        if($request->contest_account !== 'contest1tesmode1' && $request->contest_account !== 'contest2pramode2'){
            $student->save();
        }   
        //コース名のタイトルファイルで使う　resources\views\layouts\include\typingcoursenamecontest.blade.php
        $course = Course::where('courses.invalid',0)
            ->where('courses.contest',$school->contest)
            ->where(function($query) use($school){
                $query->where('courses.school_id', $school->id);
                $query->orWhere('courses.school_id', null);
            })
            ->orderBy('courses.display_order','ASC')
            ->first();
                
        //【JSに渡すデータ】選んだコースのタブの全コースに紐づいてる単語（複数）⇒各コースの単語の振り分けはJSで行う
        $wordData = Course::select('words.word_mana','words.word_kana','words.id','words.pronunciation_file_name','words.display_order','course_id')
            ->leftJoin('course_word','courses.id','=','course_word.course_id')
            ->leftJoin('words','words.id','=','course_word.word_id')
            ->where('courses.invalid',0)
            ->where('courses.contest', $school->contest)
            ->where(function($query) use($school){
                $query->where('courses.school_id', $school->id);
                $query->orWhere('courses.school_id', null);
            })
            ->orderBy('display_order','ASC')
            ->get();

        //本番コースのタブの全コースデータを取得（タイピング画面の左の一覧リスト）
        $tabCourseList = Course::where('contest', $school->contest)
            ->where('invalid',0)
            ->where(function($query) use($school){
                $query->where('courses.school_id', $school->id);
                $query->orWhere('courses.school_id', null);
            })
            ->select('id','course_name','random','course_category','test_time','tab')
            ->orderBy('display_order','ASC')
            ->get();
        $coursesArray = $tabCourseList->toArray();   //配列形式にする、加工がしやすいため      
        //ナレッジタイピング本体では、toggleボタンで音声ありなし、アルファベットありなしをするが、コンテストではなし。JSで使われているからここで先に設定しておく。
        $soundData='no';//音有り、なし　yesはあり
        $alphabetData='yes';//アルファベットあり、なし　yesはあり
        if ($wordData->isEmpty()) {
            abort(400);
        } else {
            if($school->contest==1){
                $viewinfo='contest.typingtest_record';
            } else {
                $viewinfo='contest.typing_record';
            }
            return view($viewinfo,compact('student','school','course','wordData','coursesArray','tabCourseList','soundData','alphabetData','contest_account'));
        }
    }

    /**
     * 【デモコンテスト】「スコア一覧」ボタン押下⇒デモ専用のスコア一覧ページへ⇒本番は学校管理でAuthを通るため、別で作る
     *  URLは　https://n-tayping.com/contest1tesmode1/demoscorelist  or　https://n-tayping.com/contest2pramode2/demoscorelist
     */
    public function demoscorelist($contest_account){
        if($contest_account=='contest1tesmode1' || $contest_account=='contest2pramode2'){
            $school = School::where('schools.id',config('configrations.DEMO_CONTEST_INFO.testmode_school_id'))->first();
            return view('contest.scorelist_demo',compact('contest_account', 'school'));
        } else {
            abort(400);
        }
    }
}
