<?php

namespace App\Http\Controllers\Contest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\ScoreHistoryContest;
use App\Exports\Export;
//赤線がでるが、PDFでつかうので必要
use Barryvdh\DomPDF\Facade\Pdf;
use Maatwebsite\Excel\Facades\Excel;

/**
 * 【コンテスト】学校管理メニュー　＞　スコア履歴一覧、エクセル出力、PDF印刷
 * 【重要】PDF印刷は時間がかかるため、ページネーションは100（手動設定）
 * 【重要】小学校用と塾用にわける（生徒のパスワード表示カラムを再利用。schoolsテーブルのpassword_displayが１なら塾、それ以外は小学校
 * Route::get
 * (1)スコア履歴一覧 【重要】通常のscore_history　ではなく　score_history_contestから取得
 * (2)検索＆エクセル出力
 * (3)PDF印刷
*/
class ScorelistSchoolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
    }
   /**
     * ★【コンテスト】スコア履歴一覧
     * Route::get
     * @return  view('contest.scorelist_school')
     */
    public function index()
    {
        //検索ボックスの初期化
        $user_name =null;
        $grade_select =null;
        $kumi_select = null;
        $attendance_no_select =null;

        //学校情報
        $school = School::where('id','=',auth()->user()->school_int_id)->first();
        $contest_account=$school->school_id.$school->password_teacher_init;
        //検索ボックスの初期値
        $startDate = null;
        $startTime = null;
        $endDate = null;
        $endTime = null;
        $courseName = null;
        $user_name = null;
        $grade = null;
        $kumi = null;
        $attendanceNo = null;
        $name = null;
        $sort_score_desc='';//スコア降順チェックボックス
        
        //スコア履歴データの取得
            $scoreHistory = ScoreHistoryContest::leftJoin('conusers','score_history_contest.conuser_id','=','conusers.id')
                ->select('conusers.id as conusersId','conusers.*','score_history_contest.*')    
                ->where('score_history_contest.school_int_id',auth()->user()->school_int_id)
                ->orderBy('score_history_contest.created_at','DESC')
                ->paginate(100);       
        
            $scoreHistory_num=ScoreHistoryContest::where('score_history_contest.school_int_id',auth()->user()->school_int_id)->count();
        //共通リターンview　
        return view('contest.scorelist_school',compact('school','startDate','startTime','contest_account',
            'endDate','endTime','courseName','grade','kumi','scoreHistory','name','sort_score_desc','scoreHistory_num'));        
    }

    /**
     * ★【コンテスト】スコア履歴　検索＆エクセル出力
     * ※スコア履歴の エクセル出力⇒検索結果をエクセル出力するので、同じメソッド内で処理
     * ※エクセルのプロパティ（タイトルや名前など）vendor\phpoffice\phpspreadsheet\src\PhpSpreadsheet\Document\Properties.php
     * Route::match(['get', 'post']　※重要⇒ページネーションは100
     * @return  view('contest.scorelist_school')
     */
    public function searchAndExcel(Request $request){
    
        //検索入力値を変数にセット
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        //開始時間が空欄の処理
            $startTime = $request->input('start_time');
            if($startTime === null){
                $startTime  = '00:00';
            //終了時間が空欄の処理
            }
            $endTime = $request->input('end_time');
            if($endTime === null){
                $endTime  = '23:59';
            }
        $courseName = $request->input('course_name');
        $grade = $request->input('grade');
        $kumi = $request->input('kumi');
        $name = $request->input('name');
        //セレクトボックスは、選択しないと、何もかえさないため値を代入
        if($request->has('sort_score_desc')){
            $sort_score_desc='checked';
        } else {
            $sort_score_desc='';
        }

        //viewに渡すデータ
        $school = School::where('id',auth()->user()->school_int_id)->first();
        $contest_account=$school->school_id.$school->password_teacher_init;
        $scoreHistory_num=ScoreHistoryContest::where('score_history_contest.school_int_id',auth()->user()->school_int_id)->count();

        //エクセル出力ボタンのnameが「excel」、検索ボタン押下（search）でviewを切り分け
        //エクセル⇒ページネーションにかかわらず出力なのでget、検索⇒paginate()  
        //コース名は、scorehistorycontestテーブルから取得
        if($sort_score_desc=='checked'){
            $scoreHistory = ScoreHistoryContest::leftjoin('conusers','score_history_contest.conuser_id','conusers.id')
                ->when($grade != '' || $grade != null ,function($q) use($grade){
                    return $q->where('conusers.grade','Like binary',"%$grade%");
                })
                ->when($kumi != '' || $kumi != null ,function($q) use($kumi){
                        return $q->where('conusers.kumi','=',$kumi);
                })
                ->when($name !== null ,function($q) use($name){
                    return $q->where('conusers.name','Like binary',"%$name%");
                })
                ->when( $courseName !== null ,function($q) use( $courseName){
                    return $q->where('score_history_contest.course_name','Like binary',"%$courseName%");
                })
                ->when($startDate !== null ,function($q) use($startDate){
                return $q->where('score_history_contest.created_at','>',$startDate);
                })
                ->when($startDate !== null && $startTime !== null,function($q) use($startDate,$startTime){
                    return $q->where('score_history_contest.created_at','>',$startDate. ' '. $startTime);
                })
                ->when($endDate !== null && $endTime !== null,function($q) use($endDate,$endTime){
                    return $q->where('score_history_contest.created_at','<',$endDate. ' '. $endTime);
                })
                ->select('conusers.id as conusersId','conusers.*','score_history_contest.*')           
                ->where('score_history_contest.school_int_id',$school->id)
                ->orderBy('point','DESC')
                ->orderByRaw('CAST(grade as SIGNED) ASC');
        } else {
            $scoreHistory = ScoreHistoryContest::leftjoin('conusers','score_history_contest.conuser_id','conusers.id')
                ->when($grade != '' || $grade != null ,function($q) use($grade){
                    return $q->where('conusers.grade','Like binary',"%$grade%");
                })
                ->when($kumi != '' || $kumi != null ,function($q) use($kumi){
                        return $q->where('conusers.kumi','=',$kumi);
                })
                ->when($name !== null ,function($q) use($name){
                    return $q->where('conusers.name','Like binary',"%$name%");
                })
                ->when( $courseName !== null ,function($q) use( $courseName){
                    return $q->where('score_history_contest.course_name','Like binary',"%$courseName%");
                })
                ->when($startDate !== null ,function($q) use($startDate){
                return $q->where('score_history_contest.created_at','>',$startDate);
                })
                ->when($startDate !== null && $startTime !== null,function($q) use($startDate,$startTime){
                    return $q->where('score_history_contest.created_at','>',$startDate. ' '. $startTime);
                })
                ->when($endDate !== null && $endTime !== null,function($q) use($endDate,$endTime){
                    return $q->where('score_history_contest.created_at','<',$endDate. ' '. $endTime);
                })
                ->select('conusers.id as conusersId','conusers.*','score_history_contest.*')           
                ->where('score_history_contest.school_int_id',$school->id)        
                ->orderBy('score_history_contest.created_at','DESC');
        };
        //dd($sort_score_desc);
        //検索ボックスの開始時間と終了時間は空欄時、時間（00:00と23:59）を入力しているため、viewに渡すときに空にする
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');

        //エクセルボタン押下時
        if($request->has('excel')){
            //データが何も表示されていない時(バリデーションチェック)
            $scoreHistory= $scoreHistory->get();
            if ($scoreHistory->isEmpty()) {
                return redirect('/school/contest-scorelist')
                ->withErrors('エクセルに出力するデータがありません。検索して、データを表示させてから「データの一括ダウンロード(Excel)」ボタンを押してください');
            } else{
            //出力するデータがあればエクセルに出力
            $view = view('contest.scorelist_excel_school',compact('scoreHistory','school','contest_account',));
            return Excel::download(new Export($view), '生徒のタイピング履歴データ'.date('Y年m月d日H時i分s秒出力').'.xlsx');
            }
        //検索ボタン押下時(ページネーション100)
        }else if($request->has('search')  || $request->has('page') ){
            $scoreHistory= $scoreHistory->paginate(100);
            return view('contest.scorelist_school',compact('school','startDate','startTime','contest_account',
            'endDate','endTime','courseName','grade','kumi','scoreHistory','name','sort_score_desc','scoreHistory_num'));       
        } else {
            abort(500);
        }        
    }
    /**
     * ★【コンテスト】スコア履歴　記録証PDF出力
     * Route::match(['get', 'post']　※ページネーションあり
     * 記録証はシンプルとカラフルの2種類
     * ライブラリ：\vendor\dompdf\dompdf\README.md　　　vendor\dompdf
     * フォント：vendor\dompdf\dompdf\lib\fonts　　CSS:vendor\dompdf\dompdf\lib\res\html.css
     * @return   $pdf->stream()
     */
    public function pdf(Request $request){
        //印刷チェックがあれば（$request['checks']は、チェックしているscorehistoryのIDが配列で格納）
        if(isset($request['checks'])){
                $select = null; //何に使っているか？
                $school = School::where('id','=',auth()->user()->school_int_id)->first();
                //PDFプリントに必要なデータ
                $transcripts = ScoreHistoryContest::leftjoin('conusers','score_history_contest.conuser_id','=','conusers.id')
                    ->select('conusers.id as conusersId','conusers.*','score_history_contest.*')    
                    ->where('score_history_contest.school_int_id',$school->id)
                    ->whereIn('score_history_contest.id',$request['checks'])
                    ->orderByRaw('CAST(grade as SIGNED) ASC')
                    ->orderByRaw('CAST(kumi as SIGNED) ASC')
                    ->orderByRaw('CAST(attendance_no as SIGNED) ASC')
                    ->get();

                $comment='タイピングコンテストにおいて、すばらしい記録をおさめたことを賞します。';
                $pdf_view = 'contest.certificate_basic';
                if($request->has('pdf_basic') ){                    
                    $juni='記録証';
                    $comment='タイピングコンテストにおいて、上記の記録をおさめたことを証します。';
                } elseif ($request->has('pdf_yusyou')){
                    $juni='優勝';
                } elseif ($request->has('pdf_junyusyou')){
                    $juni='準優勝';
                } elseif ($request->has('pdf_kin')){
                    $pdf_view = 'contest.certificate_kin';
                    $juni='金賞';
                } elseif ($request->has('pdf_gin')){
                    $pdf_view = 'contest.certificate_gin';
                    $juni='銀賞';
                } elseif ($request->has('pdf_dou')){
                    $pdf_view = 'contest.certificate_dou';
                    $juni='銅賞';
                } elseif ($request->has('pdf_ganbatta')){
                    $juni='がんばったで賞';
                    $comment='あなたは、タイピング練習を大変がんばりましたのでここに賞します。';
                } elseif ($request->has('pdf_1')){
                    $juni='１位';
                } elseif ($request->has('pdf_2')){
                    $juni='２位';
                } elseif ($request->has('pdf_3')){
                    $juni='３位';
                } elseif ($request->has('pdf_4')){
                    $juni='４位';
                } elseif ($request->has('pdf_5')){
                    $juni='５位';
                } elseif ($request->has('pdf_6')){
                    $juni='６位';
                } elseif ($request->has('pdf_7')){
                    $juni='７位';
                } elseif ($request->has('pdf_8')){
                    $juni='８位';
                } elseif ($request->has('pdf_9')){
                    $juni='９位';
                } elseif ($request->has('pdf_10')){
                    $juni='１０位';
                } else {
                    abort(500);
                }
                $pdf = PDF::loadView($pdf_view,compact('transcripts','school','pdf_view','juni','comment'));
                return $pdf->stream();
        } else{
            //印刷チェックが1つもない場合、エラーメッセージでリダイレクト
            return redirect('/school/contest-scorelist')
                ->withErrors('「印刷」項目に1つもチェックがありません。印刷したいデータにチェックしてください（印刷項目は表の一番左です）');
        }
    }
}