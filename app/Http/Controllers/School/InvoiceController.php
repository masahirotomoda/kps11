<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\School;
//赤線がでるが、PDFでつかうので必要
use Barryvdh\DomPDF\Facade\Pdf;

/**
 * 学校管理メニュー　＞　請求書管理
 * 【重要】
 * Route::get
 * (1)請求書一覧
 * (2)検索
 * (3)PDF印刷
*/
class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
    }
   /**
     * ★請求書一覧
     * Route::get
     * @return  view('school.invoice')
     */
    public function index()
    {
        $school = School::where('id',auth()->user()->school_int_id)->first();
        
        //スコア履歴データの取得(ページネーションなし)
        $invoices = Invoice::leftjoin('schools','invoices.school_int_id','schools.id')
            ->select('invoices.*','schools.name','schools.invoice')
            ->where('schools.invoice',1)
            ->where('invoices.show_invoice',1)
            ->where('schools.id',auth()->user()->school_int_id)
            ->orderBy('invoices.seikyu_day','DESC')
            ->get();
        
        //共通リターンview　
        return view('school.invoice',compact('school','invoices'));        
    }

    /**
     * ★請求書、受領書　PDF出力
     * ライブラリ：\vendor\dompdf\dompdf\README.md　　　vendor\dompdf
     * フォント：vendor\dompdf\dompdf\lib\fonts　　CSS:vendor\dompdf\dompdf\lib\res\html.css
     * @return   $pdf->stream()
     */
    public function pdf(Request $request){
//dd($request->all());
        //請求書チェックがあれば（$request['checks']は、チェックしている請求書のIDが配列で格納）        
        //pdf1（請求書）ボタン押下
        if($request->has('pdf1') && isset($request['checks1']) ){
            $school = School::where('id',auth()->user()->school_int_id)->first();
            //PDFプリントに必要なデータ
            $invoices = Invoice::leftjoin('schools','invoices.school_int_id','schools.id')
                ->where('schools.invoice',1)
                ->where('schools.id',auth()->user()->school_int_id)
                ->where('invoices.show_invoice',1)
                ->whereIn('invoices.id',$request->checks1)
                ->select('invoices.*','schools.name')
                ->orderBy('invoices.seikyu_day','DESC')
                ->get();

            $pdf = PDF::loadView('pdf.transcript_invoice',compact('invoices'));
            return $pdf->stream();
        } elseif($request->has('pdf2') && isset($request['checks2']) ){
            $school = School::where('id',auth()->user()->school_int_id)->first();
            //PDFプリントに必要なデータ
            $invoices = Invoice::leftjoin('schools','invoices.school_int_id','schools.id')
                ->where('schools.invoice',1)
                ->where('schools.id',auth()->user()->school_int_id)
                ->where('invoices.show_receipt',1)
                ->whereIn('invoices.id',$request->checks2)
                ->select('invoices.*','schools.name')
                ->orderBy('invoices.seikyu_day','DESC')
                ->get();
                //dd($invoices);
            if($invoices==null){
                return redirect('/school/invoice')->withErrors('まだ、受領書の準備ができていません。');
            }

            $pdf = PDF::loadView('pdf.transcript_receipt',compact('invoices'));
            return $pdf->stream();
        } else {
            return redirect('/school/invoice');
        }
    }
}