<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\Information;

/**
* 学校管理メニュー ＞　メインメニュー
*(1)メインメニュー　※お知らせは、既定のページネーションをつかわない
*(2)すべてのお知らせ
*/
$pagenation = 200;
class MenuController extends Controller
{
    public function __construct()
    {
        //logger()->debug('<-- school/MenuController@construct');
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
    }

    /**
     * ★メインメニュー
     * Route::get
     * @return view('school.menu')
     */
    public function index()
    {
        //logger()->debug('<-- school/MenuController@index');

        //学校データ（ヘッダーで使用、学校名）
        $school = School::where('schools.id',auth()->user()->school_int_id)->first();
        //コンテスト用コンテストアカウント（ヘッダーで使用）
        $contest_account = $school->school_id.$school->password_teacher_init;

        //新着情報（最新2件,公開フラグあり）
        $information = Information::limit(2)
            ->where('invalid','=','0')
            ->orderBy('created_at','DESC')
            ->get();

        //リターンview
        return view('school.menu',compact('school','contest_account','information'));
    }
    /**
     * ★メインメニュー　すべてのお知らせボタン押下（１件目から表示）
     * Route::get
     * ページネーション　30にしている
     * @return view('school.menuinformation')
     */
    public function allinfo()
    {
        //logger()->debug('<-- MenuController@allInformation');
        //学校データ（ヘッダーで使用）
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('name')->first();
        //コンテスト用コンテストアカウント（ヘッダーで使用）
        $contest_account = $school->school_id.$school->password_teacher_init;

        //お知らせ
        //4件目から表示させたいができない　【理由】skip()（offsetと同じ）で4件目を指定できるが、paginationすると、paginationのoffsetに上書きされる。
        //get()で、絞り込んだものを変数に入れて、paginationしてもエラー（get()だとcollection型で、ページネーションのメソッドはないから)
        //【解決】bladeファイルで($loop->index >2)で3件目から表示させる
        //⇒しかし、ページネーションで次ページや不具合発生　【最終的】すべて表示で1件目～表示させる
        $information = Information::where('invalid','<>',1)
            ->orderBy('created_at','DESC')
            ->paginate(30);

        //リターンview
        return view('school.menuinformation',compact('school','contest_account','information'));
    }

}
