<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Clas;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Exports\Export;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Writer\Ods\Content;
use Exception;

/**
 *  学校管理メニュー ＞　生徒一覧　　※ページネーションあり
 * (1)生徒一覧表示
 * (2)検索＆エクセル出力
 * (3)生徒の新規＆更新
 * (4)生徒の削除
 * (5)生徒のパスワードリセット
 * (6)本日の登録生徒（インポート後の登録生徒確認用）
 * (7)エクセル一括登録
 * (8)エクセル一括登録用のテンプレートダウンロード
*/
$pagenation = 200;
class StudentlistController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- school/StudentlistController@construct');
        $this->middleware('auth'); //ログインしているか
        $this->middleware('schooladmin.role');  //権限チェック
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★生徒一覧
     * Route::match(['get', 'post']※クラス一覧から学年、組のパラメータをもって遷移するケースもあるためPOSTも可
     * @return view('school.studentlist')
     */
    public function index()
    {
        logger()->debug('<-- school/StudentlistController@index');
        global $pagenation;
        //学校情報（ヘッダー＆パスワードリセット値）
        $school = School::where('schools.id','=',auth()->user()->school_int_id)
            ->select('name','id','password_student_init','password_display')->first();

        //学年、組のセレクトボックス値（モデルで関数作成）
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        //検索ボックスの既定値
        $user_name = null;
        $grade_select = null;
        $kumi_select = null;

        //生徒データ（並べ替えは文字列でも学年や組が昇順になるようにする）
        $users = User::leftJoin('classes','users.class_id','=','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('users.name as user_name','users.id as user_id','users.school_int_id as school_int_id','users.school_id as school_id','users.created_at as created_at',
                'role','class_id','login_account','attendance_no','grade.id as grade_id','kumi.id as kumi_id',
                'grade.name as grade_name','kumi.name as kumi_name','users.password as password')
            ->where('users.school_id','=',auth()->user()->school_id)
            ->where('users.role','=','生徒')
            ->orderByRaw('CHAR_LENGTH(grade.name) ASC,grade.name ASC,
                CHAR_LENGTH(kumi.name) ASC, kumi.name ASC, attendance_no ASC')
            ->paginate($pagenation);

        //リターンview
        return view('school.studentlist',compact('users','school','grade_selection',
                'kumi_selection','user_name','grade_select','kumi_select'));
    }
    /**
     * ★検索＆エクセル出力　※ボタンで振り分け 　※生徒一覧からの遷移あり（学年、組をPOSTする）
     * 検索項目：「学年」「組」「生徒名」
     * Route::match(['get', 'post']　※ページネーションあり
     * @return 検索ボタン押下時　view('school.teacherlist')
     * @return view('school.studentlist')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- school/StudentlistController@search');
        global $pagenation;
        //dd($request->all());

        //成功メッセージをださないため
        session()->flash('flash_message', null);

        //検索値を変数に代入
        $user_name = $request->input('user_name_search');
        $grade_select = $request->input('grade');
        $kumi_select = $request->input('kumi');
        
        //学校データ
        $school = School::where('id','=',auth()->user()->school_int_id)->select('name','id','password_display')->first();

        //検索ボックス、学年と組のセレクトボックス値
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        //検索後の生徒データ（並べ替えは文字列でも学年や組が昇順になるようにする）
        //SQL内「when」で、エクセル出力⇒get（）　検索⇒paginete（）で切り分けしている
        $users = User::leftJoin('classes','users.class_id','=','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('users.name as user_name','users.id as user_id','users.school_int_id as school_int_id','users.school_id as school_id','users.created_at as created_at',
                'role','class_id','login_account','attendance_no','grade.id as grade_id','kumi.id as kumi_id',
                'grade.name as grade_name','kumi.name as kumi_name','users.password as password')
            ->where('users.school_int_id','=',auth()->user()->school_int_id)
            ->when($user_name != '' || $user_name !=null,function($q) use($user_name){
                return $q->where('users.name','Like binary',"%$user_name%");
            })
            ->where('users.role','=','生徒')
            ->when($grade_select != '' || $grade_select != null ,function($q) use($grade_select){
            return $q->where('grade.id','=',$grade_select);
            })
            ->when($kumi_select != '' || $kumi_select != null ,function($q) use($kumi_select){
                return $q->where('kumi.id','=',$kumi_select);
            })
            ->orderByRaw('CHAR_LENGTH(grade.name) ASC,grade.name ASC,
                CHAR_LENGTH(kumi.name) ASC, kumi.name ASC, attendance_no ASC')
            ->when($request->has('excel') ,function($q) use($kumi_select){
                return $q->get();
            })
            ->when($request->has('search') || $request->has('page') ,function($q) use($pagenation){
               return $q->paginate($pagenation);
            });

            //ここからviewで切り分け
            //エクセル出力ボタン押下時
            if($request->has('excel')){
                if($users->isEmpty()) {
                    return redirect('/school/studentlist')
                    ->withErrors('エクセルに出力するデータが1件もありません。検索をしてデータ表示させてから、「データの一括ダウンロード（Excel）」ボタンを押してください。');
                }

                $view = view('school.studentlistexcel',compact('users'));
                //ページを更新しないので表示されない⇒session()->flash('flash_message', 'エクセル出力に成功しました');
                return Excel::download(new Export($view), '生徒一覧'.date('Y年m月d日H時i分s秒出力').'.xlsx');
            //検索ボタン押下時
            //「page」がある理由：検索ボタンをおさずに次ページに移動する時があるため
            }elseif($request->has('search') || $request->has('page')){
                return view('school.studentlist',compact('users','school','grade_selection',
                    'kumi_selection','user_name','grade_select','kumi_select'));
            //かならずelseをつける
            }else{
                    return view('school.studentlist',compact('users','school','grade_selection',
                    'kumi_selection','user_name','grade_select','kumi_select'));
                }
    }
    /**
     * ★生徒の新規＆更新
     * school/addnew_student.jsで表に1行追加し、入力データを$requestで受け取る
     * 「form　open」と「submit」ボタンは　addnew_student.js　にある
     * （studentlist.bladeにある新規ボタンクリックで、Jsで1行追加するのみ）
     * Route::match(['get', 'post']　⇒ページネーションがあるので
     *
     * @return view('school.studentlist')
     * ※POST：処理後リダイレクト　GET:リダイレクト(直接URLをたたくと　/school/studentlist　リダイレクトされる）　else：abord500
     */
    public function new(Request $request)
    {
        logger()->debug('<-- school/StudentlistController@new');
        global $pagenation;

        //多重送信対策　新規ボタン押下後、ボタンはdisabledになるため、多重送信はされないが、念のため。
        $request->session()->regenerateToken();

        //POSTメソッドで、ページネーション（pageを含まない）していない時
        if($request->isMethod('post') && $request->missing('page')){
                //リクエストデータを$reqDataに格納　⇒配列形式にしないとバリデーションがうまくいかない
                $reqData = $request->all();

                // 学校データ
                $school = School::find(auth()->user()->school_int_id);

                //学校ごとの最大ユーザー数（生徒＋先生-学校管理者1名-先生1名）が超えていないかチェック（学校管理1名と先生1名は含まない）
                //最大ユーザー数
                $max_user_num = $school['user_number'];
                //現在のユーザー数
                $user_num =  User::where('school_id','=',auth()->user()->school_id)
                    //ナレッジ用、学校管理隠しアカウントがあればカウントしない
                    ->where('users.login_account','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN')) 
                    ->select('id') ->count();
                //dd($user_num);

                // $reqDataでバリデーション（requied )チェックするための準備（学校管理1名、先生1名は含まない）
                if($user_num < $max_user_num +2){
                $reqData['over_max_student_num'] ="OK";
                } else {
                $reqData['over_max_student_num'] = null;
                }

                //出席番号を半角数字にして$reqDataにセット　※出席番号は本来必須、わからない時はゼロをいれてもらう
                $reqData['studentlist_attendance_no'] =  mb_convert_kana($reqData['attendance_no'],"n");

                //学年、組のどちらか、または両方未入力なら、 $student_class['id']に'未入力'を代入
                //この処理がないと、学年が未入力の時、「学年は必須です」「学年、組の組み合わせが間違っている」と2つのエラーメッセージがでる。
                if($reqData['grade_id'] ==null || $reqData['kumi_id'] ==null){
                    $reqData['studentlist_clas'] = '未入力';

                //そうでないと（学年、組の両方が入力）クラスIDを取得
                } else {
                    $student_class = Clas::where('school_id','=',auth()->user()->school_int_id)
                        ->where('grade_id',$request -> input('grade_id') )
                        ->where('kumi_id',$request -> input('kumi_id') )
                        -> first();
                //クラスが存在しないと、$student_class['id']　は何も返さない（要素名が存在しない）ので、nullをセットする　⇒バリデーションでrequiredにかけるため
                    if(isset($student_class)){
                        $reqData['studentlist_clas'] = $student_class['id'];
                    } else {
                        $reqData['studentlist_clas'] = null;
                    }
                }

                //生徒名をセット
                $reqData['studentlist_user_name'] = $reqData['student_name'];
                //バリデーション実行  ※クラスIDは配列形式にする
                $validator = Validator::make($reqData, [
                    'grade_id' => ['required'],
                    'kumi_id' => ['required'],
                    'studentlist_attendance_no' => ['required','digits_between:0,4'],
                    'studentlist_user_name' => ['required','max:20'],
                    'studentlist_clas' => ['required'],
                    'over_max_student_num' => ['required'],
                ]);

                //バリデーション失敗時　リダイレクト
                if ($validator->fails()) {
                    //成功メッセージをださないため
                    session()->flash('flash_message', null);
                    return redirect('/school/studentlist')
                        ->withErrors($validator);
                }
                //バリデーション成功時、ここから新規と更新の切り分け
                //新規
                if($request->has('update')){
                    //ユーザーのインスタンス作成
                    $User = new User();
                    $User->role = '生徒'; // ロール
                    $User->password = $school->password_student_init; // パスワード
                    $User->school_int_id = auth()->user()->school_int_id; // 学校ID(数値)
                    $User->school_id = auth()->user()->school_id; // 学校ID

                    //ログインIDをランダムに発行（重複しないまで繰り返す）
                    //ログインIDを取得　※10101 ~ 99999 の内
                    while(true){
                        $id_no = str_pad(mt_rand(10101, 99999), 5, 0, STR_PAD_LEFT);
                        $n_no = User::where('login_account','Like',"%$id_no")
                            ->where('school_id',auth()->user()->school_id)
                            ->first();
                        if($n_no === null){
                            break;
                        }
                    }
                    $User->login_account = $id_no; // ログインID

                    //保存成功時のフラッシュメッセージの内容
                    $flashmessage = '」さんを登録しました';

                //更新（編集後、保存ボタン押下）
                } elseif($request->has('save')){
                    $User = User::find($request->user_id);
                    //保存成功時のフラッシュメッセージの内容
            $flashmessage = '」さんの情報を更新しました';
                } else {
                    logger()->error('●学校管理メニュー、生徒の新規or更新ボタン押下せずにエラー');
                    abort(500);
                }
                //ここから共通処理（名前、クラス、出席番号は共通）
                $User->name = $reqData['student_name'];
                $User->class_id = $reqData['studentlist_clas'];
                $User->attendance_no = $reqData['studentlist_attendance_no'];

                try{
                    DB::beginTransaction();
                        $User->save();
                        session()->flash('flash_message', '生徒を登録しました');
                    DB::commit();
                }catch(Exception $e){
                    logger()->error('学校管理メニュー、生徒の新規or更新でエラー');
                    logger()->error($e);
                    DB::rollback();
                    abort(500);
                }

                //成功時のメッセージ（更新コースを表示、新規と更新でメッセージを変える）
                session()->flash('flash_message','「'. $User->name.$flashmessage);

                //リダイレクト　※多重送信を防ぐためリダイレクト
                return redirect('/school/studentlist');

        //GETなら
        } elseif($request->isMethod('get')){
            return redirect('/school/studentlist');
        //POST でもGETでもない時
        } else {
            logger()->error('●学校管理メニュー、生徒の新規登録でPOSTorGETどちらでもない時のエラー');
                abort(500);
        }
    }

    /**
     * ★生徒の削除
     * Route::post
     * 【重要】score_history とcourse_with_bestpoint にある生徒データも削除⇒DBでカスケードデリート
     * checkDelBtn.jsでチェックしているユーザーID取得
     *
     * @return redirect('/school/studentlist')
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- school/StudentlistController@delete');

        //dd($request->checks);　//チェックされたuserのIDを配列で取得
        $checks = $request->checks;
        //削除チェックを入れてから、編集ボタンを押すと、削除チェックを外し処理をJSでしている（checkedit_teacher)が、赤い削除ボタンは活性化したまま。
        //つまり、削除チェックなしの状態で、削除ボタンがおせる。
        if($checks === null){
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            return redirect('/school/studentlist')
                ->withErrors('削除チェックがないため、削除に失敗しました。削除したいデータにチェックをしてから、削除を実行してください。'); 
         } else {
             //ここから削除実行
            try{
                User::whereIn('id', $checks)->delete();
            } catch (Exception $e) {
                logger()->error('●学校管理メニュー、生徒の削除でエラー');
                logger()->error($e);
                abort(500);
            }
            //削除成功時、flashメッセージ
            session()->flash('flash_message', '生徒情報を削除しました');
        }
            //リダイレクト
            return redirect('/school/studentlist');
    }

    /**
     * ★生徒のパスワードリセット
     * Route::post
     * @return redirect('/school/studentlist')
     */
    public function resetPassword(Request $request)
    {
        logger()->debug('<-- school/StudentlistController@resetPassword');

        //選んだ生徒データ
        $user = User::find($request->user_id);
        //生徒のパスワード初期値
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('password_student_init')->first();
        //パスワード初期値をハッシュ化
        $user->password = $school->password_student_init;
        //保存
        try{
            $user->save();
        } catch (Exception $e) {
            logger()->error('●学校管理メニュー、先生のパスワードリセットでエラー');
            logger()->error($e);
            abort(500);
        }
        //削除成功時、flashメッセージ
        session()->flash('flash_message', $user -> name.'さんのパスワードをリセットしました');
        //リダイレクト
        return redirect('/school/studentlist');
    }
    /**
     * ★本日の登録生徒一覧（インポートした生徒の確認）
     * 
     * @return view('school.studentlist')
     */
    public function addstudent(){
        logger()->debug('<-- school/StudentlistController@addstudent');
        global $pagenation;
        
        //学校情報
        $school = School::where('schools.id','=',auth()->user()->school_int_id)
            ->select('id','name','password_student_init')->first();

        //学年、組のセレクトボックス値（モデルで関数作成）
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        //検索ボックスの既定値
        $user_name = null;

        //編集中の学年、組のセレクトボックスの既定値
        $grade_select = null;
        $kumi_select = null;

        //今日
        $add_day=date("Y/m/d");
        $users_data = User::leftJoin('classes','users.class_id','=','classes.id')
        ->leftJoin('grade','classes.grade_id','=','grade.id')
        ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
        ->select('users.name as user_name','users.id as user_id','users.school_int_id as school_int_id','users.school_id as school_id','users.created_at as created_at',
            'role','class_id','login_account','attendance_no','grade.id as grade_id','kumi.id as kumi_id',
            'grade.name as grade_name','kumi.name as kumi_name','users.password as password')
        ->where('users.school_id','=',auth()->user()->school_id)
        ->where('users.role','=','生徒')
        ->where('users.created_at','>',$add_day)
        ->orderBy('users.created_at','DESC');

        $users = $users_data -> get();
        //本日登録した生徒数
        $student_num = $users_data -> count();
        //本日登録した生徒数のフラッシュメッセージ
        session()->flash('flash_message', '本日'.$student_num.'名の生徒を登録しました');

    //リターンview
    return view('school.studentlist',compact('users','school','grade_selection',
            'kumi_selection','user_name','grade_select','kumi_select','student_num',));

   
    }

    /**
     * ★エクセル一括登録
     * StudentImportクラスをNewする
     * Route::match(['get', 'post']　インポート失敗時、戻るボタン押下のケースが多いため
     * インポート中のバリデーション　vendor\maatwebsite\excel\src\Validators\Failure.php
     * @param Request $request Content型の$content（アップローしたファイル）
     * @return view('school.studentlist')
     */
    public function import(Content $content,Request $request){
        logger()->debug('<-- school/StudentlistController@import');
        global $pagenation;
   
        ini_set('memory_limit','1024M');
        set_time_limit(180);
        //多重送信対策　インポート後更新ボタン押下で、二重登録するのを防ぐ
        //インポート後、リダイレクトするため、必要ないが念のため
        $request->session()->regenerateToken();

        //学校情報
        $school = School::where('schools.id','=',auth()->user()->school_int_id)
            ->select('id','name','password_student_init')->first();

        //学年、組のセレクトボックス値（モデルで関数作成）
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        //検索ボックスの既定値
        $user_name = null;

        //編集中の学年、組のセレクトボックスの既定値
        $grade_select = null;
        $kumi_select = null;

        // アップロードされたエクセルファイル
        $file = request()->file('file');
        logger()->debug($file);
        $import = null;

        //バリデーションカスタムメッセージにするための準備 （バリデーションについてはteacherlistコントローラに記載））
        //dd($request['excel_file'] );
        $request['excel_file'] =  $file;
        $validator = Validator::make($request->all(), [
        //バリデーション：ファイルサイズ500kb、拡張子はxlsx
           'excel_file' => ['
                           required',
                           //maxだと効かない　単位はキロバイト　1⇒1kb　1024⇒1Mb
                           'between:"0","500"',
                            'mimes:xlsx',
                            //mimetypeとmimesのどちらかを入れる、検証中⇒最終的に入れるか検討すること！！！
                            //'mimetypes:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"',
                       ],
       ]);
       //バリデーション失敗時
       if ($validator->fails()) {
           //成功メッセージをださないため
           session()->flash('flash_message', null);
           return redirect('/school/studentlist')
               ->withErrors($validator);
       }

        //バリデーション成功時
        try {
            //エクセルファイルをバリデーションして保存するクラス「StudentImport」をnewして「$import」
            $import = new StudentImport();
            //「excel」ディレクトリにアップロードされたエクセルファイルを保存し、$importに格納
            $file->store('excels');
            Excel::import($import, $file);

            //元のエクセルファイルにデータが1件もない場合
            if( $import->id_list() === null){
                return redirect('/school/studentlist')
                ->withErrors('エクセルファイルにデータがありません。ファイルを確認してください');
            } else {
                //インポートした生徒数（フラッシュメッセージで使用）
                $inported_user_count = count($import->id_list());
                session()->flash('flash_message', $inported_user_count.'名の生徒を登録しました');
            }

        } catch (ValidationException $e) {
            logger()->debug('●学校管理メニューの生徒一覧、エクセル一括登録でエラー');
            logger()->debug($e->errors());
            session()->flash('flash_message', 'エクセル一括登録に失敗しました。ファイルのデータを確認してください');
            return redirect('/school/studentlist')
                //dd($e->errors()); //エラーの赤波線出るが、取得できているので問題なし！
                ->withErrors($e->errors());
        }
        //リダイレクト
        return redirect('/school/studentlist');
    }

        /**  ※インポート後、追加生徒を表示させたが、更新ボタン押下で多重送信される（これは対策しているが）、インポートに失敗したとき、戻るボタン押下などで、すぐに419エラーページにとんでします。
         * インポート失敗は多いと予想⇒その時、419エラーに多々飛ぶのはよくないため。
         * そのため、対策が見つかるまで、コメントアウト。
        $user_name = null;
        //インポートした人数だけ、保存後表示される
        //id_listに　インポートしたユーザーのIDを格納している
        $users = User::where(function ($query) use ($import) {
                foreach ($import->id_list() as $field){
                    $query->orWhere('login_account','=',$field);
                }
            })
            ->select('users.name as user_name','users.id as user_id','users.school_id as school_id', 'users.login_account as login_account')
            ->leftJoin('classes','users.class_id','=','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('users.name as user_name','users.id as user_id','users.school_int_id as school_int_id','users.school_id as school_id',
                'role','class_id','login_account','attendance_no','grade.id as grade_id','kumi.id as kumi_id',
                'grade.name as grade_name','kumi.name as kumi_name','users.password as password')
            ->orderByRaw('CHAR_LENGTH(grade.name) ASC,grade.name ASC,
                CHAR_LENGTH(kumi.name) ASC, kumi.name ASC, attendance_no ASC')
            ->paginate($pagenation);

        //リターンview
        return view('school.studentlist',compact('users','school','grade_selection',
            'kumi_selection','user_name','grade_select','kumi_select','student_num'));

        */


    /**
     * ★エクセル一括登録用のテンプレートダウンロード
     * 開発環境　\storage\app\exceltemplate\school_student_exceltemplate.xlsx'　　ルーティング 学校ID/achool_student_exceltemplate.xlsx'
     * use Illuminate\Support\Facades\Storage;
     */
    public function download()
    {
        logger()->debug('<-- school/StudentlistController@download');

        $filePath = 'private/exceltemplate/【ナレッジタイピング】生徒登録用テンプレート.xlsx';
        $fileName = '【ナレッジタイピング】生徒登録用テンプレート.xlsx';
        $mimeType = Storage::mimeType($filePath);
        $headers = [['Content-Type' => $mimeType]];

        return Storage::download($filePath, $fileName, $headers);
    }
}
