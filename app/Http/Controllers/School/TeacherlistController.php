<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Models\User;
use App\Models\Clas;
use App\Models\School;
use App\Exports\Export;
use PhpOffice\PhpSpreadsheet\Writer\Ods\Content;
use Maatwebsite\Excel\Facades\Excel;
use Exception;

    /**
     * 学校管理メニュー　先生情報　※ページネーションなし
     * (1)先生一覧表示
     * (2)検索＆エクセル出力
     * (3)新規＆更新
     * (4)先生の削除
     * (5)パスワードリセット
     * (6)エクセル一括登録　★二重登録確認（ページ数200以下）
     * (7)エクセル一括登録用のテンプレートダウンロード
     *
     */
class TeacherlistController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- TeacherlistController@construct');
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
    }

    /**
     * ★先生一覧
     * Route::get  ※ページネーションなし
     * @return view('school.teacherlist')
     */
    public function index()
    {
        logger()->debug('<-- TeacherlistController@index');
        //学校データ（ヘッダーで必要）
        $school = School::where('schools.id','=',auth()->user()->school_int_id)
            ->select('name','password_teacher_init')->first();

        //検索ボックスの初期値は空欄
        $user_name = null;

        

        //先生データ
        $users = User::where('users.school_id','=',auth()->user()->school_id)
            ->select('users.name as user_name','users.id as user_id','users.school_id as school_id', 'users.login_account as login_account')
            ->where('users.role','=','先生')
           
            ->orderBy('user_name','ASC')
            ->get();

        //全先生数（先生IDをカウント）
        $teacher_num =  User::where('school_id','=',auth()->user()->school_id)
            ->where('users.role','=','先生') ->select('id') ->count();

        //表示されている先生数(検索後の数、ページネーションがないので必要）
        $current_teacher_num = $users->count();

        //リターンview
        return view('school.teacherlist',compact('users','school','user_name','teacher_num','current_teacher_num'));
    }
    /**
     * ★検索＆エクセル出力　※ボタンで振り分け
     * Route::match(['get', 'post']　※【重要】ページネーションなしなので、多重送信対策なし
     *
     * @return 検索ボタン押下時　view('school.teacherlist')　
     * @return  エクセル出力ボタン押下時　Excel::download
     */
    public function search(Request $request)
    {
        logger()->debug('<-- TeacherlistController@search');

        //検索値を変数に代入
        $user_name = $request->input('user_name');

        //検索結果後の先生データ
        $users = User::where('users.school_int_id','=',auth()->user()->school_int_id)
            ->where('users.name','Like binary',"%$user_name%")
            ->where('users.role','=',"先生")
            ->select('users.name as user_name','users.id as user_id','users.school_id as school_id', 'users.login_account as login_account')
            ->get();

        //全先生数（先生IDをカウント）
        $teacher_num =  User::where('school_int_id','=',auth()->user()->school_int_id)
        ->where('users.role','=','先生') ->select('id') ->count();

        //表示されている先生数(検索後の数）
        $current_teacher_num = $users->count();

        //学校情報から、先生のパスワード初期値を取得
        $school = School::where('schools.id','=',auth()->user()->school_int_id)
            ->select('name','password_teacher_init')->first();

        //【ここから振り分け】検索ボタンか、エクセル出力ボタンかをsubmitボタンの名前で判断
        //検索ボタン押下時
        if($request->has('search')){
            return view('school.teacherlist',compact('users','school','user_name','teacher_num','current_teacher_num'));
        //エクセル出力ボタン押下時
        }elseif($request->has('excel')){
            if($users->isEmpty()) {
                return redirect('/school/teacherlist')
                ->withErrors('エクセルに出力するデータが1件もありません。検索をしてデータ表示させてから、「エクセル出力」ボタンを押してください。');
            }

            $view = view('school.teacherlistexcel',compact('users'));
            return Excel::download(new Export($view), '先生一覧'.date('Y年m月d日H時i分s秒出力').'.xlsx');
        } else {
            return view('school.teacherlist',compact('users','school','user_name','teacher_num','current_teacher_num'));
        }
    }
    /**
     * ★先生の新規＆更新　※ボタンで振り分け
     * Route::post　※ページネーションなし
     * @return view('school.teacherlist')
     */
    public function new(Request $request)
    {
        logger()->debug('<-- TeacherlistController@new');
        //多重送信対策　
        $request->session()->regenerateToken();

        //検索ボックスの既定値は空欄
        $user_name = null;

        //リクエストデータを配列形式に変換（バリデーションのため）
        $reqData = $request->all();

        //学校ごとの最大ユーザー数（生徒＋先生-学校管理者1名-先生1名）が超えていないかチェック（学校管理1名と先生1名は含まない）
        //学校情報
        $school = School::where('schools.id','=',auth()->user()->school_int_id)
            ->select('user_number','name','id','password_teacher_init')->first();
        //学校に登録できるユーザー最大数
        $max_user_num = $school['user_number'];
        //学校管理＆先生＆生徒を含むユーザー数、ナレッジ用隠し学校アカウント（b33259)がある時は、除外する
        $user_num =  User::where('school_int_id','=',auth()->user()->school_int_id)
            ->where('login_account','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN'))
            ->count();
            //dd( $user_num);

       // 最大登録数をオーバーしたらバリデーション（requied )チェックするための準備（学校管理1名、先生1名は含まない）       
       if($user_num < $max_user_num +2){
            $reqData['over_max_student_num'] ="OK"; //バリデーションクリア
        } else {
            $reqData['over_max_student_num'] = null; //バリデーションにひっかかる
        }

        //名前をセットしてバリデーション実行
        $reqData['teacherlist_user_name'] = $reqData['name'];
        $validator = Validator::make($reqData, [
            'teacherlist_user_name' => ['required','max:20'],
            'over_max_student_num' => ['required'],
        ]);

        //バリデーション失敗時　リダイレクト
        if ($validator->fails()) {
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            return redirect('/school/teacherlist')
                ->withErrors($validator);
        }
        //バリデーション成功時　ここから振り分け
        //新規　(新しい行にデータを入力して保存ボタン押下)　addnew_teacher.js
        if($request->has('update')){
            
            //先生は学年「なし」、組「なし」からクラスIDを取得
            $teacher_class = Clas::where('classes.school_id','=',auth()->user()->school_int_id)
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                ->select('grade.name as grade_name','kumi.name as kumi_name','classes.id as class_id')
                ->where('grade.name','=','なし') 
                ->where('kumi.name','=','なし') 
                -> first();
            //dd( $teacher_class->class_id);

            //ユーザーのインスタンス作成
            $User = new User();
            $User->name = $reqData['name']; // 名前
            $User->role = '先生'; // ロール
            $User->password = bcrypt($school->password_teacher_init); // パスワード
            $User->school_int_id = auth()->user()->school_int_id; // 学校ID(数値)
            $User->school_id = auth()->user()->school_id; // 学校ID
            $User->class_id = $teacher_class->class_id; // クラスID（学年なし、組なしのクラス）
            $User->attendance_no = 0; // 先生は一律「０」ゼロ

            //ログインIDを取得　※10101 ~ 99999 の内　先生は接頭辞t

            try {
                while(true){
                    $id_no = 't' . str_pad(mt_rand(10101, 99999), 5, 0, STR_PAD_LEFT);
                    //先生のid（t～も認識できるようにLike）
                    $n_no = User::where('login_account','Like',"%$id_no")
                        ->where('school_id',auth()->user()->school_id)
                        ->first();
                    if($n_no === null){
                        break;
                    }
                }
                $User->login_account = $id_no; // ログインID
            }catch(Exception $e){
                logger()->error('●学校管理メニュー、先生新規作成でIDを自動生成中エラー');
                logger()->error($e);
                abort(500);
            }

            //保存成功時のフラッシュメッセージの内容
            $flashmessage = '先生を登録しました';

        //更新（編集後、保存ボタン押下）
        } elseif($request->has('save')){
            //編集中のコースデータを取得
            $User =User::find($request -> input('id'));
            $User->name = $request->name; // 名前
            //保存成功時のフラッシュメッセージの内容
            $flashmessage =  '先生の情報を更新しました';
        } else {
            logger()->error('●学校管理メニュー、先生の新規or更新で保存ボタンを押下しない時のエラー');
            abort(500);
        }

        //ここから共通処理
        try{
            //新規データを保存
            $User->save();
            session()->flash('flash_message', $User->name.$flashmessage);
        }catch(Exception $e){
            logger()->error('●学校管理メニュー、先生の新規or更新でエラー');
            logger()->error($e);
            abort(500);
        }
        //リダイレクト ※リターンviewで更新データを一番上に表示させたいが、リロードすると二重登録をしてしまうためリダイレクト
        return redirect('/school/teacherlist');
    }
    /**
     * ★先生の削除　
     * Route::post
     * 【重要】course_with_bestpoint にある先生データも削除⇒DBでカスケードデリート(ただし、score_history は削除しない)
     * checkDelBtn.jsでチェックしているユーザーID取得
     * @return redirect('/school/teacherlist')
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- TeacherlistController@delete');

        //dd($request->checks);　//チェックされたuserのIDを配列で取得
        $checks = $request->checks;

        //削除チェックを入れてから、編集ボタンを押すと、削除チェックを外し処理をJSでしている（checkedit_teacher)が、赤い削除ボタンは活性化したまま。
        //つまり、削除チェックなしの状態で、削除ボタンがおせる。
        if($checks === null){
           //成功メッセージをださないため
           session()->flash('flash_message', null);
           return redirect('/school/teacherlist')
               ->withErrors('削除チェックがないため、削除に失敗しました。削除したいデータにチェックをしてから、削除を実行してください。'); 
        } else {
            //ここから削除実行
            try{
                DB::beginTransaction();
                User::whereIn('id', $checks)->delete();
                DB::commit();
            } catch (Exception $e) {
                logger()->error('●学校管理メニュー、先生の削除でエラー');
                logger()->error($e);
                DB::rollback();
                abort(500);
            }
            //削除成功時、flashメッセージ
            session()->flash('flash_message', '先生情報を削除しました');
        }
        //リダイレクト
        return redirect('/school/teacherlist');
    }
    /**
     * ★先生のパスワードリセット
     * Route::post　ページネーションなし
     * @return redirect('/school/teacherlist')
     */
    public function reset(Request $request)
    {
        logger()->debug('<-- TeacherlistController@reset');

        //選んだ先生データ
        $user = User::find($request->user_id);
        //先生のパスワード初期値
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('password_teacher_init')->first();
        //パスワード初期値をハッシュ化
        $user->password = bcrypt($school->password_teacher_init);
        //保存
        try{
            $user->save();
        } catch (Exception $e) {
            logger()->error('学校管理メニュー、先生のパスワードリセットでエラー');
            logger()->error($e);
            abort(500);
        }
        //リセット成功時、flashメッセージ
        session()->flash('flash_message', $user->name. '先生のパスワードをリセットしました');
        //リダイレクト
        return redirect('/school/teacherlist');
    }
    /**
     * ★エクセル一括登録　
     * 【確認すること】インポートする人数をページネーションのページ数以下にする。次ページにいくとPOSTリクエストを発して二重登録となるため
     * Route::match(['get', 'post']　インポート失敗時、戻るボタン押下のケースが多いため
     * TeacherImportクラスをNewする
     *
     * @param Request Content $content
     * @return redirect('/school/teacherlist')
     */
    //Content型の$content　⇒アップロードされたファイル
    public function importCsv(Content $content,Request $request)
    {
        logger()->debug('<-- TeacherlistController@importCsv');
        ini_set('memory_limit','1024M');
        set_time_limit(180);

        //多重送信対策　インポート後更新ボタン押下で、二重登録するのを防ぐ
        //インポート後、リダイレクトするため、必要ないが念のため　
        $request->session()->regenerateToken();

        //検索ボックスの既定値
        $teacher_name = null;

        // アップロードされたエクセルファイル　
        $file = request()->file('file');
         logger()->debug($file);
         $import = null;

        /**fileで使える関数（エクセルの場合、参考）
        $request->file('file')->getSize()　//ファイルサイズ取得関数
        $request->file('file')->getClientMimeType()　//MimeTypeタイプ取得
        　　⇒application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        $request->file('file')->guessClientExtension()　//拡張子
        　　⇒xlsx
        $request->file('file')->getClientOriginalName() //アップロードしたファイル名
        【重要】ファイル最大値にmaxを指定しても効かない
        ⇒大容量のサイズを指定する場合、Laravel以前にPHPの設定ファイル「php.ini」でアップロード最大サイズ、POST最大サイズを変更しなければ、エラーとなる。
        maxはPHPで指定した最大サイズより大きくなくてはいけない（なので今回はmaxつかわない）
        */

        //バリデーションカスタムメッセージにするための準備
        //dd($request['excel_file'] );
         $request['excel_file'] =  $file;
         $validator = Validator::make($request->all(), [
            //バリデーション：ファイルサイズ500kb、拡張子はxlsx
            //'excel_file' => ['required','between:"10000","20004"','mimes:xlsx'],
            'excel_file' => ['
                            required',
                            //maxだと効かない　単位はキロバイト　1⇒1kb　1024⇒1Mb
                            'between:"0","500"',
                            'mimes:xlsx',
                            //mimetypeとmimesのどちらかを入れる、検証中⇒最終的に入れるか検討すること！！！
                            //'mimetypes:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"',
                        ],
        ]);
        //バリデーション失敗時
        if ($validator->fails()) {
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            return redirect('/school/teacherlist')
                ->withErrors($validator);
        }

        //バリデーション成功時
        try {
            //エクセルファイルをバリデーションして保存するクラス「TeacherImport」をnewして「$import」
            $import = new TeacherImport();
            //「excel」ディレクトリにアップロードされたエクセルファイルを保存し、$importに格納
            $file->store('excels');
            Excel::import($import, $file);

            //元のエクセルファイルにデータが1件もない場合
            if( $import->id_list() === null){
                return redirect('/school/teacherlist')
                    ->withErrors('エクセルファイルにデータがありません。ファイルを確認してください');
            } else {
                //インポートした生徒数（フラッシュメッセージで使用）
                $inported_user_count = count($import->id_list());
                session()->flash('flash_message', $inported_user_count.'名の先生を登録しました');
            }

        } catch (ValidationException $e) {
            logger()->debug('●学校管理メニューの先生一覧、エクセル一括登録でエラー');
            session()->flash('flash_message', 'エクセル一括登録に失敗しました。ファイルのデータを確認してください');
            //リダイレクト
            return redirect('/school/teacherlist')
                ->withErrors($e->errors());
        }
        //リダイレクト
        return redirect('/school/teacherlist');
    }

        /**  ※インポート後、追加生徒を表示させたが、更新ボタン押下で多重送信される（これは対策しているが）、インポートに失敗したとき、戻るボタン押下などで、すぐに419エラーページにとんでします。
         * インポート失敗は多いと予想⇒その時、419エラーに多々飛ぶのはよくないため。
         * そのため、対策が見つかるまで、コメントアウト。
        //学校データクエリ（ヘッダー＆先生のパスワード初期値で使用）
        $school = School::where('school_id','=',auth()->user()->school_id)
            ->select('name','password_teacher_init')->first();
        //検索ボックス（名前）の初期値
        $user_name = null;
        //インポートした人数だけ、保存後表示される
        //【重要】ページネーションでページ遷移すると、POSTリクエストを送って二重登録となる。⇒200ページなら、登録件数に200のバリデーションをかけること！
        //id_listに　インポートしたユーザーのIDを格納している
        $user = User::where(function ($query) use ($import) {
            foreach ($import->id_list() as $field){
                $query->orWhere('login_account','=',$field);
            }
        })
        ->select('users.name as user_name','users.id as user_id',
            'users.school_id as school_id', 'users.login_account as login_account');

        //先生データ取得
        $users = $user->get();

        //全先生数（先生IDをカウント）
        $teacher_num =  User::where('school_id','=',auth()->user()->school_id)
                ->where('users.role','=','先生') ->select('id') ->count();

        //ページに表示されている先生数
        $current_teacher_num = $user->count();

        //リターンview
        session()->flash('flash_message', $current_teacher_num.'人の先生を登録しました。新たに登録した先生は下記の表で確認できます');
        return view('school.teacherlist',compact('users','school','user_name','teacher_num','current_teacher_num'));
    */

    /**
     * ★エクセル一括登録用のテンプレートダウンロード
     * 開発環境　\storage\app\exceltemplate\school_teacher_exceltemplate.xlsx'　　ルーティング 学校ID/achool_teacher_exceltemplate.xlsx'
     * use Illuminate\Support\Facades\Storage;
     */
    public function download()
    {
        logger()->debug('<-- TeacherlistController@download');

        $filePath = 'private/exceltemplate/【ナレッジタイピング】先生登録用テンプレート.xlsx';
        $fileName = '【ナレッジタイピング】先生登録用テンプレート.xlsx';
        $mimeType = Storage::mimeType($filePath);
        $headers = [['Content-Type' => $mimeType]];

        return Storage::download($filePath, $fileName, $headers);
    }
}
