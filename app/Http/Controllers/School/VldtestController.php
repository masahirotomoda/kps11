<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\School;
use App\Models\Course;
use Exception;

    /**
     * バレッドキッズ専用　ICT試験タイピング管理　※ページネーションなし
     * (1)生徒一覧表示
     * (2)試験を選ぶ
     *
     */
class VldtestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
    }

    /**
     * ★生徒一覧
     * Route::get  ※ページネーションなし
     * @return view('school.vldtest')
     */
    public function index()
    {
        //学校データ（ヘッダーで必要）
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->first();

        //生徒データ
        $users = User::where('users.school_int_id',auth()->user()->school_int_id)
            ->where('users.role','生徒')           
            ->orderBy('attendance_no','ASC')
            ->get();

        //試験コースのドロップダウンリスト
        $test_courses= Course::where('invalid', 0) 
            ->where('tab','vldtest')
            ->orderBy('display_order','ASC')
            ->get();

        $test_list = $test_courses->pluck('course_name','id')->toArray();
        
        //リターンview
        return view('school.vldtest',compact('users','school','test_list'));
    }
   
    /**
     * ★検定コースを選んで生徒画面での検定コース表示・非表示設定
     * Route::post　ページネーションなし
     * @return redirect('/school/vldtest')
     */
    public function update(Request $request)
    {
        //logger()->debug('<-- VldtestController@testselect');
//dd($request->all());
        //選んだ生徒データ
        $user = User::find($request->userid);
        //dd($user);
        //選んだ検定コースのID（コースID）
        $user->vldtest = $request->selected_test;
        //dd($user);
        //保存
        try{
            $user->save();
        } catch (Exception $e) {
            logger()->error('学校管理メニュー、バレッド専用タイピング検定、試験選択エラー');
            logger()->error($e);
            abort(500);
        }
        //リセット成功時、flashメッセージ
        session()->flash('flash_message', $user->name. 'さんのタイピング検定コース表示・非表示設定を更新しました');
        //リダイレクト
        return redirect('/school/vld-test');
        
    }
    
}
