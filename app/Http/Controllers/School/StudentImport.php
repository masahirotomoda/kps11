<?php

namespace App\Http\Controllers\School;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\Clas;
use Illuminate\Validation\ValidationException;
use Validator;
use \DB;

$id_list;

class StudentImport implements ToCollection, WithStartRow,WithValidation
{
    public static $startRow = 2;                // csvの1行目にヘッダがあるので2行目から取り込む
    public $data = array();
    public function collection(Collection $rows)
    {
        logger()->debug('<-- StudentImport@collection');
        global $id_list;
        $validator = Validator::make($rows->toArray(), [
        ]);
        $index = 2;
        $hasError = false;
        if($rows->count() >= 201){
            $hasError = true;
            $validator->errors()->add('error','一度に登録可能な人数（200名）を超えています。ファイルを分割して登録してください');
            throw new ValidationException($validator);
            return;
        }

        $school = School::where('school_id',auth()->user()->school_id)->first();
        $user_count = User::where('school_int_id',$school->id)
            ->where('users.login_account','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN'))
            ->count();
        logger()->debug('ユーザ数'.$user_count);

        //学校ごとの最大ユーザー数（生徒＋先生-学校管理者1名-先生1名）が超えていないかチェック（学校管理1名と先生1名は含まない）
        if($rows->count() + $user_count > $school->user_number + 2){
            $hasError = true;
            $validator->errors()->add('error','契約している「利用者数」を超えたため、新規登録はできません。学校管理者または、ナレッジタイピング事務局にお問い合わせください。');
            logger()->debug('登録可能な人数を超えています。');
            throw new ValidationException($validator);
            return;
        }

        foreach ($rows as $row){
            logger()->debug($row);
            $school = School::where('school_id',auth()->user()->school_id)->first();
            //学年
            if($row[0] !== null || $row[0] === ''){
                $grade = Grade::where('school_id',$school->id)->where('name',$row[0])->first();
                if($grade === null){
                    $hasError = true;
                    $validator->errors()->add('error',$index.'行目に該当する「学年('.$row[0].')」がありません');
                    $grade_id = null;
                }else{
                    $grade_id = $grade->id;
                }
            }else{
                $grade_id = null;
            }
            //組
            if($row[1] !== null || $row[1] === ''){
                $kumi = Kumi::where('school_id',$school->id)->where('name',$row[1])->first();
                //dd($row);
                if($kumi === null){
                    $hasError = true;
                    $validator->errors()->add('error',$index.'行目に該当する「組('.$row[1].')」がありません');
                    $kumi_id = null;
                }else{
                    $kumi_id = $kumi->id;
                }
            }else{
                $kumi_id = null;
            }

            //クラス
            $class = Clas::where('school_id',$school->id)
                ->where('grade_id',$grade_id)
                ->where('kumi_id',$kumi_id)
                ->first();
            if($class === null){
                $hasError = true;
                $validator->errors()->add('error',$index.'行目に該当する「クラス」がありません');
                $class_id = null;
            }else{
                $class_id = $class->id;
            }

            while(true){
                $id_no = str_pad(mt_rand(10101, 99999), 5, 0, STR_PAD_LEFT);
                $n_no = User::where('login_account','Like',"%$id_no")
                    ->where('school_int_id',$school->id)
                    ->count();
                logger()->debug($n_no);
                if($n_no === 0){
                    break;
                }
            }

            //パスワードとID
            $login_account = $id_no;
            $password = $school->password_student_init;
            logger()->debug('user creating.');
            User::create([
                'school_id' => $school->school_id,
                'school_int_id' => $school->id,
                'role' => '生徒',
                'name' => $row[3],
                'login_account' => $login_account,
                'password' => $password,
                'class_id' => $class_id,
                'attendance_no'=> $row[2]
            ]);
            $id_list[$index] = $login_account;
            logger()->debug('user created.');
            $index ++;
        }
        if($hasError === true){
            DB::rollback();
            throw new ValidationException($validator);
        }else{
            DB::commit();
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return self::$startRow;
    }

    public function id_list()
    {
        global $id_list;
        return $id_list;
    }
    public function rules(): array{
        return [
                '*.0' => "nullable|max:20",         // 学年
                '*.1' => "nullable|max:20",         // 組
                '*.2' => "required|numeric|between:0,9999",  // 出席番号
                '*.3' => "required|string|max:20",           // 名前
        ];
    }
   public function customValidationAttributes()
   {
       return [ '0' => '「学年」',
                '1' => '「組」',
                '2' => '「出席番号」',
                '3' => '「名前」',
        ];
   }
}
