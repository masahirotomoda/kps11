<?php

namespace App\Http\Controllers\School;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\BestScore;
use App\Exports\Export;
//赤線がでるが、PDFでつかうので必要
use Barryvdh\DomPDF\Facade\Pdf;
use Maatwebsite\Excel\Facades\Excel;

/**
* 学校管理メニュー　生徒別ベストスコア、エクセル出力、PDF印刷
*【重要】PDF印刷に時間がかかるためページネーションは100
*【重要】ベストスコアテーブルにはschool_id(int_id)カラムがあるが、それは使わずに、usersから学校IDを取得すること！
* ⇒トライアル学校から学校に昇格したときに、学校IDが引き継がれないため。
*(1)ベストスコア一覧
*(2)検索＆エクセル出力
*(3)PDF印刷
*/
$pagenation = 200;
class BestScoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        logger()->debug('<-- BestScoreController@construct');
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }

    /**
     * 学校管理メニュー　生徒別ベストスコア(先生の成績は見せない)
     * ※先生メニューのベストスコアとは若干違う（先生メニューのほうはトライアルとの振り分けがある）
     * ※日付書式はコントローラーでできなかったのでViewで実装
     *
     * @return  view('school.bestscore')
     */
    public function index()
    {
        logger()->debug('<-- (School)BestScoreController@index');
        global $pagenation;
        $school = School::where('id','=',auth()->user()->school_int_id)->first();
        //学年、組のドロップダウンリストの値
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);

        //検索ボックスの値は、最初空欄
        $user_name = null;
        $courseName = null;
        $grade = null;
        $kumi = null;
        $name = null;

        //ベストスコア情報（ページネーションは100） ⇒既定の200だとPDF出力に時間がかかるため
        $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
            ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
            ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
            ->leftJoin('classes','classes.id','=','users.class_id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
                'course_with_bestpoint.best_point_created_at as b_created_at','course_with_bestpoint.id as bestpoint_id','course_with_bestpoint.rank as bestpoint_rank','users.*',
                'course_with_bestpoint.best_point')
            ->groupBy('course_with_bestpoint.course_id')
            ->groupBy('users.id')
            ->where('users.role','=','生徒')
            ->where('users.school_id',$school->school_id)
            ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')
            ->paginate(100);

            //dd($bestScore);

        return view('school.bestscore',compact('school','courseName',
            'grade','kumi','name','grade_selection','kumi_selection','bestScore',));
    }
    /**
     * 学校管理メニュー　生徒別ベストスコア検索「学年」「組」「生徒の一部」「コース名の一部」
     * @param Request $request
     * @return
     */
    public function search(Request $request)
    {
        logger()->debug('<-- (School)BestScoreController@search');

        global $pagenation;
        $school = School::where('id','=',auth()->user()->school_int_id)->select('name','id','trial','school_id')->first();

        //postデータを変数に代入
        $startDateTime = $request->input('start-date-time');
        $endDateTime = $request->input('end-date-time');
        $courseName = $request->input('course_name');
        $user_name = $request->input('name');
        $grade = $request->input('grade');
        $kumi = $request->input('kumi');

        //学年、組のドロップダウンリストの値
        $grade_selection = Grade::selectListById($school->id);
        $kumi_selection = Kumi::selectListById($school->id);
        $name = $request->input('name');


        //$request にexcelが含まれていたら、エクセル出力
        if($request->has('excel')){
            logger()->debug('excel');

            $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
                ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
                ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
                ->leftJoin('classes','classes.id','=','users.class_id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
                    'course_with_bestpoint.best_point_created_at as b_created_at' ,'course_with_bestpoint.id as bestpoint_id','users.*','course_with_bestpoint.rank as bestpoint_rank',
                    'course_with_bestpoint.best_point')
                ->groupBy('course_with_bestpoint.course_id')
                ->groupBy('users.id')
                ->where('users.role','=','生徒')
                ->where('users.school_id',$school->school_id)
                ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')
                ->when($grade != '' || $grade != null ,function($q) use($grade){
                    return $q->where('classes.grade_id','=',$grade);
                })
                ->when($kumi != '' || $kumi != null ,function($q) use($kumi){
                    return $q->where('classes.kumi_id','=',$kumi);
                })
                ->when($user_name !== null ,function($q) use($user_name){
                    return $q->where('users.name','Like binary',"%$user_name%");
                })
                ->when($courseName !== null ,function($q) use($courseName){
                    return $q->where('courses.course_name','Like binary',"%$courseName%");
                })
                ->get();
                //dd($bestScore);

                if ($bestScore->isEmpty()) {
                   return redirect('/school/bestscore')
                   ->withErrors('エクセルに出力するデータがありません。検索して、データを表示させてから「データの一括ダウンロード(Excel)」ボタンを押してください');
                }
            $view = view('excel.bestscoreexcel',compact('bestScore'));
            return Excel::download(new Export($view), '生徒のタイピングベストスコア'.date('Y年m月d日H時i分s秒出力').'.xlsx');

        //$request にsearchが含まれていたら、ベストスコア一覧ページを表示
        }elseif($request->has('search') || $request->has('page')){
            logger()->debug('view');
            //ページネーションは100 ⇒既定の200だと、PDF出力に時間がかかるため
            $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
                ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
                ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
                ->leftJoin('classes','classes.id','=','users.class_id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
                    'course_with_bestpoint.best_point_created_at as b_created_at' ,'course_with_bestpoint.id as bestpoint_id','users.*','course_with_bestpoint.rank as bestpoint_rank',
                    'course_with_bestpoint.best_point')
                ->groupBy('course_with_bestpoint.course_id')
                ->groupBy('users.id')
                ->where('users.role','=','生徒')
                ->where('users.school_id',$school->school_id)
                ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')
                ->when($grade != '' || $grade != null ,function($q) use($grade){
                    return $q->where('classes.grade_id','=',$grade);
                })
                ->when($kumi != '' || $kumi != null ,function($q) use($kumi){
                    return $q->where('classes.kumi_id','=',$kumi);
                })
                ->when($user_name !== null ,function($q) use($user_name){
                    return $q->where('users.name','Like binary',"%$user_name%");
                })
                ->when($courseName !== null ,function($q) use($courseName){
                    return $q->where('courses.course_name','Like binary',"%$courseName%");
                })
                ->paginate(100);

            return view('school.bestscore',compact('school','courseName',
                'grade','kumi','name','grade_selection','kumi_selection','bestScore',));
        }
    }
    /**
     * 学校管理メニュー　生徒別ベストスコア一覧　記録証PDF出力
     * @param Request $request
     * @return   $pdf->stream()
     */
    public function pdf(Request $request){
        logger()->debug('<-- BestscorePrintController@pdf');

        //印刷チェックがあれば（$request['checks']は、チェックしているscorehistoryのIDが配列で格納）
        if(isset($request['checks'])){
                $select = null;//??
                $school = School::where('id','=',auth()->user()->school_int_id)->select('name','id','trial','school_id')->first();

                $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
                    ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
                    ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
                    ->leftJoin('classes','classes.id','=','users.class_id')
                    ->leftJoin('grade','classes.grade_id','=','grade.id')
                    ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                    ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name','courses.course_category as course_category ','courses.course_category',
                        'course_with_bestpoint.best_point_created_at as b_created_at','course_with_bestpoint.rank','course_with_bestpoint.id as bestpoint_id','users.*','course_with_bestpoint.rank as bestpoint_rank',
                        'course_with_bestpoint.best_point')
                    ->groupBy('course_with_bestpoint.course_id')
                    ->groupBy('users.id')
                    ->where('users.role','=','生徒')
                    ->where('users.school_id',$school->school_id)
                    ->whereIn('course_with_bestpoint.id',$request->checks)
                    ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')
                    ->get();
                //dd($bestScore);
                //pdf1（シンプル記録証）ボタン押下
                if($request->has('pdf1')){
                    $pdf = PDF::loadView('pdf.transcript_bestscore_simple',compact('bestScore','school'));

                }else{
                //pdf1（カラフル記録証）ボタン押下
                    $pdf = PDF::loadView('pdf.transcript_bestscore_colorful',compact('bestScore','school'));
                }
                return $pdf->stream();
        } else{
            //印刷チェックが1つもない場合、エラーメッセージでリダイレクト
            return redirect('/school/bestscore')
                ->withErrors('【印刷できません】印刷したいデータにチェック（表の一番左の項目）してからボタンを押してください');
        }
    }
}
