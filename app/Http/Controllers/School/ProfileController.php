<?php

namespace App\Http\Controllers\school;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\User;
use App\Models\Course;
use App\Models\CourseWord;
use Exception;

/**
 * 学校管理メニュー ＞　学校アカウント情報
 * (1)学校アカウント情報
 * (2)パスワード変更
 */
class ProfileController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- school/ProfileController@construct');
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
    }
    /**
     * ★学校アカウント情報
     * 学校アカウントは、1校につき1つのみ。なので、ログイン中の学校アカウント情報が表示される。
     * （学校アカウントを2つ発行した場合、それぞれログイン中のアカウント情報のみわかる）
     * Route::get
     * @return view('school.profile')
     */
    public function index()
    {
        logger()->debug('<-- school/ProfileController@index');
        $user = User::find(auth()->user()->id);
        $school = School::find(auth()->user()->school_int_id);
        $school_id =$school->school_id; //例：ABC12345

        //現在のユーザー数（先生1名と学校管理者をのぞく）
        //ナレッジ用隠し学校アカウント（b33259)がある時は、除外する
        $user_num =  User::where('users.school_int_id','=',auth()->user()->school_int_id)
        ->where('login_account','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN'))
        ->select('id') ->count() - 2;

        //現在のコース数
        $course_num = Course::where('courses.school_id','=',$school ->id)
                ->select('id') ->count();

        //現在の単語数
        $word_num = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
            ->leftJoin('words','course_word.word_id','words.id')
            ->where('courses.school_id',$school->id)
            ->count();
        //ドメイン名（ナレッジのタイピングのドメイン、configrationsから取得）
        $domain_name = config('configrations.DOMAIN_NAME');

        //リターンview
        return view('school.profile',compact('user','school','user_num','school_id','course_num','word_num','domain_name'));
    }

    /**
     * ★パスワード変更＆生徒パスワード表示非表示
     * @param equest $request
     * @return view('school.profile')
     */
    public function changePassword(Request $request)
    {
        logger()->debug('<-- school/ProfileController@changePassword');
        //dd($request->all());
        //ログイン中の学校管理者情報
        $user = User::where('id','=',auth()->id())->first();

        //ここから分岐　パスワード変更ボタン押下
        if($request->has('user_password_reset_btn')){
                //パスワードのバリデーション アルファベットと数字と記号!#$%&を3つ以上含む10～20文字
                //先生パスワードリセットと同じコンポーネントをつかっているため、バリデーションメッセージを変更するため
                $reqData['change_password_school'] =$request->change_password;
                $reqData['change_password_school_confirmation'] =$request->change_password_confirmation;
                //dd($request->all());

                //バリデーション実行
                $validator = Validator::make($reqData, [
                    'change_password_school' => [
                        'confirmed', //確認パスワードとの相違をチェック
                        'required',
                        'regex:<^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[!@;:#$%&])|(?=.*[A-Z])(?=.*[0-9])(?=.*[!@;:#$%&])|(?=.*[a-z])(?=.*[0-9])(?=.*[!@;:#$%&]))([a-zA-Z0-9!@;:])>',
                        'between:10,20',
                    ]
                ]);
                //バリデーション失敗時
                if ($validator->fails()) {
                    return redirect('/school/profile')
                        ->withErrors($validator);
                }
                //バリデーション成功時
                try{
                    //パスワードの更新後、flashメッセージ
                    $user['password'] = bcrypt($request['change_password']);
                    $user->save();
                }catch(Exception $e) {
                    logger()->error('学校管理者　パスワード更新エラー.');
                    logger()->error($e);
                    abort(500);
                }
                //保存成功時のメッセージ
                $message='パスワードを変更しました';
        //ここから、生徒パスワード表示更新ボタン押下
        } else {
                $school=School::find(auth()->user()->school_int_id);
                //dd($school);
                if ($request->has('password_display')){
                    $school->password_display = 1;
                    $message='生徒のパスワードは「表示」に設定しました。';
                } else {
                    $school->password_display = 0;
                    $message='生徒のパスワードは「非表示」に設定しました。';
                }
                try{
                    $school->save();
                }catch(Exception $e) {
                    logger()->error('●学校管理メニュー、プロフィールの生徒のパスワード表示するかの更新でエラー.');
                    logger()->error($e);
                    abort(500);
                }
        }
        //ここから共通
        //保存成功時のメッセージ
        session()->flash('flash_message', $message);
        //リダイレクト
        return redirect('/school/profile');
    }
}
