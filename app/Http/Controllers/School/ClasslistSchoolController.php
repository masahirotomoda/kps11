<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
//↓↓先生メニューの「クラス管理」のメソッドを使う
use App\Http\Controllers\User\ClasslistController;

/**
 *  学校管理メニュー ＞　クラス一覧　
 * 先生メニューのクラス管理と同じなので、Controllerのメソッド、bladeファイル共有
 * ※同じcontrllerを使わない理由：権限が異なるため、コンストラクタで権限設定しているため
 * ※クラス名が同じだとエラーなのでClasslistSchoolController
 * (1)クラス一覧 　
 * (2)クラス別所属生徒一覧
*/
class ClasslistSchoolController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- school/ClasslistSchoolController@construct');
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
    }
    /**
     * //★クラス一覧
     * @return view('user.teacher.byclass')　⇒先生メニューと共有
     */
    public function index()
    {
        logger()->debug('<-- school/ClasslistSchoolController@index');      
        $class = new classlistController();
        $result = $class -> index();
        return $result;
    }
    /**
     * ★クラス別所属生徒
     * @return view('user.teacher.byclassbelongs')　⇒先生メニューと共有
     */
    public function belongs(Request $request)
    {
        logger()->debug('<--  school/ClasslistController@construct');
        $class = new classlistController();
        $result = $class -> belongs($request);
        return $result;
    }
}
