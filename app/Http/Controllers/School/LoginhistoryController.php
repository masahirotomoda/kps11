<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\LoginLog;
use App\Exports\Export;
use Excel;
use DateTime;

    /**
     * 学校管理メニュー　管理画面ログイン履歴　※学校管理画面＆先生メニューへのログイン（生徒はなし）
     * ※ユーザーが削除されたら、自動で履歴が削除されるのを防ぐため、ユーザー名と学校名は、リレーションでなく、カラムに落とす
     * (1)スコア履歴一覧
     * (2)検索＆エクセル出力
     */
$pagenation = 200;
class LoginhistoryController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- school/LoginhistoryController@construct');
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★ログイン履歴一覧　※学校管理＆先生メニューへのログイン　※生徒のログイン履歴は記録しない
     * Route::get
     * ログインアカウント（学校管理者と先生のアカウント）は学校ごとに作る(例：先生アカウントt55667はA学校やB学校にもいるケースがある）⇒DBには同じログインアカウントが存在する
     * @return view('school.loginhistory')
     */
    public function index()
    {
        logger()->debug('<-- school/LoginhistoryController@index');
        global $pagenation;

        //検索ボックスの初期値
        $user_name = null;
        $ipaddress = null;
        $startDateTime = null;
        $endDateTime = null;
        $name = null;
        $chkSchool = true;
        $chkTeacher = true;

        //学校データ（ヘッダーで使用）
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('name','id')->first();
        
        //ログインデータ(login_logsテーブルだけで完結する) ※ナレッジの隠し学校アカウントは除外
        $login_logs = LoginLog::where('login_school_int_id','=',auth()->user()->school_int_id)
            ->where('login_logs.login_from','<>','システム管理画面')
            ->where('login_logs.login_id','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN'))
            ->orderBy('login_logs.created_at','DESC')
            ->paginate($pagenation);


        /**  ユーザー（先生）を削除後、そのログインアカウントを別の人が使うケースもあるので、ユーザーテーブルからは取得しない
        //ログイン履歴データ　⇒1つのテーブルで作るようになったので、使わない
        $login_logs = LoginLog::leftJoin('users', function ($join) {
            //学校単位でログインアカウント作成、DBでは重複があるため学校IDとログインアカウントと学校IDの2つの抽出条件が必要な時のSQL
                $join->on('login_logs.login_id','=','users.login_account');
                $join->on('login_logs.school_id','=','users.school_id');
                })
            ->select('users.name as user_name','login_logs.created_at as created_at','login_logs.*')
            ->where('users.school_int_id','=',auth()->user()->school_int_id)
            ->where('login_logs.login_from','<>','システム管理画面')
            ->orderBy('login_logs.created_at','DESC')
            ->paginate($pagenation);
         */

            
//dd( $login_logs);
        return view('school.loginhistory',compact('school','login_logs','user_name','ipaddress','user_name'
            ,'startDateTime','endDateTime','chkSchool','chkTeacher',));
    }
    /**
     * ★検索
     * 「開始日」「終了日」「名前の一部」「IPアドレスの一部」「先生」「学校管理者」
     * Route::match(['get', 'post']
     * @return view('school.loginhistory')　⇒検索
     * @return view('school.loginhistoryexcel')　⇒エクセル
     */
    public function search(Request $request)
    {
        logger()->debug('<-- school/LoginhistoryController@search');
        global $pagenation;

        //学校データ
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('name','id','school_id')->first();

        //検索入力値を変数にセット
        $ipaddress = $request->input('ipaddress');
        $chkSchool = $request->input('schooladmin');
        $chkTeacher = $request->input('schoolteacher');
        $startDateTime = $request->input('start_date');
        $endDateTime = $request->input('end_date');

        $endDateTimeTmp = new DateTime($request->input('end_date'));
        $user_name = $request->input('user_name');

        //ログイン履歴データクエリ　※ナレッジの隠し学校アカウントは除外
        $endDateTimeTmp->modify('+1 day');
        $login_logs_data = LoginLog::where('login_school_int_id','=',auth()->user()->school_int_id)
        //ナレッジの隠し学校アカウント除外
        ->where('login_logs.login_id','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN'))
        ->when($startDateTime != '' ,function($q) use($startDateTime){
            return $q->where('login_logs.created_at','>',$startDateTime);
        })
        ->when($endDateTime != '' ,function($q) use($endDateTimeTmp){
            return $q->where('login_logs.created_at','<',$endDateTimeTmp);
        })
        ->when($chkTeacher !== null && $chkSchool === null,function($q) use($chkSchool){
            return $q->where('login_logs.role','先生');
        })
        ->when($chkSchool !== null && $chkTeacher === null,function($q) use($chkTeacher){
            return $q->where('login_logs.role','学校管理者');
        })
        //学校管理者と先生の両方にチェックがない時は検索結果はデータなし
        ->when($chkSchool === null && $chkTeacher === null,function($q) use($chkTeacher){
            return $q->where('login_logs.role','なし');
        })
        ->when($ipaddress != '' || $ipaddress != null ,function($q) use($ipaddress){
            return $q->where('login_logs.ipaddress','Like',"%$ipaddress%");
        })
        ->when($user_name != '' || $user_name != null ,function($q) use($user_name){
            return $q->where('login_logs.login_username','Like binary',"%$user_name%");
        })
        ->orderBy('login_logs.created_at','DESC');
      
        //ここから振り分け　（検索ボタン押下かエクセルボタン押下）
        //検索ボタン押下時（ページネーションでデータ取得）
        if($request->has('search') || $request->has('page')){
            $login_logs = $login_logs_data ->paginate($pagenation);
           
            return view('school.loginhistory',compact('school','login_logs','user_name','ipaddress','user_name'
                ,'startDateTime','endDateTime','chkSchool','chkTeacher',));
        //エクセルボタン押下時（get()で取得）
        }elseif($request->has('excel')){
            $login_logs = $login_logs_data ->get();
            $view = view('school.loginhistoryexcel',compact('school','login_logs','user_name','ipaddress','user_name'
                ,'startDateTime','endDateTime','chkSchool','chkTeacher',));
            return Excel::download(new Export($view), '管理画面ログイン履歴'.date('Y年m月d日H時i分s秒出力').'.xlsx');
        } else {
            logger()->error('●学校管理メニュー、ログイン履歴でボタン押下せずエラー');
            abort(500);
        }
    }    
}