<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\BattleScoreHistory;
use Carbon\Carbon;

/**
 * 学校管理メニュー　＞　ランキングスタジアムスコア履歴
 * Route::get
 * (1)スコア履歴一覧
*/
class BattleScoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('schooladmin.role');
    }
   /**
     * ★ランキングスタジアムスコア履歴一覧(先生の成績は表示しない)
     * 表示順⇒スコアの発生した降順
     * Route::get
     * @return  view('school.transcript')
     */
    public function index()
    {
        //学校情報
        $school = School::where('id',auth()->user()->school_int_id)->first();

        //名前検索の初期値
        $user_name=null;

        //今月と先月の２コースのみ、セレクトボタンから選べる
        //今月のコース1件取得して、そこからコースIDとコース名を取得 
        $course1 = BattleScoreHistory::whereBetween('created_at', [
            Carbon::now()->startOfMonth(),  // 今月の開始日
            Carbon::now()->endOfMonth()     // 今月の終了日
        ])
            ->select('course_id', 'course_name')
            ->first();
        
        //先月のコース1件取得して、そこからコースIDとコース名を取得 
        $course2 = BattleScoreHistory::whereBetween('created_at', [
            Carbon::now()->subMonth()->startOfMonth(),  // 先月の開始日
            Carbon::now()->subMonth()->endOfMonth()     // 先月の終了日
        ])
            ->select('course_id', 'course_name')
            ->first();
    
        // 今月と先月のコースを結合(mergeはつかえないため)
        $course_selection = [];
        if ($course1) {
            $course_selection[$course1->course_id] = $course1->course_name;
        }
        if ($course2) {
            $course_selection[$course2->course_id] = $course2->course_name;
        }
        //検索ボックスの初期値は今月のコース
        $select_course = $course1->course_id;

        //バトルスコア履歴
        $BattleScoreHistory = BattleScoreHistory::leftjoin('users','battlescore_history.user_id','users.id')
            //学校ごとの生徒に絞り込むのはBladeファイルでやる。【理由】全データのランキングが必要だから  
            //->where('users.school_int_id',Auth()->user()->school_int_id)
            ->where('battlescore_history.course_id',$course1->course_id)//今月
            ->select('users.name','users.school_int_id','battlescore_history.*')
            ->orderBy('battlescore_history.battle_score','DESC')
            ->get();
        //bladeで学校の絞り込みでつかう変数
        $school_int_id=Auth()->user()->school_int_id;
        //ページネーションなしのため、bladeで総数表示
        $battlescore_count=$BattleScoreHistory->count();
        
        //リターンview　
        return view('school.battlescorehistory',compact('school','select_course','course1','user_name',
            'course_selection','BattleScoreHistory','school_int_id','battlescore_count'));        
    }
   /**
     * ★検索　名前検索とコース選択（今月、先月の2コースのみ、既定は今月）
     * Route::post
     * @return view('school.battlescore-history')
     */
    public function search(Request $request)
    {
        //dd($request->all());
        //学校データ(ヘッダーで使用)
        $school = School::where('schools.id',auth()->user()->school_int_id)->first();

        //セレクトボックスの値（コースID）
        $select_course = $request->input('select_course');
        //名前検索の値 検索ボックスの値を引き継ぐ
        $user_name=$request->input('user_name');

        //バトルスコア履歴
        $BattleScoreHistory = BattleScoreHistory::leftjoin('users','battlescore_history.user_id','users.id')
            ->where('battlescore_history.course_id',$select_course)
            ->select('users.name','users.school_int_id','battlescore_history.*')
            ->orderBy('battlescore_history.battle_score','DESC')
            ->get();

        //今月と先月の２コースのみ、セレクトボタンから選べる
        //今月のコース1件取得して、そこからコースIDとコース名を取得 
        $course1 = BattleScoreHistory::whereBetween('created_at', [
            Carbon::now()->startOfMonth(),  // 今月の開始日
            Carbon::now()->endOfMonth()     // 今月の終了日
        ])
            ->select('course_id', 'course_name')
            ->first();
        
        //先月のコース1件取得して、そこからコースIDとコース名を取得 
        $course2 = BattleScoreHistory::whereBetween('created_at', [
            Carbon::now()->subMonth()->startOfMonth(),  // 先月の開始日
            Carbon::now()->subMonth()->endOfMonth()     // 先月の終了日
        ])
            ->select('course_id', 'course_name')
            ->first();
    
        // 今月と先月のコースを結合
        $course_selection = [];
        if ($course1) {
            $course_selection[$course1->course_id] = $course1->course_name;
        }
        if ($course2) {
            $course_selection[$course2->course_id] = $course2->course_name;
        }

        $school_int_id=Auth()->user()->school_int_id;
        $battlescore_count=$BattleScoreHistory->count();

        //リターンview
        return view('school.battlescorehistory',compact('school','select_course','course1','user_name',
        'course_selection','BattleScoreHistory','school_int_id','battlescore_count'));     
    }
}