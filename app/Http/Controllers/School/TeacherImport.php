<?php

namespace App\Http\Controllers\School;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\Clas;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

//インポートしたユーザーのIDを格納するグローバル変数
$id_list;
class TeacherImport implements ToCollection, WithStartRow,WithValidation
{
    public static $startRow = 2;                // csvの1行目にヘッダがあるので2行目から取り込む
    public function collection(Collection $rows)
    {
        logger()->debug('<-- TeacherImport@collection');
        global $id_list;
        $school = School::where('school_id',auth()->user()->school_id)->first();
        $hasError = false;
        $validator = Validator::make($rows->toArray(), [
        ]);
        $index = 2;
        DB::beginTransaction();
        logger()->debug($rows);
        if($rows->count() >= 201){
            $hasError = true;
            $validator->errors()->add('error','一度に登録可能な人数（200名）を超えています。ファイルを分割して登録してください');
            throw new ValidationException($validator);
            return;
        }
        $user_count = User::where('school_int_id',$school->id)
            ->where('users.login_account','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN'))
            ->count();
        //学校ごとの最大ユーザー数（生徒＋先生-学校管理者1名-先生1名）が超えていないかチェック（学校管理1名と先生1名は含まない）
        if($rows->count() + $user_count > $school->user_number + 2 ){
            $hasError = true;
            $validator->errors()->add('error','契約している「利用者数」を超えたため、新規登録はできません。学校管理者または、ナレッジタイピング事務局にお問い合わせください。');
            throw new ValidationException($validator);
            return;
        }

        $teacher_class = Clas::where('classes.school_id','=',auth()->user()->school_int_id)
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('grade.name as grade_name','kumi.name as kumi_name','classes.id as class_id')
            ->where('grade.name','=','なし')
            ->where('kumi.name','=','なし')
            -> first();
        if($teacher_class === null){
            $hasError = true;
            $validator->errors()->add('error','先生用のクラスがありません。先に「なし」年「なし」組を作成してください');
            throw new ValidationException($validator);
            return;
        }


        foreach ($rows as $row){
            logger()->debug($row);
            while(true){
                $id_no = str_pad(mt_rand(10101, 99999), 5, 0, STR_PAD_LEFT);
                $n_no = User::where('login_account','Like',"%$id_no")
                    ->where('school_id',$school->id)
                    ->first();
                if($n_no === null){
                    break;
                }
            }
            //先生アカウントは「ｔ」が接頭辞
            $login_account = 't'.$id_no;
            $password = bcrypt($school->password_teacher_init);
            $data[$index] = User::create([
                'school_id' => $school->school_id,
                'school_int_id' => $school->id,
                'class_id' => $teacher_class->class_id,
                'role' => config('configrations.ROLE_LIST')[3],
                'name' => $row[0],
                'login_account' => $login_account,
                'password' => $password,
                'attendance_no' => 0,
            ]);
            $data += array( $index => $login_account );
            $id_list[$index] = $login_account;
            logger()>debug('user created.');
            $index ++;
        }
        if($hasError === true){
            DB::rollback();
            throw new Exception;
        }else{
            DB::commit();
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return self::$startRow;
    }

    public function id_list()
    {
        global $id_list;
        return $id_list;
    }

    public function rules(): array{
        return [
                '*.0' => "required|string|max:20",         // 名前
            ];
    }

   public function customValidationAttributes()
   {
       return [ '0' => '「名前」',
    ];
   }


}
