<?php

namespace App\Http\Controllers\Free;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\CourseWord;

class TypingController extends Controller
{
    /**
     * ★Free版　全コース共通タイピング画面 
     *  postとgetがOK⇒基本はPOST　だがSEO的に
     * (1)n-typing.com/free/course　を直接入力すると（get）★の中段
     * (2)n-typing.com/free/course-test　を直接入力すると（get）テストの中1問目
     * (3)n-typing.com/free/course-keytouch　を直接入力すると（get）キータッチの1問目
     * (4)n-typing.com/free/course-map　を直接入力すると（get）地図の1問目
     * (5)n-typing.com/free/course-isan　を直接入力すると（get）地図タブの遺産の1問目
     * (5)n-typing.com/free/course-english　を直接入力すると（get）英語★の1問目
     * jsonに渡すデータは最小限にすること（jsなのでソースが閲覧できるため）
     */
    public function __construct(){}

    /**
     * 【無料版】タイピング画面を出力する　※全コース共通（練習モード、テストモード、地図モード）
     * メニュー一覧からPOSTでコースID取得　※route:post
     */
    public function index(Request $request)
    {
        //getの不正入力をはじく
        $is_isan=false;
        if ($request->isMethod('get')){
            if($request->is('free/course')){
                $courseId=4;
            } elseif ($request->is('free/course-test')){
                $courseId=93;
            } elseif ($request->is('free/course-keytouch')){
                $courseId=121;
            } elseif ($request->is('free/course-map')){
                $courseId=422;
            } elseif ($request->is('free/course-english')){
                $courseId=700;
            } elseif ($request->is('free/course-isan')){
                $courseId=432;
                $is_isan=true;
            } else {
                abort('404');
            }
        } else {
            //courseIDを取得したいがforeach内でhiddenが繰り返し使えないので、nameにIDを入れる。4番目（token、sound,alpha,ID）なので、keyで取得
            $req=$request->all();
            foreach($req as $key => $value){
                $courseId =  $key;
            }
        }
        
        //選んだコース
        $course=Course::where('courses.id',$courseId)->first();
        //メニューから選んだコースのタブの全コースデータを取得（例）「中段」を選んだら、★コースすべてのコース情報を取得
        //タイピングコースにある「次へ」「前へ」「途中で止める」はすべてJSで処理（スピードUPのため）

        //世界遺産コース（東武ワールドスクエア）、無料プランしか表示できない。しかし、東武に確認してもらうため、ナレッジ経堂校は
        //表示させる必要がある。暫定措置で、地図コースのみ、分岐させる。
        if($course->tab=='map'){
            $tabCourseList = Course::where('is_free_course', 1)
                ->where('invalid',0)
                ->where('tab',$course->tab)
                //->whereNull('school_id')
                ->select('id','course_name','random','course_category','test_time','tab')
                ->orderBy('display_order','ASC')
                ->get();

        } else {
            $tabCourseList = Course::where('is_free_course', 1)
                ->where('invalid',0)
                ->where('tab',$course->tab)
                ->whereNull('school_id')
                ->select('id','course_name','random','course_category','test_time','tab')
                ->orderBy('display_order','ASC')
                ->get();
        }
        //dd($tabCourseList);
        $coursesArray = $tabCourseList->toArray();   //配列形式にする、加工がしやすいため
        //世界遺産コース（東武ワールドスクエア）、無料プランしか表示できない。しかし、東武に確認してもらうため、ナレッジ経堂校は
        //表示させる必要がある。暫定措置で、地図コースのみ、分岐させる。
        if($course->tab=='map'){
            $wordData = CourseWord::leftJoin('courses','courses.id','course_word.course_id')
                ->leftJoin('words','words.id','course_word.word_id')
                ->where('courses.is_free_course',1)
                ->where('courses.invalid',0)
                ->where('courses.tab',$course->tab)
                //->whereNull('school_id')
                ->select('words.word_mana','words.word_kana','words.id','words.pronunciation_file_name','words.display_order','course_id')
                ->orderBy('display_order','ASC')
                ->get();
        } else {
            $wordData = CourseWord::leftJoin('courses','courses.id','course_word.course_id')
            ->leftJoin('words','words.id','course_word.word_id')
            ->where('courses.is_free_course',1)
            ->where('courses.invalid',0)
            ->where('courses.tab',$course->tab)
            ->whereNull('school_id')
            ->select('words.word_mana','words.word_kana','words.id','words.pronunciation_file_name','words.display_order','course_id')
            ->orderBy('display_order','ASC')
            ->get();
        }
        
        //音あり、アルファベットありはcookieに保存
        $soundData = $request->hasCookie('ntyping_option_sound') ? $request->cookie('ntyping_option_sound') : 'yes';
        $alphabetData = $request->hasCookie('ntyping_option_alphabet') ? $request->cookie('ntyping_option_alphabet') : 'yes';
            
        //viewの振り分け
        if ($wordData->isEmpty()) {
            abort(404);
        } elseif($course->tab=='map') {
            $ViewFile='user.free.typingmap';
        } elseif($course->tab=='english1' || $course->tab=='english2' || $course->tab=='english3') {
            $ViewFile='user.free.typingenglish';
        } elseif($course->tab=='test' || $course->tab=='key') {
            $ViewFile='user.free.typingtest';
        } else{
            $ViewFile='user.free.typing';
        }        
        return view($ViewFile, compact('coursesArray','course', 'wordData', 'soundData', 'alphabetData','tabCourseList','is_isan'));
    }
    public function redirect($CourseID)
    {
        return redirect('/');
    }
}
