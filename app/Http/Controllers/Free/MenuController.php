<?php

namespace App\Http\Controllers\Free;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Blog;
use App\Models\Information;

class MenuController extends Controller
{
    /**
     * 【Free】タイピングコース一覧 
     */
    public function __construct(){}

    /**
     * 【Free】メインメニュー　n-typing.com
     */
    public function index(Request $request)
    {
        //どのタブを表示するか
        $map_tab_active = null;
        $star1_tab_active = null;
        $star2_tab_active = null;
        $star3_tab_active = null;
        $test_tab_active = null;
        $keytouch_tab_active = null;
        $currentmonth_tab_active = null;
        $english1_tab_active = null;
        $english2_tab_active = null;
        $english3_tab_active = null;
        //タイピング画面から戻るとき、タイピングしていたコースのタブを表示
        if($request->isMethod('post') && $request->has('gotopbtn')){     
            if($request->tab=='map'){
                $map_tab_active = 'active';//bootstrapのタブを使っている。css のclassに「active」があると、表示される
            } elseif($request->tab=='star1'){
                $star1_tab_active = 'active';
            } elseif($request->tab=='star2'){
                $star2_tab_active = 'active';
            } elseif($request->tab=='star3'){
                $star3_tab_active = 'active';
            } elseif($request->tab=='test'){
                $test_tab_active = 'active';
            } elseif($request->tab=='key'){
                $keytouch_tab_active = 'active';
            } elseif ($request->tab=='month'){
                $currentmonth_tab_active = 'active';
            } elseif ($request->tab=='english1'){
                $english1_tab_active = 'active';
            } elseif ($request->tab=='english2'){
                $english2_tab_active = 'active';
            } elseif ($request->tab=='english3'){
                $english3_tab_active = 'active';
            } else {
                $star1_tab_active = 'active';
            }
        } else {//最初にTOPを開いた時は、★コースを表示
            $star1_tab_active = "active";
        }
        //タブ1 今月のタイピング
        $courses1 = Course::where('invalid',0)
            ->where('is_free_course',1)
            ->where('tab','month')
            ->orderBy('display_order','ASC')
            ->get();
        //タブ2 ★コース
        $courses2 = Course::where('invalid',0)
            ->where('is_free_course',1)
            ->where('tab','star1')
            ->orderBy('display_order','ASC')
            ->get();
        //タブ3　★★コース
        $courses3 = Course::where('invalid',0)
            ->where('is_free_course',1)
            ->where('tab','star2')
            ->orderBy('display_order','ASC')
            ->get();
        //タブ4　★★★コース
        $courses4 = Course::where('invalid',0)
                ->where('is_free_course',1)
                ->where('tab','star3')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ5　テストモード
        $courses5 = Course::where('invalid',0)
                ->where('is_free_course',1)
                ->where('tab','test')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ6　キータッチ
        $courses6 = Course::where('invalid',0)
                ->where('is_free_course',1)
                ->where('tab','key')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ7　地図＆世界遺産
        $courses7 = Course::where('invalid',0)
                ->where('is_free_course',1)
                ->where('tab','map')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ8　英語1
        $courses8 = Course::where('invalid',0)
                ->where('is_free_course',1)
                ->where('tab','english1')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ9　英語2
        $courses9 = Course::where('invalid',0)
                ->where('is_free_course',1)
                ->where('tab','english2')
                ->orderBy('display_order','ASC')
                ->get();
        //タブ10　英語3
        $courses10 = Course::where('invalid',0)
                ->where('is_free_course',1)
                ->where('tab','english3')
                ->orderBy('display_order','ASC')
                ->get();
            

        //音あり、アルファベットあり
        $sound = $request->hasCookie('ntyping_option_sound') ? $request->cookie('ntyping_option_sound') : 'yes';
        $alpha = $request->hasCookie('ntyping_option_alphabet') ? $request->cookie('ntyping_option_alphabet') : 'yes';

        //bladeで使うため、変数用意
        $formUrl='/';

        //入門ラッキーボタン　courses2のidを配列にいれ、bladeに渡す。どれを出すかはJS処理
        foreach ($courses2 as $cs2) {
            $nyumon[]=$cs2->id;
        }
        //中級ラッキーボタン　courses1,courses3,courses7のidを配列にいれ、bladeに渡す。3つをマージする
        foreach ($courses1 as $cs1) {
            $cyukyu1_ids[]=$cs1->id;
        }
        foreach ($courses3 as $cs3) {
            $cyukyu3_ids[]=$cs3->id;
        }
        foreach ($courses7 as $cs7) {
            $cyukyu7_ids[]=$cs7->id;
        }
        $cyukyu = array_merge($cyukyu1_ids, $cyukyu3_ids, $cyukyu7_ids);
        //上級ラッキーボタン　courses4,courses5のidを配列にいれ、bladeに渡す。2つをマージする
        foreach ($courses4 as $cs4) {
            $jokyu4_ids[]=$cs4->id;
        }
        foreach ($courses5 as $cs5) {
            $jokyu5_ids[]=$cs5->id;
        }
        $jokyu = array_merge($jokyu4_ids, $jokyu5_ids);   

        //上部のニュースタイトル見出し　※informationテーブルのtitleカラムで、idが1のデータの決め打ち
        $information=Information::where('id','2')
            ->select('title')
            ->first();
        $newstitle=$information->title;

        $information = Blog::where('private',0)->limit(3)->orderBy('created_at','DESC')->get();
        return view('user.free.menu',compact(
            'nyumon','cyukyu','jokyu','newstitle','information','courses1','courses2','courses3','courses4','courses5','courses6','courses7','courses8','courses9','courses10',
            'sound','alpha','map_tab_active','star1_tab_active','star2_tab_active','star3_tab_active','test_tab_active','keytouch_tab_active','currentmonth_tab_active','english1_tab_active','english2_tab_active','english3_tab_active','formUrl',
        ));
        }
    public function allInfo()
    {
        global $pagenation;
        $information = Blog::where('private',0)
			->orderBy('created_at','DESC')
            ->paginate(20);
        return view('user.free.menuinformation',compact('information'));
    }
}
