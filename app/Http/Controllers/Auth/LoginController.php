<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\LoginLog;
use App\Models\School;
use App\Models\Trial;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use DateTime;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $maxAttempts = 10;     // ログイン試行回数（回）
    protected $decayMinutes = 10;   // ログインロックタイム（分）

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        logger()->debug('<-- LoginController@construct');
        $this->middleware('guest')->except('logout');
    }
    public function username() {
        return 'login_account';
    }
    public function showLoginForm(Request $request){
        logger()->debug('<-- LoginController@showLoginForm');
        $school_id = $request->cookie('ntyping_school_id_cookie_sys');
        $login_account = $request->cookie('ntyping_login_cookie_sys');
        if ($request->hasCookie('ntyping_login_cookie_sys'))
        {
            $remember = 'on';
        } else {
            $remember = null;
        }
        return view('system.login',compact('school_id','school_id','login_account','remember', ));
    }
    public function showSchoolLoginForm(Request $request){
        logger()->debug('<-- LoginController@showSchoolLoginForm');
        $school_id = $request->cookie('ntyping_school_id_cookie_school');
        $login_account = null;
        $remember = null;
        return view('school.login',compact('school_id','login_account','remember', ));
    }
    public function showUserLoginForm(Request $request){
        logger()->debug('<-- LoginController@showUserLoginForm');
        $school_id = $request->cookie('ntyping_school_id_cookie_user');
        $login_account = $request->cookie('ntyping_login_cookie_user');
        if ($request->hasCookie('ntyping_login_cookie_user'))
        {
            $remember = 'on';
        } else {
            $remember = null;
        }
        return view('user.login',compact('school_id','login_account','remember' ));
    }
    public function systemLogin(Request $request)
    {
        $school_id = $request->input('schoolId');
        $login_account = $request->input('login_account');
        $password = $request->input('password');
        $remember = $request->input('remember');
        logger()->debug('<-- LoginController@login parameter='.$login_account.' '.$password.' '.$school_id);

        if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        if (Auth::attempt(['login_account' => $login_account, 'password' => $password, 'school_id' => $school_id])) {
            // Authentication was successful...
            $request->session()->regenerate();
            session()->put('school_id',$school_id);
            $school = School::where('school_id','=',auth()->user()->school_id)->first();
            $user= User::where('id',auth()->user()->id)->first();

            LoginLog::create(['login_from'=>'システム管理画面','login_id'=>$login_account,'login_username'=>$user->name,'login_schoolname'=>$school->name,
                'school_id'=>$school_id,'country'=>'','os'=>$this->getOS(),'browser'=>$this->getBrowser(),''=>'','ipaddress'=>$request-> ip(),
                'login_user_id'=>$user->id,'login_school_int_id'=>$school->id,'role'=>$user->role]);
            Cookie::queue('ntyping_last_login_role', 'システム管理者', 60*24*90);   //90日有効
            Cookie::queue('ntyping_school_id_cookie_sys', $school_id, 60*24*90);   //90日有効
            if($remember === 'remember'){
                Cookie::queue('ntyping_login_cookie_sys', $login_account, 60*24*90);   //90日有効
            }else{
                Cookie::queue(Cookie::forget('ntyping_login_cookie_sys'));
            }
            $response = redirect()->route('systemLoginForm',$school_id);
        }
        $this->incrementLoginAttempts($request);
        logger()->debug('Login Failed');

        return back()->withErrors([
            'login_account' => 'ログイン情報が正しくありません。',
        ]);
    }
    public function schoolLogin(Request $request)
    {
        $school_id = $request->input('schoolId');
        $login_account = $request->input('login_account');
        $password = $request->input('password');
        $remember = $request->input('remember');
        logger()->debug('<-- LoginController@login parameter='.$login_account.' '.$password.' '.$school_id);

        if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        //トライアルのユーザーは期限が過ぎていたらログインさせない処理
        $u = User::where('login_account',$login_account)
            ->where('school_id',$school_id)
            ->get()->first();
        if($u !== null){
            if($u->trial_flg == 1){
                logger()->debug('trial term check');
                $trial = Trial::where('school_id',$u->school_int_id)
                    ->where('class_id',$u->class_id)
                    ->first();
                $time1 = new DateTime($trial->end_of_trial);
                $time2 = new DateTime(now());

                $diff = $time2->diff($time1)->format('%R%a');
                logger()->debug($diff);
                if($diff < 0){
                    logger()->info('expiration of a term');
                    return redirect()->route('userLoginForm');
                }
                logger()->debug($trial);
            }
        }
        //学校IDが無効の場合ログインさせない処理
        $school = School::where('school_id','=',$request->schoolId)->first();
        if($school !== null){
            if($school->invalid == true){
                return back()->withErrors([
                    'login_account' => '学校IDが無効です',
                ]);
                return redirect()->route('userLoginForm');
            }
        }
        if (Auth::attempt(['login_account' => $login_account, 'password' => $password, 'school_id' => $school_id])) {
            // Authentication was successful...
            $request->session()->regenerate();
            session()->put('school_id',$school_id);
            $user= User::where('id',auth()->user()->id)->first();
            LoginLog::create(['login_from'=>'学校管理画面','login_id'=>$login_account,'login_username'=>$u->name,'login_schoolname'=>$school->name,
                'school_id'=>$school_id,'country'=>'','os'=>$this->getOS(),'browser'=>$this->getBrowser(),''=>'','ipaddress'=>$request-> ip(),
                'login_user_id'=>$user->id,'login_school_int_id'=>$school->id,'role'=>$user->role]);
            Cookie::queue('ntyping_last_login_role', '学校管理者', 60*24*90);   //90日有効
            Cookie::queue('ntyping_school_id_cookie_school', $school_id, 60*24*90);   //90日有効
            return redirect()->route('schoolLoginForm',$school_id);
        }
        logger()->debug('Login Failed!');
        $this->incrementLoginAttempts($request);
        return back()->withErrors([
            'login_account' => 'ログイン情報が正しくありません。',
        ]);
    }
    public function userLogin(Request $request)
    {
        $school_id = $request->input('schoolId');
        $login_account = $request->input('login_account');
        $password = $request->input('password');
        $remember = $request->input('remember');
        logger()->debug('<-- LoginController@login parameter='.$login_account.' '.$password.' '.$school_id);

        if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        $u = User::where('login_account',$login_account)
            ->where('school_id',$school_id)
            ->get()->first();
        $school = School::where('school_id','=',$school_id)->first();
        //学校が無効の場合ログインさせない処理
        if($school !== null){
            if($school->invalid == true){
                return back()->withErrors([
                    'login_account' => '学校IDが無効です',
                ]);
                return redirect()->route('userLoginForm');
            }
        }
        //トライアルのユーザーは期限が過ぎていたらログインさせない処理
        if($u !== null){
            if($u->trial_flg == 1){
                logger()->debug('trial term check');
                $trial = Trial::where('class_id',$u->class_id)
                    ->first();
                $time1 = new DateTime($trial->end_of_trial);
                $time2 = new DateTime(now());

                $diff = $time2->diff($time1)->format('%R%a');
                logger()->debug($diff);
                if($diff < 0){
                    logger()->info('expiration of a term');
                    return back()->withErrors([
                        'login_account' => 'トライアルの有効期限が切れています。',
                    ]);
                }
                logger()->debug($trial);
            }
        }

        if($u !== null && $u->role === '生徒' && $u->password === $password){
            Auth::login($u, true);
            $request->session()->regenerate();
            session()->put('school_id',$school_id);
            Cookie::queue('ntyping_last_login_role', '生徒', 60*24*90);   //90日有効
            Cookie::queue('ntyping_school_id_cookie_user', $school_id, 60*24*90);   //90日有効
            if($remember === 'remember'){
                Cookie::queue('ntyping_login_cookie_user', $login_account, 60*24*90);   //90日有効
            }else{
                Cookie::queue(Cookie::forget('ntyping_login_cookie_user'));
            }
            return redirect()->route('userLoginForm');
        }

        //ユーザ画面で、ログインアカウントの先頭文字が’a’、’b’、’k’は特殊にしてログインさせない
        logger()->debug(substr($login_account, 0, 1) . ' : ' . $login_account);
        if(substr($login_account, 0, 1) === 'a' ||
            substr($login_account, 0, 1) === 'b' ||
            substr($login_account, 0, 1) === 'k' ){
            logger()->debug('This account cant user login. : ' . $login_account);
            }else{
            if (Auth::attempt(['login_account' => $login_account, 'password' => $password, 'school_id' => $school_id])) {
                logger()->debug('Authentication was successful... : ' . $login_account);
                // Authentication was successful...
                $request->session()->regenerate();
                LoginLog::create(['login_from'=>'先生管理画面','login_id'=>$login_account,'login_username'=>$u->name,'login_schoolname'=>$school->name,
                    'school_id'=>$school_id,'country'=>'','os'=>$this->getOS(),'browser'=>$this->getBrowser(),''=>'','ipaddress'=>$request-> ip(),
                    'login_user_id'=>$u->id,'login_school_int_id'=>$school->id,'role'=>$u->role]);
                Cookie::queue('ntyping_last_login_role', '先生', 60*24*90);   //90日有効
                Cookie::queue('ntyping_school_id_cookie_user', $school_id, 60*24*90);   //90日有効

                if($remember === 'remember'){
                    Cookie::queue('ntyping_login_cookie_user', $login_account, 60*24*90);   //90日有効
                }else{
                    Cookie::queue(Cookie::forget('ntyping_login_cookie_user'));
                }
                return redirect()->route('userLoginForm');
            }
        }
        logger()->debug('LoginFailed');
        $this->incrementLoginAttempts($request);
        return back()->withErrors([
            'login_account' => 'ログイン情報が正しくありません。',
        ]);
    }
    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }
    public function getBrowser(){
        $ua =  $_SERVER['HTTP_USER_AGENT'];
        $browser = "Unknown";

        if (strstr($ua, 'Edge') || strstr($ua, 'Edg')) {
            $browser = "Microsoft Edge";
        } elseif (strstr($ua, 'Trident') || strstr($ua, 'MSIE')) {
            $browser = "Microsoft Internet Explorer";
        } elseif (strstr($ua, 'OPR') || strstr($ua, 'Opera')) {
            $browser = "Opera";
        } elseif (strstr($ua, 'Chrome')) {
            $browser = "Google Chrome";
        } elseif (strstr($ua, 'Firefox')) {
            $browser = "Firefox";
        } elseif (strstr($ua, 'Safari')) {
            $browser = "Safari";
        }

        return $browser;

    }
    public function getOS(){
        $ua =  $_SERVER['HTTP_USER_AGENT'];
        $platform = "Unknown";
        if(preg_match('/iphone/i', $ua)){
            $platform = 'iPhone';
          }elseif(preg_match('/ipad/i', $ua)){
            $platform = 'iPad';
          }elseif(preg_match('/android/i', $ua)){
            $platform = 'Android';
          }elseif(preg_match('/windows phone/i', $ua)){
            $platform = 'Windows Phone';
          }elseif(preg_match('/linux/i', $ua)){
            $platform = 'Linux';
          }elseif(preg_match('/macintosh|mac os/i', $ua)) {
            $platform = 'Mac';
          }elseif(preg_match('/windows/i', $ua)){
            $platform = 'Windows';
          }
        return $platform;
    }
}
