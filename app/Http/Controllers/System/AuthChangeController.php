<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Information;
use App\Models\School;
use Illuminate\Support\Facades\Auth;
class AuthChangeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        logger()->debug('<-- AuthChangeContoroller@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
     //   $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function schoolMenu(Request $request)
    {
        logger()->debug('<-- AuthChangeContoroller@schoolMenu');
        $user = Auth::user();
        $user->school_id = $request->school_id;
        $school = School::where('id','=',auth()->user()->school_id)->select('name')->first();
        $information = Information::where('invalid','<>',1)->limit(10)->orderBy('updated_at','DESC')->get();

        return view('school.menu',compact('school','information'));
    }

}
