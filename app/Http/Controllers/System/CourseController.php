<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\School;
use App\Models\Course;
use App\Models\Word;
use App\Models\CourseWord;
//use App\Models\AudioFile;

/**
 *  システム管理メニュー ＞　コース一覧
 * (1)コース集計メソッド
 * (2)コース一覧
 * (3)コース検索
 * (4)コース新規＆更新
 * (5)コース削除
 * (6)単語一覧
 * (7)管理用単語一覧
 * (8)単語（テストモード）管理画面
 * (9)単語新規
 * (10)単語削除
*/
$pagenation = 200;
class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★コース一覧
     * Route::get
     * ※コースに複数の単語が紐づく、コースと単語は1対1の中間テーブルでつなぐ
     * @return view('system.addcourse')
     */
    public function index()
    {
        global $pagenation;
      
        //検索ボックスの初期値
        $display_order=null;
        $input_course_id = null;
        $course_name = null;
        $course_type = null;
        $course_category = null;
        $course_level = null;
        $course_tab = null;
        $school_name = null;
        

        //カテゴリ、レベル、タイプ、タブのセレクトボックスの値
        $category_selection = Course::selectCategoryList();
        $level_selection = Course::selectLevelList();
        $type_selection = config('configrations.COURSE_TYPE');
        $tab_selection = config('configrations.COURSE_TAB');

        //チェックボックスの初期値
        $free = true;
        $notfree = true;
        $invalid = null;
        $valid = true;
        $contest = null;
        //表内の学校セレクトボックスの値
        $school_search_selection = School::selectList();
        //コース一覧のデータ
        $courses = Course::leftjoin('schools','courses.school_id','schools.id')
            ->select('courses.*','schools.name as school_name','courses.id as course_id')
            ->orderBy('school_name', 'ASC')
            ->orderBy('course_type', 'ASC')
            ->orderBy('course_level', 'ASC')
            ->orderBy('display_order', 'ASC')
            ->paginate($pagenation);

            //リターンview
            return view('system.addcourse',compact('courses','display_order','input_course_id',
            'course_name','course_type','school_name','course_category','course_level',
            'category_selection','level_selection','type_selection','invalid',
            'valid','free','notfree','school_search_selection','course_tab','tab_selection','contest'));
    }
    /**
     * ★コース検索
     *Route::match(['get', 'post']
     * @return view('system.addcourse')
     */
    public function searchCourse(Request $request)
    {
        global $pagenation;
//dd($request->all());
        //検索ボックスの検索値を変数に格納
        $display_order = $request->input('i_display_order');
        $school_name = $request->input('i_school_name');
        $input_course_id = mb_convert_kana($request->input('i_course_id'), 'n');
        $course_name = $request->input('i_course_name');
        $course_type = $request->input('i_course_type');
        $course_tab = $request->input('i_course_tab');
        $course_category = $request->input('i_course_category');
        $course_level = $request->input('i_course_level');
        $free = $request->input('i_is_free');
        $notfree = $request->input('i_not_free');
        $invalid = $request->input('i_invalid');
        $valid = $request->input('i_valid');
        $contest = $request->input('i_contest');
        
        //検索結果のコースデータ
        $courses = Course::leftjoin('schools','courses.school_id','schools.id')
            ->select('courses.*','schools.name as school_name','courses.invalid as courses_invalid','courses.id as course_id')
            ->when($school_name != '' || $school_name != null ,function($q) use($school_name){
                return $q->where('schools.name','Like binary',"%$school_name%");
            })->when($course_name != '' || $course_name != null ,function($q) use($course_name){
                return $q->where('course_name','Like binary',"%$course_name%");
            })->when($input_course_id != '' || $input_course_id != null ,function($q) use($input_course_id){
                return $q->where('courses.id','=',"$input_course_id");
            })->when($course_type != '' || $course_type != null ,function($q) use($course_type){
                return $q->where('course_type',$course_type);
            })->when($course_tab != '' || $course_tab != null ,function($q) use($course_tab){
                return $q->where('tab',$course_tab);
            })->when($course_category != '' || $course_category != null ,function($q) use($course_category){
                return $q->where('course_category',$course_category);
            })->when($course_level != '' || $course_level != null ,function($q) use($course_level){
                return $q->where('course_level',$course_level);
            })->when(($free == true) && ($notfree == '' || $notfree == null) ,function($q) use($notfree){
                return $q->where('is_free_course',1);
            })->when(($notfree == true) && ($free == '' || $free == null) ,function($q) use($notfree){
                return $q->where('is_free_course',0);
            })->when(($invalid == true ) && ($valid == null || $valid == '') ,function($q) use($invalid){
                return $q->where('courses.invalid',1);
            })->when(($valid == true ) && ($invalid == null || $invalid == '') ,function($q) use($invalid){
                return $q->where('courses.invalid',0);
            })->when(($contest == 'on' ) ,function($q) {
                return $q->where('courses.contest','>',0);
            })
            
            ->orderBy('display_order','ASC')
            ->paginate($pagenation);

        //リターンviewに渡すデータ
        //検索セレクトボックスの値
        $category_selection = Course::selectCategoryList();
        $level_selection = Course::selectLevelList();
        $type_selection = config('configrations.COURSE_TYPE');
        $tab_selection = config('configrations.COURSE_TAB');

        //表内の学校セレクトボックスの値
        $school_search_selection = School::selectList();

        //成功のフラッシュメッセージが表示されるのを防ぐため
        session()->flash('flash_message', null);

        //リターンview
        return view('system.addcourse',compact('courses','display_order','input_course_id',
            'course_name','course_type','school_name','course_category','course_level',
            'category_selection','level_selection','type_selection','invalid',
            'valid','free','notfree','school_search_selection','course_tab','tab_selection','contest'));
}  
    /**
     * ★コース新規＆更新　ボタンで振り分け
     * Route::match(['get', 'post']　⇒多重送信対策は未実装
     * （１）新規作成で保存ボタン押下　　（２）編集後、保存ボタン押下
     * @return view('system.addcourse')
     */
    public function saveCourse(Request $request)
    {
        global $pagenation;

        //「表示順」「テスト時間」を半角数字に変換 ※array型にしないとひっかかる　ダメ⇒$request->display_order
        //if　をつかわないと、空欄時に「0」がはいる
        if( $request['display_order']){
        $request['display_order'] = mb_convert_kana($request->display_order,'n');
        }
        if( $request['test_time']){
        $request['test_time'] = mb_convert_kana($request->test_time,'n');
        }
        if( $request['contest']){
            $request['contest'] = mb_convert_kana($request->contest,'n');
            }

        //バリデーション実行
        $validator = Validator::make($request->all(), [
            'display_order' => ['integer','between:0,9999','nullable'],
            'course_name' => ['string','max:30','required'],
            'course_category' => ['required'],
            'course_level' => ['required'],
            'course_type' => ['required'],
            'course_tab' => ['required'],
            'test_time' => ['integer','between:0,9999','nullable'],
        ]);
        //バリデーション失敗
        if ($validator->fails()) {
            //成功のフラッシュメッセージが表示されるのを防ぐため
           session()->flash('flash_message', null);
            return redirect('/course')
                ->withErrors($validator);
        }
        //ここから、ボタンで処理切り分け
        //更新（編集後、保存ボタン押下）
        if($request -> has('save')){
            $Course = Course::find($request->input('id'));
        //新規（新規作成後、保存ボタン押下）
        } elseif ($request -> has('update')){
            $Course = new Course();
        }
        //ここから、共通処理　リクエストデータを$Courseに入れる
        $Course->invalid = $request->invalid == '1' ? true : false; // 無効
        $Course->is_free_course = $request->is_free_course == '1' ? true : false; // free
        $Course->display_order = $request->display_order;        // 表示順
        $Course->contest = $request->contest;        // コンテスト
        $Course->course_name = $request->course_name;            // コース名
        $Course->course_category = $request->course_category;    // 分類
        $Course->course_level = $request->course_level;          // レベル
        $Course->random = $request->is_random == '1' ? true : false; // ランダムか
        $Course->test_time = $request->test_time;               // テスト時間
        $Course->course_type = $request->course_type;            // コースタイプ
        $Course->tab = $request->course_tab;            // コースtab
        $Course->school_id = $request->school_id_selected;      // 学校ID 学校独自の追加コースのみ
        
        try{
            $Course->save(); 
            session()->flash('flash_message', 'コースを登録or更新しました'); 
        }catch(\Exception $e){
            logger()->error('システム管理メニュー、コース新規作成or更新でエラー');
            logger()->error($e);
            abort(500);
        }

        //リターンviewに渡すデータ
        $display_order = $request->input('i_display_order');
        $school_name = $request->input('i_school_name');
        $input_course_id = $request->input('i_course_id');
        $course_name = $request->input('i_course_name');
        $course_type = $request->input('i_course_type');
        $course_tab = $request->input('i_course_tab');
        $course_category = $request->input('i_course_category');
        $course_level = $request->input('i_course_level');
        $category_selection = Course::selectCategoryList();
        $level_selection = Course::selectLevelList();
        $type_selection = config('configrations.COURSE_TYPE');
        $tab_selection = config('configrations.COURSE_TAB');
        $free = true;
        $notfree = true;
        $invalid = null;
        $valid = true;
        $contest = null;

        //表内の学校セレクトボックスの値
        $school_search_selection = School::selectList();
        //コース一覧データ（新規or更新した1件のみ表示）
           $courses = Course::where('courses.id',$Course->id)
           ->paginate($pagenation);
        //リターンview
        return view('system.addcourse',compact('courses','display_order','input_course_id',
            'course_name','course_type','school_name','course_category','course_level',
            'category_selection','level_selection','type_selection','invalid',
            'valid','free','notfree','school_search_selection','course_tab','tab_selection','contest'));
    }
    /**
     * ★コース削除
     * Route::post
     * 【重要】コース削除⇒紐づく中間テーブル（course_word)と単語テーブル(words)も削除される ※SQLでベストスコアも自動で削除される
     * @return redirect('/course')
     */
    public function deleteCourse(Request $request)
    {
        //削除するコースのコースIDが[「$checks」に配列として格納
        $checks = $request->checks;

        if($checks !== null ){
            DB::beginTransaction();
            try {
                //(1)単語を削除
                Word::leftJoin('course_word','words.id','course_word.word_id')
                    ->whereIn('course_word.course_id', $checks)->delete();
                //(2)単語とコースをつなぐ中間テーブル（course_word）を削除
                CourseWord::whereIn('course_id', $checks)->delete();
                //(3)コースを削除
                Course::whereIn('id', $checks)->delete();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                logger()->error('システム管理メニュー、コース削除でエラー');
                logger()->error($e);
                abort(500);
            }
            //成功のフラッシュメッセージが表示されるのを防ぐため
            session()->flash('flash_message', 'コースと単語（中間テーブルも）とベストスコアも削除しました。');
            //リダイレクト
            return redirect('/course');
        }
    }

///////////////////////////////////////ここからは単語一覧////////////////////////////////////////////

    /**
     * ★単語一覧　※viewで切り分け（通常コースの単語一覧とテストコース（1問のみ）の単語一覧）
     * Route::post ⇒ページネーションなし
     * （１）コース一覧で単語登録ボタン押下で単語一覧ページに遷移
     * （２）単語一覧ページの「すべて表示」「リセット」ボタン押下（「すべて表示」ボタンでリダイレクトできない（リクエストパラメータにコースIDをもたせるため）
     * 　※ページネーションなし
     * @return view('system.addcourse')     * 
     */
    public function addWord(Request $request)
    {
        //成功のフラッシュメッセージが表示されるのを防ぐため
        session()->flash('flash_message', null);
        //音声ファイル削除のチェックボックス
        $file_delete = null;

        //コース情報とコースに紐づいている学校情報（無料ナレッジタイピングで表示コースは学校情報はない⇒nullがかえる）
        $course_id = $request->input('course_id');
        $course= Course::leftjoin('schools','courses.school_id','schools.id')
            ->where('courses.id',$course_id)
            ->select('courses.id  as course_id','courses.school_id as course_school_id',
                'schools.id as school_int_id','schools.school_id as school_school_id',
                'schools.name as school_name','courses.*')
            -> first();

        //コースに登録されている単語一覧データ　※wordsテーブルでなく中間テーブルのcourseword
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
            ->leftJoin('words','course_word.word_id','words.id')
            ->leftJoin('schools','courses.school_id','schools.id')
            ->where('course_word.course_id',$course_id)
            ->select('course_word.id as cw_id','course_word.word_id as cw_word_id','course_word.course_id as cw_course_id',
                'words.id as word_id','courses.id as course_id','courses.school_id as course_school_id','schools.name as school_name',
                'schools.id as school_id','courses.display_order as course_display_order','words.display_order as word_display_order',
                'courses.*','words.*','course_word.*')
            ->orderByRaw('words.display_order ASC','words.word_kana ASC')
            -> get();
         
        //リターンview　　テストモードは1問で、文字数が最大2000なので別のviewで振り分け
        if($course->tab == 'test' || $course->tab == 'key' || $course->tab == 'long'  ||  $course->tab == 'vld' || $course->course_type == 'テスト'){  
            $courseWords= $courseWords->first();     
            return view('system.courseaddwordtest',compact('courseWords','course','course_id'));
        //通常のコース（単語は複数）
        } else {
          return view('system.courseaddword',compact('courseWords','course','course_id','file_delete'));
        }        
    } 
    /**
     * ★【管理用】ALL単語一覧（中間テーブルつき）
     * 一覧ページ表示と、検索を1つのメソッドにまとめる 
     * Route::match(['get', 'post'],　⇒ページネーションあり　
     * @return view('system.addcourse')
     */
    public function wordList(Request $request)
    {        
        global $pagenation;
        //検索値を変数にセット       
        $course_id = mb_convert_kana($request->course_id,'n');
        $course_name = $request->course_name;
        $course_word_id = mb_convert_kana($request->course_word_id,'n');
        $course_word_name = $request->course_word_name;
        $word_id = mb_convert_kana($request->word_id,'n');
        $word_name = $request->word_name;
        $school_id = mb_convert_kana($request->school_id,'n');
        $school_name = $request->school_name;
        $course_type = $request->course_type;
        $course_tab = $request->course_tab;
        $type_selection = config('configrations.COURSE_TYPE');
        $tab_selection = config('configrations.COURSE_TAB');


        //単語一覧を中間テーブル（course_word）から取得　※コースと単語を一対一でつなぐ
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
        ->leftJoin('words','course_word.word_id','words.id')
        ->leftJoin('schools','courses.school_id','schools.id')
        ->when($course_id != '' || $course_id !=null,function($q) use($course_id){
            return $q->where('course_word.course_id','=',"$course_id");
        })
        ->when($course_name != '' || $course_name !=null,function($q) use($course_name){
            return $q->where('courses.course_name','Like binary',"%$course_name%");
        })
        ->when($course_word_id != '' || $course_word_id !=null,function($q) use($course_word_id){
            return $q->where('course_word.id','Like',"%$course_word_id%");
        })
        ->when($word_id != '' || $word_id !=null,function($q) use($word_id){
            return $q->where('course_word.word_id','=',"$word_id");
        })
        ->when($word_name != '' || $word_name !=null,function($q) use($word_name){
            return $q->where('words.word_mana','Like binary',"%$word_name%");
        })
        ->when($school_id != '' || $school_id !=null,function($q) use($school_id){
            return $q->where('schools.school_id','=',"$school_id");
        })
        ->when($school_name != '' || $school_name !=null,function($q) use($school_name){
            return $q->where('schools.name','Like binary',"%$school_name%");        
        })
        ->when($course_type != '' || $course_type != null ,function($q) use($course_type){
            return $q->where('course_type',$course_type);
        })
        ->when($course_tab != '' || $course_tab != null ,function($q) use($course_tab){
            return $q->where('course_tab',$course_tab);
        })
        
        ->select('course_word.id as cw_id','course_word.word_id as cw_word_id','course_word.course_id as cw_course_id','courses.invalid as course_invalid',
            'words.id as word_id','courses.id as course_id','courses.school_id as course_school_id','schools.name as school_name',
            'schools.id as school_int_id','schools.school_id as school_id','courses.display_order as course_display_order','words.display_order as word_display_order',
            'courses.*','words.*','course_word.*')
        ->orderBy('course_word.word_id','ASC')
        ->paginate($pagenation);
        //成功のフラッシュメッセージが表示されるのを防ぐため
        session()->flash('flash_message', null);

        //リターンview
        return view('system.coursewordlist',compact('courseWords','course_id','course_name','course_word_id','word_id',
                    'word_name','school_id','school_name','type_selection','course_type'));
    }

    /**
     * ★テストモードの単語画面 　※3つのボタンで処理を切り分け
     * Route::post ⇒ページネーションなし
     * (1)更新ボタン押下　(2)新規登録ボタン押下　(3)削除ボタン押下
     * 【重要】単語を新規登録すると、コースをつなぐ中間テーブル（course_word）も作成or削除する
     * @return view('system.addcourse')
     */
    public function wordTest(Request $request)
    {
        logger()->debug('<-- CourseController@wordTest');      
        //dd($request->all());
        //コース情報とコースに紐づいている学校情報（無料ナレッジタイピングで表示コースは学校情報はない⇒nullがかえる）
        $course_id = $request->input('course_id');
        $course= Course::leftjoin('schools','courses.school_id','schools.id')
        ->where('courses.id',$course_id)
        ->select('courses.id  as course_id','courses.school_id as course_school_id','courses.invalid as invalid',
            'schools.id as school_int_id','schools.school_id as school_school_id',
            'schools.name as school_name','courses.*')
        -> first();

        //(1)「更新ボタン」押下時（ボタン名：test_save）
        if($request->has('test_save')){
            $word = Word::find($request->input('word_id')); 
            $word -> word_mana = $request->input('word_mana');
            try{
                $word -> save();
                session()->flash('flash_message', '単語を更新しました');
            } catch (\Exception $e) {
                logger()->error('システム管理メニュー、単語の更新でエラー');
                logger()->error($e);
                abort(500);
            }         
        //(2)新規ボタン押下時　　Wordのインスタンスをつくり、単語情報を$wordにセット
        } else if($request->has('test_add')){  
            //dd($request->all());
            $word = new Word();
            $word -> word_mana = $request->input('word_mana');        
            DB::beginTransaction();
            try{
                $word->save();
                //コースと単語をつなぐcourse_wordテーブルにcourse_idとword_id をセットして保存
                $courseWord = new CourseWord();
                $courseWord->course_id = $request->course_id;
                $courseWord->word_id = $word->id;
                $courseWord->save();

                session()->flash('flash_message', '単語を登録しました');
                DB::commit();
            }catch(\Exception $e){
                logger()->error('システム管理メニュー、新規単語の保存（wordsテーブルとcourse_wordテーブル');
                logger()->error($e);
                DB::rollback();
                abort(500);
            }
        //(3)「削除ボタン」押下時（モーダルの削除ボタンで処理実行）
        }  else if($request->has('modal_del_btn')){                 
            DB::beginTransaction();
            //dd($request->all());
            try {
                //(1)単語を削除
                 Word::destroy($request->input('word_id'));
                

                //(2)単語とコースをつなぐ中間テーブル（course_word）を削除
                CourseWord::where('word_id',$request->input('word_id'))->delete();

                 session()->flash('flash_message', '単語を削除しました。（関連する中間テーブルも削除）');
                DB::commit();
            }catch(\Exception $e){
                logger()->error('システム管理メニュー、テストモードの単語削除でエラー');
                logger()->error($e);
                DB::rollback();
                abort(500);
            }
            return redirect('/course');
        }
        //ここから新規＆更新　共通　リターンviewに渡すデータ
        //コースに登録されている単語一覧データ　※wordsテーブルでなく中間テーブルのcourseword
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
            ->leftJoin('words','course_word.word_id','words.id')
            ->leftJoin('schools','courses.school_id','schools.id')
            ->where('course_word.course_id',$course_id)
            ->select('course_word.id as cw_id','course_word.word_id as cw_word_id','course_word.course_id as cw_course_id',
                'words.id as word_id','courses.id as course_id','courses.school_id as course_school_id','schools.name as school_name',
                'schools.id as school_id','courses.display_order as course_display_order','words.display_order as word_display_order',
                'courses.*','words.*','course_word.*')
            ->orderByRaw('words.display_order ASC','words.word_kana ASC')
            -> first();            
        
        //リターンview
        return view('system.courseaddwordtest',compact('courseWords','course','course_id',));
    }

    /**
     * ★単語の新規作成　
     * 【重要】単語を新規登録すると、コースをつなぐ中間テーブル（course_word）も作成する
     * 　Route::post ⇒ページネーションなし
     * @return view('system.addcourse')
     */
    public function addwordAdd(Request $request)
    {
        logger()->debug('<-- CourseController@addwordAdd');

        //単語一覧表示に必要なデータ
        $file_delete = $request->input('file_delete','off');

        
        //コース情報とコースに紐づいている学校情報（無料ナレッジタイピングで表示コースは学校情報はない⇒nullがかえる）
        $course_id = $request->input('course_id');
        $course= Course::leftjoin('schools','courses.school_id','schools.id')
        ->where('courses.id',$course_id)
        ->select('courses.id  as course_id','courses.school_id as course_school_id',
            'schools.id as school_int_id','schools.school_id as school_school_id',
            'schools.name as school_name','courses.*')
        -> first();

        //表示順を半角数字にして、バリデーション実行
        if ($request -> display_order){
            $request['display_order'] = mb_convert_kana($request['display_order'], 'n');
        }        
        $validator = Validator::make($request->all(), [
            'display_order' => ['nullable','integer','between:0,9999'],
            'word_mana' => ['required','string','max:50'],
            //カタカナ、漢字不可  「＝」はエラーで登録できない
            'word_kana' => ['required', 'regex:/^[ あ-ん゛゜、。,.!?%()#$-@*+><;:　っゎぁ-ぉゃ-ょーA-Za-z0-9]+$/u','max:40'],
            'audio' => ['nullable','mimes:mp3','max:300'],
        ]);

        //バリデーション失敗時
        if ($validator->fails()) {
            //リターンwiewに渡すデータ
                //コースに登録されている単語一覧データ　※wordsテーブルでなく中間テーブルのcourseword
                $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
                ->leftJoin('words','course_word.word_id','words.id')
                ->leftJoin('schools','courses.school_id','schools.id')
                ->where('course_word.course_id',$course_id)
                ->select('course_word.id as cw_id','course_word.word_id as cw_word_id','course_word.course_id as cw_course_id',
                    'words.id as word_id','courses.id as course_id','courses.school_id as course_school_id','schools.name as school_name',
                    'schools.id as school_id','courses.display_order as course_display_order','words.display_order as word_display_order',
                    'courses.*','words.*','course_word.*')
                ->orderByRaw('words.display_order ASC','words.word_kana ASC')
                -> get();

            //成功のフラッシュメッセージが表示されるのを防ぐため
            session()->flash('flash_message', null);
            //リターンview
            return view('system.courseaddword',compact('courseWords','course','course_id','file_delete'))
                ->withErrors($validator);
        }
        //(1)更新　⇒編集後の保存ボタン押下
        if($request -> has('save')){
            $word = Word::find($request->word_id);
            $flashmessage = '」の情報を更新しました';  //成功時のフラッシュメッセージの内容


        //(2)新規　⇒新規作成後の保存ボタン押下 ※名前が逆だが気にせず・・・
        } else if ($request -> has('update')){
            $word = new Word();
            $flashmessage = '」を登録しました';   //成功時のフラッシュメッセージの内容 


        }        
        
        //ここから、新規＆更新の共通処理　⇒単語データを$wordにセット
        $word -> display_order = $request->display_order;
        $word -> word_mana = $request->word_mana;
        $word -> word_kana = $request->word_kana;
        //dd($request->word_kana);
        //チェックボックスはチェックなしだとキーを返さない。
        if($request ->has('file_delete') && $word->pronunciation_file_name !== null){
            //音声ファイル名を削除
            $word->pronunciation_file_name = null;
        }        

        //音声ファイルの更新
        if($request->has('audio')){
            //ファイル名変換処理
            $fname = sprintf('%04d', $course -> school_int_id).uniqid(bin2hex(random_bytes(1))).'.mp3';
            logger()->debug('fname = '.$fname);
            $file = request()->file('audio');
            $file->storeAs('public/audio',$fname);
            $word->pronunciation_file_name = $fname;
        }  
       //保存
        DB::beginTransaction();
        try{
            $word->save();
            
            //新規のみ、コースと単語をつなぐcourse_wordテーブルにcourse_idとword_id をセットして保存
            if ($request -> has('update')){
                $courseWord = new CourseWord();
                $courseWord->course_id = $request->course_id;
                $courseWord->word_id = $word->id;
                $courseWord->save();
            }
            
            session()->flash('flash_message', '単語を登録or更新しました');
            DB::commit();
        }catch(\Exception $e){
            logger()->error('システム管理メニュー、単語の新規＆保存でエラー（wordsテーブルとcourse_wordテーブル');
            logger()->error($e);
            DB::rollback();
            abort(500);
        }
       
        //リターンview渡すデータ　新規登録した単語を一番上に表示
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
            ->leftJoin('words','course_word.word_id','words.id')
            ->leftJoin('schools','courses.school_id','schools.id')
            ->where('course_word.course_id',$course_id)
            ->select('course_word.id as cw_id','course_word.word_id as cw_word_id','course_word.course_id as cw_course_id',
                'words.id as word_id','courses.id as course_id','courses.school_id as course_school_id','schools.name as school_name',
                'schools.id as school_id','courses.display_order as course_display_order','words.display_order as word_display_order',
                'courses.*','words.*','course_word.*')
            ->orderByRaw('words.updated_at DESC')
            -> get();
        
        //リターンview       
            return view('system.courseaddword',compact('courseWords','word','course','file_delete'));
            
        }       
    /**
     * ★単語の削除　【重要】関連するcourse_wordテーブルのデータも削除
     * Route::post ⇒ページネーションなし
     * (1)削除チェックした単語を削除　(2)テストモードの単語（1問のみ）削除
     * @return view('school.courseaddword')
     */
    public function deleteWord(Request $request)
    {
        //dd($request->all());
        logger()->debug('<-- CourseController@deleteWord');
        $file_delete = $request->input('file_delete');

       
        //コース情報とコースに紐づいている学校情報（無料ナレッジタイピングで表示コースは学校情報はない⇒nullがかえる）
       $course_id = $request->input('course_id');
       $course= Course::leftjoin('schools','courses.school_id','schools.id')
       ->where('courses.id',$course_id)
       ->select('courses.id  as course_id','courses.school_id as course_school_id',
           'schools.id as school_int_id','schools.school_id as school_school_id','courses.invalid as invalid',
           'schools.name as school_name','courses.*')
       -> first();

        //削除するコースのコースIDが$checks　に配列として格納
        $checks = $request->checks;
                
        DB::beginTransaction();
        try {
            //(1)単語を削除
            Word::whereIn('id', $checks)->delete();

            //(2)単語とコースをつなぐ中間テーブル（course_word）を削除
            CourseWord::whereIn('word_id', $checks)->delete();
            session()->flash('flash_message', '単語を削除しました');
            DB::commit();
        }catch(\Exception $e){
            logger()->error('システム管理メニュー、単語削除＆学校の単語登録数更新でエラー');
            logger()->error($e);
            DB::rollback();
            abort(500);
        }
        
        //リターンview渡すデータ　新規登録した単語を一番上に表示
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
            ->leftJoin('words','course_word.word_id','words.id')
            ->leftJoin('schools','courses.school_id','schools.id')
            ->where('course_word.course_id',$course_id)
            ->select('course_word.id as cw_id','course_word.word_id as cw_word_id','course_word.course_id as cw_course_id','courses.invalid as invalid',
                'words.id as word_id','courses.id as course_id','courses.school_id as course_school_id','schools.name as school_name',
                'schools.id as school_id','courses.display_order as course_display_order','words.display_order as word_display_order',
                'courses.*','words.*','course_word.*')
            ->orderByRaw('words.updated_at DESC')
            -> get();
        
        //リターンview       
            return view('system.courseaddword',compact('courseWords','course','file_delete'));
    }
}