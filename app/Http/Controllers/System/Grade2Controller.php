<?php

namespace App\Http\Controllers\system;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\School;
use App\Models\User;
use Exception;

    /**
     * システム管理メニュー　＞　【学校別】学年設定
     * (1)学校選択
     * (2)学年一覧表示
     * (3)検索
     * (4)新規＆更新
     * (5)削除
     */
class Grade2Controller extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- Grade2Controller@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
    }
    /**
     * ★学校選択
     * Route::get
     * @return view('system.grade')
     */
    public function schoolSelect(Request $request)
    {
        logger()->debug('<-- Grade2Controller@schoolSelect');
        //学校のセレクトボックス値（メソッドはモデルで定義）
        $school_search_selection = School::selectList();
        natcasesort($school_search_selection); //より自然にソート昇順　第1５小学校等、数字と文字が混在している時に希望の挙動
        
        $selected_school_int_id = null; //初期値
        return view('system.selectschool',compact('school_search_selection','selected_school_int_id'));
    }   
    /**
     * ★学年一覧
     * Route::post
     * @return view('system.grade2')
     */
    public function index(Request $request)
    {
        logger()->debug('<-- Grade2Controller@index');
        //dd($request->all());
        //選んだ学校のID
        //前ページの学校選択なら「$request->select_school_id」、その他(hiddenで持つ）「$request->hidden_school_id」
        if($request->hidden_school_id){
            $selected_school_int_id = $request->hidden_school_id;
        } else {
            $selected_school_int_id = $request->select_school_id;
        }
        //学校情報（ページ上部に表示）
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //ユーザデータ
        $users = User::leftJoin('classes','class_id','classes.id')
            ->where('classes.school_id','=',$selected_school_int_id)
            ->select('classes.grade_id')
            ->get();

        //学年データ
        $grades = Grade::leftJoin('schools','grade.school_id','schools.id')
            ->where('grade.school_id','=',$selected_school_int_id)
            ->select('schools.name as school_name','grade.name as grade_name','schools.school_id as sc_id',
                'grade.id as grade_id','schools.id as school_id','grade.*')
            ->orderByRaw('school_name ASC ,CHAR_LENGTH(grade.name) ASC,grade.name ASC') 
            ->get();

        //学年の数（ページネーションがないため、表示数を手動で設定）
        $current_grade_num=$grades->count(); //ページに表示されている件数
        $grade_num=$grades->count(); //学年数　※一覧なので同数

        //リターンview
        return view('system.grade2',compact('grades','users','school_name','school','current_grade_num','grade_num'));
    }    
    /**
     * ★新規＆更新　※ボタンで分岐　　※同一学校で重複した学年を作るとエラー（DBで処理）
     * Route::post
     * @return return view('system.grade2')
     */
    public function save(Request $request)
    {
        //dd($request->all());
        //選んだ学校のID
        //前ページの学校選択なら「$request->select_school_id」、その他(hiddenで持つ）「$request->hidden_school_id」
        if($request->hidden_school_id){
            $selected_school_int_id = $request->hidden_school_id;
        } else {
            $selected_school_int_id = $request->select_school_id;
        }
        
        //【ここから分岐】編集の保存ボタン押下時
        if($request->has('save')){
            $validator = Validator::make($request->all(), [
                'grade_name' => ['string','max:10','required'],
            ]);
            $grade = Grade::find($request->id);
            //更新データをセット
            $grade->name = mb_convert_kana($request->grade_name,'n');

        //新規ボタン押下時
        }elseif($request->has('update')){
            //dd($request->all());
            $validator = Validator::make($request->all(), [
                'grade_name_new' => ['string','max:10','required'],
            ]);
            //新規データをセット
            $grade = new Grade();
            $grade->name = mb_convert_kana($request->grade_name_new,'n');
            $grade->school_id = $selected_school_int_id;
        }
        //【ここから共通処理】バリデーション失敗時
        if ($validator->fails()) {
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            //リダイレクトの代わりにindexメソッド
            return $this -> index($request)        
                ->withErrors($validator);
        }
        try{
            $grade->save();
            session()->flash('flash_message', '学年を追加or更新しました');
        }catch(Exception $e){
            if($e->errorInfo[1] === 1062 ||$e->errorInfo[1] !== null){
                logger()->info('The grade you tried to create a new one is a duplicate.');
                //リダイレクトの代わりにindexメソッド
                return $this->index($request)
                    ->withErrors(['message' => '同じ名前の学年が既にあります']);
            }else{
                //それ以外はエラーページへ
                logger()->error('The grade save has unknown error.');
                logger()->error($e);
                abort(500);
            }
        }
        //リダイレクトの代わりにindexメソッド
        return $this -> index($request);
    }

    /**
     * ★削除　※クラスが存在すると削除できない （DBで処理）
     * Route:post
     * @return return view('system.grade2')
     */
    public function delete(Request $request)
    {
        global $pagenation;
        logger()->debug('<-- Grade2Controller@delete');

        //削除チェックした学年IDが$checksに配列で格納
        $checks = $request->checks;
        //削除
        if($checks != null ){
            DB::beginTransaction();
            foreach($checks as $check){
                try{
                    logger()->debug(intval($check));
                    Grade::destroy(intval($check));
                }catch(Exception $e){
                    if($e->errorInfo[1] === 1451){      //外部キー違反
                        logger()->info('The grade cant destroy,cause alredy in use classes.');
                        DB::rollBack();
                            //リダイレクトの代わりにindexメソッド
                            return $this->index($request)
                            ->withErrors(['message' => '選択された学年は既にクラスで使用されているため削除できません']);
                        DB::rollBack();
                    }else{
                        DB::rollBack();
                        logger()->error('The grade destroy unknown error.');
                        logger()->error($e);
                        abort(500);
                    }
                }
            }
            DB::commit();
            session()->flash('flash_message', '学年を削除しました');
        }
        
        //リダイレクトの代わりにindexメソッド
        return $this->index($request);
    }
}