<?php

namespace App\Http\Controllers\system;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Exports\Export;
use App\Models\User;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\Clas;
use PhpOffice\PhpSpreadsheet\Writer\Ods\Content;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\System\UserImport;
use Exception;

    /**
     * システム管理メニュー　＞　ユーザー管理
     * (1)ユーザー一覧表示
     * (2)検索＆エクセル出力＆学校選択
     * (3)エクセル一括登録
     * (4)新規＆更新＆パスワードリセット
     * (5)エクセル一括登録用のテンプレートダウンロード
     * (6)削除
     */
$pagenation =200;
class UserInfoController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- UserInfoController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★ユーザー一覧
     * @return view('system.userinfo)
     * Route::get
     */
    public function index()
    {
        logger()->debug('<-- UserInfoController@index');
        global $pagenation;

        //検索　ロールのチェックボックス
        $school_adm = true;
        $teacher = true;
        $student = true;
        //検索ボックス
        $school_name = null;
        $attendance_no = null;
        $grade_name = null;
        $kumi_name = null;
        $user_name = null;
        $login_account = null;

        //学校のセレクトボックス値
        $school_search_selection = School::selectList();
        //natcasesort($school_search_selection); //より自然にソート昇順

        //選んだ学校のID(学校を選択しないと、ユーザーの新規、編集ができない)
        $school_name_search = null;
        //表の中の「学年」「組」のセレクトボックス（modelで関数を作っている）編集時にアクティブ
        $grade_selection = Grade::selectListId();
        $kumi_selection = Kumi::selectListId();
        //表の中の「ロール」セレクトボックス、configrationsから値を取得　ただし新規作成以外は編集不可
        $roleSelection = config('configrations.ROLE_LIST3');
        //フラグ　学校選択ボタン、新規学校作成ボタンどちらかを表示させるかのフラグ
        $selected = false;
        //フラグ　yesだと編集モード、noだと検索モード、cheakedit-user.js で処理 検索モードだと一覧の編集ボタンが非アクディブ
        $school_selected_flg= 'no';

        //bladeページ一番下のdivのhiddenで使用、選んだ学校に対応した学年と組のセレクトボックスの値と名前
        $grade_new_selection = Grade::selectListId();
        $kumi_new_selection = Kumi::selectListId();
        $grade_new_select = null;
        $kumi_new_select = null;
        //ユーザーデータ
        $users = User::leftJoin('schools','users.school_int_id','=','schools.id')
            ->leftJoin('classes','users.class_id','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('schools.name as school_name','users.name as user_name','users.id as user_id',
                'role','class_id','attendance_no','users.login_account','schools.id as school_id','schools.school_id as sc_id',
                'kumi.name as kumi_name','grade.name as grade_name','grade.id as grade_id','kumi.id as kumi_id','users.trial_flg',
                'schools.password_school_init','schools.password_teacher_init','schools.password_student_init',)
            ->where('users.role','!=','システム管理者')
            ->orderByRaw('schools.name ASC,role ASC,classes.id ASC,user_name')
            ->paginate($pagenation);

            //リターンview
            return view('system.userinfo',compact('users','school_adm','teacher','student'
                ,'school_name','attendance_no','user_name','login_account','roleSelection',
                'grade_selection','kumi_selection','school_search_selection','selected','school_name_search',
                'grade_new_selection','grade_new_select','kumi_new_selection','kumi_new_select','grade_name','kumi_name','school_selected_flg'));
    }
    /**
     * ★検索＆エクセル出力＆学校選択
     * Route::match(['get', 'post']
     * 【重要】検索後、学校を選ぶときに、検索条件を引き継ぐために、検索ボタンと学校選択ボタンを同じformに入れてる
     * 【重要】検索結果をエクセル出力するので、同じformに入れている
     * （１）エクセル出力ボタン押下時　（２）検索ボタン押下時　（３）学校選択ボタン押下時
     *　システム管理メニューの学校管理からの遷移（$requestを持ちまわる）あり
     * @return  view('system.userinfo')　⇒検索＆学校選択
     * @return　Excel::download　⇒エクセル出力
     */
    public function search(Request $request)
    {
        logger()->debug('<-- UserInfoController@search');
        global $pagenation;
        //dd($request->all());
        //ロールのチェックボックスの検索地を変数にセット
        $school_adm = $request->input('school_adm');
        $teacher = $request->input('teacher');
        $student = $request->input('student');

        //検索値を変数にセット
        $school_name = $request->input('school_name');
        $attendance_no = $request->input('s_attendance_no');
        $grade_name = $request->input('grade_name');
        $kumi_name = $request->input('kumi_name');
        $user_name = $request->input('s_user_name');
        $login_account = $request->input('login_account');

        //表内の学年、クラスのセレクトボックスの値と名前（編集時にアクティブになるためセレクトボックス）
        $grade_selection = Grade::selectList();
        $kumi_selection = Kumi::selectList();
        $roleSelection = config('configrations.ROLE_LIST3');
        //セレクトボックスは、選択しないと、何もかえさないため値を代入
        $selected = $request->input('selected');
                if($selected === null){
            $selected = false;
        }
        //フラグ　no⇒検索モード、yes⇒編集、新規作成モード cheakedit-users.js で処理
        $school_selected_flg= 'no';

        //学校を選ぶセレクトボックスと、選んだ学校のID
        $school_search_selection = School::selectListByName($school_name);
        $school_name_search = $request->input('school_name_search');

        //bladeファイルの一番下にdevのhidden内で使用ファイル　⇒学校に応じた学年、組が表示される
        $grade_new_selection = Grade::selectListById($school_name_search);
        $kumi_new_selection = Kumi::selectListById($school_name_search);
        $grade_new_select = null;
        $kumi_new_select = null;
        //学校管理者、先生、生徒にチェックがあるものを$selectRoleの配列にいれる⇒SQLの検索条件を配列に入れる
        $selectRole = [];
        if($school_adm !== null){
            array_push($selectRole,'学校管理者');
        }
        if($teacher !== null){
            array_push($selectRole,'先生');
        }
        if($student !== null){
            array_push($selectRole,'生徒');
        }
        //(1)◆エクセル出力ボタン押下時（ボタンの名前がexcelout）
        if($request->has('excelout')){

            $users = User::leftJoin('schools','users.school_int_id','=','schools.id')
            ->leftJoin('classes','users.class_id','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->whereIn( 'users.role', $selectRole )  //チェックの入っているものが配列に入っている
            ->when($school_name !== null ,function($q) use($school_name){
                return $q->where('schools.name','Like binary',"%$school_name%");
            })
            ->when($grade_name !== null ,function($q) use($grade_name){
                return $q->where('grade.name','Like binary',"%$grade_name%");
            })
            ->when($kumi_name !== null ,function($q) use($kumi_name){
                return $q->where('kumi.name','Like binary',"%$kumi_name%");
            })
            ->when($user_name !== null ,function($q) use($user_name){
                return $q->where('users.name','Like binary',"%$user_name%");
            })
            ->when($attendance_no !== null ,function($q) use($attendance_no){
                return $q->where('users.attendance_no',$attendance_no);
            })
            ->when($login_account !== null ,function($q) use($login_account){
                return $q->where('users.login_account',$login_account);
            })
            ->select('schools.name as school_name','users.name as user_name','users.id as user_id','users.created_at','users.updated_at',
                'role','class_id','attendance_no','users.login_account','schools.id as school_int_id','schools.school_id as school_id',
                'kumi.name as kumi_name','grade.name as grade_name','grade.id as grade_id','kumi.id as kumi_id',
                'schools.password_school_init','schools.password_teacher_init','schools.password_student_init',)
            ->orderByRaw('schools.name ASC')
            ->orderByRaw('CHAR_LENGTH(grade.name) ASC,CHAR_LENGTH(kumi.name) ASC')
            ->orderByRaw('grade.name ASC,kumi.name ASC')
            ->orderByRaw('role ASC,user_name ASC')
            ->get();

            //エクセル出力するデータがない場合、リダイレクト
            if ($users->count() == 0) {
                return redirect('/user')
                    ->withErrors('エクセル出力データがありません。検索で出力するデータを表示させてください。');
            }
            //リターン　エクセル出力
            $view = view('system.userinfoexcel',compact('users'));
            return Excel::download(new Export($view), '【管理】ユーザー一覧'.date('Y-m-d-H-i-s').'.xlsx');

        //(2)◆検索ボタン押下時　（ボタンの名前がsearch）
        //」pageを入れる理由⇒検索結果後、次ページに移動するとき「検索」ボタンを押さずにページ遷移、ここを通らせるため
        }elseif($request->has('search') || $request->has('page')  ) {
            $users = User::leftJoin('schools','users.school_int_id','=','schools.id')
                ->leftJoin('classes','users.class_id','classes.id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                ->whereIn( 'users.role', $selectRole )
                ->when($school_name !== null ,function($q) use($school_name){
                    return $q->where('schools.name','Like binary',"%$school_name%");
                })
                ->when($grade_name !== null ,function($q) use($grade_name){
                    return $q->where('grade.name','Like binary',"%$grade_name%");
                })
                ->when($kumi_name !== null ,function($q) use($kumi_name){
                    return $q->where('kumi.name','Like binary',"%$kumi_name%");
                })
                ->when($user_name !== null ,function($q) use($user_name){
                    return $q->where('users.name','Like binary',"%$user_name%");
                })
                ->when($attendance_no !== null ,function($q) use($attendance_no){
                    return $q->where('users.attendance_no',$attendance_no);
                })
                ->when($login_account !== null ,function($q) use($login_account){
                    return $q->where('users.login_account',$login_account);
                })
                ->select('schools.name as school_name','users.name as user_name','users.id as user_id','schools.school_id as sc_id',
                    'role','class_id','attendance_no','users.login_account','schools.id as school_id','users.trial_flg',
                    'kumi.name as kumi_name','grade.name as grade_name','grade.id as grade_id','kumi.id as kumi_id',
                    'schools.password_school_init','schools.password_teacher_init','schools.password_student_init',)
                    ->orderByRaw('schools.name ASC')
                    ->orderByRaw('CHAR_LENGTH(grade.name) ASC,CHAR_LENGTH(kumi.name) ASC')
                    ->orderByRaw('grade.name ASC,kumi.name ASC')
                    ->orderByRaw('role ASC,user_name ASC')
                    ->paginate($pagenation);


            return view('system.userinfo',compact('users','school_adm','teacher','student'
                ,'school_name','attendance_no','user_name','login_account','roleSelection',
                'grade_selection','kumi_selection','school_search_selection','selected','school_name_search',
                'grade_new_selection','grade_new_select','kumi_new_selection','kumi_new_select','grade_name','kumi_name','school_selected_flg'));

        //(3)◆セレクトボックス　学校を選んだら  （学校を選んでから編集新規作成ができる）
        }elseif($request->has('select')){
            $selected = true;
            //編集ボタンをアクティブにするフラグ⇒編集モード
            $school_selected_flg = 'yes';
            //学校ID
            $school_name_search = $request->input('school_name_search');
            //学年と組のセレクトボックスの値
            $grade_selection = Grade::selectListById($school_name_search);
            $kumi_selection = Kumi::selectListById($school_name_search);
            //dd($request->all());
            $users = User::leftJoin('schools','users.school_int_id','=','schools.id')
                ->leftJoin('classes','users.class_id','classes.id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                ->where('schools.id','=',$school_name_search)
                ->whereIn( 'users.role', $selectRole )
                ->when($grade_name !== null ,function($q) use($grade_name){
                    return $q->where('grade.name','Like binary',"%$grade_name%");
                })
                ->when($kumi_name !== null ,function($q) use($kumi_name){
                    return $q->where('kumi.name','Like binary',"%$kumi_name%");
                })
                ->when($user_name !== null ,function($q) use($user_name){
                    return $q->where('users.name','Like binary',"%$user_name%");
                })
                ->when($attendance_no !== null ,function($q) use($attendance_no){
                    return $q->where('users.attendance_no',$attendance_no);
                })
                ->when($login_account !== null ,function($q) use($login_account){
                    return $q->where('users.login_account',$login_account);
                })
                ->select('schools.name as school_name','users.name as user_name','users.id as user_id','schools.school_id as sc_id',
                    'role','class_id','attendance_no','users.login_account','schools.id as school_id','users.trial_flg',
                    'kumi.name as kumi_name','grade.name as grade_name','grade.id as grade_id','kumi.id as kumi_id',
                    'schools.password_school_init','schools.password_teacher_init','schools.password_student_init',)
                    ->orderByRaw('schools.name ASC')
                    ->orderByRaw('CHAR_LENGTH(grade.name) ASC,CHAR_LENGTH(kumi.name) ASC')
                    ->orderByRaw('grade.name ASC,kumi.name ASC')
                    ->orderByRaw('role ASC,user_name ASC')
                    ->paginate($pagenation);

            //dd($users);
            return view('system.userinfo',compact('users','school_adm','teacher','student'
                ,'school_name','attendance_no','user_name','login_account','roleSelection',
                'grade_selection','kumi_selection','school_search_selection','selected','school_name_search',
                'grade_new_selection','grade_new_select','kumi_new_selection','kumi_new_select','grade_name','kumi_name', 'school_selected_flg'));
        }
    }
    /**
     * ★エクセル一括登録
     *Route::match(['get', 'post']
     * @return view('system.userinfo')
     */
    public function importCsv(Content $content)
    {
        logger()->debug('<-- UserInfoController@importCsv');
        ini_set('memory_limit','1024M');
        set_time_limit(180);
        global $pagenation;
        //ロールのチェックボックス
        $school_adm = true;
        $teacher = true;
        $student = true;
        //検索ボックスの値
        $school_name = null;
        $attendance_no = null;
        $user_name = null;
        $login_account = null;
        //学校のセレクトボックスの選択値
        $school_search_selection = School::selectList();
        //学年、組のセレクトボックスの選択値
        $grade_selection = Grade::selectListId();
        $kumi_selection = Kumi::selectListId();
        //ロールのセレクトボックスの選択値
        $roleSelection = config('configrations.ROLE_LIST3');

        //フラグ　no⇒検索モード、yes⇒編集、新規作成モード cheakedit-user.js で処理
        $school_selected_flg= 'no';

        //学校を選ぶセレクトボックスと、選んだ学校のID
        $school_search_selection = School::selectListByName($school_name);
        $school_name_search = null;

        //bladeファイルの一番下にdevのhidden内で使用ファイル　⇒学校に応じた学年、組が表示される
        $grade_new_selection = Grade::selectListById($school_name_search);
        $kumi_new_selection = Kumi::selectListById($school_name_search);
        $grade_new_select = null;
        $kumi_new_select = null;

        $school_name_search = null;
        $grade_name = null;
        $kumi_name = null;
        $selected = false;

        // アップロードされたエクセルファイル
        $file = request()->file('file');
         logger()->debug($file);
        try {
            $import = new UserImport();
            $file->store('excels');
            Excel::import($import, $file);
        } catch (ValidationException $e) {
            logger()->debug('◆システム管理メニュー、ユーザー管理エクセル一括登録でバリデーションエラー.');
            logger()->debug($e->errors());
            session()->flash('flash_message', 'インポート（バリデーション）に失敗しました。');
            //バリデーションエラーの場合、リダイレクト
            return redirect('/user')
                ->withErrors($e->errors());
        } catch(Exception $e){
            logger()->debug('◆システム管理メニュー、ユーザー管理エクセル一括登録でエラー.');
            abort(500);
        }
        logger()->debug('import OK.');

        $users = User::leftJoin('schools','users.school_int_id','=','schools.id')
            ->leftJoin('classes','users.class_id','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('schools.name as school_name','users.name as user_name','users.id as user_id','schools.school_id as sc_id',
                'role','class_id','attendance_no','users.login_account','schools.id as school_id','users.trial_flg',
                'kumi.name as kumi_name','grade.name as grade_name','grade.id as grade_id','kumi.id as kumi_id',
                'schools.password_school_init','schools.password_teacher_init','schools.password_student_init',)
            ->where('users.role','!=','システム管理者')
            ->where(function ($query) use ($import) {
                foreach ($import->id_list() as $field){
                    $query->orWhere('login_account','=',$field);
                }
            })
            ->orderBy('users.updated_at','DESC')
            ->paginate($pagenation);
        //リターンview
        return view('system.userinfo',compact('users','school_adm','teacher','student'
            ,'school_name','attendance_no','user_name','login_account','roleSelection',
            'grade_selection','kumi_selection','school_search_selection','selected','school_name_search',
            'grade_new_selection','grade_new_select','kumi_new_selection','kumi_new_select','grade_name','kumi_name', 'school_selected_flg'));

    }
    /**
     * ★新規＆更新＆パスワードリセット　ボタンで切り分け
     * Route::match(['get', 'post'] ⇒多重送信対策は未実装
     * (1)新規作成後、保存ボタン（update）　(2)編集後の保存ボタン（save）
     * @return redirect('/user')
     */
    public function save(Request $request)
    {
        //dd($request->all());
        global $pagenation;
        logger()->debug('<-- UserInfoController@save');

        //出席番号を半角にする
        if (isset($request['attendance_no'])){
            $request['attendance_no'] = mb_convert_kana($request['attendance_no'], 'n');
            }

        //(1)新規ボタン押下時（ボタン名はupdate）
        if($request->has('update')){

            //学校情報
            $school = School::where('id',"=",$request->school_name_search)->first();

            //学年、組からクラスID取得
            $clas_id = Clas::where('school_id',"=",$request->school_name_search)
            ->where('grade_id',$request->grade)
            ->where('kumi_id',$request->kumi)
            ->select('id')->first();

            //バリデーション準備　　$validation_dataにクラスIDとユーザー名を入れる
            //クラスがないときはクラスIDにnullがはいり、必須のバリデーションにひっかかる
            if($clas_id == null){
                $validation_data['system_user_clas_id'] =null;
            } else {
                $validation_data['system_user_clas_id'] =  $clas_id->id;
            }
            $validation_data['system_user_name'] = $request->student_name;
            $validation_data['attendance_no'] = $request->attendance_no;
            //dd($request->all());

            //学校に登録できるユーザー最大数
            $max_user_num = $school['user_number'];
            //学校管理＆先生＆生徒を含むユーザー数、ナレッジ用隠し学校アカウント（b33259)がある時は、除外する
            $user_num =  User::where('school_int_id','=',$school->id)
                ->where('login_account','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN'))
                ->count();
            //dd($user_num);
            // 最大登録数をオーバーしたらバリデーション（requied )チェックするための準備（学校管理1名、先生1名は含まない）       
            if($user_num < $max_user_num +2){
                $validation_data['over_max_student_num'] ="OK"; //バリデーションクリア
            } else {
                $validation_data['over_max_student_num'] = null; //バリデーションにひっかかる
            }
            //dd($school['user_number']);

            //バリデーション実行　ユーザー名の文字数は　addnew-users.js にmaxlength20を設定してある
            $validator = Validator::make($validation_data, [
                'system_user_clas_id' => ['required'],
                'system_user_name' => ['required','max:20'],
                'attendance_no' => ['required','digits_between:0,4'],
                'over_max_student_num' => ['required'],
            ]);

            //バリデーション失敗時　バリデーションメッセージはvalidation.php で指定
            if ($validator->fails()) {
                return redirect('/user')
                    ->withErrors($validator);
            }

            //$newUserに新規データをセットして保存
            $newUser = new User();
            $newUser->name = $request->student_name; // 名前
            $newUser->role = $request->role; // ロール
            $newUser->class_id =  $clas_id['id']; // クラスID
            $newUser->attendance_no = $request['attendance_no'];
            $newUser->school_int_id = $school->id;; // 学校ID(数値)
            $newUser->school_id = $school->school_id; // 学校ID

            //ロール（生徒、先生、学校管理者）により、初期パスワードをセット　※システム管理者はリセット不可
            if($newUser->role==config('configrations.ROLE_LIST.2')){
                $newUser->password = bcrypt($school->password_school_init); // 学校管理パスワード
            } elseif($newUser->role==config('configrations.ROLE_LIST.3')){
                $newUser->password = bcrypt($school->password_teacher_init); // 先生パスワード
            } elseif($newUser->role==config('configrations.ROLE_LIST.4')) {
                $newUser->password = $school->password_student_init; // 生徒パスワード初期値
            }

            try{
                DB::beginTransaction();
                //ログインIDをランダムに発行（重複しないまで繰り返す）
                //ログインIDを取得　※10101 ~ 99999 の内
                while(true){
                    $id_no = str_pad(mt_rand(10101, 99999), 5, 0, STR_PAD_LEFT);
                    $n_no = User::where('login_account','Like',"%$id_no")
                        ->where('school_id',auth()->user()->school_id)
                        ->first();
                    if($n_no === null){
                        break;
                    }
                }
                //dd(config('configrations.ROLE_LIST.2'));
                //学校管理者には「a]、先生には「t」の接頭辞をつける
                if($newUser->role==config('configrations.ROLE_LIST.2')){
                    $newUser->login_account = 'a'.$id_no;
                } elseif($newUser->role==config('configrations.ROLE_LIST.3')){
                    $newUser->login_account = 't'.$id_no;
                } else {
                    $newUser->login_account = $id_no; //生徒
                }
                
                //新規ユーザー情報をセットして保存
                $newUser->save();
                session()->flash('flash_message', $newUser->name.'さんを新規登録しました。');
                DB::commit();
            }catch(Exception $e){
                logger()->error('◆システム管理メニュー、ユーザーの新規作成でエラー');
                logger()->error($e);
                DB::rollback();
                abort(500);
            }
        }

        //(2)更新　⇒編集後の「保存」ボタン押下 ※save
        elseif($request->has('save')){
            //dd($request->all());
            //学年、組からクラスID取得　$request->school_name_search　⇒選んだ学校のID
            $clas_id = Clas::where('school_id',"=",$request->select_school_id)
            ->where('grade_id',"=",$request->grade_id)
            ->where('kumi_id',"=",$request->kumi_id)
            ->select('id')->first();

            //バリデーションの準備　クラスIDがないとnullにして、バリデーションにかける
            if($clas_id == null){
                $validation_data['system_user_clas_id'] =null;
            } else {
                $validation_data['system_user_clas_id'] =  $clas_id->id;
            }
            $validation_data['system_user_name'] = $request->user_name;
            $validation_data['attendance_no'] = $request->attendance_no;

            //学校情報
            $school = School::where('id',"=",$request->select_school_id)
            ->select('id','school_id','password_student_init','password_teacher_init','password_school_init')->first();

            //バリデーション
            $validator = Validator::make( $validation_data, [
                'system_user_clas_id' => ['required'],//学年と組が存在しないクラスの場合、nullなのでエラー
                'system_user_name' => ['required','max:20'],
                'attendance_no' => ['required','digits_between:0,4'],
            ]);

            //バリデーション失敗時
            if ($validator->fails()) {
                return redirect('/user')
                    ->withErrors($validator);
            }
            try{
                DB::beginTransaction();
                    //ユーザー情報をセットして保存（学年と組はクラスIDにして保存）
                    $user=User::find($request->save_user_id);
                    $user
                    ->fill([
                        'class_id' => $clas_id->id,
                        //'role' => $request->role,
                        'name' => $request->user_name,
                        'attendance_no' => $request->attendance_no,
                        ])
                 ->save();
                    session()->flash('flash_message', $user->name.'さんの情報を更新しました');
                DB::commit();
            }catch(Exception $e){
                logger()->error('◆システム管理メニュー、ユーザーの更新でエラー');
                logger()->error($e);
                DB::rollback();
                abort(500);
            }
        }
        //(3)◆パスワードリセットボタン押下時　⇒他ページはリセットは別formだが、ユーザーID取得のため
        elseif($request->has('modal_password_reset_btn')){

            //dd($request->all());
            $school = School::where('id',"=",$request -> select_school_id)
            ->select('id','school_id','password_student_init','password_teacher_init','password_school_init')->first();
            $user=User::find($request->save_user_id);

            //パスワード初期値、生徒はそのまま、先生＆学校管理者はパスワードをハッシュ化
            //ロール（生徒、先生、学校管理者）により、初期パスワードをセット ※システム管理はリセット不可

            if($user->role == config('configrations.ROLE_LIST.4')){
                $init_password = $school -> password_student_init; //生徒
            } elseif ($user -> role == config('configrations.ROLE_LIST.3')){
                $init_password = bcrypt($school -> password_teacher_init);//先生
            } elseif($user -> role == config('configrations.ROLE_LIST.2')){
                $init_password = bcrypt($school -> password_school_init);//学校管理
            }

            //dd( $init_password);
            //パスワードを初期値にセットして保存
            try{
                $user->password = $init_password;
                $user ->save();
            session()->flash('flash_message', 'パスワードをリセットしました');
            }catch(Exception $e){
                logger()->error('システム管理メニュー、ユーザーのパスワードリセットでエラー');
                logger()->error($e);
                abort(500);
            }
        //ここまで、ボタン別の処理
        }

        //ここからは、共通⇒リターンviewに渡すデータ
        //検索　ロールのチェックボックス
        $school_adm = true;
        $teacher = true;
        $student = true;

        //検索ボックス
        $school_name = null;
        $attendance_no = null;
        $grade_name = null;
        $kumi_name = null;
        $user_name = null;
        $login_account = null;

        //学校のセレクトボックス
        $school_search_selection = School::selectList();
        //選んだ学校のID
        $school_name_search = null;

        //一覧表の中の「学年」「組」のセレクトボックス（modelで関数を作っている）編集時にアクティブ
        $grade_selection = Grade::selectListId();
        $kumi_selection = Kumi::selectListId();
        //dd($grade_selection);

        //一覧表の中の「ロール」セレクトボックス、configrationsから値を取得
        $roleSelection = config('configrations.ROLE_LIST3');

        //フラグ　学校選択ボタン、新規学校作成ボタンどちらかを表示させるかのフラグ
        $selected = false;
        //フラグ　学校選択ボタンを押すと、ユーザー一覧の中にある編集ボタンがアクティブになる
        $school_selected_flg= 'no';

        //bladeページ一番下のdivのhiddenで使用、選んだ学校に対応した学年と組のセレクトボックスの値と名前
        $grade_new_selection = Grade::selectListId();
        $kumi_new_selection = Kumi::selectListId();
        $grade_new_select = null;
        $kumi_new_select = null;

        $users = User::leftJoin('schools','users.school_int_id','=','schools.id')
            ->leftJoin('classes','users.class_id','classes.id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->select('schools.name as school_name','users.name as user_name','users.id as user_id','schools.school_id as sc_id',
                'role','class_id','attendance_no','users.login_account','schools.id as school_id','users.trial_flg',
                'kumi.name as kumi_name','grade.name as grade_name','grade.id as grade_id','kumi.id as kumi_id',
                'schools.password_school_init','schools.password_teacher_init','schools.password_student_init',)
            ->where('users.role','!=','システム管理者')
            ->orderByRaw('users.updated_at DESC')
            ->paginate($pagenation);

            return view('system.userinfo',compact('users','school_adm','teacher','student'
                ,'school_name','attendance_no','user_name','login_account','roleSelection',
                'grade_selection','kumi_selection','school_search_selection','selected','school_name_search',
                'grade_new_selection','grade_new_select','kumi_new_selection','kumi_new_select','grade_name','kumi_name','school_selected_flg'));

            }
    /**
     * ★エクセル一括登録用のテンプレートダウンロード
     * Route::get
     * 開発環境　\storage\app\exceltemplate\system_user_exceltemplate.xlsx'　　ルーティング 学校ID/system_user_exceltemplate.xlsx'
     * @return redirect('/user')
     */
    public function download()
    {
        logger()->debug('<-- UserInfoController@download');

        $filePath = 'private/exceltemplate/【管理】ユーザー一括登録用テンプレート.xlsx';
        $fileName = '【管理】ユーザー一括登録用テンプレート.xlsx';
        $mimeType = Storage::mimeType($filePath);
        $headers = [['Content-Type' => $mimeType]];

        return Storage::download($filePath, $fileName, $headers);
    }
    /**
     * ★削除
     * Route::post
     * 【重要】ユーザーを削除すると、紐づくscore_historyテーブル、corese_with_bestpointテーブルのデータも削除される
     * 上記関連テーブルの削除はカスケードデリートで設定する（検証OK）
     * @return redirect('/user')
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- UserInfoController@delete');
        $checks = $request->checks;
        if($checks !== null ){
            DB::beginTransaction();
            foreach($checks as $check){
                try{
                    $selected = true;
                    User::destroy(intval($check));
                }catch(\Exception $e){
                        DB::rollBack();
                        logger()->error('◆システム管理メニュー、ユーザー削除でエラー');
                        logger()->error($e);
                        abort(500);
                }
            }
            DB::commit();
        }
        return redirect('/user');
    }
}
