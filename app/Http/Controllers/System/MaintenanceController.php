<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Information;
use DateTime;


    /**
     * システム管理メニュー　メンテナンス情報管理　
     * ※先生メニュー、学校管理メニュー、システム管理メニューのメインメニューに表示されるサーバーメンテナンス情報など
     * (1)メンテナンス情報一覧表示
     * (2)検索
     * (3)編集画面へ遷移
     * (4)新規＆更新保存
     */
$pagenation = 200;
class MaintenanceController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- MaintenanceController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★メンテナンス情報一覧
     * Route::get  ※ページネーションあり
     * @return view('system.maintenance')
     */
    public function index()
    {
        logger()->debug('<-- MaintenanceController@index');
        global $pagenation;

        //検索ボックス初期値
        $article = null;
        $startDate = null;
        $endDate = null;

        //メンテナンス情報データ
        $information = Information::orderby('created_at','DESC')
            ->paginate($pagenation);
        //リターンview
        return view('system.maintenance',compact('information','article','startDate','endDate'));
    }
    /**
     * ★検索
     * Route::match(['get', 'post']　※ページネーションあり
     * @return view('system.maintenance')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- MaintenanceController@search');
        global $pagenation;

        //検索値を変数にセット
        $article = $request->input('article');
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        //終了日を+１する理由⇒SQLで「　>$endDate」 にするとその日が含まれない（1/5を設定すると1/4まで対象、1/5を含ませるために+1）
        $endDateTmp = new DateTime($request->input('end_date'));
        $endDateTmp->modify('+1 day');

        //検索後のメンテナンス情報データ        
        $information = Information::where('title','Like',"%$article%")
            ->when($startDate != '' ,function($q) use($startDate){
                return $q->where('information.created_at','>',$startDate);
            })
            ->when($endDateTmp != '' ,function($q) use($endDateTmp){
                return $q->where('information.created_at','<',$endDateTmp);
            })
            ->orderby('created_at','DESC')
            ->paginate($pagenation);

        //リターンview
        return view('system.maintenance',compact('information','article','startDate','endDate'));
    }   
    /**
     * ★編集ぺージへ遷移　ボタンで分岐(1)新規ボタン　(2)編集ボタン
     * Route::post  ページネーションなし
     * @return view('system.maintenance')
     */
    public function add(Request $request)
    {
        logger()->debug('<-- MaintenanceController@add');        

        //新規ボタン押下
        if($request -> has('add')){
            $information = new Information();

            //「重要」カラムは「boolean」でなく「一般」「重要」のどちらか。初期値は「一般」
            $importance_yes = null;
            $importance_no = '一般';

        //編集ボタン押下
        } else if($request -> has('edit')){
            $information = Information::find($request -> input('id'));
           
            if($information -> importance == '重要'){
              $importance_yes = '重要';
              $importance_no = null;
            } else {
              $importance_yes = null;
              $importance_no = '一般';
            }
        //それ以外エラー
        } else {        
            logger()->error('◆システム管理メニュー、メンテナンス情報の　ボタン押下でないエラー');
            abort(500);
        }

        //有効、無効情報をtoggleボタンにもたせる
        if($information -> invalid == "1"){
            $invalid_yes = 1;
            $invalid_no = 0;
          } else {
            $invalid_yes = 0;
            $invalid_no = 1;
          }
        //リターンview
        return view('system.maintenanceedit',compact('information','importance_yes','importance_no','invalid_yes','invalid_no'));
    }
    /**
     * ★新規＆更新保存 ※削除機能はない（各学校に配信したのが突然見えなくなるのはおかしいので）「無効」をつかう　
     * 更新と新規保存はボタンで分岐
     * 「title」カラムが記事本文
     * Route::post
     * @return view('system.maintenance'')
     */
    public function new(Request $request)
    {
        logger()->debug('<-- MaintenanceController@deliUpdate');       

        //dd($request->all());
        //新規の場合
        if ($request -> id  === null){
            
            $information = new Information();
            $flashmessage = '【新規登録】保存しました。登録画面の「公開ボタン」で、公開されます。';
        } else {
        //更新の場合
            $information = Information::find($request->input('id'));
            $flashmessage = '【更新】記事ID:'.$request -> id.'　更新しました。※作成日「昇順」⇒一番上に表示されません';
        }
        //本文（titleカラム）が空欄だと、保存エラーでabort(500)に飛ばされるため、ここでバリデーション
        if($request->input('title') === null){
            return redirect('/maintenance-info')
                ->withErrors('空欄です。かならず本文を入力してください。');
        }

        //ここから共通処理、データをセット
        //toggleボタン　DBのimportanceカラムの値は「重要」「一般」の2つのみ。toggleボタン用に変換
        $information->importance = $request->input('importance');
        $information->invalid = $request->input('invalid');
        $information->title = $request->input('title');
       
        //保存
        try{
            $information -> save();
            session()->flash('flash_message', $flashmessage);

        }catch(\Exception $e){
            logger()->error('◆システム管理メニュー、メンテナンス情報の新規or更新でエラー');
            logger()->error($e);
            abort(500);
        }
/** リターンviewに渡すと多重送信のため、リダイレクトに変更
        //リターンviewに渡す変数　重要か一般か
        if($request->input('importance') == '重要'){
            $importance_yes = '重要';
            $importance_no = null;
        } else {
            $importance_no = '一般';
            $importance_yes = null;
        }
        //無効か有効か？
        if($request->input('invalid') == '1'){
            $invalid_yes = '1';
            $invalid_no = '0';
        } else {
            $invalid_yes = '0';
            $invalid_no = '1';
        }
        return view('system.maintenancenew',compact('information','importance_yes','importance_no','invalid_yes','invalid_no'));

 */
        return redirect('/maintenance-info');
    }
}
