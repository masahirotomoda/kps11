<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\User;
use Exception;


    /**
    * システム管理メニュー　＞　バレッド専用　ICT検定管理
    *(1)バレッド生徒一覧
    *(2)学校名、生徒名検索検索
    *(3)試験選択（生徒のタイピング画面に表示・非表示）
    */
$pagenation = 200;
class VldtestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }

    /**
     * ★バレッド生徒一覧
     * Route::get
     * @return  view('system.bldtest')
     */
    public function index()
    {
        //検索ボックスの初期値
        $school_name = null;
        $user_name = null;

        //バレッド生徒一覧
        $users = User::leftJoin('schools','users.school_int_id','schools.id')
            ->select('users.name','users.id','schools.name as school_name','schools.school_id','users.vldtest')
            ->where('users.role','生徒')
            ->where('schools.logo',1)
            ->orderBy('schools.id','ASC')
            ->orderBy('users.attendance_no','ASC')
            ->get();
        //ページネーションなしなので、カウント数
        $user_count=$users->count();

        //試験コースのドロップダウンリスト 決め打ち⇒試験リストは　searchメソッドでも使用
       //試験コースのドロップダウンリスト
       $test_courses= Course::where('invalid', 0) 
       ->where('tab','vldtest')
       ->orderBy('display_order','ASC')
       ->get();

       $test_list = $test_courses->pluck('course_name','id')->toArray();

        //リターンview
        return view('system.vldtest',compact('users','test_list','school_name','user_name','user_count'));
    }

     /**
     * ★検索（学校名、生徒名）
     * Route::get post
     * @return  view('system.bldtest')
     */
    public function search(Request $request)
    {
        //検索ボックスの初期値
        $school_name = $request->schoolname;
        $user_name = $request->username;

        //検索後のバレッド生徒データ
        $users = User::leftJoin('schools','users.school_int_id','schools.id')
            ->select('users.name','users.id','schools.name as school_name','schools.school_id','users.vldtest')
            ->where('users.role','生徒')
            ->where('schools.logo',1)            
            ->when($school_name != '' || $school_name != null ,function($q) use($school_name){
                return $q->where('schools.name','Like binary','%'.$school_name.'%');
            })
            ->when($user_name != '' || $user_name != null ,function($q) use($user_name){
                return $q->where('users.name','Like binary','%'.$user_name.'%');
            })        
            ->orderBy('schools.id','ASC')
            ->orderBy('users.attendance_no','ASC')
            ->get();

        //ページネーションなしなので、カウント数
        $user_count=$users->count();
        
        //バレッドICT検定タイピングコース、決め打ち
        //$test_id=[850,851,852,853,854,855,856,857,858,859,860,861,862,863,864,865,866,867,868,869,870,871,872,873,874,875,876,877,878,879,880,891,892,893];
        $test_id=[1,2,3,4,5];

        $test_list = DB::table('courses')
        ->whereIn('id', $test_id)
        ->pluck('course_name', 'id')
        ->toArray();

        //リターンview
        return view('system.vldtest',compact('users','test_list','school_name','user_name','user_count'));
    }

    /**
     * ★検定コースを選んで生徒画面での検定コース表示・非表示設定
     * Route::post
     * @return redirect('/vldtest')
     */
    public function update(Request $request)
    {
        //選んだ生徒データ
        $user = User::find($request->userid);
        //選んだ検定コースのID（コースID）
        $user->vldtest = $request->selected_test;
        //保存
        try{
            $user->save();
        } catch (Exception $e) {
            logger()->error('システム管理、バレッド専用タイピング検定、試験選択エラー');
            logger()->error($e);
            abort(500);
        }
        //リセット成功時、flashメッセージ
        session()->flash('flash_message', $user->name. 'さんのタイピング検定コース表示・非表示設定を更新しました');
        //リダイレクト
        return redirect('/vldtest');        
    }
}