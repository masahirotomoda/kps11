<?php

namespace App\Http\Controllers\System;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\Clas;
use Illuminate\Validation\ValidationException;

$id_list;

class UserImport implements ToCollection, WithStartRow,WithValidation
{
    public static $startRow = 2;                // csvの1行目にヘッダがあるので2行目から取り込む

    public function collection(Collection $rows)
    {
        logger()->debug('<-- UserImport@collection');
        global $id_list;
        $hasError = false;
        $validator = Validator::make($rows->toArray(), [
        ]);
        //dd($rows->toArray());
        $index = 2;
        DB::beginTransaction();
        foreach ($rows as $row){

            logger()->debug($row);
            $school = School::where('name',$row[0])->first();
            if($school === null){
                logger()->alert($index.'::Import Error with school name not Found. : '.$row[0]);
                $hasError = true;
                $validator->errors()->add('error',$index.'行目に該当する「学校名('.$row[0].')」がありません');
                $index ++;
                continue;
            }
            //学年
            if($row[3] !== null || $row[3] === ''){
                $grade = Grade::where('school_id',$school->id)->where('name',$row[3])->first();
                if($grade === null){
                    //ロールが生徒以外だったらバリデーション無視（無いクラスでも許容）ただしNullにする
                    if($row[1] == !config('configrations.ROLE_LIST')[4]){
                        $hasError = true;
                        $validator->errors()->add('error',$index.'行目に該当する「学年('.$row[3].')」がありません');
                    }
                    $grade_id = null;
                }else{
                    $grade_id = $grade->id;
                }
            }else{
                $grade_id = null;
            }
            //組
            if($row[4] !== null || $row[4] === ''){
                $kumi = Kumi::where('school_id',$school->id)->where('name',$row[4])->first();
                if($kumi === null){
                    //ロールが生徒以外だったらバリデーション無視（無いクラスでも許容）ただしNullにする
                    if($row[1] == !config('configrations.ROLE_LIST')[4]){
                        $hasError = true;
                        $validator->errors()->add('error',$index.'行目に該当する「組('.$row[4].')」がありません');
                    }
                    $kumi_id = null;
                }else{
                    $kumi_id = $kumi->id;
                }
            }else{
                $kumi_id = null;
            }

            //クラス
            $class = Clas::where('school_id',$school->id)
                ->where('grade_id',$grade_id)
                ->where('kumi_id',$kumi_id)
                ->first();
            if($class === null){
                $hasError = true;
                $validator->errors()->add('error',$index.'行目に該当する「クラス」がありません');
                $class_id = null;
            }else{
                $class_id = $class->id;
            }


            while(true){
                $id_no = str_pad(mt_rand(10101, 99999), 5, 0, STR_PAD_LEFT);
                $n_no = User::where('login_account','Like',"%$id_no")
                    ->where('school_id',$school->id)
                    ->first();
                if($n_no === null){
                    break;
                }
            }

            //パスワードとID
            if($row[1] === config('configrations.ROLE_LIST')[4]){
                $login_account = $id_no;
                $password = $school->password_student_init;
            }elseif($row[1] === config('configrations.ROLE_LIST')[3]){
                $login_account = 't'.$id_no;
                $password = bcrypt($school->password_teacher_init);
            }else{
                $login_account = 'a'.$id_no;
                $password = bcrypt($school->password_school_init);
            }
            logger()->debug('user creating.');
            User::create([
                'school_id' => $school->school_id,
                'school_int_id' => $school->id,
                'role' => $row[1],
                'name' => $row[2],
                'login_account' => $login_account,
                'password' => $password,
                'class_id' => $class_id,
                'attendance_no'=> $row[5]
            ]);
            $id_list[$index] = $login_account;
            logger()->debug('user created. ' . $row[2] . ' ' . 'index = ' .$index . ':' .$id_list[$index] );
            $index ++;
        }
        if($hasError === true){
            DB::rollback();
            throw new ValidationException($validator);
        }else{
            DB::commit();
        }

    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return self::$startRow;
    }

    public function rules(): array{
        return [
                '*.0' => "required|string",         // 学校名
              //  '*.1' => 'in:' . implode(',', config('configrations.ROLE_LIST')),   // 権限種別
              '*.1' =>  "nullable",   // 権限種別
              '*.2' => "required|string|max:50",          // 名前
                '*.3' => "nullable|max:20",         // 学年
                '*.4' => "nullable|max:20",         // 組
                '*.5' => "nullable|numeric|between:0,9999",           // 出席番号
            ];
    }
   public function customValidationAttributes()
   {
       return [ '0' => '「学校名」',
                '1' => '「権限種別」',
                '2' => '「名前」',
                '3' => '「学年」',
                '4' => '「組」',
                '5' => '「出席番号」',
    ];
   }
   public function id_list()
   {
       global $id_list;
       return $id_list;
   }
}
