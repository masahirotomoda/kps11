<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use app\Models\User;
use App\Models\School;
use \Exception;

/**
* システム管理メニュー　＞　システム管理アカウント情報　 ※ページネーションなし
* (1)システム管理アカウント情報
* (2)名前更新
* (3)パスワード変更     
 */
class ProfileController extends Controller
{    
    public function __construct()
    {
        logger()->debug('<-- ProfileController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
    }

    /**
     * ★システム管理者と学校情報
     * @return view('system.profile')
     */
    public function index()
    {
        logger()->debug('<-- ProfileController@index');
       
        //システム管理者情報
        $user = User::where('id','=',auth()->id())->first();
        //学校データ ※必要
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('name')->first();
        //リターンview
        return view('system.profile',compact('user','school'));
    }
    /**
     * ★更新(名前だけ)
     * @return view('system.profile')
     */
    public function update(Request $request)
    {
        logger()->debug('<-- ProfileController@update name = '.$request->input('name').' id = '.$request->input('id'));
        //システム管理者
        $user = User::where('id','=',auth()->id())->select('id','name')->first();

        //バリデーション
        $request->validate([
            'name' => 'required|string|max:20',
        ]);

        //名前データを$userにセット
        $user -> name = $request -> input('name');
        
        try{
            //システム管理者名の更新後、flashメッセージ
            $user->save();
            session()->flash('flash_message', 'システム管理者名を更新しました');

        }catch(Exception $e) {
            logger()->error('◆システム管理メニュー、システム管理者の名前更新でエラー');
            logger()->error($e);
            abort(500);
        }          
        //リダイレクト
        return redirect('/profile');
    }

    /**
     * ★パスワード変更
     *　パスワードルール：学校管理者と同じ「10～20文字。アルファベット（大文字・小文字）、数字、記号（!#$%& ）の内、3種類以上」
     * @return view('system.profile')
     */
    public function changePassword(Request $request)
    {
        logger()->debug('<-- ProfileController@passwordChange name = '.$request->input('name').' id = '.$request->input('id'));
        
        //システム管理者情報
        $user = User::where('id','=',auth()->id())->first();
        //学校データ（ヘッダーで使用）
        $school = School::where('schools.id','=',auth()->user()->school_int_id)->select('name')->first();

        //パスワードのバリデーション学校管理者と同じパスワードルール）
        //学校管理と同じバリデーションエラーメッセージにするため
        $request['change_password_school'] = $request['change_password'];
        //名前に「_confirmation」とつけるとパスワード相違チェックのバリデーションができる
        $request['change_password_school_confirmation'] = $request['change_password_confirmation'];
        //dd($request->all());
        $request->validate([
                'change_password_school' => [
                    'confirmed', //確認パスワードとの相違をチェック
                    'required',                   
                    'regex:<^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[!@;:#$%&])|(?=.*[A-Z])(?=.*[0-9])(?=.*[!@;:#$%&])|(?=.*[a-z])(?=.*[0-9])(?=.*[!@;:#$%&]))([a-zA-Z0-9!@;:])>', 
                    'between:10,20',   
                ]
        ]);
        try{
            //パスワードの更新（ハッシュ化して保存） 
            $user['password'] = bcrypt($request['change_password_school']);
            $user->save();
                session()->flash('flash_message', 'パスワードを変更しました');
        }catch(Exception $e) {
            logger()->error('◆システム管理メニュー、システム管理者パスワード更新エラー.');
            logger()->error($e);
            abort(500);
        } 
        //リダイレクト
        return redirect('/profile');
    }
}
