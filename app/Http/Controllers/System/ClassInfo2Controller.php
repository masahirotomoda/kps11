<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Clas;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\School;
use App\Models\User;
use Exception;
    /**
     * システム管理メニュー　＞　クラス設定(2) 学校別  ※ページネーションなし
     * (1)学校選択
     * (2)クラス一覧表示
     * (3)検索
     * (4)新規＆更新
     * (5)クラス別生徒一覧
     * (6)削除
     */
class ClassInfo2Controller extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- system/ClassInfo2Controller@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
    }
    /**
     * ★学校を選ぶ（セレクトボックスから選択）
     * Route::get
     * @return view('system.classinfo2selectschool')
     */
    public function schoolSelect(Request $request)
    {
        logger()->debug('<-- system/ClassInfo2Controller@schoolSelect');

        //学校のセレクトボックス値（メソッドはモデルで定義）
        $school_search_selection = School::selectList();
        natcasesort($school_search_selection); //より自然にソート昇順　第1５小学校等、数字と文字が混在している時に希望の挙動
        $selected_school_int_id = null; //初期値
        return view('system.selectschool',compact('school_search_selection','selected_school_int_id'));
    }
 
    /**
     * ★クラス一覧　※学校選択から遷移する。学年と組をの組み合わせがクラス、生徒はクラスに紐づいている
     * 【重要】学校IDを持ちまわるためリダイレクトできない。その代わりにこのindexメソッドを使う
     * Route::post　※ページネーションなし
     * @return view('system.classinfo2')
     */
    //学校選択から学校IDを引き継ぐため、引数が$request
    public function index(Request $request)
    {
        logger()->debug('<-- system/ClassInfo2Controller@index');
        //dd($request->all());
        //検索ボックス初期値
        $s_grade = null;  //学年
        $s_kumi = null;  //組

        //選んだ学校のID
        //前ページの学校選択なら「$request->select_school_id」、その他(hiddenで持つ）「$request->hidden_school_id」
        if($request->hidden_school_id){
            $selected_school_int_id = $request->hidden_school_id;
        } else {
        $selected_school_int_id = $request->select_school_id;
        }
        //学校情報（ページ上部に表示）
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;
        
        //「学年」「組」のセレクトボックス値（selectListById関数は、modelで定義）
        $grade_selection = Grade::selectListById($selected_school_int_id);
        $kumi_selection = Kumi::selectListById($selected_school_int_id); 

        //ユーザデータ
        $users = User::where('users.school_int_id','=',$selected_school_int_id)
            ->select('users.class_id')
            ->get();
        
        //クラスデータ
        $classes = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
            ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
            ->leftjoin('schools','classes.school_id','=','schools.id')
            ->select('classes.id as class_id','schools.id as school_int_id','schools.name as school_name','schools.school_id as sc_id',
                'kumi.id as kumi_id','grade.id as grade_id','kumi.name as kumi_name','grade.name as grade_name','classes.*')            
            ->where('schools.id','=',$selected_school_int_id)
            ->orderByRaw('CHAR_LENGTH(grade.name) ASC,grade.name ASC,CHAR_LENGTH(kumi.name) ASC,kumi.name ASC') 
            ->get();

            //dd($classes);
        
        //クラスの数（ページネーションがないため、表示数を手動で設定）
        $current_class_num=$classes->count();
        $class_num=$classes->count();

        //リターンview
        return view('system.classinfo2',compact('users','classes','school','school_name','s_grade','s_kumi','grade_selection','kumi_selection','current_class_num','class_num'));
    }

    /**
     * ★検索＆すべて表示ボタン　※ボタンで切り分け
     * Route::post　※ページネーションなし
     * @return view('system.classinfo2')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- system/ClassInfo2Controller@search');  

        //dd($request->all());
        //成功メッセージをださないため
        session()->flash('flash_message', null);

        //選んだ学校のID(前ページのセレクトボックスから選択)と学校データ
        $selected_school_int_id = $request->select_school_id;
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //「学年」「組」のセレクトボックス値（selectListById関数は、modelで定義）
        $grade_selection = Grade::selectListById($selected_school_int_id);
        $kumi_selection = Kumi::selectListById($selected_school_int_id);
        
        //ユーザデータ
        $users = User::where('users.school_int_id','=',$selected_school_int_id)
            ->select('users.class_id')
            ->get();
        
        //【ここから分岐】検索ボタン押下　
        if($request -> has('search')){
            //検索値を変数に代入
            $s_grade = $request->input('s_grade');
            $s_kumi = $request->input('s_kumi');

            //検索後のクラスデータ
            $classes = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
            ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
            ->leftjoin('schools','classes.school_id','=','schools.id')
            ->where('schools.id','=',$selected_school_int_id)
            ->select('classes.id as class_id','schools.id as school_int_id','schools.name as school_name','schools.school_id as sc_id',
                'kumi.id as kumi_id','grade.id as grade_id','kumi.name as kumi_name','grade.name as grade_name','classes.*')
            ->when($s_grade !== null ,function($q) use($s_grade){
                return $q->where('classes.grade_id','=',$s_grade);
            })
            ->when($s_kumi !== null ,function($q) use($s_kumi){
                return $q->where('classes.kumi_id','=',$s_kumi);
            })
            ->orderByRaw('CHAR_LENGTH(grade.name) ASC,grade.name ASC,CHAR_LENGTH(kumi.name) ASC,kumi.name ASC') 
            ->get();
            
            //クラス総数
            $class_num = Clas::where('school_id','=',$selected_school_int_id)->count();
            //検索後のクラス数
            $current_class_num = $classes->count();
            //リターンview
            return view('system.classinfo2',compact('classes','school','school_name','s_grade','s_kumi','grade_selection','kumi_selection','class_num','current_class_num','users'));

        //すべて表示ボタン押下（学校IDを持ちまわるため、リダイレクトできない）
        } elseif($request -> has('all_btn')){
            //リダイレクトの代わりにindexメソッド
            return $this->index($request);
        }
    }
    /**
     * ★新規＆更新　ボタンで分岐
     * Route::get 
     * @return view('system.classinfo2')
     */
    public function save(Request $request)
    {
        logger()->debug('<-- system/ClassInfo2Controller@save');
        //dd($request->all());
        
        if($request->grade_id == null || $request->kumi_id == null){
            return $this->index($request)
                //「withErrors」でエラーをはいているが、バリデーションモーダルは問題なくでる                
                    ->withErrors(['message' => '学年、又はクラスは必須です。']);
        }
        //【ここから分岐】編集の保存ボタン押下
        if($request->has('save')){
            //学校のIDをhiddenで取得
            $selected_school_int_id = $request->hidden_school_id;

            //クラスIDをhiddenで取得して、クラスデータを編集中のクラスデータを取得           
             $clas = Clas::find($request->id);
              
        //新規登録の「保存」ボタン押下
        }elseif($request->has('update')){ 
            //選んだ学校のID(前ページのセレクトボックスから選択)と学校データ
            $selected_school_int_id = $request->hidden_school_id;
            //クラスのインスタンスを作る
            $clas = new Clas();            
        } else {
            logger()->error('◆システム管理メニュー、クラスの新規or更新で保存ボタンを押下しない時のエラー');
            abort(500);
        } 
         //【ここから共通処理】学年と組はセレクトボックス利用、しかし、何も選ばないとnullが入るためバリデーション
         //クラスデータをセット 
         $clas->grade_id = $request->input('grade_id');
         $clas->kumi_id = $request->input('kumi_id');
         $clas->school_id = $selected_school_int_id;
   
        //保存
        try{
            logger()->debug($clas);
            $clas->save();
            session()->flash('flash_message', 'クラスを追加or更新しました');
        }catch(Exception $e){
            //dd($e->errorInfo);
            if($e->errorInfo[1] === 1062 ){
                //dd($e->errorInfo);
                //学校、学年、組が同じものを作ろうとしている（Unique規制違反)
                logger()->info('◆システム管理メニュー、クラスの新規作成で重複エラー');
                logger()->debug($e);

                //成功メッセージをださないため
                session()->flash('flash_message', null);
                //リダイレクトの代わりにindexメソッド
                return $this->index($request)
                //「withErrors」でエラーをはいているが、バリデーションモーダルは問題なくでる
                
                    ->withErrors(['message' => '同一な学年、組のクラスは作成できません']);

            }else{
                //学校、学年、組が同じものを作ろうとしている（Unique規制違反)以外はエラーページへ
                logger()->error('◆システム管理メニュー、学校別クラス新規or更新で、ボタン押下以外でエラー');
                logger()->error($e);
                abort(500);
            }
        } //ここまでキャッチ
        //リダイレクトの代わりにindexメソッド
        return $this->index($request);
    }
    /**
     * ★クラス別所属生徒一覧 
     * @return view('system.classinfo2')
     * Route::post　
     */
    public function studentinfo(Request $request)
    {
        $class_id = $request->class_id;
        //生徒情報  （生徒一覧）    
        $users = User::where('users.role','=','生徒')
        ->where('users.class_id','=',$class_id)
        ->orderByRaw('attendance_no ASC')
        ->get();        

        //クラス情報（上の表で使用）
        $class = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
        ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
        ->leftjoin('schools','classes.school_id','=','schools.id')
        ->select('grade.name as grade_name','kumi.name as kumi_name','schools.name as school_name')
        ->where('classes.id','=',$class_id)
        ->first();

        //クラスの生徒数（上の表で使用）
        $student_num_byclass = $users -> count();

        //リターンview
        return view('system.classstudent',compact('users','class','student_num_byclass' ));
    }
    /**
     * ★削除  ※1件ずつ外部キー違反をチェックするため、foreachで削除する
     * Route::post
     * @return view('system.classinfo2')
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- system/ClassInfo2Controller@delete');
//dd($request->all());
        $checks = $request->checks;
        if($checks !== null ){
            //dd($request->all());
            DB::beginTransaction();
            
            foreach($checks as $check){
                try{
                    Clas::destroy(intval($check));
                    
                }catch(Exception $e){
                    //dd($e->errorInfo[1]);
                    if($e->errorInfo[1] === 1451){      //外部キー違反  
                        logger()->info('The classes cant destroy,cause alredy in use.');
                        DB::rollBack();
                        //リダイレクトの代わりにindexメソッド
                        $this->index($request)
                            ->withErrors(['message' => '生徒が存在するクラスは削除できません']);
                    }else{
                        DB::rollBack();
                        logger()->error('The classes destroy unknown error.');
                        logger()->error($e);
                        abort(500);
                    }
                }
                DB::commit();
                session()->flash('flash_message', '削除しました');
            }
        }
        
        //リダイレクトの代わりにindexメソッド
        return $this->index($request);
    }
}