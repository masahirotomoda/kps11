<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Information;
use App\Models\Trial;
use App\Models\School;

/**
 * システム管理メニュー　＞　メインメニュー　※ページネーションあり
 * (1)メインメニュー
 * (2)すべてのお知らせ　30件
 * (3)すべてのトライアル学校　100件
 * 【重要】ヘッダーにある学校名は、「ナレッジプログラミングスクール」で固定しているので、$school（学校名取得のため）を各メソッドで作る必要はない
 * @return view('system.menualltrial')
 */
$pagenation = 200;
class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //logger()->debug('<-- system/MenuController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }

    /**
     * ★メインメニュー
     * Route::get
     * @return  view('system.menu')
     */
    public function index(Request $request)
    {
        //logger()->debug('<-- system/MenuController@index');
        //2件のみ表示
        $information = Information::where('invalid','<>',1)->limit(2)->orderBy('created_at','DESC')->get();
        //3件のみ表示
        $trial = Trial::limit(3)->orderBy('created_at','DESC')->get();
        return view('system.menu',compact('information','trial'));
    }
    /**
     * ★すべてのお知らせ
     * メインメニューには2件、すべてのお知らせは1件目から表示で30件
     *　Route::get
     * @return view('system.menuinformation')
     */
    public function allinfo()
    {
        //logger()->debug('<-- system/MenuController@allinfo');
        global $pagenation;
        $school = School::where('school_id','=',auth()->user()->school_id)->first();
        $information = Information::where('invalid','<>',1)->orderBy('created_at','DESC')
            ->paginate(30);
        return view('system.menuinformation',compact('school','information'));
    }
    /**
     * ★すべてのトライアル学校情報
     * トライアル管理画面はあるので、ここでは速報的に表示
     *　Route::get
     * @return view('system.menualltrial')
     */
    public function alltrial()
    {
        //logger()->debug('<-- system/MenuController@alltrial');
        global $pagenation;
        $school = School::where('school_id','=',auth()->user()->school_id)->first();
        $trials = Trial::leftJoin('schools','trials.school_id','schools.id')
            ->leftJoin('classes','trials.class_id','classes.id')
            ->leftJoin('kumi','classes.kumi_id','kumi.id')
            ->select('trials.name as trial_name','schools.id as school_int_id','schools.school_id as sc_id','schools.name as school_name','kumi.name as kumi_name','trials.*','classes.kumi_id as kumi_id','classes.id as class_id' )
            ->orderBy('created_at','DESC')
            ->paginate(100);

        //dd($trials);
        return view('system.menualltrial',compact('school','trials'));
    }
}
