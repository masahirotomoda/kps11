<?php

namespace App\Http\Controllers\system;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\School;
//赤線がでるが、PDFでつかうので必要
use Barryvdh\DomPDF\Facade\Pdf;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Export;
use DateTime;


/**
 * システム管理メニュー　＞　請求書管理
 * 【重要】indexに検索もいれる
 * (1)請求書一覧＆検索＆エクセル出力
 * (2)編集・新規登録画面に遷移
 * (3)編集・新規登録保存
 * (4)領収書・受領書PDF印刷
*/
class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
   /**
     * ★請求書一覧＆検索＆エクセル出力
     * Route::get　post
     * @return  view('system.invoice')
     */
    public function index(request $request)
    {
        global $pagenation;
//dd($request->all());
        //検索値を変数に代入(input の第二引数は、第一引数が空の時につかう値)
        $school_id = $request->input('school_id',null);
        $school_name = $request->input('school_name',null);
        $show_invoice = $request->input('show_invoice',null);
        $show_receipt = $request->input('show_receipt',null);
        $kigengire = $request->input('kigengire',null);
        $start_date = $request->input('start_date',null);
        $end_date = $request->input('end_date',null);
        $valid_select =$request->input('valid_date',null);
        $juryo_day =$request->input('juryo_day',null);
        
        //請求書発行済みかのセレクトボックス値値
        $show_invoice_selection = array(null => "請求発行？未？","1" => "請求書発行済み","0" => "未",);
        $show_receipt_selection = array(null => "受領発行？未？","1" => "受領書発行済み","0" => "未",);
        $juryo_selection = array(null => "入金チェック?","yes" => "入金OK","no" => "未",);       

        //終了日を+１する理由⇒SQLで「　>$endDate」 にするとその日が含まれない（1/5を設定すると1/4まで対象、1/5を含ませるために+1）
        $endDateTimeTmp = new DateTime($request->input('end_date'));
        $endDateTimeTmp->modify('+1 day'); 

        //検索結果
        $invoices_data = Invoice::leftJoin('schools','invoices.school_int_id','schools.id')
            ->where('schools.invoice',1)
            ->where('schools.invalid',0)
            ->when($show_invoice !== null ,function($q) use($show_invoice){
                return $q->where('invoices.show_invoice',$show_invoice);
            })
            ->when($show_receipt !== null ,function($q) use($show_receipt){
                return $q->where('invoices.show_receipt',$show_receipt);
            })
            ->when($juryo_day !== null && $juryo_day=='yes',function($q){
                return $q->where('invoices.juryo_day','<>',null);
            })
            ->when($juryo_day !== null && $juryo_day=='no',function($q){
                return $q->where('invoices.juryo_day',null);
            })
            ->when($school_name !== null ,function($q) use($school_name){
                return $q->where('schools.name','Like binary',"%$school_name%");
            })
            ->when($school_id !== null ,function($q) use($school_id){
                return $q->where('schools.school_id','Like binary',"%$school_id%");
            })
            //カレンダーの開始日、終了日
            ->when($start_date != '' ,function($q) use($start_date){
                return $q->where('invoices.kigen_day','>=',$start_date);
            })
            ->when($end_date != '' ,function($q) use($end_date){
                return $q->where('invoices.kigen_day','<=',$end_date);
            })
            ->select('invoices.*','name','schools.id as sc_int_id','contest','schools.created_at as sc_created_at','schools.updated_at as sc_updated_at')
            ->orderBy('seikyu_day','DESC');
        //エクセルボタン押下
        if($request->has('excel')){
            $invoices=$invoices_data->get();
            if ($invoices->isEmpty()) {
                return redirect('/bestscore')
                ->withErrors('エクセルに出力するデータがありません。検索して、データを表示させてから「データの一括ダウンロード(Excel)」ボタンを押してください');
            }
            $view = view('excel.invoiceexcel',compact('invoices'));
            return Excel::download(new Export($view), '【システム管理】請求書ダウンロード'.date('Y年m月d日H時i分s秒出力').'.xlsx');

        } else {//トップページ表示（index）、検索の場合     
            $invoices=$invoices_data->paginate($pagenation);
            return view('system.invoice',compact('invoices','juryo_selection','show_invoice_selection','show_receipt_selection','show_invoice','show_receipt','school_id','school_name','kigengire','start_date','end_date','juryo_day'));        
        }        
    }
    /**
     * ★編集画面へ遷移 
     * ※postにしたかったが、印刷チェックが外側のformをとり、入れ子formができないため、getにする
     * Route::get
     * @return  view('system.invoiceadd')
     */
    public function edit($invoiceId)
    {
//dd($invoiceId);
        $howpay_selection = array(null => "支払い方法","クレジットカード" => "クレジットカード","銀行振込" => "銀行振込",);
        $school_selection = School::selectList();
        $invoice = Invoice::where('invoices.id',$invoiceId)->first();
        
        $school=School::where('schools.id',$invoice->school_int_id)->first();
        $AddOrUpdate = 'update';

        return view('system.invoiceadd',compact('invoice','AddOrUpdate','school_selection','howpay_selection','school'));
    }
    /**
     * ★新規登録画面へ遷移 
     * ※postにしたかったが、印刷チェックが外側のformをとり、入れ子formができないため、getにする
     * Route::get
     * @return  view('system.invoiceadd')
     */
    public function add($add)
    {
        $howpay_selection = array(null => "支払い方法","クレジットカード" => "クレジットカード","銀行振込" => "銀行振込",);
        $school_selection = School::selectList();
        $invoice=null;
        $AddOrUpdate = 'add';
        $school=null;
        
        return view('system.invoiceadd',compact('invoice','AddOrUpdate','school_selection','howpay_selection','school'));
    }
    /**
     * ★新規、編集の保存
     * Route::get　post
     * @return  redirect
     */

    public function store(request $request)
    {
        //バリデーション
        if($request->input('seikyu_day') > $request->input('kigen_day')){
            session()->flash('flash_message', null);
            return redirect('/invoice')
                ->withErrors("発行日と期限日を確認");
        }
        if($request->has('update')){
            $invoice = Invoice::where('invoices.id',$request->invoice_id)->first();
            $invoice->id=$request->invoice_id;
            $success_message="請求書を編集しました。";
            //dd($invoice);
        } elseif($request->has('add')){
            $invoice=new Invoice();
            $success_message="請求書を新規登録しました。";
        } else {
            abort(500);
        }
           
        $invoice->seikyu_day=$request->seikyu_day;
        $invoice->kigen_day=$request->input('kigen_day');
        $invoice->juryo_day=$request->input('juryo_day');
        $invoice->service=$request->input('service');
        $invoice->service_period=$request->input('service_period');
        $invoice->howpay=$request->input('howpay');
        $invoice->other=$request->input('other');
        $invoice->memo=$request->input('memo');
        $invoice->school_int_id=$request->input('school_int_id');
        $invoice->tanka=$request->input('tanka');
        $invoice->count=$request->input('count');
        $invoice->price=$request->input('price');
        $invoice->tax=$request->input('tax');
        if($request->has('show_invoice')){
            $invoice->show_invoice=1;
        } else {
            $invoice->show_invoice=0;
        }
        if($request->has('show_receipt')){
            $invoice->show_receipt=1;
        } else {
            $invoice->show_receipt=0;
        }
        //dd($invoice);
        $invoice->save();
        session()->flash('flash_message', $success_message);

        return redirect('/invoice');
    }
    /**
     * ★削除
     * Route::post
     * @return  redirect('/invoice');
     */
     public function delete(request $request)
     {
        Invoice::where('id', $request->id)->delete();
        session()->flash('flash_message', '請求書を削除しました');
 
         return redirect('/invoice');
     }
    /**
     * ★請求書、受領書　PDF出力、請求書のコピー
     * ライブラリ：\vendor\dompdf\dompdf\README.md　　　vendor\dompdf
     * フォント：vendor\dompdf\dompdf\lib\fonts　　CSS:vendor\dompdf\dompdf\lib\res\html.css
     * @return   $pdf->stream()
     */
    public function pdf(Request $request){
    //dd($request->all());
        //請求書チェックがあれば（$request['checks']は、チェックしている請求書のIDが配列で格納）        
        //pdf1（請求書）ボタン押下
        if($request->has('pdf1') && isset($request['checks1']) ){
            $school = School::where('id',auth()->user()->school_int_id)->first();
            //PDFプリントに必要なデータ
            $invoices = Invoice::leftjoin('schools','invoices.school_int_id','schools.id')
                ->where('schools.invoice',1)
                //->where('invoices.show_invoice',1)//請求書発行がTrueでなくても確認できるように
                ->whereIn('invoices.id',$request->checks1)
                ->select('invoices.*','schools.name')
                ->orderBy('invoices.seikyu_day','DESC')
                ->get();

            $pdf = PDF::loadView('pdf.transcript_invoice',compact('invoices'));
            return $pdf->stream();
        //pdf2（受領書）ボタン押下
        } elseif($request->has('pdf2') && isset($request['checks2']) ){
            $school = School::where('id',auth()->user()->school_int_id)->first();
            //PDFプリントに必要なデータ
            $invoices = Invoice::leftjoin('schools','invoices.school_int_id','schools.id')
                ->where('schools.invoice',1)
                //->where('invoices.show_receipt',1)//受領書発行がTrueでなくても確認できるように
                ->whereIn('invoices.id',$request->checks2)
                ->select('invoices.*','schools.name')
                ->orderBy('invoices.seikyu_day','DESC')
                ->get();
           

            if($invoices==null){
                return redirect('/invoice')->withErrors('まだ、受領書の準備ができていません。');
            }
            $pdf = PDF::loadView('pdf.transcript_receipt',compact('invoices'));
            return $pdf->stream();
        } elseif($request->has('copy') && isset($request['checks3']) ){
            //請求書コピー
            if(count($request['checks3'])>1){
                return redirect('/invoice')->withErrors("請求書チェックは1件のみです。");
            }
            $invoiceData = Invoice::find($request['checks3'][0])->toArray();
            //コピーして保存
            $invoice=new Invoice();
            $invoice->fill($invoiceData)->save();
            //編集はgetパラメータなのでredirect
            return redirect('/invoice-edit/'.$invoice->id);           
        } else {
            return redirect('/invoice')->withErrors('処理に失敗しました。');;
        }
    }
}