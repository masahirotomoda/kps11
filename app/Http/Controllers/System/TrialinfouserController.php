<?php

namespace App\Http\Controllers\system;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\School;
use App\Models\Clas;
use App\Models\Kumi;
use Exception;

    /**
     * システム管理メニュー　＞　トライアル管理　＞　ユーザー一括更新
     * ※トライアル管理のトップページはtrialinfocontroller このクラスは、「ユーザ編集」ボタン押下の遷移先ページで、トライアルクラスのユーザー削除、有料へ昇格した際のIDのつけかえ等をおこなう。
     * (1)ユーザー一覧
     * (2)ユーザーの一括更新
     * (3)ユーザー削除(ユーザーのベストポイントが自動削除)
     */
$pagenation =200;
class TrialinfouserController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- TrialinfouserController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★ユーザー一覧（遷移前の学校選択のデータを引き継ぐ）
     * @return view('system.trialinfouser')
     * Route::match(['get', 'post'] 
     */
    public function index(Request $request)
    {
        logger()->debug('<-- TrialinfouserController@index');
        global $pagenation;
        //dd($request->all());
      
        //選んだ学校のIDとクラスデータと組データ（上の表で使用）
        $trial_school_int_id = $request->hidden_school_id;
        $trial_school = School::find($request->hidden_school_id);
        $trial_class_id = $request->hidden_class_id;
        $trial_grade_name = $request->hidden_grade;
        $trial_kumi_name = $request->hidden_kumi;
        
        //ユーザーデータ（下の表で使用）
        $users = User::leftJoin('schools','users.school_int_id','schools.id')
        ->leftJoin('classes','users.class_id','classes.id')
        ->leftJoin('grade','classes.grade_id','grade.id')
        ->leftJoin('kumi','classes.kumi_id','kumi.id')
        ->select('schools.name as school_name','schools.id as school_int_id','schools.school_id as school_id',
            'kumi.name as kumi_name','grade.name as grade_name','grade.id as grade_id','kumi.id as kumi_id','users.*')
        //選んだクラス
        ->where('users.class_id','=',$trial_class_id)
        ->orderBy('role','ASC')
        ->orderBy('attendance_no','ASC')
        ->paginate($pagenation);

        //リターンview
        return view('system.trialinfouser',compact('trial_school','users','trial_school_int_id','trial_class_id','trial_kumi_name','trial_grade_name')); 
    } 
  
    /**
     * ★一括更新　　一括更新ボタン（update_btn）
     * 古いトライアルのクラスとクラスIDを削除する。新しい学校にユーザーを移行する。（トライアルフラグをはずす）
     * Route::match(['get', 'post'] 
     * @return redirect('/user')
     */
    public function update(Request $request)
    {
        //dd($request->all());
        global $pagenation;
        logger()->debug('<-- TrialinfouserController@update');

        //古い（トライアル）学校のIDとクラスデータと組データ
        $trial_school_int_id = $request->hidden_school_id;
        $trial_school = School::find($trial_school_int_id);
        $trial_class_id = $request->hidden_class_id;
        $trial_grade_name = $request->hidden_grade;
        $trial_kumi_name = $request->hidden_kumi;

        //新しい（有料）学校のIDとクラスデータと組データ
        $new_school_int_id = $request->new_school_int_id;
        $new_school = School::find($request->new_school_int_id);
        $new_grade_name = $request->new_grade_name;
        $new_kumi_name = $request->new_kumi_name;
        $new_trial_flg = $request->new_trial_flg;

        //クラスID以外のバリデーション実行
        $validator = Validator::make($request->all(), [
            'new_school_int_id' => ['required','integer'],
            'new_school_id' => ['required','alpha_num','max:8'],
            'new_grade_name' => ['required','max:20'],
            'new_kumi_name' => ['required','max:20'],
            'new_trial_flg' => ['required','boolean'],
        ]);
        
        //バリデーション失敗時　バリデーションメッセージはvalidation.php で指定
        if ($validator->fails()) {
            //リダイレクトの代わりにindexメソッド
            return $this->index($request)->withErrors($validator);
        }

        //新しい学年名、組名からクラスIDを取得
        $new_class = Clas::leftJoin('schools','classes.school_id','schools.id')
        ->leftJoin('grade','classes.grade_id','grade.id')
        ->leftJoin('kumi','classes.kumi_id','kumi.id')
        ->where('schools.id','=',$new_school_int_id)
        ->where('grade.name','=',$new_grade_name)
        ->where('kumi.name','=',$new_kumi_name)
        ->select('grade.name as grade_name','kumi.name as kumi_name','classes.id as class_id')
        ->first();
        //dd($new_class);

        //クラスIDのバリデーション準備
        //クラスがないときはクラスIDにnullがはいり、必須のバリデーションにひっかかる
        if($new_class === null){
            $request['class_id'] = null;
        } else {
            $request['class_id'] =  $new_class->class_id;
        }            
        //dd($request->all());

        //クラスID必須のバリデーション実行
        $validator = Validator::make($request->all(), [
            'class_id' => ['required'],
        ]);

        //クラスIDのバリデーション失敗時
        if ($validator->fails()) {
            //リダイレクトの代わりにindexメソッド
            return $this->index($request)->withErrors('学年かクラスが存在しません。確認しましょう。');
        }

        //すべてのバリデーション成功時、ユーザーの情報を更新            
        //トライアル学校（古い）ユーザーデータ
        $users = User::where('users.class_id','=',$trial_class_id)->get();
        //dd($users->isNotEmpty('id'));

        //ここから、更新処理　※組とクラスは、ここでは削除しない。学校を削除するときに削除される。
        if($users->isNotEmpty('id')){
            try{
                DB::beginTransaction();
                foreach ($users as $user){
                    $user->class_id = $request->class_id;
                    $user->school_int_id = $new_school_int_id;
                    $user->school_id = $new_school->school_id;
                    $user->trial_flg = $new_trial_flg;
                    $user -> save();
                }
                DB::commit();
            } catch (Exception $e) {
                logger()->error('◆システム管理メニュー、トライアル学校から有料プランへ一括変更でエラー');
                logger()->error($e);
                DB::rollback();
                abort(500);
            }
            //更新成功時、flashメッセージ
            session()->flash('flash_message', '更新しました');
        } else {
            //更新失敗時、flashメッセージ
            session()->flash('flash_message', '更新に失敗しました');
        }
        return $this->index($request);
     } 
     /**
     * ★削除
     * @return  
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- TrialinfouserController@delete');
        //dd($request->all());
        //削除するコースのコースIDが[「$checks」に配列として格納
        $checks = $request->checks;

        if($checks !== null ){
            try{
                DB::beginTransaction();
                User::whereIn('id', $checks)->delete();
                DB::commit();
            } catch (Exception $e) {
                logger()->error('◆システム管理メニュー、トライアル学校のユーザー削除でエラー');
                logger()->error($e);
                DB::rollback();
                abort(500);
            }
            //削除成功時、flashメッセージ
            session()->flash('flash_message', 'ユーザーを削除しました。');
        } else {
            session()->flash('flash_message', 'ユーザーの削除に失敗しました。');

        }
        return redirect('/trialinfo');

     }
}