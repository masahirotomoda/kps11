<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\BattleScoreHistory;
use App\Models\School;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Export;

    /**
    * システム管理メニュー　＞　ランキングバトル　
    *(1)ランキングスコア履歴一覧
    *(2)検索
    *(3)削除
    */
$pagenation = 200;
class BattleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }

    /**
     * ★ランキングスコア履歴一覧
     * Route::get
     * @return  view('system.battle')
     */
    public function index()
    {
        global $pagenation;
        //$school = School::where('school_id','=',auth()->user()->school_id)->first();

        //検索ボックスの初期値
        $startDate = null;
        $startTime = null;
        $endDate = null;
        $endTime = null;
        $course_name = null;
        $user_name = null;
        $school_name = null;
        $name = null;
        $nick_name = null;

        //検索　ロール、降順のチェックボックス
        $teacher = false;
        $student = true;
        $sort = false;

        //スコア履歴データ
        $BattleScoreHistory = BattleScoreHistory::leftjoin('users','battlescore_history.user_id','=','users.id')
            ->leftJoin('schools','users.school_int_id','=','schools.id')
            ->select('users.name as user_name','battlescore_history.created_at as sh_created_at','users.*','battlescore_history.*','schools.name as school_name')
            ->orderByRaw('battlescore_history.created_at DESC')
            ->paginate($pagenation);

        //リターンview
        return view('system.battle',compact('startDate','startTime','teacher','student',
            'endDate','endTime','course_name','user_name','BattleScoreHistory','name','school_name','nick_name','sort'));
    }
    /**
     * * ★ランキングスコア履歴　検索、エクセル出力
     * Route::match(['get', 'post']
     * 
     * @return  view('system.battle')
     */
    public function search(Request $request)
    {
        global $pagenation;

        //検索値を変数にセット　ロールのチェックボックス
        $teacher = $request->input('teacher');
        $student = $request->input('student');
        $sort = $request->input('sort');

        //先生、生徒にチェックがあるものを$selectRoleの配列にいれる⇒SQLの検索条件を配列に入れる
        $selectRole = [];
        if($teacher !== null){
            array_push($selectRole,'先生');
        }
        if($student !== null){
            array_push($selectRole,'生徒');
        }
        
        //検索値を変数にセット
        $course_name = $request->input('course_name',null);
        $name = $request->input('name',null);
        $school_name = $request->input('school_name',null);
        $nick_name = $request->input('nick_name',null);

        //検索値を変数にセット
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        //開始時間が空欄の処理
            $startTime = $request->input('start_time',null);
            if($startTime == null){
                $startTime  = '00:00:01';
        //終了時間が空欄の処理
            }
            $endTime = $request->input('end_time',null);
            if($endTime == null){
                $endTime  = '23:59:59';
            }
        //ランキングスコア履歴データ　※クエリのみ
        if($sort==true){//降順チェックボックスにチェックがあれば、スコアの降順
            $BattleScoreHistory = BattleScoreHistory::leftjoin('users','battlescore_history.user_id','=','users.id')
            ->leftJoin('schools','users.school_int_id','=','schools.id')
            ->select('users.*','battlescore_history.*','schools.name as school_name','schools.school_id','schools.id as school_intid')
            ->whereIn( 'users.role', $selectRole )
            ->when($school_name !== null ,function($q) use($school_name){
                return $q->where('schools.name','Like binary',"%$school_name%");
            })
            ->when($name !== null ,function($q) use($name){
                return $q->where('users.name','Like binary',"%$name%");
            })
            ->when($nick_name !== null ,function($q) use($nick_name){
                return $q->where('battlescore_history.nickname','Like binary',"%$nick_name%");
            })
            ->when( $course_name !== null ,function($q) use( $course_name){
                return $q->where('battlescore_history.course_name','Like binary',"%$course_name%");
            })
            //日付と時間の間に半角スペース必須
            ->when($startDate !== null ,function($q) use($startDate,$startTime){
                return $q->where('battlescore_history.created_at','>',$startDate. ' '. $startTime);
            })
            ->when($endDate !== null ,function($q) use($endDate,$endTime){
                return $q->where('battlescore_history.created_at','<',$endDate. ' '. $endTime);
            })
            ->orderBy('battlescore_history.battle_score','DESC')
            ->orderBy('battlescore_history.created_at','DESC');
        } else {//降順ボックスにチェックがなければ、日付の降順
            $BattleScoreHistory = BattleScoreHistory::leftjoin('users','battlescore_history.user_id','=','users.id')
            ->leftJoin('schools','users.school_int_id','=','schools.id')
            ->select('users.*','battlescore_history.*','schools.name as school_name','schools.school_id','schools.id as school_intid')
            ->whereIn( 'users.role', $selectRole )
            ->when($school_name !== null ,function($q) use($school_name){
                return $q->where('schools.name','Like binary',"%$school_name%");
            })
            ->when($name !== null ,function($q) use($name){
                return $q->where('users.name','Like binary',"%$name%");
            })
            ->when($nick_name !== null ,function($q) use($nick_name){
                return $q->where('battlescore_history.nickname','Like binary',"%$nick_name%");
            })
            ->when( $course_name !== null ,function($q) use( $course_name){
                return $q->where('battlescore_history.course_name','Like binary',"%$course_name%");
            })
            //日付と時間の間に半角スペース必須
            ->when($startDate !== null ,function($q) use($startDate,$startTime){
                return $q->where('battlescore_history.created_at','>',$startDate. ' '. $startTime);
            })
            ->when($endDate !== null ,function($q) use($endDate,$endTime){
                return $q->where('battlescore_history.created_at','<',$endDate. ' '. $endTime);
            })
            ->orderBy('battlescore_history.created_at','DESC');
        }            

        //ここから分岐 エクセル出力ボタン押下
        if($request->has('excel')){
            $BattleScoreHistory = $BattleScoreHistory -> get();

            //検索ボックスの開始時間と終了時間は空欄時、時間（00:00:01と'23:59:59）を入力しているため、viewに渡すときに空にする
            $startTime = $request->input('start_time');
            $endTime = $request->input('end_time');

            //リターンview　エクセル出力
            $view = view('system.battleexcel',compact('BattleScoreHistory'));
            return Excel::download(new Export($view), '【管理】ランキングスタジアム履歴データ'.date('Y-m-d-H-i-s').'.xlsx');
        
        } elseif($request->has('search') || $request->has('page')){
            $BattleScoreHistory = $BattleScoreHistory -> paginate($pagenation);
            
        
            //検索ボックスの開始時間と終了時間は空欄時、時間（00:00:01と'23:59:59）を入力しているため、viewに渡すときに空にする
            $startTime = $request->input('start_time');
            $endTime = $request->input('end_time');

            //リターンview
            return view('system.battle',compact('startDate','startTime','school_name',
                'endDate','endTime','course_name','teacher','student','BattleScoreHistory','name','nick_name','sort'));
    
        } else {
            return redirect('/battle');
        }
    }
    /**
     *★削除
     * Route::post
     * @return view('system.bsttle')
     */
    public function delete(Request $request){

        //削除チェックがあれば（$request['checks']は、チェックしているbattlescorehistoryのIDが配列で格納）
        $checks = $request->checks;
        if($checks !== null ){
            DB::beginTransaction();
            foreach($checks as $check){
                try{
                    BattleScoreHistory::destroy(intval($check));
                }catch(\Exception $e){
                        DB::rollBack();
                        logger()->error('◆システム管理メニュー、ランキングスタジアム削除でエラー');
                        logger()->error($e);
                        abort(500);
                }
            }
            DB::commit();
        }
        return redirect('/battle');
    }
}