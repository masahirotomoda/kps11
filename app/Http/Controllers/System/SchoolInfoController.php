<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\Clas;
use App\Models\User;
use App\Models\CourseWord;
use App\Models\Course;
use App\Models\Word;
use App\Models\ScoreHistoryContest;
use App\Models\Conuser;
use App\Models\BestScore;
use App\Exports\Export;
use Maatwebsite\Excel\Facades\Excel;
use \Exception;

/**
 * システム管理メニュー ＞　学校管理　
 * Route::get
 * (1)学校一覧　※ページネーションあり
 * (2)検索＆エクセル出力
 * (3)登録＆編集画面へ遷移
 * (4)更新
 * (5)新規
 * (6)削除
*/
$pagenation = 200;
class SchoolInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★学校一覧 ※コンテスト学校も含む
     *　学校＆コース＆単語の最大数と現在数を表示（学校＆コースはSQLで実装、単語はbladeでforeachで取得）
     *　Route::get　※ページネーションあり
     * @return view('system.schoolinfo')
     */
    public function index()
    {
        global $pagenation;
        //学校情報（現在のユーザー数、コース数も取得） ※単語数はここで取得できないため、bladeでforeachで取得する
        $schools = DB::table('schools AS sc')
        ->select([
            'sc.*'
            , DB::raw("(SELECT COUNT(DISTINCT u.id) AS user_count
                FROM users AS u WHERE u.school_int_id = sc.id
                GROUP BY sc.id) AS user_count")
            , DB::raw("(SELECT COUNT(DISTINCT c.id) AS user_count
                FROM courses AS c WHERE c.school_id = sc.id
                GROUP BY sc.id) AS course_count")
        ])
        ->orderBy('invalid','ASC')
        ->orderBy('address','ASC')
        ->orderBy('name','ASC')
        ->paginate($pagenation);
        //dd($schools);

        //有効な学校数
        $cnt = School::Where('invalid',0)->count();

        //学校特別アカウントがあるかチェック（bladeファイルで使用）
        $users = User::where('role','学校管理者')->get();
        //dd($users);

        //検索ボックスの初期値
        $school_id = null;
        $school_name = null;
        $valid = true;
        $invalid = true;
        $contest = false;

        //単語一覧を中間テーブル（course_word）から取得　※コースと単語を一対一でつなぐ
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
        ->leftJoin('words','course_word.word_id','words.id')
        ->leftJoin('schools','courses.school_id','schools.id')
        ->select('course_word.id as course_word_id','courses.school_id as course_school_id','words.word_mana')
        ->get();

        //リターンview
        return view('system.schoolinfo',compact('users','schools','school_id','school_name','valid','invalid','cnt','courseWords','contest'));
    }
    /**
     * ★検索＆エクセル出力　ボタンで振り分け
     * Route::match(['get', 'post']　ページネーションあり　
     * @return view('system.schoolinfo')
     */
    public function search(Request $request)
    {
        global $pagenation;
        //dd($request->all());

        //学校特別アカウントがあるかチェック（bladeファイルで使用）
        $users = User::where('role','学校管理者')->get();

        //検索値を変数にセット
        $school_id = $request->input('school_id');
        $school_name = $request->input('school_name');
        $invalid = $request->input('invalid'); //カラムはinvalid　invalidとvalidに分けて表示
        $valid = $request->input('valid');
        $contest = $request->input('contest');
        //検索後の学校データのクエリ（自分がログインしている学校情報ではない）
        $schools = DB::table('schools AS sc')
        ->select([
            'sc.*'
            , DB::raw("(SELECT COUNT(DISTINCT u.id) AS user_count
                FROM users AS u WHERE u.school_int_id = sc.id
                GROUP BY sc.id) AS user_count")
            , DB::raw("(SELECT COUNT(DISTINCT c.id) AS user_count
                FROM courses AS c WHERE c.school_id = sc.id
                GROUP BY sc.id) AS course_count")
            ])
        ->when($school_id !== null ,function($q) use($school_id){
            return $q->where('sc.school_id','Like',"%$school_id%");
        })
        ->when($school_name !== null ,function($q) use($school_name){
            return $q->where('sc.name','Like binary',"%$school_name%");
        })
        ->when(($valid !== null && $invalid === null ) ,function($q) use($invalid){
            return $q->where('invalid', '=', false);
        })
        ->when(($valid === null && $invalid !== null ) ,function($q) use($invalid){
            return $q->where('invalid', '=', true);
        })
        ->when(($contest !== null) ,function($q) use($contest){
            return $q->where('contest', '>', 0);
        })
        ->orderBy('invalid','ASC')
        ->orderBy('address','ASC')
        ->orderBy('name','ASC');

        //全学校数（有効のみ）
        $cnt = School::Where('invalid',0)->count();

        //単語登録数を取得するためのデータ　※単語一覧を中間テーブル（course_word）から取得　※コースと単語を一対一でつなぐ
        //データをviewで渡してviewでforeachで集計する
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
        ->leftJoin('words','course_word.word_id','words.id')
        ->leftJoin('schools','courses.school_id','schools.id')
        ->select('course_word.id as course_word_id','courses.school_id as course_school_id','words.word_mana')
        ->get();


        //【ここから振り分け】検索ボタンか、エクセル出力ボタンかをsubmitボタンの名前で判断
        //検索ボタン押下時
        if($request->has('search') || $request ->has('page')){
            $schools = $schools ->paginate($pagenation);
            return view('system.schoolinfo',compact('schools','school_id','school_name','valid','invalid','cnt','courseWords','users','contest'));
        //エクセル出力ボタン押下時
        }elseif($request->has('excel')){
            $schools = $schools -> get();
            $view = view('system.schoolinfoexcel',compact('schools','school_id','school_name','valid','invalid','cnt','courseWords','users'));
            return Excel::download(new Export($view), '【管理】学校一覧'.date('Y-m-d-H-i-s').'.xlsx');
        } else {
            logger()->error('◆システム管理メニュー、学校の検索orエクセル出力で保存ボタンを押下しない時のエラー');
            abort(500);
        } 
    }

    /**
     * ★登録＆編集画面へ遷移
     * 新規ボタン押下⇒新規入力ページへ　　編集ボタン押下⇒編集画面へ　※遷移先のbladeは同じ
     * ※【重要】保存処理でない、ページへ遷移するだけのメソッド
     * ページネーションなし
     * @return view('system.schoolinfo')
     */
    public function edit(Request $request)
    {
        //学校データ（ログインしているシステム管理の学校）
        $school = School::where('schools.id','=',Auth()->user()->school_int_id)->first();

        //【ここから処理分岐】編集ボタン押下時
        if($request->has('update')){
            $AddOrUpdate = 'update';  //bladeファイルで使用　新規か更新かのフラグ
            $id = $request ->input('id');
            $school_info = School::where('id','=',$id)->first();

        //新規ボタン押下時 
        } else if($request->has('add')){
            $AddOrUpdate = 'add';
            //新規学校用（データは空）
            $school_info = new School();
            $id = null;
        }
        
        //【ここから共通】リターンview　学校IDを持ちまわる2階層目なので、リダイレクトできない
        return view('system.schoolinfoedit',compact('school','AddOrUpdate','id','school_info'));
    }
    /**
     * ★更新　※編集ページで更新ボタン押下
     *  Route::post　保存ボタン連打防止対策はおこなわない（更新されるだけなので）
     * @return view('system.schoolinfoedit')　⇒リダイレクトにしない理由：更新ボタンで重複更新を行うが、更新だけなので問題なし
     */
    public function update(Request $request)
    {        
        //bladeファイルで使用　編集か新規か？のフラグ
        $AddOrUpdate = 'update';
        //dd($request->all());

        //バリデーション コンテスト用は先生のパスワード初期値が学校アカウントの後半8文字となるため、バリデーションをゆるくする
        if($request->contest>0){
            $validator = Validator::make($request->all(), [
                //更新にユニークはバリデーションではじかれる ignoreの設定をしないと、自分のIDを重複とみなし、エラーするため
                'school_id' => ['required', 'string','regex:<[a-zA-Z0-9]{8,8}>',Rule::unique('schools')->ignore($request->id)],
                'name' => ['string','max:20','required'],
                'address' => ['string','max:20','required'],
                'user_number' => ['integer','between:1,10000','required'],
                'course_number' => ['integer','between:1,1000','required'],
                'word_number' => ['integer','between:1,10000','required'],
                'password_school_init' => ['string','required'],//ゆるい
                'password_teacher_init' =>['string','required'],//ゆるい
                'password_student_init' => ['integer','between:1,999999','required'],
                'contest' => ['integer','between:0,2','required'],
            ]);

        } else {
            $validator = Validator::make($request->all(), [
                //更新にユニークはバリデーションではじかれる ignoreの設定をしないと、自分のIDを重複とみなし、エラーするため
                'school_id' => ['required', 'string','regex:<[a-zA-Z0-9]{8,8}>',Rule::unique('schools')->ignore($request->id)],
                'name' => ['string','max:20','required'],
                'address' => ['string','max:20','required'],
                'user_number' => ['integer','between:1,10000','required'],
                'course_number' => ['integer','between:1,1000','required'],
                'word_number' => ['integer','between:1,10000','required'],
                //'password_school_init' => ['string',
                //    'regex:<^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[!@;:#$%&])|(?=.*[A-Z])(?=.*[0-9])(?=.*[!@;:#$%&])|(?=.*[a-z])(?=.*[0-9])(?=.*[!@;:#$%&]))([a-zA-Z0-9!@;:#$%&]){10,}$>'
                //    ,'required'],
                //'password_teacher_init' => ['string',
                //    'regex:/\A(?=.*?[a-z])(?=.*?\d)[a-z\d]{6,21}+\z/i'
                //    ,'required'],
                'password_student_init' => ['integer','between:1,999999','required'],
                'contest' => ['integer','between:0,2','required'],
            ]);
        }
        //バリデーション失敗時
        if ($validator->fails()) {
            //dd($request->all());
            $school_info = School::where('schools.id','=',$request->input('id'))->first();
            //リターンview
            return view('system.schoolinfoedit',compact('AddOrUpdate','school_info'))
                ->withErrors($validator);
        }
        
        //dd($request->all());
        //更新データをセット（$request->all はセキュリティで危ないので個別にセット）
        $school_info = School::find($request->input('id'));
        $school_info -> school_id = $request -> input('school_id');
        $school_info -> name = $request -> input('name');
        $school_info -> address = $request -> input('address');
        $school_info -> tel = $request -> input('tel');
        $school_info -> mail = $request -> input('mail');
        $school_info -> user_number = $request -> input('user_number');
        $school_info -> course_number = $request -> input('course_number');
        $school_info -> word_number = $request -> input('word_number');
        $school_info -> password_school_init = $request -> input('password_school_init');
        $school_info -> password_teacher_init = $request -> input('password_teacher_init');
        $school_info -> password_student_init = $request -> input('password_student_init');
        $school_info -> contest = $request -> input('contest');
        $school_info -> memo1 = $request -> input('memo1');
        $school_info -> logo = $request -> input('logo');
        //$school_info -> trial = $request -> input('trial');
        //チェックボックスは、チェックがないと何もかえさない。
        //「無効」⇒1　「有効」⇒0
        if($request->has('invalid')){
            $school_info->invalid = 1;
        } else {
            $school_info->invalid = 0;
        }
        //「長文コース表示」⇒1　「非表示」⇒0 //カラムは「word_count」
        if($request->has('long_course')){
            $school_info->long_course = 1;
        } else {
            $school_info->long_course = 0;
        }
        //生徒パスワードボタン表示⇒1　非表示」⇒0
        if($request->has('password_display')){
            $school_info->password_display = 1;
        } else {
            $school_info->password_display = 0;
        }
        //トライアルか⇒1　トライアルでない⇒0
        if($request->has('trial')){
            $school_info->trial = 1;
        } else {
            $school_info->trial = 0;
        }
        //ログイン名を大きく表示させる⇒1　表示しない（既定値）⇒0
        if($request->has('display_name')){
            $school_info->display_name = 1;
        } else {
            $school_info->display_name = 0;
        }
        //単語の音声ファイルアップロードや編集を表示させる⇒1　表示しない（既定値）⇒0
        if($request->has('word_sound')){
            $school_info->word_sound = 1;
        } else {
            $school_info->word_sound = 0;
        }
        //請求書を学校管理画面に表示させる⇒1　表示しない（既定値）⇒0
        if($request->has('invoice')){
            $school_info->invoice = 1;
        } else {
            $school_info->invoice = 0;
        }
        //タイピングスコアタイプ⇒1　旧算出方法（0点～300点）（既定値・100点満点）⇒0
        if($request->has('scoretype')){
            $school_info->scoretype = 1;
        } else {
            $school_info->scoretype = 0;
        }
        
        //保存
        try{ 
            $school_info -> save();
            session()->flash('flash_message', $school_info -> name.'の情報を更新しました');
        }catch(Exception $e){
            logger()->error('◆システム管理メニュー、学校の更新でエラー');
            logger()->error($e);
            abort(500);
        }

        //リターンview 　更新ボタン連打しても、更新するだけなので、リターンview
        return view('system.schoolinfoedit',compact('AddOrUpdate','school_info'));
    }

/**
     * ★新規　新規ページで保存ボタン押下
     * Route::post　※ページネーションなし
     * @return  redirect('/school-info');
     */
    public function add(Request $request)
    {
        //多重送信対策　保存ボタン連打で、二重登録するのを防ぐ
        //新規保存後、リダイレクトされるので必要ないが、念のため
        $request->session()->regenerateToken();
        
        //新規か更新かのフラグ
        $AddOrUpdate = 'add';

        //バリデーション コンテスト用は先生のパスワード初期値が学校アカウントの後半8文字となるため、バリデーションをゆるくする
        if($request->contest>0){
            $validator = Validator::make($request->all(), [
                //更新にユニークはバリデーションではじかれる ignoreの設定をしないと、自分のIDを重複とみなし、エラーするため
                'school_id' => ['required', 'string','regex:<[a-zA-Z0-9]{8,8}>','unique:schools'],
                'name' => ['string','max:20','required'],
                'address' => ['string','max:20','required'],
                'user_number' => ['integer','between:1,10000','required'],
                'course_number' => ['integer','between:1,1000','required'],
                'word_number' => ['integer','between:1,10000','required'],
                'password_school_init' => ['string','required'],//ゆるい
                'password_teacher_init' =>['string','required'],//ゆるい
                'password_student_init' => ['integer','between:1,999999','required'],
                'contest' => ['integer','between:0,2','required'],
            ]);

        } else {
            $validator = Validator::make($request->all(), [
                //学校IDのユニークチェック（例　ABC12345）
                'school_id' => ['required', 'string','regex:<[a-zA-Z0-9]{8,8}>','unique:schools'],
                'name' => ['string','max:20','required'],
                'address' => ['string','max:20','required'],
                'user_number' => ['integer','between:1,10000','required'],
                'course_number' => ['integer','between:1,1000','required'],
                'word_number' => ['integer','between:1,10000','required'],
                'password_school_init' => ['string',
                    'regex:<^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[!@;:#$%&])|(?=.*[A-Z])(?=.*[0-9])(?=.*[!@;:#$%&])|(?=.*[a-z])(?=.*[0-9])(?=.*[!@;:#$%&]))([a-zA-Z0-9!@;:#$%&]){10,}$>'
                    ,'required'],
                'password_teacher_init' => ['string',
                    'regex:/\A(?=.*?[a-z])(?=.*?\d)[a-z\d]{6,21}+\z/i'
                    ,'required'],
                'password_student_init' => ['integer','between:1,999999','required'],
                'contest' => ['integer','between:0,2','required'],
            ]);
        }
        //バリデーション失敗時
        if ($validator->fails()) {
            //入力値を保持（idはまだ発行されていないのでnull）
            $school_info = new School();
            $school_info->id = null;
            $school_info = $request;
            //リターンview
            return view('system.schoolinfoedit',compact('school_info','AddOrUpdate'))
                ->withErrors($validator);
        }
        //新規データをセット（$request->all はセキュリティで危ないので個別にセット）
        $school_info = new School();         
        $school_info -> school_id = $request -> input('school_id');
        $school_info -> name = $request -> input('name');
        $school_info -> address = $request -> input('address');
        $school_info -> tel = $request -> input('tel');
        $school_info -> mail = $request -> input('mail');
        $school_info -> user_number = $request -> input('user_number');
        $school_info -> course_number = $request -> input('course_number');
        $school_info -> word_number = $request -> input('word_number');
        $school_info -> password_school_init = $request -> input('password_school_init');
        $school_info -> password_teacher_init = $request -> input('password_teacher_init');
        $school_info -> password_student_init = $request -> input('password_student_init');
        $school_info -> contest = $request -> input('contest');
        $school_info -> memo1 = $request -> input('memo1');
        $school_info -> logo = $request -> input('logo');

        //「無効」⇒0　「有効」⇒1
        if($request->has('invalid')){
            $school_info->invalid = 1;
        } else {
            $school_info->invalid = 0;
        }
        //生徒パスワードボタン表示⇒1　非表示」⇒0
        if($request->has('password_display')){
            $school_info->password_display = 1;
        } else {
            $school_info->password_display = 0;
        }
        //トライアルか⇒1　トライアルでない⇒0
        if($request->has('trial')){
            $school_info->trial = 1;
        } else {
            $school_info->trial = 0;
        } 
        //「長文コース表示」⇒1　「非表示」⇒0 //カラムは「word_count」
        if($request->has('long_course')){
            $school_info->long_course = 1;
        } else {
            $school_info->long_course = 0;
        }
        //ログイン名を大きく表示させる⇒1　表示しない（既定値）⇒0
        if($request->has('display_name')){
            $school_info->display_name = 1;
        } else {
            $school_info->display_name = 0;
        }
        //単語の音声ファイルアップロードや編集を表示させる⇒1　表示しない（既定値）⇒0
        if($request->has('word_sound')){
            $school_info->word_sound = 1;
        } else {
            $school_info->word_sound = 0;
        }
        //請求書を学校管理画面に表示させる⇒1　表示しない（既定値）⇒0
        if($request->has('invoice')){
            $school_info->invoice = 1;
        } else {
            $school_info->invoice = 0;
        }

        //保存
        try{
            $school_info -> save();
            session()->flash('flash_message', $school_info -> name.'を登録しました');
            $AddOrUpdate = 'update';

        }catch(Exception $e){
            logger()->error('◆システム管理メニュー、学校の新規作成でエラー');
            logger()->error($e);
            abort(500);
        }
        //リダイレクト　連打で多重送信防止のため
        return redirect('/school-info');
    }

    /**
     * ★システム管理メニュー　学校の削除
     * Route::post
     * 【DBで実装】学校を削除すると先生生徒が削除される。ユーザーが削除されるとベストスコアが削除。（スコア履歴は残る）
     * 【このdelete処理の中で削除】学校の学年、組、クラス、追加コースのコース、course_word（中間テーブル）、ワードテーブルも削除される。 
     * @return redirect('/school-info')
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- SchoolInfoController@delete');
        $id = $request->id;
        $school = School::where('id',$id)->first();
        //dd($id);
        
        try{

            if($school->contest>0){
                DB::beginTransaction(); 
                    //(1)コースの単語を削除
                    Word::leftJoin('course_word','words.id','course_word.word_id')
                        ->leftjoin('courses','course_word.course_id','courses.id')
                        ->where('courses.school_id', $id)->delete();
                    //(2)単語とコースをつなぐ中間テーブル（course_word）を削除
                    CourseWord::leftjoin('courses','course_word.course_id','courses.id')
                        ->leftJoin('words','course_word.word_id','words.id')
                        ->where('courses.school_id',$id)
                        ->delete();
                    //(3)コースを削除
                    Course::where('school_id', $id)->delete();
                    //(4)コンテスト用スコア履歴を削除
                    ScoreHistoryContest::where('school_int_id', $id)->delete();
                    //(5)コンテスト用生徒を削除
                    Conuser::where('school_int_id', $id)->delete();
                    //(6)学校を削除⇒DB側でユーザー（コンテスト用生徒でなく、学校管理者     
                    School::destroy($id);                
                DB::commit();
            } else{
                DB::beginTransaction(); 
                    //(1)コースの単語を削除
                    Word::leftJoin('course_word','words.id','course_word.word_id')
                        ->leftjoin('courses','course_word.course_id','courses.id')
                        ->where('courses.school_id', $id)->delete();
                    //(2)単語とコースをつなぐ中間テーブル（course_word）を削除
                    CourseWord::leftjoin('courses','course_word.course_id','courses.id')
                        ->leftJoin('words','course_word.word_id','words.id')
                        ->where('courses.school_id',$id)
                        ->delete();
                    //(3)コースを削除
                    Course::where('school_id', $id)->delete();
                    //(4)学校を削除⇒DB側でユーザーが削除される    ※ユーザーを削除しないと学年、組が削除できない     
                    School::destroy($id);
                    //(5)学年を削除
                    Grade::where('school_id',$id)->delete();
                    //(6)組を削除
                    Kumi::where('school_id',$id)->delete();
                    //(7)クラスを削除
                    Clas::where('school_id',$id)->delete();
                    //(8)ベストスコアを削除（履歴は月次処理で削除されるため、ここでは削除しない）
                    BestScore::where('school_id',$id)->delete();
                DB::commit();
            }         
            session()->flash('flash_message', $school -> name.'を削除しました。（学校、ユーザー、学年、組、クラス、ベストスコアを削除、追加コース（単語）、スコア履歴は残っています）');
        }catch(Exception $e){
            DB::rollBack();
            logger()->error('◆システム管理メニュー、学校の削除');
            logger()->error($e);
            abort(500);
        }
        //学校一覧ページへリダイレクト
        return redirect('/school-info');
    }
}
