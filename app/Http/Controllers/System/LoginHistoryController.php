<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LoginLog;
use App\Exports\Export;
use DateTime;

    /**
     * システム管理メニュー　＞　ログイン履歴
     * ログイン履歴は、他のテーブルに依存しない。ログインテーブルで完結する（ユーザーが削除されてもログがの残るようにするため）
     * (1)ログイン履歴一覧
     * (2)検索＆エクセル出力
     */
$pagenation = 200;
class LoginHistoryController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- LoginHistoryController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★ログイン履歴一覧
     * ※他のテーブルに依存しないように、ログインテーブルで完結する（ユーザーを削除して、データが消えるのを防ぐ）
     * @return  view('system.loginhistory')
     */
    public function index()
    {
        logger()->debug('<-- /systemLoginhistoryController@index');
        global $pagenation;
        
        //検索ボックス初期値
        $startDateTime = null;
        $endDateTime = null;
        $school_name = null;
        $user_id = null;
        $user_name = null;
        $ipaddress = null;
        $chkSystem = true;
        $chkSchool = true;
        $chkTeacher = true;

        //ログイン履歴データ
        $login_logs = LoginLog::orderBy('login_logs.created_at','DESC')
            ->paginate($pagenation);

        //リターンview
        return view('system.loginhistory',compact('school_name','login_logs','ipaddress','user_name','user_id',
            'chkSystem','chkSchool','chkTeacher','startDateTime','endDateTime'));
    }
    /**
     * ★検索＆エクセル出力　※ボタンで分岐
     * @return  view('system.loginhistory')　⇒検索ボタン押下
     * @return \Excel::download　⇒エクセル出力ボタン押下
     */
    public function search(Request $request)
    {
        logger()->debug('<-- system/LoginhistoryController@search');
        global $pagenation;

        //検索値を変数にセット
        $startDateTime = $request->input('start_date');
        $endDateTime = $request->input('end_date');
        $user_id = $request->input('user_id');
        $user_name = $request->input('user_name');
        $ipaddress = $request->input('ipaddress');
        $school_name = $request->input('school_name');
        $chkSystem = $request->input('chkSystem');
        $chkSchool = $request->input('chkSchool');
        $chkTeacher = $request->input('chkTeacher');

        //システム管理者、学校管理者、先生のチェックを配列にいれる（SQLに入れるため）
        $chk=null;     //初期化
        if( $chkSystem){
        $chk[]= 'システム管理者';
        }
        if($chkSchool){
        $chk[] = '学校管理者';
        }
        if($chkTeacher){
        $chk[] = '先生';
        }
        //dd($chk);
        
        //終了日を+１する理由⇒SQLで「　>$endDate」 にするとその日が含まれない（1/5を設定すると1/4まで対象、1/5を含ませるために+1）
        $endDateTimeTmp = new DateTime($request->input('end_date'));
        $endDateTimeTmp->modify('+1 day'); 
                
        //検索後のログイン履歴情報　※クエリのみ
        $login_logs = LoginLog::when($startDateTime != '' ,function($q) use($startDateTime){
                return $q->where('login_logs.created_at','>',$startDateTime);
            })
            ->when($endDateTime != '' ,function($q) use($endDateTimeTmp){
                return $q->where('login_logs.created_at','<',$endDateTimeTmp);
            })
            //システム管理者、学校管理者、先生のチェック
            ->when($chk !== null,function($q) use($chk){
                return $q->whereIn('login_logs.role',$chk);
            })
            ->when($ipaddress != '' || $ipaddress != null ,function($q) use($ipaddress){
                return $q->where('login_logs.ipaddress','Like',"%$ipaddress%");
            })
            ->when($user_name != '' || $user_name != null ,function($q) use($user_name){
                return $q->where('login_logs.login_username','Like binary',"%$user_name%");
            })
            ->when($user_id != '' || $user_id != null ,function($q) use($user_id){
                return $q->where('login_logs.login_user_id','=',"$user_id");
            })
            ->when($school_name != '' || $school_name != null ,function($q) use($school_name){
                return $q->where('login_logs.login_schoolname','Like binary',"%$school_name%");
            })
            ->orderBy('login_logs.created_at','DESC');           

        //ここからボタンで分岐　※エクセル出力ボタン押下⇒ページネーションにかかわらず出力
        if($request->has('excel')){
            $login_logs  = $login_logs ->get();
            //リターンviewはエクセル出力
            $view = view('system.loginhistoryexcel',compact('login_logs'));
            return \Excel::download(new Export($view), '【管理】システム管理ログイン履歴データ'.date('Y-m-d-H-m-s').'.xlsx');

        }else{
        //検索ボタン押下時（ページネーション）
            $login_logs  = $login_logs ->paginate($pagenation);
            //リターンview
            return view('system.loginhistory',compact('school_name','login_logs','ipaddress','user_name','user_id',
                'chkSystem','chkSchool','chkTeacher','startDateTime','endDateTime'));
        }
    }
}
