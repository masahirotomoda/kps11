<?php

namespace App\Http\Controllers\system;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Kumi;
use App\Models\School;
use App\Models\User;
use Exception;



    /**
     * システム管理メニュー　＞　【学校別】組設定
     * (1)学校選択
     * (2)組一覧表示
     * (3)検索
     * (4)新規＆更新
     * (5)削除
     */
class Kumi2Controller extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- Kumi2Controller@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
    }
    /**
     * ★学校選択
     * Route::get
     * @return view('system.kumi')
     */
    public function schoolSelect(Request $request)
    {
        logger()->debug('<-- Kumi2Controller@schoolSelect');
        //学校のセレクトボックス値（メソッドはモデルで定義）
        $school_search_selection = School::selectList();
        natcasesort($school_search_selection); //より自然にソート昇順　第1５小学校等、数字と文字が混在している時に希望の挙動
        $selected_school_int_id = null; //初期値
        return view('system.selectschool',compact('school_search_selection','selected_school_int_id'));
    }   
    /**
     * ★学年一覧
     * Route::post
     * @return view('system.kumi2')
     */
    public function index(Request $request)
    {
        logger()->debug('<-- Kumi2Controller@index');
        //dd($request->all());
        //選んだ学校のID
        //前ページの学校選択なら「$request->select_school_id」、その他(hiddenで持つ）「$request->hidden_school_id」
        if($request->hidden_school_id){
            $selected_school_int_id = $request->hidden_school_id;
        } else {
            $selected_school_int_id = $request->select_school_id;
        }
        //学校情報（ページ上部に表示）
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //ユーザデータ
        $users = User::leftJoin('classes','class_id','classes.id')
            ->where('classes.school_id','=',$selected_school_int_id)
            ->select('classes.kumi_id')
            ->get();

        //組データ
        $kumi = Kumi::leftJoin('schools','kumi.school_id','schools.id')
            ->where('kumi.school_id','=',$selected_school_int_id)
            ->select('schools.name as school_name','kumi.name as kumi_name','schools.school_id as sc_id',
                'kumi.id as kumi_id','schools.id as school_id','kumi.*')
            ->orderByRaw('CHAR_LENGTH(kumi.name) ASC,kumi.name ASC') 
            ->get();
            

        //学年の数（ページネーションがないため、表示数を手動で設定）
        $current_kumi_num=$kumi->count();
        $kumi_num=$current_kumi_num;
        //dd($current_kumi_num);
        //リターンview
        return view('system.kumi2',compact('users','kumi','school_name','school','current_kumi_num','kumi_num'));
    }    
    /**
     * ★新規＆更新　※ボタンで分岐　　※同一学校で重複した学年を作るとエラー（DBで処理）
     * Route::post 
     * @return return view('system.kumi2')
     */
    public function save(Request $request)
    {
        logger()->debug('<-- Kumi2Controller@save');
        //dd($request->all());
        //選んだ学校のID
        //前ページの学校選択なら「$request->select_school_id」、その他(hiddenで持つ）「$request->hidden_school_id」
        if($request->hidden_school_id){
            $selected_school_int_id = $request->hidden_school_id;
        } else {
            $selected_school_int_id = $request->select_school_id;
        }
        
        //【ここから分岐】編集の保存ボタン押下時
        if($request->has('save')){
            $validator = Validator::make($request->all(), [
                'kumi_name' => ['string','max:10','required'],
            ]);
            if ($validator->fails()) {
                //成功メッセージをださないため
                session()->flash('flash_message', null);
                //リダイレクトの代わりにindexメソッド 【重要】バリデーションメッセージがindexに引き継がれないため、メッセージは手動
                return $this -> index($request)        
                    ->withErrors($validator);
            }

            $kumi = Kumi::find($request->id);
            //更新データをセット
            $kumi->name = mb_convert_kana($request->kumi_name,'n');

        //新規ボタン押下時
        }elseif($request->has('update')){
            //dd($request->all());
            $validator = Validator::make($request->all(), [
                'kumi_name_new' => ['string','max:10','required'],
            ]);
            if ($validator->fails()) {
                //成功メッセージをださないため
                session()->flash('flash_message', null);
                //リダイレクトの代わりにindexメソッド 【重要】バリデーションメッセージがindexに引き継がれないため、メッセージは手動
                return $this -> index($request)        
                    ->withErrors($validator);
            }

            //新規データをセット
            $kumi = new Kumi();
            $kumi->name = mb_convert_kana($request->kumi_name_new,'n');
            $kumi->school_id = $selected_school_int_id;
            //dd($kumi);
        }
        //【ここから共通処理】バリデーション失敗時

        try{
            $kumi->save();
            session()->flash('flash_message', '組を追加or更新しました');
        }catch(Exception $e){
            //dd($e);
            
            if($e->errorInfo[1] === 1062 ||$e->errorInfo[1] !== null){
                logger()->info('The kumi you tried to create a new one is a duplicate.');
                //リダイレクトの代わりにindexメソッド
                return $this->index($request)
                    ->withErrors(['message' => '同じ名前の組が既にあります']);
            }else{
                //それ以外はエラーページへ
                logger()->error('The kumi save has unknown error.');
                logger()->error($e);
                abort(500);
            }
        }
        //リダイレクトの代わりにindexメソッド
        return $this -> index($request);
    }

    /**
     * ★削除　※クラスが存在すると削除できない （DBで処理）
     * Route:post
     * @return return view('system.kumi2')
     */
    public function delete(Request $request)
    {
        global $pagenation;
        logger()->debug('<-- Kumi2Controller@delete');

        //削除チェックした学年IDが$checksに配列で格納
        $checks = $request->checks;
        //削除
        if($checks != null ){
            DB::beginTransaction();
            foreach($checks as $check){
                try{
                    logger()->debug(intval($check));
                    Kumi::destroy(intval($check));
                }catch(Exception $e){
                    if($e->errorInfo[1] === 1451){      //外部キー違反
                        logger()->info('The kumi cant destroy,cause alredy in use classes.');
                        DB::rollBack();
                            //リダイレクトの代わりにindexメソッド
                            return $this->index($request)
                            ->withErrors(['message' => '選択された組は既にクラスで使用されているため削除できません']);
                        DB::rollBack();
                    }else{
                        DB::rollBack();
                        logger()->error('The kumi destroy unknown error.');
                        logger()->error($e);
                        abort(500);
                    }
                }
            }
            DB::commit();
            session()->flash('flash_message', '学年を削除しました');
        }
        
        //リダイレクトの代わりにindexメソッド
        return $this->index($request);
    }
}