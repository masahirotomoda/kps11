<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Trial;
use App\Models\Clas;
use App\Models\Kumi;
use App\Models\User;
use DateTime;
use Exception;

/**
 * システム管理メニュー　＞　トライアル管理　※ページネーションあり
 * ※トライアル一覧に「学校編集ボタン」「ユーザー編集ボタン」あり。ユーザー編集ボタンはtrialinfousercontrollerをつかう
 * (1)トライアル一覧
 * (2)検索
 * (3)学校編集画面へ遷移（学校編集ボタン押下）
 * (4)更新
 * (5)削除
 */
$pagenation = 200;
class TrialinfoController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- system/TrialinfoController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★トライアル一覧
     *  Route::get
     * @return view('system.trialinfo')
     */
    public function index()
    {
        logger()->debug('<-- system/TrialinfoController@index');
        global $pagenation;

        //検索ボックス初期値
        $s_trial_name = null;
        $s_teachers_name = null;
        //期限切れか有効化のセレクトボックス値と初期値
        $valid_selection = array(null => "有効？期限切れ？","有効" => "有効","期限切れ" => "期限切れ",);
        $valid_select = null;
        //カレンダーの開始日、終了日
        $startDateTime = null;
        $endDateTime = null;

        //ユーザーデータ（各トライアルのユーザー数を表示するため）
        $users = User::where('users.school_int_id','=',config('configrations.TRIAL_SCHOOL_NO'))->get();

        //トライアル一覧データ
        $trials = Trial::leftJoin('schools','trials.school_id','schools.id')
            ->leftJoin('classes','trials.class_id','classes.id')
            ->leftJoin('grade','classes.grade_id','grade.id')
            ->leftJoin('kumi','classes.kumi_id','kumi.id')
            ->select('trials.name as trial_name','schools.id as school_int_id','schools.school_id as sc_id','schools.name as school_name','kumi.name as kumi_name','trials.*','classes.kumi_id as kumi_id','classes.grade_id as grade_id','grade.name as grade_name','classes.id as class_id' )
            ->orderBy('end_of_trial','ASC')
            ->paginate($pagenation);

            //dd($users);
    //リターンview
        return view('system.trialinfo',compact('users','trials','s_trial_name','s_teachers_name','startDateTime','endDateTime','valid_select','valid_selection'));
    }
    /**
     * ★検索
     * Route::match(['get', 'post']
     * @return view('system.trialinfo')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- system/TrialinfoController@search');
        global $pagenation;

        //期限切れか有効化のセレクトボックス値値
        $valid_selection = array(null => "有効？期限切れ？","有効" => "有効","期限切れ" => "期限切れ",);
        
        //dd($request->all());
        //検索値を変数に代入(input の第二引数は、第一引数が空の時につかう値)
        $s_trial_name = $request->input('s_trial_name',null);
        $s_teachers_name = $request->input('s_teachers_name',null);
        $startDateTime = $request->input('start_date',null);
        $endDateTime = $request->input('end_date',null);
        $valid_select =$request->input('valid_date',null);

        //終了日を+１する理由⇒SQLで「　>$endDate」 にするとその日が含まれない（1/5を設定すると1/4まで対象、1/5を含ませるために+1）
        $endDateTimeTmp = new DateTime($request->input('end_date'));
        $endDateTimeTmp->modify('+1 day'); 

        //有効？期限切れかのセレクトボックスの日付　（マイナス1日にしないと、今日を含んでしまうため）
        $nextday = new DateTime('-1 day');
        $beforeday = new DateTime('-1 day');

        //ユーザーデータ（各トライアルのユーザー数を表示するため）
        $users = User::where('users.school_int_id','=',config('configrations.TRIAL_SCHOOL_NO'))->get();

        //検索後のコースデータ
        $trials_data = Trial::leftJoin('schools','trials.school_id','schools.id')
            ->leftJoin('classes','trials.class_id','classes.id')
            ->leftJoin('kumi','classes.kumi_id','kumi.id')
            //トライアル学校NOはconfigrationsで管理
            ->where('trials.school_id','=',config('configrations.TRIAL_SCHOOL_NO'))
            //学校名検索
            ->when($s_trial_name !== null ,function($q) use($s_trial_name){
                return $q->where('trials.name','Like binary',"%$s_trial_name%");
            })
            //先生名検索
            ->when($s_teachers_name !== null ,function($q) use($s_teachers_name){
                return $q->where('trials.teachers_name','Like binary',"%$s_teachers_name%");
            })
            //有効か期限切れかのセレクトボックス検索
            ->when($valid_select == '有効',function($q) use($nextday){
                return $q->where('trials.end_of_trial','>',$nextday);
            })
            ->when($valid_select == '期限切れ',function($q) use($beforeday){
                return $q->where('trials.end_of_trial','<',$beforeday);
            })
            //カレンダーの開始日、終了日
            ->when($startDateTime != '' ,function($q) use($startDateTime){
                return $q->where('trials.end_of_trial','>',$startDateTime);
            })
            ->when($endDateTime != '' ,function($q) use($endDateTimeTmp){
                return $q->where('trials.end_of_trial','<',$endDateTimeTmp);
            })
            ->select('trials.school_id as school_int_id','schools.school_id as sc_id','schools.name as school_name','kumi.name as kumi_name','trials.*','classes.kumi_id as kumi_id' );
            
        //並べ替えボタン   
        if($request->has('order_mousikomi_asc')){      //申込昇順
            $trials = $trials_data->orderBy('created_at','ASC')
            ->paginate($pagenation);
        } elseif($request->has('order_mousikomi_desc')){      //申込降順
            $trials = $trials_data->orderBy('created_at','DESC')
            ->paginate($pagenation);
        } elseif($request->has('order_endday_desc')){      //トライアル期間終了日の降順
            $trials = $trials_data->orderBy('created_at','DESC')
            ->paginate($pagenation);
        } elseif($request->has('order_class_asc')){      //クラス昇順
            $trials = $trials_data->orderBy('created_at','DESC')
            ->paginate($pagenation);
        } elseif($request->has('order_class_desc')){      //申込降順
            $trials = $trials_data->orderBy('created_at','DESC')
            ->paginate($pagenation);
        } else{      //既定値は、トライアル期間終了日の降順
            $trials = $trials = $trials_data->orderBy('end_of_trial','ASC')
            ->paginate($pagenation);
        }

        //リターンview
        return view('system.trialinfo',compact('users','trials','s_trial_name','s_teachers_name','startDateTime','endDateTime','valid_select','valid_selection','beforeday'));
    }
    /**
     * ★学校編集画面へ遷移
     * Route::post
     * システム管理なので、バリデーション省略　ゆるい作りにしてある
     * @return view('system.trialinfo')
     */

    public function edit(Request $request)
    {
        logger()->debug('<-- system/TrialinfoController@edit');
        global $pagenation;
        //dd($request->all());

        $user_trial_flg_on = null;
        $user_trial_flg_off = null;

        //編集ボタンを押したトライアル学校のid（hiddenで取得）
        $id = $request ->input('edit_trial_id');

        //トライアル学校データ⇒1件なのでfirst()
        $trial = Trial::leftJoin('schools','trials.school_id','schools.id')
            ->leftJoin('classes','trials.class_id','classes.id')
            ->leftJoin('kumi','classes.kumi_id','kumi.id')
            ->where('trials.id','=',$id)
            ->select('trials.name as trial_name','schools.id as school_int_id','schools.school_id as sc_id','schools.name as school_name','kumi.name as kumi_name','trials.*','classes.kumi_id as kumi_id','classes.id as class_id' )
            ->first();
        
        //リターンview
        return view('system.trialinfoedit',compact('trial'));
    }
    /**
     * ★更新
     * Route::post
     * システム管理なので、バリデーション省略　更新後リダイレクト
     * @return redirect('/trialinfo')
     */

    public function update(Request $request)
    {
        logger()->debug('<-- system/TrialinfoController@update');
        global $pagenation;
       //dd($request->all());

        //トライアル学校データ
        $trialdata = Trial::find($request->id);

        //リクエスト値をセットして保存
        $trial = $trialdata->fill($request->all())->save();
        session()->flash('flash_message', $trialdata -> name.'の学校情報を更新しました');

        //リダイレクト
        return redirect('/trialinfo');
    }
    /**
     * ★削除
     * Route::post
     * トライアル学校削除⇒【重要】プログラムで自動削除(1)組削除(2)クラス削除　※トライアルは学年は「なし」
     * 　　　　　　　　　　ユーザは手動で削除（ユーザ編集ボタンでユーザー編集画面の削除ボタン押下）
     * @return redirect('/school-info')
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- system/TrialinfoController@delete');
        //dd($request->all());
        //削除するトライアルクラスデータ
        $id = $request->input('id');
        $trial = Trial::find($id);

        //トライアルのクラスID
        $trial_class_id = $trial->class_id;

        //トライアルの組ID
        $clas_info = Clas::find($trial_class_id);
        $trial_kumi_id = $clas_info->kumi_id;

        try{
            DB::beginTransaction();
            Kumi::destroy($trial_kumi_id);
            Clas::destroy($trial_class_id);
            Trial::destroy($id);
            DB::commit();
            session()->flash('flash_message', $trial->name.'を削除しました ※組とクラスも削除しました。');
        }catch(Exception $e){
            DB::rollBack();
            logger()->error('◆システム管理メニュー、トライアル（組、クラスも自動削除）の削除');
            logger()->error($e);
            abort(500);
        }
        //トライアル一覧へリダイレクト
        return redirect('/trialinfo');
    }
}
