<?php

namespace App\Http\Controllers\system;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\School;
use App\Models\BestScore;
use App\Exports\Export;
//赤線がでるが、PDFでつかうので必要
use Barryvdh\DomPDF\Facade\Pdf;
use Maatwebsite\Excel\Facades\Excel;

/**
* システムメニュー　生徒別ベストスコア、エクセル出力、PDF印刷
*(1)ベストスコア一覧
*(2)検索＆エクセル出力
*(3)PDF印刷
*/
$pagenation = 200;
class BestScoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        logger()->debug('<-- system/BestScoreController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }

    /**
     * ★ベストスコア一覧(先生の成績も見えるが、初期値は生徒のみ)
     * 【重要】ベストスコアテーブルにはschool_id(int_id)カラムがあるが、それは使わずに、usersから学校IDを取得すること！
     *       ⇒トライアル学校から学校に昇格したときに、学校IDが引き継がれないため。
     * ※先生メニューのベストスコアは生徒のみ
     * ※日付書式はコントローラーでできなかったのでViewで実装
     *
     * @return  view('bestscore')
     */
    public function index()
    {
        logger()->debug('<-- sustem/BestScoreController@index');
        global $pagenation;

        $school = School::where('id','=',auth()->user()->school_int_id)->first();

        //検索ボックスの初期値
        $course_name = null;
        $user_name = null;
        $grade_name = null;
        $kumi_name = null;
        $school_name = null;
        $name = null;

        //検索　ロールのチェックボックス
        $school_adm = false;
        $teacher = false;
        $student = true; //初期値は生徒のみチェック

         //ベストスコア情報（
         $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
            ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
            ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
            ->leftJoin('classes','classes.id','=','users.class_id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->leftJoin('schools','users.school_int_id','=','schools.id')
            ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
                'schools.name as school_name','users.id as user_id','courses.id as course_id',
                'course_with_bestpoint.best_point_created_at as b_created_at','course_with_bestpoint.id as bestpoint_id','users.*',
                'course_with_bestpoint.best_point')
            ->groupBy('course_with_bestpoint.course_id')
            ->groupBy('users.id')
            ->where('users.role','=','生徒')
            ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')
            ->paginate($pagenation);

         //dd($bestScore);

        return view('system.bestscore',compact('bestScore','school','user_name','course_name','grade_name','kumi_name','name','school_name','school_adm','teacher','student'));
    }

    /**
     * ★検索「学年」「組」「生徒の一部」「コース名の一部」
     *  ※ボタンで分岐　(1)検索ボタン押下　(2)エクセル出力ボタン押下
     * @return view('system.bestscore')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- (School)BestScoreController@search');

        global $pagenation;
        $school = School::where('id','=',auth()->user()->school_int_id)->select('name','id')->first();

        //検索値を変数にセット　ロールのチェックボックス
        $school_adm = $request->input('school_adm');
        $teacher = $request->input('teacher');
        $student = $request->input('student');

        //学校管理者、先生、生徒にチェックがあるものを$selectRoleの配列にいれる⇒SQLの検索条件を配列に入れる
        $selectRole = [];
        if($school_adm !== null){
            array_push($selectRole,'学校管理者');
        }
        if($teacher !== null){
            array_push($selectRole,'先生');
        }
        if($student !== null){
            array_push($selectRole,'生徒');
        }
        //検索値を変数にセット
        $course_name = $request->input('course_name',null);
        $grade_name = $request->input('grade',null);
        $kumi_name = $request->input('kumi',null);
        $name = $request->input('name',null);
        $school_name = $request->input('school_name',null);

        //ベストスコアデータ　※クエリのみ
        $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
            ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
            ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
            ->leftJoin('classes','classes.id','=','users.class_id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->leftJoin('schools','users.school_int_id','=','schools.id')
            ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
                'schools.name as school_name','users.id as user_id','courses.id as course_id',
                'course_with_bestpoint.best_point_created_at as b_created_at','course_with_bestpoint.id as bestpoint_id','users.*',
                'course_with_bestpoint.best_point')
            ->groupBy('course_with_bestpoint.course_id')
            ->groupBy('users.id')
            ->whereIn( 'users.role', $selectRole )
            ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')

        ->when($grade_name != '' || $grade_name != null ,function($q) use($grade_name){
            return $q->where('grade.name','=',$grade_name);
        })
        ->when($kumi_name != '' || $kumi_name != null ,function($q) use($kumi_name){
            return $q->where('kumi.name','=',$kumi_name);
        })
        ->when($name !== null ,function($q) use($name){
            return $q->where('users.name','Like binary',"%$name%");
        })
        ->when($course_name !== null ,function($q) use($course_name){
            return $q->where('courses.course_name','Like binary',"%$course_name%");
        })
        ->when($school_name !== null ,function($q) use($school_name){
            return $q->where('schools.name','Like binary',"%$school_name%");
        });

        //ここから分岐 エクセル出力ボタン押下
        if($request->has('excel')){
            $bestScore = $bestScore -> get();
            if ($bestScore->isEmpty()) {
                return redirect('/bestscore')
                ->withErrors('エクセルに出力するデータがありません。検索して、データを表示させてから「エクセル出力」ボタンを押してください');
            }

            //リターンview　エクセル出力
            $view = view('system.bestscoreexcel',compact('bestScore'));
            return Excel::download(new Export($view), '【管理】ベストスコアデータ'.date('Y-m-d-H-i-s').'.xlsx');

        } elseif($request->has('search') || $request->has('page')){
            $bestScore = $bestScore -> paginate($pagenation);

            //リターンview
            return view('system.bestscore',compact('bestScore','school_name','course_name','grade_name','kumi_name','school_adm','teacher','student','name','school_name','course_name'));

        } else {
            return redirect('/bestscore');
        }
    }
    /**
     * ★記録証PDF出力
     * @param Request $request
     * @return   $pdf->stream()
     */
    public function pdf(Request $request){
        logger()->debug('<-- TranscriptPrintController@pdf');

        //印刷チェックがあれば（$request['checks']は、チェックしているscorehistoryのIDが配列で格納）
        if(isset($request['checks'])){
            $school = School::where('id','=',auth()->user()->school_int_id)->select('name','id','trial')->first();
            $bestScore = BestScore::where('course_with_bestpoint.bp_rank',1)
                ->leftjoin('users','course_with_bestpoint.user_id','=','users.id')
                ->leftJoin('courses','courses.id','=','course_with_bestpoint.course_id')
                ->leftJoin('classes','classes.id','=','users.class_id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                ->select('courses.course_name as course_name','kumi.name as kumi_name','grade.name as grade_name','users.name as user_name','courses.course_category as course_category ',
                    'course_with_bestpoint.best_point_created_at as b_created_at','course_with_bestpoint.rank','course_with_bestpoint.id as bestpoint_id','users.*',
                    'course_with_bestpoint.best_point')
                ->groupBy('course_with_bestpoint.course_id')
                ->groupBy('users.id')
                ->whereIn('course_with_bestpoint.id',$request->checks)
                ->orderByRaw('grade.name ASC , kumi.name ASC, users.attendance_no ASC ,course_name ASC')
                ->get();
                //dd($bestScore);
            //pdf1（シンプル記録証）ボタン押下
            if($request->has('pdf1')){
                $pdf = PDF::loadView('pdf.transcript_bestscore_simple',compact('bestScore','school'));

            }else{
            //pdf1（カラフル記録証）ボタン押下
                $pdf = PDF::loadView('pdf.transcript_bestscore_colorful',compact('bestScore','school'));
            }
            return $pdf->stream();
        } else{
            //印刷チェックが1つもない場合、エラーメッセージでリダイレクト
            return redirect('/bestscore')
                ->withErrors('【印刷できません】印刷したいデータにチェック（表の一番左の項目）してからボタンを押してください');
        }
    }
}
