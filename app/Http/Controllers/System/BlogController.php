<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Blog;
use Exception;

/**
 * システム管理メニュー　＞　ブログ管理　※独自のページネーション数
 * (1)ブログ一覧表示 
 * (2)検索
 * (3)新規＆編集画面へ遷移
 * (4)新規＆更新＆削除
 */
class BlogController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- BlogController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
    }
    /**
     * ★ブログ一覧　※ページネーションは100件【理由】画像があるため、200件だと表示に時間がかかるため。
     * Route::get
     * エディターのウィジットは「trix」https://github.com/basecamp/trix#built-for-the-modern-web
     * @return vview('system.blog')
     */
    public function index()
    {
        logger()->debug('<-- BlogController@index');
        //検索ボックス初期値
        $search_box_title = null;
        $search_box_article = null;
        //ブログデータ
        $blogs = Blog::orderby('created_at','DESC')->paginate(100);
        //リターンview
        return view('system.blog',compact('blogs','search_box_title','search_box_article'));
    }
    /**
     * ★検索　「記事とタイトル」
     * Route::match(['get', 'post']
     * @return view('system.blog')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- BlogController@index');
        
        //検索値を変数にセット
        $search_box_title = $request -> input('search_box_title',null);
        $search_box_article = $request -> input('search_box_article',null);

        //検索結果後データ タイトルと記事を含めたいが未実装　現在は記事のみ
        $blogs = Blog::orderby('created_at','DESC')
            ->when($search_box_title !== null ,function($q) use($search_box_title){
                return $q->where('title','Like',"%$search_box_title%");
            })
            ->when($search_box_article !== null ,function($q) use($search_box_article){
                return $q->where('article','Like',"%$search_box_article%");
            })
            ->paginate(100);

        //リターンview
        return view('system.blog',compact('blogs','search_box_title','search_box_article'));
    }
    /**
     * ★編集画面（エディター）へ遷移　※新規と更新で分岐
     * 新規ボタンと更新ボタンは別formだが、URLは同じなので同じメソッド
     * Route::post
     * @return view('system.blogedit')
     */
    public function edit(Request $request)
    {
        logger()->debug('<-- BlogController@post');
    //dd($request->all());
        //更新ボタン押下
        if($request -> has('edit')){
            //編集中のブログデータ取得
            $blog = Blog::find($request->input('id'));

            //公開、非公開をtoggleボタンに持たせる
            if($blog->private === 0){
                $open = 'checked';
                $close = false;
            }else{
                $open = false;
                $close = 'checked';
            }
        //新規ボタン押下
        } elseif($request -> has('add')){
            $blog = new Blog();
            //toggleボタン初期値は「非公開」
            $open = false;
            $close = 'checked';
        }
        $img_delete = null;

        //ここから共通 リターンview
        return view('system.blogedit',compact('blog','open','close','img_delete'));
    }
    /**
     * ★新規＆更新＆削除 ※削除もあるので注意！　ブログidがあれば更新、なければ新規
     * trixエディタ使用　https://github.com/basecamp/trix
     * laraveのバリデーションがエディタのせい（？）で効かないので個別バリデーション
     * Route::post
     * @return return redirect('/bloglist')
     */
    public function save(Request $request)
    {
        logger()->debug('<-- BlogController@save');
        //dd($request->all());
        
        //削除ボタン押下時（モーダルの削除ボタン）
        if ($request -> has('modal_del_btn')){
            try{       
                Blog::destroy($request -> input('id'));
                session()->flash('flash_message', 'ブログを削除しました');
            }catch(Exception $e){
                logger()->error('◆システム管理メニュー、ブログ削除でエラー');
                logger()->error($e);
                abort(500);
            }

            //リターンview
            return redirect('/bloglist');

        //新規の場合
        } elseif ($request -> id  === null){
            $blog = new Blog();
            $flashmessage = '【新規登録しました】公開するには「公開ボタン」を押してください';
        } else {
        //更新の場合
        $blog = Blog::find($request->input('id'));
        $flashmessage = '【更新しました】作成日（降順）⇒一番上に表示されません';
        }

        //新規と更新用の空欄のバリデーション 【重要】editorをつかっているため、通常のvalidationではきかない
        if($request->title === null || $request->article === null){
            //リターンview
            return redirect('/bloglist')
                ->withErrors('タイトルと本文、両方入力しててください');
        }
        //アップロードファイルのバリデーション、ファイルが存在したら、画像拡張子のチェック
        //ファイル名　$request->file('file')->guessClientExtension()
        //拡張子　file('file')->getClientOriginalName()
        if($request -> file !== null){
            $file_kakuchoushi = $request->file('file')->guessClientExtension();
            if ($file_kakuchoushi =='png' || $file_kakuchoushi =='jpg' || $file_kakuchoushi =='jpeg' || $file_kakuchoushi =='gif'){
            //リターンview
            }else{
            return redirect('/bloglist')
                ->withErrors('画像ファイル(png,jpeg,jpg.gif)をアップロードしてください。');
            }
        }


        //ここから共通処理、データをセット
        //toggleボタン　value値が「公開」「非公開」、これを「private」カラム（boolean）に変換
        if($request->private == '公開'){
            $blog->private = 0;
        }else{
            $blog->private = 1;
        }
        $blog->article = $request->article;
        $blog->title = $request->title;
        $img_delete = $request->img_delete;
        
        
        //アップロード画像があれば、画像処理(1)DBのimageカラムにファイル名をセット(2)
        DB::beginTransaction();
        try{
            if($request -> file !== null){
                //ファイル名取得
                $file_name = $request->file('file')->getClientOriginalName();
                //DBに保存するファイル名
                $blog->image = time(). $file_name; 
                $file = $request->file('file');
                // $file->storeAs('public/blogimages',$blog->image);
                //画像をストレージにアップロード
                $file->storeAs('public/blogimages',$blog->image);            
            }

            //画像削除にチェックがあれば画像名をnull　※ファイル自体は残っている
            if($request-> input('img_delete') == true){            
            $blog -> image = null;
            $flashmessage = '【更新】画像を削除して、ブログを更新しました。';
            }
            
            //保存        
            $blog->save();
            session()->flash('flash_message', $flashmessage);

        }catch(Exception $e){
            logger()->error('◆ブログ新規or更新でエラー');
            logger()->error($e);
            DB::rollBack();
            abort(500);
        }
        DB::commit();

        //リダイレクト（多重送信防止のため）
        return redirect('/bloglist');
    }
}
