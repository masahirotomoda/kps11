<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
* システム管理メニュー > 検証（チェック）
*(1)チェック一覧
*/
class CheckController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- CheckController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');    
    }
    /**
     * ★検証一覧
     * @return view('system.check')
     */
    public function index()
    {
        logger()->debug('<-- CheckController@index');
        //リターンview
        return view('system.check');
    }

    /**
     * ★エラーページ表示ボタン
     * @return abord
     * 
    */
    public function error(Request $request)
    {
        logger()->debug('<-- CheckController@error');

        if($request->has('error401')){
            abort(401); //Unauthorized
        } elseif ($request->has('error403')){
            abort(403); //Forbidden
        } elseif ($request->has('error404')){
            abort(404); //Not Found
        } elseif ($request->has('error419')){
            abort(419); //セッションタイムアウト
        } elseif ($request->has('error429')){
            abort(429); //Too Many Requests
        } elseif ($request->has('error500')){
            abort(500); //Internal Server Error
        } elseif ($request->has('error503')){
            abort(503); //Service Unavailable
        }
    }
}