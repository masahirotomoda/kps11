<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Clas;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\School;
use App\Models\User;

    /**
     * システム管理メニュー　＞　クラス設定
     * (1)クラス一覧表示
     * (2)検索
     * (3)学校を選ぶ
     * (4)新規＆更新
     * (5)クラス別生徒一覧
     * (6)削除
     */
$pagenation = 200;
class ClassInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }

    /**
     * ★クラス一覧　※学年と組をの組み合わせがクラス、生徒はクラスに紐づいている
     * Route::get
     * @return view('system.classinfo')
     */
    public function index()
    {
        global $pagenation;
        //学校を選んでいるかフラグ　⇒選んでいない
        $selected = false;

        //検索ボックス初期値
        $school_name = null;  //学校名
        $sc_id = null;  //学校ID
        
        //学校セレクトボックス値と、入力値を代入する変数
        $school_name_search = null;
        $school_search_selection = School::selectList();

        //クラスデータ
        $classes = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
            ->select('classes.id as class_id','schools.id as school_int_id','schools.name as school_name','schools.school_id as sc_id',
                'kumi.id as kumi_id','grade.id as grade_id',
                'kumi.name as kumi_name','grade.name as grade_name')
            ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
            ->leftjoin('schools','classes.school_id','=','schools.id')
            ->orderByRaw('school_name ASC,CHAR_LENGTH(grade.name) ASC,grade.name ASC') 
            ->paginate($pagenation);

        
        //リターンview
        return view('system.classinfo',
            compact('classes','selected','school_name','sc_id','school_search_selection','school_name_search'));
    }

    /**
     * ★検索「学校名」「学校ID]
     * Route::match(['get', 'post']
     * ※学校情報ページからの遷移あり(POST 学校名をパラメータとして）
     * @return view('system.classinfo')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- system/ClassInfoController@search');  
        global $pagenation;

        //成功メッセージをださないため
        session()->flash('flash_message', null);

        //学校を選んだかのフラグ
        $selected = false;
        $selected = $request->input('selected');
        if($selected === null){
            $selected = false;
        }   
        //dd($request->all());

        //検索ボックス値
        $school_name = $request->input('school_name');
        $sc_id = $request->input('school_id');

        //学校セレクトボックス値と、入力値を代入する変数
        $school_name_search = $request->input('school_name_search');
        //$school_search_selection = School::selectListByName($school_name);

        //「$school_search_selection」とは学校選択ボックスのセレクト値　⇒　学校名検索の結果（複数もあり）（例：ナレッジと検索した時、ナレッジ小、ナレッジ塾、ナレッジ中学校が表示される）
        //パラメータを学校IDに変えて、学校IDの検索結果も処理する
        //検索ボックスに学校IDと名前が入っていても学校ID検索を一番優先とする ※学校名と学校IDの両方を処理するのは次回の開発で！
        if($sc_id !== null)
            $school_search_selection = School::selectListById($sc_id);
        elseif($school_name !== null)
            $school_search_selection = School::selectListByName($school_name);
        else{
            $school_search_selection =School::selectList();
        }

       //学校セレクトボックスの選択した学校ID（school_int_id)
        $school_name_search = $request->input('school_name_search');
        //dd($sc_id);
        //検索後のクラスデータ
        $classes = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
            ->select('classes.id as class_id','schools.id as school_int_id','schools.name as school_name','schools.school_id as sc_id',
                'kumi.id as kumi_id','grade.id as grade_id',
                'kumi.name as kumi_name','grade.name as grade_name')
            ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
            ->leftjoin('schools','classes.school_id','=','schools.id')
            ->when($sc_id !== null ,function($q) use($sc_id){
                return $q->where('schools.school_id','Like',"%$sc_id%");
            })
            ->when($school_name !== null ,function($q) use($school_name){
                return $q->where('schools.name','Like binary',"%$school_name%");
            })
            ->orderByRaw('school_name ASC,CHAR_LENGTH(grade.name) ASC,grade.name ASC') 
            ->paginate($pagenation);
        
        //リターンview
        return view('system.classinfo',compact('classes','selected','school_name','sc_id','school_search_selection','school_name_search'));
    }
    /**
     * ★新規作成のために学校を選ぶ（セレクトボックスから選択）
     * Route::match(['get', 'post']
     * @return view('system.classinfo')
     */
    public function schoolSelect(Request $request)
    {
        logger()->debug('<-- system/ClassInfoController@schoolSelect');
        global $pagenation;
        
        //成功メッセージをださないため
        session()->flash('flash_message', null);

        //学校を選んだかのフラグ ⇒選んでいる
        $selected = true;
        //dd($request->all());

        //検索ボックス値
        $sc_id =  null;
        $school_name =  null;
        
        //学校セレクトボックス値（すべての学校表示）
        $school_search_selection =School::selectList();       

        //学校セレクトボックスで選んだ学校int_id (例：45)
        $school_name_search = $request->input('school_name_search');
        session()->put('school_name_search',$school_name_search);

        //学校を選んだ後、その学校に所属する学年、組のセレクトボックス値
        $grade_new_selection = Grade::selectListById($school_name_search);
        $kumi_new_selection = Kumi::selectListById($school_name_search);

        //編集ボタン押下後の学年セレクトボックスの選んだ学年IDと組
        $grade_new_select = null;
        $kumi_new_select = null;

        //学校を選んだあとのクラスデータ
        $classes = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
            ->select('classes.id as class_id','schools.id as school_int_id','schools.name as school_name','schools.school_id as sc_id',
                'kumi.id as kumi_id','grade.id as grade_id',
                'kumi.name as kumi_name','grade.name as grade_name')
            ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
            ->leftjoin('schools','classes.school_id','=','schools.id')
            ->where('schools.id',$school_name_search)
            ->orderByRaw('school_name ASC,CHAR_LENGTH(grade.name) ASC,grade.name ASC') 
            ->paginate($pagenation);

        //リターンview
        return view('system.classinfo',
            compact('classes','selected','grade_new_selection','kumi_new_selection','grade_new_select','kumi_new_select','sc_id',
            'school_name','school_name_search','school_search_selection'));

    }
    /**
     * ★新規＆更新　ボタンで分岐
     * Route::match(['get', 'post']　 
     * @return view('system.classinfo')⇒多重送信対策（リダイレクト）していない
     */
    public function save(Request $request)
    {
        logger()->debug('<-- system/ClassInfoController@new');
        global $pagenation;

        //学校を選んだかのフラグ ⇒選んでいる
        $selected = $request->input('selected');
        if($selected === null){
            $selected = false;
        }

        //検索ボックス値
        $sc_id =  null;
        $school_name =  null;
        
        //学校セレクトボックス値（すべての学校表示）
        $school_search_selection =School::selectList();       

        //学校セレクトボックスで選んだ学校int_id (例：45)
        $school_name_search = $request->input('school_name_search');
        //session()->put('school_name_search',$school_name_search);

        //学校を選んだ後、その学校に所属する学年、組のセレクトボックス値
        $grade_new_selection = Grade::selectListById($school_name_search);
        $kumi_new_selection = Kumi::selectListById($school_name_search);

        //編集ボタン押下後の学年セレクトボックスの選んだ学年IDと組
        $grade_new_select = null;
        $kumi_new_select = null;

        //【ここから分岐】編集の保存ボタン押下時
        if($request->has('save')){
            //dd($request->all());            
             $clas = Clas::find($request->input('id'));
              
         //新規登録の「保存」ボタン押下
         }elseif($request->has('update') || $request->has('page')){ 
            //dd($request->all());             
             $clas = new Clas();            
        } else {
            logger()->error('◆システム管理メニュー、クラスの新規or更新で保存ボタンを押下しない時のエラー');
            abort(500);
        } 
         //【ここから共通処理】学年と組はセレクトボックス利用なので、バリデーションなし
         //クラスデータをセット
         $clas->grade_id = $request->input('grade_id');
         $clas->kumi_id = $request->input('kumi_id');
         $clas->school_id = $request->input('school_id');
        
         //保存
        try{
            logger()->debug($clas);
            $clas->save();
            //dd( $clas->save());
            session()->flash('flash_message', 'クラスを追加or更新しました');
        }catch(\Exception $e){
            //dd($e->errorInfo);
            if($e->errorInfo[1] === 1062 ){
                //dd($e->errorInfo);
                //学校、学年、組が同じものを作ろうとしている（Unique規制違反)
                logger()->info('◆システム管理メニュー、クラスの新規作成で重複エラー');
                logger()->debug($e);

                //リターンview
                $classes = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
                    ->select('classes.id as class_id','schools.id as school_int_id','schools.name as school_name','schools.school_id as sc_id',
                        'kumi.id as kumi_id','grade.id as grade_id',
                        'kumi.name as kumi_name','grade.name as grade_name')
                    ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
                    ->leftjoin('schools','classes.school_id','=','schools.id')
                    ->where('schools.id',$request->input('school_id'))
                    ->orderBy('classes.updated_at','DESC')
                    ->paginate($pagenation);

                    //「withErrors」でエラーをはいているが、バリデーションモーダルは問題なくでる
                    return view('system.classinfo',compact('classes','selected','school_name','school_search_selection','school_name_search','sc_id'))
                        ->withErrors(['message' => '同一な学年、組のクラスは作成できません']);

                }else{
                //それ以外はエラーページへ
                logger()->error('The kumi save unknown error.');
                logger()->error($e);
                abort(500);
            }
        } //ここまでキャッチ

        //リターンviewに渡すデータ
        //学校セレクトボックスの値を、現在編集している学校にする
        $school_name_search = $request->input('school_id');

        //クラスデータ
        $classes = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
                ->select('classes.id as class_id','schools.id as school_int_id','schools.name as school_name','schools.school_id as sc_id',
                    'kumi.id as kumi_id','grade.id as grade_id',
                    'kumi.name as kumi_name','grade.name as grade_name')
                ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
                ->leftjoin('schools','classes.school_id','=','schools.id')
                ->where('schools.id',$request->input('school_id'))
                ->orderBy('classes.updated_at','DESC')
                ->paginate($pagenation);
            //多重送信対策していない（更新データが一番上表示で、ユーザビリティ優先）
            return view('system.classinfo',compact('classes','selected','school_name','school_search_selection','school_name_search','sc_id'));
        }
    /**
     * ★クラス別所属生徒一覧 
     * @return view('system.classinfo')
     * Route::post　⇒ページネーションないため
     */
    public function studentinfo(Request $request)
    {
        $class_id = $request->class_id;
        //生徒情報  （生徒一覧）    
        $users = User::where('users.role','=','生徒')
        ->where('users.class_id','=',$class_id)
        ->orderByRaw('attendance_no ASC')
        ->get();        

        //クラス情報（上の表で使用）
        $class = Clas::leftjoin('grade','classes.grade_id','=','grade.id')
        ->leftjoin('kumi','classes.kumi_id','=','kumi.id')
        ->leftjoin('schools','classes.school_id','=','schools.id')
        ->select('grade.name as grade_name','kumi.name as kumi_name','schools.name as school_name')
        ->where('classes.id','=',$class_id)
        ->first();

        //クラスの生徒数（上の表で使用）
        $student_num_byclass = $users -> count();

        //リターンview
        return view('system.classstudent',compact('users','class','student_num_byclass' ));
    }
    /**
     * ★削除
     * Route::post　⇒削除後リダイレクトなので
     * @return view('system.classinfo')
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- system/ClassInfoController@delete');
        global $pagenation;

        $checks = $request->checks;
        if($checks !== null ){
            DB::beginTransaction();
            foreach($checks as $check){
                try{
                    $selected = true;
                    Clas::destroy(intval($check));
                }catch(\Exception $e){
                    //dd($e->errorInfo[1]);
                    if($e->errorInfo[1] === 1451){      //外部キー違反
                       
                        logger()->info('The classes cant destroy,cause alredy in use.');
                        $selected = false;
                        DB::rollBack();
                        
                        DB::rollBack();
                        return redirect('/class')
                            ->withErrors(['message' => '生徒が存在するクラスは削除できません']);
                    }else{
                        DB::rollBack();
                        logger()->error('The classes destroy unknown error.');
                        logger()->error($e);
                        abort(500);
                    }
                }
                DB::commit();
                session()->flash('flash_message', 'クラスを削除しました');
            }
        }
        return redirect('/class');
    }
}