<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ScoreHistory;
use App\Models\School;
use App\Exports\Export;
//赤線がでるが、PDFでつかうので必要
use Barryvdh\DomPDF\Facade\Pdf;
use Maatwebsite\Excel\Facades\Excel;

    /**
    * システム管理メニュー　＞　スコア履歴、エクセル、PDF　
    *(1)スコア履歴一覧
    *(2)検索＆エクセル出力
    *(3)PDF印刷
    */
$pagenation = 200;
class TranscriptController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- system/TranscriptController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }

    /**
     * ★スコア履歴一覧 　※score_historyテーブルから取得（bestscoreでない）
     * Route::get ⇒ページネーションあり
     * @return  view('system.transcript')
     */
    public function index()
    {
        logger()->debug('<-- system/TranscriptController@index');
        global $pagenation;
        //$school = School::where('school_id','=',auth()->user()->school_id)->first();

        //検索ボックスの初期値
        $startDate = null;
        $startTime = null;
        $endDate = null;
        $endTime = null;
        $course_name = null;
        $user_name = null;
        $grade_name = null;
        $kumi_name = null;
        $school_name = null;
        $name = null;

        //検索　ロールのチェックボックス
        $school_adm = false;
        $teacher = false;
        $student = true;

        //スコア履歴データ
        $scoreHistory = ScoreHistory::leftjoin('users','score_history.user_id','=','users.id')
            ->leftJoin('classes','classes.id','=','users.class_id')
            ->leftJoin('grade','classes.grade_id','=','grade.id')
            ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
            ->leftJoin('schools','users.school_int_id','=','schools.id')
            ->select('kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
            'score_history.created_at as sh_created_at','users.*','score_history.*','schools.name as school_name')
            ->orderByRaw('score_history.created_at DESC')
            ->paginate($pagenation);

        //リターンview
        return view('system.transcript',compact('startDate','startTime','school_adm','teacher','student',
            'endDate','endTime','course_name','grade_name','kumi_name','scoreHistory','name','school_name'));
    }
    /**
     * * ★スコア履歴　検索＆エクセル出力
     * Route::match(['get', 'post']
     * 検索項目：「開始日時」「開始時間」「終了日時」「終了時間」「学校名」「コース名」「ロール」「学年」「組」「生徒名」
     * ※ボタンで分岐　(1)検索ボタン押下　(2)エクセル出力ボタン押下
     * @return  view('system.transcript')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- system/TranscriptController@search');
        global $pagenation;

        //検索値を変数にセット　ロールのチェックボックス
        $school_adm = $request->input('school_adm');
        $teacher = $request->input('teacher');
        $student = $request->input('student');

        //学校管理者、先生、生徒にチェックがあるものを$selectRoleの配列にいれる⇒SQLの検索条件を配列に入れる
        $selectRole = [];
        if($school_adm !== null){
            array_push($selectRole,'学校管理者');
        }
        if($teacher !== null){
            array_push($selectRole,'先生');
        }
        if($student !== null){
            array_push($selectRole,'生徒');
        }
        //検索値を変数にセット
        $course_name = $request->input('course_name',null);
        $grade_name = $request->input('grade',null);
        $kumi_name = $request->input('kumi',null);
        $name = $request->input('name',null);
        $school_name = $request->input('school_name',null);

        //検索値を変数にセット
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        //開始時間が空欄の処理
            $startTime = $request->input('start_time',null);
            if($startTime == null){
                $startTime  = '00:00:01';
        //終了時間が空欄の処理
            }
            $endTime = $request->input('end_time',null);
            if($endTime == null){
                $endTime  = '23:59:59';
            }
        //スコア履歴データ　※クエリのみ
        $scoreHistory = ScoreHistory::leftjoin('users','score_history.user_id','=','users.id')
        ->leftJoin('classes','classes.id','=','users.class_id')
        ->leftJoin('grade','classes.grade_id','=','grade.id')
        ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
        ->leftJoin('schools','users.school_int_id','=','schools.id')
        ->select('kumi.name as kumi_name','grade.name as grade_name','users.name as user_name','schools.name as school_name',
            'score_history.created_at as sh_created_at','users.*','score_history.*')
        ->whereIn( 'users.role', $selectRole )
        ->when($school_name !== null ,function($q) use($school_name){
            return $q->where('schools.name','Like binary',"%$school_name%");
        })
        ->when($grade_name !== null ,function($q) use($grade_name){
        return $q->where('grade.name','Like binary',"%$grade_name%");
        })
        ->when($kumi_name !== null ,function($q) use($kumi_name){
                return $q->where('kumi.name','Like binary',"%$kumi_name%");
        })
        ->when($name !== null ,function($q) use($name){
            return $q->where('users.name','Like binary',"%$name%");
        })
        ->when( $course_name !== null ,function($q) use( $course_name){
            return $q->where('score_history.course_name','Like binary',"%$course_name%");
        })
        //日付と時間の間に半角スペース必須
        ->when($startDate !== null ,function($q) use($startDate,$startTime){
            return $q->where('score_history.created_at','>',$startDate. ' '. $startTime);
        })
        ->when($endDate !== null ,function($q) use($endDate,$endTime){
            return $q->where('score_history.created_at','<',$endDate. ' '. $endTime);
        })
        ->orderBy('score_history.created_at','DESC');

        //ここから分岐 エクセル出力ボタン押下
        if($request->has('excel')){
            $scoreHistory = $scoreHistory -> get();

            //検索ボックスの開始時間と終了時間は空欄時、時間（00:00:01と'23:59:59）を入力しているため、viewに渡すときに空にする
            $startTime = $request->input('start_time');
            $endTime = $request->input('end_time');

            //リターンview　エクセル出力
            $view = view('system.transcriptexcel',compact('scoreHistory'));
            return Excel::download(new Export($view), '【管理】タイピング履歴データ'.date('Y-m-d-H-i-s').'.xlsx');
        
        } elseif($request->has('search') || $request->has('page')){
            $scoreHistory = $scoreHistory -> paginate($pagenation);
        
            //検索ボックスの開始時間と終了時間は空欄時、時間（00:00:01と'23:59:59）を入力しているため、viewに渡すときに空にする
            $startTime = $request->input('start_time');
            $endTime = $request->input('end_time');

            //リターンview
            return view('system.transcript',compact('startDate','startTime','school_name',
                'endDate','endTime','course_name','grade_name','kumi_name','school_adm','teacher','student','scoreHistory','name'));
    
        } else {
            return redirect('/typing-history');
        }
    }
    /**
     *★記録証PDF出力
     * PDFチェックしているデータをPDF出力　※PDFの画像はbase64に変換
     * 記録証はシンプルとカラフルの2種類
     * ライブラリ：\vendor\dompdf\dompdf\README.md　　　vendor\dompdf
     * フォント：vendor\dompdf\dompdf\lib\fonts　　CSS:vendor\dompdf\dompdf\lib\res\html.css
     * Route::post
     * @return   $pdf->stream()
     */
    public function pdf(Request $request){
        logger()->debug('<-- system/TranscriptController@pdf');

        //印刷チェックがあれば（$request['checks']は、チェックしているscorehistoryのIDが配列で格納）
        if(isset($request['checks'])){
            //PDFプリントに必要なデータ
            $select = null; //?
            $school = School::where('school_id','=',auth()->user()->school_id)->select('name','id')->first();
            $transcripts = ScoreHistory::leftjoin('users','score_history.user_id','=','users.id')
                ->leftJoin('classes','classes.id','=','users.class_id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                ->select('kumi.name as kumi_name','grade.name as grade_name','users.name as user_name',
                    'score_history.created_at as sh_created_at','users.*','score_history.*')
                ->whereIn('score_history.id',$request->checks)
                ->orderBy('score_history.created_at','DESC')
                ->get();
            
                //pdf1（シンプル記録証）ボタン押下
                if($request->has('pdf1') ){
                    $pdf = PDF::loadView('pdf.transcript_scorehistory_simple',compact('transcripts','school'));

                } elseif($request->has('pdf2')){
                //pdf1（カラフル記録証）ボタン押下
                    $pdf = PDF::loadView('pdf.transcript_scorehistory_colorful',compact('transcripts','school'));
                } else {
                    logger()->error('◆システム管理メニュー、スコア履歴PDF出力でPDFボタン押下せずエラー');
                    abort(500);
                }
                return $pdf->stream();
        } else{
        //印刷チェックが1つもない場合、エラーメッセージでリダイレクト
            //成功フラッシュメッセージが表示されるのを防ぐ
            session()->flash('flash_message', null);
            //リダイレクト
            return redirect('/typing-history')
            ->withErrors('「印刷」項目に1つもチェックがありません。印刷したいデータにチェックしてください（印刷項目は表の一番左です）');
        }
    }
}