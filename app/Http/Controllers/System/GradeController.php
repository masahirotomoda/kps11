<?php

namespace App\Http\Controllers\system;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\School;
use Exception;

    /**
     * システム管理メニュー　＞　学年設定
     * (1)学年一覧表示
     * (2)検索
     * (3)学校を選ぶ
     * (4)新規＆更新
     * (5)削除
     */
$pagenation = 200;
class GradeController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- GradeController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
   
    /**
     * ★学年一覧
     * Route::get
     * @return view('system.grade')
     */
    public function index()
    {
        global $pagenation;
        $selected = false;

        //検索ボックス初期値
        $school_name = null;  //学校名
        $school_id = null;  //学校ID

        //学校セレクトボックス値と、入力値を代入する変数
        $school_name_search = null;
        $school_search_selection = School::selectList();
        //natcasesort($school_search_selection); //より自然にソート昇順

        //学年データ
        $grades = Grade::leftJoin('schools','grade.school_id','schools.id')
            ->select('schools.name as school_name','grade.name as grade_name','schools.school_id as sc_id',
                'grade.id as grade_id','schools.id as school_id')
            //orderByだとエラーなので
            ->orderByRaw('school_name ASC ,CHAR_LENGTH(grade.name) ASC,grade.name ASC') 
            ->paginate($pagenation);

        //成功メッセージをださないため
        //session()->flash('flash_message', null);

        //リターンview
        return view('system.grade',compact('grades','school_name','school_id',
            'school_search_selection','school_name_search','selected'));
    }
    /**
     * ★学年検索　「学校名」「学校ID」⇒school_int_idでない（例）abc12345
     * Route::match(['get', 'post']
     * @return return view('system.grade')
     */
    public function search(Request $request)
    {
        global $pagenation;
        logger()->debug('<-- GradeController@search');

        //検索ボックス値
        
        $school_name = $request->input('school_name');
        $school_id = mb_convert_kana($request->school_id,'n');

        //学校セレクトボックス値と、入力値を代入する変数
        $school_name_search = $request->input('school_name_search');
        $school_search_selection = School::selectListByName($school_name);
        
        //？
        $selected = $request->input('selected');
        if($selected === null){
            $selected = false;
        }
         //学年データ
         $grades = Grade::leftJoin('schools','grade.school_id','schools.id')
            ->select('schools.name as school_name','grade.name as grade_name','schools.school_id as sc_id',
                'grade.id as grade_id','schools.id as school_id')
            ->when($school_id !== null ,function($q) use($school_id){
                return $q->where('schools.school_id','Like',"%$school_id%");
            })
            ->when($school_name !== null ,function($q) use($school_name){
                return $q->where('schools.name','Like binary',"%$school_name%");
            })
            //orderByだとエラーなので
            ->orderByRaw('school_name ASC,CHAR_LENGTH(grade.name) ASC,grade.name ASC') 
            ->paginate($pagenation);

        //成功メッセージをださないため
        session()->flash('flash_message', null);

         //リターンview
        return view('system.grade',compact('grades','school_name','school_id',
            'school_search_selection','school_name_search','selected'));
    }
    /**
     * ★新規作成のために学校を選ぶ（セレクトボックスから選択）
     *Route::match(['get', 'post'] 
     * @return return view('system.grade')
     */
    public function schoolSelect(Request $request)
    {
        global $pagenation;
        logger()->debug('<-- GradeController@schoolSelect');
        $selected = true;

        //検索ボックス値
        $school_name = $request->input('school_name');
        $school_id = $request->input('school_id');
        
        //学校ドロップダウンの名前と値
        $school_name_search = $request->input('school_name_search');
        $school_search_selection = School::selectListByName($school_name);
        
        //学校セレクトボックス値と、入力値を代入する変数
        $grade_new_selection = Grade::selectListById($school_name_search);
        $grade_new_select = null;

        //学年データ（セレクトボックスで選択された学校の学年）
        $grades = Grade::leftJoin('schools','grade.school_id','schools.id')
        ->where('schools.id','=',$school_name_search)
        ->select('schools.name as school_name','grade.name as grade_name','schools.school_id as sc_id',
            'grade.id as grade_id','schools.id as school_id',)
        ->orderByRaw('school_name ASC,CHAR_LENGTH(grade.name) ASC,grade.name ASC') 
        ->paginate($pagenation);

        //成功メッセージをださないため
        session()->flash('flash_message', null);

        //リターンview　
        return view('system.grade',compact('grades','school_name','school_id',
            'school_search_selection','school_name_search','selected','grade_new_selection','grade_new_select'));
    }    
    /**
     * ★新規＆更新　※ボタンで分岐　　※同一学校で重複した学年を作るとエラー（DBで処理）
     * Route::match(['get', 'post'] ⇒ページネーションのため、リターンviewで多重送信対策していない
     * @return return view('system.grade')
     * 
     */
    public function save(Request $request)
    {
        global $pagenation;
       //？　フラグ
        $selected = $request->input('selected');
        if($selected === null){
            $selected = false;
        }else{
            $selected = true;
        }
        //【ここから分岐】編集の保存ボタン押下時
        if($request->has('save')){
            $validator = Validator::make($request->all(), [
                'grade_name' => ['string','max:10','required'],
            ]);
            $grade = Grade::find($request->id);
            //更新データをセット
            $grade->name = mb_convert_kana($request->grade_name,'n');
            $grade->school_id = $request->school_id;
            $_id = $request->id;

        //新規ボタン押下時
        }elseif($request->has('update')){
            //dd($request->all());
            $validator = Validator::make($request->all(), [
                'grade_name_new' => ['string','max:10','required'],
            ]);
            //新規データをセット
            $grade = new Grade();
            $grade->name = mb_convert_kana($request->grade_name_new,'n');
            $grade->school_id = $request->school_id;
        }
        //【ここから共通処理】バリデーション失敗時
        if ($validator->fails()) {
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            return redirect('/grade')         
                ->withErrors($validator);
        }
        try{
            $_grade = $grade->save();
            if($request->has('new')){
                $_id = $_grade->id;
            }
            session()->flash('flash_message', '学年を追加or更新しました');
        }catch(Exception $e){
            if($e->errorInfo[1] === 1062 ||$e->errorInfo[1] !== null){
                logger()->info('The grade you tried to create a new one is a duplicate.');
                
                return redirect('/grade')
                    ->withErrors(['message' => '同じ名前の学年が既にあります']);
            }else{
                //それ以外はエラーページへ
                logger()->error('The grade save has unknown error.');
                logger()->error($e);
                abort(500);
            }
        }

        $grades = Grade::leftJoin('schools','grade.school_id','schools.id')
            ->select('schools.name as school_name','grade.name as grade_name','schools.school_id as sc_id',
                'grade.id as grade_id','schools.id as school_id', 'grade.updated_at as grade_updated_at',)
                ->orderBy('grade.updated_at','DESC') 
                ->paginate($pagenation);

            //viewに渡す変数
            $selected = false;    
            //学校検索ボックス
            $school_name = null;
            $school_id = null;
            //学校ドロップダウンの名前と値
            $school_name_search = null;
            $school_search_selection = School::selectList();

            //リターンview　⇒更新データが一番上に表示。多重送信対策（リダイレクト）はしていない
            return view('system.grade',compact('grades','school_id','school_name',
            'school_search_selection','school_name_search','selected'));
    }

    /**
     * ★削除　※クラスが存在すると削除できない （DBで処理）
     * Route::match(['post']　⇒削除後リダイレクトなのでPOSTのみでOK
     * @return return view('system.grade')
     */
    public function delete(Request $request)
    {
        global $pagenation;
        logger()->debug('<-- GradeController@delete');
        
        $selected = $request->input('selected');
        if($selected === null){
            $selected = false;
        }else{
            $selected = true;
        }

        //削除チェックした学年IDが$checksに配列で格納
        $checks = $request->checks;
        //削除
        if($checks != null ){
            DB::beginTransaction();
            foreach($checks as $check){
                try{
                    logger()->debug(intval($check));
                    Grade::destroy(intval($check));
                }catch(Exception $e){
                    if($e->errorInfo[1] === 1451){      //外部キー違反
                        logger()->info('The grade cant destroy,cause alredy in use classes.');
                        DB::rollBack();
                            return redirect('/grade')
                            ->withErrors(['message' => '選択された学年は既にクラスで使用されているため削除できません']);
                        DB::rollBack();
                    }else{
                        DB::rollBack();
                        logger()->error('The grade destroy unknown error.');
                        logger()->error($e);
                        abort(500);
                    }
                }
            }
            DB::commit();
            //成功メッセージを出したいが、リダイレクト先のindexでフラッシュメッセージをnullにしているため、表示できない⇒nullをなしにした
            session()->flash('flash_message', '学年を削除しました');
        }        
        return redirect('/grade');
    }
}