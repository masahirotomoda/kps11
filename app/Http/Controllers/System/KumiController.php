<?php

namespace App\Http\Controllers\system;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Kumi;
use App\Models\School;
use Exception;

    /**
     * システム管理メニュー　＞　組設定
     * (1)学年一覧表示
     * (2)検索
     * (3)学校を選ぶ
     * (4)新規＆更新
     * (5)削除
     */
$pagenation = 200;
class KumiController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- KumiController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★組一覧
     * Route::get
     * @return view('system.kumi)
     */
    public function index()
    {
        logger()->debug('<-- KumiController@index');
        global $pagenation;
        //session()->put('school_name',null);
        //session()->put('school_name_search',null);
        //session()->put('school_search_selection',null);
        $selected = false;

        //検索ボックス初期値
        $school_name = null;  //学校名
        $school_id = null;  //学校ID

        //学校セレクトボックス値と、入力値を代入する変数
        $school_name_search = null;
        $school_search_selection = School::selectList();
        //natcasesort($school_search_selection); //より自然にソート昇順
        
        //組情報
        $kumi = Kumi::leftJoin('schools','kumi.school_id','=','schools.id')
        ->select('schools.name as school_name','kumi.name as kumi_name','schools.school_id as sc_id',
            'kumi.id as kumi_id','schools.id as school_id')
        //orderByだとエラーなので
        ->orderByRaw('school_name ASC ,CHAR_LENGTH(kumi.name) ASC,kumi.name ASC') 
        ->paginate($pagenation);

        //成功メッセージをださないため
        //session()->flash('flash_message', null);
        
        //リターンview
        return view('system.kumi',compact('kumi','school_name','school_id',
            'school_name_search','school_search_selection','selected'));
    }
    /**
     * ★検索「学校名」「学校ID]
     * Route::match(['get', 'post']
     * @return return view('system.kumi')     
    */
     public function search(Request $request)
    {
        logger()->debug('<-- KumiController@search');
        global $pagenation;

        //検索ボックス値
        $school_name = $request->input('school_name');
        $school_id = $request->input('school_id');

        //学校セレクトボックス値と、入力値を代入する変数
        $school_name_search = $request->input('school_name_search');
        $school_search_selection = School::selectListByName($school_name);

        //学校を選んだかのフラグ
        $selected = false;
        // dd($request);
        $selected = $request->input('selected');
        if($selected === null){
            $selected = false;
        }

        //組データ
        $kumi = Kumi::leftJoin('schools','kumi.school_id','=','schools.id')
        ->select('schools.name as school_name','kumi.name as kumi_name','schools.school_id as sc_id',
            'kumi.id as kumi_id','schools.id as school_id')
        ->when($school_id !== null ,function($q) use($school_id){
            return $q->where('schools.school_id','Like',"%$school_id%");
        })
        ->when($school_name !== null ,function($q) use($school_name){
            return $q->where('schools.name','Like binary',"%$school_name%");
        })
        ->where('schools.name','Like binary',"%$school_name%")
        ->orderByRaw('school_name ASC ,CHAR_LENGTH(kumi.name) ASC,kumi.name ASC') 
        ->paginate($pagenation); 

        //成功メッセージをださないため
        session()->flash('flash_message', null);

        //リターンview
        return view('system.kumi',compact('kumi','school_name','school_id',
            'school_name_search','school_search_selection','selected'));
    }
    /**
     * ★新規作成のために学校を選ぶ（セレクトボックスから選択）
     * Route::match(['get', 'post']
     * @return return view('system.kumi')
     */
    public function schoolSelect(Request $request)
    {
        global $pagenation;
        logger()->debug('<-- KumiController@schoolSelect');
        //学校を選んだかのフラグ
        $selected = true;

        //検索ボックス値
        $school_name = $request->input('school_name');
        $school_id = $request->input('school_id');        

        //学校ドロップダウンの名前と値
        $school_name_search = $request->input('school_name_search');
        $school_search_selection = School::selectListByName($school_name);

         //学校セレクトボックス値と、入力値を代入する変数
        $kumi_new_select = null;
        $kumi_new_selection = Kumi::selectListById($school_name_search);

        //組データ（セレクトボックスで選択された学校の学年）
        $kumi = Kumi::leftJoin('schools','kumi.school_id','=','schools.id')
            ->where('schools.id',$school_name_search)
            ->select('schools.name as school_name','kumi.name as kumi_name','schools.school_id as sc_id',
                'kumi.id as kumi_id','schools.id as school_id')
            ->orderByRaw('school_name ASC ,CHAR_LENGTH(kumi.name) ASC,kumi.name ASC') 
            ->paginate($pagenation);
        
        //成功メッセージをださないため
        session()->flash('flash_message', null);
        
        //リターンview
        return view('system.kumi',compact('kumi','school_name','school_id',
            'school_name_search','school_search_selection','selected','kumi_new_selection','kumi_new_select'));
    }

    /**
     * ★新規＆更新　※ボタンで分岐　　※同一学校で重複した学年を作るとエラー（DBで処理）
     * Route::match(['get', 'post'] ⇒ページネーションのため、リターンviewで多重送信対策していない
     * @return  redirect('/kumi')
     */
    public function save(Request $request)
    {
        logger()->debug('<-- KumiController@save');
        global $pagenation;
        //？フラグ
        $selected = $request->input('selected');
        if($selected === null){
            $selected = false;
        }else{
            $selected = true;
        }
        
        //【ここから分岐】編集の保存ボタン押下時
        if($request->has('update') ){
            //dd($request->all());
            $validator = Validator::make($request->all(), [
                'kumi_name' => ['string','max:10','required'],
            ]);
            $kumi = Kumi::find($request->id);
            //更新データをセット
            $kumi->name = mb_convert_kana($request->kumi_name,'n');
            $kumi->school_id = $request->school_id;
            $_id = $request->id;

        //新規登録の「保存」ボタン押下
        }else{
            $validator = Validator::make($request->all(), [
                'kumi_name_new' => ['string','max:10','required'],
            ]);
            $kumi = new Kumi();
            //新規データをセット
            $kumi->name = mb_convert_kana($request->kumi_name_new,'n');
            $kumi->school_id = $request->school_id;
        }
        //【ここから共通処理】バリデーション失敗時
        if ($validator->fails()) {
            //成功メッセージをださないため
            session()->flash('flash_message', null);
            return redirect('/kumi')
                ->withErrors($validator);
        }
        try{
            $_kumi = $kumi->save();
            if($request->has('new')){
                $_id = $_kumi->id;
            }
            session()->flash('flash_message', '組を追加or更新しました');
        }catch(Exception $e){
            if($e->errorInfo[1] === 1062 ||$e->errorInfo[1] !== null){
                logger()->info('The kumi you tried to create a new one is a duplicate.');
                return redirect('/kumi')
                    ->withErrors(['message' => '同じ名前の組が既にあります']);
            }else{
                //それ以外はエラーページへ
                logger()->error('The grade save has unknown error.');
                logger()->error($e);
                abort(500);
            }
        }
           //viewに渡す変数
           $selected = false; 

           //学校検索ボックス
           $school_name = null;
           $school_id = null;       
   
           //学校ドロップダウンの名前と値
           $school_name_search = null;
           $school_search_selection = School::selectList();

           //組を選ぶドロップダウンリストの名前と値
           $kumi_new_select = null;
           $kumi_new_selection = Kumi::selectListById($school_name_search);
   
           //組情報（ドロップダウンで選んだ学校の組情報）
           $kumi = Kumi::leftJoin('schools','kumi.school_id','=','schools.id')
               ->select('schools.name as school_name','kumi.name as kumi_name','schools.school_id as sc_id',
                   'kumi.id as kumi_id','schools.id as school_id','kumi.updated_at as kumi_updated_at')
               ->orderBy('kumi_updated_at', 'DESC')
               ->paginate($pagenation);
            //リターンview　⇒更新データが一番上に表示。多重送信対策（リダイレクト）はしていない
            return view('system.kumi',compact('kumi','school_name','school_id',
               'school_name_search','school_search_selection','selected','kumi_new_selection','kumi_new_select'));
    }
    /**
     * ★削除　※クラスが存在すると削除できない（DB処理）
     * Route::post⇒削除後リダイレクトなのでPOSTのみでOK
     * @return redirect('/kumi')
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- KumiController@delete');
        //$checksには、削除チェックしている組IDが配列で入っている 
        $checks = $request->checks; 
        if($checks !== null ){
            foreach($checks as $check){                
                DB::beginTransaction();
                try{
                    Kumi::destroy(intval($check));
                    //dd($check);
                }catch(Exception $e){
                    if($e->errorInfo[1] === 1451){      //外部キー違反
                        logger()->info('The kumi cant destroy,cause alredy in use classes.');
                        DB::rollBack();

                        return redirect('/kumi')
                            ->withErrors(['message' => '選択された組は既にクラスで使用されているため削除できません']);
                            DB::rollBack();
                    }else{
                        DB::rollBack();
                        logger()->error('The kumi destroy has unknown error.');
                        logger()->error($e);
                        abort(500);
                    }
                }
            DB::commit();
            session()->flash('flash_message', '組を削除しました');
            }           
        }
        return redirect('/kumi');
    }
}