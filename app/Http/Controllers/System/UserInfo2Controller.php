<?php

namespace App\Http\Controllers\system;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Exports\Export;
use App\Models\User;
use App\Models\School;
use App\Models\Grade;
use App\Models\Kumi;
use App\Models\Clas;
use Maatwebsite\Excel\Facades\Excel;
use Exception;

    /**
     * システム管理メニュー　＞　ユーザー管理(2)学校別
     * (1)ユーザデータ取得SQL
     * (2)学校選択
     * (1)ユーザー一覧表示
     * (2)検索＆エクセル出力＆学校選択
     * (3)新規＆更新＆パスワードリセット＆削除
     * (4)特別学校アカウント作成ページへ遷移（ナレッジが使う隠し学校アカウント）
     * (5)特別学校アカウント作成
     * (6)隠しボタンによるユーザー編集へ遷移
     * (7)隠しボタンによるユーザー編集（ログインアカウントとパスワードをバリデーションなしで保存したい時）
     */
class UserInfo2Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.role');
    }
    /**
     * ★ユーザデータ取得SQL このクラスでのみ使用
     * @return $users
     */
    private function get_users($sc_int_id)
    {
        $pagenation = config('configrations.PAGENATION');
        $users = User::leftJoin('schools','users.school_int_id','schools.id')
        ->leftJoin('classes','users.class_id','classes.id')
        ->leftJoin('grade','classes.grade_id','grade.id')
        ->leftJoin('kumi','classes.kumi_id','kumi.id')
        ->select('schools.name as school_name','users.name as user_name','users.id as user_id',
            'role','class_id','attendance_no','users.login_account','schools.id as school_id','schools.school_id as sc_id',
            'kumi.name as kumi_name','grade.name as grade_name','grade.id as grade_id','kumi.id as kumi_id',
            'schools.password_school_init','schools.password_teacher_init','schools.password_student_init','users.*')
        ->where('users.role','<>',config('configrations.ROLE_LIST.1'))
        //選んだ学校のみ表示
        ->where('users.school_int_id','=',$sc_int_id)
        ->orderByRaw('role ASC,classes.id ASC,user_name')
        ->paginate($pagenation);

        return $users;        
    }
    /**
     * ★学校選択　⇒セレクトボックスで学校を選ぶ
     * @return view('system.userinfo2selectschool')
     */
    public function schoolSelect(Request $request)
    {
    //学校のセレクトボックス値（メソッドはモデルで定義）
    $school_search_selection = School::selectList();
    natcasesort($school_search_selection); //より自然にソート昇順　第1５小学校等、数字と文字が混在している時に希望の挙動
    $selected_school_int_id = null;
    return view('system.selectschool',compact('school_search_selection','selected_school_int_id'));            
    }

    /**
     * ★ユーザー一覧（遷移前の学校選択のデータを引き継ぐ）
     * @return view('system.userinfo)
     * Route::get
     */
    public function index(Request $request)
    {
        //検索　ロールのチェックボックス
        $school_adm = true;
        $teacher = true;
        $student = true;

        //検索ボックス       
        $s_attendance_no = null;
        $s_user_name = null;
        $s_login_account = null;
      
        //選んだ学校のID(前ページのセレクトボックスから選択)と学校データ
        $selected_school_int_id = $request->select_school_id;
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //「学年」「組」のセレクトボックス値（selectListById関数は、modelで定義）
        $grade_selection = Grade::selectListById($selected_school_int_id);
        $kumi_selection = Kumi::selectListById($selected_school_int_id);
        //「学年」「組」のセレクトボックス値の初期値
        $s_grade_id = null;
        $s_kumi_id = null;  

        //表の中の「ロール」セレクトボックス、configrationsから値を取得　ただし新規作成以外は編集不可
        $roleSelection = config('configrations.ROLE_LIST3');

        //ユーザーデータ
        $users = $this->get_users($school->id);
        //リターンview
        return view('system.userinfo2',compact('school','users','school_adm','teacher','student',
            's_grade_id','s_kumi_id','s_attendance_no','s_user_name','s_login_account','roleSelection',
            'grade_selection','kumi_selection','selected_school_int_id')); 
    } 
    /**
     * ★検索＆エクセル出力
     * @return view('system.userinfo)
     * Route::get
     */
    public function search(Request $request)
    {
        $pagenation = config('configrations.PAGENATION');
        //ロールのチェックボックスの検索値を変数にセット
        $school_adm = $request->input('school_adm');
        $teacher = $request->input('teacher');
        $student = $request->input('student');

        //検索値を変数にセット
        $s_attendance_no = $request->input('s_attendance_no');
        $s_grade_id = $request->input('s_grade_id');
        $s_kumi_id = $request->input('s_kumi_id');
        $s_user_name = $request->input('s_user_name');
        $s_login_account = $request->input('s_login_account');
        
        //選んだ学校のID(前ページから引き継ぐ)と学校データ
        $selected_school_int_id = $request->select_school_id;
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //「学年」「組」のセレクトボックス値（selectListById関数は、modelで定義）
        $grade_selection = Grade::selectListById($selected_school_int_id);
        $kumi_selection = Kumi::selectListById($selected_school_int_id);

        //表の中の「ロール」セレクトボックス、configrationsから値を取得　ただし新規作成以外は編集不可
        $roleSelection = config('configrations.ROLE_LIST3');

        //検索値の出席番号を半角にする（全角で入力しても検索できるために）
        if (isset($request['s_attendance_no'])){
            $request['s_attendance_no'] = mb_convert_kana($request['s_attendance_no'], 'n');
            }

        //検索値のログインアカウントを半角にする（全角で入力しても検索できるために）
        if (isset($request['s_login_account'])){
            $request['s_login_account'] = mb_convert_kana($request['s_login_account'], 'n');
            }

        //学校管理者、先生、生徒にチェックがあるものを$selectRoleの配列にいれる⇒SQLの検索条件を配列に入れる
        $selectRole = [];
        if($school_adm !== null){
            array_push($selectRole,'学校管理者');
        }
        if($teacher !== null){
            array_push($selectRole,'先生');
        }
        if($student !== null){
            array_push($selectRole,'生徒');
        }        

        /////ここから分岐
        //(1)全て表示ボタンorリセットボタン　⇒全ユーザーデータ表示。2階層目なので、リダイレクトできない
        if($request->has('all_btn') || $request->has('cancel_btn')){
            //ユーザーデータ
            $users = $this->get_users($school->id);

            //検索　ロールのチェックボックス初期値にする
            $school_adm = true;
            $teacher = true;
            $student = true;
            //検索ボックス初期値にする       
            $s_attendance_no = null;
            $s_grade_id = null;
            $s_kumi_id = null;
            $s_user_name = null;
            $s_login_account = null;
       
            //dd($request->all());
            //リターンview
            return view('system.userinfo2',compact('school','users','school_adm','teacher','student',
                's_grade_id','s_kumi_id','s_attendance_no','s_user_name','s_login_account','roleSelection',
                'grade_selection','kumi_selection','selected_school_int_id'));

        //(2)エクセル一括出力ボタンor検索ボタン押下（クエリのみ）
        } elseif ($request->has('excelout') || $request->has('search') || $request->has('page')){
                $users_data = User::leftJoin('schools','users.school_int_id','=','schools.id')
                ->leftJoin('classes','users.class_id','classes.id')
                ->leftJoin('grade','classes.grade_id','=','grade.id')
                ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
                ->where('users.role','<>','システム管理者')
                ->where('users.school_int_id','=',$selected_school_int_id)
                ->whereIn( 'users.role', $selectRole )  //チェックの入っているものが配列に入っている
                ->when($s_grade_id !== null ,function($q) use($s_grade_id){
                    return $q->where('classes.grade_id','=',$s_grade_id);
                })
                ->when($s_kumi_id !== null ,function($q) use($s_kumi_id){
                    return $q->where('classes.kumi_id','=',$s_kumi_id);
                })
                ->when($s_user_name !== null ,function($q) use($s_user_name){
                    return $q->where('users.name','Like binary',"%$s_user_name%");
                })
                ->when($s_attendance_no !== null ,function($q) use($s_attendance_no){
                    return $q->where('users.attendance_no','=',$s_attendance_no);
                })
                ->when($s_login_account !== null ,function($q) use($s_login_account){
                    return $q->where('users.login_account','Like',"%$s_login_account%");
                })
                ->select('schools.name as school_name','users.name as user_name','users.id as user_id',
                    'role','class_id','attendance_no','users.login_account','schools.id as school_int_id','schools.school_id as school_id',
                    'kumi.name as kumi_name','grade.name as grade_name','grade.id as grade_id','kumi.id as kumi_id',
                    'schools.password_school_init','schools.password_teacher_init','schools.password_student_init','users.*')
                ->orderByRaw('schools.name ASC')
                ->orderByRaw('CHAR_LENGTH(grade.name) ASC,CHAR_LENGTH(kumi.name) ASC')
                ->orderByRaw('grade.name ASC,kumi.name ASC')
                ->orderByRaw('role ASC,user_name ASC');

                //さらに、絞り込む⇒エクセルボタン押下　※get()
                if($request->has('excelout')){
                    $users=$users_data->get();
                    //リターン　エクセル出力（エクセル出力データが0件の時のバリデーションはなし）
                    $view = view('system.userinfoexcel',compact('users'));
                    return Excel::download(new Export($view), '【管理】ユーザー一覧'.date('Y-m-d-H-i-s').'.xlsx');
                //さらに、絞り込む⇒検索ボタン押下　※paginate()
                } elseif($request->has('search') || $request->has('page')){
                    $users=$users_data->paginate($pagenation);
                    //リターンview
                    return view('system.userinfo2',compact('school','users','school_adm','teacher','student',
                        's_grade_id','s_kumi_id','s_attendance_no','s_user_name','s_login_account','roleSelection',
                        'grade_selection','kumi_selection','selected_school_int_id'));
                }
        }
    }     
    /**
     * ★新規＆更新＆パスワードリセット＆削除　ボタンで切り分け
     * Route::match(['get', 'post'] ⇒多重送信対策は未実装
     * (1)新規作成後、保存ボタン（update）　(2)編集後の保存ボタン（save）(3)パスワードリセットボタン（modal_password_reset_btn）(4)削除ボタン（modal_delete_btn）
     * @return redirect('/user')
     */
    public function save(Request $request)
    {
        //ロールのチェックボックス
        $school_adm = true;
        $teacher = true;
        $student = true;

         //検索ボックス
         $s_attendance_no = null;
         $s_grade_id = null;
         $s_kumi_id = null;
         $s_user_name = null;
         $s_login_account = null;
         
         //選んだ学校のID(前ページから引き継ぐ)と学校データ
         $selected_school_int_id = $request -> hidden_school_id;
         $school=School::find($selected_school_int_id);
         $school_name = $school->name;
 
         //「学年」「組」のセレクトボックス値（selectListById関数は、modelで定義）
         $grade_selection = Grade::selectListById($selected_school_int_id);
         $kumi_selection = Kumi::selectListById($selected_school_int_id);
 
         //表の中の「ロール」セレクトボックス、configrationsから値を取得　ただし新規作成以外は編集不可
         $roleSelection = config('configrations.ROLE_LIST3');

        //新規or更新した出席番号を半角にする
        if (isset($request['attendance_no'])){
            $request['attendance_no'] = mb_convert_kana($request['attendance_no'], 'n');
            }
        //★ここから分岐(1)新規ボタン押下時（ボタン名はupdate）
        if($request->has('update')){
            if($school->contest>0){//コンテストでは学校管理者のみ登録。編集は不可とする。
                $newUser = new User();
                $newUser->name = $request->student_name; // 名前
                $newUser->role = $request->role; // ロール
                $newUser->class_id =null; // クラスID
                $newUser->attendance_no = $request['attendance_no'];
                $newUser->password = $school->password_school_init;
                $newUser->password = bcrypt($school -> password_school_init); // 学校管理パスワード初期値
                $newUser->login_account = 'test';//仮にtestとする。確定後編集
                $newUser->school_int_id = $school->id; // 学校ID(数値)
                $newUser->school_id = $school->school_id; // 学校ID
                $newUser->save();
            } else {
                //学年、組からクラスID取得
                $clas_id = Clas::where('school_id',"=",$school->id)
                ->where('classes.grade_id','=',$request->grade)
                ->where('classes.kumi_id','=',$request->kumi)
                ->select('id')->first();

                //バリデーション準備　　$validation_dataにクラスIDとユーザー名を入れる
                //クラスがないときはクラスIDにnullがはいり、必須のバリデーションにひっかかる
                if($clas_id == null){
                    $validation_data['system_user_clas_id'] =null;
                } else {
                    $validation_data['system_user_clas_id'] =  $clas_id->id;
                }
                $validation_data['system_user_name'] = $request->student_name;
                $validation_data['attendance_no'] = $request->attendance_no;

                //dd($request->all());

                //学校に登録できるユーザー最大数
                $max_user_num = $school['user_number'];
                //学校管理＆先生＆生徒を含むユーザー数、ナレッジ用隠し学校アカウント（b33259)がある時は、除外する
                $user_num =  User::where('school_int_id','=',$school->id)
                    ->where('login_account','<>',config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN'))
                    ->count();
                //dd($user_num);
                // 最大登録数をオーバーしたらバリデーション（requied )チェックするための準備（学校管理1名、先生1名は含まない）       
                if($user_num < $max_user_num +2){
                    $validation_data['over_max_student_num'] ="OK"; //バリデーションクリア
                } else {
                    $validation_data['over_max_student_num'] = null; //バリデーションにひっかかる
                }

                //バリデーション実行　ユーザー名の文字数は　addnew-users.js にmaxlength20を設定してある
                $validator = Validator::make($validation_data, [
                    'system_user_clas_id' => ['required'],
                    'system_user_name' => ['required','max:20'],
                    'attendance_no' => ['required','digits_between:0,4'],
                    'over_max_student_num' => ['required'],
                ]);

                //バリデーション失敗時　バリデーションメッセージはvalidation.php で指定
                if ($validator->fails()) {
                    //ユーザーデータ
                    $users = $this->get_users($school->id);
                    //リターンview
                        return view('system.userinfo2',compact('school','users','school_adm','teacher','student',
                            's_grade_id','s_kumi_id','s_attendance_no','s_user_name','s_login_account','roleSelection',
                            'grade_selection','kumi_selection','selected_school_int_id'))
                            ->withErrors($validator);
                }

                //バリデーション成功時、$newUserに新規データをセットして保存
                $newUser = new User();
                $newUser->name = $request->student_name; // 名前
                $newUser->role = $request->role; // ロール
                $newUser->class_id =  $clas_id['id']; // クラスID
                $newUser->attendance_no = $request['attendance_no'];
                if($request->role === config('configrations.ROLE_LIST.4')){
                    $newUser->password = $school->password_student_init; // 生徒パスワード
                } elseif($request->role === config('configrations.ROLE_LIST.3')){
                    $newUser->password = bcrypt($school->password_teacher_init); // 先生パスワード
                } elseif($request->role === config('configrations.ROLE_LIST.2')){
                $newUser->password = bcrypt($school->password_school_init); // 学校管理パスワード初期値
                }
                $newUser->school_int_id = $school->id;; // 学校ID(数値)
                $newUser->school_id = $school->school_id; // 学校ID

                try{
                    DB::beginTransaction();
                    //ログインIDをランダムに発行（重複しないまで繰り返す）
                    //ログインIDを取得　※10101 ~ 99999 の内
                    while(true){
                        $id_no = str_pad(mt_rand(10101, 99999), 5, 0, STR_PAD_LEFT);
                        $n_no = User::where('login_account','Like',"%$id_no")
                            ->where('school_id',auth()->user()->school_id)
                            ->first();
                        if($n_no === null){
                            break;
                        }
                    }
                    //学校管理者には「a]、先生には「t」の接頭辞をつける
                    if($newUser->role==config('configrations.ROLE_LIST.2')){
                        $newUser->login_account = 'a'.$id_no;
                    } elseif($newUser->role==config('configrations.ROLE_LIST.3')){
                        $newUser->login_account = 't'.$id_no;
                    } elseif($newUser->role==config('configrations.ROLE_LIST.4')){
                        $newUser->login_account = $id_no; //生徒
                    } else {
                        $newUser->login_account = 'エラー'.$id_no; 
                    }
                    
                    //新規ユーザー情報をセットして保存
                    $newUser->save();
                    session()->flash('flash_message', $newUser->name.'さんを新規登録しました');
                    DB::commit();
                }catch(\Exception $e){
                    logger()->error('システム管理メニュー、ユーザーの新規作成でエラー');
                    logger()->error($e);
                    DB::rollback();
                    abort(500);
                }
            }
        }
        //(2)★更新　⇒編集後の「保存」ボタン押下 ※save
        elseif($request->has('save')){
            //dd($request->all());
            //学年、組からクラスID取得　$request->school_name_search　⇒選んだ学校のID
            $clas_id = Clas::where('school_id',"=",$school->id)
            ->where('grade_id',"=",$request->grade_id)
            ->where('kumi_id',"=",$request->kumi_id)
            ->select('id')->first();

            //バリデーションの準備　クラスIDがないとnullにして、バリデーションにかける
            if($clas_id == null){
                $validation_data['system_user_clas_id'] =null;
            } else {
                $validation_data['system_user_clas_id'] =  $clas_id->id;
            }
            $validation_data['system_user_name'] = $request->user_name;
            $validation_data['attendance_no'] = $request->attendance_no;

            //バリデーション
            $validator = Validator::make( $validation_data, [
                'system_user_clas_id' => ['required'],  //学年と組が存在しないクラスの場合、nullなのでエラー
                'system_user_name' => ['required','max:20'],
                'attendance_no' => ['required','digits_between:0,4'],
            ]);
            //バリデーション失敗時
            if ($validator->fails()) {
                //ユーザーデータ
                $users = $this->get_users($school->id);

                //リターンview
                    return view('system.userinfo2',compact('school','users','school_adm','teacher','student',
                        's_grade_id','s_kumi_id','s_attendance_no','s_user_name','s_login_account','roleSelection',
                        'grade_selection','kumi_selection','selected_school_int_id'))
                        ->withErrors($validator);
            }
            try{
                DB::beginTransaction();
                    //ユーザー情報をセットして保存（学年と組はクラスIDにして保存）
                    $user=User::find($request->save_user_id);
                    $user
                    ->fill([
                        'class_id' => $clas_id->id,
                        //'role' => $request->role,
                        'name' => $request->user_name,
                        'attendance_no' => $request->attendance_no,
                        ])
                 ->save();
                    session()->flash('flash_message', $user->name.'さんの情報を更新しました');
                DB::commit();
            }catch(Exception $e){
                logger()->error('システム管理メニュー、ユーザーの更新でエラー');
                logger()->error($e);
                DB::rollback();
                abort(500);
            }
        }
        //(3)★パスワードリセットボタン押下時　⇒他ページはリセットは別formだが、ユーザーID取得のため
        elseif($request->has('modal_password_reset_btn')){

            //dd($request->all());
            $user=User::find($request->save_user_id);

            //パスワード初期値、生徒はそのまま、先生＆学校管理者はパスワードをハッシュ化
            if($user->role === config('configrations.ROLE_LIST.4')){
                $init_password = $school -> password_student_init;
            } elseif ($user -> role === config('configrations.ROLE_LIST.3')){
                $init_password = bcrypt($school -> password_teacher_init);
            } elseif($user -> role === config('configrations.ROLE_LIST.2')){
                $init_password = bcrypt($school -> password_school_init);
            }
            //パスワードを初期値にセットして保存
            try{
                $user->password = $init_password;
                $user ->save();
            session()->flash('flash_message', 'パスワードをリセットしました');
            }catch(Exception $e){
                logger()->error('システム管理メニュー、ユーザーのパスワードリセットでエラー');
                logger()->error($e);
                abort(500);
            }
        
        //(4)★削除ボタン押下
        }elseif($request->has('modal_delete_btn')){
            //dd($request->all());
            $checks = $request->checks;
            if($checks !== null ){
                DB::beginTransaction();
                foreach($checks as $check){
                    try{
                        $selected = true;
                        User::destroy(intval($check));
                    }catch(\Exception $e){
                            DB::rollBack();
                            logger()->error('◆システム管理メニュー、ユーザー削除でエラー');
                            logger()->error($e);
                            abort(500);
                    }
                }
                DB::commit();
            }
        }
        //ここからは、共通⇒リターンviewに渡すデータ
        //検索　ロールのチェックボックス
        $school_adm = true;
        $teacher = true;
        $student = true;

        //検索ボックス
        $s_school_name = null;
        $a_attendance_no = null;
        $a_grade_name = null;
        $a_kumi_name = null;
        $a_user_name = null;
        $a_login_account = null;

        //「学年」「組」のセレクトボックス値（selectListById関数は、modelで定義）
        $grade_selection = Grade::selectListById($selected_school_int_id);
        $kumi_selection = Kumi::selectListById($selected_school_int_id);

        //表の中の「ロール」セレクトボックス、configrationsから値を取得　ただし新規作成以外は編集不可
        $roleSelection = config('configrations.ROLE_LIST3');

         //ユーザーデータ
         $users = $this->get_users($school->id);
         //リターンview
         return view('system.userinfo2',compact('school','users','school_adm','teacher','student',
            's_grade_id','s_kumi_id','s_attendance_no','s_user_name','s_login_account','roleSelection',
            'grade_selection','kumi_selection','selected_school_int_id'));
     }
    /**
     * ★特別学校アカウント作成ページへ遷移（「特別学校アカウント作成」ボタン押下）
     * @return view('system.userinfo2schoolaccount)
     * Route::post
     */
    public function addschoolaccount(Request $request)
    {
        logger()->debug('<-- UserInfo2Controller@addschoolaccount');
        //dd($request->all());        
      
        //選んだ学校のID(hiddenで取得）と学校データ
        $selected_school_int_id = $request->select_school_id;
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //新規登録データの初期値
        $name = null;
        $login_account = null;
        $password = null;
        
        //リターンview
        return view('system.userinfo2schoolaccount',compact('school','name','selected_school_int_id','login_account','password')); 
    } 
    /**
     * ★特別学校アカウント作成（ナレッジが使う隠し学校アカウント、ログイン履歴に表示させない、利用最大数に含めない）
     * @return view('system.userinfo2schoolaccount)
     * Route::post
     */
    public function saveschoolaccount(Request $request)
    {
        logger()->debug('<-- UserInfo2Controller@saveschoolaccount');
        //dd($request->all());        
      
        //選んだ学校のIDと学校データ
        $selected_school_int_id = $request->select_school_id;
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //特別アカウントは学年「なし」、組「なし」からクラスIDを取得
        $tokubetuaccount_class = Clas::where('classes.school_id','=',$selected_school_int_id)
        ->leftJoin('grade','classes.grade_id','=','grade.id')
        ->leftJoin('kumi','classes.kumi_id','=','kumi.id')
        ->select('grade.name as grade_name','kumi.name as kumi_name','classes.id as class_id')
        ->where('grade.name','=','なし') 
        ->where('kumi.name','=','なし') 
        -> first();
        //dd( $tokubetuaccount_class->class_id);

        $newUser = new User();
            $newUser->name = $request->name; // 名前
            $newUser->role ='学校管理者'; // ロール
            $newUser->attendance_no = 0;
            $newUser->login_account = $request->login_account;//ログインアカウント
            $newUser->password = bcrypt($request->password); // 特別学校管理パスワード
            $newUser->school_int_id = $school->id;; // 学校ID(数値)
            $newUser->school_id = $school->school_id; // 学校ID
            $newUser->class_id = $tokubetuaccount_class->class_id; // クラスID（学年なし、組なしのクラス）

        //バリデーション
        $validator = Validator::make($request->all(), [
            'login_account' => ['required', 'string'],
            'name' => ['string','max:20','required'],
            'password' => ['string',
                'regex:<^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[a-z])(?=.*[A-Z])(?=.*[!@;:#$%&])|(?=.*[A-Z])(?=.*[0-9])(?=.*[!@;:#$%&])|(?=.*[a-z])(?=.*[0-9])(?=.*[!@;:#$%&]))([a-zA-Z0-9!@;:#$%&]){10,}$>'
                ,'required'],
        ]);
        //バリデーション失敗時
        if ($validator->fails()) {
            //dd($request->all());
            //新規登録データの初期値
            $name = $request->name;
            $login_account = $request->login_account;
            $password = $request->password;
        
            //リターンview
            return view('system.userinfo2schoolaccount',compact('school','name','selected_school_int_id','login_account','password'))
                ->withErrors($validator);
        }
        
        //dd($newUser);
        try{
            $newUser->save();
            session()->flash('flash_message', $newUser->name.'さんを特別学校アカウントとして登録しました');
        }catch(\Exception $e){
            logger()->error('システム管理メニュー、学校別ユーザー登録、特別学校アカウント作成でエラー');
            logger()->error($e);
            abort(500);
        }
        //return　viewのための準備
        //検索　ロールのチェックボックス
        $school_adm = true;
        $teacher = true;
        $student = true;

        //検索ボックス       
        $s_attendance_no = null;
        $s_user_name = null;
        $s_login_account = null;
      
        //選んだ学校のID(前ページのセレクトボックスから選択)と学校データ
        $selected_school_int_id = $request->select_school_id;
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //「学年」「組」のセレクトボックス値（selectListById関数は、modelで定義）
        $grade_selection = Grade::selectListById($selected_school_int_id);
        $kumi_selection = Kumi::selectListById($selected_school_int_id);
        //「学年」「組」のセレクトボックス値の初期値
        $s_grade_id = null;
        $s_kumi_id = null;  

        //表の中の「ロール」セレクトボックス、configrationsから値を取得　ただし新規作成以外は編集不可
        $roleSelection = config('configrations.ROLE_LIST3');

        //ユーザーデータ
        $users = $this->get_users($school->id);
        //リターンview
        return view('system.userinfo2',compact('school','users','school_adm','teacher','student',
            's_grade_id','s_kumi_id','s_attendance_no','s_user_name','s_login_account','roleSelection',
            'grade_selection','kumi_selection','selected_school_int_id')); 
    }
    /**
     * ★隠しボタンによるユーザー編集へ遷移（「＊」ボタン押下。ナレッジ専用）
     * @return view('system.userinfo2useredit)
     * Route::post
     */
    public function useredit(Request $request)
    {
        //選んだ学校のID(hiddenで取得）と学校データ
        $selected_school_int_id = $request->select_school_id;
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //選んだユーザー情報
        $user_id = $request->user_id;
        $user = User::find($user_id);

        //表の中の「ロール」セレクトボックス、configrationsから値を取得
        $roleSelection = config('configrations.ROLE_LIST3');

        //入力ボックスの初期値
        $name = $user->name;
        $role = $user->role;
        $login_account = $user->login_account;
        $password = null;
        //リターンview
        return view('system.userinfo2useredit',compact('school','user','selected_school_int_id','login_account','password','name','role','roleSelection')); 
    }
    /**
     * ★隠しボタンによるユーザー編集（ナレッジ専用、ログインアカウントとパスワードをバリデーションなしで保存したい時）
     * @return view('system.userinfo2useredit)
     * Route::post
     */
    public function usersave(Request $request)
    {
        //選んだ学校のIDと学校データ
        $selected_school_int_id = $request->select_school_id;
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //選んだユーザー情報
        $user_id = $request->user_id;
        $user = User::find($user_id);

        //更新データをセット
        $user->name = $request->name;
        $user->role =$request->role;
        $user->login_account = $request->login_account;
        //パスワード欄が空欄だと変更なし
        if($request->password !== null){
            if($user->role=='生徒'){
                $password = $request->password;
            } elseif($user->role=='先生'){
                $password = bcrypt($request->password);
            } elseif($user->role=='学校管理者'){
                $password = bcrypt($request->password);
            }
            $user->password = $password;
        }        
        try{
            $user->save();
            session()->flash('flash_message', $user->name.'さんの情報を更新しました。');
        }catch(Exception $e){
            logger()->error('システム管理メニュー、学校別ユーザー登録、ユーザー編集でエラー');
            abort(500);
        }
        //return　viewのための準備
        //入力ボックスの初期値
        $name = $user->name;
        $role = $user->role;
        $login_account = $user->login_account;
        $password = null;

        //検索　ロールのチェックボックス
        $school_adm = true;
        $teacher = true;
        $student = true;

        //検索ボックス       
        $s_attendance_no = null;
        $s_user_name = null;
        $s_login_account = null;
      
        //選んだ学校のID(前ページのセレクトボックスから選択)と学校データ
        $selected_school_int_id = $request->select_school_id;
        $school=School::find($selected_school_int_id);
        $school_name = $school->name;

        //「学年」「組」のセレクトボックス値（selectListById関数は、modelで定義）
        $grade_selection = Grade::selectListById($selected_school_int_id);
        $kumi_selection = Kumi::selectListById($selected_school_int_id);
        //「学年」「組」のセレクトボックス値の初期値
        $s_grade_id = null;
        $s_kumi_id = null;  

        //表の中の「ロール」セレクトボックス、configrationsから値を取得　ただし新規作成以外は編集不可
        $roleSelection = config('configrations.ROLE_LIST3');

        //ユーザーデータ
        $users = $this->get_users($school->id);
        //リターンview
        return view('system.userinfo2',compact('school','users','school_adm','teacher','student',
            's_grade_id','s_kumi_id','s_attendance_no','s_user_name','s_login_account','roleSelection',
            'grade_selection','kumi_selection','selected_school_int_id')); 
    }
}
