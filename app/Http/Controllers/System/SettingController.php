<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/**
 * システム管理メニュー　＞　基本設定
 * ※config\configrations.php　にある基本設定　【重要】プログラムから更新できないので、閲覧のみ
 * (1)基本設定
 */
class SettingController extends Controller
{   
    public function __construct()
    {
        logger()->debug('<-- SettingController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        
    } 
/**
 * ★基本設定
 *  Route::get
 * @return view('system.setting')
 */
    public function index()
    {
        logger()->debug('<-- SettingController@index');
        //configrations.php を読み込む
        $settings = [
            'PAGENATION' => config('configrations.PAGENATION'),
            'SESSION_TIME' => config('configrations.SESSION_TIME'),
            'COPYRIGHT' => config('configrations.COPYRIGHT'),
            'TRIAL_PERIOD_SCHOOL' => config('configrations.TRIAL_PERIOD_SCHOOL'),
            'TRIAL_PERIOD_CRAM' => config('configrations.TRIAL_PERIOD_CRAM'),
            'META_TITLE' => config('configrations.META_TITLE'),
            'TRIAL_SCHOOL_NO' => config('configrations.TRIAL_SCHOOL_NO'),
            'DOMAIN_NAME' => config('configrations.DOMAIN_NAME'),
            'ADMIN_SCHOOL' => config('configrations.ADMIN_SCHOOL'),
            'ADMIN_ZIP' => config('configrations.ADMIN_ZIP'),
            'ADMIN_ADDRESS' => config('configrations.ADMIN_ADDRESS'),
            'ADMIN_TEL' => config('configrations.ADMIN_TEL'),
            'ADMIN_MAIL' => config('configrations.ADMIN_MAIL'),
            'ADMIN_IPADDRESS' => config('configrations.ADMIN_IPADDRESS'),
            'TRIAL_SCHOOL_NO_URL' => config('configrations.TRIAL_SCHOOL_NO_URL'),
            'ADMIN_SCHOOL_NO' => config('configrations.ADMIN_SCHOOL_NO'),
            'ADMIN_SCHOOL_NO_URL' => config('configrations.ADMIN_SCHOOL_NO_URL'),
            'SCHOOL_ACCOUNT_FOR_ADMIN' => config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN'),


        ];
        //dd($settings);
        //リターンview
        return view('system.setting',compact('settings'));
    }
}
