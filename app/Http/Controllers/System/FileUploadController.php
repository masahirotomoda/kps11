<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\CourseWord;
use Exception;

    /**
     * ★【管理用】ランキングスタジアム英語モード用画像ファイルアップロード
     */
class FileUploadController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- FileUploadController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
    }
    

    /**
     * ★【管理用】イラスト用画像ファイルアップロード 一覧表示(地図タブ、英語タブ、ランキングバトル（英語）)
     * 検索、削除、アップロードメソッドのredirect先はすべてindexにする。（return viewは使わない）
     * @return return view('system.fileupload')
     */
    //検索もこのindexを使うため、パラメータは$request
    public function index(Request $request)
    {
        logger()->debug('<-- FileUploadController@index');
        
        //検索値を変数にセット  
        if($request->isMethod("get")){     
        $course_name = null;
        $word_name = null;
        $tab_select='english1';
        } else {
            $course_name = $request->course_name;
            $word_name = $request->word_name; 
            $tab_select= $request->tab_select;
        }

        //単語一覧を中間テーブル（course_word）から取得　※コースと単語を一対一でつなぐ
        $courseWords = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
        ->leftJoin('words','course_word.word_id','words.id')
        ->leftJoin('schools','courses.school_id','schools.id')
        ->when($word_name != '' || $word_name !=null,function($q) use($word_name){
            return $q->where('words.word_mana','Like binary',"%$word_name%");
        })
        ->when($course_name != '' || $course_name !=null,function($q) use($course_name){
            return $q->where('courses.course_name','Like binary',"%$course_name%");
        })
        ->where('courses.tab',$tab_select)
        ->when($tab_select === 'battle', function ($q) {
            return $q->where('courses.course_type', '英語');
        }) // 「battle」の場合のみタイプに「英語」を条件に追加
        ->select('courses.invalid as course_invalid',
            'words.id as word_id','courses.id as course_id','courses.course_name','words.word_mana','schools.name as school_name','courses.tab')
        ->orderBy('course_word.word_id','ASC')
        ->get();
        //dd($courseWords);

        //public/img/english と public/img/map にある画像（png）をすべて取得
        $directories = [
            public_path('img/english'),
            public_path('img/map')
        ];
        
        $all_imgfile = [];
        
        foreach ($directories as $directory) {
            if (is_dir($directory)) {
                $files = scandir($directory);
                foreach ($files as $file) {
                    if (is_file($directory . '/' . $file) && preg_match('/\.(jpg|jpeg|png|gif)$/i', $file)) {
                        $filename = pathinfo($file, PATHINFO_FILENAME); // 拡張子なしのファイル名取得
                        $all_imgfile[] = $filename;
                    }
                }
            }
        }
        //dd( $all_imgfile);

        //リターンview
        return view('system.fileupload',compact('courseWords','course_name','word_name','all_imgfile'));
    }

    /**
     * ★【管理用】画像ファイルアップロード
     * 画像はワードIDを利用（例）ワードIDが1432⇒1432.png
     * @return redirect('/fileupload');
     */
     
    public function upload(Request $request)
    {
        logger()->debug('<-- FileUploadController@upload');
        //UPしたファイル名を取得        
        $file_name = $request->file('file')->getClientOriginalName();
        //バトルモードの英語イラストか、英語イラスト表紙かで保存するパスが異なる
        if($request->category=='upload_english'){
            $directory = public_path('img/english');
        } elseif($request->category=='upload_english_top'){
            $directory = public_path('img/english/top');
        } else {
            $directory = public_path('img/map');
        }
        //ファイルをアップロード
        try {
            $request->file('file')->move($directory, $file_name);
            session()->flash('flash_message', '画像をアップロードしました。'); 
        }catch(Exception $e){
            logger()->error('★画像をアップロードする時のエラー');
            logger()->error($e);
            abort(500);
        }

        //画像アップロードのTOPページにリダイレクト
        return redirect('/fileupload');
    }
     
    /**
     *★【管理用】画像の削除
     * 【重要】DBのカラムに画像パスはない。ストレージのファイルをを削除するのみ。DBは削除しない
     * @return redirect('/fileupload')
     */
    public function delete(Request $request)
    {        
        //削除する単語IDが$checks　に配列として格納
        
        $checks = $request->checks;
        //dd($checks[0]);
        $course = CourseWord::leftjoin('courses','course_word.course_id','courses.id')
            ->leftJoin('words','course_word.word_id','words.id')
            ->where('words.id',$checks[0])
            ->first();
        if($course->tab=='english' || $course->tab=='battle'){

            $directory = public_path('img/english');
        } elseif($course->tab=='map'){
            $directory = public_path('img/map');
        } else{
            $directory = public_path('img/english');
        }
            dd( $directory);
        try {
            foreach($checks as $delete_file_name){

                unlink($directory.'/'.$delete_file_name.'.png');
            }

        }catch(\Exception $e){
            logger()->error('システム管理メニュー、画像削除でエラー');
            logger()->error($e);
            abort(500);
        }
        session()->flash('flash_message', '画像を削除しました。'); 
        
        //画像アップロードのTOPページにリダイレクト
        return redirect('/fileupload');
    }
}