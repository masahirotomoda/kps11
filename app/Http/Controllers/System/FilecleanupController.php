<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Word;
use App\Models\Blog;
use Exception;

    /**
     * システム管理メニュー　音声ファイル、画像ファイルの整理
     * ページネーションなし
     * ※更新や削除でストレージに残っている音声ファイルを削除する
     * (1)使っていない音声ファイル一覧
     * (2)削除
     */
class FilecleanupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin.role');
    }
    /**
     * ★音声ファイル一覧
     * Route::get
     * @return view('system.filecleanup')
     */
    public function index()
    {
        $status_file_selectbox1 = 'checked';
        $status_file_selectbox2 = false;
        $file_category = 'mp3';

        //ストレージのaudioにあるファイルをすべて取得（例：piblic/audio/aaaa123.mp3)
        //使っているものと使っていない音声ファイルが混在している
        $directory = 'public/audio';
        $file_info = Storage::files($directory);        

        //wordテーブルから音声ファイルをすべて取得（使っている音声ファイルのみ）
        $words = Word::select('pronunciation_file_name')
            ->where('pronunciation_file_name','<>','')
            ->get();        

        //wordテーブルから取得した音声ファイル名を配列にする（toarray関数が使えなかったため）
        foreach ($words as $word){            
            $is_pronunciation_file[]=$word->pronunciation_file_name;
        }
        
        //ファイル名（パス付）をファイル名だけにする
        //スト=ージにあるファイルが、wordテーブルにある単語につかわれていないものを「 $is_not_pronunciation_file」に入れる
        $is_not_file=null; //初期化
        foreach ($file_info as $file){
            $_file=(substr( $file, 13)); //public/audio/ で13文字以降のファイル名のみ取得
            if(!in_array($_file, $is_pronunciation_file)){
                $is_not_file[]=$_file;
            }
        }

        //未使用音声ファイルがない時、$is_not_fileは初期化のままnull、つまりカウントできず、エラーになる。
        if($is_not_file === null){
            $is_not_file_count = null;
        } else {
            //使っていない音声ファイルが存在すれば、カウントする。
            $is_not_file_count = count($is_not_file);
        }

        $SoundOrImage = 'sound';
        
        //リターンview
        return view('system.filecleanup',compact('is_not_file','is_not_file_count','status_file_selectbox1','status_file_selectbox2','file_category','SoundOrImage'));
    }
    /**
     * ★音声ファイルか画像ファイルかラジオボタン選択
     * Route::post
     * @return view('system.filecleanup')
     */
    public function select(Request $request)
    {
        if($request->file_selectbox =='mp3'){
            return $this -> index();
        } elseif($request->file_selectbox =='img'){
            //ストレージのblogimagesにあるファイルをすべて取得（例：public/blogimages/16661146101.jpg)
            //使っているものと使っていないファイルが混在している
            $directory = 'public/blogimages';
            $file_info = Storage::files($directory);        
            $file_category = 'img';
            
            //blogテーブルからファイルをすべて取得（使っているファイルのみ）
            $blog_images = Blog::select('image')
            ->where('image','<>','')
            ->get();        
            //blogテーブルから取得したファイル名を配列にする
            foreach ($blog_images as $blog_image){            
                $is_image_file[]=$blog_image->image;
            }
            //初期化
            $is_not_file=null;
            foreach ($file_info as $file){
                $_file=(substr( $file, 18)); //public/blogimages/ で18文字以降のファイル名のみ取得
                if(!in_array($_file, $is_image_file)){
                    $is_not_file[]=$_file;
                }
            }                    
            
            //未使用画像ファイルがない時、$is_not_fileは初期化のままnull、つまりカウントできず、エラーになる。
            if($is_not_file === null){
                $is_not_file_count = null;
            } else {
                //使っていない画像ファイルが存在すれば、カウントする。
                $is_not_file_count = count($is_not_file);
            }
            $status_file_selectbox1 = null;
            $status_file_selectbox2 = 'checked';

            $SoundOrImage = 'image';
 
            //リターンview
            return view('system.filecleanup',compact('is_not_file','is_not_file_count','status_file_selectbox1','status_file_selectbox2','file_category','SoundOrImage'));
    }
}

    /**
     * ★削除
     * @return  return redirect('/file'
     */
    public function delete(Request $request)
    {
        logger()->debug('<-- FilecleanupController@delete');
        //dd($request->all());
        //削除するコースのコースIDが[「$checks」に配列として格納
        $checks = $request->checks;

        //削除処理
        if($checks !== null){
            if($request->hidden_file_selectbox == 'mp3'){
                $file_category_path = 'audio';
                $message = '未使用の音声ファイルを削除しました。';
            } else {
                $file_category_path = 'blogimages';
                $message = '未使用の画像ファイルを削除しました。';
            }
            try{           
                    foreach($checks as $delete_file_name){
                        Storage::delete('public/'.$file_category_path.'/'.$delete_file_name);
                    }
                    //削除成功のフラッシュメッセージ
                    session()->flash('flash_message', $message);
            }catch(Exception $e){
                    logger()->error('◆システム管理メニュー、未使用の音声or画像ファイル削除でエラー');
                    logger()->error($e);
                    abort(500);
            }
        } else {
            session()->flash('flash_message', '※何も削除していません！！削除項目にチェックをしてから削除ボタンをおしてください。');
        }            
        //リダイレクト
        return redirect('/file');
    }    
}
