<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Note;


    /**
     * システム管理メニュー　ノート（開発メモ）　
     * (1)ノート一覧表示
     * (2)検索
     * (3)新規、編集画面へ遷移
     * (4)削除＆新規＆更新
     */
$pagenation = 200;
class NoteController extends Controller
{
    public function __construct()
    {
        logger()->debug('<-- NoteController@construct');
        $this->middleware('auth');
        $this->middleware('admin.role');
        global $pagenation;
        $pagenation = config('configrations.PAGENATION');
    }
    /**
     * ★ノート一覧
     * Route::get  ※ページネーションあり
     * @return view('system.note')
     */
    public function index()
    {
        logger()->debug('<-- NoteController@index');
        global $pagenation;

        //検索ボックス初期値
        $category = null;
        $memo1 = null; //内容
        $memo2 = null; //進捗

        //メンテナンス情報データ
        $notes = note::orderby('updated_at','DESC')
            ->paginate($pagenation);
        //リターンview
        return view('system.note',compact('notes','category','memo1','memo2'));
    }
    /**
     * ★検索
     * Route::match(['get', 'post']　※ページネーションあり
     * @return view('system.note')
     */
    public function search(Request $request)
    {
        logger()->debug('<-- NoteController@search');
        global $pagenation;

        //検索値を変数にセット
        $category = $request->input('s_category',null);
        $memo1 = $request->input('s_memo1',null);//内容
        $memo2 = $request->input('s_memo2',null);//進捗

        //検索後のノートデータ        
        $notes = note::when($category != null ,function($q) use($category){
                return $q->where('notes.category','Like',"%$category%");
            })
            ->when($memo1 != null ,function($q) use($memo1){
                return $q->where('notes.memo1','Like',"%$memo1%");
            })
            ->when($memo2 != null ,function($q) use($memo2){
                return $q->where('notes.memo2','Like',"%$memo2%");
            })
            ->orderby('updated_at','DESC')
            ->paginate($pagenation);

         //リターンview
         return view('system.note',compact('notes','category','memo1','memo2'));
    }   
    /**
     * ★編集ぺージへ遷移　ボタンで分岐(1)新規ボタン　(2)編集ボタン
     * Route::post  ページネーションなし
     * @return view('system.note')
     */
    public function edit(Request $request)
    {
        logger()->debug('<-- NoteController@add');        

        //新規ボタン押下
        if($request -> has('add')){
            $note = new Note();

        //編集ボタン押下
        } else if($request -> has('edit')){
            $note = Note::find($request -> input('id'));
           
        //それ以外エラー
        } else {        
            logger()->error('◆システム管理メニュー、ノートのボタン押下でないエラー');
            abort(500);
        }

        //リターンview
        return view('system.noteedit',compact('note'));
    }
    /**
     * ★新規＆更新保存＆削除　まとめて 
     * 削除、更新、新規保存はボタンで分岐
     * 「title」カラムが記事本文
     * Route::post
     * @return view('system.note')
     */
    public function update(Request $request)
    {
        logger()->debug('<-- NoteController@deliUpdate');       

        //dd($request->all());
        //削除ボタン押下時（モーダルの削除ボタン）
        if ($request -> has('modal_del_btn')){
            try{       
                Note::destroy($request -> input('id'));
                session()->flash('flash_message', '【削除】削除しました');
            }catch(\Exception $e){
                logger()->error('◆システム管理メニュー、ノート削除でエラー');
                logger()->error($e);
                DB::rollBack();
                abort(500);
            }
            //リターンview
            return redirect('/note');

        //新規の場合
        } elseif ($request -> id  === null){ 
            $note = new Note();
            $flashmessage = '【新規登録】新規登録しました';

        //更新の場合
        } else {
            $note = Note::find($request->input('id'));
            $flashmessage = '【更新】更新しました';
        }
        //本文（memo1 or memo2カラム）が空欄だと、保存エラーでabort(500)に飛ばされるため、ここでバリデーション
        if($request->input('category') === null || $request->input('memo1') === null || $request->input('memo2') === null){
            return redirect('/note')
                ->withErrors('カテゴリー、本文、完了項目は必須です');
        }

        //ここから共通処理、データをセット
        $note->category = $request->input('category');
        $note->memo1 = $request->input('memo1');
        $note->memo2 = $request->input('memo2');
       
        //保存
        try{
            $note -> save();
            session()->flash('flash_message', $flashmessage);

        }catch(\Exception $e){
            logger()->error('◆システム管理メニュー、ノートの新規or更新でエラー');
            logger()->error($e);
            abort(500);
        }
        //リダイレクト
        return redirect('/note');
    }
}
