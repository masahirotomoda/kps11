<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        logger()->debug('<-- HomeContoroller@construct');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    $school_id = Auth::user()->school_id;
    logger()->debug('<-- HomeContoroller@index school_id='.$school_id);
    logger()->debug(url()->current());
    if(is_null($school_id)){
        return route('/');
    }else{
        switch(Auth::user()->role){
            case 'システム管理者':
                return redirect('/'.$school_id.'/system-menu');
                break;

            case '学校管理者':
                return redirect('/'.$school_id.'/school/menu');
                break;

            case '先生':
            case '生徒':
                return redirect('/'.$school_id.'/user-menu');
                break;
            default :
        }
        return redirect('/');
    }
    }
    public function trialTerm()
    {
        logger()->debug('<-- HomeContoroller@trialTerm');

        return view('trial.terms');

    }


}
