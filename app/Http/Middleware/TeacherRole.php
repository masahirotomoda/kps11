<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class TeacherRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        logger()->debug('<-- TeacherRole@handle');
        if(auth()->user()->role === '先生'){
                return $next($request);
        }
        logger()->warning('access violation. '.auth()->user());
        abort(403);
    }
}
