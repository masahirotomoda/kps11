<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  string|null  ...$guards
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        logger()->debug('<-- RedirectIfAuthenticated@handle');
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                logger()->debug('Guard true');
                logger()->debug('LoginRole Cookie::' . $request->Cookie('ntyping_last_login_role'));

                switch($request->Cookie('ntyping_last_login_role')){
                    case 'システム管理者':
                        return redirect('/system-menu');
                        break;

                    case '学校管理者':
                        return redirect('/school/menu');
                        break;
                    case '先生':
                    case '生徒':
                        logger()->debug('User');
                        return redirect('/user-menu');
                        break;
                    default :
                        return redirect('/');
                        break;
                }
            }
        }
       return $next($request);
    }
}
