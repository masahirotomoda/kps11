<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UrlVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        logger()->debug($request);
        logger()->debug(auth()->user()->school_id);

        if($request->schoolID === auth()->user()->school_id ){
            return $next($request);
        }
        logger()->warning('access violation. '.auth()->user());
        abort(403);
    }
}
