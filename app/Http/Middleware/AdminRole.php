<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\IpUtils;
class AdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    private const ALLOWED_IPS = [
        '127.0.0.1', // ローカルからのアクセスは許可
        '183.77.254.227', //ナレッジプログラミングスクール
        '153.242.88.0', //市川自宅 NTT 2024-3-25 変更
        '60.68.207.154/32', //Innovation Plus
    ];

    public function handle(Request $request, Closure $next)
    {
        if(auth()->user()->role === 'システム管理者' ){
            if (config('app.env') === 'local' || $this->isAllowedIp($request->ip())) {
                logger()->debug('IP OK!');
                return $next($request);
            }
            logger()->warning('Access denied from '.$request->ip());
            throw new AuthorizationException(sprintf('Access denied from %s', $request->ip()));
        }
        logger()->warning('access violation. '.auth()->user());
        abort(403);
    }
    /**
     * @param string $ip
     * @return bool
     */
    private function isAllowedIp(string $ip): bool
    {
        return IpUtils::checkIp($ip, self::ALLOWED_IPS);
    }

}
