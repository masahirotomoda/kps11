<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        logger()->debug('<-- Authenticate@redirectTo');
        if (! $request->expectsJson()) {
            //ログインしていない場合の遷移先
            switch($request->Cookie('ntyping_last_login_role')){
                case 'システム管理者':
                    logger()->debug('Last login is system admin');
                    return '/system-login';
                    break;

                case '学校管理者':
                    logger()->debug('Last login is school admin');
                    return '/school-login';
                    break;

                case '先生':
                case '生徒':
                    logger()->debug('Last login is user');
                    return '/login';
                    break;

                default :
                    return '/';
                    break;

            }
            return route('logout');
        }
    }
}
