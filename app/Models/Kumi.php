<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kumi extends Model
{
    use HasFactory;

    /// 主キーカラム名を指定
    protected $primaryKey = 'id';

    protected $table = 'kumi';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [
        'id',
    ];
    public static function selectList()
    {
        $kumis = Kumi::all();
        $list = array();
        $list += array(null=>'');
        foreach ($kumis as $kumi) {
           $list += array( $kumi->name => $kumi->name );
        }
        natcasesort($list);
        return $list;
    }
    public static function selectListId()
    {
        $kumis = Kumi::all();
        $list = array();
        foreach ($kumis as $kumi) {
           $list += array( $kumi->id => $kumi->name );
        }
        $list += array(null=>'');
        natcasesort($list);
        return $list;
    }
    public static function selectListById($id)
    {
        $grades = Kumi::where('school_id','=',$id)
            ->orderByRaw('CHAR_LENGTH(name) ASC')
            ->orderBy('name','ASC')
            ->get();

        $list = array();
        foreach ($grades as $grade) {
            $list += array( $grade->id => $grade->name );
        }
        natcasesort($list);
        return $list;
    }
}
