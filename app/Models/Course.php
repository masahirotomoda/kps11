<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * モデルと関連しているテーブル
     *
     * @var string
     */
    use HasFactory;

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /// 主キーカラム名を指定
    protected $primaryKey = 'id';

/*public static function selectLevelList()
    {
        $levels = Course::groupBy('course_level')->select('course_level')->get();
        $list = array();
        foreach ($levels as $lv) {
           $list += array( $lv->course_level => $lv->course_level );
        }
        ksort($list);
        return $list;
    }
    public static function selectCategoryList()
    {
        $category = Course::groupBy('course_category')->select('course_category')->get();
        $list = array();
        foreach ($category as $ct) {
           $list += array( $ct->course_category => $ct->course_category );
        }
        ksort($list);
        return $list;
    }
    */
    public static function selectLevelList()
    {
        $levels = config('configrations.LEVEL_LIST');
        return $levels;
    }
    public static function selectCategoryList()
    {
        $levels = config('configrations.CATEGORY_LIST');
        return $levels;
    }


}
