<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $table = 'blog';
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
    // 主キーカラム名
    protected $primaryKey = 'id';
}
