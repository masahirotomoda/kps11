<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BestScore extends Model
{
    use HasFactory;
    protected $table = 'course_with_bestpoint';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
    /// 主キーカラム名を指定
    protected $primaryKey = 'id';

}
