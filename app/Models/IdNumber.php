<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IdNumber extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $guarded = [
        'id',
    ];

    /// 主キーカラム名を指定
    protected $primaryKey = 'id';

}
