<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BattleScoreHistory extends Model
{
    use HasFactory;

    protected $table = 'battlescore_history';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /// 主キーカラム名を指定
    protected $primaryKey = 'id';

}
