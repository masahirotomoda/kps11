<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class School extends Model
{
    use HasFactory;
    use Sortable;

    /// 主キーカラム名を指定
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public static function selectList()
    {
        $schools = School::all();
        $list = array();
        foreach ($schools as $school) {
           $list += array( $school->id => $school->name );
        }
        ksort($list);
        return $list;
    }

    public static function selectListByName($name)
    {
        $schools = School::where('name','Like',"%$name%")->get();
        $list = array();
        foreach ($schools as $school) {
           $list += array( $school->id => $school->name );
        }
        ksort($list);
        return $list;
    }
    //フランチャイズなどオリジナルロゴ、ロゴカラムの番号を取得
    public static function getSchoolLogoNo($school_Int_Id)
    {
        $School_Logo_No = School::where('id',$school_Int_Id)->select('logo')->first();
        return $School_Logo_No;
    }
}
