<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'grade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [
        'id',
    ];
    
    public static function selectList()
    {
        $grades = Grade::all();
        $list = array();
        foreach ($grades as $grade) {
           $list += array( $grade->name => $grade->name );
        }
        $list += array(null=>'');
        //natcasesort($list); //効いていない
        return $list;
    }
    public static function selectListId()
    {
        $grades = Grade::all();
        $list = array();
        foreach ($grades as $grade) {
           $list += array( $grade->id => $grade->name );
        }
        $list += array(null=>'');
        natcasesort($list);
        return $list;
    }

    public static function selectListById($id)
    {
        logger()->debug('school_int_id = '.$id);
        $grades = Grade::where('school_id','=',$id)
            ->orderByRaw('CHAR_LENGTH(name) ASC')
            ->orderBy('name','ASC')
            ->get();
        $list = array();
        foreach ($grades as $grade) {
            $list += array( $grade->id => $grade->name );
        }
        natcasesort($list);
        return $list;
    }
}
