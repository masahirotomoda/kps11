<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AudioFile extends Model
{
    use HasFactory;

    // テーブル名
    protected $table = 'audio_files';

    // アクセスさせないカラム名
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    // 主キーカラム名
    protected $primaryKey = 'id';
}
