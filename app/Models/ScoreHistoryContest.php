<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScoreHistoryContest extends Model
{
    use HasFactory;

    protected $table = 'score_history_contest';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /// 主キーカラム名を指定
    protected $primaryKey = 'id';

}
