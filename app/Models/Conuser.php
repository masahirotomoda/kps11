<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conuser extends Model
{
    /**
     * モデルと関連しているテーブル
     *
     * @var string
     */
    use HasFactory;

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    // 主キーカラム名を指定
    protected $primaryKey = 'id';
}
