<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScoreHistory extends Model
{
    use HasFactory;

    protected $table = 'score_history';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /// 主キーカラム名を指定
    protected $primaryKey = 'id';

}
