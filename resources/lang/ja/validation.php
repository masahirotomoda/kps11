<?php

return [

    /*
    |--------------------------------------------------------------------------
    | バリデーション言語行
    |--------------------------------------------------------------------------
    |
    | 以下の言語行はバリデタークラスにより使用されるデフォルトのエラー
    | メッセージです。サイズルールのようにいくつかのバリデーションを
    | 持っているものもあります。メッセージはご自由に調整してください。
    |
    */

    'accepted'             => ':attributeを承認してください。',
    'accepted_if' => ':otherが:valueの場合、:attributeを承認してください。',
    'active_url'           => ':attributeが有効なURLではありません。',
    'after'                => ':attributeには、:dateより後の日付を指定してください。',
    'after_or_equal'       => ':attributeには、:date以降の日付を指定してください。',
    'alpha'                => ':attributeはアルファベットのみがご利用できます。',
    'alpha_dash'           => ':attributeはアルファベットとダッシュ(-)及び下線(_)がご利用できます。',
    'alpha_num'            => ':attributeはアルファベット数字がご利用できます。',
    'array'                => ':attributeは配列でなくてはなりません。',
    'before'               => ':attributeには、:dateより前の日付をご利用ください。',
    'before_or_equal'      => ':attributeには、:date以前の日付をご利用ください。',
    'between'              => [
        'numeric' => ':attributeは、:minから:maxの間で指定してください。',
        'file'    => ':attributeは、:min kBから、:max kBの間で指定してください。',
        'string'  => ':attributeは、:min文字から、:max文字の間で指定してください。',
        'array'   => ':attributeは、:min個から:max個の間で指定してください。',
    ],
    'boolean'              => ':attributeは、true(1)かfalse(0)を指定してください。',
    'confirmed'            => ':attributeと、確認フィールドとが、一致していません。',
    'confirm'              => '「利用条件に同意する」をチェックしてください。',
    'current_password'     => 'パスワードが正しくありません。',
    'date'                 => ':attributeには有効な日付を指定してください。',
    'date_equals'          => ':attributeには、:dateと同じ日付けを指定してください。',
    'date_format'          => ':attributeは:format形式で指定してください。',
    'different'            => ':attributeと:otherには、異なった内容を指定してください。',
    'digits'               => ':attributeは:digits桁で指定してください。',
    //'digits_between'       => ':attributeは:min桁から:max桁の間で指定してください。',
    'digits_between'       => ':attributeは数字（:min～:max）で入力してください',
    'dimensions'           => ':attributeの図形サイズが正しくありません。',
    'distinct'             => ':attributeには異なった値を指定してください。',
    'email'                => ':attributeには、有効なメールアドレスを指定してください。',
    'ends_with'            => ':attributeには、:valuesのどれかで終わる値を指定してください。',
    'exists'               => '選択された:attributeは正しくありません。',
    'file'                 => ':attributeにはファイルを指定してください。',
    'filled'               => ':attributeに値を指定してください。',
    'gt'                   => [
        'numeric' => ':attributeには、:valueより大きな値を指定してください。',
        'file'    => ':attributeには、:value kBより大きなファイルを指定してください。',
        'string'  => ':attributeは、:value文字より長く指定してください。',
        'array'   => ':attributeには、:value個より多くのアイテムを指定してください。',
    ],
    'gte'                  => [
        'numeric' => ':attributeには、:value以上の値を指定してください。',
        'file'    => ':attributeには、:value kB以上のファイルを指定してください。',
        'string'  => ':attributeは、:value文字以上で指定してください。',
        'array'   => ':attributeには、:value個以上のアイテムを指定してください。',
    ],
    'image'                => ':attributeには画像ファイルを指定してください。',
    'in'                   => '選択された:attributeは正しくありません。',
    'in_array'             => ':attributeには:otherの値を指定してください。',
    'integer'              => ':attributeは整数で入力してください。',
    'ip'                   => ':attributeには、有効なIPアドレスを指定してください。',
    'ipv4'                 => ':attributeには、有効なIPv4アドレスを指定してください。',
    'ipv6'                 => ':attributeには、有効なIPv6アドレスを指定してください。',
    'json'                 => ':attributeには、有効なJSON文字列を指定してください。',
    'lt'                   => [
        'numeric' => ':attributeには、:valueより小さな値を指定してください。',
        'file'    => ':attributeには、:value kBより小さなファイルを指定してください。',
        'string'  => ':attributeは、:value文字より短く指定してください。',
        'array'   => ':attributeには、:value個より少ないアイテムを指定してください。',
    ],
    'lte'                  => [
        'numeric' => ':attributeには、:value以下の値を指定してください。',
        'file'    => ':attributeには、:value kB以下のファイルを指定してください。',
        'string'  => ':attributeは、:value文字以下で指定してください。',
        'array'   => ':attributeには、:value個以下のアイテムを指定してください。',
    ],
    'max'                  => [
        'numeric' => ':attributeには、:max以下の数字を指定してください。',
        'file'    => ':attributeには、:max kB以下のファイルを指定してください。',
        'string'  => ':attributeは、:max文字以下で指定してください。',
        'array'   => ':attributeは:max個以下指定してください。',
    ],
    'mimes'                => ':attributeは「:values」形式（150kb以下）を指定してください',
    'mimetypes'            => ':attributeには:valuesタイプのファイルを指定してください。',
    'min'                  => [
        'numeric' => ':attributeには、:min以上の数字を指定してください。',
        'file'    => ':attributeには、:min kB以上のファイルを指定してください。',
        'string'  => ':attributeは、:min文字以上で指定してください。',
        'array'   => ':attributeは:min個以上指定してください。',
    ],
    'multiple_of' => ':attributeには、:valueの倍数を指定してください。',
    'not_in'               => '選択された:attributeは正しくありません。',
    'not_regex'            => ':attributeの形式が正しくありません。',
    'numeric'              => ':attributeには、数字を指定してください。',
    'password'             => '正しいパスワードを指定してください。',
    'present'              => ':attributeが存在していません。',
    'regex'                => ':attributeに正しい形式を指定してください。',
    'required'             => ':attributeは必ず入力してください。',
    'required_if'          => ':otherが:valueの場合、:attributeも指定してください。',
    'required_unless'      => ':otherが:valuesでない場合、:attributeを指定してください。',
    'required_with'        => ':valuesを指定する場合は、:attributeも指定してください。',
    'required_with_all'    => ':valuesを指定する場合は、:attributeも指定してください。',
    'required_without'     => ':valuesを指定しない場合は、:attributeを指定してください。',
    'required_without_all' => ':valuesのどれも指定しない場合は、:attributeを指定してください。',
    'prohibited'           => ':attributeは入力禁止です。',
    'prohibited_if' => ':otherが:valueの場合、:attributeは入力禁止です。',
    'prohibited_unless'    => ':otherが:valueでない場合、:attributeは入力禁止です。',
    'prohibits'            => 'attributeは:otherの入力を禁じています。',
    'same'                 => ':attributeと:otherには同じ値を指定してください。',
    'size'                 => [
        'numeric' => ':attributeは:sizeを指定してください。',
        'file'    => ':attributeのファイルは、:sizeキロバイトでなくてはなりません。',
        'string'  => ':attributeは:size文字で指定してください。',
        'array'   => ':attributeは:size個指定してください。',
    ],
    'starts_with'          => ':attributeには、:valuesのどれかで始まる値を指定してください。',
    'string'               => ':attributeは文字列を指定してください。',
    'timezone'             => ':attributeには、有効なゾーンを指定してください。',
    'unique'               => ':attributeの値は既に存在しています。',
    'uploaded'             => ':attributeのアップロードに失敗しました。',
    'url'                  => ':attributeに正しい形式を指定してください。',
    'uuid'                 => ':attributeに有効なUUIDを指定してください。',

    /*
    |--------------------------------------------------------------------------
    | Custom バリデーション言語行
    |--------------------------------------------------------------------------
    |
    | "属性.ルール"の規約でキーを指定することでカスタムバリデーション
    | メッセージを定義できます。指定した属性ルールに対する特定の
    | カスタム言語行を手早く指定できます。
    |
    */

    'custom' => [
        '属性名' => [
            'ルール名' => 'カスタムメッセージ',
        ],
    ],
    'custom' => [
        'student_class_id' => [
            'required' => '「学年」か「クラス」がまちがっています。',
        ],

        'attendance_no' => [
            'numeric' => '出席番号は、数字（4文字まで）です。',
            'required' => '出席番号は、かならず入力しよう。',
        ],

        'student_change_password' => [
            'confirmed' => '「1回目」と「2回目」のパスワードがちがいます。',
            'numeric' => 'パスワードは、数字４～６文字です。',
            'regex' => 'パスワードは、数字４～６文字です。',
            'required' => 'パスワードは、かならず入力しよう。',
        ],

        'name' => [
            //'string' => '名前は文字で入力してください',
            'max' => '名前の最大文字数は「20文字」です。',
            'required' => '名前を必ず入力してください。',
        ],

        'change_password' => [
            'confirmed' => '1回目と2回目のパスワード入力が違います。',
            'required' => 'パスワードを必ず入力してください。',
            'regex' => 'パスワードは、①6～20文字　②アルファベット（大文字か小文字、どちらか1つ以上）、③数字が必要です。※全角不可（例：ｗ52931)'
        ],
        'change_password_school' => [
            'confirmed' => '1回目と2回目のパスワード入力が違います。',
            'required' => 'パスワードを必ず入力してください。',
            'regex' => 'パスワードは、①10～20文字　②アルファベット（大文字・小文字）　③数字　④記号（!#$%& ）のうち、3種類以上必要です。※全角不可',
        ],
        
        'studentlist_user_name' => [
            'required' => '生徒名は必須です。',
            'max' => '生徒名は最大20文字です。',
        ],
        'teacherlist_user_name' => [
            'required' => '先生名は必須です。',
            'max' => '先生名は最大20文字です。',
        ],
        'studentlist_attendance_no' => [
            'required' => '出席番号は必須です。出席番号がない時は、「0」（ゼロ）を入力して、再度、登録してください。',
            'digits_between' => '出席番号は、で0～9999までの数字で入力してください',
        ],
        
        'grade_id' => [
            'required' => '「学年」を選択してください。',
        ],
        'kumi_id' => [
            'required' => '「組」を選択してください。',
        ],
        'display_order' => [
            'integer' => '「表示順」は数字（0～9999、最大4桁）で入力してください。',
        ],
        'studentlist_clas' => [
            'required' => '「学年」か「組」の組み合わせが間違っています。確認して、再度、登録してください。',
        ],
        'system_user_clas_id' => [
            'required' => '「学年」か「組」の組み合わせが間違っています。確認して、再度、登録してください。',
        ],
        'over_max_student_num' => [
            'required' => '契約している「利用者数」を超えたため、新規登録はできません。学校管理者または、ナレッジタイピング事務局にお問い合わせください。',
        ],
        'over_max_user_num' => [
            'required' => '契約している「利用者数」を超えたため、新規登録はできません。学校管理者または、ナレッジタイピング事務局にお問い合わせください。',
        ],
        'display_order_unique_count' => [
            'max' => '「コース表示順」が他のコースと重複しています。重複しない数字（0～9999まで）を入力してください。',
       ],
        'excel_file' => [
            'required' => 'エクセルファイルをアップロードしてください。',
            //'max' => '容量オーバーです。ファイルサイズは１M（メガ）までです', //maxがきかない
            'between' => '容量オーバーです。ファイルサイズは500KB（キロバイト）までです。',
            'mimes' => 'ファイルはエクセル2016以降で拡張子は「.xlsx」です。',
            'mimetypes' => 'アップロードできるのは「マイクロソフト社」の「エクセル」ファイルです。',   
        ],






    ],


    /*
    |--------------------------------------------------------------------------
    | カスタムバリデーション属性名
    |--------------------------------------------------------------------------
    |
    | 以下の言語行は、例えば"email"の代わりに「メールアドレス」のように、
    | 読み手にフレンドリーな表現でプレースホルダーを置き換えるために指定する
    | 言語行です。これはメッセージをよりきれいに表示するために役に立ちます。
    |
    */

    'attributes' => [
        'user_name' => '名前',
        'attendance' => '出席番号',
        'display_order' => '表示順',
        'name' => '名前',
        'teachers_name' => '先生名',
        'mail' => '電子メール',
        'tel' => 'TEL',
        'address' => '所在地',
        'industry' => '業種',
        'password1st' => 'パスワード',
        'password2nd' => 'パスワード（確認用）',
        'school_id' => '学校ID',
        'user_number' => '利用者数',
        'course_number' => '追加コース数',
        'word_number' => '追加単語数',
        'password_school_init' => 'パスワード初期値（学校管理）',
        'password_teacher_init' => 'パスワード初期値（先生）',
        'password_student_init' => 'パスワード初期値（生徒）',
        'username' => '名前',
        'course_name' => 'コース名',
        'word_mana' => '単語',
        'word_kana' => '「よみがな」では「カタカナ」「漢字」は使えません。詳しくは、この画面右上の「？単語の登録方法」で確認してください。単語（よみがな）',
        'audio' => '音声ファイル',
        'change_password_school' => 'パスワード',
        'grade_name_new' => '学年',
        'grade_name' => '学年',
        'kumi_name_new' => '組',
        'kumi_name' => '組',
        'system_user_name' => 'ユーザー名（最大20文字）',
        'course_category' => 'コースカテゴリー',
        'course_level' => 'コースレベル',
        'course_type' => 'コースタイプ',
        'test_time' => 'テストモードの時間',
        'title' => '保存に失敗しました。ブログタイトル',
        'article' => '保存に失敗しました。ブログ記事',
        'grade_id' => '学年',
        'kumi_id' => '組',
        'category' => '運営形態',
        'new_school_int_id' => '学校管理ID（例）18',
        'new_school_id' => '学校ID（例）abc12345',
        'new_grade_name' => '学年（例）1年',
        'new_kumi_name' => '組（例）3組',
        'new_trial_flg' => 'ﾄﾗｲｱﾙﾌﾗｸﾞ（ﾄﾗｲｱﾙ解除「0」、ﾄﾗｲｱﾙ「1」）',
        'password' => 'パスワード',
        'login_account' => 'ログインID',
        'kumi' => '組',
        'grade' => '学年',

    ],

];
