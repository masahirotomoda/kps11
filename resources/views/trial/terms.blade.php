@extends('layouts.trial')
@section('head')
@endsection

@section('content')
<div class="container textpage">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <h2>「ナレッジタイピング for School 無料トライアル」利用規約</h2>
                <div class="card-body">
                    ナレッジ・プログラミングスクール（以下「当スクール」といいます）がサービスを提供している「ナレッジタイピング for School 無料トライアル」（以下「本サービス」といいます）を利用する法人又は団体利用者（以下「登録事業者」といいます）は、以下の利用規約に同意したものとし ます。
                    <br>
                    <br>
                    <strong>第１条【登録】</strong><br> 登録事業者は、本サービス利用開始にあたり、申込みに必要な情報を当スクールに提供し、それら情報に変更があった場合、すみやかに通知する必要があります。
                    <br>
                    <br>
                    <strong>第2条【アカウント管理】</strong><br>
                    <ul>
                        <li>登録事業者は、本サービス利用に必要なアカウント情報を、自己の責任において適切に管理、保管し、第三者に利用させたり、貸与、譲渡、名義変更、売買等をしてはいけません。</li>
                        <li>アカウント情報の管理不十分、使用上の過誤、第三者の使用等による損害の責任は登録事業者が負うものとし、当スクールは一切の責任を負いません。</li>
                    </ul>
                    <br>
                    <strong>第3条【無料トライアル利用対象と目的】</strong><br> 本サービスは、教育，学習支援業（塾、プログラミング教室、幼稚園、各種学校、学童、児童サークル、クラブ等）の法人や団体を対象としており、個人ユーザはご利用できません。 また、「ナレッジタイピング for School」の導入を検討する以外の目的ではご利用できません。
                    <br><br>
                    <strong>第4条【無料トライアル期間】</strong><br>
                    <ul>
                        <li>各種学校は12か月間</li>
                        <li>教室や塾等は6か月間</li>
                    </ul>
                    <br>
                    <strong>第5条【本サービスの利用】</strong><br>
                    <ul>
                        <li>本サービスは、提供しているサービスに対する保証行為を一切していません。
                        </li>
                        <li>登録事業者は、本サービスを利用する受講生や生徒等（以下「利用ユーザ」といいます）に、本規約上の義務を遵守させる必要があり、利用ユーザによる行為は登録事業者の行為とみなして一切の責任を負うものとします。
                        </li>
                        <li>本サービスは、予告なく機能変更、運用停止、廃止することがあります。
                        </li>
                    </ul>
                    <br>
                    <strong>第6条【プライバシーポリシー】</strong><br> 登録事業者または、利用ユーザの個人情報の取扱いについては、プライバシーポリシーをご覧ください。
                    <br>
                    <br>
                    <strong>第7条【著作権等】</strong><br> 本サービスで提供している情報、内容、画面構成、ソースコード、マニュアル、ドキュメント等に関する著作権、その他の知的財産権は当スクールに帰属します。
                    <br>
                    <br>
                    <strong>第8条【禁止事項】</strong><br> 登録事業者や利用ユーザが以下の行為を行うことを禁じます。違反した場合、登録取消や本サービスへのアクセスを停止することがあります。これらの措置に対 して登録事業者や利用ユーザに生じた損害について当スクールは一切の責任を負いません。
                    <br>
                    <ul>
                        <li>無料トライアル申込みの際に、虚偽のデータを送信、または第三者になりすます行為</li>
                        <li>アカウントを第三者に譲渡、販売する行為</li>
                        <li>本サービスの一部、又は全部の複製、改変、または公開、送信、送信可能化、頒布、譲渡、貸与、翻訳、翻案、使用許諾、転載、商品化、再利用等する行為</li>
                        <li>当スクールまたは第三者に損害を与える行為</li>
                        <li>法令に違反する行為</li>
                        <li>本サービスのデータの改変、リバースエンジニアリングその他の解析行為</li>
                        <li>通常の利用範囲を超えてサーバーに負担をかける行為</li>
                        <li>本サービスの利用を妨害または支障をきたす行為</li>
                        <li>その他、当スクールが不適切と判断する行為</li>
                    </ul>
                    <br>
                    <strong>第9条【本サービスの停止等】</strong><br>
                    <ol>
                        <li>当スクールは、以下のいずれかに該当する場合には、登録事業者への事前通知なく、本サービスの一部または全部を停止することができます。</li>
                        <ul>
                            <li>本サービスのシステム保守を行う場合</li>
                            <li>コンピューター、通信回線等が事故により停止した場合</li>
                            <li>天災地変などの不可抗力により本サービスが提供できなくなった場合</li>
                            <li>その他、合理的な理由により当スクールがサービスの停止を必要と判断した場合</li>
                        </ul>
                        <li>当スクールは合理的な判断により、本サービスの提供を終了することができます。</li>
                        <li>本条に基づき当スクールが行った措置において登録事業者に生じた損害（タイピング記録等の消失を含む）について一切の責任を負いません。</li>
                        <br>
                    </ol>
                    <strong>第10条【免責事項】</strong><br>
                    <ul>
                        <li>本サービスを利用したことにより直接的または間接的にユーザに発生した損害については、一切賠償しません。</li>
                        <li>システム障害等で本サービスが利用できなくなってもユーザは損害賠償請求権を含む何らの請求権も有しないものとします。</li>
                    </ul>
                    <br>
                    <strong>第11条【損害賠償】</strong><br> 本規約の違反によって当スクールが被害を被った場合、ユーザに対し損害賠償を請求できることに、ユーザは同意します。
                    <br>
                    <br>
                    <strong>第12条【合意管轄】</strong><br> 裁判所での争いとなったときは、東京簡易裁判所又は東京地方裁判所を第一審の専属的合意管轄裁判所とします。
                    <br>
                    <br>
                    <strong>第13条【改訂】</strong><br> 本規約は将来改定されることがあります。必ず最新の利用規約をご確認のうえ、ご利用ください。
                    <br>
                    <br>
                    <p class="rightText">2022年4月1日　制定</p>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
