@extends('layouts.free')
@section('title')
ナレッジタイピング for school | 無料トライアル受付完了
@endsection
@section('description')
ナレッジタイピング for schoolは、生徒管理、成績管理ができる塾・学校向けのタイピング練習アプリです。
@endsection
@section('css')
<link href="/css/style.css?v=20240123" rel="stylesheet">
@endsection
@section('js')
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>
@endsection
@section('content')
<main class="py-4">
    <div class="container teacher-top">
        <div class="pageLinkBlock">
            <div class="mainBlock">
                <div class="trialBlock">
                    <h2 class="ktTitle">トライアル受付完了</h2>
                    <p>先生1名と、生徒40名のアカウントを発行しました。</p>
                    <p class="redconspicuous">※このページを必ず印刷、または保存しておきましょう。<small>ブラウザの「戻る」ボタンは使わないでください。</small></p>
                    <table id="trialPublication">
                        <tbody>
                            <tr>
                                <th scope="row">ログインURL</th>
                                <td>すぐにナレッジタイピングをご利用できます！
                                    <div class="outerBtn"><a href="https://n-typing.com/logout" target="_blank">ナレッジタイピング ログイン画面へ</a></div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">トライアル申込日</th>
                                <td>{{date('Y年n月j日')}}</td>
                            </tr>
                            <tr>
                                <th scope="row">トライアル終了日</th>
                                <td> {{ date('Y年n月j日' ,strtotime($trial->end_of_trial)) }}
                                    <br>※2023年6月30日までβ版として無料トライアルをご利用いただけます。終了日が6月30日以降の場合、そちらが優先されます。
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">トライアルNO</th>
                                <td>{{ $kumi->name }}</td>
                            </tr>
                            <tr>
                                <th scope="row">お問い合わせ先</th>
                                <td>ナレッジ・プログラミングスクール内ナレッジタイピング事務局　<br><a href="https://knowledge-p.jp/contact/" target="_blank">お問合せフォーム</a>　又は info-knowledge@knowledge-p.co.jp</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <table id="trialTableList">
                        <thead>
                            <tr>
                                <th scope="col">名前</th>
                                <th scope="col">学校ID <span class="red">※全員同じ</span></th>
                                <th scope="col">ログインID</th>
                                <th scope="col">パスワード<br><small>ログイン後、変更してください。</samall></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->school_id }}</td>
                                <td>{{ $user->login_account }}</td>
                                @php
                                if($user->role === '先生'){
                                $ps = $school->password_teacher_init;
                                }else{
                                $ps = $school->password_student_init;
                                }
                                @endphp
                                <td>{{ $ps }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="outerBtn"><a href="https://n-typing.com/logout" target="_blank">ナレッジタイピング ログイン画面へ</a></div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection