@extends('layouts.free')
@section('title')
無料トライアル申し込み | ナレッジタイピング for school
@endsection
@section('description')
ナレッジタイピング for schoolは、生徒管理、成績管理、コース作成ができる塾・学校向けのタイピング練習アプリです。
@endsection
@section('css')
<link href="/css/style.css?v=20240123" rel="stylesheet">
@endsection
@section('js')
<script src="/js/trial.js"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>
@endsection
@section('content')
<main class="py-4">
    <div class="container teacher-top">
        <div class="pageLinkBlock">
            <div class="mainBlock">
                <div class="trialBlock">
                    <h2 class="ktTitle"><small>1分で利用開始！</small>トライアル申し込みフォーム</h2>
                    <p>トライアルでは、以下のアカウントを発行します。<small>　※ナレッジタイピング for school は<span class="red">個人でのご利用</span>ができません。</small>
                    </p>
                    <ul class="trialList">
                        <li><span class="trialListTitle">先生1名</span><span class="trialListText">生徒管理、生徒のスコア管理（印刷）機能と生徒同様の「タイピング練習」がご利用できます。<br><small>・オリジナルコース作成機能はトライアルではご利用できません。</small></span></li>
                        <li><span class="trialListTitle">生徒40名</span><span class="trialListText">基本のローマ字五十音からテストモードまで150以上のコースの練習ができ、スコアを記録します。</span></li>
                        <li><span class="trialListTitle">トライアル期間</span><span class="trialListText">小・中・高校、専門学校、大学 ⇒ 3ヵ月、上記以外の各種教室 ⇒ 1ヵ月</span></li>
                    </ul>
                    <p><span class="red">*</span>は必須項目です。</p>
                    <form method="POST" action="{{ route('trialApplication') }}">
                        @csrf
                        <table id="trialTable">
                            <tbody>
                                <tr>
                                    <th>運営形態<span class="red">*</span><small>※個人でのお申し込みはできません</small></th>
                                    <td>{{--★セレクトボックスの値がバリデーションエラーで保持されない。後日対応--}}
                                        {{ Form::select('category', $category_selection, $category_select, ['class' => 'form-select','maxlength' => 20,'id' => 'category','placeholder' => '運営形態']) }}
                                        @error('category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th>業種<small>※運営形態で「その他」を選んだ方はご記入ください</small></th>
                                    <td>
                                        <input id="industry" type="text" placeholder="（例）学童クラブ、パソコンクラブ" class="form-control @error('industry') is-invalid @enderror" maxlength="20" name="industry" value="{{ old('industry') }}"  >
                                        @error('industry')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th>学校名・教室名<span class="red">*</span><small>最大20文字</small></th>
                                    <td>
                                        <input id="name" type="text" placeholder="（例）世田谷区立トライアル小学校" maxlength="20" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th>学年</th>
                                    <td>
                                        <input id="grade" type="text" placeholder="（例）3年" class="form-control @error('grade') is-invalid @enderror" name="grade" value="{{ old('grade') }}">
                                        @error('grade')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th>クラス</th>
                                    <td>
                                        <input id="kumi" type="text" placeholder="（例）1組" class="form-control @error('kumi') is-invalid @enderror" name="kumi" value="{{ old('kumi') }}">
                                        @error('kumi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th>先生・担当者名<span class="red">*</span></th>
                                    <td>
                                        <input id="teachers_name" type="text" placeholder="（例）ナレッジ太郎" class="form-control @error('teachers_name') is-invalid @enderror" name="teachers_name" value="{{ old('teachers_name') }}" >
                                        @error('teachers_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th>メール<span class="red">*</span></th>
                                    <td>
                                        <input id="mail" type="text" placeholder="（例）taro@xxxxx.com" class="form-control @error('mail') is-invalid @enderror" name="mail" value="{{ old('mail') }}" >
                                        @error('mail')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th>所在地<span class="red">*</span></th>
                                    <td>
                                        <input id="address" type="text" placeholder="（例）東京都世田谷区経堂X-X-X" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" >
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <th>TEL<span class="red">*</span></th>
                                    <td>
                                        <input id="tel" type="text" placeholder="（例）03-3333-XXXX" class="form-control @error('tel') is-invalid @enderror" name="tel" value="{{ old('tel') }}" >
                                        @error('tel')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="trialInfoRed">
                            <p>「トライアルを利用する」ボタンを押したら、確認画面まで、しばらくお待ちください。<br>ブラウザの「戻るボタン」やボタンを何度も押さないでください。</p>
                        </div>
                        <div class="btnOuter">
                            <p>[「ナレッジタイピング利用規約」「プライバシーポリシー」に同意する]へチェックを入れると確認・同意したものとみなします。</p>
                            <div class="form-check-labelOuter">
                                <input class="form-check-input" type="checkbox" name="confirm" id="confirm" class="form-control @error('confirm') is-invalid @enderror" value="1">
                                <label class="form-check-label">
                                    <a href="https://knowledge-p.jp/terms/" target="_blank">ナレッジタイピング利用規約</a>・<a href="https://knowledge-p.jp/privacy/" target="_blank">プライバシーポリシー</a>に同意する
                                </label>
                            </div>
                            <button type="submit" name="SubmitButton" id="SubmitButton" class="btn btn-primary" disabled>トライアルを利用する（アカウント発行）</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection