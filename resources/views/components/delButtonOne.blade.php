{{-- 1件だけ削除するボタン、使用blade(1)システム管理⇒学校管理の編集(2)システム管理⇒コース管理⇒テストモード単語削除（3）システム管理⇒ブログ削除(4)システム管理⇒トライアル削除）
    1件だけ削除なので、del/modal.jsが必要ない、そのかわりに「for_delButtonOne.jsをつかう--}}
<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="del">
  削除
</button>

{{-- モーダルウィンドウ --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >
                   削除の確認
                </h5>
            </div>
            <div class="modal-body">
                <p id="del-title" class="text-danger"><strong>{{ $name }} のデータを削除します</strong></p>
                <p id="del-message">{{ $message }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    キャンセル
                </button>
                {{-- 削除用のアクションを実行させるフォーム --}}
                <form action="{{ route($route,auth()->user()->school_id) }}" id="{{ $id }}" method="POST">
                {{ Form::hidden($hidden_name, $hidden_value)}}
                    @csrf
                    <button name = "modal_del_btn" id="delete-execute" type="submit" class="btn btn-danger">
                        削除
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
