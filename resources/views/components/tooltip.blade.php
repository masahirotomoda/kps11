{{-- ツールチップ --}}
<div class="qnaBox">
    <div class="questionBtn">{{ $word }}</div>
    <div class="answerBox">
        <p>{{ $message }}</p>
    </div>
</div>