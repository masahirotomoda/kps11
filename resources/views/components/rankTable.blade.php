<button type="submit" class="btn btn-primary btnRankTable" onclick="toggleModalFunc('p-rankTable')">ランク表</button>
<div class="typingtestGraph hidden" id="p-rankTable">
  <div class="typingtestGraphBg" onclick="toggleModalFunc('p-rankTable')"></div>
  <div class="typingtestGraphOuter">
    <div class="typingtestGraphInner">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">級</th>
            <th scope="col">点数開始</th>
            <th scope="col">点数終了</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">20級</th>
            <td>0</td>
            <td>49</td>
          </tr>
          <tr>
            <th scope="row">19級</th>
            <td>50</td>
            <td>99</td>
          </tr>
          <tr>
            <th scope="row">18級</th>
            <td>100</td>
            <td>149</td>
          </tr>
          <tr>
            <th scope="row">17級</th>
            <td>150</td>
            <td>199</td>
          </tr>
          <tr>
            <th scope="row">16級</th>
            <td>200</td>
            <td>249</td>
          </tr>
          <tr>
            <th scope="row">15級</th>
            <td>250</td>
            <td>299</td>
          </tr>
          <tr>
            <th scope="row">14級</th>
            <td>300</td>
            <td>349</td>
          </tr>
          <tr>
            <th scope="row">13級</th>
            <td>350</td>
            <td>399</td>
          </tr>
          <tr>
            <th scope="row">12級</th>
            <td>400</td>
            <td>449</td>
          </tr>
          <tr>
            <th scope="row">11級</th>
            <td>450</td>
            <td>499</td>
          </tr>
          <tr>
            <th scope="row">10級</th>
            <td>500</td>
            <td>549</td>
          </tr>
          <tr>
            <th scope="row">9級</th>
            <td>550</td>
            <td>599</td>
          </tr>
          <tr>
            <th scope="row">8級</th>
            <td>600</td>
            <td>649</td>
          </tr>
          <tr>
            <th scope="row">7級</th>
            <td>650</td>
            <td>699</td>
          </tr>
          <tr>
            <th scope="row">6級</th>
            <td>700</td>
            <td>749</td>
          </tr>
          <tr>
            <th scope="row">5級</th>
            <td>750</td>
            <td>799</td>
          </tr>
          <tr>
            <th scope="row">4級</th>
            <td>800</td>
            <td>849</td>
          </tr>
          <tr>
            <th scope="row">3級</th>
            <td>850</td>
            <td>899</td>
          </tr>
          <tr>
            <th scope="row">2級</th>
            <td>900</td>
            <td>949</td>
          </tr>
          <tr>
            <th scope="row">1級</th>
            <td>950</td>
            <td>999</td>
          </tr>
          <tr>
            <th scope="row">1段</th>
            <td>1000</td>
            <td>1049</td>
          </tr>
          <tr>
            <th scope="row">2段</th>
            <td>1050</td>
            <td>1099</td>
          </tr>
          <tr>
            <th scope="row">3段</th>
            <td>1100</td>
            <td>1149</td>
          </tr>
          <tr>
            <th scope="row">4段</th>
            <td>1150</td>
            <td>1199</td>
          </tr>
          <tr>
            <th scope="row">5段</th>
            <td>1200</td>
            <td>1249</td>
          </tr>
          <tr>
            <th scope="row">6段</th>
            <td>1250</td>
            <td>1299</td>
          </tr>
          <tr>
            <th scope="row">7段</th>
            <td>1300</td>
            <td>1349</td>
          </tr>
          <tr>
            <th scope="row">8段</th>
            <td>1350</td>
            <td>1399</td>
          </tr>
          <tr>
            <th scope="row">9段</th>
            <td>1400</td>
            <td>1449</td>
          </tr>
          <tr>
            <th scope="row">10段</th>
            <td>1450</td>
            <td>1499</td>
          </tr>
          <tr>
            <th scope="row">11段</th>
            <td>1500</td>
            <td>1549</td>
          </tr>
          <tr>
            <th scope="row">12段</th>
            <td>1550</td>
            <td>1599</td>
          </tr>
          <tr>
            <th scope="row">13段</th>
            <td>1600</td>
            <td>1649</td>
          </tr>
          <tr>
            <th scope="row">14段</th>
            <td>1650</td>
            <td>1699</td>
          </tr>
          <tr>
            <th scope="row">15段</th>
            <td>1700</td>
            <td>1749</td>
          </tr>
          <tr>
            <th scope="row">16段</th>
            <td>1750</td>
            <td>1799</td>
          </tr>
          <tr>
            <th scope="row">17段</th>
            <td>1800</td>
            <td>1849</td>
          </tr>
          <tr>
            <th scope="row">18段</th>
            <td>1850</td>
            <td>1899</td>
          </tr>
          <tr>
            <th scope="row">19段</th>
            <td>1900</td>
            <td>1949</td>
          </tr>
          <tr>
            <th scope="row">20段</th>
            <td>1950</td>
            <td>1999</td>
          </tr>
          <tr>
            <th scope="row">マスター</th>
            <td>2000</td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="typingtestGraphBtn">
      <button type="button" id="typing_graph_close" class="c-button--sm" onclick="toggleModalFunc('p-rankTable')">閉じる</button>
    </div>
  </div>
</div>