{{-- lazyload（画像遅延）使用のFreeトップページの画像のみ利用 jpgのみ--}}
<a href="" data-toggle="modal" data-target="#{{$image_name}}">{{$word}}</a>
{{-- モーダルウィンドウ --}}
<div class="modal fade" id="{{$image_name}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <img class="lazyload" data-src="/img/{{$image_name}}.jpg">
      </div>
      <div class="modal-footer">
      </div>
      <div class="closeOuter">
        <button type="button" class="btn btn-secondary close" data-dismiss="modal" aria-label="Close">閉じる</button>
    </div>
    </div>
  </div>
</div>
