{{--複数削除でhiddenがある--}}
@php
    $id_attr = 'modal-delete-' . $route . '-' . $id;
@endphp

{{-- 削除ボタン,$route は、form actionのURL（delete処理をするメソッドへのルーティング web.php） --}}
<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="del" disabled>
選択して削除する
</button>

{{-- モーダルウィンドウ --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="{{ $id_attr }}-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="{{ $id_attr }}">
                   削除の確認
                </h5>
            </div>
            <div class="modal-body">
                <p id="del-title" class="text-danger"><strong>データを削除します</strong></p>
            </div>
            <p id="del-message">{{ $message }}</p>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    キャンセル
                </button>
                {{-- 削除用のアクションを実行させるフォーム --}}
                <form action="{{ route($route,auth()->user()->school_id) }}" id="{{ $id }}" method="POST">
                    @csrf
                    {{ Form::hidden($hidden_name, $hidden_value)}}

                    <button name="modal_delete_btn" id="delete-execute" type="submit" class="btn btn-danger">
                        削除
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
