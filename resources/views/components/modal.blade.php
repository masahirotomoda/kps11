{{-- モーダルウィンドウ --}}
<button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#r" id="pass">
{{ $btn_name }}
</button>
<div class="modal fade" id="r" tabindex="-1" role="dialog" aria-labelledby="r-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>
                {{ $title }}
                </h5>
            </div>
            <div class="modalCell">
                <strong>{{ $message }}</strong>
            </div>
            <div class="closeOuter">
                <button type="button" class="btn btn-secondary close" data-dismiss="modal" aria-label="Close">閉じる</button>
            </div>
        </div>
    </div>
</div>