{{-- ツールチップ(必須マーク「＊」つき) --}}
<div class="qnaBox">
    <div class="questionBtn">{{ $word }}<span class="red">*</span></div>
    <div class="answerBox">
        <p>{{ $message }}</p>
    </div>
</div>