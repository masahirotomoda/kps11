{{--【一人用】生徒プロフィールなど一人のパスワードリセットで使用、「password_btn_null.js」とセットで使う--}}

{{-- パスワード確認 --}}
<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#modalreset" id="reset" onclick="clear_text()">
{{ $passwordbtn }}
</button>
{{-- モーダルウィンドウ --}}
<div class="modal fade" id="modalreset" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>
                パスワードを変更する
                </h5>
            </div>
            <div>           
                <div class="modalCell">
                    {{ Form::text('change_password',null, ['class' => 'form-control', 'id' => 'change_password', 'autocomplete' => 'off' ,'maxlength' => $maxlength ,'placeholder' => 'あたらしいパスワード']) }}
                </div>
                <div class="modalCell">
                    {{ Form::text('change_password_confirmation',null, ['class' => 'form-control', 'id' => 'change_password_confirmation', 'autocomplete' => 'off' ,'maxlength' => $maxlength ,'placeholder' => 'あたらしいパスワード(確認)']) }}
                </div>
                <div class="modalCell">
                    <p>{{ $passwordcomment }}</p>
                </div>
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    キャンセル
                </button>
                <button type="submit" name="user_password_reset_btn" class="btn btn-danger" id="change">
                {{ $passwordbtn }}
                </button>
            </div>
        </div>
    </div>
</div>