{{-- 画像のモーダル 必須の「*」あり --}}
<a href="" data-toggle="modal" data-target="#{{$image_name}}">
  {{ $word}}<span class="red">*</span>
</a>
{{-- モーダルウィンドウ --}}
  <div class="modal fade" id="{{$image_name}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-body">
                <img src="/img/{{$image_name}}.png">
              </div>
              <div class="modal-footer">
              </div>
              <div class="closeOuter">
                <button type="button" class="btn btn-secondary close" data-dismiss="modal" aria-label="Close">閉じる</button>
            </div>
          </div>
      </div>
  </div>

