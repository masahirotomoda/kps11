@php
$id_attr = 'modal-pass-' . $controller . '-' . $id;
@endphp

{{-- パスワード確認 --}}
<button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#{{ $id_attr }}" id="pass">
    パスワード確認
</button>

{{-- モーダルウィンドウ --}}
<div class="modal fade" id="{{ $id_attr }}" tabindex="-1" role="dialog" aria-labelledby="{{ $id_attr }}-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>
                    パスワード確認
                </h5>
            </div>
            <div class="modalCell passwordmc">
                <strong>{{$password}}</strong>
            </div>
            <div class="closeOuter">
                <button type="button" class="btn btn-secondary close" data-dismiss="modal" aria-label="Close">閉じる</button>
            </div>
        </div>
    </div>
</div>