@php
$id_attr = 'modal-reset-' . $controller . '-' . $id;
@endphp

{{-- パスワード確認 --}}
<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#{{ $id_attr }}" id="reset">
    パスワード変更
</button>
{{-- モーダルウィンドウ --}}
<div class="modal fade" id="{{ $id_attr }}" tabindex="-1" role="dialog" aria-labelledby="{{ $id_attr }}-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>
                    パスワード変更
                </h5>
            </div>
            <div>
                {{ Form::open(['url' => '/school/userpas-change', 'files' => true , 'method' => 'post',]) }}
                {{ Form::token() }}
                <div class="modalCell">
                    {{ Form::text('password1st',null, ['class' => 'form-control', 'id' => 'password1st', 'autocomplete' => 'off' ,'maxlength' => 20,'placeholder' => '新パスワード','required']) }}
                </div>
                <div class="modalCell">
                    {{ Form::text('password2nd',null, ['class' => 'form-control', 'id' => 'password2nd', 'autocomplete' => 'off' ,'maxlength' => 20,'placeholder' => '新パスワード(確認)','required']) }}
                </div>
                <div class="modalCell">
                    <p>10～20文字で入力。大文字、小文字、数字、記号(!@;:#$%&)のうち3種類以上</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    キャンセル
                </button>
                <button type="submit" class="btn btn-danger" id="change">
                    変更
                </button>
                {{ Form::hidden('id',$id) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>