@php
    $id_attr = 'modal-reset-' . $controller . '-' . $id;
@endphp

{{-- パスワード確認 --}}
<button class="btn btn-secondary btn-sm" type="button"  data-toggle="modal" data-target="#{{ $id_attr }}" id="reset">
  リセット
</button>
{{-- モーダルウィンドウ --}}
<div class="modal fade" id="{{ $id_attr }}" tabindex="-2" role="dialog" aria-labelledby="{{ $id_attr }}-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>
                    パスワードリセット
                </h5>
            </div>
            <div>
                <div class="modalCell">
                    <strong>{{$name}}さんのパスワードを初期パスワード「{{ $init_password }}」にリセットしますか？</strong>
                </div>
            </div>
            <div>
                <div class="modalCell">
                    <p>ここでは「リセット」しかできません。「パスワードの変更」は本人の「マイメニュー」でおこないます。</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    キャンセル
                </button>
                <form action="{{ url($url) }}" method="post">
                    {{ Form::hidden('school_id', auth()->user()->school_id)}}
                    {{ Form::hidden('user_id', $id)}}
                    @csrf
                    <button name="password_reset_btn" type="submit" class="btn btn-danger" id="modal_reset_btn">
                        リセット
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
