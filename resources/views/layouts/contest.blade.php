{{-- ★コンテストのタイピングメニューの全体フレーム（ヘッダーフッター）、スマホ対応必要、TOPページなのでソースはきれいに --}}
@php
if($school->logo == 0){
    $className='navbar-brand';
    } else {
    $className='navbar-brand'.$school->logo;
    } 
@endphp
<!doctype html>
<html lang="ja">
<head>
@yield('googletag')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>{{--★titleは各ページごと --}}
    <meta name="description" content=@yield('description')> {{-- meta descriptionは各ページごと --}}
    <meta name="keywords" content="タイピング,typing,タイピング教材,タイピング練習,タイピングテスト,タイピングコンテスト,タイピングコン大会"/>
    <meta property="og:title" content=@yield('title') >{{--★og:titleとtitleは共通、各ページごと --}}
    <meta property="og:type" content="website" >
    <meta property="og:description" content=@yield('description')>{{--★og:descriptionは各ページごと（meta descriptionと同じ） --}}
    <meta property="og:url" content="{{ asset('/') }}" >
    <meta property="og:site_name" content="ナレッジタイピングforコンテスト | 小学校のタイピングコンテスト用サイト" >
    <meta property="og:image" content="/img/ogp.png" >
    <meta property="og:locale" content="ja_JP" >
    <link rel="apple-touch-icon" href="/img/icon/apple-touch-icon.png">
    <link href="/img/icon/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link rel="icon" href="/img/icon/favicon.ico">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700;800&display=swap" rel="stylesheet">
    @yield('css'){{-- ★Stylesは各ページごと --}}
</head>
<body>
<div id="app" class="bgTeacherstudent freePageBlock">
<header class="freePage contestPage">
    <div id="headerInner">
        <h1>
            <a class="{{$className}}" href="/{{$contest_account}}/contest">ナレッジタイピングforコンテスト</a>
        </h1>
        @if($school->id==config('configrations.DEMO_CONTEST_INFO.testmode_school_id') || $school->id==config('configrations.DEMO_CONTEST_INFO.practicemode_school_id'))
        <p>小学生のための<br>タイピングコンテスト</p>
        <div id="naviOut">
            <div id="naviInner">{{--★下記spanは必要--}}
                <div class="navBtn"><span></span><span></span></div>
                    <div class="freenav">
                    <div class="outerBtn">
                        <a href="https://n-typing.com" target="_blank">ナレッジタイピングFree</a>
                        <a href="https://n-typing-school.com" target="_blank">ナレッジタイピング for school</a>
                        <a href="https://knowledge-p.jp/contact/" target="_blank">お問い合わせ</a>
                    </div>
                    <ul class="freePageHeadBtn">
                        <li><a href="/{{$contest_account}}/contest#top">TOP</a></li>
                        <li><a href="/{{$contest_account}}/contest#about">このサイトについて</a></li>
                        <li><a href="/{{$contest_account}}/contest#price">料金</a></li>
                        <li><a href="/{{$contest_account}}/contest#faq">よくある質問</a></li>
                        <li><a href="/{{$contest_account}}/contest#school">運営スクール</a></li>
                    </ul>
        @else
        <p>タイピングコンテスト開催中</p>
        <div id="naviOut">
            <div id="naviInner">{{--★下記spanは必要--}}
                <div class="navBtn"><span></span><span></span></div>
                    <div class="freenav">
                    <ul class="freePageHeadBtn">
                        <li><a href="/{{$contest_account}}/contest#top">TOP</a></li>
                        <li><a href="/{{$contest_account}}/contest#about">このサイトの使い方</a></li>
                        <li><a href="/{{$contest_account}}/contest#contest">コンテスト実施方法</a></li>
                        <li><a href="/{{$contest_account}}/contest#faq">Q&A</a></li>
                        <li><a href="{{url('/school-login')}}">先生専用Page</a></li>
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>
@yield('content'){{--★ここに各ページごとのコンテンツが入る--}}
</div>
@if(isset($practice_courses)){{--ページトップに戻るボタンはTOPページのみ表示、タイピング画面では非表示--}}
<div class="pagetop"><a href="#top"><span><img src="/img/pagetop.svg" alt="ページトップ"/></span></a></div>
@endif
<footer>
    <div class="footer-content">
        <ul class="footer-contentList">
            <li class="terms">
                <a href="https://knowledge-p.jp/terms/" target="_blank" title="ナレッジタイピング利用規約">利用規約</a>
            </li>
            <li class="privacy_policy">
                <a href="https://knowledge-p.jp/privacy/" target="_blank">プライバシーポリシー</a>
            </li>
            <li class="security_policy">
                <a href="https://knowledge-p.jp/security/" target="_blank">情報セキュリティ基本方針</a>
            </li>
            <li class="security_policy">
                <a href="https://knowledge-p.jp/commerce/" target="_blank">特定商取引法に基づく表記</a>
            </li>
            <li class="copyright">
                <a href="https://knowledge-p.jp/" target="_blank">{{config('configrations.COPYRIGHT')}}</a>
            </li>
        </ul>
    </div>
</footer>
@yield('js'){{--jsは各ページで指定--}}
</body>
</html>
