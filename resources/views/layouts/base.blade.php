@php  //ページごとの情報をconfigrationsから取得
//初期化
$role_style = null;
$back_btn_name = null;
$back_btn = null;
$kanri_title = null;
$search_btn = null;
$is_pagination = null;
//URLのドメイン名以下の文字列取得（例）n-typing.com/teacher/student-listの場合、「teacher/student-list」
$currentURL=request()->path();

//データ取得（app/config/configrations.php）
foreach (config('configrations.PAGE_INFO') as $url => $url_info) {
    if ($currentURL == $url){
        $role_style = $url_info['ROLE_STYLE'];  //ロールのスタイル
        $back_btn_name = $url_info['BACK_BTN_NAME']; //バックボタンの名前
        $back_btn = $url_info['BACK_BTN']; //バックボタンの遷移先
        $kanri_title = $url_info['KANRI_TITLE'];  //ページごとの見出しとmetaタイトル
        $search_btn = $url_info['SEARCH_BTN'];  //検索ボタンの横にある「すべて表示」ボタンの遷移先（リダイレクト）
        $is_pagination = $url_info['PAGINATION']; //ページネーションの有無（なければnull）
    };
};
//アイコンを生徒先生画面、学校管理画面、システム画面ごとに設定
$Role = Auth()->user()->role; //ロール⇒生徒、先生、学校管理、システム管理
switch($Role){
case '生徒':
    $shortcuticon = "favicon-student.ico"; //ショートカットアイコン
    $appleicon ="apple-touch-icon-student.png"; //ipadのホームに追加アイコン
    break;
case '先生':
    $shortcuticon = "favicon-student.ico";
    $appleicon ="apple-touch-icon-student.png";
    break;
case '学校管理者':
    $shortcuticon = "favicon-school.ico";
    $appleicon ="apple-touch-icon-school.png";
    break;
case 'システム管理者': 
    $shortcuticon = "favicon-system.ico";
    $appleicon ="apple-touch-icon-system.png";
    break;
default:    
    $shortcuticon = "favicon-student.ico";
    $appleicon ="apple-touch-icon-student.png";
    break;
}

@endphp
<!doctype html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$kanri_title}} | ナレッジタイピング</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">{{-- CSRF Token 「addnew_student.js」など新規レコードで空行をつくる時に使うので必須--}}
    <link rel="apple-touch-icon" href="/img/icon/{{$appleicon}}">
    <link href="/img/icon/{{$shortcuticon}}" rel="shortcut icon" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700;800&display=swap" rel="stylesheet">
    @yield('head_1'){{-- Styles --}}
    <link href="/css/style.css?v=20250120" rel="stylesheet"> {{--基本のstyle.cssの上のCSS--}}
    @yield('head_2'){{--style.cssの下のCSS --}}
</head>
<body>{{-- 全体をラップ。ロールごとのスタイル--}}
<div id='app' class = "{{$role_style}}">
@include('layouts.include.schoolheader')
<main>
<div class="container">{{--コンテンツ全体--}}
    <div class="backBtn">{{--戻るボタン--}}
    @if(!empty($back_btn))
    <button type="button" class="btn btn-primary" onclick="location.href='{{ $back_btn}}'">{{ $back_btn_name}}</button>
    @endif
    </div>
    @if(!empty($kanri_title))
    <h2 class="ktTitle">{{ $kanri_title}}</h2>
    @endif
    <div class="mainBlock">
        @yield('content'){{--viewファイルを読み込む--}}
        @if($is_pagination !== null)
        @include('layouts.include.paginationsimple')
        @endif
    </div>{{--/mainBlock--}}
</div>{{--/containe--}}
</main>
</div>{{--/id='app'--}}
@include('layouts.include.footer'){{-- 共通js（bootstrap--}}{{--bootstrap のCDN 念のためコメントアウト、以下はサーバーに設置<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>--}}
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>
@yield('js'){{-- 追加js各ページで設定--}}
</body>
</html>