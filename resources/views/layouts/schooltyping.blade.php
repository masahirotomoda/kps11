{{--先生生徒用のタイピング画面の全体フレーム（フリーのタイピングは別）--}}
{{--使用は2ファイル　view/user/student/typing   view/user/student/typingtest--}}
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700;800&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="/css/typing.css?v=20250226">
  <link rel="stylesheet" href="/css/style.css?v=20250226">
  <title>{{$course->course_name}} | ナレッジタイピング</title>
  <link rel="apple-touch-icon" href="/img/icon/apple-touch-icon-student.png">
  <link href="/img/icon/favicon-student.ico" rel="shortcut icon" type="image/x-icon">
</head>
<body>{{--ROLE STYPEはconfigrations でなく、直接指定--}}

  <div id="app" class="bgTeacherstudent">
    @include('layouts.include.schoolheader')
    <section class="l-main">{{--コース名--}}
      <div class="titleOuter">
      <nav aria-label="breadcrumb">
      <form action="/user-menu" method="post" name="typingTitleCourseForm" id="typingTitleCourseForm">
        @csrf
        <input type="hidden" name="tab" value="{{$course->tab}}">
        <ol class="breadcrumb typingbc">
          <li class="breadcrumb-item" id="BackToTopmenuBtn" ><button type="submit" name="gotopbtn" > コースメニュー</button></li>
          <li class="breadcrumb-item active" aria-current="page">{{$course->course_name}}</li>
        </ol>
    </form>
      </nav>
      </div>
      @yield('content')
  </div>
  </section>
  </div>
  <!--/app-->
  @include('layouts.include.footer')
  @yield('js')
</body>
</html>