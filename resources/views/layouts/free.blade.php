{{-- ★フリーの全体フレーム（ヘッダーとフッター）、スマホ対応必要、TOPページなのでソースはきれいに --}}
{{-- ★使用ファイル⇒フリー４ファイル view/user/free/typing  typingtest、menu（コース一覧）,menuinformation（すべてのお知らせ）--}}
{{-- ★使用ファイル⇒トライアル2ファイル view/trial/application（申込フォーム）  result（アカウント発行） --}}
<!doctype html>
<html lang="ja">
<head>
@yield('googletag')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>{{--★★titleは各ページごと --}}
    <meta name="description" content="@yield('description')"> {{-- meta descriptionは各ページごと --}}
    <meta name="keywords" content="タイピング,typing,タイピング教材,タイピング練習,ICT教材,キータッチ2000,無料"/>
    <meta property="og:title" content="@yield('title')" >{{--★og:titleとtitleは共通、各ページごと --}}
    <meta property="og:type" content="website" >
    <meta property="og:description" content="@yield('description')">{{--★og:descriptionは各ページごと（meta descriptionと同じ） --}}
    <meta property="og:url" content="{{asset('/')}}" >
    <meta property="og:site_name" content="ナレッジタイピング | 小学生の無料タイピング練習サイト" >
    <meta property="og:image" content="/img/ogp.png" >
    <meta property="og:locale" content="ja_JP" >
    <link rel="apple-touch-icon" href="/img/icon/apple-touch-icon.png">
    <link href="/img/icon/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link rel="icon" href="/img/icon/favicon.ico" >{{--↓は、共通CSS googlefontsの事前読み込み--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700;800&display=swap" rel="stylesheet">
    @yield('css'){{-- ★Stylesは各ページごと --}}
</head>
<body>
<div id="app" class="bgTeacherstudent freePageBlock">{{--★ROLE STYLE はconfigrationsから取得せず、直接指定 --}}
<header class="freePage">
    <div id="headerInner">
        <h1>
            <a class="navbar-brand" href="/">ナレッジタイピング</a>
        </h1>
        <p>小学生のための<br>タイピング練習サイト</p>
        <div id="naviOut">
            <div id="naviInner">{{--★下記spanは必要--}}
                <div class="navBtn"><span></span><span></span></div>
                <div class="freenav">
                    <div class="outerBtn">
                        <a href="https://n-typing-english.com/" target="_blank">中学英単語タイピング</a>
                        <a href="/contest2pramode2/contest" target="_blank">【デモ】コンテスト用</a>
                        <a href="https://n-typing-school.com" target="_blank">塾学校向けプラン</a>
                        <a href="https://knowledge-p.jp/contact/" target="_blank">お問合せ</a>
                    </div>
                    <ul class="freePageHeadBtn">
                        <li><a href="/">トップ</a></li>
                        <li><a href="/#about">このサイトについて</a></li>
                        <li><a href="/#news">お知らせ</a></li>
                        <li><a href="/#faq">よくある質問</a></li>
                        <li><a href="/#school">運営スクール</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
@yield('content'){{--★ここに各ページごとのコンテンツが入る--}}
</div>
@if(isset($star1_tab_active)){{--トップページだけトップに戻るボタン表示、タイピング画面では表示させないため--}}
<div class="pagetop"><a href="#top"><span><img src="/img/pagetop.svg" alt="ページトップ"/></span></a></div>
@endif
<footer>
    <div class="footer-content">
        <ul class="footer-contentList">
            <li class="terms">
                <a href="https://knowledge-p.jp/terms/" target="_blank" title="ナレッジタイピング利用規約">利用規約</a>
            </li>
            <li class="privacy_policy">
                <a href="https://knowledge-p.jp/privacy/" target="_blank">プライバシーポリシー</a>
            </li>
            <li class="security_policy">
                <a href="https://knowledge-p.jp/security/" target="_blank">情報セキュリティ基本方針</a>
            </li>
            <li class="security_policy">
                <a href="https://knowledge-p.jp/commerce/" target="_blank">特定商取引法に基づく表記</a>
            </li>
            <li class="copyright">
                <a href="https://knowledge-p.jp/" target="_blank">{{config('configrations.COPYRIGHT')}}</a>
            </li>
        </ul>
    </div>
</footer>
@yield('js'){{--jsは各ページで指定--}}
</body>
</html>
