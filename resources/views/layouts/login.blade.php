@php  //先生（生徒）、学校、システムの全体スタイルを取得、$currentURL(現在のURL)はドメインn-typing以降の文字列取得、例、n-typing.com/login⇒login
//アイコンを生徒先生画面、学校管理画面、システム画面ごとに設定
$currentURL=request()->path();
switch ($currentURL) {
    case "login";//先生&生徒ログインURL
        $role_style = 'bgTeacherstudent'; //全体のスタイルシート名
        $shortcuticon = 'favicon-student.ico'; //ショートカットアイコン
        $appleicon ="apple-touch-icon-student.png"; //ipadのホームに追加アイコン
        $login_role = 'ログイン'; //metaのタイトルの後半
        break;
    case "school-login";//学校管理ログインURL
        $role_style = 'bgSchool';
        $shortcuticon = "favicon-school.ico";
        $appleicon ="apple-touch-icon-school.png";
        $login_role = '学校管理'; //metaのタイトル 検索は関係なく、ブックマークを意識する
        break;
    case "system-login";//システム管理ログインURL
        $role_style = 'bgSystem';
        $shortcuticon = "favicon-system.ico";
        $appleicon ="apple-touch-icon-system.png";
        $login_role = 'システム管理'; //metaのタイトル
        break;
    default;
        $role_style = null;
        $forget_url = null;
        $shortcuticon = "favicon-student.ico";
        $appleicon ="apple-touch-icon-student.png";
        $login_role = 'システム管理'; //metaのタイトル
}
@endphp
<!doctype html>{{--TOPページはHTMLソースもきれい(インデント）にすること--}}
<html lang="ja">
<head>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GZPD2RNV4Q"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-GZPD2RNV4Q');
</script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$login_role}}</title>{{--クロールとリンク禁止--}}
    <meta name="robots" content="noindex,nofollow">{{--トークンは削除不可、管理画面で新規追加(1行追加)でここのtokenのクローンを作る--}}
    <meta name="description" content="ナレッジタイピング for school のご利用者はここからログインしてください。">
    <meta name="keywords" content="ナレッジタイピング,ナレッジタイピング for school" />
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="apple-touch-icon" href="/img/icon/{{$appleicon}}">
    <link href="/img/icon/{{$shortcuticon}}" rel="shortcut icon" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700;800&display=swap" rel="stylesheet">
    <link href="/css/style.css?v=20240123" rel="stylesheet">
@yield('head'){{--★追加CSSなどheadに入れる情報はここ、cssのpathはpublic\css\style.css--}}
</head>
<body>{{--★先生、学校、システム管理ごとの全体スタイル(上部phpで変数作成)をここで設定、ID,パスワード共に、必須、最大20文字をhtml側で設定
ログイン試行回数10回とロック時間10分やログイン関連の設定⇒app\Http\Controllers\Auth\LoginController.php --}}
<div id='app' class="{{$role_style}}">
    <main>
        <div class="container login-page theacher-login">
            <div class="loginBoxOuter">
                <div class="loginBox">
                    <div class="loginBoxInner">
                        <div class="card-header">ログイン</div>
                            @yield('content')
                        <div class="schoolId">
                            {{Form::text('schoolId',$school_id,
                            $errors->has('login_account') ? ['class' => 'form-control is-invalid', 'placeholder' => '学校ID','autocomplete' => 'off', 'maxlength' => 20,'required']
                                                    : ['class' => 'form-control', 'placeholder' => '学校ID','autocomplete' => 'off', 'maxlength' => 20, 'required'])}}
                        </div>
                        <div class="loginId">
                            {{Form::text('login_account',$login_account,
                            $errors->has('login_account') ? ['class' => 'form-control is-invalid', 'placeholder' => 'ログインID','autocomplete' => 'off', 'maxlength' => 20,'required']
                                                        : ['class' => 'form-control', 'placeholder' => 'ログインID','autocomplete' => 'off', 'maxlength' => 20, 'required'])}}
                            @error('login_account')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="loginPass">
                            <input id="password" type="password" maxlength="20" autocomplete="off" placeholder="パスワード" class="form-control @error('password') is-invalid @enderror" name="password" required>
                            <button type='button' id="btn_passview">表示</button>
                            @error('password')
                            <span class="invalid-feedback" role="alert"><strong>{{$message}}</strong></span>
                            @enderror
                        </div>
                        @if($currentURL === 'login' || $currentURL ==='system-login')
                        <div class="form-check">
                            {{Form::checkbox('remember', 'remember', $remember, ['class'=>'form-check-input','id'=>'remember']) }}
                            <label class="form-check-label" for="remember">ログインIDを保存する</label>
                        </div>
                        @endif
                        <div class="submitOuter">
                            <button type="submit" class="btn btn-primary">ログイン</button>
                        </div>
                        <div class="loginAt">{{-- （1）ログインできない時は？の画像のモーダル --}}
                            <a href="" data-toggle="modal" data-target="#cautionModal">（1）ログインできない時は、ここをチェック</a>
                            <div class="modal fade" id="cautionModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                        <img src="/img/login1.png">
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                        <div class="closeOuter">
                                        <button type="button" class="btn btn-secondary close" data-dismiss="modal" aria-label="Close">閉じる</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>(2)それでもログインできない時は<a href='/logout'>「強制ログアウト」</a></p><br>
                        <input type="hidden" name="school_id" value="{{$school_id}}">
                        </form>
                        @if($currentURL == 'school-login')
                        <div class="btnOther"><a href="../login">生徒・先生のログインはこちら</a></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@include('layouts.include.footer')
<script src="/js/password_watch.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>
