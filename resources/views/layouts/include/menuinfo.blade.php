@include('layouts.include.pagination')
 
    <div class="infoBlock">        
        <div class="infoBlockCell">            
            <div class="infoBlockCellText">
            @foreach ($information as $info)
                <time class="infoTime">{{ $info->updated_at }}
                    @if($info->importance === '重要')
                        <span class="infoNew">重要</span>
                    @endif
                    <div class="infoBlockCellTitle">{{ $info->title }}</div>
                    @if(date("Y-m-d H:i:s",strtotime("-10 day")) < $info->updated_at)
                        <span class="infoNew">NEW</span>
                    @endif
            @endforeach
            </div>
        </div> 
    </div>