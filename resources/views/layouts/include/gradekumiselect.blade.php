@if($school->password_display){{--●●塾、教室--}}
    <div class="school_grade_select">    
        {{Form::text('school_grade',  $grade_select, ['class' => 'form-select','id' => 'school_grade','placeholder' => '教室名の一部']) }}
    </div>
    <div class="school_attendance_no_select">    
        {{Form::text('school_attendance_no', $attendance_no_select, ['class' => 'form-select','id' => 'school_attendance_no','placeholder' => '生徒番号']) }}
    </div>
    <div class="school_student_name_text">    
        {{Form::text('school_student_name', $user_name, ['class' => 'form-control','id' => 'school_student_name','placeholder' => '生徒名の一部','size' =>'20','maxlength' =>'20','autocomplete' =>'off']) }}
    </div>
@else
    <div class="school_grade_select">    
        {{Form::select('school_grade', config('configrations.SCHOOL_GRADE') , $grade_select, ['class' => 'form-select','id' => 'school_grade']) }}
        <label for="school_grade">学年:</label>
    </div>
    <div class="school_kumi_select">   
        {{Form::select('school_kumi', config('configrations.SCHOOL_KUMI') , $kumi_select, ['class' => 'form-select','id' => 'school_kumi']) }}
        <label for="school_kumi">組:</label>
    </div>
    <div class="school_attendance_no_select">    
        {{Form::select('school_attendance_no', config('configrations.SCHOOL_ATTENDANCE_NO') , $attendance_no_select, ['class' => 'form-select','id' => 'school_attendance_no']) }}
        <label for="school_attendance_no">出席番号:</label></div>
    <div class="school_student_name_text">    
        {{Form::text('school_student_name', $user_name, ['class' => 'form-control','id' => 'school_student_name','placeholder' => '生徒名','size' =>'20','maxlength' =>'20','autocomplete' =>'off']) }}
        <label for="school_student_name">名前:</label>
    </div>
@endif
