<div> 
    @foreach ($information as $info)
        <div class="infoBlockCellnoImage">
            <time class="infoTime"> {{ date_format($info->updated_at, 'Y年m月d日') }}</time>
            <div class="infoBlockCellTitle">                
                @if($info->importance === '重要')
                    <span class="infoNew">重要</span>
                @endif
                @if(date("Y-m-d H:i:s",strtotime("-10 day")) < $info->updated_at)
                    <span class="infoNew">NEW</span>
                @endif                 
                {{ $info->title }}                
			</div>
        </div>
    @endforeach
</div>