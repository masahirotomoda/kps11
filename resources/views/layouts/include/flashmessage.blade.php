@php //フラッシュメッセージがあると$is_flashmessageが1になる。Jsに渡すための変数。
if(session('flash_message')== null){
    $is_flashmessage = 0;
} else {
    $is_flashmessage = 1;
}
@endphp
@if (session('flash_message'))
            <div class="flash_message" id="flash_message">
                <p>{{session('flash_message')}}</p>
            </div>
@endif