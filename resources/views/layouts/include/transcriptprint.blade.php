@php
if(Auth()->user()->role === '学校管理者'){
    $_role = 'school';
} elseif(Auth()->user()->role === '先生'){
    $_role = 'teacher';
}
@endphp
{{--学校管理者と先生で切り分け form開始 検索＆エクセル出力--}}
{{Form::open(['url' => '/'.$_role.'/transcript-print-bt', 'method' => 'post','id'=>'search_form'])}}
<div class="menuSpaces smallBox">
    <div class="menuSpacesCell spaceAll">
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('昨日')">昨日</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('今日')">今日</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('AM')">今日の午前</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('PM')">今日の午後</button>
        <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
            @slot('word', 'この画面の使い方')
            @slot('image_name', 'teacher33')
        @endcomponent
        </div>
    </div>
</div>
<div class="menuSpaces underFix">
    @include('layouts.include.calendar')
    <div class="menuSpacesCell">
        {{ Form::text('course_name', $courseName, ['type' => 'text' , 'class' => 'form-control','id' => 'course_name', 'maxlength' => 40 ,'placeholder' => 'コース名の一部']) }}
    </div>{{--トライアルは学年、クラスの検索なし--}}
    <div class="menuSpacesCell spaceAll">
        @if($school->trial !==1 )
        {{ Form::select('grade', $grade_selection, $grade, ['class' => 'form-select','id' => 'grade','placeholder' => '学年']) }}
        {{ Form::select('kumi', $kumi_selection, $kumi, ['class' => 'form-select','id' => 'kumi','placeholder' => '組']) }}
        @endif
        {{ Form::text('name', $name, ['type' => 'text' , 'class' => 'form-control','id' => 'name','maxlength' => 20 ,'placeholder' => '生徒名の一部']) }}
        @include('layouts.include.searchbtn')
        <button class="btn btn-outline-primary" id="all_reset_btn" type="button" onclick="location.href='./transcript-print'">操作のキャンセル</button>
    </div>
    <div class="menuSpacesCell">
        {{ Form::close() }} {{--学校管理者と先生で切り分け form開始 記録証印刷--}}
        {{Form::open(['url' => '/'.$_role.'/transcript-print-pdf', 'method' => 'post','id'=>'pdf'])}}
        <button name="pdf1" id="pdf1" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">記録証・印刷用PDF（シンプル）</button>
        <button name="pdf2" id="pdf2" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">記録証・印刷用PDF（カラフル）</button>
    </div>
    <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
        @slot('word', '印刷方法')
        @slot('image_name', 'teacher31')
        @endcomponent
    </div>
</div>
@include('layouts.include.alertmessage')
@include('layouts.include.pagination')
<div class="menuSpaces topMinusBox">
    {{ Form::submit('データの一括ダウンロード(Excel)', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm rightBox','form'=>'search_form']) }}
    <div class="manualSpace links bottomBox">
        @component('components.modalimage')
        @slot('word', 'ダウンロード方法')
        @slot('image_name', 'teacher32')
        @endcomponent
    </div>
</div>
<table class="table transcript" id="mainTable">
    <thead>
        <tr>
            <th>
                <input type="checkbox" id="checkAll" value="1">
                @component('components.tooltip')
                    @slot('word', '印刷')
                    @slot('message', '表示データをまとめて印刷する時はここをチェック。個別印刷は、個別にチェック')
                @endcomponent
            </th>
            <th scope="col">日時</th>
            <th scope="col">コース名</th>
            <th scope="col">学年</th>
            <th scope="col">組</th>
            <th scope="col">出席No</th>
            <th scope="col">生徒名</th>
            <th scope="col">スコア</th>
            <th scope="col">
                @component('components.tooltip')
                    @slot('word', '級')
                    @slot('message', '級は「キータッチ2000検定コース」でのみ表示')
                @endcomponent
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($scoreHistory as $sh)
        <tr>
            <td><input type="checkbox" class="checks" name="checks[]" value="{{ $sh->id }}" id="{{ 'pdf'.$sh->id }}">
            <td>{{date_format($sh->created_at,'Y/m/d　　H:i')}} </td>
            <td>{{ $sh->course_name }}</td>
            <td>{{ $sh->grade_name }}</td>
            <td>{{ $sh->kumi_name }}</td>
            <td>{{ $sh->attendance_no }}</td>
            <td>{{ $sh->name }}</td>
            <td>{{ $sh->point }}</td>
            <td>{{ $sh->rank }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ Form::close() }}