@php
$alert_title = '次の理由で、保存できませんでした';//このタイトルが既定値、印刷やエクセル出力でのタイトルは別なので、下記で設定
$currentURL=request()->path();
    foreach (config('configrations.PAGE_INFO') as $url => $url_info) {
                if ($currentURL == $url && isset($url_info['ALERT_TITLE'])){
                        $alert_title = $url_info['ALERT_TITLE'];  //アラートモーダルのタイトル情報
                };
            };
@endphp
{{--隠しボタン、bootstrapのモーダルがボタンイベントで発動するので、強制的にボタンイベントを作るため--}}
                <button type="button"  data-toggle="modal" data-target="#alertmodal" id="alertbtn"></button>
                <div class="modal fade" id="alertmodal" tabindex="-1" role="dialog" aria-labelledby="flashmessage-label" aria-hidden="true" >
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>
                                    {{$alert_title}}
                                </h5>
                            </div>
                            <div class="modalCell">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                                </ul>
                            </div>
                            <div class="closeOuter">
                                <button type="button" class="btn btn-secondary close" data-dismiss="modal" aria-label="Close">閉じる</button>
                            </div>
                        </div>
                    </div>
                </div>{{--hiddenにバリデーションエラーの有無を持たせる⇒エラーがあれば、addnew_all.js モーダルメッセージ表示--}}
        @if($errors->all() == null)
        {{form::hidden('is_error','no',['id'=>'is_error'])}}
        @else
        {{form::hidden('is_error','yes',['id'=>'is_error'])}}
        @endif