<!-- タイピングキーボード -->
<div id="keyboard-area-init"><img src="/img/keyboard-init.png" alt="タイピング初期画面"></div>
<div class="p-keyboard hidden" id="keyboard-area" >
        <!-- タイピングキーボード -->
        <!-- 1段目 -->
        <div class="p-keyboard__key--normal" id="p-keyboard__key--zh">
          <!-- 半角/全角 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--one">
          <!-- 1 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--two">
          <!-- 2 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--three">
          <!-- 3 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--four">
          <!-- 4 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--five">
          <!-- 5 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--six">
          <!-- 6 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--seven">
          <!-- 7 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--eight">
          <!-- 8 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--nine">
          <!-- 9 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--zero">
          <!-- 0 -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--hypen">
          <!-- - -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--tilde">
          <!-- ^ -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--en">
          <!-- ¥ -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--del">
          <!-- del -->
        </div>
        <!-- 2段目 -->
        <div class="p-keyboard__key--md" id="p-keyboard__key--tab">
          <!-- tab -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--q">
          <!-- q -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--w">
          <!-- w -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--e">
          <!-- e -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--r">
          <!-- r -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--t">
          <!-- t -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--y">
          <!-- y -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--u">
          <!-- u -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--i">
          <!-- i -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--o">
          <!-- o -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--p">
          <!-- p -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--atmark">
          <!-- @ -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--lbracket">
          <!-- [ -->
        </div>
        <div class="p-keyboard__key--enter" id="p-keyboard__key--enter">
          <!-- enter -->
        </div>
        <!-- 3段目 -->
        <div class="p-keyboard__key--lg" id="p-keyboard__key--control">
          <!-- control -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--a">
          <!-- a -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--s">
          <!-- s -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--d">
          <!-- d -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--f">
          <!-- f -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--g">
          <!-- g -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--h">
          <!-- h -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--j">
          <!-- j -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--k">
          <!-- k -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--l">
          <!-- l -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--semicolon">
          <!-- ; -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--colon">
          <!-- : -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--rbracket">
          <!-- ] -->
        </div>
        <div class="p-keyboard__margin--md"></div>
        <!-- 4段目 -->
        <div class="p-keyboard__key--xl" id="p-keyboard__key--lshift">
          <!-- lshift -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--z">
          <!-- z -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--x">
          <!-- x -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--c">
          <!-- c -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--v">
          <!-- v -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--b">
          <!-- b -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--n">
          <!-- n -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--m">
          <!-- m -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--comma">
          <!-- , -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--period">
          <!-- . -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--slash">
          <!-- / -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--underscore">
          <!-- _ -->
        </div>
        <div class="p-keyboard__key--lg" id="p-keyboard__key--rshift">
          <!-- rshift -->
        </div>
        <!-- 5段目 -->
        <div class="p-keyboard__margin--lg"></div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--loption">
          <!-- loption -->
        </div>
        <div class="p-keyboard__key--md" id="p-keyboard__key--lcommnad">
          <!-- lcommnad -->
        </div>
        <div class="p-keyboard__key--max" id="p-keyboard__key--space">
          <!-- space -->
        </div>
        <div class="p-keyboard__key--md" id="p-keyboard__key--rcommnad">
          <!-- rcommnad -->
        </div>
        <div class="p-keyboard__key--normal" id="p-keyboard__key--roption">
          <!-- roption -->
        </div>
        <div class="p-keyboard__margin--lg"></div>
      </div>