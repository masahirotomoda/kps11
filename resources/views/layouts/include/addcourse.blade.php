@php
//学校管理と先生で振り分け
use App\Models\Course;
    switch(Auth()->user()->role){
    case "学校管理者";
    $_role ="school";
    $delbtn = "schoolCourseDelete"; //削除ボタンのルーティング名
    break;
case "先生";
    $_role ="teacher";
    $delbtn = "teacherCourseDelete"; //削除ボタンのルーティング名
    break;
default;
    $_role =null;
    $delbtn =null;
    break;
}
@endphp
{{Form::open(['url' => '/'.$_role.'/addcourse-search', 'method' => 'post'])}}
@include('layouts.include.flashmessage')
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{Form::text('i_course_name', $course_name, ['class' => 'form-control', 'maxlength' => 30,'placeholder' => 'コース名の一部'])}}
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('i_valid', '有効', $valid, ['class'=>'form-check-input','id'=>'i_valid'])}}
            <label class="form-check-label" for="i_valid">有効</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('i_invalid', '無効', $invalid, ['class'=>'form-check-input','id'=>'i_invalid'])}}
            <label class="form-check-label" for="i_invalid">無効</label>
        </div>
         @include('layouts.include.searchbtn')
         {{--<button class="btn btn-outline-primary" id="all_reset_btn" type="button" onclick="location.href='./add-course'">操作のキャンセル</button>--}}
    </div>
    <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
        @slot('word', 'コースの作り方')
        @slot('image_name','teacher38')
        @endcomponent
    </div>
</div>
{{Form::close()}}
<div class="menuSpaces">
    <button class="btn btn-secondary btn-sm" type="button" name="add" id="add">新しいコースを作る</button>
    @component('components.delButton')
    @slot('route', $delbtn)
    @slot('id', 'course-checks')
    @slot('name', 'コース')
    @slot('message','')
    @endcomponent
</div>
@include('layouts.include.alertmessage')
<p>コース数:{{$courses -> count()}}件</p>
<table class="table tablecourse" id="mainTable">
    <thead>
        <tr>
            <th>
            @component('components.tooltip')
                @slot('word', '削除')
                @slot('message', '表示データをまとめて削除する時はここをチェック。個別削除は、個別にチェック')
            @endcomponent
            <input type="checkbox" id="checkAll" value="1">
            </th>
            <th>無効</th>
            <th>
                @component('components.modalimagerequired')
                @slot('word', '出題順（数字/重複×）')
                @slot('image_name', 'teacher36')
                @endcomponent
            </th>
            <th>コース名<span class="red">*</span>(最大30文字)</th>
            <th>
                @component('components.modalimage')
                @slot('word', 'ﾗﾝﾀﾞﾑ')
                @slot('image_name','teacher37')
                @endcomponent
            </th>
            <th>編集・保存</th>
            <th>登録日<br>更新日</th>
            <th>
                @component('components.tooltiprequired')
                @slot('word', '単語')
                @slot('message', 'コースに、かならずタイピングする単語を登録します')
                @endcomponent
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($courses as $cs)
        @if ($cs->invalid === 1)
        <tr class="table-secondary">
            @else
        <tr>
            @endif
            <td><input type="checkbox" form="course-checks" class="form-check-input" name="checks[]" value="{{ $cs->id }}" id="{{ $cs->course_name }}"></td>
            {{Form::open(['url' => '/'.$_role.'/add-course-new', 'files' => true , 'method' => 'post'])}}
            {{Form::hidden('course_id',$cs->id,['id'=>'course_id'])}}
            {{Form::hidden('school_id',$cs->school_id,['id'=>'school_id'])}}
            <td>{{Form::checkbox('invalid', '1', $cs->invalid, ['class'=>'form-check-input','disabled']) }}</td>
            <td>{{Form::text('display_order',$cs->display_order,['class' => 'form-control col-xs-2','maxlength' => 4,'readonly'])}}</td>
            <td>{{Form::text('course_name',$cs->course_name,['class' => 'form-control col-xs-2','maxlength' => 30,'readonly'])}}</td>
            <td>{{Form::checkbox('is_random', '1', $cs->random, ['class'=>'form-check-input','disabled'])}}</td>
            <td nowrap>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit" id="edit" val="1" >編集</button>
                {{Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled'])}}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>
            </td>
            <td>{{date_format($cs->created_at, 'Y/m/d')}}<br>
                {{date_format($cs->updated_at, 'Y/m/d')}}
            </td>
            {{Form::close() }}
            {{Form::open(['url' => '/'.$_role.'/addcourse-add-word', 'method' => 'post', 'class' => 'd-flex p-1', 'style' => 'width:90%'])}}
            {{Form::hidden('course_id',$cs->id) }}
            {{Form::hidden('school_id',$cs->school_id,['id'=>'school_id'])}}
            <td>{{ Form::submit('単語の登録・編集', ['name'=>'addWord','class'=>'btn btn-primary btn-block',])}}</td>
            {{Form::close() }}
        </tr>
        @endforeach
    </tbody>
</table>
<div class="hidden">
    {{Form::select('nd_category_selection',config('configrations.CATEGORY_LIST'),null,['class' => 'form-select','id' => 'nd_category_selection'])}}
    {{Form::select('nd_level_selection',config('configrations.LEVEL_LIST'),null,['class' => 'form-select','id' => 'nd_level_selection'])}}
</div>