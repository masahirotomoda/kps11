{{--【コンテスト用】タイピングコース名--}}
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb typingbc">
        <li class="breadcrumb-item" id="BackToTopmenuBtn"> <a href="{{url('/')}}/{{$contest_account}}/contest">コースメニュー</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ $course->course_name }}</li>
      </ol>
    </nav>