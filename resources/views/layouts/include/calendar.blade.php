<div class="menuSpacesCell mscLong">
        <div class="plusText">
            <span class="inputText">開始日時</span>
            {{ Form::date('start_date',$startDate,['id' => 'start_date','class' => 'form-control']) }}
            {{ Form::time('start_time',$startTime,['id' => 'inputMDEx1','class' => 'form-control']) }}
        </div>
        <div class="plusText">
            <span class="inputText">終了日時</span>
            {{ Form::date('end_date',$endDate,['id' => 'end_date','class' => 'form-control']) }}
            {{ Form::time('end_time',$endTime,['id' => 'inputMDEx2','class' => 'form-control']) }}
        </div>
  </div>