@php  //ページごとのconfig情報を取得
$pagination = null;
$currentURL=request()->path();

    foreach (config('configrations.PAGE_INFO') as $url => $url_info) {
                if ($currentURL == $url){
                        $pagination = $url_info['PAGINATION'];  //ページネーション情報
                };
            };
switch ($pagination) {
    case "users";
        $pagination_db = $users;
        break;
    case "courses";
        $pagination_db = $courses;
        break;
    case "bestScore";
        $pagination_db = $bestScore;
        break;
    case "information";
        $pagination_db = $information;
        break;
     case "scoreHistory";
        $pagination_db = $scoreHistory;
        break;
    case "classes";
        $pagination_db = $classes;
        break;
    case "login_logs";
        $pagination_db = $login_logs;
        break;
    case "blog_private";
        $pagination_db = $blog_private;
        break;
    case "grades";
        $pagination_db = $grades;
        break;
    case "kumi";
        $pagination_db = $kumi;
        break;
    case "schools";
        $pagination_db = $schools;
        break;
    case "courseWords";
        $pagination_db = $courseWords;
        break;
    case "blogs";
        $pagination_db = $blogs;
        break;
    case "trials";
        $pagination_db = $trials;
        break;
    case "notes";
        $pagination_db = $notes;
        break;
    case "battlescoreHistory";
        $pagination_db = $BattleScoreHistory;
        break;
    default;
        $pagination_db = null;
}
@endphp
@if($pagination_db!== null)
<div class = "pagination_wrap">
    <div class = "pagination_link">{{--getパラメータでページネーション,トークンをURLに含めない except()⇒～以外、laravelの仕様でgetパラメータはtokenなしでOK--}}
    {{$pagination_db->appends(request()->except('_token'))->links()}}
    </div>
    @if (count($pagination_db) >0)
    <div class = "pagination_detail">
        <p>全{{ $pagination_db->total() }}件中
            {{  ($pagination_db->currentPage() -1) * $pagination_db->perPage() + 1}} -
            {{ (($pagination_db->currentPage() -1) * $pagination_db->perPage() + 1) + (count($pagination_db) -1)  }}件を表示。</p>
        <div class="pagetop"><a href="#top"><span><img src="/img/pagetop.svg" alt="ページトップ"/></span></a></div>
    </div>
    @else
    <div class = "pagination_no">
    <p class="text-danger">データがありません。</p>
    </div>
    @endif
</div>
@endif