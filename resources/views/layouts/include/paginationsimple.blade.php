@php  
//ページごとのconfig情報を取得、【ページ下のページネーション】
//ページ上部ページネーションは、「全〇〇件中〇件」と表示させるが、下部のは必要ない、これは下部用のページ番号のみのページネーション
//共通bladeの「base.blade.php」でのみ使用

$pagination = null;
$currentURL=request()->path();

    foreach (config('configrations.PAGE_INFO') as $url => $url_info) {           
                if ($currentURL == $url){                                                 
                        $pagination = $url_info['PAGINATION'];  //ページネーション情報
                };
            }; 

switch ($pagination) {
    case "users";
        $pagination_db = $users;
        break;
    case "courses";
        $pagination_db = $courses;
        break;
    case "bestScore";
        $pagination_db = $bestScore;
        break;
    case "information";
        $pagination_db = $information;
        break;
    case "scoreHistory";
        $pagination_db = $scoreHistory;
        break;   
    case "classes";
        $pagination_db = $classes;
        break;
    case "login_logs";
        $pagination_db = $login_logs;
        break;
    case "blog_private";
        $pagination_db = $blog_private;
        break;
    case "grades";
        $pagination_db = $grades;
        break;
    case "kumi";
        $pagination_db = $kumi;
        break;
    case "schools";
        $pagination_db = $schools;
        break;
    case "blogs";
        $pagination_db = $blogs;
        break;
    case "courseWords";
        $pagination_db = $courseWords;
        break;
    case "trials";
        $pagination_db = $trials;
        break;
    default;
        $pagination_db = null;
}
              
@endphp
@if($pagination_db!== null)
<div class = "pagination_wrap">
    <div class = "pagination_link">
        {{ $pagination_db->appends(request()->input())->links() }}        
    </div>
</div>
@endif

