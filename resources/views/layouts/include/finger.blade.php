<div class="p-hands mt-4">
        <!-- 手 -->
        <!-- 左手 -->
        <div class="p-fingers">
          <div class="p-fingers__finger--md" id="p-fingers__finger--llittle"></div><!-- 小指 -->
          <div class="p-fingers__finger--lg" id="p-fingers__finger--lthird"></div><!-- 薬指 -->
          <div class="p-fingers__finger--xl" id="p-fingers__finger--lsecond"></div><!-- 中指 -->
          <div class="p-fingers__finger--lg" id="p-fingers__finger--lfirst"></div><!-- 人差し指 -->
          <div class="p-fingers__finger--sm ml-2" id="p-fingers__finger--lthumb"></div><!-- 親指 -->
        </div>
        <!-- 右手 -->
        <div class="p-fingers">
          <div class="p-fingers__finger--sm mr-2" id="p-fingers__finger--rthumb"></div><!-- 親指 -->
          <div class="p-fingers__finger--lg" id="p-fingers__finger--rfirst"></div><!-- 人差し指 -->
          <div class="p-fingers__finger--xl" id="p-fingers__finger--rsecond"></div><!-- 中指 -->
          <div class="p-fingers__finger--lg" id="p-fingers__finger--rthird"></div><!-- 薬指 -->
          <div class="p-fingers__finger--md" id="p-fingers__finger--rlittle"></div><!-- 小指 -->
        </div>
</div>