@php
$search_btn = null;
$currentURL=request()->path();
    foreach (config('configrations.PAGE_INFO') as $url => $url_info) {
        if ($currentURL == $url){
                $search_btn = $url_info['SEARCH_BTN'];  //すべて表示ボタンの遷移先
        };
    };
@endphp
<button class="btn btn-outline-primary searchIcon" type="submit" name="search">検索</button>
<button class="btn btn-outline-primary" id="all_btn" type="button" onclick="location.href='{{$search_btn}}'">すべて表示</button>