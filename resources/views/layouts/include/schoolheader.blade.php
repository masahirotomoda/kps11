@php
//$schoolのlogoカラムがattributeで取得できないため、modelに関数をつくり直接取得
use App\Models\School;

$Role = Auth()->user()->role;
$schoolid = auth()->user()->school_id;
$school_Int_Id = auth()->user()->school_int_id;
$school_logo_no=School::getSchoolLogoNo($school_Int_Id);//Modelに関数作成
$avatar_name=auth()->user()->avatar;

switch($Role){
  case '生徒':
    $homeurl ="/login";
    $username = Auth::user()->name;
    $btnurl ="/student/profile";
    $btnname = "マイメニュー";
    $schoolname = $school->name;
    $displayname = $school->display_name;

    if($school_logo_no['logo'] == 0){
      $className='navbar-brand';
    } else {
      $className='navbar-brand'.$school_logo_no['logo'];
    }
    break;
  case '先生':
    $homeurl = "/login";
    $username = Auth::user()->name." 先生";
    $btnurl = "/teacher/menu";
    $btnname = "先生メニュー";
    $schoolname = $school->name;

    if($school_logo_no['logo'] == 0){
      $className='navbar-brand';
    } else {
      $className='navbar-brand'.$school_logo_no['logo'];
    }
    break;
  case '学校管理者':
    $username = Auth::user()->name;
    $btnurl = "/school/menu";
    $btnname = "学校管理メニュー";
    $schoolname = $school->name;

    if($school_logo_no['logo'] == 0){
      $className='navbar-brand';
    } else {
      $className='navbar-brand'.$school_logo_no['logo'];
    }
    if($school->contest>0){
    $homeurl = "/$contest_account/contest";
    } else {
      $homeurl = "/school-login";
    }
    break;
  case 'システム管理者': //ナレッジは固定なので⇒各コントローラーschool情報を取得する必要なし
    $homeurl = "/system-login";
    $username = Auth::user()->name;
    $btnurl = "/system-menu";
    $btnname = "システム管理メニュー";
    $schoolname = "ナレッジ・プログラミングスクール";
    $className='navbar-brand';
    break;
  default:
    $homeurl = "url()";
    $username = null;
    $btnurl = "url()";
    $btnname = null;
    $schoolname = null;
  }

@endphp
<header>
{{--dd(School::getSchoolLogoNo($school_Int_Id))--}}
    <div id="headerInner" class="plusNameLarge">
      <h1><a class="{{$className}}" href="{{url($homeurl)}}">ナレッジタイピング</a></h1>
      @if($Role === '生徒'){{--先生、学校管理者、システム管理者は表示させないため、入れ子--}}
      @if($school->display_name === 1)
      <div class="headerInfo nameLarge">{{$username}}</div>
      @endif
      @endif
      <div class="headerInfo headerSchool">{{ $schoolname }}</div>
      <div class="headerInfo headerName">{{ $username }}</div>
      <div class="headerBtn">

        {{-- ↓ここのclassがフラグ tsi_redは赤、tsi_pinkはピンク、tsi_blueは青、tsi_yellowは黄色、tsi_whiteは白、tsi_greenは緑、それぞれの背景とキャラが表示されます。 --}}
        @if($Role === '生徒' || $Role === '先生')
          @if($avatar_name==null)
          <div class="tsicons tsi_default"></div>
          @else
          <div class="tsicons tsi_{{$avatar_name}}"></div>
          @endif
        @endif
        <button type="button" class="btn btn-primary" onclick="location.href='{{url($btnurl)}}'">{{ $btnname }}</button><br>
        <button type="button" class="btn btn-primary" onclick="location.href='/logout'">ログアウト</button><br>
      </div>
    </div>
</header>
