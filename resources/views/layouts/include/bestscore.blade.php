@php
if(Auth()->user()->role === '学校管理者'){
    $_role = 'school';
} elseif(Auth()->user()->role === '先生'){
    $_role = 'teacher';
}
@endphp
{{--学校管理者と先生で切り分け form開始 検索＆エクセル出力--}}
{{Form::open(['url' => '/'.$_role.'/bestscore-search', 'method' => 'post','id'=>'search_form'])}}
@include('layouts.include.alertmessage')
<div class="menuSpaces">
    <div class="menuSpacesCell spaceAll">
        {{--トライアルは学年、組のセレクトボックス非表示--}}
        @if($school->trial === 0)
            {{ Form::select('grade', $grade_selection, $grade, ['class' => 'form-select','id' => 'grade','placeholder' => '学年']) }}
            {{ Form::select('kumi', $kumi_selection, $kumi, ['class' => 'form-select','id' => 'kumi','placeholder' => '組']) }}
        @endif
        {{ Form::text('name', $name, ['type' => 'text' , 'class' => 'form-control', 'maxlength' => 20 ,'id' => 'name','placeholder' => '生徒名の一部']) }}
        {{ Form::text('course_name', $courseName, ['type' => 'text' , 'class' => 'form-control', 'maxlength' => 40 ,'id' => 'course_name','placeholder' => 'コース名の一部']) }}

        @include('layouts.include.searchbtn')
        {{--<button class="btn btn-outline-primary"  id="all_reset_btn" type="button" onclick="location.href='./bestscore'">操作ｷｬﾝｾﾙ</button>--}}
    </div>
    <div class="menuSpacesCell spaceNone">
        {{ Form::close() }}
        {{--学校管理者と先生で切り分け form開始 記録証印刷--}}
        {{ Form::open(['url' => '/'.$_role.'/transcript-bestscore-print-pdf', 'method'=>'post','id'=>'pdf']) }}
        <button name="pdf1" id="pdf1" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">記録証・印刷用PDF（シンプル）</button>
        <button name="pdf2" id="pdf2" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">記録証・印刷用PDF（カラフル）</button>
    </div>
    <div class="menuSpacesCell">
        <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
            @slot('word', '印刷方法')
            @slot('image_name','teacher34')
        @endcomponent
        </div>
        <div class="manualSpace links links-sm">
        @component('components.modalimage')
            @slot('word', 'スコア履歴との違い')
            @slot('image_name', 'teacher22')
        @endcomponent
        </div>
    </div>
</div>
@include('layouts.include.pagination')
<div class="menuSpaces topMinusBox">
    {{ Form::submit('データの一括ダウンロード(Excel)', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm rightBox','form'=>'search_form']) }}
    <div class="manualSpace links bottomBox">
        @component('components.modalimage')
        @slot('word', 'ダウンロード方法')
        @slot('image_name', 'teacher35')
        @endcomponent
    </div>
</div>
<table class="table bestscore" id="mainTable">
    <thead>
        <tr>
            <th scope="col">
                @component('components.tooltip')
                    @slot('word', '印刷')
                    @slot('message', '表示データをまとめて印刷する時はここをチェック。個別印刷は、個別にチェック')
                @endcomponent
                <input type="checkbox" id="checkAll" value="1">
            </th>
            <th scope="col">学年</th>
            <th scope="col">組</th>
            <th scope="col">出席No</th>
            <th scope="col">生徒名</th>
            <th scope="col">コース名</th>
            <th scope="col">最高点</th>
            <th scope="col">
                @component('components.tooltip')
                    @slot('word', '級')
                    @slot('message', '級は「キータッチ2000検定コース」でのみ表示')
                @endcomponent
            </th>
            <th scope="col">日時</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($bestScore as $bs)
        <tr>
            <td><input type="checkbox" class="checks" name="checks[]" value="{{ $bs->bestpoint_id }}" id="{{ 'pdf'.$bs->bestpoint_id }}"></td>
            <td>{{ $bs->grade_name }}</td>
            <td>{{ $bs->kumi_name }}</td>
            <td>{{ $bs->attendance_no }}</td>
            <td>{{ $bs->name }}</td>
            <td>{{ $bs->course_name }}</td>
            <td>{{ $bs->best_point }}</td>
            <td>{{ $bs->bestpoint_rank }}</td>
            <td>{{ date_format(new DateTime($bs->b_created_at),'Y/m/d H:i') }} </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ Form::close() }}
