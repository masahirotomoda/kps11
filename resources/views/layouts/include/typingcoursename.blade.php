    {{--タイピングコース名 使用ファイル user/free/typingとtypingtest--}}
    <nav aria-label="breadcrumb">
      <form action="/free-top" method="post" name="typingTitleCourseForm" id="typingTitleCourseForm">
        @csrf
        <input type="hidden" name="tab" value="{{$course->tab}}">
        <ol class="breadcrumb typingbc">
          <li class="breadcrumb-item" id="BackToTopmenuBtn" ><button type="submit" name="gotopbtn" > コースメニュー</button></li>
          <li class="breadcrumb-item active" aria-current="page">{{$course->course_name}}</li>
        </ol>
    </form>
    </nav>