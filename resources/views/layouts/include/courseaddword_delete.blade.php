@php
use App\Models\Course;
switch(Auth()->user()->role){
    case "学校管理者";
    $_role ="school";
    $delbtn = "schoolWordDelete"; //削除ボタンのルーティング名
    break;
case "先生";
    $_role ="teacher";
    $delbtn = "teacherWordDelete";
    break;
default;
    $_role =null;
    $delbtn = null;
    break;
}
@endphp
{{--★hiddenでschool_idを取得しているが、addnew-courseaddword.jsで使うので削除しないこと--}}

@include('layouts.include.flashmessage')
<table class="table addcourseword" id="mainTable">
    <thead>
        <tr>
            <th scope="col">無効 or 有効 ?</th>
            <th scope="col">コース名</th>
            <th scope="col">単語の出題順</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                @if($course->invalid == 0)
                    {{"有効"}}
                @else
                    <p class="text-danger">{{"無効"}}</p>
                @endif
            </td>
            <td>{{ $course->course_name }}</td>
            <td>
                @if($course->random === 1)
                    {{"ランダム出題"}}
                @else
                    {{"出題順"}}<br><small>{{"（下記の「出題順」の昇順)"}}</small>
                @endif
            </td>
        </tr>
        {{Form::hidden('school_id',$school_id,['id'=>'school_id']) }}
        {{ Form::hidden('course_id',$course_id,['id'=>'course_id']) }}
    </tbody>
</table>
<div class="menuSpaces msCenter">
    @component('components.delButton')
    @slot('route', $delbtn)
    @slot('id', 'word-checks')
    @slot('name', '単語')
    @slot('message','')
    @endcomponent
    {{--すべて表示とリセットは同じ挙動なので、名前は「all_btn」で同じにする--}}
    <button class="btn btn-outline-primary btn-sm" type="button" name="add" id="add">単語を登録する</button>
    {{ Form::open(['url' => Auth::user()->school_id.'/'.$_role.'/'.'addcourse-add-word', 'method' => 'post']) }}
    {{ Form::submit('すべて表示', ['name'=>'all_btn','id'=>'all_btn','class'=>'btn btn-outline-primary']) }}
    {{ Form::submit('操作のキャンセル', ['name'=>'all_btn','id'=>'all_reset_btn','class'=>'btn btn-outline-primary']) }}
    {{ Form::hidden('course_id',$course_id) }}
    {{ Form::close() }}

    {{--以下の内容は、よみがなのモーダル画像でいれるため、最終的に削除、内容確認のために、今はのこしている--}}
    @component('components.modal')
    @slot('btn_name', 'よみがなで使える文字一覧')
    @slot('title', '単語「よみがな」で使える文字')
    @slot('message', 'ひらがな、アルファベット大文字小文字、数字、特殊文字「ー」「、」「。」「.」「:」「;」「全角スペース」「半角スペース」※カタカナ、漢字は使えません。')
    @endcomponent
{{--以下は操作のキャンセルの行の右はじにおきたい--}}
    <div class="manualSpace spaces links">
    @component('components.modalimage')
    @slot('word', 'タイピングコースの作り方')
    @slot('image_name', 'studet_profile1.png')
    @endcomponent
</div>

    {{ Form::hidden('course_id',$course_id,['form' => 'word-checks' ]) }}
    {{Form::hidden('school_id',$school_id,['form' => 'word-checks' ]) }}
</div>
@include('layouts.include.alertmessage')
@if ($courseWords->count() >0)
   <p>単語登録：{{ $courseWords->count() }}件<p>
@else
    <p class="text-danger">単語が未登録です。</p>
@endif
<table class="table tablecoursewords" id="mainTableTwo">
    <thead>
        <tr>
            <th>
                @component('components.tooltip')
                    @slot('word', '削除')
                    @slot('message', '表示データをまとめて削除する時はここをチェック。個別に削除するときは、個別にチェック')
                @endcomponent
                <input type="checkbox" id="checkAll" value="1" class="form-check-input">
            </th>
            <th>@component('components.tooltip')
                    @slot('word', '出題順(数字)')
                    @slot('message', '最大4桁。昇順で出題されます。ランダム出題なら、入力する必要はありません')
                @endcomponent
            </th>
            <th>
                {{--以下はなしにして--}}
                    @component('components.tooltiprequired')
                    @slot('word', '単語')
                    @slot('message', 'タイピング画面には、ここでの入力が表示される。ひらがな、カタカナ、漢字、数字、アルファベット可')
                    @endcomponent
                {{--以下に変更します（画像のが説明がわかるため）--}}
                    @component('components.modalimagerequired')
                    @slot('word', '単語')
                    @slot('image_name', 'studet_profile1.png')
                    @endcomponent   
            <small>（例）歩く、ミカン、がっこう、CAT、dog</small>
            </th>
            <th>
                {{--以下はなしにして--}}
                    @component('components.tooltiprequired')
                    @slot('word', 'よみがな')
                    @slot('message', 'アルファベットに変換される。（例）さくら（タイピング画面ではsakura）、Apple（アルファベットはそのまま表示）')
                    @endcomponent
                {{--以下に変更します（画像のが説明がわかるため）--}}
                    @component('components.modalimagerequired')
                    @slot('word', 'よみがな')
                    @slot('image_name', 'studet_profile1.png')
                    @endcomponent                   
                <small>（例）あるく、みかん、CAT、dog ⇒ ひらがな、ｱﾙﾌｧﾍﾞｯﾄ <font class="oranges">※漢字・カタカナ不可</font></small>
            </th>
            <th>編集・保存</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($courseWords as $courseWord)
        <tr>
            <td><input type="checkbox" form="word-checks" class="form-check-input" name="checks[]" value="{{ $courseWord->id }}" id="{{$courseWord->word_mana }}"></td>
            {{ Form::open(['url' => Auth::user()->school_id.'/'.$_role.'/add-word', 'files' => true ,'enctype' => 'multipart/form-data', 'method' => 'post']) }}
            {{ Form::hidden('course_id',$course_id) }}
            {{ Form::hidden('school_id',$school_id)}}
            {{ Form::hidden('id',$courseWord->id) }}
            <td>{{ Form::text('display_order',$courseWord->display_order,['class' => 'form-control col-xs-2','maxlength' => '4','readonly']) }}</td>
            <td>{{ Form::text('word_mana',$courseWord->word_mana,['class' => 'form-control col-xs-2','maxlength' => '50','readonly']) }}</td>
            <td>{{ Form::text('word_kana',$courseWord->word_kana,['class' => 'form-control col-xs-2','maxlength' => '50','readonly']) }}</td>


            <td nowrap>

                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm', 'disabled']) }}
                {{ Form::close() }}
                {{--★キャンセルボタンで、ファイル選択画像名（未アップロード）をクリアにできないため、キャンセルボタン押下は「すべて表示」と同じメソッドにとばす、コースIDをもちまわるため、リダイレクトできない--}}
                {{ Form::open(['url' => Auth::user()->school_id.'/'.$_role.'/'.'addcourse-add-word', 'method' => 'post']) }}
                {{ Form::submit('ｷｬﾝｾﾙ', ['name'=>'editcancel','class'=>'btn btn-outline-primary btn-sm','disabled']) }}
                {{ Form::hidden('course_id',$course_id) }}
                {{ Form::close() }}

            </td>
        </tr>
        @endforeach
    </tbody>
</table>