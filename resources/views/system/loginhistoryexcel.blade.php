<table>
    <tr>
    <th>日時</th>
    <th>学校int_id</th>
    <th>学校名</th>
    <th>ログインID</th>
    <th>user_id</th>
    <th>ロール</th>
    <th>名前</th>
    <th>From</th>
    <th>IPアドレス</th>
    <th>OS</th>
    <th>ブラウザ</th>
    </tr>
    <tbody>
        @foreach ($login_logs as $logs)
        <tr>
        <td>{{ $logs->created_at }}</td>
        <td>{{ $logs->login_school_int_id }}</td>
        <td>{{ $logs->login_schoolname }}</td>
        <td>{{ $logs->login_id }}</td>
        <td>{{ $logs->login_user_id }}</td>
        <td>{{ $logs->role }}</td>
        <td>{{ $logs->login_username }}</td>
        <td>{{ $logs->login_from }}</td>
        <td>{{ $logs->ipaddress }}</td>
        <td>{{ $logs->os }}</td>
        <td>{{ $logs->browser }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
