@extends('layouts.base')

@section('js')
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/system/checkedit-class.js"></script>
<script src="/js/system/addnew-class.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/btn_all_reset.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>
@endsection

@section('content')

@php
//編集ボタン押下で、学年、組のセレクトボックス値を学校ごとに取得するため必要
use App\Models\Grade;
use App\Models\Kumi;
@endphp

@include('layouts.include.flashmessage')
{{--★検証確認後、削除  <div id="SearchOrEditMode">学校選択モード(selected)</div>  --}}
{{ Form::open(['url' => '/class-search', 'method' => 'post']) }}
{{ Form::hidden('selected',$selected,['id'=>'selected']) }}
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{ Form::text('school_name', $school_name, ['class' => 'form-control', 'id' => 'school_name', 'placeholder' => '学校名の一部']) }}
        {{-- Form::text('school_id', $sc_id, ['class' => 'form-control', 'id' => 'sc_id', 'placeholder' => '学校ID']) --}}{{--バグが起こるのでなし--}}
        @include('layouts.include.searchbtn')
        {{--★リセットと同じ挙動、操作が少し複雑なので設置--}}
        <button class="btn btn-outline-primary" id="all_reset_btn" type="button" onclick="location.href='./class'">操作ｷｬﾝｾﾙ</button>
    </div>
</div>
{{ Form::close() }}
{{ Form::open(['url' => '/class-select', 'method' => 'post']) }}
{{--★「selected」は学校を選んだかのフラグ--}}
{{ Form::hidden('selected',$selected,['id'=>'selected2']) }}
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{--★学校を選んだ時--}}
        @if($selected === true)
        {{ Form::select('school_name_search',$school_search_selection,$school_name_search,['class' => 'form-select','id' => 'school_new_selection','form' => 'insert_btn']) }}
        {{--★id名「grade_new_selection」「kumi_new_selection」の値をaddnew-class.js で使うためhidden--}}
        {{ Form::select('grade_new_select',$grade_new_selection,$grade_new_select,['class' => 'form-select','id' => 'grade_new_selection','form' => 'insert_btn','hidden']) }}
        {{ Form::select('kumi_new_select',$kumi_new_selection,$kumi_new_select,['class' => 'form-select','id' => 'kumi_new_selection','form' => 'insert_btn', 'hidden']) }}
        @else
        {{--★学校を選んでいない（検索モードで）時--}}
        {{ Form::select('school_name_search',$school_search_selection,$school_name_search,['class' => 'form-select','id' => 'school_new_selection']) }}
        @endif

        {{--★学校を選んでいない（検索モードで）時--}}
        @if($selected === false)
        <button name="select" type="submit" class="btn btn-secondary btn-sm">選んだ学校にクラスをつくる</button>
        @else
        {{--★学校を選んだ時--}}
        <button name="add" id="add" type="button" class="btn btn-secondary btn-sm">学年と組を入力する</button>
        @endif
    </div>
</div>
{{ Form::close() }}
<div class="menuSpaces">
    @component('components.delButton')
    @slot('route', 'classInfoDelete')
    @slot('id', 'class-checks')
    @slot('name', 'クラス')
    @slot('message','クラスを削除すると、そのクラスに所属する生徒のクラス情報がなくなります。※生徒は削除されません')
    @endcomponent
</div>
@include('layouts.include.alertmessage')
@include('layouts.include.pagination')
<table class="table classinfosearch" id="mainTable">
    <thead>
        <tr>
            <th>削除<input type="checkbox" id="checkAll" value="1"> </th>
            <th scope="col">【学校ID】学校</th>
            <th scope="col">クラスID</th>
            <th scope="col">学年*<small><br>「なし」も作る事！</small></th>
            <th scope="col">組*<small><br>「なし」も作る事！</small></th>
            <th scope="col">編集・保存</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($classes as $class)
        {{ Form::open(['url' => '/class-add','method' => 'post']) }}
        {{ Form::hidden('school_id', $class->school_int_id)}}
        {{ Form::hidden('id', $class->class_id)}}
        <tr>
            {{--★idに削除するデータの名前を持たせる⇒del_modal.jsで削除確認モーダルで名前を表示させるため--}}
            <td><input type="checkbox" class="checks" name="checks[]" form="class-checks" value="{{ $class->class_id }}" id="【{{ $class->school_name}}】{{$class->grade_name}}　{{$class->kumi_name}}"></td>
            <td>【{{ $class->sc_id }}】　{{ $class->school_name }}</td>
            <td>{{ $class->class_id }}</td>
            <td> {{ Form::select('grade_id',Grade::selectListById($class->school_int_id)
                                        ,$class->grade_id,['class' => 'form-select','id' => 'grade','disabled']) }}
            </td>
            <td>{{ Form::select('kumi_id',Kumi::selectListById($class->school_int_id)
                                        ,$class->kumi_id,['class' => 'form-select','id' => 'kumi','disabled']) }}</td>
            <td nowrap>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>
                {{ Form::close() }}
                {{ Form::open(['url' => '/class-student','method' => 'post']) }}
                {{ Form::hidden('class_id', $class->class_id)}}
                <button class="btn btn-secondary btn-sm" name="student" id="student" type="submit">生徒</button>
            </td>
            {{ Form::close() }}
        </tr>
        @endforeach
    </tbody>
</table>

@endsection
