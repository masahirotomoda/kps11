<table>
    <thead>
    <tr>
        <th>学校管理No</th>
        <th>学校ID</th>
        <th>学校名</th>
        <th>学年</th>
        <th>組</th>
        <th>出席番号</th>
        <th>ロール</th>
        <th>ユーザID</th>
        <th>ユーザ名</th>
        <th>コースID</th>
        <th>コース名</th>
        <th>最高点</th>
        <th>級(ｷｰﾀｯﾁ)</th>
        <th>日付</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($bestScore as $bs)
        <tr>

        <td>{{ $bs->school_int_id }}</td>
        <td>{{ $bs->school_id}}</td>
        <td>{{ $bs->school_name}}</td>
        <td>{{ $bs->grade_name }}</td>
            <td>{{ $bs->kumi_name }}</td>
            <td>{{ $bs->attendance_no }}</td>
            <td>{{ $bs->role }}</td>
            <td>{{ $bs->user_id }}</td>
            <td>{{ $bs->name }}</td>
            <td>{{ $bs->course_id }}</td>
            <td>{{ $bs->course_name }}</td>
            <td>{{ $bs->best_point }}</td>
            <td>{{ $bs->rank }}</td>
            <td>{{ date_format(new DateTime($bs->updated_at),'Y年m月d日 H:i') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
