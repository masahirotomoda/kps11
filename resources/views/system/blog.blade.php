@extends('layouts.base')
@section('js')
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection
@section('head_2')
<link href="{{ asset('/css/trix.css') }}" rel="stylesheet">
@endsection

@section('content')
@include('layouts.include.flashmessage')
@include('layouts.include.pagination')

<div class="menuSpaces">
    {{ Form::open(['url' => '/blog-search','method' => 'post']) }}

    <div class="menuSpacesCell spaceAll">
        {{ Form::text('search_box_title', $search_box_title, ['class' => 'form-control', 'maxlength' => 20,'placeholder' => 'タイトルの一部']) }}
        {{ Form::text('search_box_article', $search_box_article, ['class' => 'form-control', 'maxlength' => 20,'placeholder' => '本文の一部']) }}
        @include('layouts.include.searchbtn')
    </div>
    {{ Form::close() }}
    {{ Form::open(['url' => '/blog-edit', 'method' => 'post' ]) }}
    {{ Form::token() }}
    <div class="menuSpacesCell spaceAll">
        <button name="add" type="submit" class="btn btn-secondary btn-sm">新しいブログを書く</button>
        <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
        @slot('word', '使い方')
        @slot('image_name','system17.png')
        @endcomponent
        </div>
    </div>
</div>
{{ Form::close() }}

<div class="infoBlock">
    @foreach ($blogs as $blog)
    <div class="infoBlockCell">
        <div class="infoBlockCellImage">
            @if($blog->image === null)
            no image
            @else
            <img src="{{ asset('storage/blogimages\/').$blog->image }}" width="200">
            @endif
        </div>
        <div class="infoBlockCellText">
            <div class="infoBlockCellTitle">
            @if($blog->private == 0)
            <font class="blues">【公開中】</font>
            @else
            <font class="oranges">【非公開】</font>
            @endif
            {!! $blog->title !!}</div>
            {{--!!を使わないと、htmlタグが表示される--}}
            <p>{!! $blog->article !!}</p>
        </div>
        <div class="btnOuter">
            {{ Form::open(['url' => '/blog-edit', 'method' => 'post' , 'name' => 'ansform']) }}
            {{ Form::token() }}
            {{ Form::hidden('id',$blog->id) }}

            作成日：{{$blog->created_at}}　/　更新日：{{$blog->updated_at}}
            <input type="submit" name="edit" value="編集" class="btn btn-secondary btn-sm">
            @include('layouts.include.alertmessage')
            {{ Form::close() }}
        </div>
    </div>
    @endforeach
</div>
@endsection