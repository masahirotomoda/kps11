@extends('layouts.base')
@section('js')
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/system/addnew-course.js"></script>
<script src="/js/system/checkedit-course.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/addnew_all.js"></script>

@endsection
@section('content')
@php
use App\Models\Course;
@endphp
{{Form::open(['url' => '/course-search', 'files' => true , 'method' => 'post'])}}

@include('layouts.include.flashmessage')
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{ Form::text('i_school_name', $school_name, ['class' => 'form-control', 'placeholder' => '学校名の一部']) }}
        {{ Form::text('i_course_id', $input_course_id, ['class' => 'form-control','maxlength' => 5 ,'placeholder' => 'コースID']) }}
        {{ Form::text('i_course_name', $course_name, ['class' => 'form-control','maxlength' => 50 ,'placeholder' => 'コース名の一部']) }}
        {{ Form::select('i_course_type', $type_selection, $course_type, ['class' => 'form-select','placeholder' => 'タイプ']) }}
        {{ Form::select('i_course_category', $category_selection, $course_category, ['class' => 'form-select','placeholder' => '分類']) }}
        {{ Form::select('i_course_level', $level_selection, $course_level, ['class' => 'form-select','placeholder' => 'レベル']) }}
    </div>
</div>
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{Form::select('i_course_tab', $tab_selection, $course_tab, ['class' => 'form-select','placeholder' => 'タブ'])}}
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('i_is_free', $free, $free, ['class'=>'form-check-input',]) }}
            <label class="form-check-label" for="i_valid">無料コース</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('i_not_free', $notfree, $notfree, ['class'=>'form-check-input',]) }}
            <label class="form-check-label" for="i_invalid">有料</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('i_valid', $valid, $valid, ['class'=>'form-check-input',]) }}
            <label class="form-check-label" for="i_valid">有効</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('i_invalid', $invalid, $invalid, ['class'=>'form-check-input',]) }}
            <label class="form-check-label" for="i_invalid">無効</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('i_contest', $contest, $contest, ['class'=>'form-check-input',]) }}
            <label class="form-check-label" for="i_contest">ｺﾝﾃｽﾄ</label>
        </div>
        @include('layouts.include.searchbtn')
        <button class="btn btn-secondary btn-sm" type="button" name="wordlist" onclick="location.href='./course-wordlist'">ALL単語</button>
    </div>
</div>
{{ Form::close() }}
<div class="menuSpaces">
        <button name="add" id="add" type="button" class="btn btn-secondary btn-sm">新しいコースを作る</button>
        @component('components.delButton')
        @slot('route', 'systemCourseDelete')
        @slot('id', 'course-checks')
        @slot('name', 'タイピングコース')
        @slot('message','コースを削除すると、紐づいている「単語」「コースワード（中間テーブル）」と生徒のベストスコアも削除されます。※スコア履歴は残ります。')
        @endcomponent
</div>
@include('layouts.include.alertmessage')
@include('layouts.include.pagination')
<span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">【コンテスト】1（本番タブ・テストモード）2（本番タブ・練習モード）3（練習タブ・テストモード）4（練習タブ・練習モード）</span>
<br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">【バレッド専用長文コース】categoryで「テスト」選ぶ（記録証が～文字と表示）カテゴリーはキータッチ2000がある所</span>
<br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">【ランキングスタジアム】tabはbattle、course_typeが組込⇒練習モード、長文⇒長文モード、英語⇒英語モード、テスト⇒テストモード、表示したいコース1件以外は「無効」にする。Free版には実装しないので注意。ランキングスタジアムは、コースを削除しても履歴は削除されない。</span>
<br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">【バレッドICT検定】tabは「vldtest」、course_typeは「長文」、分類は「その他」、レベルは「その他」」</span>
<br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">【今月チャレンジコース】tabは「chg」、course_typeは「組込み」、分類は「その他」、レベルは1月～12月の該当月を選ぶ。【重要】4月スタート。必ず4月1日に12か月分をUPし、過去12か月分は「無効」にする。12か月しか有効にしてはいけない。</span>
<table class="table tablecoursesis" id="mainTable">
    <thead>
        <tr>
            <th> <input type="checkbox" id="checkAll" value="1"><br>削除</th>
            <th scope="col">無効</th>
            <th scope="col">free<br>ｺﾝﾃｽﾄ</th>
            <th scope="col" width="7%">表示順<span class="red">*<br>4桁重複×</span><br>コースID</th>
            <th scope="col">学校名<br>コース名</th>
            <th scope="col">タイプ<br>分類</th>
            <th scope="col" width="12%">レベル<br>ﾃｽﾄ時間</th>
            <th scope="col">タブ<br>ﾗﾝﾀﾞﾑ</th>
            <th scope="col">保存</th>
            <th scope="col">登録日<br>更新日</th>
            <th scope="col">単語</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($courses as $cs)
        {{ Form::open(['url' => '/course-save', 'files' => true , 'method' => 'post', 'class' => 'd-flex p-1', ]) }}
        {{ Form::hidden('id', $cs->id) }}
        @if ($cs->invalid !== 1)
        <tr>
        @else
        <tr class="bg-secondary text-white invalidity">
        @endif
            <td><input type="checkbox" form="course-checks" class="form-check-input" name="checks[]" value="{{ $cs->id }}" id="【{{$cs->course_name}}】"></div>
            <td>{{ Form::checkbox('invalid', '1', $cs->invalid, ['class'=>'form-check-input','disabled']) }}</td>
            <td>{{ Form::checkbox('is_free_course', '1', $cs->is_free_course, ['class'=>'form-check-input','disabled']) }}<br>
                {{ Form::text('contest',$cs->contest,['class' => 'form-control col-xs-2 small', 'readonly']) }}</td>
            <td>{{ Form::text('display_order',$cs->display_order,['class' => 'form-control col-xs-2','readonly']) }}
                <br>({{$cs->id}})
            </td>
            <td class="small">

            @if($cs->school_id !== null)
            {{ Form::select('school_id_selected',$school_search_selection,$cs->school_id,['class' => 'form-select','disabled']) }}<br>
            @else
            {{ Form::select('school_id_selected',$school_search_selection, null,['class' => 'form-select','disabled', 'placeholder' => '']) }}<br>
            @endif


            {{ Form::text('course_name',$cs->course_name,['class' => 'form-control col-xs-2 small', 'maxlength' => 30,'readonly','required']) }}
            </td>
            <td>{{ Form::select('course_type',$type_selection,$cs->course_type,['class' => 'form-select','disabled']) }}<br>
                {{ Form::select('course_category',Course::selectCategoryList(),$cs->course_category,['class' => 'form-select','disabled']) }}
            </td>
            <td>
                {{ Form::select('course_level',Course::selectLevelList(),$cs->course_level,['class' => 'form-select','disabled']) }}<br>
                {{ Form::text('test_time',$cs->test_time,['class' => 'form-control col-xs-2','readonly']) }}
            </td>
            <td>{{ Form::select('course_tab',$tab_selection,$cs->tab,['class' => 'form-select','disabled']) }}<br>
                {{ Form::checkbox('is_random', '1', $cs->random, ['class'=>'form-check-input','disabled']) }}</td>
            <td>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit" id="edit" val="1" >編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >キャンセル</button>
            </td>
            <td>{{ date_format($cs->created_at, 'y/m/d') }}<br>
                {{ date_format($cs->updated_at, 'y/m/d') }}
            </td>
            {{ Form::close() }}
            {{ Form::open(['url' => '/course-addword', 'files' => true , 'method' => 'post', 'class' => 'd-flex p-1', 'style' => 'width:90%']) }}
            {{ Form::hidden('course_id',$cs->id,['id'=>'course_id']) }}
            {{ Form::hidden('school_id',$cs->school_id,['id'=>'school_id']) }}
            <td>{{ Form::submit('編集', ['name'=>'addWord','class'=>'btn btn-primary btn-block',]) }}</td>
            {{ Form::close() }}
        </tr>
        @endforeach
    </tbody>
</table>
<div class="hidden">
    {{ Form::select('nd_category_selection',Course::selectCategoryList(),null,['class' => 'form-select','id' => 'nd_category_selection']) }}
    {{ Form::select('nd_level_selection',Course::selectLevelList(),null,['class' => 'form-select','id' => 'nd_level_selection']) }}
    {{ Form::select('nd_type_selection',$type_selection,null,['class' => 'form-select','id' => 'nd_type_selection']) }}
    {{ Form::select('nd_school_selection',$school_search_selection,null,['class' => 'form-select','id' => 'nd_school_selection', 'placeholder' => '学校を選ぶ']) }}
    {{ Form::select('nd_tab_selection',$tab_selection,null,['class' => 'form-select','id' => 'nd_tab_selection']) }}
</div>
@endsection