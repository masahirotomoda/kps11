@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/checkDelBtn.js') }}"></script>
<script src="{{ asset('/js/checkall-dim.js') }}"></script>
<script src="{{ asset('/js/del_modal.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection

@section('content')
@include('layouts.include.flashmessage')
<h3 class="names">【{{$trial_school->school_id}}】{{$trial_school->name}}/組：{{$trial_kumi_name}}(クラスID:{{$trial_class_id}})</h3>

<div class="manualSpace noSpaceDanger">
    <p><span class="text-danger">【トライアルから有料へ昇格】</span>①新しい学校作成②学年作成③組作成④クラス作成⑤既存ユーザーをそのまま生かす（スコア記録など保持）時は、ここで、一括変更をします。新しい学校ID、新しい学年、新しい組を入力して、トライアルフラグを外します。一括更新が成功したら、このトライアルクラスにユーザーはいなくなります。⑤次に、トライアルクラス（トライアルテーブル）を削除します。削除すると、自動でトライアルクラスが削除されます。</p>
</div>

{{ Form::open(['url' => '/trialinfo-allupdate', 'method' => 'post']) }}
{{ Form::hidden('hidden_school_id', $trial_school->id)}}
{{ Form::hidden('hidden_class_id', $trial_class_id)}}
{{ Form::hidden('hidden_grade', $trial_grade_name)}}
{{ Form::hidden('hidden_kumi', $trial_kumi_name)}}
<table class="table trialinfo-edituser" id="mainTable">
    <tr>
        <th>項目</th>
        <th>古いトライアル情報</th>
        <th>新しい学校情報</th>
        <th>備考</th>

    </tr>
    <tr>
        <td>学校int_ID</td>
        <td>{{$trial_school->id}}</td>
        <td>{{ Form::text('new_school_int_id',null,['class' => 'form-control col-xs-2',]) }}</td>
        <td>先に、新しい学校を作成しておくこと。（例）15　※半角数字　<span class="red">※存在しない学校int_idを入力すると、エラー</span>します。</td>
    </tr>
    <tr>
        <td>学校ID</td>
        <td>{{$trial_school->school_id}}</td>
        <td>{{ Form::text('new_school_id',null,['class' => 'form-control col-xs-2',]) }}</td>
        <td>URLで使用（例）abc12345　※英数8文字　<span class="red">※存在しない学校idを入力すると、エラー</span>します。</td>
    </tr>
    <tr>
        <td>学年</td>
        <td>{{$trial_grade_name}}</td>
        <td>{{ Form::text('new_grade_name',null,['class' => 'form-control col-xs-2',]) }}</td>
        <td>先に、新しい学年を作成しておくこと。（例）1年　※存在しない学年を入力するとバリデーションではじかれます。</td>
    </tr>
    <tr>
        <td>組</td>
        <td>{{$trial_kumi_name}}</td>
        <td>{{ Form::text('new_kumi_name',null,['class' => 'form-control col-xs-2',]) }}</td>
        <td>先に、新しい組を作成しておくこと。（例）3組、わかば組　※存在しない組を入力するとバリデーションではじかれます。</td>
    </tr>
    <tr>
        <td>ﾄﾗｲｱﾙﾌﾗｸﾞ</td>
        <td>1</td>
        <td>{{ Form::text('new_trial_flg',null,['class' => 'form-control col-xs-2',]) }}</td>
        <td>ﾌﾗｸﾞ解除には「0」ゼロを入力。解除すると、トライアル期限にかかわらずログインできる。※１か０以外を入力するとバリデーションではじかれます。</td>
    </tr>
</table>

<div class="btnOuter">
<button class="btn btn-secondary btn-sm" type="submit" name="update_btn" id="update_btn">一括更新する</button>
</div>
{{ Form::close() }}

@component('components.delButtonHidden')
    @slot('route', 'systemTrialInfoUserDelete')
    @slot('id', 'user-checks')
    @slot('name', 'ユーザー')
    @slot('hidden_name', 'hidden_class_id')
    @slot('hidden_value', $trial_class_id)
    @slot('message','※ユーザーを削除すると、ユーザーのベストスコアも自動削除されます。（スコア履歴は残ります）')
@endcomponent

@include('layouts.include.alertmessage')
@include('layouts.include.pagination')

<p>一括変更で、クラスを変更したら、トライアルクラスのユーザーは存在しなくなります。</p>
<table class="table tableuser03" id="mainTableTwo">
    <thead>
        <tr>
            <th scope="col"><input type="checkbox" id="checkAll" value="1">削除</th>
            <th scope="col">学校int_ID</th>
            <th scope="col">学校ID</th>
            <th scope="col">権限(名前ID）<br>名前</th>
            <th scope="col">学年</th>
            <th scope="col">組</th>
            <th scope="col">出席No</th>
            <th scope="col">ﾄﾗｲｱﾙﾌﾗｸﾞ</th>
            <th scope="col">ログインID<br>updated<br>created</th>
            <th scope="col">更新・登録</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td><input type="checkbox" form="user-checks" class="form-check-input" name="checks[]" value="{{$user->id}}" id="{{$user->name}}"></td>
            <td class="align-baseline"><span name="edit">{{$user->school_int_id}}</span></td>{{--checkDelBtn.jsでnameが「edit]が必要なため--}}
            <td class="align-baseline">{{$user->school_id}}</td>
            <td class="align-baseline">{{$user->role}}<br>（{{$user->id}}）{{$user->name}}</td>
            <td class="align-baseline">{{$user->grade_name}}</td>
            <td class="align-baseline">{{$user->kumi_name}}</td>
            <td class="align-baseline">{{$user->attendance_no}}</td>
            <td class="align-baseline">ﾌﾗｸﾞ：{{$user->trial_flg}}</td>
            <td class="align-baseline">{{$user->login_account}}</td>
            <td class="align-baseline">更新：{{ date_format($user->updated_at, 'Y/m/d') }}<br>登録：{{ date_format($user->created_at, 'Y/m/d') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
