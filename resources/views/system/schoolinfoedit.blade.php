@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/system/for_delButtonOne.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection

@section('content')
@include('layouts.include.flashmessage')

@if($AddOrUpdate == 'update')
<h3 class="titlesBorders">更新モード</h3>
@elseif($AddOrUpdate == 'add')
<h3 class="titlesBorders">新規登録モード</h3>
@endif
<p>このページで、ブラウザの<span class="red">「更新ボタン」は使わない</span>でください。</p>
{{--更新と新規で処理を分ける　$AddOrUpdateはフラグ--}}
@if($AddOrUpdate === 'update')
{{ Form::open(['url' => '/school-info-update', 'method' => 'post']) }}
@else
{{ Form::open(['url' => '/school-add', 'method' => 'post']) }}
@endif
@if ($school_info->invalid == '1')
<h3 class="titlesBorders">この学校は無効です</h3>
@endif
<table class="table schoolinfonew" id="mainTable">
    <thead>
        <tr>
            <th>項目</th>
            <th>内容</th>
            <th>注意事項</th>
        </tr>
    </thead>
    <tr>
        <th scope="row">学校管理NO<span class="red">*</span></th>
        <td>{{Form::text('id', $school_info -> id, ['class' => 'form-control', 'placeholder' => '学校管理NO', 'readonly'])}}</td>
        <td>自動採番、変更不可。学校テーブルのID（schools.id）</td>
    </tr>
    <tr>
        <th scope="row">学校ID<span class="red">*</span></th>
        <td>{{Form::text('school_id', $school_info -> school_id, ['class' => 'form-control', 'maxlength'=>'8','placeholder' => '学校ID'])}}</td>
        <td>登録後、<span class="red">【重要】変更不可（ユーザーに学校IDが記録されるため）、大文字小文字認識</span>。8文字で数字とアルファベット含む(schools.school_id)
        <br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">コンテストアカウント（18文字）の前半の8文字になる</span>
        </td>
    </tr>
    <tr>
        <th scope="row">学校名<span class="red">*</span></th>
        <td>{{Form::text('name', $school_info -> name, ['class' => 'form-control', 'maxlength'=>'20','placeholder' => '学校名'])}}</td>
        <td>登録後、<span class="red">変更不可</span>。できれば15文字が表彰状にあう。最大20文字
        <br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">※コンテストでは15文字以内。表彰状に使う。コンテスト名でもOK</span>
        </td>    
    </tr>
    <tr>
        <th scope="row">住所<span class="red">*</span></th>
        <td>{{Form::text('address', $school_info -> address, ['class' => 'form-control', 'maxlength'=>'20','placeholder' => '住所'])}}</td>
        <td>市町村まで(例)東京都世田谷区 （最大20文字）</td>
    </tr>
    <tr>
        <th scope="row">TEL</th>
        <td>{{Form::text('tel', $school_info -> tel, ['class' => 'form-control', 'maxlength'=>'50','placeholder' => 'TEL'])}}</td>
        <td>（例）03-XXXX-XXXX（最大50文字）</td>
    </tr>
    <tr>
        <th scope="row">メール</th>
        <td>{{Form::text('mail', $school_info -> mail, ['class' => 'form-control', 'maxlength'=>'50','placeholder' => 'メール'])}}</td>
        <td>（例）info@knowledge.com（最大50文字）</td>
    </tr>
    <tr>
        <th scope="row">ユーザー最大数<span class="red">*</span></th>
        <td>{{Form::text('user_number', $school_info -> user_number, ['class' => 'form-control', 'maxlength'=>'5', 'placeholder' => 'ユーザー数（最大）'])}}</td>
        <td>半角数字で入力。先生・生徒を含む。最大10,000名※ただし<span class="red">先生1名と学校管理者1名は含まない※特別学校アカウントが存在すれば、それも含まない</span>。特別学校アカウントはconfigで設定
        <br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">コンテストには必要ないが必須カラムなので3と入力</span>
        </td>
    </tr>
    <tr>
        <th scope="row">追加コース最大数<span class="red">*</span></th>
        <td>{{Form::text('course_number', $school_info -> course_number, ['class' => 'form-control','maxlength'=>'4','placeholder' => '追加コース数（最大）'])}}</td>
        <td>半角数字で入力。最大1,000
        <br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">コンテストには必要ないが必須カラムなので3と入力</span>
        </td>
    </tr>
    <tr>
        <th scope="row">追加コースの単語最大数<span class="red">*</span></th>
        <td>{{Form::text('word_number', $school_info -> word_number, ['class' => 'form-control','maxlength'=>'5','placeholder' => '追加単語数（最大）'])}}</td>
        <td>半角数字で入力。最大10,000
        <br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">コンテストには必要ないが必須カラムなので3と入力</span>
        </td>
    </tr>
    <tr>
        <th scope="row">パスワード初期値（学校管理）<span class="red">*</span></th>
        <td>{{Form::text('password_school_init', $school_info -> password_school_init, ['class' => 'form-control','maxlength'=>'20', 'placeholder' => 'パスワード初期値（学校管理）','autocomplete' => 'off'])}}</td>
        <td>10～20文字で入力。<span class="red">大文字、小文字、数字、記号(!@;:#$%&)のうち3種類以上</span>
        <br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">※【重要】コンテストはバリデーション無効。コンテストの学校管理で使用。全先生が使うパスワードなので、わかりやすく（例）setagayacontest2023</span>
        </td>
    </tr>
    <tr>
        <th scope="row">パスワード初期値（先生）<span class="red">*</span></th>
        <td>{{Form::text('password_teacher_init', $school_info -> password_teacher_init, ['class' => 'form-control','maxlength'=>'20','placeholder' => 'パスワード初期値（先生）'])}}</td>
        <td>6～20文字で、数字とアルファベット（大文字、小文字どちらも可）が必要です
        <br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">※【重要】コンテストは変更不可、バリデーション無効、コンテストアカウントの後半の8文字となる（例）setagaya</span>
        </td>
    </tr>
    <tr>
        <th scope="row">パスワード初期値（生徒）<span class="red">*</span></th>
        <td>{{Form::text('password_student_init', $school_info -> password_student_init, ['class' => 'form-control','maxlength'=>'6','placeholder' => 'パスワード初期値（生徒）'])}}</td>
        <td>数字4～6文字
        <br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">※コンテストではつかわないので「1111」</span>
        </td>
    </tr>
    <tr>
        <th scope="row">請求書発行</th>
        <td>{{ Form::checkbox('invoice','display',$school_info->invoice,['class'=>'form-check-input']) }}請求書を発行する</td>
        <td>学校管理画面で請求書と受領書の発行ができる</td>
    </tr>
    <tr>
        <th scope="row">生徒パスワードの確認表示</th>
        <td>{{ Form::checkbox('password_display','display',$school_info->password_display,['class'=>'form-check-input']) }}表示する ※コンテストと併用項目</td>
        <td>生徒一覧画面の「生徒のパスワードを表示」ボタンを表示するか？【重要】<span class="red">トライアルはチェックしても非表示</span>
            <br><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">【重要】コンテストで塾、教室（バレッド）はチェック。学校名⇒教室名、クラス⇒なし、出席番号⇒生徒番号に変更している</span>
        </td>
    </tr>
    <tr>
        <th scope="row">先生・生徒ログイン名拡大表示</th>
        <td>{{ Form::checkbox('display_name','displayname',$school_info->display_name,['class'=>'form-check-input']) }}ログイン名の拡大表示をする</td>
        <td>1台のPCを複数使用する際、ログアウト忘れ防止のため、ログイン名を大きく表示する【重要】<span class="red">トライアルはチェックしても非表示</span></td>
    </tr>
    <tr>
        <th scope="row">単語登録で音声ファイル編集表示</th>
        <td>{{ Form::checkbox('word_sound','wordsound',$school_info->word_sound,['class'=>'form-check-input']) }}単語登録で音声ファイル編集を表示</td>
        <td>追加コースで、音声ファイルのアップロード、編集が表示される。【重要】<span class="red">トライアルはコース追加ができない</span></td>
    </tr>
    <tr>
        <th scope="row">無効</th>
        <td>{{Form::checkbox('invalid',$school_info -> invalid , $school_info -> invalid, ['class'=>'form-check-input']) }}無効</td>
        <td>無効では、生徒情報など削除されません。</td>
    </tr>
    <tr>
        <th scope="row">長文コース表示</th>
        <td>{{Form::checkbox('long_course',$school_info -> long_course , $school_info -> long_course, ['class'=>'form-check-input']) }}長文コース表示</td>
        <td>長文コースは有料プランのみ。学校テーブルの「long_course」カラムinteger が1だと有効。</td>
    </tr>
    <tr>
        <th scope="row">タイピングスコアタイプ</th>
        <td>{{Form::checkbox('scoretype',$school_info -> scoretype , $school_info -> scoretype, ['class'=>'form-check-input']) }}旧タイプの算出方法（0～300点）</td>
        <td>古い算出方法（0～300点前後）はチェック。新算出方法（100点満点）はチェックなし（既定値）学校テーブルの「scoretype」カラムinteger が1だと旧タイプ。</td>
    </tr>
    <tr>
        <th scope="row">トライアル</th>{{--編集不可にしてdisabledにしたいが、リクエストがpostされないので--}}
        <td>{{Form::checkbox('trial',$school_info ->trial , $school_info ->trial, ['class'=>'form-check-input']) }}トライアル</td>
        <td>トライアル学校は、クラスで分ける。ID:1 学校ID:tri12345,configrationsで設定<br>
        <div class="manualSpace spaces links">
            @component('components.modalimage')
            @slot('word', 'トライアル学校のアカウントが少なくなったら？')
            @slot('image_name', 'system3.png')
            @endcomponent
        </div>
    </td>
    </tr>
    <tr>
        <th scope="row">ロゴ<span class="red">*</span></th>
        <td>{{Form::text('logo', $school_info -> logo, ['class' => 'form-control','maxlength'=>'1'])}}</td>
        <td><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">0:ナレッジロゴ、1:バレッド 2:iTeen</span><br>コンテストも適用</td>
    </tr>
    <tr>
        <th scope="row">コンテスト<span class="red">*</span></th>
        <td>{{Form::text('contest', $school_info -> contest, ['class' => 'form-control','maxlength'=>'1'])}}</td>
        <td><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">0:コンテストなし、1:基本のテストモード 2:練習モード</span></td>
    </tr>
    <tr>
        <th scope="row">コンテストTOPページメッセージ</th>
        <td>{{Form::textarea('memo1', $school_info -> memo1, ['class' => 'form-control'])}}</td>
        <td><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">無記入でもOK、TOP右上に表示される生徒向けメッセージ</span></td>
    </tr>
    <tr>
        <th scope="row">登録日/更新日</th>
        <td>登録日：{{$school_info -> created_at}}<br>更新日：{{$school_info -> updated_at}}</td>
        <td></td>
    </tr>
</table>

<div class="btnOuter">
    @if($AddOrUpdate === 'update')
    <button name="update" class="btn btn-primary btn-block" id="update_btn" type="submit">更新する</button>
    @else
    <button name="add" class="btn btn-primary btn-block" id="update_btn" type="submit">新規登録する</button>
    @endif
</div>
{{ Form::close() }}
{{--削除はめったに使わないため（誤操作を防ぐ）、更新ボタンと離す、--}}
@if($AddOrUpdate === 'update')
    @component('components.delButtonOne')
    @slot('route', 'schoolDelete')
    @slot('id', 'teacher-checks')
    @slot('name', $school_info['name'])
    @slot('hidden_name', 'id')
    @slot('hidden_value', $school_info['id'])
    @slot('message','※削除した学校情報に紐づく、先生、生徒、追加コース、生徒の成績等がすべて削除されます')
    @endcomponent
@endif
@include('layouts.include.alertmessage')
@endsection