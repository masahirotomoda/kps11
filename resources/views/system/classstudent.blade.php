@extends('layouts.base')
@section('content')
<div>
    <table class="table headtable">
        <tr>
            <td>{{$class->school_name}}</td>
            <td>{{$class->grade_name}}</td>
            <td>{{$class->kumi_name}}</td>
            <td>生徒数:{{ $student_num_byclass }}人</td>
        </tr>
</div>
<div>
    </table>
    <table class="table classinfoupdstudent" id="mainTable">
        <thead>
            <tr>
                <th scope="col">出席番号</th>
                <th scope="col">生徒名</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{{ $user->attendance_no}}</td>
                <td>{{ $user->name }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection