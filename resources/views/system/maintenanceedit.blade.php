@extends('layouts.base')
@section('js')
{{--空欄時のバリデーションで使用--}}
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection

@section('content')

@include('layouts.include.flashmessage')

{{ Form::open(['url' => '/maintenance-new' , 'method' => 'post',]) }}
{{ Form::hidden('id',$information->id) }}
<div class="infoInputCell">
    {{Form::textarea('title', $information->title, ['class' => 'form-control', 'rows' => '10','maxlength' =>'255'])}}
</div>
<div class="menuSpaces">
    <div class="menuSpacesCell">
        <div class="menuSpacesOne">
            <div class="radioOut">
                {{ Form::radio('importance', '重要',$importance_yes, ['class' => 'radio-button__input']) }}
                <label for="1">重要</label>
            </div>
            <div class="radioOut">
                {{ Form::radio('importance', '一般',$importance_no, ['class' => 'radio-button__input']) }}
                <label for="0">一般</label>
            </div>
        </div>
        <div class="menuSpacesOne">
            <div class="radioOut">
                {{ Form::radio('invalid','0',$invalid_no, ['class' => 'radio-button__input']) }}
                <label for="0">公開する</label>
            </div>
            <div class="radioOut">
                {{ Form::radio('invalid','1',$invalid_yes, ['class' => 'radio-button__input']) }}
                <label for="1">非公開</label>
            </div>
        </div>
        <div class="leftSpace">
            <button name ="save" id="update_btn" type="submit" class="btn btn-primary btn-block">登録する</button>
        </div>
    </div>
</div>
{{ Form::close() }}
<div class="manualSpace">
    <p>配信した案内は削除できません。「非公開」にチェックすると非表示となります。<span class="red">最大文字数は255文字</span>です。</p>
</div>

@endsection
