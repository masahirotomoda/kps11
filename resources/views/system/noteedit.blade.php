@extends('layouts.base')

@section('js')
<script src="{{ asset('js/trix.js') }}"></script>
<script src="{{ asset('js/addnew_all.js') }}"></script>
@endsection

@section('head_1')
<link href="{{ asset('/css/trix.css') }}" rel="stylesheet">
@endsection

@section('content')

@include('layouts.include.flashmessage')

{{ Form::open(['url' => '/note-update' , 'method' => 'post',]) }}
{{ Form::hidden('id',$note->id) }}

<div class="bottomSpace">
    <div class="plusText ptfull">
        <span class="inputText">カテゴリ<span class="red">*</span>（修正依頼、バグ、重要バグ、バグ完了、記録（残しておく情報）、未実装、実装済み、アカウント情報、メニュー（各メニュー項目））</span>
        {{Form::text('category', $note->category, ['class' => 'form-control','placeholder' => 'カテゴリー'])}}
    </div>
</div>
<div class="bottomSpace">
    <div class="trixeditorOuter">
        <input id="x" type="hidden" value = "{{ $note->memo1 }}" name="memo1">
        <trix-editor input="x"></trix-editor>
    </div>
</div>
<div class="bottomSpace">
    <div class="plusText ptfull">
        <span class="inputText">進捗<span class="red">*</span>（未、完了、対応中）</span>
        {{Form::text('memo2', $note->memo2, ['class' => 'form-control','placeholder' => '進捗'])}}
    </div>
    <div class="leftSpace">
        <div class="btnOuter">
        @if($note->id === null)
            <input type="submit" name="update" value="登録する" class="btn btn-secondary btn-sm">
        @else
            <input type="submit" name="update" value="更新する" class="btn btn-secondary btn-sm">
        @endif

        @component('components.delButtonOne')
            @slot('route', 'systemNoteUpdate')
            @slot('id', 'Delete')
            @slot('name', $note['category'])
            @slot('hidden_name', 'id')
            @slot('hidden_value', $note['id'])
            @slot('message','ノートを削除します')
        @endcomponent
        </div>
    </div>
</div>
{{ Form::close() }}
@endsection
