@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/password_btn_null.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_password_update.js') }}"></script>
@endsection

@section('content')

@include('layouts.include.flashmessage')
<h3 class="titles">ログイン情報</h3>
<table class="table" id="twolineLeftth02">
    <tr>
        <th scope="row">【無料】タイピング</th>
        <td>https://n-typing.com</td>
    </tr>
    <tr>
        <th scope="row">【学校版】生徒・先生ログイン</th>
        <td>https://n-typing.com/login</td>
    </tr>
    <tr>
        <th scope="row">【学校版】先生、生徒の強制ログアウト<br>同じURLにリダイレクト</th>
        <td>https://n-typing.com/logout</td>
    </tr>
    <tr>
        <th scope="row">【学校版】学校管理の強制ログアウト<br>同じURLにリダイレクト</th>
        <td>https://n-typing.com/school-logout</td>
    </tr>
    <tr>
        <th scope="row">【システム管理】管理ログイン</th>
        <td>https://n-typing.com/system-login</td>
    </tr>
</table>
<h3 class="titles">システム管理者情報</h3>
<table class="table" id="twolineLeftth">
    <tr>
        <th scope="row">学校ID</th>
        <td>{{ $user->school_id }}</td>
    </tr>
    <tr>
        <th scope="row">システム管理者所属学校</th>
        <td>{{ $school->name }}</td>
    </tr>
    <tr>
        {{ Form::open(['url' => '/profile-update', 'method' => 'post']) }}
        <th scope="row">システム管理者名</th>
        <td><input type="text" name="name" maxlength="20" value="{{ $user->name }}">
            <button type="submit" class="btn btn-primary btn-block">更新</button>
        </td>
        {{ Form::close() }}
    </tr>
    <tr>
        <th scope="row">ログインID</th>
        <td> {{ $user->login_account }}</td>
    </tr>
    <tr>
        <th scope="row">パスワード<br><small>（セキュリティのため非表示）</small></th>
        <td><small>パスワードは10～20文字。アルファベット（大文字・小文字）、数字、記号（!#$%& ）の内、3種類以上必要</small><br>
            {{ Form::open(['url' => '/password-change', 'method' => 'post']) }}
            @component('components.changeOnePaswd')
                @slot('passwordbtn', 'パスワードを変更する')
                @slot('title', 'パスワードを変更する')
                @slot('passwordcomment', '10～20文字で入力。アルファベット（大文字・小文字）、数字、記号（!#$%& ）のうち、3種類以上必要です')
                @slot('maxlength', '20')
            @endcomponent
            {{ Form::close() }}
        </td>
    </tr>
</table>
@include('layouts.include.alertmessage')
@endsection