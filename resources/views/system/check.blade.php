@extends('layouts.base')

@section('content')


{{ Form::open(['url' => '/error','method' => 'post']) }}

<div class="menuSpaces">
<button name="error401" type="submit" class="btn btn-outline-primary btn-sm" >401Unauthorized</button>
<button name="error403" type="submit" class="btn btn-outline-primary btn-sm" >403Forbidden</button>
<button name="error404" type="submit" class="btn btn-outline-primary btn-sm" >404Not Found</button>
<button name="error419" type="submit" class="btn btn-outline-primary btn-sm" >419セッションタイムアウト</button>
<button name="error429" type="submit" class="btn btn-outline-primary btn-sm" >429Too Many Requests</button>
<button name="error500" type="submit" class="btn btn-outline-primary btn-sm" >500Internal Server Error</button>
<button name="error503" type="submit" class="btn btn-outline-primary btn-sm" >503Service Unavailable</button>
{{ Form::close() }}
</div>
@endsection
