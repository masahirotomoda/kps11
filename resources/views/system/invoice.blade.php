@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/checkall-dim_invoice.js') }}"></script>
<script src="{{ asset('/js/validation_modal.js') }}"></script>
<script src="{{ asset('/js/btn_excelexport.js') }}"></script>
<script src="{{ asset('/js/btn_pdfprint_invoice.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>

@endsection

@section('content')
@include('layouts.include.flashmessage')

{{Form::open(['url' => '/invoice', 'method' => 'post', 'id'=>'invoiceForm'])}}
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{Form::text('school_name',$school_name,['class' => 'form-control', 'id' => 'school_name', 'placeholder' => '学校名の一部']) }}
        {{Form::text('school_id', $school_id,['class' => 'form-control', 'id' => 'school_id', 'placeholder' => '学校ID（例：sakura55）']) }}
        {{Form::select('show_invoice',$show_invoice_selection,$show_invoice, ['class'=>'form-select']) }}
        {{Form::select('show_receipt',$show_receipt_selection,$show_receipt, ['class'=>'form-select']) }}
        {{Form::select('juryo_day',$juryo_selection,$juryo_day, ['class'=>'form-select']) }}
    </div>
</div>
<div class="menuSpacesCell spaceAll">
        <div class="plusText ptfull">
            <span class="inputText">支払い期限【開始日】</span>
            {{ Form::date('start_date',$start_date,['id' => 'start_date','class' => 'form-control']) }}
        </div>
        <div class="plusText ptfull">
            <span class="inputText">支払い期限【終了日】</span>
            {{ Form::date('end_date', $end_date, ['class' => 'form-control']) }}
        </div>
        <div class="plusText btnArea">
            @include('layouts.include.searchbtn')
        </div>
</div>
@include('layouts.include.alertmessage')
{{ Form::close() }}

<div class="menuSpaces underFix">
    <div class="btn btn-secondary btn-sm"><a href ="/invoice-add/add">請求書の新規登録</a></div>
    <div class="menuSpacesCell">
        <button name="pdf1" id="pdf1" type="submit" class="btn btn-outline-primary btn-sm pdfBtn" form="pdf">請求書PDF</button>
        <button name="pdf2" id="pdf2" type="submit" class="btn btn-outline-primary btn-sm pdfBtn" form="pdf">受領書PDF</button>
        <button name="copy" id="copy" type="submit" class="btn btn-outline-primary btn-sm pdfBtn" form="pdf">請求書コピー</button>
    </div>
</div>
@include('layouts.include.pagination')
<div class="menuSpaces topMinusBox">
    {{ Form::submit('データの一括ダウンロード(Excel)', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm rightBox','form'=>'invoiceForm']) }}
</div>
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th><input type="checkbox" id="checkAll1" value="1">請求書</th>
            <th><input type="checkbox" id="checkAll2" value="1">領収書</th>
            <th>コピー<be>1つのみ</th>
            <th scope="col">ID</th>
            <th scope="col">請求書発行</th>
            <th scope="col">受領書発行</th>
            <th scope="col">学校名</th>            
            <th scope="col">サービス期間</th>
            <th scope="col">サービス</th>
            <th scope="col">請求日</th>
            <th scope="col">支払い期限</th>
            <th scope="col">入金日</th>
            <th scope="col">編集</th>
        </tr>
    </thead>
    {{Form::open(['url' => '/invoice-pdf', 'method' => 'post','id'=>'pdf'])}}
    <tbody>
        @foreach ($invoices as $invoice)
        <tr>
            <td><input type="checkbox" class="checks" name="checks1[]" value="{{$invoice->id}}" id="{{ 'pdfInvoice'.$invoice->id }}"></td>
            <td><input type="checkbox" class="checks" name="checks2[]" value="{{$invoice->id}}" id="{{ 'pdfReceipt'.$invoice->id }}"></td>
            <td><input type="checkbox" class="checks" name="checks3[]" value="{{$invoice->id}}" id="{{ 'pdfCopy'.$invoice->id }}"></td>
            <td>{{$invoice->id}}</td>
            @if($invoice->show_invoice==1)
            <td>〇</td>
            @else
            <td>×</td>
            @endif
            @if($invoice->show_receipt==1)
            <td>〇</td>
            @else
            <td>×</td>
            @endif
            <td>{{$invoice->name}}</td>
            <td>{{$invoice->service_period}}</td>
            <td>{{$invoice->service}}</td>
            <td>{{$invoice->seikyu_day}}</td>
            @if($invoice->kigen_day < now())
            <td><span class="red">{{$invoice->kigen_day}}</span></td>
            @else
            <td>{{$invoice->kigen_day}}</td>
            @endif
            <td>{{$invoice->juryo_day}}<br>【{{$invoice->price}}円】</td>
            <td><a href ="/invoice-edit/{{$invoice->id}}">編集</a>
            </td>
        </tr>
        @endforeach
    </tbody>
    {{Form::close()}}    
</table>
@endsection


