@extends('layouts.base')
@section('js')
    <script src="{{ asset('/js/checkDelBtn.js') }}"></script>
    <script src="{{ asset('/js/checkall-dim.js') }}"></script>
    <script src="{{ asset('/js/validation_modal.js') }}"></script>
    <script src="{{ asset('/js/btn_excelexport.js') }}"></script>
    <script src="{{ asset('/js/btn_all_reset.js') }}"></script>
    <script src="{{ asset('/js/del_modal.js') }}"></script>

@endsection
@section('content')

{{ Form::open(['url' => '/battle-search' , 'method' => 'post']) }}
@include('layouts.include.alertmessage')
<div class="menuSpaces underFix">
    @include('layouts.include.calendar')
    <div class="menuSpacesCell">
        {{ Form::text('school_name', $school_name, ['type' => 'text' , 'class' => 'form-control','maxlength' => 40 ,'id' => 'school_name','placeholder' => '学校名の一部']) }}
    </div>
    <div class="menuSpacesCell">
        {{ Form::text('course_name', $course_name, ['type' => 'text' , 'class' => 'form-control','maxlength' => 40 ,'placeholder' => 'コースの一部']) }}
    </div>
    <div class="menuSpacesOneCheck">
        {{ Form::checkbox('teacher','先生',$teacher,['class'=>'form-check-input']) }}
        <label class="form-check-label" for="teacher">先生</label>
    </div>
    <div class="menuSpacesOneCheck">
        {{ Form::checkbox('student','生徒',$student,['class'=>'form-check-input']) }}
        <label class="form-check-label" for="student">生徒</label>
    </div>
    <div class="menuSpacesOneCheck">
        {{ Form::checkbox('sort','降順',$sort,['class'=>'form-check-input']) }}
        <label class="form-check-label" for="sort">降順</label>
    </div>
    <div class="menuSpacesCell">
        {{ Form::text('nick_name', $nick_name, ['type' => 'text' , 'class' => 'form-control','maxlength' => 40 ,'placeholder' => 'ニックネームの一部']) }}
    </div>
    <div class="menuSpacesCell">
        {{ Form::text('name', $name, ['type' => 'text' , 'class' => 'form-control', 'maxlength' => 20,'id' => 'name','placeholder' => '生徒名の一部']) }}
        @include('layouts.include.searchbtn')
        {{Form::submit('エクセル出力', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm'])}}
    </div>
    {{ Form::close() }}
    <div class="menuSpacesCell">
    @component('components.delButton')
    @slot('route', 'systemBattleDelete')
    @slot('id', 'battle-checks')
    @slot('name', 'ランキングスタジアムスコア履歴')
    @slot('message','')
    @endcomponent
</div>
</div>

@include('layouts.include.flashmessage')
@include('layouts.include.pagination')
<table class="table typinghistory02" id="mainTable">
    <thead>
        <tr>
            <th>削除<input class="form-check-input" type="checkbox" id="checkAll" value="1"> </th>
            <th scope="col">日時(降順)</th>
            <th scope="col">コース名<br>(コースID)</th>
            <th scope="col">ニックネーム<br>（ID）ユーザ名</th>
            <th scope="col">学校<br>(管理No/学校ID）</th>
            <th scope="col">ロール</th>
            <th scope="col">ｽｺｱ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($BattleScoreHistory as $sh)
        <tr>
            <td><input type="checkbox" form="battle-checks" class="form-check-input" name="checks[]" value="{{ $sh->id }}" id="【{{$sh->name}}】【{{$sh->course_name}}】【{{$sh->battle_score}}点】"></td>
            {{--削除チェックボックスをチェックして、削除ボタンがアクティブになるJS（checkDelBtn.js)では、編集ボタンの数だけループ。ここでは、編集ボタンがないため、name=editをつけるため、強引に「output」要素を使っている。--}}
            <td><output name ="edit">{{date_format($sh->created_at,'Y/m/d　　H:i')}} </output></td>
            <td>({{ $sh->course_id }})<br>{{ $sh->course_name }}</td>
            <td><span class='red'>{{ $sh->nickname }}</span><br>{{ $sh->name }}({{$sh->user_id}})</td>
            <td>{{ $sh->school_name }}<br>({{$sh->school_int_id}}){{$sh->school_id}}</td>
            
            
            @if($sh->role==="生徒")
            <td>{{$sh->role}}</td>
            @else
            <td><span class="red">{{$sh->role}}</span></td>
            @endif
            <td>{{ $sh->battle_score }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
