@extends('layouts.base')

@section('js')
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/system/addnew-grade2.js"></script>
<script src="/js/system/checkedit-grade2.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>

@endsection

@section('content')
@include('layouts.include.flashmessage')
<h3 class="names">【{{$school->school_id}}】{{$school->name}}({{$school->id}})</h3>

<div class="menuSpaces">
    <button name="add" id="add" type="button" class="btn btn-secondary btn-sm" >新しく学年を登録</button>
    {{--学校IDを持ちまわるため、通常のdelbuttonは使えないためdelButtonHidden--}}
    @component('components.delButtonHidden')
    @slot('route', 'systemGrade2Delete')
    @slot('id', 'grade-checks')
    @slot('name', '学年')
    @slot('hidden_name', 'hidden_school_id')
    @slot('hidden_value', $school->id)
    @slot('message','※削除できるのは、クラスで未使用の学年です。すでに使われている学年ならエラーメッセージがでます')
    @endcomponent
    {{ Form::open(['url' => '/grade2', 'files' => true , 'method' => 'post']) }}
    {{ Form::hidden('hidden_school_id', $school->id,)}}
    <button class="btn btn-outline-primary" id="all_btn" type="submit">すべて表示</button>
    {{ Form::close() }}
</div>
@include('layouts.include.alertmessage')
{{--ページネーションがないため手動で件数表示--}}
@if ($current_grade_num === 0)
    <p>データがありません</p>
@else
    <p>全{{ $grade_num }}件中 {{ $current_grade_num }}件を表示</p>
@endif
<table class="table tablecoursesys" id="mainTable">
    <thead>
        <tr>
            <th scope="col">削除<input type="checkbox" id="checkAll" value="1" class="form-check-input"></th>
            <th scope="col">学年ID(生徒数)<br><span class="red">先生用に「なし」を作る</span></th>
            <th scope="col">学年<small>※最大10文字<br>（例）5年、なし、1002<br>青色は生徒がいない組</small></th>
            <th scope="col">登録・更新</th>
            <th scope="col">編集・保存</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($grades as $gr)
        {{ Form::open(['url' => '/grade2-save', 'files' => true , 'method' => 'post']) }}
        {{ Form::hidden('hidden_school_id', $school->id,['form' => 'insert_btn','id' => 'school_id'])}}
        {{ Form::hidden('hidden_school_id', $school->id,)}}
        {{ Form::hidden('id',$gr->grade_id) }}
        <tr> {{--★idに削除するデータの名前を持たせる⇒del_modal.jsで削除確認モーダルで名前を表示させるため--}}
            <td><input type="checkbox" form="grade-checks" class="form-check-input" name="checks[]" value="{{ $gr->grade_id }}" id="【{{$gr->school_name}}】{{ $gr->grade_name }}"></td>
            <td>{{ $gr->grade_id }}
                @php
                    $grade_count = null;
                    foreach ($users as $user) {
                        if($user->grade_id === $gr->grade_id){
                            $grade_count = $grade_count+1;
                        }
                    }
                @endphp
                @if($grade_count !== null)
                    (生徒数：{{$grade_count}}）
                @endif
            </td>
            <td>
                @if($grade_count === null)
                {{ Form::text('grade_name',$gr->grade_name,['class' => 'form-control col-xs-2','maxlength' => '10','readonly','style'=>'color:blue;']) }}
                @else
                {{ Form::text('grade_name',$gr->grade_name,['class' => 'form-control col-xs-2','maxlength' => '10','readonly',]) }}
                @endif
            </td>
            <td>更新{{date_format($gr->updated_at,'Y/m/d')}}<br>登録{{date_format($gr->created_at,'Y/m/d')}}</td>
            <td nowrap>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>
            </td>
        </tr>
        {{ Form::close() }}
        @endforeach
    </tbody>
</table>
{{--★学校ID取得、addnew-grade2.Jsで使用--}}
<div hidden>
    {{ Form::text($school->id,$school->id,['id' => 'get_school_int_id']) }}
</div>
@endsection