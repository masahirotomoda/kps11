@extends('layouts.base')

@section('content')
@include('layouts.include.pagination')
<div>
    @foreach ($trials as $trial)
    <div class="infoBlockCellnoImage">
        <time class="infoTime"> {{ date_format($trial->created_at, 'Y年m月d日') }}</time>
        <div class="infoBlockCellTitle">
            【{{ $trial->category }}】 /{{ $trial->grade }}/{{ $trial->kumi }}/{{ $trial->teachers_name }}/{{ $trial->tel }}/{{ $trial->mail }}
        </div>
    </div>
    @endforeach
</div>
@endsection


