@extends('layouts.base')

@section('js')
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/system/addnew-user2.js"></script>
<script src="/js/system/checkedit-user2.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
//js/btn_password_reset.js は単数のパスワードリセットなので使えない
<script src="/js/system/btn_excelexport.js"></script>
@endsection

@section('content')

@include('layouts.include.flashmessage')
<h3 class="names">【{{$school->school_id}}】{{$school->name}}({{$school->id}})</h3>
{{--(1)★検索フォーム開始　2つのボタンで切り分け(A)検索（B）エクセル出力--}}
{{ Form::open(['url' => '/user2-search','method' => 'post']) }}
{{ Form::hidden('select_school_id',$school->id) }}

<div class="menuSpaces">
    @if($school->contest>0)
    <h3 class="names"><span style="background: linear-gradient(transparent 70%, #ffff7f 0%);">コンテストでは学校管理者の登録のみ。クラス空欄でOK.バリデーションなし。新規と削除のみ機能。編集は不可</span></h3>
    @endif
    <div class="menuSpacesCell">
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('school_adm','学校管理者',$school_adm,['class'=>'form-check-input','id' => 'school']) }}
            <label class="form-check-label" for="school_adm">学校管理者</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('teacher','先生',$teacher,['class'=>'form-check-input','id' => 'teacher']) }}
            <label class="form-check-label" for="teacher">先生</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('student','生徒',$student,['class'=>'form-check-input','id' => 'student']) }}
            <label class="form-check-label" for="student">生徒</label>
        </div>
        {{ Form::select('s_grade_id',$grade_selection,$s_grade_id,['class' => 'form-select','placeholder' => '学年']) }}<br>
        {{ Form::select('s_kumi_id',$kumi_selection,$s_kumi_id,['class' => 'form-select','placeholder' => '組']) }}
        {{ Form::text('s_attendance_no', $s_attendance_no, ['class' => 'form-control', 'placeholder' => '出席番号','id' => 's_attendance_no']) }}
        {{ Form::submit('特別学校アカウント発行', ['name'=>'school_account','class'=>'btn btn-secondary btn-sm','form'=>'addschoolaccount']) }}
    </div>
    <div class="menuSpacesCell">
        {{ Form::text('s_user_name', $s_user_name, ['id'=>'s_user_name','class' => 'form-control', 'placeholder' => 'ユーザー名の一部']) }}
        {{ Form::text('s_login_account', $s_login_account, ['class' => 'form-control', 'placeholder' => 'ログインID','id' => 'school_name','id' => 'login_account']) }}
        {{--inclideのsearchbtnは使わない。2階層目なので、学校IDを引き継ぐから--}}
        <button class="btn btn-outline-primary searchIcon" type="submit" name="search">検索</button>
        <button class="btn btn-outline-primary" id="all_btn" name="all_btn" type="submit">すべて表示</button>
        <button class="btn btn-outline-primary" id="all_reset_btn" name="cancel_btn" type="submit">操作ｷｬﾝｾﾙ</button>
        {{ Form::submit('エクセル出力', ['name'=>'excelout','id'=>'excelbtn','class'=>'btn btn-outline-primary']) }}
    </div>
</div>
<div class="menuSpaces">
{{ Form::close() }}
    <button name="add" id="add" type="button" class="btn btn-secondary btn-sm" >新しくユーザーを登録</button>
{{--特別学校アカウントボタンのためのフォーム、フォーム外にsubmitボタンがあるが、idでつないでいる--}}
{{ Form::open(['url' => '/user2-addschoolaccount','method' => 'post','id' => 'addschoolaccount']) }}
{{ Form::hidden('select_school_id',$school->id) }}
{{ Form::close() }}
{{--学校IDを持ちまわるため、通常のdelbuttonは使えないためdelButtonHidden--}}
    @component('components.delButtonHidden')
    @slot('route', 'systemUser2Delete')
    @slot('id', 'user-checks')
    @slot('name', 'ユーザー')
    @slot('hidden_name', 'hidden_school_id')
    @slot('hidden_value', $school->id)
    @slot('message','ユーザーを削除すると、タイピングスコア履歴、ベストスコア履歴も削除されます')
    @endcomponent
</div>

@include('layouts.include.alertmessage')
@include('layouts.include.pagination')

<table class="table tableuser02" id="mainTable">
    <thead>
        <tr>
            <th>削除<input class="form-check-input" type="checkbox" id="checkAll" value="1"> </th>
            <th scope="col">権限<br>ユーザID<br>名前<span class="red">*</span></th>
            <th scope="col">学年<span class="red">*</span>組<span class="red">*<br>学年組がnull⇒空欄にならない</span></th>
            <th scope="col">出席No<span class="red">*無い⇒0</span><br>tryﾌﾗｸﾞ</th>
            <th scope="col">学校ID<br>編集・保存</th>
            <th scope="col">ログインID<br>登録・更新日</th>
            <th scope="col">パスワード初期値</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)

        {{--★(3)★新規＆更新系フォーム開始（foreach内）ボタン３つで切り分け(A)ユーザー新規作成（B）ユーザー更新（C）パスワードリセット--}}
        {{ Form::open(['url' => '/user2-save','method' => 'post']) }}
        {{ Form::hidden('hidden_school_id',$school->id,['id'=>'hidden_school_id']) }}
        {{ Form::hidden('save_user_id',$user->user_id) }}

        <tr>{{--★idは削除モーダルに表示する名前などの情報--}}
            <td class="align-baseline">
                <input type="checkbox" form="user-checks" class="form-check-input" name="checks[]" value="{{ $user->user_id }}"
                    id="【{{$school->name}}】
                    学年：{{$user->grade_name }}
                    組： {{$user->kumi_name }}
                    【{{$user->role}}】{{$user->user_name }}">
            </td> {{--★ロールは変更できないが、新規作成の時にセレクトボックスから選べるようにdisabled⇒編集ボタン押下でアクティブJs処理--}}
            <td class="align-baseline">
                {{ Form::select('role',$roleSelection,$user->role,['class' => 'form-select','disabled',])}}{{$user->user_id}}<br>
                {{ Form::text('user_name',$user->user_name,['class' => 'form-control col-xs-2','id'=>'s_user_name','maxlength' => 20 , 'required','readonly',]) }}
                {{ Form::hidden('_user_name',$user->user_name)}}
            </td>
            <td class="align-baseline">
                    {{ Form::select('grade_id',$grade_selection,$user->grade_id,['class' => 'form-select','disabled']) }}<br>
                    {{ Form::select('kumi_id',$kumi_selection,$user->kumi_id,['class' => 'form-select','disabled']) }}
            </td>
            <td class="align-baseline">{{ Form::text('attendance_no',$user->attendance_no,['class' => 'form-control','maxlength' => '4','readonly',]) }}
                @if($user->trial_flg === 1)
                    <br><font class="blues">ﾌﾗｸﾞ：{{$user->trial_flg}}</font>
                @endif
            </td>
            <td class="align-baseline">
                {{ $user->school_id}}<br>
                <button name="edit" class="btn btn-outline-primary btn-sm" type="button" >編集</button>
                <button name="save" class="btn btn-secondary btn-sm" type="submit" disabled>保存</button>
                <button  name="cancel" class="btn btn-outline-primary btn-sm" type="button" disabled>ｷｬﾝｾﾙ</button>
            </td>
            {{--★ロールごとのパスワード初期値--}}
            <td class="align-baseline">{{ $user->login_account }}<br>(更){{date_format($user->updated_at,'Y/m/d')}}<br>(新){{date_format($user->created_at,'Y/m/d')}}</td>
                @if ($user->role === '学校管理者')
            <td class="align-baseline">{{ $school->password_school_init }}<br>
                @elseif($user->role === '先生')
            <td class="align-baseline">{{ $school->password_teacher_init }}<br>
                @else
            <td class="align-baseline">{{ $school->password_student_init }}<br>
                @endif
                {{-- ★パスワードリセットモーダルはコンポーネントを通常使うが、学校選択が必要なため、ここに入れる。
                    php内は、ユーザ事にモーダルにIDをつけている --}}
                @php
                $id_attr = 'modal-reset-' . 'posts' . '-' . $user->user_id;
                @endphp
                {{-- ★パスワードリセットモーダル ボタンの「data-target」の値とモーダルのidが同じだと紐づく --}}
                <button name ="password_reset_btn" class="btn btn-secondary btn-sm" type="button"  data-toggle="modal" data-target="#{{ $id_attr }}" id="reset" >
                    リセット
                </button>
                {{-- ★パスワードリセットモーダルウィンドウ --}}
                <div class="modal fade" id="{{ $id_attr }}" tabindex="-2" role="dialog" aria-labelledby="{{ $id_attr }}-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>
                                【{{$user->user_name}}】　
                                初期パスワードにリセットしますか？
                                </h5>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    キャンセル
                                </button>
                                    <button name="modal_password_reset_btn" id="modal_reset_btn" type="submit" class="btn btn-danger">
                                        リセット
                                    </button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::open(['url' => '/user2-edituser','method' => 'post']) }}
                {{ Form::hidden('select_school_id',$school->id) }}
                {{ Form::hidden('user_id',$user->id) }}
                {{ Form::submit('特別編集', ['name'=>'user_edit','class'=>'btn btn-secondary btn-sm']) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{--★学校選択ボタン押下後、選んだ学校の「学年」「組」がセレクトボックスに表示される。これをJsでコピーして、新規作成のカラムを作る--}}
<div hidden>
    {{ Form::select('nd_grade_selection',$grade_selection,null,['class' => 'form-select','id' => 'nd_grade_selection']) }}
    {{ Form::select('nd_kumi_selection',$kumi_selection,null,['class' => 'form-select','id' => 'nd_kumi_selection']) }}
    {{ Form::select('nd_role_selection',$roleSelection,null,['class' => 'form-select','id' => 'nd_role_selection']) }}
    {{ Form::text($school->id,$school->id,['id' => 'get_school_int_id']) }}
</div>
@endsection
