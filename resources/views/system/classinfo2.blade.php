@extends('layouts.base')
@section('js')
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/system/checkedit-class2.js"></script>
<script src="/js/system/addnew-class2.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/btn_all_reset.js"></script>
@endsection
@section('content')
@include('layouts.include.flashmessage')
<h3 class="names">【{{$school->school_id}}】{{$school->name}}({{$school->id}})</h3>
<div class="menuSpaces uppers">
    <div class="menuSpacesCell spaceOnes">
        {{ Form::open(['url' => '/class2-search','method' => 'post']) }}
        {{ Form::hidden('select_school_id',$school->id) }}
        {{ Form::select('s_grade',$grade_selection,$s_grade, ['class' => 'form-control','id'=>'s_grade','placeholder' => '学年']) }}
        {{ Form::select('s_kumi', $kumi_selection,$s_kumi, ['class' => 'form-control','id'=>'s_kumi','placeholder' => '組']) }}
        <button class="btn btn-outline-primary searchIcon" type="submit" name="search">検索</button>{{--1階層目なので、リダイレクトできずsubmitボタン--}}
        <button class="btn btn-outline-primary" id="all_btn" type="submit" name="all_btn">すべて表示</button>
        {{ Form::close() }}
    </div>
</div>
<div class="menuSpaces">
    <div class="menuSpacesCell">
        <button name="add" id="add" type="button" class="btn btn-secondary btn-sm">クラス登録</button>
        {{--学校IDを持ちまわるため、通常のdelbuttonは使えないためdelButtonHidden--}}
        @component('components.delButtonHidden')
        @slot('route', 'classInfo2Delete')
        @slot('id', 'class-checks')
        @slot('name', 'クラス')
        @slot('hidden_name', 'hidden_school_id')
        @slot('hidden_value', $school->id)
        @slot('message','クラスに生徒が所属していると、クラスは削除できません。また、クラスを削除しても、「学年」「組」は削除されません。')
        @endcomponent
    </div>
</div>
@include('layouts.include.alertmessage')
{{--ページネーションがないため手動で件数表示--}}
@if ($current_class_num === 0)
<p>データがありません</p>
@else
<p>全{{ $class_num }}件中 {{ $current_class_num }}件を表示</p>
@endif
<table class="table classinfosearch" id="mainTable">
    <thead>
        <tr>
            <th>削除<input type="checkbox" id="checkAll" value="1"> </th>
            <th scope="col">クラスID<br><small>青字⇒生徒0</small></th>
            <th scope="col">学年(ID)<br><span class="red">先生用「なし」</span></th>
            <th scope="col">組(ID)<br><span class="red">先生用「なし」</span></th>
            <th scope="col">登録・更新</th>
            <th scope="col">編集・保存</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($classes as $class)
        {{ Form::open(['url' => '/class2-add','method' => 'post']) }}
        {{ Form::hidden('hidden_school_id', $school->id)}}
        {{ Form::hidden('id', $class->class_id)}}
        <tr>
            {{--★idに削除するデータの名前を持たせる⇒del_modal.jsで削除確認モーダルで名前を表示させるため、IDに情報を持たせる--}}
            <td><input type="checkbox" class="checks" name="checks[]" form="class-checks" value="{{ $class->class_id }}" id="{{$class->grade_name}}　{{$class->kumi_name}}"></td>
            <td>
                @php
                $class_count = null;
                foreach ($users as $user) {
                if($user->class_id === $class->class_id){
                $class_count = $class_count+1;
                }
                }
                @endphp
                @if($class_count == null)
                <font class="blues">{{ $class->class_id }}</font>
                @else
                {{ $class->class_id }}
                （生徒数：{{$class_count}}）
                @endif
            </td>
            <td>{{ Form::select('grade_id',$grade_selection,$class->grade_id,['class' => 'form-select','id' => 'grade','disabled']) }}({{$class->grade_id}})</td>{{--新規登録時、ここのクローンを作っている--}}
            <td>{{ Form::select('kumi_id',$kumi_selection,$class->kumi_id,['class' => 'form-select','id' => 'kumi','disabled']) }}({{$class->kumi_id}})</td>
            <td>更新{{date_format($class->updated_at,'Y/m/d')}}<br>登録{{date_format($class->created_at,'Y/m/d')}}</td>
            <td nowrap>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled>ｷｬﾝｾﾙ</button>
                {{ Form::close() }}
                {{ Form::open(['url' => '/class-student', 'method' => 'post']) }}
                {{ Form::hidden('class_id', $class->class_id)}}
                <button class="btn btn-secondary btn-sm" name="student" id="student" type="submit">生徒</button>
            </td>
            {{ Form::close() }}
        </tr>
        @endforeach
    </tbody>
</table>{{--★学校ID取得、addnew-class2.Jsで使用--}}
<div hidden>
    {{ Form::text($school->id,$school->id,['id' => 'get_school_int_id']) }}
</div>
@endsection