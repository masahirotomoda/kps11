<table>
    <tr>
        <th>学校ID</th>
        <th>学校名</th>
        <th>名前</th>
        <th>作成日</th>
        <th>更新日</th>
        <th>学校int_id</th>
        <th>権限種別</th>
        <th>ユーザーID</th>
        <th>学年</th>
        <th>組</th>
        <th>出席番号</th>
        <th>ログインID</th>
        <th>パスワード<br><center>先生・初期値</center></th>
        <th>パスワード<br><center>生徒・初期値</center></th>
        <th>パスワード<br><center>学校管理者・初期値</center></th>
    </tr>
    <tbody>
        @foreach ($users as $user)
        <tr>            
            <td>{{ $user->school_id }}</td>
            <td>{{ $user->school_name }}</td>
            <td>{{ $user->user_name }}</td>
            <td>{{ $user->created_at }}</td>
            <td>{{ $user->updated_at }}</td>
            <td>{{ $user->school_int_id }}</td>
            <td>{{ $user->role }}</td>
            <td>{{ $user->user_id }}</td>            
            <td>{{ $user->grade_name }}</td>
            <td>{{ $user->kumi_name }}</td>
            <td>{{ $user->attendance_no }}</td>
            <td>{{ $user->login_account }}</td>
            <td>{{ $user->password_teacher_init }}</td>
            <td>{{ $user->password_student_init }}</td>
            <td>{{ $user->password_school_init }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
