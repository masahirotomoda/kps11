@extends('layouts.base')

@section('js')
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/system/addnew-kumi.js"></script>
<script src="/js/system/checkedit-kumi.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
@endsection

@section('content')

{{--★検証確認後、削除--}}
@if($selected=='1')
学校選択モード(selected)
@endif

@include('layouts.include.flashmessage')
{{ Form::open(['url' => '/kumi-search', 'files' => true , 'method' => 'post']) }}
{{Form::token()}}

<div class="menuSpaces">
    <div class="menuSpacesCell">     
        {{Form::text('school_name', $school_name, ['class' => 'form-control', 'id' => 'school_name', 'placeholder' => '学校名の一部']) }}
        {{Form::text('school_id', $school_id, ['class' => 'form-control', 'id' => 'sc_id', 'placeholder' => '学校ID']) }}

        @include('layouts.include.searchbtn')
        {{--★リセットと同じ,操作が少し複雑なのでつける--}}
        <button class="btn btn-outline-primary" id="all_reset_btn" type="button" type="button" onclick="location.href='./kumi'">操作ｷｬﾝｾﾙ</button>
    </div>
</div>
{{ Form::close() }}
{{ Form::open(['url' => '/kumi-select', 'files' => true , 'method' => 'post']) }}
{{Form::token()}}
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{--★「selected」は学校を選んだかのフラグ--}}
        {{--★学校を選んだ時--}}
        @if($selected === true)
        {{--★id名「school_id」の値をaddnew-grade.js で使うためhidden--}}
        {{ Form::hidden('school_id',$school_name_search,['form' => 'insert_btn','id' => 'school_id']) }}
        {{--★$school_search_selectionがdisabledの理由⇒選んだ学校に絞った学年が表示されるセレクトボックス⇒addnew-grade.jpで使うから。新規ボタン押下で空の1行を追加するとき、選んだ学校の学年がセレクトボックスにあるために、id名が「school_new_selection」の値を取得、ここの値をコピーして作るから--}}
        {{ Form::select('school_name_search',$school_search_selection,$school_name_search,['class' => 'form-select','id' => 'school_new_selection','form' => 'insert_btn','disabled']) }}
        {{ Form::select('kumi_new_select',$kumi_new_selection,$kumi_new_select,['class' => 'form-select','id' => 'grade_new_selection','form' => 'insert_btn',]) }}
        @else
        {{--★学校を選んでいない（検索モードで）時--}}
        {{ Form::select('school_name_search',$school_search_selection,$school_name_search,['class' => 'form-select','id' => 'school_new_selection',]) }}
        @endif

        {{--★学校を選んでいない（検索モードで）時--}}
        @if($selected === false)
        <button name="select" type="submit" class="btn btn-secondary btn-sm">選んだ学校に組をつくる</button>
        @else
        {{--★学校を選んだ時、submit でなくbuttonである理由⇒Jsでボタンのクリックイベントを取得するだけなので。--}}
        <button name="add" id="add" type="button" class="btn btn-secondary btn-sm">組を入力する</button>
        @endif
    </div>
</div>
{{ Form::close() }}
<div class="menuSpaces">  
    @component('components.delButton')
    @slot('route', 'systemKumiDelete')
    @slot('id', 'kumi-checks')
    @slot('name', '組')
    @slot('message','※削除できるのは、クラスで未使用の組です。すでに使われている組ならエラーメッセージがでます')
    @endcomponent    
</div>
@include('layouts.include.alertmessage')
@include('layouts.include.pagination')

<table class="table tablekumi" id="mainTable">
    <thead>
        <tr>
            <th scope="col">削除<input type="checkbox" id="checkAll" value="1"> </th>
            <th scope="col">【学校ID】学校名</th>
            <th scope="col">組ID</th>            
            <th scope="col">組<small>※最大10文字<br>（例）3組、わかくさ、1002</small></th>
            <th scope="col">編集・保存</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($kumi as $km)
        {{ Form::open(['url' => '/kumi-save', 'files' => true , 'method' => 'post']) }}
        {{ Form::hidden('school_id', $km->school_id)}}
        {{ Form::hidden('id', $km->kumi_id)}}
        <tr>   
            {{--★idに削除するデータの名前を持たせる⇒del_modal.jsで削除確認モーダルで名前を表示させるため--}}     
            <td><input type="checkbox" class="form-check-input" name="checks[]" form="kumi-checks" value="{{ $km->kumi_id }}" id="【{{$km->school_name}}】　{{ $km->kumi_name }}"></td>
            <td>【{{ $km->sc_id }}】　{{ $km->school_name }}</td>
            <td>{{ $km->kumi_id }}</td>
            <td>{{ Form::text('kumi_name',$km->kumi_name,['class' => 'form-control col-xs-2','maxLength'=>10,'readonly',]) }}</td>
            <td nowrap>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'update','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>            
            </td>
        </tr>
        {{ Form::close() }}
        @endforeach
    </tbody>
</table>
@endsection