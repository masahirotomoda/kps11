@extends('layouts.base')

@section('content')

@include('layouts.include.flashmessage')
<h3 class="names">【{{$school->school_id}}】{{$school->name}}({{$school->id}})</h3>
<p><span class="text-danger">ログインIDとパスワードをバリデーションにかからず登録したい時</span></p>
{{--【ナレッジ専用】アカウントとパスワードをイレギュラーにする（バリデーションなし）--}}
{{ Form::open(['url' => '/user2-saveuser','method' => 'post']) }}
{{ Form::hidden('select_school_id',$school->id) }}
{{ Form::hidden('user_id',$user->id) }}
<div>
    ユーザーID：
    {{Form::text('user_id', $user->id,['class' => 'form-control','disabled']) }}<br> 
    ロール ※「生徒」,「先生」,「学校管理者」のみ　<span class="red">重要</span><br>
    {{ Form::select('role',$roleSelection,$user->role,['class' => 'form-select'])}}<br>
    ログインID 例：生徒　12345 (数字5桁) 先生t12345(接頭辞t 数字5桁) 特別学校アカウント　例：b33259<span class="red">重複チェックはしないため、あらかじめ重複しないことを確認する</span><br>
    ※重複があると500エラーページに飛ばされる。<br>
    {{Form::text('login_account', $user->login_account,['class' => 'form-control', 'placeholder' => '12345']) }}<br>
    名前　例：ナレッジ太郎　例：ナレッジ小【特別学校学校管理者】<br>
    {{ Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => '例：ナレッジ太郎']) }}<br>
    パスワード(非表示）　例：生徒　1234(数字4桁) 先生　abc123 英数2つ以上6文字～10文字 abc12345!!　<span class="red">変更しない場合は空欄</span><br>
    {{ Form::text('password', null, ['class' => 'form-control', 'placeholder' => '例：1111 ']) }}<br>
    <button class="btn btn-secondary btn-sm" name="useredit_btn" type="submit">更新する</button>   
</div>
{{ Form::close() }}
@endsection
