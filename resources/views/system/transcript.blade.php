@extends('layouts.base')
@section('js')
<script src="{{ asset('/js/dateedit.js') }}"></script>
    <script src="{{ asset('/js/checkall-dim.js') }}"></script>
    <script src="{{ asset('/js/validation_modal.js') }}"></script>
    <script src="{{ asset('/js/btn_excelexport.js') }}"></script>
    <script src="{{ asset('/js/btn_pdfprint.js') }}"></script>
    <script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection
@section('content')
@php
use App\Models\Grade;
use App\Models\Kumi;
@endphp
{{ Form::open(['url' => '/typing-history-search' , 'method' => 'post']) }}
@include('layouts.include.alertmessage')
<div class="menuSpaces smallBox">
    <div class="menuSpacesCell spaceAll">
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('昨日')">昨日</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('今日')">今日</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('AM')">今日の午前</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('PM')">今日の午後</button>
    </div>
    <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
        @slot('word', 'この画面の使い方')
        @slot('image_name','system14.png')
        @endcomponent
    </div>
</div>
<div class="menuSpaces underFix">
    @include('layouts.include.calendar')
    <div class="menuSpacesCell">
        {{ Form::text('school_name', $school_name, ['type' => 'text' , 'class' => 'form-control','maxlength' => 40 ,'id' => 'school_name','placeholder' => '学校名の一部']) }}
    </div>
    <div class="menuSpacesCell">
        {{ Form::text('course_name', $course_name, ['type' => 'text' , 'class' => 'form-control','maxlength' => 40 ,'placeholder' => 'コースの一部']) }}
    </div>
    <div class="menuSpacesOneCheck">
        {{ Form::checkbox('school_adm','学校管理者',$school_adm,['class'=>'form-check-input']) }}
        <label class="form-check-label" for="school_adm">学校管理者</label>
    </div>
    <div class="menuSpacesOneCheck">
        {{ Form::checkbox('teacher','先生',$teacher,['class'=>'form-check-input']) }}
        <label class="form-check-label" for="teacher">先生</label>
    </div>
    <div class="menuSpacesOneCheck">
        {{ Form::checkbox('student','生徒',$student,['class'=>'form-check-input']) }}
        <label class="form-check-label" for="student">生徒</label>
    </div>
    <div class="menuSpacesCell">
        {{ Form::text('grade', $grade_name, ['type' => 'text' , 'class' => 'form-control','maxlength' => 10 ,'placeholder' => '学年の一部']) }}
        {{ Form::text('kumi', $kumi_name, ['type' => 'text' , 'class' => 'form-control','maxlength' => 10 ,'placeholder' => '組の一部']) }}
        {{ Form::text('name', $name, ['type' => 'text' , 'class' => 'form-control', 'maxlength' => 20,'id' => 'name','placeholder' => '生徒名の一部']) }}
        @include('layouts.include.searchbtn')
        <button class="btn btn-outline-primary" id="all_reset_btn" type="button" onclick="location.href='./typing-history'">操作ｷｬﾝｾﾙ</button>
        {{ Form::submit('エクセル出力', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm']) }}
    </div>
</div>
{{ Form::close() }}
{{ Form::open(['url' => '/typing-history-pdf' , 'method' => 'post','id'=>'pdf']) }}
<div class="menuSpaces">
        <div class="menuSpacesCell spaceAll">
        <button name="pdf1" id="pdf1" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">記録証・印刷用PDF（シンプル）</P></button>
        <button name="pdf2" id="pdf2" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">記録証・印刷用PDF（カラフル）</button>
    </div>
</div>
@include('layouts.include.flashmessage')
@include('layouts.include.pagination')
<table class="table typinghistory02" id="mainTable">
    <thead>
        <tr>
            <th>PDF<input class="form-check-input" type="checkbox" id="checkAll" value="1"></th>
            <th scope="col">日時(降順)</th>
            <th scope="col">コース名<br>(コースID)</th>
            <th scope="col">学校<br>(管理No/学校ID）</th>
            <th scope="col">年-組<br>(出席NO)ﾛｰﾙ</th>
            <th scope="col">ユーザ名<br>（ID）</th>
            <th scope="col">ｽｺｱ</th>
            <th scope="col">級</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($scoreHistory as $sh)
        <tr>
            {{--選択ボタンとりあえず--}}
            <td><input type="checkbox" class="checks" name="checks[]" value="{{ $sh->id }}" id="{{ 'pdf'.$sh->id }}">
            <td>{{date_format($sh->created_at,'Y/m/d　　H:i')}} </td>
            <td>{{ $sh->course_name }}<br>({{ $sh->course_id }})</td>
            <td>{{ $sh->school_name }}<br>({{$sh->school_int_id}}){{$sh->school_id}}</td>
            <td>{{ $sh->grade_name }}-{{ $sh->kumi_name }}<br>({{ $sh->attendance_no }})
            @if($sh->role==="生徒")
            {{$sh->role}}
            @else
            <span class="red">{{$sh->role}}</span>
            @endif
            <td>{{ $sh->name }}<br>({{$sh->user_id}})</td>
            <td>{{ $sh->point }}</td>
            <td>{{ $sh->rank }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ Form::close() }}
@endsection
