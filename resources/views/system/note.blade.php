@extends('layouts.base')

@section('js')
<script src="{{ asset('js/trix.js') }}"></script>
<script src="{{ asset('js/addnew_all.js') }}"></script>
@endsection
@section('head_1')
<link href="{{ asset('/css/trix.css') }}" rel="stylesheet">
@endsection

@section('content')
{{ Form::open(['url' => '/note-search', 'method' => 'post']) }}
@include('layouts.include.flashmessage')
@include('layouts.include.alertmessage')
<div class="menuSpaces">
    <div class="menuSpacesCell spaceAll">
        <div class="plusText ptfull">
            <span class="inputText">カテゴリ(修正依頼、バグ、重要バグ、バグ完了、記録（残しておく情報）、メモ、未実装、実装済み、アカウント情報、メニュー（各メニュー項目））</span>
            {{ Form::text('s_category',$category,['class' => 'form-control','placeholder' => 'カテゴリーの一部']) }}
        </div>
    </div>
</div>
<div class="menuSpacesCell spaceAll">
    <div class="plusText ptfull width30">
        <span class="inputText">内容</span>
        {{ Form::text('s_memo1',$memo1,['class' => 'form-control','placeholder' => 'ノートの一部']) }}
    </div>
    <div class="plusText ptfull width30">
        <span class="inputText">進捗（未、完了、対応中）</span>
        {{ Form::text('s_memo2',$memo2,['class' => 'form-control','placeholder' => '進捗']) }}
    </div>
    <div class="plusText btnArea">
        @include('layouts.include.searchbtn')
    </div>
</div>
{{ Form::close() }}
{{ Form::open(['url' => '/note-edit', 'method' => 'post']) }}
<div class="menuSpaces">
    <div class="menuSpacesCell spaceAll">
        <button name ="add" type="submit" class="btn btn-secondary btn-sm">新規登録</button>
    </div>
</div>
{{ Form::close() }}
@include('layouts.include.pagination')
<table class="table note" id="mainTable">
    <thead>
        <tr>
            <th scope="col">ID・投稿日・更新日</th>
            <th scope="col">カテゴリ・編集・進捗</th>
            <th scope="col">内容</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($notes as $note)
        {{ Form::open(['url' => '/note-edit', 'method' => 'post']) }}
        {{ Form::hidden('id',$note->id) }}
        <tr>
            <td>【ID:{{ $note->id }}】<br>{{ date_format(new DateTime($note->created_at),'Y/m/d') }}<br>{{ date_format(new DateTime($note->updated_at),'Y/m/d') }}  </td>

            <td><font class="blues">{{$note->category}}</font>
                <br><button name="edit" class="btn btn-outline-primary btn-sm" type="submit">編集</button>
                <br><font class="oranges">{{ $note->memo2}}</font>
            </td>
            <td>
                <br>{!!$note->memo1!!}
            </td>
        </tr>
        {{ Form::close() }}
        @endforeach
    </tbody>
</table>
@endsection
