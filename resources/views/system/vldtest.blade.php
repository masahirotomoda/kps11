@extends('layouts.base')

@section('js')
@endsection

@section('content')
{{ Form::open(['url' => '/vldtest-search', 'method' => 'post']) }}

@include('layouts.include.flashmessage')
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{ Form::text('schoolname', $school_name, ['class' => 'form-control', 'id' => 'school_name','maxlength' => '30', 'placeholder' => '学校名の一部']) }}
        {{ Form::text('username', $user_name, ['class' => 'form-control', 'id' => 'user_name','maxlength' => '20', 'placeholder' => '生徒名の一部']) }}
        @include('layouts.include.searchbtn')
        <button class="btn btn-outline-primary" id="ｎ" type="button" onclick="location.href='./vldtest'">操作のキャンセル</button>
    </div>
</div>
{{ Form::close() }}
<div class = "pagination_no"><p>全{{$user_count}}人</p></div>
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th scope="col">学校名(id)</th>
            <th scope="col">生徒名</th>
            <th scope="col">表示コース</th>
            <th scope="col">検定設定</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{$user->school_name}}({{$user->school_id}})</td>
            <td>{{--★formタグはtrに設置できない。td内にすること--}}
                {{ Form::open(['url' => '/vldtest-update', 'method' => 'post']) }}
                @csrf
                {{ Form::hidden('userid', $user->id)}}
                {{$user->name}}
            </td>            
            <td>
                @if($user->vldtest != null)
                    @foreach($test_list as $id => $name)
                        @if($id==$user->vldtest)
                            {{$name}}
                        @endif
                    @endforeach
                @endif
            </td>
            <td>
            @php
                $id_attr = 'modal-reset-' .'posts'. '-' . $user->id;
            @endphp
                {{-- 検定コースドロップダウン --}}
                <button class="btn btn-secondary btn-sm" type="button"  data-toggle="modal" data-target="#{{ $id_attr }}" id="reset">
                検定コース設定
                </button>
                {{-- モーダルウィンドウ --}}
                <div class="modal fade" id="{{ $id_attr }}" tabindex="-2" role="dialog" aria-labelledby="{{ $id_attr }}-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>
                                    検定コースをえらぶ
                                </h5>
                            </div>
                            <div style="display: flex; justify-content: center;">
                                    <select name="selected_test" id="subject">
                                        <option value=null>検定終了</option>                                        
                                        @foreach($test_list as $id => $name)
                                            <option value="{{ $id }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div>
                                <div class="modalCell" >
                                    <p><strong>検定が終わったら「検定終了」を選んで下さい。</strong></p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        キャンセル
                                    </button>                                    
                                    <button name="test_select_btn" type="submit" class="btn btn-danger" id="modal_reset_btn">
                                        更新する
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
