@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/checkDelBtn.js') }}"></script>
<script src="{{ asset('/js/checkall-dim.js') }}"></script>
<script src="{{ asset('/js/system/addnew-grade.js') }}"></script>
<script src="{{ asset('/js/system/checkedit-grade.js') }}"></script>
<script src="{{ asset('/js/del_modal.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection

@section('content')
@include('layouts.include.flashmessage')

{{--★検証後、削除@if($selected=='1')学校選択モード@endif--}}
{{ Form::open(['url' => '/grade-search','method' => 'post']) }}
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{Form::text('school_name', $school_name, ['class' => 'form-control', 'id' => 'school_name','maxlength' => '20','placeholder' => '学校名の一部']) }}
        {{Form::text('school_id', $school_id, ['class' => 'form-control', 'id' => 'sc_id','maxlength' => '8','placeholder' => '学校ID']) }}
        @include('layouts.include.searchbtn')
        {{--★リセットと同じ,操作が少し複雑なのでつける--}}
        <button class="btn btn-outline-primary" id="all_reset_btn" type="button" onclick="location.href='./grade'">操作のｷｬﾝｾﾙ</button>
    </div>
</div>
{{ Form::close() }}
{{ Form::open(['url' => '/grade-select', 'method' => 'post']) }}
{{--★「selected」は学校を選んだかのフラグ --}}
{{ Form::hidden('selected',$selected) }}
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{--★学校を選んだ時--}}
        @if($selected === true)
        {{--★id名「school_id」の値をaddnew-grade.js で使うためhidden--}}
        {{ Form::hidden('school_id',$school_name_search,['form' => 'insert_btn','id' => 'school_id']) }}
        {{--★$school_search_selectionがdisabledの理由⇒選んだ学校に絞った学年が表示されるセレクトボックス⇒addnew-grade.jpで使うから。新規ボタン押下で空の1行を追加するとき、選んだ学校の学年がセレクトボックスにあるために、id名が「school_new_selection」の値を取得、ここの値をコピーして作るから--}}
        {{ Form::select('school_name_search',$school_search_selection,$school_name_search,['class' => 'form-select','id' => 'school_new_selection','form' => 'insert_btn','disabled']) }}
        {{ Form::select('grade_new_select',$grade_new_selection,$grade_new_select,['class' => 'form-select','id' => 'grade_new_selection','form' => 'insert_btn',]) }}
        @else
        {{--★学校を選んでいない（検索モードで）時--}}
        {{ Form::select('school_name_search',$school_search_selection,$school_name_search,['class' => 'form-select','id' => 'school_new_selection',]) }}
        @endif

        {{--★学校を選んでいない（検索モードで）時--}}
        @if($selected === false)
        <button name="select" type="submit" class="btn btn-secondary btn-sm">選んだ学校に学年をつくる</button>
        @else
        {{--★学校を選んだ時、submit でなくbuttonである理由⇒Jsでボタンのクリックイベントを取得するだけなので。--}}
        <button name="add" id="add" type="button" class="btn btn-secondary btn-sm">学年を入力する</button>
        @endif
        <div class="manualSpace links links-sm rightBox">
            @component('components.modalimage')
            @slot('word', '注意事項')
            @slot('image_name','system4.png')
            @endcomponent
        </div>
    </div>

</div>
{{ Form::close() }}
<div class="menuSpaces">
    @component('components.delButton')
    @slot('route', 'systemGradeDelete')
    @slot('id', 'grade-checks')
    @slot('name', '学年')
    @slot('message','※削除できるのは、クラスで未使用の学年です。すでに使われている学年ならエラーメッセージがでます')
    @endcomponent
</div>
@include('layouts.include.alertmessage')
@include('layouts.include.pagination')
<table class="table tablecoursesys" id="mainTable">
    <thead>
        <tr>
            <th scope="col">削除<input type="checkbox" id="checkAll" value="1" class="form-check-input"></th>
            <th scope="col">【学校ID】学校名</th>
            <th scope="col">学年ID</th>
            <th scope="col">学年<small>※最大10文字<br>年も入力、数字のみOK<br><span class="red">「なし」必須</span></small></th>
            <th scope="col">編集・保存</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($grades as $gr)
        {{ Form::open(['url' => '/grade-save', 'files' => true , 'method' => 'post']) }}
        {{ Form::hidden('school_id',$gr->school_id) }}
        {{ Form::hidden('id',$gr->grade_id) }}
        <tr> {{--★idに削除するデータの名前を持たせる⇒del_modal.jsで削除確認モーダルで名前を表示させるため--}}
            <td><input type="checkbox" form="grade-checks" class="form-check-input" name="checks[]" value="{{ $gr->grade_id }}" id="【{{$gr->school_name}}】{{ $gr->grade_name }}"></td>
            <td>【{{ $gr->sc_id }}】　{{ $gr->school_name }}</td>
            <td>{{ $gr->grade_id }}</td>
            <td>{{ Form::text('grade_name',$gr->grade_name,['class' => 'form-control col-xs-2','maxlength' => '10','readonly',]) }}</td>
            <td nowrap>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>            
            </td>
        </tr>
        {{ Form::close() }}
        @endforeach
    </tbody>
</table>
@endsection