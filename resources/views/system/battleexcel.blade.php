<table>
    <tr>
        <th>日時</th>
        <th>コースID</th>
        <th>コース名</th>
        <th>学校管理No</th>
        <th>学校ID</th>
        <th>学校名</th>
        <th>ロール</th>
        <th>ユーザID</th>
        <th>ユーザ名</th>
        <th>ニックネーム</th>
        <th>スコアスコア</th>
    </tr>
    <tbody>
        @foreach ($BattleScoreHistory as $sh)
        <tr>
            <td>{{ $sh->created_at }}</td>
            <td>{{ $sh->course_id }}</td>
            <td>{{ $sh->course_name }}</td>
            <td>{{ $sh->school_intid }}</td>
            <td>{{ $sh->school_id }}</td>
            <td>{{ $sh->school_name }}</td>
            <td>{{ $sh->role }}</td>
            <td>{{ $sh->user_id }}</td>
            <td>{{ $sh->name }}</td>            
            <td>{{ $sh->nickname }}</td>
            <td>{{ $sh->battle_score }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
