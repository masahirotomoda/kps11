@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
<script src="{{ asset('/js/btn_excelexport.js') }}"></script>
@endsection

@section('content')
{{ Form::open(['url' => '/login-history-search', 'files' => true , 'method' => 'post']) }}
<div class="menuSpaces underFix">
    <div class="menuSpacesCell spaceAll">
        <div class="plusText ptfull">
            <span class="inputText">開始日</span>
            {{ Form::date('start_date',$startDateTime,['id' => 'start_date','class' => 'form-control']) }}
        </div>
        <div class="plusText ptfull">
            <span class="inputText">終了日</span>
            {{ Form::date('end_date', $endDateTime, ['class' => 'form-control', 'id' => 'user_name', 'placeholder' => '終了日付']) }}
        </div>
    </div>
    <div class="menuSpacesCell spaceAll">
        {{ Form::text('school_name', $school_name, ['class' => 'form-control', 'id' => 'school_name', 'placeholder' => '学校名の一部']) }}
        {{ Form::text('user_id', $user_id, ['class' => 'form-control', 'id' => 'user_id', 'placeholder' => 'ユーザーID']) }}
        {{ Form::text('user_name', $user_name, ['class' => 'form-control', 'id' => 'user_name', 'placeholder' => 'ユーザー名の一部']) }}
        {{ Form::text('ipaddress', $ipaddress, ['class' => 'form-control', 'id' => 'ipaddress', 'placeholder' => 'IPアドレス']) }}
    </div>
    <div class="menuSpacesCell spaceAll">
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('chkSystem',$chkSystem, $chkSystem , ['class'=>'form-check-input','id'=>'chkSystem']) }}
            <label class="form-check-label" for="chkSystem">システム管理者</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('chkSchool',$chkSchool, $chkSchool , ['class'=>'form-check-input','id'=>'chkSchool']) }}
            <label class="form-check-label" for="chkSchool">学校管理者</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('chkTeacher',$chkTeacher, $chkTeacher , ['class'=>'form-check-input','id'=>'chkTeacher']) }}
            <label class="form-check-label" for="chkTeacher">先生</label>
        </div>
        @include('layouts.include.searchbtn')
        <button type="submit" name="excel" class="btn btn-outline-primary btn-sm">エクセル出力</button>
    </div>
</div>
@include('layouts.include.pagination')
{{ Form::close() }}
<table class="table loginhistory" id="mainTable">
    <thead>
        <tr>
            <th scope="col">日時</th>
            <th scope="col">学校.ID<br><small>学校削除でも残る</small></th>
            <th scope="col">ログインID<br>ロール</th>
            <th scope="col">名前・ID<br><small>ユーザー削除でも残る</small></th>
            <th scope="col">どの管理画面か</th>
            <th scope="col">IPアドレス</th>
            <th scope="col">OS</th>
            <th scope="col">ブラウザ</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            @foreach ($login_logs as $log)
            <td>{{ $log->created_at }}</td>
            <td>{{ $log->login_schoolname }}<br>({{$log->login_school_int_id}}){{$log->login_school_id}}</td>
            <td>{{ $log->login_id }}<br>{{$log->role}}</td>
            <td>{{ $log->login_username }}<br>({{$log->login_user_id}})</td>
            <td>{{ $log->login_from }}</td>
            <td>{{ $log->ipaddress }}</td>
            <td>{{ $log->os }}</td>
            <td>{{ $log->browser }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
