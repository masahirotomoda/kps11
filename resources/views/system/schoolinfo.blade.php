@extends('layouts.base')

@section('js')
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
<script src="/js/system/btn_excelexport.js"></script>
@endsection

@section('content')

@include('layouts.include.flashmessage')
{{ Form::open(['url' => '/school-info-search', 'method' => 'post']) }}
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{Form::text('school_name', $school_name, ['class' => 'form-control', 'id' => 'school_name', 'placeholder' => '学校名の一部']) }}
        {{Form::text('school_id', $school_id, ['class' => 'form-control', 'id' => 'school_id', 'placeholder' => '学校ID']) }}
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('valid',true, $valid, ['class'=>'form-check-input','id'=>'valid']) }}
            <label class="form-check-label" for="valid">有効</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('invalid',true, $invalid, ['class'=>'form-check-input','id'=>'invalid']) }}
            <label class="form-check-label" for="invalid">無効</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('contest',true, $contest, ['class'=>'form-check-input','id'=>'contest']) }}
            <label class="form-check-label" for="invalid">ｺﾝﾃｽﾄ</label>
        </div>
        @include('layouts.include.searchbtn')
        {{ Form::submit('Ecxel', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-outline-primary']) }}
    </div>
</div>
{{ Form::close() }}
{{--新規作成をsubmitボタンにする理由⇒本来新規なのでリクエストデータは必要ないため、buttom属性でOKだが、HTTPリクエストをGET不可でPOSTのみにするため--}}
{{ Form::open(['url' => '/school-info-edit', 'method' => 'post']) }}
<div class="menuSpaces">
    <button name='add' type="submit" class="btn btn-secondary btn-sm" onclick="location.href='./school-info-edit'">学校を登録する</button>
    <div class="infotext">
        <p>有効な学校数:{{ $cnt }} 校</p>
    </div>
</div>
{{ Form::close() }}
@include('layouts.include.pagination')
@include('layouts.include.alertmessage')
<table class="table schoolinfo" id="mainTable">
    <thead>
        <tr>
            <th scope="col">【学校ID】<br>学校管理NO</th>
            <th scope="col">名前</th>
            <th scope="col">住所</th>
            <th scope="col">無効<br>trial</th>
            <th scope="col">
                @component('components.tooltip')
                    @slot('word', 'ﾕｰｻﾞ数')
                    @slot('message', 'ﾕｰｻﾞ数に、学校管理者1名、先生1名は含まない。特別学校ｱｶｳﾝﾄがあればそれも含まない。※ﾕｰｻﾞ数の-2か-3になる。')
                @endcomponent
                <br>（Max）<br>特別ｱｶｳﾝﾄ</th>
            <th scope="col">【ｺｰｽ数】<br>（Max）</th>
            <th scope="col">【単語数】<br>（Max）</th>
            <th scope="col">詳細</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($schools as $school)
        @if ($school->invalid == 1)
        <tr class="bg-secondary text-white invalidity">
        @elseif($school->contest > 0)
        <tr style="background: #afeeee;">
        @else
        <tr>
        @endif
            <td>{{ $school->school_id }}<br>（No:{{ $school->id }}）</td>
            <td>{{ $school->name }}</td>
            <td>{{ $school->address }}</td>
            <td>{{Form::checkbox('invalid', $school->invalid, $school->invalid, ['class'=>'','disabled'])}}

                @if($school->trial == '1')
                <br>trial
                @endif
            </td>
            <td>
                @php
                $is_specil_school_account=false;//特別学校アカウントが存在するか？
                $specil_school_account=null;//特別学校アカウント(b33259)の値
                //選択行の学校のユーザー（学校管理者のみ）の中で、特別学校アカウントがあれば、特別学校アカウントが存在ありにする
                foreach($users as $user){
                    if ($user->school_int_id==$school->id){
                        if($user->login_account == config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN')){
                            $is_specil_school_account = true;//学校特別アカウントあり
                            $specil_school_account = config('configrations.SCHOOL_ACCOUNT_FOR_ADMIN');//学校特別アカウント
                        }
                    }
                }
                //学校アカウントが存在したら、-3(学校管理者＆先生＆特別学校アカウント)
                if($is_specil_school_account === true){
                    $user_total_count =  $school->user_count - 3;
                //学校アカウントがなければ、-2(学校管理者＆先生)
                } else {
                    $user_total_count =  $school->user_count - 2;
                }
                @endphp
                @if(  $user_total_count > $school->user_number){{--現在のユーザ数が最大ユーザ数を超えたら赤色--}}
                <font class="oranges">{{ $user_total_count}}</font>
                @else{{--0以下だと不自然なので、登録なしにする--}}
                    @if( $user_total_count < 0)
                        <font class="blues">登録なし</font>
                    @else
                        <font class="blues">{{ $user_total_count}}</font>
                    @endif
                @endif
                <br>（Max:{{ $school->user_number }}）
                <br>{{$specil_school_account}}
            </td>
            <td>{{--現在のコース数が最大コース数を超えたら赤色--}}
                @if( $school->course_count > $school->course_number)
                <font class="oranges">{{ $school->course_count}}</font>
                @else
                <font class="blues">{{ $school->course_count}}</font>
                @endif
                <br>（Max:{{ $school->course_number }})
            </td>
            <td>{{--現在の単語数--}}
                @php
                    $word_count = null;
                    foreach ($courseWords as $courseword) {
                        if($courseword->course_school_id === $school->id){
                            $word_count = $word_count+1;
                        }
                    }
                @endphp
                {{--現在の単語数が最大単語数を超えたら赤色--}}
                @if($word_count > $school->word_number)
                <font class="oranges">{{$word_count}}</font>
                @else
                <font class="blues">{{$word_count}}</font>
                @endif
                <br>（Max:{{ $school->word_number }}）
            </td>
            <td>
            {{ Form::open(['url' => '/school-info-edit', 'method' => 'post']) }}
                {{ Form::hidden('id', $school->id)}}
                {{ Form::button('編集', ['type' => 'submit','name' => 'update', 'class' => 'btn btn-secondary btn-sm']) }}
            {{ Form::close() }}

            {{ Form::open(['name' => 'schoolinfo', 'url' => '/class-search', 'method' => 'post']) }}
                {{ Form::hidden('school_name', $school->name)}}
                {{ Form::button('クラス', ['type' => 'submit','name' => 'cls', 'class' => 'btn btn-secondary btn-sm']) }}
            {{ Form::close() }}

            {{ Form::open(['name' => 'schoolinfo', 'url' => '/user-search', 'method' => 'post']) }}
                {{--hiddenでsearchをリクエストに持たせる理由⇒システム管理のユーザ一管理の検索（searchメソッド）にわたすが、searchメソッドはボタン3つをボタン名で分岐させて
                    処理している、そのため検索ボタン（nameがsearch）をもたせることにより、searchメソッドが発動する、その際に、検索ボックスに、「学校名」と「生徒」（valueは生徒）「先生」（valueは先生）のパラメータをもたせる--}}
                {{ Form::hidden('search', null)}}
                {{ Form::hidden('school_name', $school->name)}}
                {{ Form::hidden('student', '生徒')}}
                {{ Form::hidden('teacher', '先生')}}
                {{ Form::hidden('school_adm', '学校管理者')}}
                {{ Form::button('ユーザ', ['type' => 'submit','name' => 'user', 'class' => 'btn btn-secondary btn-sm']) }}
            {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection