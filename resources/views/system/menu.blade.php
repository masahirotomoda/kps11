@extends('layouts.base')
@section('content')
<div class="infoTitle">
    <span>お知らせ</span>
</div>
@include('layouts.include.information')
<div class="btnOuter bottomSpace">
    <a class="btn-link" href="./menu-allinfo">すべてのお知らせ</a>
</div>
<div class="infoTitle">
    <span>新着トライアル</span>
</div>
@foreach ($trial as $tr)
<div class="infoBlockCellnoImage">
    <time class="infoTime"> {{ date_format($tr->created_at, 'Y年m月d日') }}</time>
    <div class="infoBlockCellTitle">
        {{ $tr->name }} {{ $tr->grade }} {{ $tr->kumi }} {{ $tr->teachers_name }}
    </div>
</div>
@endforeach
<div class="btnOuter bottomSpace">
    <a class="btn-link" href="./menu-alltrial">すべてのトライアル</a>
</div>
<div class="mainBlock" id="menu-top">
    <!--mainBlock start-->
    <div class="twoBlockCellTitle">トライアル</div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./trialinfo'">トライアル</button>
        </div>
        <div class="twoBlockRight">
            トライアル申込情報
        </div>
    </div>
    <div class="twoBlockCellTitle">システム管理</div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./profile'">システム管理</button>
        </div>
        <div class="twoBlockRight">
            URL情報、パスワード変更など
        </div>
    </div>
    <div class="twoBlockCellTitle">学校</div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./school-info'">学校</button>
        </div>
        <div class="twoBlockRight">
            学校の新規登録・編集・削除、学校管理者のパスワードリセット
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./grade'">学年(1)</button>
        </div>
        <div class="twoBlockRight">
            学校ごとに学年を設定
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./grade2-school-select'">学年(2)学校別</button>
        </div>
        <div class="twoBlockRight">
            学校ごとに学年を設定
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./kumi'">組(1)</button>
        </div>
        <div class="twoBlockRight">
            学校ごとに組を設定
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./kumi2-school-select'">組(2)学校別</button>
        </div>
        <div class="twoBlockRight">
            学校ごとに組を設定
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./class'">クラス(1)</button>
        </div>
        <div class="twoBlockRight">
            学校ごとに、学年と組を組み合わせた「クラス」を登録
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./class2-school-select'">クラス(2)学校別</button>
        </div>
        <div class="twoBlockRight">
    【学校別】学年と組を組み合わせた「クラス」を登録
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./user'">ユーザー(1)</button>
        </div>
        <div class="twoBlockRight">
            学校管理者、先生、生徒の新規登録・編集・削除、パスワードリセット、エクセル出力、エクセル一括登録
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./user2-school-select'">ユーザー(2)学校別</button>
        </div>
        <div class="twoBlockRight">
            【学校別】学校管理者、先生、生徒の新規登録・編集・削除、パスワードリセット、エクセル出力（×エクセル一括登録）
        </div>
    </div>
    <div class="twoBlockCellTitle">タイピングコース</div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./course'">コース管理</button>
        </div>
        <div class="twoBlockRight">
            組込・追加・テストコースの登録・編集・削除
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./typing-history'">タイピング履歴<small>エクセル出力</small><small>記録証印刷</small></button>
        </div>
        <div class="twoBlockRight">
            タイピング履歴のエクセル出力・記録証印刷
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./bestscore'">ベストスコア<small>記録証印刷</small></button>
        </div>
        <div class="twoBlockRight">
        ベストスコアのエクセル出力・記録証印刷
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./battle'">ランキングスタジアム スコア履歴</button>
        </div>
        <div class="twoBlockRight">
            ランキングスタジアムのスコア履歴
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./vldtest'">バレッド専用ICT検定管理</button>
        </div>
        <div class="twoBlockRight">
            バレッド専用ICTタイピング検定問題の表示非表示
        </div>
    </div>
    <div class="twoBlockCellTitle">コンテスト</div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./contest-studentlist'">コンテスト生徒一覧</button>
        </div>
        <div class="twoBlockRight">
            コンテスト参加校の生徒一覧、追加、編集、削除
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./contest-scorelist'">コンテスト・スコア一覧</button>
        </div>
        <div class="twoBlockRight">
            コンテスト参加生徒のスコア一覧
        </div>
    </div>
    <div class="twoBlockCellTitle">情報発信</div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./maintenance-info'">メンテナンス情報管理</button>
        </div>
        <div class="twoBlockRight">
            サーバーメンテナンスやアップデート情報
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./bloglist'">ブログ管理</button>
        </div>
        <div class="twoBlockRight">
            TOPページの新着情報
        </div>
    </div>
    <div class="twoBlockCellTitle">その他</div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./login-history'">管理画面ログイン履歴</button>
        </div>
        <div class="twoBlockRight">
            システム管理画面・学校管理画面・先生のログインの履歴　※生徒のログイン履歴はなし
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./setting'">基本設定</button>
        </div>
        <div class="twoBlockRight">
            運営会社情報、ページネーション、トライアル学校NOなど
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./note'">システム開発Note</button>
        </div>
        <div class="twoBlockRight">
            開発メモ、進捗チェックなど
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./file'">ファイル整理</button>
        </div>
        <div class="twoBlockRight">
            音声ファイル、画像ファイルの整理
        </div>
    </div>
    {{--リンク先なし--}}
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./check'">検証</button>
        </div>
        <div class="twoBlockRight">
            エラーページのチェック
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./fileupload'">画像アップロード</button>
        </div>
        <div class="twoBlockRight">
            地図コースの画像アップロード
        </div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./invoice'">請求書・受領書</button>
        </div>
        <div class="twoBlockRight">
            請求書と受領書の発行
        </div>
    </div>
</div>
@endsection
