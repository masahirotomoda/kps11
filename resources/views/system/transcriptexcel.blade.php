<table>
    <tr>
        <th>日時</th>
        <th>コースID</th>
        <th>コース名</th>
        <th>学校管理No</th>
        <th>学校ID</th>
        <th>学校名</th>
        <th>学年</th>
        <th>組</th>
        <th>出席番号</th>
        <th>ロール</th>
        <th>ユーザID</th>
        <th>ユーザ名</th>
        <th>最高点</th>
        <th>級(ｷｰﾀｯﾁ)</th>
    </tr>
    <tbody>
        @foreach ($scoreHistory as $sh)
        <tr>
            <td>{{ $sh->sh_created_at }}</td>
            <td>{{ $sh->course_id }}</td>
            <td>{{ $sh->course_name }}</td>
            <td>{{ $sh->school_int_id }}</td>
            <td>{{ $sh->school_id }}</td>
            <td>{{ $sh->school_name }}</td>
            <td>{{ $sh->grade_name }}</td>
            <td>{{ $sh->kumi_name }}</td>
            <td>{{ $sh->attendance_no }}</td>
            <td>{{ $sh->role }}</td>
            <td>{{ $sh->user_id }}</td>
            <td>{{ $sh->name }}</td>
            <td>{{ $sh->point }}</td>
            <td>{{ $sh->rank }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
