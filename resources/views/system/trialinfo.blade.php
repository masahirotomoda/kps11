@extends('layouts.base')

@section('js')
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
@endsection

@section('content')
@include('layouts.include.flashmessage')

{{ Form::open(['url' => '/trialinfo-search', 'method' => 'post']) }}

<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{Form::text('s_trial_name',$s_trial_name,['class' => 'form-control', 'id' => 's_trial_name', 'placeholder' => '【申込時】学校名の一部']) }}
        {{Form::text('s_teachers_name', $s_teachers_name,['class' => 'form-control', 'id' => 's_teachers_name', 'placeholder' => '【申込時】先生名の一部']) }}
        {{Form::select('valid_date',$valid_selection,$valid_select, ['class'=>'form-select']) }}
    </div>
</div>
<div class="menuSpacesCell spaceAll">
        <div class="plusText ptfull">
            <span class="inputText">トライアル終了日【開始日】</span>
            {{ Form::date('start_date',$startDateTime,['id' => 'start_date','class' => 'form-control']) }}
        </div>
        <div class="plusText ptfull">
            <span class="inputText">トライアル終了日【終了日】</span>
            {{ Form::date('end_date', $endDateTime, ['class' => 'form-control']) }}
        </div>
        <div class="plusText btnArea">
            @include('layouts.include.searchbtn')
        </div>
</div>
<div class="menuSpaces">
    <div class="menuSpacesCell">
    {{ Form::submit('申込↑', ['name'=>'order_mousikomi_asc','class'=>'btn btn-outline-primary']) }}
    {{ Form::submit('申込↓', ['name'=>'order_mousikomi_desc','class'=>'btn btn-outline-primary']) }}
    {{ Form::submit('終了日↑', ['name'=>'order_endday_asc','class'=>'btn btn-outline-primary']) }}
    {{ Form::submit('クラス↑', ['name'=>'order_class_asc','class'=>'btn btn-outline-primary']) }}
    {{ Form::submit('クラス↓', ['name'=>'order_class_desc','class'=>'btn btn-outline-primary']) }}
</div>
{{ Form::close() }}
<div class="manualSpace">
    <p>【トライアルクラス削除】①このページの学校編集ボタン押下⇒学校編集画面の削除ボタン押下（学年、組、クラスを自動削除）②このページのユーザ編集ボタン押下⇒削除ボタンでユーザー（先生＆生徒）削除⇒自動でベストポイント削除（スコア履歴は残る）<br>
【有料学校へ昇格】①新しい学校を作る②学年を作る③組を作る④クラスを作る⑤このページのユーザー編集ボタン押下⇒既存ユーザを新しい学校＆クラスにつけかえ</p>
</div>

@include('layouts.include.pagination')
<table class="table schoolinfo" id="mainTable">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">申込日<br>終了日</th>
            <th scope="col">組名(クラスID）<br>先生名</th>
            <th scope="col">人数<br>先生＋生徒</th>
            <th scope="col">分類<br>業種</th>
            <th scope="col">申込み学校名<br>住所</th>
            <th scope="col">申込み学年<br>申込み組</th>
            <th scope="col">編集</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($trials as $trial)
        <tr>
            <td>{{ $trial->id }}</td>
            <td>{{ date_format($trial->created_at, 'Y/m/d') }}<br>
                @if($trial->end_of_trial < now())
                <span class="red">{{ $trial->end_of_trial }}</span></td>
                @else
                {{ $trial->end_of_trial }}</td>
                @endif
            <td>{{ $trial->kumi_name }}({{ $trial->class_id }})<br>{{ $trial->teachers_name }}</td>
            <td>
                @php
                    $class_count = null;
                    foreach ($users as $user) {
                        if($user->class_id === $trial->class_id){
                            $class_count = $class_count+1;
                        }
                    }
                @endphp
                {{$class_count}}
            </td>

            <td>{{ $trial->category }}<br>{{ $trial->industory }}</td>
            <td>{{ $trial->name }}<br>{{ $trial->address }}</td>
            <td>{{ $trial->grade }}<br>{{ $trial->kumi }}</td>
            <td>
                {{ Form::open(['url' => '/trialinfo-edit', 'method' => 'post']) }}
                {{ Form::hidden('edit_trial_id', $trial->id)}}
                {{ Form::submit('学校編集', ['name' => 'edit', 'class' => 'btn btn-secondary btn-sm']) }}
                {{ Form::close() }}

                {{ Form::open(['url' => '/trialinfo-edituser', 'method' => 'post']) }}
                {{ Form::hidden('hidden_school_id', $trial->school_id)}}
                {{ Form::hidden('hidden_class_id', $trial->class_id)}}
                {{ Form::hidden('hidden_kumi', $trial->kumi_name)}}
                {{ Form::hidden('hidden_grade', $trial->grade_name)}}
                {{ Form::submit('ﾕｰｻﾞ編集', ['name' => 'edit_user', 'class' => 'btn btn-secondary btn-sm']) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection


