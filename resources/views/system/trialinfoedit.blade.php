@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/system/for_delButtonOne.js') }}"></script>
@endsection

@section('content')
@include('layouts.include.flashmessage')

{{ Form::open(['url' => '/trialinfo-update', 'method' => 'post']) }}
{{ Form::hidden('id', $trial->id)}}
<div class="manualSpace noSpaceDanger">
    <p>トライアルクラスを削除⇒⇒<span class="text-danger">自動で組（トライアル12組など）とクラスIDが削除</span>されます。<span class="text-danger">その前に、かならずユーザーを削除</span>、または、ユーザーを生かす場合は、新しい学校情報を付与すること！</p>
</div>

    <table class="table schoolinfonew" id="mainTable">
        <thead>
            <tr>
                <th>項目</th>
                <th>内容</th>
                <th>注意事項</th>
            </tr>
        </thead>
        <tr>
            <th scope="row">トライアルID</th>
            <td>{{$trial -> id}}</td>
            <td>自動採番、変更不可。

            </td>
        </tr>
        <tr>
            <th scope="row">申込日<br>トライアル終了日<span class="red">*</span></th>
            <td>{{$trial -> created_at}}
                <br>{{Form::text('end_of_trial', $trial -> end_of_trial, ['class' => 'form-control', 'required','placeholder' => 'トライアル終了日'])}}</td>
            <td>トライアルは小中高学校、専門学校は3か月、その他教室は1か月 <span class="red">※終了日は変更可</span>トライアルフラグはuserのカラムに設置。トライアルフラグがあり、有効期限が過ぎているとログインできない。トライアルから有料版に昇格するときは、①トライアルは削除して、新規として学校アカウントを発行(トライアルでのユーザーのタイピング履歴は引き継がない②ユーザーを生かす場合、ユーザのトライアルフラグを削除して、学校IDを新しいものにつけかえる。</td>
        </tr>
        <tr>
            <th scope="row">【ID】・（int_id）<br>トライアル学校名</p></th>
            <td>【{{$trial -> sc_id}}】　　（{{$trial -> school_int_id}}）　　{{$trial -> school_name}}</td>
            <td>トライアル学校のID（int_id）は「coufigrations」で設定</td>
        </tr>
        <tr>
            <th scope="row">先生名<span class="red">*</span></th>
            <td>{{Form::text('teachers_name', $trial -> teachers_name, ['class' => 'form-control','maxlength'=>'20', 'placeholder' => '【申込時】先生名'])}}
            <td>最大文字数20字</td>
        </tr>
        <tr>
            <th scope="row">【ID】・組</th>
            <td>【{{$trial -> kumi_id}}】　{{$trial -> kumi_name}}</td>
            <td>トライアルは「組」単位。組は連番で発行。学年は「なし」が自動で設定される</td>
        </tr>
        <tr>
            <th scope="row">クラスID</th>
            <td>{{$trial -> class_id}}</td>
            <td>学年と組の組み合わせ（学年は「なし」）</td>
        </tr>
        <tr>
            <th scope="row">【申込】カテゴリー・業種</th>
            <td>{{Form::text('category', $trial -> category, ['class' => 'form-control','maxlength'=>'20', 'placeholder' => '【申込時】カテゴリー'])}}
                <br>{{Form::text('industry', $trial -> industry, ['class' => 'form-control','maxlength'=>'20', 'placeholder' => '【申込時】業種'])}}
            </td>
            <td>カテゴリーで「なし」の場合のみ、業種入力</td>
        </tr>
        <tr>
            <th scope="row">【申込】学校名<span class="red">*</span></th>
            <td>{{Form::text('name', $trial -> name, ['class' => 'form-control','maxlength'=>'20','required', 'placeholder' => '【申込時】学校名'])}}</td>
            <td>最大文字数20字</td>
        </tr>
        <tr>
            <th scope="row">【申込】住所<span class="red">*</span></th>
            <td>{{Form::text('address', $trial -> address, ['class' => 'form-control','maxlength'=>'50', 'placeholder' => '【申込時】住所'])}}</td>
            <td>最大文字数50字</td>
        </tr>
        <tr>
            <th scope="row">【申込】学年・組</th>
            <td>{{Form::text('grade', $trial -> grade, ['class' => 'form-control','maxlength'=>'20', 'placeholder' => '学年'])}}
                <br>{{Form::text('kumi', $trial -> kumi, ['class' => 'form-control','maxlength'=>'20', 'placeholder' => '組'])}}
            </td>
            <td>最大文字数20字</td>
        </tr>
        <tr>
            <th scope="row">メール<span class="red">*</span></th>
            <td>{{Form::text('mail', $trial -> mail, ['class' => 'form-control','maxlength'=>'50','placeholder' => 'メール'])}}</td>
            <td>最大文字数50字</p></td>
        </tr>
        <tr>
            <th scope="row">TEL<span class="red">*</span></th>
            <td>{{Form::text('tel', $trial -> tel, ['class' => 'form-control','maxlength'=>'50','placeholder' => 'TEL'])}}</td>
            <td>最大文字数50字</td>
        </tr>
        <tr>
            <th scope="row">対応履歴<br>（最大1000字）</th>
            <td colspan="2">{{Form::textarea('memo1', $trial -> memo1, ['class' => 'form-control','maxlength'=>'1000', 'placeholder' => 'メモ1'])}}</td>
        </tr>
        <tr>
            <th scope="row">備考<br>（最大1000字）</th>
            <td colspan="2">{{Form::textarea('memo2', $trial -> memo2, ['class' => 'form-control','maxlength'=>'1000', 'placeholder' => 'メモ2'])}}</td>
        </tr>
    </table>

    <div class="btnOuter">
        {{Form::submit('更新する', ['name'=>'update','class'=>'btn btn-primary btn-block'])}}
        {{ Form::close() }}
        @component('components.delButtonOne')
            @slot('route', 'systemTrialInfoDelete')
            @slot('id', 'trial_delete_id')
            @slot('name', $trial['name'])
            @slot('hidden_name', 'id')
            @slot('hidden_value', $trial['id'])
            @slot('message',$trial['name'].'を削除します。【重要】先にユーザ（先生＆生徒）を削除してください！この削除では「トライアル学校のクラス（trialテーブル）」「組（例）トライアル3組」「クラス」が削除されます')
        @endcomponent
    </div>

@endsection
