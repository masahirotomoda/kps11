@extends('layouts.base')

@section('js')
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/system/addnew-user.js"></script>
<script src="/js/system/checkedit-user.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
<script src="/js/system/btn_excelexport.js"></script>
//js/btn_password_reset.js を使うと、ページロード時に存在しないため（学校選択後表示）エラーをはくので未使用
@endsection

@section('content')

@include('layouts.include.flashmessage')

{{--(1)★検索フォーム開始　3つのボタンで切り分け(A)検索（B）学校選択（C）エクセル出力--}}
{{ Form::open(['url' => '/user-search','method' => 'post']) }}
<div class="menuSpaces">
    <div class="menuSpacesCell">
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('school_adm','学校管理者',$school_adm,['class'=>'form-check-input','id' => 'school']) }}
            <label class="form-check-label" for="school_adm">学校管理者</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('teacher','先生',$teacher,['class'=>'form-check-input','id' => 'teacher']) }}
            <label class="form-check-label" for="teacher">先生</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{ Form::checkbox('student','生徒',$student,['class'=>'form-check-input','id' => 'student']) }}
            <label class="form-check-label" for="student">生徒</label>
        </div>
        {{ Form::text('school_name', $school_name, ['class' => 'form-control', 'id' => 'school_name', 'placeholder' => '学校名の一部']) }}
        {{ Form::text('grade_name', $grade_name, ['class' => 'form-control', 'placeholder' => '学年','id' => 'grade_name']) }}
        {{ Form::text('kumi_name', $kumi_name, ['class' => 'form-control', 'placeholder' => '組','id' => 'kumi_name']) }}
        {{ Form::text('s_attendance_no', $attendance_no, ['class' => 'form-control', 'placeholder' => '出席番号','id' => 's_attendance_no']) }}
    </div>
    <div class="menuSpacesCell">
        {{ Form::text('s_user_name', $user_name, ['id'=>'s_user_name','class' => 'form-control', 'placeholder' => 'ユーザー名の一部']) }}
        {{ Form::text('login_account', $login_account, ['class' => 'form-control', 'placeholder' => 'ログインID','id' => 'school_name','id' => 'login_account']) }}
       @include('layouts.include.searchbtn');
        <button class="btn btn-outline-primary" id="all_reset_btn" type="button" onclick="location.href='./user'">操作ｷｬﾝｾﾙ</button>
        <button class="btn btn-outline-primary btn-sm" id="excelbtn" name="excelout" type="submit">エクセル出力</button>
    </div>
</div>
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{--★学校選択ボタン押下後、ユーザー一覧にある編集ボタンがアクティブになるフラグ　checkedit_user.jsで処理--}}
        {{--★学校選択ボタン押下⇒$school_selected_flgはyes、そうでないとno--}}
        {{form::hidden('school_selected_flg',$school_selected_flg,['id'=>'school_selected_flg'])}}

        {{--★selected フラグ⇒学校選択ボタンか新規追加ボタンか--}}
        {{ Form::hidden('selected',$selected) }}

        {{--★学校を選んだ時--}}
        @if($selected === true)
        {{ Form::select('school_name_search',$school_search_selection,$school_name_search,['class' => 'form-select smallSelect','id' => 'school_new_selection','form' => 'insert_btn']) }}
        <div hidden>
        {{ Form::select('grade_new_select',$grade_new_selection,$grade_new_select,['class' => 'form-select smallSelect','id' => 'grade_new_selection','form' => 'insert_btn']) }}
        {{ Form::select('kumi_new_select',$kumi_new_selection,$kumi_new_select,['class' => 'form-select smallSelect','id' => 'kumi_new_selection','form' => 'insert_btn']) }}
        </div>

        @else
        {{--★学校を選んでいない（検索モードで）時--}}
            {{ Form::select('school_name_search',$school_search_selection,$school_name_search,['class' => 'form-select smallSelect','id' => 'school_new_selection']) }}
        @endif
        {{--★学校を選ぶボタン、ユーザー新規追加ボタン（checkedit-users.jsで活性、非活性処理--}}
        <button name="select" id="select" type="submit" class="btn btn-outline-primary btn-sm" >学校を選ぶ</button>
        <button name="add" id="add" type="button" class="btn btn-secondary btn-sm" disabled >新ユーザ登録</button>
        {{--★現在、検索モードか編集モードかわかるように表示　Jsで文字列書き換えcheckedit-users.js--}}
        <button name="SearchOrEditMode" id="SearchOrEditMode" type="button" class="btn btn-outline-primary btn-sm" >検索モード</button>
        {{ Form::close() }}
    </div>
</div>
<div class="menuSpaces">
        @component('components.delButton')
        @slot('route', 'systemUserDelete')
        @slot('id', 'user-checks')
        @slot('name', 'ユーザー')
        @slot('message','ユーザーを削除すると、ベストスコア履歴も削除されます。※スコア履歴は残ります。')
        @endcomponent

{{--(2)★エクセル一括登録フォーム開始--}}
{{ Form::open(['url' => '/user-import', 'files' => true , 'method' => 'post']) }}
    <div class="menuSpacesCell bgPlus">
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="location.href='/system_user_exceltemplate.xlsx'" download="system_user_exceltemplate.xlsx">①入力用ひな型</button>
        <label class="fileInputBox">
            <input type="file" id="file" name="file" class="inputFile" required>
            <span class="fileInputText">⇒②ファイルを選択</span>
        </label>
        <button class="btn btn-secondary btn-sm" type="submit" name="import">⇒③Excelアップロード</button>
    </div>
{{ Form::close() }}
</div>
@include('layouts.include.alertmessage')
@include('layouts.include.pagination')

<table class="table tableuser" id="mainTable">
    <thead>
        <tr>
            <th>削除<input class="form-check-input" type="checkbox" id="checkAll" value="1"> </th>
            <th scope="col">学校名<br>学校ID</th>
            <th scope="col">権限（変更×）<span class="red">*</span><br>【ﾕｰｻﾞID】<br>名前<span class="red">*</span></th>
            <th scope="col">学年・組<span class="red">*</span><br>クラスID</th>
            <th scope="col">出席No<span class="red">*</span><br>tryﾌﾗｸﾞ</th>
            <th scope="col">編集・保存</th>
            <th scope="col">ログインID</th>
            <th scope="col">パスワード初期値</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)

        {{--★(3)★新規＆更新系フォーム開始（foreach内）ボタン３つで切り分け(A)ユーザー新規作成（B）ユーザー更新（C）パスワードリセット--}}
        {{ Form::open(['url' => '/user-save','method' => 'post']) }}
        {{ Form::hidden('select_school_id',$user->school_id) }}
        {{ Form::hidden('save_user_id',$user->user_id) }}

        <tr>
            {{--★idは削除モーダルに表示する名前などの情報--}}
            <td class="align-baseline">
                <input type="checkbox" form="user-checks" class="form-check-input" name="checks[]" value="{{ $user->user_id }}"
                    id="【{{$user->school_name}}】
                    @if($user->grade_name !==null || $user->grade_name !== 'なし')
                        {{$user->grade_name }}
                    @endif
                    @if($user->kumi_name !==null || $user->kumi_name !== 'なし')
                        {{$user->kumi_name }}
                    @endif
                    　【{{$user->role}}】{{$user->user_name }}">
            </td>
            {{--★学校名はリクエストで値を取得するため、セレクトボックスでdisabled--}}
            <td class="align-baseline">{{ Form::select('school_id',$school_search_selection,$user->school_id,['class' => 'form-select','disabled',]) }}
                <br>{{$user->sc_id}}
            </td>
            {{--★ロールは変更できないが、新規作成の時にセレクトボックスから選べるようにdisabled⇒編集ボタン押下でアクティブJs処理--}}
            <td class="align-baseline">
                {{ Form::select('role',$roleSelection,$user->role,['class' => 'form-select','disabled',])}}{{$user->user_id}}<br>
                {{ Form::text('user_name',$user->user_name,['class' => 'form-control col-xs-2','id'=>'s_user_name','maxlength' => 20 , 'required','readonly',]) }}
                {{ Form::hidden('_user_name',$user->user_name)}}
            </td>
            <td class="align-baseline">
                {{--★ユーザー編集中はセレクトボックスの初期値にgrade_id,kumi_idが必要--}}
                {{--★普通の表示や検索時は、セレクトボックスがdiserbledのため、初期値にgrade_name,kumi_nameが必要--}}

                @if ($school_selected_flg == 'yes')
                    {{ Form::select('grade_id',$grade_selection,$user->grade_id,['class' => 'form-select','disabled']) }}<br>
                    {{ Form::select('kumi_id',$kumi_selection,$user->kumi_id,['class' => 'form-select','disabled']) }}
                @else
                    {{$user->grade_name}}<br>
                    {{$user->kumi_name}}<br>
                @endif
                ｸﾗｽID：{{$user->class_id}}
            </td>
            <td class="align-baseline">{{ Form::text('attendance_no',$user->attendance_no,['class' => 'form-control','maxlength' => '4','readonly',]) }}
                @if($user->trial_flg === 1)
                    <br><font color="blue">ﾌﾗｸﾞ：{{$user->trial_flg}}</font>
                @endif
            </td>
            <td class="align-baseline">
                <button name="edit" class="btn btn-outline-primary btn-sm" type="button" disabled>編集</button>
                <button name="save" class="btn btn-secondary btn-sm" type="submit" disabled>保存</button>
                <button  name="cancel" class="btn btn-outline-primary btn-sm" type="button" disabled>ｷｬﾝｾﾙ</button>

            </td>
            {{--★ロールごとのパスワード初期値--}}
            <td class="align-baseline">{{ $user->login_account }}</td>
                @if ($user->role === '学校管理者')
            <td class="align-baseline">{{ $user->password_school_init }}<br>
                @elseif($user->role === '先生')
            <td class="align-baseline">{{ $user->password_teacher_init }}<br>
                @else
            <td class="align-baseline">{{ $user->password_student_init }}<br>
                @endif

                {{-- ★パスワードリセットモーダルはコンポーネントを通常使うが、学校選択が必要なため、ここに入れる。
                    php内は、ユーザ事にモーダルにIDをつけている --}}
                @php
                $id_attr = 'modal-reset-' . 'posts' . '-' . $user->user_id;
                @endphp

                {{-- ★パスワードリセットモーダル ボタンの「data-target」の値とモーダルのidが同じだと紐づく --}}
                <button name ="password_reset_btn" class="btn btn-secondary btn-sm" type="button"  data-toggle="modal" data-target="#{{ $id_attr }}" id="reset" disabled>
                    リセット
                </button>
                {{-- ★パスワードリセットモーダルウィンドウ --}}
                <div class="modal fade" id="{{ $id_attr }}" tabindex="-2" role="dialog" aria-labelledby="{{ $id_attr }}-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>
                                【{{$user->user_name}}】　
                                初期パスワードにリセットしますか？
                                </h5>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    キャンセル
                                </button>
                                    <button name="modal_password_reset_btn" id="modal_password_reset_btn" type="submit" class="btn btn-danger">
                                        リセット
                                    </button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{--★学校選択ボタン押下後、選んだ学校の「学年」「組」がセレクトボックスに表示される。これをJsでコピーして、新規作成のカラムを作る--}}
<div hidden>
    {{ Form::select('nd_grade_selection',$grade_new_selection,null,['class' => 'form-select','id' => 'nd_grade_selection']) }}
    {{ Form::select('nd_kumi_selection',$kumi_new_selection,null,['class' => 'form-select','id' => 'nd_kumi_selection']) }}
    {{ Form::select('nd_role_selection',$roleSelection,null,['class' => 'form-select','id' => 'nd_role_selection']) }}
</div>
@endsection
