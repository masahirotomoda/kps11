<table>
    <tr>
        <th>学校ID</th>
        <th>学校int_id</th>
        <th>学校名</th>
        <th>住所</th>
        <th>有効or無効</th>
        <th>トライアル</th>
        <th>ユーザ数</th>
        <th>ユーザー最大数</th>
        <th>コース数</th>
        <th>コース最大数</th>
        <th>単語数</th>
        <th>単語最大数</th>
        <th>登録日</th>
        <th>更新日</th>
    </tr>
    <tbody>
        @foreach ($schools as $school)
        <tr>
            <td>{{ $school->school_id }}</td>
            <td>{{ $school->id }}</td>
            <td>{{ $school->name  }}</td>
            <td>{{ $school->address }}</td>
            <td>
                @if($school->invalid =='1') 
                    無効
                @endif
            </td>
            <td>
                @if($school->trial =='1')
                    トライアル
                @endif
            </td>
            <td>{{ $school->user_count }}</td>
            <td>{{ $school->user_number }}</td>
            <td>{{ $school->course_count }}</td>
            <td>{{ $school->course_number }}</td>
            <td>{{ $school->word_count }}</td>
            <td>{{ $school->word_number }}</td>
            <td>{{ $school->created_at}}</td>
            <td>{{ $school->updated_at}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
