@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/checkDelBtn.js') }}"></script>
<script src="{{ asset('/js/checkall-dim.js') }}"></script>
<script src="{{ asset('/js/del_modal.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
<script src="{{ asset('/js/flashmessage_display.js') }}"></script>


//js/btn_password_reset.js を使うと、ページロード時に存在しないため（学校選択後表示）エラーをはくので未使用
@endsection

@section('content')
@include('layouts.include.flashmessage')
<div class="menuSpaces">
    <div class="menuSpacesCell spaceAll">
        {{ Form::open(['url' => '/fileupload', 'method' => 'post']) }}
        {{ Form::text('course_name', $course_name, ['class' => 'form-control','maxlength' => 15,'placeholder' => 'コース名']) }}
        {{ Form::text('word_name', $word_name, ['class' => 'form-control','maxlength' => 15,'placeholder' => '単語名']) }}
        <select name="tab_select">
            <option value="english1">英語1</option>
            <option value="english2">英語2</option>
            <option value="english3">英語3</option>
            <option value="english4">英語4</option>
            <option value="english5">英語5</option>
            <option value="map">地図</option>
            <option value="battle">ランキング・英語</option>
        </select>
    </div>
    @include('layouts.include.searchbtn')
    {{ Form::close() }}
</div>
<div class="menuSpaces">
    @component('components.delButton')
        @slot('route', 'systemFileUploadDelete')
        @slot('id', 'img-checks')
        @slot('name', '画像ファイル')
        @slot('message','地図コースの画像ファイル（png）のみ削除。単語は削除されません。')
    @endcomponent
{{ Form::open(['url' => '/fileupload-up', 'files' => true , 'method' => 'post']) }}
    <div class="menuSpacesCell bgPlus">
        <label for="category">アップロード先:</label>
        <select name="category">
            <option value="upload_english">英語タブ＆ランキングバトル英語イラスト(img/english/100.png)</option>
            <option value="upload_english_top">【表紙】英語＆ランキングバトル英語（img/english/top/100.png）</option>
            <option value="upload_map">地図or地図表紙（img/map/maptop400.png,img/map/200.png）</option>
        </select>

        <label class="fileInputBox">
            <input type="file" id="file" name="file" class="inputFile" required>
            <span class="fileInputText">ファイルを選択</span>
        </label>
        <button class="btn btn-outline-primary" type="submit">アップロード</button>
    </div>
{{ Form::close() }}
    
</div>
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th>削除<input type="checkbox" id="checkAll" value="1"> </th>
            <th scope="col">有効〇</th>
            <th scope="col">学校名</th>
            <th scope="col">コースID</th>
            <th scope="col">コース名</th>
            <th scope="col">タブ名</th>
            <th scope="col">単語ID</th>
            <th scope="col">単語名</th>
            <th scope="col">画像名</th>
            <th scope="col">画像存在</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($courseWords as $courseWord)
        @if($courseWord->invalid == 0)
        <tr>
        @else
        <tr class="bg-secondary text-white invalidity">
        @endif
            {{--★idに削除するデータの名前を持たせる⇒del_modal.jsで削除確認モーダルで名前を表示させるため--}}
            <td>{{--★formタグはtrに設置できない。td内にすること--}}
                {{ Form::open(['url' => '/fileupload-delete', 'method' => 'post']) }}
                {{-- Form::hidden('word_id', $courseWord->word_id--}}{{--★Jsで使うのでhiddenは必要--}}
                
            
                <input type="checkbox" form="img-checks" class="form-check-input" name="checks[]" value="{{$courseWord->word_id}}" id="{{$courseWord->word_mana}}/{{$courseWord->word_id}}.png">

            </td>
            <td>
                @if($courseWord->course_invalid == 0)
                〇
                @else
                ×
                @endif
            </td>
            {{--共通で使っている「/js/checkDelBtn.js」は、削除ボタンと編集ボタン両方ある想定でつくっている。今回
                削除しかない。編集用の「name="edit"」で要素数をカウント（edit.length）、編集ボタンがないため
                「学校名」の要素数をカウントさせるため<span name="edit">で対応--}}
            <td><span name="edit">{{ $courseWord->schoo_name }}</span></td>
            <td>{{ $courseWord->course_id }}</td>
            <td>{{ $courseWord->course_name }}</td>
            <td>{{ $courseWord->tab }}</td>
            <td>{{ $courseWord->word_id }}</td>
            <td>{{ Str::limit($courseWord->word_mana, 10, '...') }}</td>
            <td>
                {{$courseWord->word_id.".png"}}                
            </td>
            <td>
            @if (in_array((string) $courseWord->word_id, $all_imgfile))
                    〇
                @else
                    ×
                @endif
            </td>
            {{ Form::close() }}{{--/form はtdの外に入れる--}}
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
