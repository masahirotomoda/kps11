@extends('layouts.base')

@section('js')
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/system/addnew-kumi2.js"></script>
<script src="/js/system/checkedit-kumi2.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>

@endsection

@section('content')
@include('layouts.include.flashmessage')
<h3 class="names">【{{$school->school_id}}】{{$school->name}}({{$school->id}})</h3>

<div class="menuSpaces">
    <button name="add" id="add" type="button" class="btn btn-secondary btn-sm" >新しく組を登録</button>

    {{--学校IDを持ちまわるため、通常のdelbuttonは使えないためdelButtonHidden--}}
    @component('components.delButtonHidden')
    @slot('route', 'systemKumi2Delete')
    @slot('id', 'kumi-checks')
    @slot('name', '組')
    @slot('hidden_name', 'hidden_school_id')
    @slot('hidden_value', $school->id)
    @slot('message','※削除できるのは、クラスで未使用の組です。すでに使われている組ならエラーメッセージがでます')
    @endcomponent

    {{ Form::open(['url' => '/kumi2','method' => 'post']) }}
    {{ Form::hidden('hidden_school_id', $school->id,)}}
    <button class="btn btn-outline-primary" id="all_btn" type="submit">すべて表示</button>
    {{ Form::close() }}
</div>
@include('layouts.include.alertmessage')
{{--ページネーションがないため手動で件数表示--}}
@if ($current_kumi_num === 0)
    <p>データがありません</p>
@else
    <p>全{{ $kumi_num }}件中 {{ $current_kumi_num }}件を表示</p>
@endif
<table class="table tablecoursesys" id="mainTable">
    <thead>
        <tr>
            <th scope="col">削除<input type="checkbox" id="checkAll" value="1" class="form-check-input"></th>
            <th scope="col">組ID<br><span class="red">先生用に「なし」を作る</span></th>
            <th scope="col">組<small>※最大10文字<br>(例）3組、わかくさ、1002<br>青文字は生徒がいないクラス</small></th>
            <th scope="col">登録・更新</th>
            <th scope="col">編集・保存</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($kumi as $km)
        {{ Form::open(['url' => '/kumi2-save', 'method' => 'post']) }}
        {{ Form::hidden('hidden_school_id', $school->id,['form' => 'insert_btn','id' => 'school_id'])}}
        {{ Form::hidden('hidden_school_id', $school->id,)}}
        {{ Form::hidden('id',$km->kumi_id) }}
        <tr>
            {{--★idに削除するデータの名前を持たせる⇒del_modal.jsで削除確認モーダルで名前を表示させるため--}}
            <td><input type="checkbox" form="kumi-checks" class="form-check-input" name="checks[]" value="{{ $km->kumi_id }}" id="【{{$km->school_name}}】{{ $km->kumi_name }}"></td>
            <td>{{ $km->kumi_id }}
                @php
                    $kumi_count = null;
                    foreach ($users as $user) {
                        if($user->kumi_id === $km->kumi_id){
                            $kumi_count = $kumi_count+1;
                        }
                    }
                @endphp
                @if($kumi_count !== null)
                    (生徒数：{{$kumi_count}}）
                @endif
            </td>
            <td>
                @if($kumi_count === null)
                {{ Form::text('kumi_name',$km->kumi_name,['class' => 'form-control col-xs-2 blues','maxlength' => '10','readonly']) }}
                @else
                {{ Form::text('kumi_name',$km->kumi_name,['class' => 'form-control col-xs-2','maxlength' => '10','readonly',]) }}
                @endif
            </td>
            <td>登録{{date_format($km->created_at,'Y/m/d')}}<br>更新{{date_format($km->updated_at,'Y/m/d')}}</td>
            <td nowrap>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>
            </td>
        </tr>
        {{ Form::close() }}
        @endforeach
    </tbody>
</table>
{{--★学校ID取得、addnew-kumi2.Jsで使用--}}
<div hidden>
    {{ Form::text($school->id,$school->id,['id' => 'get_school_int_id']) }}
</div>
@endsection