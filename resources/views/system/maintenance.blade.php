@extends('layouts.base')

@section('js')
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
@endsection

@section('content')
{{Form::open(['url' => '/maintenance-search', 'method' => 'post'])}}
@include('layouts.include.flashmessage')
@include('layouts.include.alertmessage')
<div class="menuSpaces underFix">
    <div class="menuSpacesCell spaceAll">
        <div class="plusText ptfull">
            <span class="inputText">created_start</span>
            {{Form::date('start_date',$startDate,['id' => 'start_date','class' => 'form-control'])}}
        </div>
        <div class="plusText ptfull">
            <span class="inputText">created_end</span>
            {{Form::date('end_date',$endDate,['id' => 'end_date','class' => 'form-control'])}}
        </div>
    </div>
    <div class="menuSpacesCell spaceAll">
        {{Form::text('article',$article,['id' => 'article','class' => 'form-control','placeholder' => '記事内容部分一致'])}}
        @include('layouts.include.searchbtn')
    </div>
</div>
{{Form::close()}}
{{Form::open(['url' => '/maintenance-add', 'method' => 'post'])}}
<div class="menuSpaces">
    <div class="menuSpacesCell">
        <button name ="add" type="submit" class="btn btn-secondary btn-sm">新規登録</button>
    </div>
    <p><font color="red">【重要】ピンクのIDが「1」のタイトルは、学校版のトップページ上部のニュースタイトル。緑の「2」はフリー版。</font>
</p>
</div>
{{Form::close()}}
@include('layouts.include.pagination')
<table class="table maintenanceinfo" id="mainTable">
    <thead>
        <tr>
            <th>非公開</th>
            <th scope="col">ID</th>
            <th scope="col">投稿日<br>更新日</th>
            <th scope="col">本文</th>
            <th scope="col">重要度</th>
            <th scope="col">編集</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($information as $colum)
        {{Form::open(['url' => '/maintenance-add', 'method' => 'post'])}}
        {{Form::hidden('id',$colum->id)}}
        @if($colum->id ===1)
        <tr style="background-color:#ff69b4;">
        @elseif($colum->id ===2) 
        <tr style="background-color:#3cb371;">
        @elseif($colum->invalid ===1) 
        <tr style="background-color:Gainsboro;">
        @else
        <tr>
        @endif

            <td>{{Form::checkbox('checks', $colum->invalid, $colum->invalid, ['class'=>'custom-control-input','id'=>$colum->id,'disabled'])}}</td>
            <td>{{$colum->id}}</td>
            <td>{{date_format($colum->created_at,'Y/m/d　H:i')}} <br>{{date_format($colum->updated_at,'Y/m/d　H:i')}}</td>
            <td>{{$colum->title}}</td>
            <td>{{$colum->importance}}</td>
            <td><button name="edit" class="btn btn-secondary btn-sm" type="submit">編集</button></td>
        </tr>
        {{Form::close()}}
        @endforeach
    </tbody>
</table>
@endsection
