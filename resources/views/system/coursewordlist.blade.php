@extends('layouts.base')

@section('content')
<div class="menuSpaces">
    <div class="menuSpacesCell spaceAll">
        {{ Form::open(['url' => '/course-wordlist', 'method' => 'post']) }}
        {{ Form::token() }}
        {{ Form::text('course_name', $course_name, ['class' => 'form-control','maxlength' => 15,'placeholder' => 'コース名']) }}
        {{ Form::text('course_id', $course_id, ['class' => 'form-control','maxlength' => 4,'placeholder' => 'コースID']) }}
        {{ Form::select('course_type', $type_selection, $course_type, ['class' => 'form-select','placeholder' => 'タイプ']) }}
        {{ Form::text('course_word_id', $course_word_id, ['class' => 'form-control','maxlength' => 4,'placeholder' => 'ｺｰｽﾜｰﾄﾞID']) }}
        {{ Form::text('word_name', $word_name, ['class' => 'form-control','maxlength' => 15,'placeholder' => '単語名']) }}
        {{ Form::text('word_id', $word_id, ['class' => 'form-control','maxlength' => 4,'placeholder' => '単語ID']) }}
    </div>
    <div class="menuSpacesCell spaceAll">  
        {{ Form::text('school_name', $school_name, ['class' => 'form-control','maxlength' => 10,'placeholder' => '学校名']) }}
        {{ Form::text('school_id', $school_id, ['class' => 'form-control','maxlength' => 10,'placeholder' => '学校ID']) }}
    @include('layouts.include.searchbtn')
    <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
        @slot('word', 'この画面の使い方')
        @slot('image_name','system12.png')
        @endcomponent
    </div>
    </div>
    {{ Form::close() }}
</div>
@include('layouts.include.pagination')
<table class="table course-wordlist" id="mainTable">
    <thead>
        <tr>
            <th scope="col">有効〇</th>
            <th scope="col">コースID</th>
            <th scope="col">コース名</th>
            <th scope="col">タイプ</th>
            <th scope="col">ｺｰｽﾜｰﾄﾞID</th>
            <th scope="col">単語ID</th>
            <th scope="col">単語名</th>
            <th scope="col">学校ID</th>
            <th scope="col">学校名</th>

        </tr>
    </thead>
    <tbody>
        @foreach ($courseWords as $courseWord)
        @if($courseWord->invalid == 1)
        <tr>
        @else
        <tr>
        @endif
            <td>
                @if($courseWord->invalid == 0)
                〇
                @else
                ×
                @endif
                <td>{{ $courseWord->cw_course_id }}</td>
                <td>{{ $courseWord->course_name }}</td>
                <td>{{ $courseWord->course_type }}</td>
                <td>{{ $courseWord->cw_id }}</td>
                <td>{{ $courseWord->cw_word_id }}</td>
                <td>{{ Str::limit($courseWord->word_mana, 10, '...') }}</td>
            <td>{{ $courseWord->course_school_id }}</td>
            <td>{{ $courseWord->school_name }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
