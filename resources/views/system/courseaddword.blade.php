@extends('layouts.base')

@section('js')
{{--★単語の新規作成と更新は先生メニューとは別のJs、名前が似ているのでチェック--}}
<script src="{{ asset('/js/checkDelBtn.js') }}"></script>
<script src="{{ asset('/js/checkall-dim.js') }}"></script>
<script src="{{ asset('/js/system/addnew-word.js') }}"></script>
<script src="{{ asset('/js/checkedit-addword.js') }}"></script>
<script src="{{ asset('/js/del_modal.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
@endsection

@section('content')

@include('layouts.include.flashmessage')
<table class="table addcourseword02" id="mainTable">
    <thead>
        <tr>
            <th scope="col">無効?</th>
            <th scope="col">is_free?<br>コースID</th>
            <th scope="col">表示順 </th>
            <th scope="col">学校名<br>学校ID</th>
            <th scope="col">コース名</th>
            <th scope="col">タイプ</th>
            <th scope="col">分類</th>
            <th scope="col">レベル</th>
            <th scope="col">time<br>(秒)</th>
            <th scope="col">ﾗﾝﾀﾞﾑ</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                @if($course->invalid == 0)
                有効
                @else
                <span class="text-danger">無効</span>
                @endif
            </td>
            <td>{{ Form::checkbox('is_freer_course', '1', $course->is_free_course, ['class'=>'form-check-input','disabled']) }}
                <br>({{ $course->course_id }})
            </td>
            <td>{{ $course->display_order }}</td>
            <td>{{ $course->school_name }}<br>{{ $course->school_school_id }}</td>
            <td>{{ $course->course_name}}</td>
            <td>{{ $course->course_type}}</td>
            <td>{{ $course->course_category}}</td>
            <td>{{ $course->course_level}}</td>
            <td>{{ $course->test_time}}</td>
            <td>{{ Form::checkbox('random', '1', $course->random, ['class'=>'form-check-input','disabled']) }}</td>
        </tr>
    </tbody>
</table>
<div class="menuSpaces msCenter bottomSpace">


@php
    $id_attr = 'modal-delete-' . 'systemCourseAddWordDelete' . '-' . 'word-checks';
@endphp

{{-- 削除ボタン,heddenでword_idをもたせたいため、コンポーネントのdell_btnはつかえないため、個別に削除ボタンをつくる --}}
<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" id="del" disabled>削除</button>

{{-- 削除の確認モーダルウィンドウ　del_modal.jsで削除対象の名前を取得、文字列の書き換えをおこなう --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="{{ $id_attr }}-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="{{ $id_attr }}">
                   削除の確認
                </h5>
            </div>
            <div class="modal-body">
                <p id="del-title" class="text-danger"><strong>データを削除します</strong></p>
            </div>
            <p id="del-message">単語を削除すると、紐づいている中間テーブルも削除します</p>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    キャンセル
                </button>
                {{-- 削除用のアクションを実行させるフォーム --}}
                <form action="{{ route('systemCourseAddWordDelete',auth()->user()->school_id) }}" id="word-checks" method="POST">
                    @csrf
                    <button id="delete-execute" type="submit" class="btn btn-danger">
                        削除
                    </button>
                    {{ Form::hidden('course_id',$course->course_id,['form' => 'word-checks' ]) }}

                </form>
            </div>
        </div>
    </div>
</div>
{{--削除モーダルここまで--}}
    <button name="add" id="add" type="button" class="btn btn-secondary btn-sm">単語を登録する</button>
    {{ Form::open(['url' => '/'.'course-addword', 'files' => true , 'method' => 'post']) }}
    {{--★hiddenにあるcourse_id と　school は「addnew-courseaddword.js」で使っているので必要、IDで要素を取得している
        実際は、schoolはここでは必要ないが削除しない---}}
    {{ Form::hidden('course_id',$course->course_id,['id' =>'course_id']) }}
    {{ Form::hidden('school_id',$course->school_id,['id' =>'school_id']) }}

    {{ Form::submit('すべて表示', ['name'=>'all_btn','class'=>'btn btn-outline-primary']) }}
    {{ Form::submit('リセット', ['name'=>'all_btn','class'=>'btn btn-outline-primary']) }}
    {{ Form::close() }}
{{-- モーダルの画像に変更
    @component('components.modal')
        @slot('btn_name', '注意事項')
        @slot('title', '単語「よみがな」で使える文字')
        @slot('message', '(1)ひらがな、アルファベット大文字小文字、数字、特殊文字「ー」「、」「。」「.」「:」「;」「全角スペース」「半角スペース」※カタカナ、漢字は使えません。(2)単語は最大30文字(2)表示順：ランダムにチェックがあれば、効かない。重複OK.')
    @endcomponent
--}}
    <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
        @slot('word', '単語の登録方法')
        @slot('image_name','system13.png')
        @endcomponent
    </div>
</div>

@if ($courseWords->count() >0)
   <div>単語登録：{{ $courseWords->count() }}件<div>
@else
    <span class="text-danger">単語が未登録です。</span>
@endif
@include('layouts.include.alertmessage')

<table class="table tablecoursewords" id="mainTableTwo">
    <thead>
        <tr>
            <th> <input type="checkbox" id="checkAll" value="1" class="form-check-input"><br>削除</th>
            <th>表示順<br>（単語ID）</th>
            <th>単語<small>（例）歩く、ミカン、がっこう、CAT、dog</small> </th>
            <th>よみがな<small>※ABCに変換（例）みかん、CAT、dog<br>  <font class="oranges">※漢字・カタカナ不可</font><small></th>
            <th>音声ファイル<br><small>mp3のみ</small></th>
            <th>編集・保存</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($courseWords as $courseWord)
        <tr>
            <td><input type="checkbox" form="word-checks" class="form-check-input" name="checks[]" value="{{$courseWord->cw_word_id }}" id="{{$courseWord->word_mana }}"></td>
            {{ Form::open(['url' => '/'.'add-word', 'files' => true ,'enctype' => 'multipart/form-data', 'method' => 'post']) }}
            {{ Form::token() }}

            {{ Form::hidden('course_id',$courseWord->course_id) }}
            {{ Form::hidden('word_id',$courseWord->word_id) }}
            <td>{{ Form::text('display_order',$courseWord->word_display_order,['class' => 'form-control col-xs-2','maxlength' => '4','readonly']) }}
                <br>【{{$courseWord->word_id}}】<br>(ｺｰｽﾜｰﾄﾞ：{{$courseWord->cw_id}})
            </td>
            <td>{{ Form::text('word_mana',$courseWord->word_mana,['class' => 'form-control col-xs-2','maxlength' => '50','readonly']) }}</td>
            <td>{{ Form::text('word_kana',$courseWord->word_kana,['class' => 'form-control col-xs-2','maxlength' => '50','readonly']) }}</td>
            <td>{{--★ファイルを登録していないとコンソールでファイルがないとエラーをはくので、音声ファイルが存在している時だけ表示させる--}}
            @if($courseWord->pronunciation_file_name !==null)
                <audio id="{{ $courseWord->id }}" src="{{ asset('storage/audio\/').$courseWord->pronunciation_file_name }}",readonly></audio>

                <div class="buttons">
                    <span class="soundplay" onclick="document.getElementById('{{ $courseWord->id }}').play()">再生</span>
                    <span class="soundstop" onclick="document.getElementById('{{ $courseWord->id }}').pause()">停止</span>
                    @endif
                    <label class="fileInputBox">
                        <input type="file" name="audio" class="inputFile" accept=".mp3" disabled >

	                    <span name="file_select" class="fileInputText" >ファイルを選択</span>
                    </label>
                </div>
                @if(!empty($courseWord->pronunciation_file_name))
                音声ファイルあり
                @endif
            </td>
            <td nowrap>
                {{--★音声ファイル削除ボタンは音声ファイルがある時のみ表示--}}
                @if($courseWord->pronunciation_file_name !==null)
                <div class="menuSpacesOneCheck">
                @else
                <div hidden>
                @endif
                        {{Form::checkbox('file_delete',$file_delete, null , ['class'=>'form-check-input','disabled']) }}
                        <label id="file_delete_label" class="form-check-label" for="file_delete" >ファイル削除</label>
                </div>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm', 'disabled']) }}
                {{ Form::close() }}
                {{--★キャンセルボタンで、ファイル選択画像名（未アップロード）をクリアにできないため、キャンセルボタン押下は「すべて表示」と同じメソッドにとばす、コースIDをもちまわるため、リダイレクトできない--}}
                {{ Form::open(['url' => '/'.'course-addword', 'method' => 'post']) }}
                {{ Form::submit('ｷｬﾝｾﾙ', ['name'=>'editcancel','class'=>'btn btn-outline-primary btn-sm','disabled']) }}
                {{ Form::hidden('course_id',$courseWord->course_id) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection