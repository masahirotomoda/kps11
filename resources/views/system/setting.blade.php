@extends('layouts.base')

@section('content')
<div class="menuSpaces">
    <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
        @slot('word', 'この画面の使い方')
        @slot('image_name','system18.png')
        @endcomponent
    </div>
</div>
<table class="table schoolinfonew02" id="mainTable">
    <thead>
        <tr>
            <th>項目</th>
            <th>内容</th>
            <th>注意事項</th>
        </tr>
    </thead>
    <tr>
        <th scope="row">ページネーション</th>
        <td>{{$settings['PAGENATION']}}</td>
        <td>【除外】「先生メニュー」と「学校管理」と「システム管理」のメインメニューからの新着情報⇒30に設定<br>
            【除外】「システム管理」のメインメニューからのトライアル申込⇒100に設定
            【除外】「システム管理」の「ブログ管理」の一覧⇒20に設定
            【除外】「先生メニュー」と「学校管理」の「スコア履歴」⇒50に設定（理由）200出して、PDFボタン押下すると、時間がかかるため<br>
            【除外】「先生メニュー」と「学校管理」の「ベストスコア」⇒50に設定（理由）200出して、PDFボタン押下すると、時間がかかるため<br>
            【除外】「先生メニュー」と「学校管理」の「ベストスコア」⇒50に設定（理由）200出して、PDFボタン押下すると、時間がかかるため
        </td>
    </tr>
    <tr>
        <th scope="row">セッションタイム</th>
        <td>120</td>
        <td>120(単位は分）　※開発環境、本番環境ともに、.envはサーバーをリフレッシュさせないと反映されない⇒php artisan serv</td>
    </tr>
    <tr>
        <th scope="row">コピーライト</th>
        <td>{{$settings['COPYRIGHT']}}</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">トライアル期間（公立学校）</th>
        <td>{{$settings['TRIAL_PERIOD_SCHOOL']}}</td>
        <td>3か月、トライアル画面で変更可能 ※ここの値は使っていない！TrialController の「submit」メソッドでは、直接数値指定</td>
    </tr>
    <tr>
        <th scope="row">トライアル期間（教室）</th>
        <td>{{$settings['TRIAL_PERIOD_CRAM']}}</td>
        <td>１か月、トライアル画面で変更可能 ※ここの値は使っていない！TrialController の「submit」メソッドでは、直接数値指定</td>
    </tr>
    <tr>
        <th scope="row">META_TITLE</th>
        <td>{{$settings['META_TITLE']}}</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">トライアル学校intID</th>
        <td>{{$settings['TRIAL_SCHOOL_NO']}}</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">トライアル学校ID</th>
        <td>{{$settings['TRIAL_SCHOOL_NO_URL']}}</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">ナレッジタイピングドメイン</th>
        <td>{{$settings['DOMAIN_NAME']}}</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">運営スクール</th>
        <td>{{$settings['ADMIN_SCHOOL']}}</td>
        <td>【外】新着情報</td>
    </tr>
    <tr>
        <th scope="row">運営スクール郵便番号</th>
        <td>{{$settings['ADMIN_ZIP']}}</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">運営スクール住所</th>
        <td>{{$settings['ADMIN_ADDRESS']}}</td>

        <td></td>
    </tr>
    <tr>
        <th scope="row">運営スクールTEL</th>
        <td>{{$settings['ADMIN_TEL']}}</td>

        <td>運営スクール</td>
    </tr>
    <tr>
        <th scope="row">運営スクールMail</th>
        <td>{{$settings['ADMIN_MAIL']}}</td>
        <td>お問い合わせでつかうメール、お問い合わせフォームはナレッジのを使う⇒https://knowledge-p.jp/contact/</td>
    </tr>
    <tr>
        <th scope="row">運営スクールipaddress</th>
        <td>{{$settings['ADMIN_IPADDRESS']}}</td>
        <td>運営スクールのIPアドレス</td>
    </tr>
    <tr>
        <th scope="row">運営スクール　学校intID</th>
        <td>{{$settings['ADMIN_SCHOOL_NO']}}</td>
        <td>変更不可</td>
    </tr>
    <tr>
        <th scope="row">運営スクール　学校ID</th>
        <td>{{$settings['ADMIN_SCHOOL_NO_URL']}}</td>
        <td>URLで使う、変更不可</td>
    </tr>
    <tr>
        <th scope="row">特別学校アカウント</th>
        <td>{{$settings['SCHOOL_ACCOUNT_FOR_ADMIN']}}</td>
        <td>運営スクールが各学校で使う隠し学校アカウント。ログイン履歴を表示させない、利用最大数からはずしている。全ての学校に必ず作るのでなく、必要な際に作る。その際は、必ずこのアカウントにする。【重要】このアカウントは、直書きしていない。ここを変更すると全てに反映される</td>
    </tr>
    <tr>
        <th scope="row">学校カテゴリー</th>
        <td>'SCHOOL_CATEGORY' => [
            1 => '漢字',
            2 => 'かな',</td>
        <td>まだ未使用。schoolにカラムも追加していない。今後、学校によって感じ、かなにわける時につかう</td>
    </tr>
    <tr>
        <th scope="row">ロールリスト</th>
        <td>'ROLE_LIST' => [
            1 => 'システム管理者',
            2 => '学校管理者',
            3 => '先生',
            4 => '生徒',</td>
        <td>ロールの切り分けで使用</td>
    </tr>
    <tr>
        <th scope="row">ROLE_LIST2</th>
        <td>'ROLE_LIST2' => [
            '学校管理者' => '学校管理者',
            '先生' => '先生',
            '生徒' => '生徒',</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">ROLE_LIST3</th>
        <td>'ROLE_LIST2' =>'ROLE_LIST3' => [
            '生徒' => '生徒',
            '先生' => '先生',
            '学校管理者' => '学校管理者',</td>
        <td>システム管理　ユーザー更新のドロップダウン値（生徒が一番上にくる）</td>
    </tr>
    <tr>
        <th scope="row">LEVEL_LIST</th>
        <td>'LEVEL_LIST' => [
            '★' => '★',
            '★★' => '★★',
            '★★★' => '★★★',
            'その他' => 'その他',</td>
        <td>コースで使用</td>
    </tr>
    <tr>
        <th scope="row">CATEGORY_LIST</th>
        <td>'ホームポジション' => 'ホームポジション',
            'ローマ字50音' => 'ローマ字50音',
            'ことば' => 'ことば',
            'ことわざ' => 'ことわざ',
            '英単語' => '英単語',
            '短い文章' => '短い文章',
            '長い文章' => '長い文章',
            'テスト' => 'テスト',
            '今月のタイピング' => '今月のタイピング',
            'キータッチ2000検定' => 'キータッチ2000検定',
            'その他' => 'その他',</td>
        <td>変更不可、コースで使用、今月のタイピングとキータッチはここで判定</td>
    </tr>
    <tr>
        <th scope="row">COURSE_TYPE</th>
        <td>'COURSE_TYPE' => [
            '組込' => '組込',
            'テスト' => 'テスト',
            '追加' => '追加',</td>
        <td>重要、最優先される。組込み⇒通常のタイピングレイアウト、テスト⇒テスト用レイアウト、追加⇒学校ごとの追加タブに表示されるコース</td>
    </tr>
    <tr>
        <th scope="row">ENTRY_TYPE</th>
        <td> '小学校 ' => '小学校',
            '中学校' => '中学校',
            '高校' => '高校',
            '専門学校' => '専門学校',
            '大学' => '大学',
            'プログラミング教室' => 'プログラミング教室',
            '塾' => '塾',
            '各種教室' => '各種教室',
            'その他' => 'その他',</td>
        <td>トライアル申込時のカテゴリのドロップダウンリスト</td>
    </tr>
    <tr>
        <th scope="row">ログイン周り</th>
        <td>強制ログアウト</td>
        <td>先生・生徒⇒n-typing.com/logout ⇒ログアウト後、n-typing.com/login<br>
            学校管理⇒n-typing.com/school/logout ⇒n-typing.com/school/login<br>
            ※学校管理の強制ログアウトは作っていないため、先生・生徒のログアウトを使う
        </td>
    </tr>
    <tr>
        <th scope="row">ログイン周り</th>
        <td>クッキー</td>
        <td>学校IDは、強制的にクッキー保存　※クッキー保存期間：90日<br>
            「ログインIDを保存する」にチェックすると、ログインIDのみ、クッキーに保存。パスワードはクッキーに保存しない。
        </td>
    </tr>
    <tr>
        <th scope="row">ログイン周り</th>
        <td>パスワード入力ミス回数</td>
        <td>ログイン試行回数10回とロック時間10分やログイン関連の設定⇒app\Http\Controllers\Auth\LoginController.php --}}</td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
</table>

<h3 class="titles">変更するとき・・・</h3>

<table class="table schoolinfonew02" id="mainTable">
    <thead>
        <tr>
            <th>項目</th>
            <th>内容</th>
            <th>注意事項</th>
        </tr>
    </thead>
    <tr>
        <th scope="row"></th>
        <td>特別学校アカウント発行</td>
        <td>システム管理⇒ユーザー管理（２）⇒特別学校アカウント発行ボタン押下で登録。学校アカウントはb33259で、configで設定。ログイン履歴で表示させない。学校毎の最大人数のカウントに関連。値は、すべてconfigから取得。変更の際は、configで変更すればOK</td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
</table>


<h3 class="titles">システム基本情報</h3>
<table class="table schoolinfonew02" id="mainTable">
    <thead>
        <tr>
            <th>項目</th>
            <th>内容</th>
            <th>注意事項</th>
        </tr>
    </thead>
    <tr>
        <th scope="row">モデル</th>
        <td>app\Models</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">コントローラー</th>
        <td>app\Http\Controllers</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">ミドルウェア</th>
        <td>app\Http\Middleware</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">バリデーションルール</th>
        <td>app\Rules</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">エクセル一インポート</th>
        <td>app\Imports,config\excel.php</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">エクセルエクスポート</th>
        <td>\app\Exports\Export.php</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">configrations</th>
        <td>config\configrations.php</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">セッション</th>
        <td>config\session.php</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">マイグレーション</th>
        <td>database\migrations</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">css</th>
        <td>public\css</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">画像</th>
        <td>public\img</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Js</th>
        <td>public\js</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">バリデーションカスタムメッセージ</th>
        <td>resources\lang\ja\validation.php</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">コンポーネント</th>
        <td>resources\views\components</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">エラーページ</th>
        <td>resources\views\errors</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">全体レイアウト</th>
        <td>resources\views\layouts</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">インクルードファイル</th>
        <td>resources\views\layouts\include</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">エディタ　trix</th>
        <td>https://github.com/basecamp/trix</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">ルーティング</th>
        <td>routes\web.php</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">env</th>
        <td>.env</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
    </tr>
</table>
<div class="phpInfos">
    {{phpinfo()}}
</div>
@endsection