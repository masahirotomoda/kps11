@extends('layouts.base')
@section('js')
<script src="{{ asset('/js/textarea-count.js') }}"></script>
<script src="{{ asset('/js/dell_modal.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
@endsection

@section('content')

@include('layouts.include.flashmessage')

<table class="table addcourseword02" id="mainTable">
    <thead>
        <tr>
            <th scope="col">無効</th>
            <th scope="col">free<br>コースID</th>
            <th scope="col">表示順 </th>
            <th scope="col">学校名</th>
            <th scope="col">コース名</th>
            <th scope="col">タイプ</th>
            <th scope="col">分類</th>
            <th scope="col">レベル</th>
            <th scope="col">テスト時間</th>
            <th scope="col">ランダム</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                @if($course->invalid == 0)
                    {{"有効"}}
                @else
                    <span class="text-danger">{{"無効"}}</span>
                @endif
            </td>
            <td>{{ Form::checkbox('is_freer_course', '1', $course->is_free_course, ['class'=>'form-check-input','disabled']) }}
                <br>({{ $course->course_id }})
            </td>
            <td>{{ $course->display_order }}</td>
            <td>{{ $course->school_name }}<br>{{ $course->school_id }}</td>
            <td>{{ $course->course_name}}</td>
            <td>{{ $course->course_type}}</td>
            <td>{{ $course->course_category}}</td>
            <td>{{ $course->course_level}}</td>
            <td>{{ $course->test_time}}</td>
            <td>{{ Form::checkbox('random', '1', $course->random, ['class'=>'form-check-input','disabled']) }}</td>
        </tr>
    </tbody>
</table>
<div class="menuSpaces">
    {{ Form::open(['url' => '/'.'course-addword', 'files' => true , 'method' => 'post']) }}
    {{ Form::token() }}
    {{ Form::hidden('course_id',$course->course_id) }}
    {{ Form::submit('すべて表示', ['name'=>'all_btn','class'=>'btn btn-outline-primary']) }}
    {{ Form::submit('リセット', ['name'=>'all_btn','class'=>'btn btn-outline-primary']) }}
    {{ Form::close() }}

    {{--削除ボタンはコンポーネント内にform open closeがあるため、フォーム内ではつかわない--}}
    @if($courseWords !== null)
        @component('components.delButtonOne')
            @slot('route', 'systemCourseWordTest')
            @slot('id', 'teacher-checks')
            @slot('name', '単語ID：'.$courseWords['cw_word_id'])
            @slot('hidden_name', 'word_id')
            @slot('hidden_value', $courseWords['cw_word_id'])
            @slot('message','※削除した単語に紐づく、中間テーブル（course_word）も削除されます')
        @endcomponent
    @endif
    {{ Form::open(['url' => '/wordtest', 'name' =>'test_delete_btn', 'enctype' => 'multipart/form-data', 'method' => 'post']) }}
    {{ Form::token() }}
    {{ Form::hidden('course_id',$course_id) }}

    @if($courseWords === null)
        {{ Form::submit('新規登録する', ['name'=>'test_add','class'=>'btn btn-primary btn-block']) }}
        {{--すでに単語登録があり、編集をする場合--}}
    @else
        {{ Form::submit('更新する', ['name'=>'test_save','class'=>'btn btn-primary btn-block']) }}
    @endif
</div>
<div class="courseaddwordBlock">
    <div class="courseaddwordCell">
    <h3>テスト問題
        @if($courseWords !== null)
        <small>（単語ID:{{$courseWords->cw_word_id}} course_wordID:{{$courseWords->cw_id}}）</small>
        @endif
    </h3>
    <p id="word-count">残り2000文字</p>
    <ul>
        <li>単語と単語の間に半角スペースをいれる。</li>
        <li>1行30文字（半角スペース含め）</li>
        <li>キータッチ2000検定モードのみ、大文字、小文字を認識する。</li>
    </ul>
</div>

{{--初めて単語登録する場合　course_wordテーブルのデータがない--}}
@if($courseWords === null)
    <textarea name="word_mana" rows="67" cols="30" class="form-control col-xs-2" id = "word_mana" maxlength="2000" placeholder ='最大2000文字、アルファベット' required></textarea>

{{--すでに単語登録があり、編集をする場合--}}
@else
{{Form::hidden('word_id',$courseWords->cw_word_id)}}

{{--※余計なスペースがあると入力ボックスにもスペースが入るので、左に詰める（インデントなし）--}}
<textarea name="word_mana" rows="67" cols="30" class="form-control col-xs-2" id = "word_mana" maxlength="3000" required>
{{$courseWords->word_mana}}
</textarea>
@endif
{{ Form::close() }}
</div>
@endsection