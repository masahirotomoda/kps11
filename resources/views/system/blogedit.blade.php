@extends('layouts.base')

@section('js')
<script src="{{ asset('js/trix.js') }}"></script>
<script src="{{ asset('js/addnew_all.js') }}"></script>
<script src="{{ asset('js/btn_all_reset.js') }}"></script>
@endsection
@section('head_1')
<link href="{{ asset('/css/trix.css') }}" rel="stylesheet">
@endsection

@section('content')
@include('layouts.include.flashmessage')

{{--common.js?v=20240123 にあるalertのモーダルがなぜか出ないので。。。--}}
@foreach ($errors->all() as $error)
    <li>{{$error}}</li>
@endforeach


{{ Form::open(['url' => '/blog-save', 'files' => true , 'enctype' => 'multipart/form-data' , 'method' => 'post' , 'name' => 'ansform']) }}
{{ Form::hidden('id',$blog->id) }}

<div class="menuSpaces">
    <div class="menuSpacesCell spaceAll inputFull">
    {{ Form::text('title', $blog->title, ['class' => 'form-control', 'id' => 'title', 'placeholder' => 'タイトル']) }}
    </div>
</div>

<div class="infoBlockCellImage">
    @if($blog->image === null)

    @else
    <img src="{{ asset('storage/blogimages\/').$blog->image }}" width="200">
    @endif
</div>

<div class="menuSpaces">
    <div class="menuSpacesCell">
        <label class="fileInputBox">
            <input type="file" id="file" name="file" class="inputFile" files = "true" enctype = "multipart/form-data">
            <span class="fileInputText">ファイルを選択</span>
        </label>
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('img_delete',$img_delete, null , ['class'=>'form-check-input']) }}
            <label class="form-check-label" for="chkSystem">ファイルを削除する</label>
        </div>
    </div>
        @component('components.tooltip')
            @slot('word', 'FreeTOPの表示画像ファイル（正方形）')
            @slot('message', 'アップロードできるのは「png」「jpeg(jpg)」「gif」です。')
        @endcomponent
</div>
{{--trixエディタ使用https://github.com/basecamp/trix--}}
<div class="trixeditorOuter">
    <input id="x" type="hidden" value = "{{ $blog->article }}" name="article">
    <trix-editor input="x"></trix-editor>
</div>
<div class="menuSpacesOne noSpaces">
    <div class="radioOut">
        <input type="radio" name="private"  id="private1" value='公開' {{ $open }}  >
        <label for="on">公開</label>
    </div>
    <div class="radioOut">
        <input type="radio" name="private"  id="private2" value='非公開' {{ $close }} >
        <label for="off">非公開</label>
    </div>
</div>
<div class="btnOuter">
  @if($blog->id === null)
    <button name="edit" class="btn btn-secondary btn-sm" id="update_btn" type="submit">登録する</button>
  @else
    <button name="edit" class="btn btn-secondary btn-sm" id="update_btn" type="submit">更新する</button>
  @endif

  @component('components.delButtonOne')
    @slot('route', 'systemBlogSave')
    @slot('id', 'Delete')
    @slot('name', $blog['title'])
    @slot('hidden_name', 'id')
    @slot('hidden_value', $blog['id'])
    @slot('message','ブログを削除します')
    @endcomponent
</div>
{{ Form::close() }}
@endsection

