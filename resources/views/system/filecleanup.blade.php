@extends('layouts.base')
@section('js')
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
@endsection
@php
if($SoundOrImage === 'sound'){
$file_type = '単語の音声';
} else {
$file_type = 'ブログの画像';
}
@endphp
@section('content')
@include('layouts.include.flashmessage')
{{Form::open(['url' => '/file-select', 'method' => 'post'])}}
<div class="menuSpaces">
    <div class="menuSpacesOne">
        <div class="radioOut">
            <input type="radio" name="file_selectbox" id="file_selectbox1" value='mp3' {{$status_file_selectbox1}}>
            <label for="file_selectbox1">【未使用】ワード音声ファイル</label>
        </div>
        <div class="radioOut">
            <input type="radio" name="file_selectbox" id="file_selectbox2" value='img' {{$status_file_selectbox2}}>
            <label for="file_selectbox2">【未使用】ブログ画像ファイル</label>
        </div>
    </div>
    @include('layouts.include.searchbtn')
</div>
{{Form::close()}}
{{--音声か画像かをもたせるため、通常のdelbuttonは使えないためdelButtonHidden--}}
@component('components.delButtonHidden')
@slot('route', 'systemFileDelete')
@slot('id', 'file-checks')
@slot('name', 'ファイル')
@slot('hidden_name', 'hidden_file_selectbox')
@slot('hidden_value', $file_category)
@slot('message','※削除したファイルは復元できません。')
@endcomponent
@if($is_not_file_count === null)
<span class="text-danger">未使用データはありません。</span>
@else
<span class="text-danger">使っていない{{$file_type}}ファイル：{{$is_not_file_count}}ファイル</span>
<table class="table maintenanceinfo02" id="mainTable">
    <thead>
        <tr>
            <th scope="col"><input type="checkbox" id="checkAll" value="1">削除</th>
            <th scope="col">使っていない{{$file_type}}ファイル名(public/audio)</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($is_not_file as $file)
        {{Form::open(['url' => '/file-delete', 'method' => 'post'])}}
        <tr>
            <td><input type="checkbox" form="file-checks" class="form-check-input" name="checks[]" value="{{$file}}" id="{{$file}}"></td>
            <td><span name="edit">{{$file}}</span></td>
        </tr>
        {{Form::close()}}
        @endforeach
    </tbody>
</table>
@endif
@endsection