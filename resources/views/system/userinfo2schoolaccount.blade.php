@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/addnew_all.js') }}"></script>
@endsection

@section('content')

@include('layouts.include.flashmessage')
<h3 class="names">【{{$school->school_id}}】{{$school->name}}({{$school->id}})</h3>
<p><span class="text-danger">通常ログインIDは自動発行。ログインIDを指定する学校特別アカウントをここで作成。</span></p>
{{--特別学校アカウント発行　簡易的につくるため、新規のみ、修正削除はユーザーページで--}}
{{ Form::open(['url' => '/user2-saveschoolaccount','method' => 'post']) }}
{{ Form::hidden('select_school_id',$school->id) }}
<div>   
    特別学校アカウント　例：b33259 <span class="red">重複チェックはしないため、あらかじめ重複しないことを確認する</span><br>
    {{Form::text('login_account', $login_account,['class' => 'form-control', 'placeholder' => '特別学校アカウント例：b33259']) }}<br>
    名前　例：ナレッジ小【特別学校学校管理者】<br>
    {{ Form::text('name', $name, ['class' => 'form-control', 'placeholder' => '例：経堂小【特別学校学校管理者】']) }}<br>
    パスワード　例：abc12345!!　※アルファベット、数字、記号3種類以上で10～20文字 <br>
    {{ Form::text('password', $password, ['class' => 'form-control', 'placeholder' => '例：know!!ledge.. ']) }}<br>
    <button class="btn btn-secondary btn-sm" name="addschoolaccount_btn" type="submit">登録する</button>   
</div>
@include('layouts.include.alertmessage')
{{ Form::close() }}
@endsection
