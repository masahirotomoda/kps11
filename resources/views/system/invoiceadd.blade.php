@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/system/for_delButtonOne.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
<script>
    document.getElementById('invoiceCount').addEventListener('change', (event) => {
        if(document.getElementById('invoiceTanka').value !==null){
            console.log(document.getElementById('invoiceCount'));
            let tanka=document.getElementById('invoiceTanka').value;
            console.log(tanka);
            let count=document.getElementById('invoiceCount').value;
            let price=tanka*count;
            let tax=Math.floor(price-(price/1.1));
            document.getElementById('invoicePrice').value=price;
            document.getElementById('invoiceTax').value=tax;
        } 
    });
    document.getElementById('invoiceTanka').addEventListener('change', (event) => {
        if(document.getElementById('invoiceCount').value !==null){
            let tanka=document.getElementById('invoiceTanka').value;
            let count=document.getElementById('invoiceCount').value;
            let price=tanka*count;
            let tax=Math.floor(price-(price/1.1));
            document.getElementById('invoicePrice').value=price;
            document.getElementById('invoiceTax').value=tax;
        } 
    });
</script>
@endsection

@section('content')
@include('layouts.include.flashmessage')
@php
if($AddOrUpdate == 'update'){
    $InvoiceId=$invoice -> id;
    $SchoolIntId=$invoice ->school_int_id;
    $SeikyuDay=$invoice ->seikyu_day;
    $KigenDay=$invoice ->kigen_day;
    $JuryoDay=$invoice ->juryo_day;
    $Service=$invoice -> service;
    $ServicePeriod= $invoice -> service_period;
    $Other=$invoice -> other;
    $Memo=$invoice -> memo;
    $Tanka=$invoice -> tanka;
    $Count= $invoice -> count;
    $Price= $invoice -> price;
    $Tax=$invoice -> tax;
    $ShowInvoice=$invoice -> show_invoice;
    $ShowReceipt=$invoice -> show_receipt;
    $Howpay=$invoice ->howpay;
} elseif($AddOrUpdate == 'add'){
    $InvoiceId=null;
    $SchoolIntId=null;
    $SeikyuDay=null;
    $KigenDay=null;
    $JuryoDay=null;
    $Service=null;
    $ServicePeriod=null;
    $Other=null;
    $Memo=null;
    $Tanka=null;
    $Count=null;
    $Price=null;
    $Tax=null;
    $ShowInvoice=null;
    $ShowReceipt=null;
    $Howpay=null;
}
@endphp

@if($AddOrUpdate == 'update')
<h3 class="titlesBorders">更新モード</h3>
@elseif($AddOrUpdate == 'add')
<h3 class="titlesBorders">新規登録モード</h3>
@endif
<p>このページで、ブラウザの<span class="red">「更新ボタン」は使わない</span>でください。</p>
{{--更新と新規で処理を分ける　$AddOrUpdateはフラグ--}}
{{ Form::open(['url' => '/invoice-store', 'method' => 'post']) }}
<table class="table schoolinfonew" id="mainTable">
    <thead>
        <tr>
            <th>項目</th>
            <th>内容</th>
            <th>注意事項</th>
        </tr>
    </thead>
    <tr>
        <th scope="row">請求書ID（請求書NO）<span class="red">*</span></th>
        <td>{{Form::text('invoice_id', $InvoiceId, ['class' => 'form-control', 'placeholder' => '請求書ID', 'readonly'])}}</td>        
        <td>請求書NOはIDに1000をプラス　⇒<span class="red">{{1000+$InvoiceId}}</span> ※自動採番、変更不可。</td>
    </tr>
    <tr>
        <th scope="row">学校名<span class="red">*</span></th>
        <td>{{Form::select('school_int_id', $school_selection,$SchoolIntId, ['class' => 'form-select'])}}</td>
        <td>保存されるのは、学校int_idでログインアカウントではない。</td>
    </tr>    
    <tr>
        <th scope="row">請求書・発行日<span class="red">*</span></th>
        <td>{{ Form::date('seikyu_day',$SeikyuDay,['id' => 'seikyu_day','class' => 'form-control','required' => 'required'])}}</td>
        <td>請求書の右上にある発行日 【振込】利用月の月末請求、翌月末期限（例）4月利用分は<span class="red">4月26日発行</span>、5月末入金期限【クレジット】4月利用分は3月26日発行、4月1日引き落とし</td>
    </tr>
    <tr>
        <th scope="row">請求書・支払い期限日<span class="red">*</span></th>
        <td>{{ Form::date('kigen_day',$KigenDay,['id' => 'kigen_day','class' => 'form-control','required' => 'required'])}}</td>
        <td>【振込】利用月の翌月末期限（例）4月利用分は<span class="red">5月末</span>入金期限【クレジット】4月利用分は3月26日発行、4月1日引き落とし<span class="red">4月15日</span>が期限</td>
    </tr>
    <tr>
        <th scope="row">入金日（受領日）</th>
        <td>{{ Form::date('juryo_day',$JuryoDay,['id' => 'juryo_day','class' => 'form-control'])}}</td>
        <td>受領書に記載する入金日。※受領書には、発行日はなく、日付は受領日のみ。<span class="red">クレジットは1日</span></td>
    </tr>
    <tr>
        <th scope="row">サービス名<span class="red">*</span></th>
        <td>{{Form::text('service', $Service, ['class' => 'form-control', 'maxlength'=>'50','placeholder' => 'サービス名','required' => 'required'])}}</td>
        <td>ナレッジタイピング for school</td>
    </tr>
    <tr>
        <th scope="row">サービス名利用期間</th>
        <td>{{Form::text('service_period',$ServicePeriod, ['class' => 'form-control', 'maxlength'=>'50','placeholder' => 'サービス利用期間'])}}</td>
        <td>2024年3月</td>
    </tr>
    <tr>
        <th scope="row">支払い方法<span class="red">*</span></th>
        <td>{{Form::select('howpay', $howpay_selection,$Howpay,['class' => 'form-select','placeholder' => '支払い方法','required' => 'required'])}}</td>
        <td>クレジットカード　　銀行振込　⇒どちらかに統一</td>
    </tr>
    <tr>
        <th scope="row">備考</th>
        <td>{{Form::textarea('other', $Other, ['class' => 'form-control', 'maxlength'=>'300','placeholder' => '備考'])}}</td>
        <td>請求書下部に表示する備考欄（例）生徒1名1か月110円です。（最大300文字）</td>
    </tr>
    <tr>
        <th scope="row">メモ（ナレッジ用）</th>
        <td>{{Form::textarea('memo',$Memo, ['class' => 'form-control', 'maxlength'=>'1000','placeholder' => 'ナレッジ用メモ'])}}</td>
        <td>ナレッジ用メモ（請求書には表示されない）（最大1000文字）</td>
    </tr>
    <tr>
        <th scope="row">単価<span class="red">*</span></th>
        <td>{{Form::text('tanka', $Tanka, ['id'=>'invoiceTanka','class' => 'form-control', 'maxlength'=>'100','placeholder' => '単価','required' => 'required'])}}</td>
        <td>単価（税込み）⇒数字のみ、計算するため　※半角</td>
    </tr>
    <tr>
        <th scope="row">数量<span class="red">*</span></th>
        <td>{{Form::text('count',$Count, ['id'=>'invoiceCount','class' => 'form-control', 'maxlength'=>'100','placeholder' => '数量（生徒数）','required' => 'required'])}}</td>
        <td>数量（例）30  ⇒数字のみ、計算するため※半角</td>
    </tr>
    <tr>
        <th scope="row">合計金額<span class="red">*</span></th>
        <td>{{Form::text('price',$Price, ['id'=>'invoicePrice','class' => 'form-control', 'maxlength'=>'100','placeholder' => '合計金額（税込み）','required' => 'required'])}}</td>
        <td>合計金額（例）2200　⇒数字のみ。計算するため　※半角</td>
    </tr>
    <tr>
        <th scope="row">消費税<span class="red">*</span></th>
        <td>{{Form::text('tax',$Tax, ['id'=>'invoiceTax','class' => 'form-control', 'maxlength'=>'100','placeholder' => '合計金額（税込み）','required' => 'required'])}}</td>
        <td>消費税（例）200　⇒数字のみ。計算するため　※半角</td>
    </tr>
    <tr>
        <th scope="row">請求書発行</th>
        <td>{{Form::checkbox('show_invoice',$ShowInvoice,$ShowInvoice,['class' => 'form-check-input'])}}請求書を発行する</td>
        <td>学校管理画面に表示する</td>
    </tr>
    <tr>
        <th scope="row">受領書発行</th>
        <td>{{Form::checkbox('show_receipt',$ShowReceipt,$ShowReceipt,['class' => 'form-check-input'])}}受領書を発行する</td>
        <td>学校管理画面に表示する</td>
    </tr>   
    <tr>
        <th scope="row">登録日/更新日</th>
        @if($AddOrUpdate == 'update')
        <td>登録日：{{$invoice -> created_at}}<br>更新日：{{$invoice -> updated_at}}</td>
        @else
        <td>登録日：<br>更新日：</td>
        @endif
        <td>
    </td>
    </tr>
</table>

<div class="btnOuter">
    @if($AddOrUpdate === 'update')
    <button name="update" class="btn btn-primary btn-block" id="update_Btn" type="submit">更新する</button>
    @else
    <button name="add" class="btn btn-primary btn-block" id="update_Btn" type="submit">新規登録する</button>
    @endif
</div>
{{ Form::close() }}
{{--削除はめったに使わないため（誤操作を防ぐ）、更新ボタンと離す、--}}
@if($AddOrUpdate === 'update')

@component('components.delButtonOne')
    @slot('route', 'systemInvoiceDel')
    @slot('id', $invoice['id'])
    @slot('name', '学校名：'.$school['name'])
    @slot('hidden_name', 'id')
    @slot('hidden_value', $invoice['id'])
    @slot('message','サービス名：'.$invoice['service_period'].$invoice['service'])
    @endcomponent

@endif
@include('layouts.include.alertmessage')
@endsection