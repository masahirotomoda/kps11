@extends('layouts.base')

@section('content')
@php
//URLから、システム管理の「ユーザー管理」「クラス管理」「学年設定」「組設定」を分岐
$currentURL=request()->path();
switch ($currentURL) {
    case "class2-school-select";
        $path = "class2";
        break;
    case "user2-school-select";
        $path = "user2";
        break;
    case "grade2-school-select";
        $path = "grade2";
        break;
    case "kumi2-school-select";
        $path = "kumi2";
        break;
    default;
        $path = null;
}              
@endphp
{{ Form::open(['url' => '/'.$path,'method' => 'post']) }}

<div class="menuSpaces">
{{--セレクトボックスの名前は他で使用中なので変更不可--}}
{{ Form::select('select_school_id',$school_search_selection,$selected_school_int_id,['class' => 'form-select','id' => 'school_new_selection']) }}
<button class="btn btn-outline-primary btn-sm" name="school_select_btn" type="submit">学校選択</button>  
{{ Form::close() }}
</div>
@endsection
