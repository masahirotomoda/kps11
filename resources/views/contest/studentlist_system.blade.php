@extends('layouts.base')

@section('js')
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
<script src="/js/system/btn_excelexport.js"></script>
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/del_modal.js"></script>
@endsection

@section('content')
<div class="menuSpaces bottomMin">
{{Form::open(['url' => '/contest-studentlist-search', 'method' => 'post','id' => 'search_form'])}}
@include('layouts.include.alertmessage')
    <div class="menuSpacesCell">
        {{ Form::text('school_id', $school_id, ['type' => 'text' , 'class' => 'form-control','id' => 'school_id', 'maxlength' => 8 ,'placeholder' => '学校ID']) }}
        {{ Form::text('school_name', $school_name, ['type' => 'text' , 'class' => 'form-control','id' => 'school_name', 'maxlength' => 15 ,'placeholder' => '学校名の一部']) }}
        {{Form::text('school_grade',  $grade_select, ['class' => 'form-select','id' => 'school_grade','placeholder' => '学年or教室名の一部']) }}
        {{Form::text('school_kumi', $kumi_select, ['class' => 'form-select','id' => 'school_kumi','placeholder' => '組']) }}
        {{Form::text('school_attendance_no', $attendance_no_select, ['class' => 'form-select','id' => 'school_attendance_no','placeholder' => '出席番号or生徒番号の一部']) }}
        {{Form::text('school_student_name', $user_name, ['class' => 'form-control','id' => 'school_student_name','placeholder' => '生徒名の一部','size' =>'20','maxlength' =>'20','autocomplete' =>'off']) }}
    </div>
    <div class="menuSpacesCell">
        <div class="menuSpacesOneCheck">
            <input type="checkbox" name="school_invalid" value=true" class="form-check-input" id="school_invalid" {{$school_invalid}}>
            <label class="form-check-label" for="school_invalid">学校無効</label>
        </div>
        <div class="menuSpacesOneCheck">
            <input type="checkbox" name="test_mode" value=true" class="form-check-input" id="test_mode" {{$test_mode}}>
            <label class="form-check-label" for="test_mode">テストモード</label>
        </div>
        <div class="menuSpacesOneCheck">
            <input type="checkbox" name="practice_mode" value=true" class="form-check-input" id="practice_mode" {{$practice_mode}}>
            <label class="form-check-label" for="practice_mode">練習モード</label>
        </div>  
        @include('layouts.include.searchbtn')
        <button class="btn btn-secondary btn-sm" id="excelbtn" name="excel" type="submit">Excel出力</button>
        {{Form::close()}}
        @component('components.delButton')
        @slot('route', 'contestStudentDel')
        @slot('id', 'contest_studentlist-checks')
        @slot('name', 'コンテスト生徒')
        @slot('message','生徒を削除すると、コンテストスコア履歴も削除されます。')
        @endcomponent
    </div>    
</div>
<div class="infotext">
        <p class="largeTexts"><span>全生徒数</span><strong>{{$student_num}}</strong>人</p>
        @if($student_num !==  $users->total())
        <p class="largeTexts"><span>検索後の生徒数</span><strong>{{$users->total()}}</strong>人</p>
        @endif
</div>
@include('layouts.include.pagination')
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th>削除<input type="checkbox" id="checkAll" value="1"></th>            
            <th scope="col">学校ID<br>学校アカウント</th>
            <th scope="col">学校名<br>コンテストアカウント</th>
            <th scope="col">生徒ID<br>生徒名</th>
            <th scope="col">学年or教室名</th>
            <th scope="col">組<br>出席No,生徒No</th>
            <th scope="col">登録日</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        @if($user->invalid !==1)
        <tr>
        @else
        <tr class="bg-secondary text-white invalidity">
        @endif
            <td>
                {{ Form::open(['url' => '/contest-studentlist-delete', 'method' => 'post']) }}
                {{ Form::hidden('school_id', $user->achool_id)}}{{--★Jsで使うのでhiddenは必要--}}
                {{ Form::hidden('id', $user->id)}}
                <input type="checkbox" name="checks[]" value="{{ $user->id }}" id="{{ $user->name }}" form="contest_studentlist-checks">
            </td>
            <td name="edit">{{$user->sc_id}}<br>{{$user->sc_school_id}}</td>
            <td>{{$user->sc_name}}<br>{{$user->sc_school_id}}{{$user->password_teacher_init}}</td>
            <td>{{$user->id}}<br>{{$user->name}}</td>
            <td>{{$user->grade}}</td>
            <td>{{$user->kumi}}<br>{{$user->attendance_no}}</td>
            <td>{{date('Y/m/d', strtotime($user->created_at))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
