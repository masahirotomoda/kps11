<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>実施日</th>
        <th>学校IntId</th>
        <th>学校ID</th>
        <th>学校名</th>
        <th>コンテストアカウント</th>
        <th>生徒ID</th>
        <th>名前</th>
        <th>学年or教室名</th>
        <th>組</th>
        <th>出席番号or生徒番号</th>
        <th>コースID</th>
        <th>コース名</th>
        <th>スコア</th>
        <th>時間</th>       
        <th>モード</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($scoreHistory as $sh)
        <tr>
        <td>{{ $sh->id }}</td>
        <td>{{ date_format($sh->created_at,'Y年m月d日 H:i') }}</td>
        <td>{{ $sh->sc_school_int_id }}</td>
        <td>{{ $sh->sc_school_id }}</td>
        <td>{{ $sh->sc_name }}</td>
        <td>{{$sh->sc_school_id}}{{$sh->password_teacher_init}}</td>
        <td>{{$sh->st_id}}</td>
        <td>{{ $sh->st_name }}</td>
        <td>{{ $sh->grade }}</td>
        <td>{{ $sh->kumi }}</td>
        <td>{{ $sh->attendance_no }}</td>
        <td>{{ $sh->course_id }}</td>
        <td>{{ $sh->course_name }}</td>
        <td>{{ $sh->point }}</td>
        <td>{{ $sh->time }}</td>
        <td>@if($sh->sc_contest==1)
                テストモード
            @elseif($sh->sc_contest==2)
                練習モード
            @endif</td>
        </tr>
        @endforeach
    </tbody>
</table>


