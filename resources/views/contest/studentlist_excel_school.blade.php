@if($school->password_display){{--●●塾、教室--}}
<table>
    <thead>
    <tr>
        <th>教室名</th>
        <th>生徒番号</th>
        <th>名前</th>
        <th>登録日</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->grade }}</td>
            <td>{{ $user->attendance_no }}</td>
            <td>{{ $user->name }}</td>
            <td>{{date('Y/m/d', strtotime($user->created_at))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@else{{--■■小学校--}}
<table>
    <thead>
    <tr>
        <th>学年</th>
        <th>組</th>
        <th>出席番号</th>
        <th>生徒名</th>
        <th>登録日</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->grade }}</td>
            <td>{{ $user->kumi }}</td>
            <td>{{ $user->attendance_no }}</td>
            <td>{{ $user->name }}</td>
            <td>{{date('Y/m/d', strtotime($user->created_at))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

