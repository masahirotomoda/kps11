@extends('layouts.base')

@section('js')
    <script src="/js/dateedit.js"></script>
    <script src="/js/checkall-dim.js"></script>
    <script src="/js/validation_modal.js"></script>
    <script src="/js/btn_pdfprint_contest.js"></script>
    <script src="/js/btn_all_reset_contest.js"></script>
@endsection

@section('content')
{{Form::open(['url' => '/school/contest-scorelist-search', 'method' => 'post','id'=>'search_form'])}}
<div class="menuSpaces smallBox">
    <div class="menuSpacesCell spaceAll">
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('昨日')">昨日</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('今日')">今日</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('AM')">今日の午前</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="dateEditFunction('PM')">今日の午後</button>
        <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
            @slot('word', '表彰状の印刷方法')
            @slot('image_name', 'contest_school1')
        @endcomponent
        </div>
    </div>
</div>
<div class="menuSpaces underFix">
    @include('layouts.include.calendar')
    <div class="menuSpacesCell">
        {{ Form::text('course_name', $courseName, ['type' => 'text' , 'class' => 'form-control','id' => 'course_name', 'maxlength' => 40 ,'placeholder' => 'コース名の一部']) }}
    </div>
    <div class="menuSpacesCell spaceAll contestList wraps">
    @if($school->password_display){{--●●塾、教室--}}
        <div class="school_grade_select">
            {{ Form::text('grade',  $grade, ['class' => 'form-select','id' => 'grade','placeholder' => '教室名の一部']) }}
		</div>
    @else{{--■■小学校--}}
        <div class="school_grade_select">
            {{ Form::select('grade', config('configrations.SCHOOL_GRADE'), $grade, ['class' => 'form-select','id' => 'grade','placeholder' => '学年']) }}
            <label for="grade">学年:</label>
		</div>
        <div class="school_kumi_select">
            {{ Form::select('kumi', config('configrations.SCHOOL_KUMI'), $kumi, ['class' => 'form-select','id' => 'kumi','placeholder' => '組']) }}
            <label for="kumi">組:</label>
		</div>
    @endif
        {{ Form::text('name', $name, ['type' => 'text' , 'class' => 'form-control','id' => 'name','maxlength' => 20 ,'placeholder' => '生徒名の一部']) }}
        <div class="menuSpacesOneCheck">
            <input type="checkbox" name="sort_score_desc" value="selected" class="form-check-input" id="sort_score_desc" {{$sort_score_desc}}>
            <label class="form-check-label" for="sort_score_desc">スコアを高い順</label>
        </div>
        @include('layouts.include.searchbtn')
        <button class="btn btn-outline-primary" id="all_reset_btn" type="button" onclick="location.href='./contest-scorelist'"><small id="cancel_btn">操作のキャンセル</small></button>
    </div>
    <div class="contestBtnOuter">
        <div class="menuSpacesCell">
            {{ Form::close() }}
            {{Form::open(['url' => '/contest/transcript-print-pdf', 'method' => 'post','id'=>'pdf'])}}
            <button name="pdf_basic" id="pdf_basic" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">記録証PDF（基本）</button>
            <button name="pdf_yusyou" id="pdf_yusyou" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">優勝</button>
            <button name="pdf_junyusyou" id="pdf_junyusyou" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">準優勝</button>
            <button name="pdf_kin" id="pdf_kin" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">金メダル</button>
            <button name="pdf_gin" id="pdf_gin" type="submit" class="btn btn-outline-primary btn-sm pdfBtn" onclick="printpdf('pdf_gin')">銀メダル</button>
            <button name="pdf_dou" id="pdf_dou" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">銅メダル</button>
            <button name="pdf_ganbatta" id="pdf_ganbatta" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">がんばったで賞</button>
        </div>
        <div class="menuSpacesCell">
            <button name="pdf_1" id="pdf_1" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">１位</button>
            <button name="pdf_2" id="pdf_2" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">２位</button>
            <button name="pdf_3" id="pdf_3" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">３位</button>
            <button name="pdf_4" id="pdf_4" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">４位</button>
            <button name="pdf_5" id="pdf_5" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">５位</button>
            <button name="pdf_6" id="pdf_6" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">６位</button>
            <button name="pdf_7" id="pdf_7" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">７位</button>
            <button name="pdf_8" id="pdf_8" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">８位</button>
            <button name="pdf_9" id="pdf_9" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">９位</button>
            <button name="pdf_10" id="pdf_10" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">１０位</button>
        </div>
    </div>
</div>
<div class="infotext">
        <p class="largeTexts"><span>全スコア履歴数</span><strong>{{$scoreHistory_num}}</strong>件</p>
        @if($scoreHistory_num !==  $scoreHistory->total())
        <p class="largeTexts"><span>検索後のスコア履歴数</span><strong>{{$scoreHistory->total()}}</strong>件</p>
        @endif
        <p><small>※スコアは削除できません。</small></p>
</div>
@include('layouts.include.alertmessage')
@include('layouts.include.pagination')
<div class="menuSpaces topMinusBox">
    {{ Form::submit('データの一括ダウンロード(Excel)', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm rightBox','form'=>'search_form']) }}
</div>
@if($school->password_display){{--●●塾、教室--}}
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th>
                <input type="checkbox" id="checkAll" value="1">
                @component('components.tooltip')
                    @slot('word', '印刷')
                    @slot('message', '表示データをまとめて印刷する時はここをチェック。個別印刷は、個別にチェック')
                @endcomponent
            </th>
            <th scope="col">教室名</th>
            <th scope="col">生徒番号</th>
            <th scope="col">名前</th>
            <th scope="col">コース名</th>
            <th scope="col">スコア</th>
            <th scope="col">日時</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($scoreHistory as $sh)
        <tr>
            <td><input type="checkbox" class="checks" name="checks[]" value="{{ $sh->id }}" id="{{ 'pdf'.$sh->id }}">
            <td>{{ $sh->grade }}</td>
            <td>{{ $sh->attendance_no }}</td>
            <td>{{ $sh->name }}</td>
            <td>{{ $sh->course_name }}</td>
            <td>{{ $sh->point }}</td>
            <td>{{date_format($sh->created_at,'Y/m/d　　H:i')}} </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else{{--■■小学校--}}
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th>
                <input type="checkbox" id="checkAll" value="1">
                @component('components.tooltip')
                    @slot('word', '印刷')
                    @slot('message', '表示データをまとめて印刷する時はここをチェック。個別印刷は、個別にチェック')
                @endcomponent
            </th>
            <th scope="col">学年</th>
            <th scope="col">組</th>
            <th scope="col">出席No</th>
            <th scope="col">生徒名</th>
            <th scope="col">コース名</th>
            <th scope="col">スコア</th>
            <th scope="col">日時</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($scoreHistory as $sh)
        <tr>
            <td><input type="checkbox" class="checks" name="checks[]" value="{{ $sh->id }}" id="{{ 'pdf'.$sh->id }}">
            <td>{{ $sh->grade }}</td>
            <td>{{ $sh->kumi }}</td>
            <td>{{ $sh->attendance_no }}</td>
            <td>{{ $sh->name }}</td>
            <td>{{ $sh->course_name }}</td>
            <td>{{ $sh->point }}</td>
            <td>{{date_format($sh->created_at,'Y/m/d　　H:i')}} </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif
{{ Form::close() }}
@endsection