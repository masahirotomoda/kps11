@extends('layouts.contest')
@section('content')
@section('title')
タイピングスコア一覧（デモサイト） | ナレッジタイピング for コンテスト
@endsection
@section('description')
塾や学校で開催するタイピングコンテスト専用サイトで、先生と生徒の手間を一切取り除いたプラットフォームです。専用URLを生徒に伝えるだけで開催できます。
@endsection
@section('css')
<link href="/css/style.css?v=20240118" rel="stylesheet">
@endsection

@section('js')
@endsection

@section('content')
<main class="py-4">
    <div class="container teacher-top">
        <div class="backBtn"><button type="button" class="btn btn-primary" onclick="location.href='/{{$contest_account}}/contest/'">もどる</button></div>
        <h2 class="ktTitle">【コンテスト】スコア一覧（デモサイト）</h2>
        <div class="pageLinkBlock">
            <div class="mainBlock">
                <div class="menuSpaces underFix">
                    {{--カレンダー--}}
                    <div class="menuSpacesCell mscLong">
                        <div class="plusText">
                            <span class="inputText">開始日時</span>
                            <input type="date" class="form-control" id="start_date">
                            <input type="time" class="form-control" id="inputMDEx1">
                        </div>
                        <div class="plusText">
                            <span class="inputText">終了日時</span>
                            <input type="date" class="form-control" id="end_date">
                            <input type="time" class="form-control" id="inputMDEx2">
                        </div>
                    </div>
                    <div class="menuSpacesCell">
                        <input type="text" placeholder="コース名の一部" class="form-control" maxlength="40" id="kensaku_course">
                    </div>
                    <div class="menuSpacesCell spaceAll wraps">
                        <input type="text" placeholder="学年" class="form-control" maxlength="5" id="kensaku_grade">
                        <input type="text" placeholder="クラス" class="form-control" maxlength="5" id="kensaku_class">
                        <input type="text" placeholder="名前の一部" class="form-control" maxlength="15" id="kensaku_name">
                        <div class="menuSpacesOneCheck">
                            <input type="checkbox" class="form-check-input" id="sort">
                            <label class="form-check-label" for="sort">スコアを高い順</label>
                        </div>
                        <button class="btn btn-outline-primary btn-sm" name="search" id="SearchOrEditMode">検索(デモに付き無効)</button>
                    </div>
                    <div class="contestBtnOuter">
                        <div class="menuSpacesCell">
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_basic.pdf')">記録証PDF（基本）</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_yusyou.pdf')">優勝</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_junyusyou.pdf')">準優勝</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_kin.pdf')">金メダル</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_gin.pdf')">銀メダル</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_dou.pdf')">銅メダル</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_ganbatta.pdf')">がんばったで賞</button>
                        </div>
                        <div class="menuSpacesCell">
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_1.pdf')">１位</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_2.pdf')">２位</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_3.pdf')">３位</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_4.pdf')">４位</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_5.pdf')">５位</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_6.pdf')">６位</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_7.pdf')">７位</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_8.pdf')">８位</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_9.pdf')">９位</button>
                            <button class="btn btn-outline-primary btn-sm pdfBtn" onclick="window.open('/pdf/certificate_10.pdf')">１０位</button>
                        </div>
                    </div>
                </div>
                <div class="menuSpaces centers">
                    <button class="btn btn-secondary btn-sm rightBox" onclick="window.open('/pdf/knowledge_typing_scorelist.xlsx')">データの一括ダウンロード(Excel)</button>
                </div>
                <table id="mainTable">
                    <thead>
                        <tr>
                            <th scope="col">印刷</th>
                            <th scope="col">学年</th>
                            <th scope="col">組</th>
                            <th scope="col">出席No</th>
                            <th scope="col">生徒名</th>
                            <th scope="col">コース名</th>
                            <th scope="col">スコア</th>
                            <th scope="col">日時</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk1">
                            <td>3年</td>
                            <td>1組</td>
                            <td>1</td>
                            <td>徳川　秀忠</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>98</td>
                            <td>2024年1月20日 10:24</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk2">
                            <td>3年</td>
                            <td>1組</td>
                            <td>2</td>
                            <td>毛利　元就</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>77</td>
                            <td>2024年1月20日 10:25</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk3">
                            <td>3年</td>
                            <td>1組</td>
                            <td>3</td>
                            <td>宮本　武蔵</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>43</td>
                            <td>2024年1月20日 10:26</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk4">
                            <td>3年</td>
                            <td>1組</td>
                            <td>4</td>
                            <td>浅井　長政</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>68</td>
                            <td>2024年1月20日 10:24</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk5">
                            <td>3年</td>
                            <td>1組</td>
                            <td>5</td>
                            <td>朝倉　義景</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>98</td>
                            <td>2024年1月20日 10:24</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk6">
                            <td>3年</td>
                            <td>1組</td>
                            <td>6</td>
                            <td>小田　信長</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>99</td>
                            <td>2024年1月20日 10:27</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk7">
                            <td>3年</td>
                            <td>1組</td>
                            <td>7</td>
                            <td>徳川　家康</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>100</td>
                            <td>2024年1月20日 10:25</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk8">
                            <td>3年</td>
                            <td>1組</td>
                            <td>8</td>
                            <td>豊臣　秀吉</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>88</td>
                            <td>2024年1月20日 10:26</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk9">
                            <td>3年</td>
                            <td>1組</td>
                            <td>9</td>
                            <td>武田　信玄</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>68</td>
                            <td>2024年1月20日 10:25</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk10">
                            <td>3年</td>
                            <td>1組</td>
                            <td>10</td>
                            <td>藤原　頼朝</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>76</td>
                            <td>2024年1月20日 10:24</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk11">
                            <td>3年</td>
                            <td>2組</td>
                            <td>10</td>
                            <td>明智 光秀</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>100</td>
                            <td>2024年1月20日 10:27</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk12">
                            <td>3年</td>
                            <td>2組</td>
                            <td>11</td>
                            <td>斎藤　道三</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>89</td>
                            <td>2024年1月20日 10:26</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk13">
                            <td>3年</td>
                            <td>2組</td>
                            <td>13</td>
                            <td>伊達　政宗</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>99</td>
                            <td>2024年1月20日 10:25</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk14">
                            <td>3年</td>
                            <td>2組</td>
                            <td>14</td>
                            <td>平　清盛</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>91</td>
                            <td>2024年1月20日 10:26</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk15">
                            <td>3年</td>
                            <td>2組</td>
                            <td>15</td>
                            <td>上杉　謙信</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>100</td>
                            <td>2024年1月20日 10:24</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk16">
                            <td>3年</td>
                            <td>2組</td>
                            <td>16</td>
                            <td>源　義経</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>48</td>
                            <td>2024年1月20日 10:27</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk17">
                            <td>3年</td>
                            <td>2組</td>
                            <td>17</td>
                            <td>足利　尊氏</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>67</td>
                            <td>2024年1月20日 10:25</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk18">
                            <td>3年</td>
                            <td>2組</td>
                            <td>18</td>
                            <td>黒田　官兵衛</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>100</td>
                            <td>2024年1月20日 10:26</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk19">
                            <td>3年</td>
                            <td>2組</td>
                            <td>19</td>
                            <td>前田　利家</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>89</td>
                            <td>2024年1月20日 10:25</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk20">
                            <td>3年</td>
                            <td>2組</td>
                            <td>20</td>
                            <td>直江　兼続</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>100</td>
                            <td>2024年1月20日 10:24</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="pdf_chk21">
                            <td>3年</td>
                            <td>2組</td>
                            <td>21</td>
                            <td>北条　時宗</td>
                            <td>【3年生】タイピングコース（ことわざ編）</td>
                            <td>53</td>
                            <td>2024年1月20日 10:25</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection