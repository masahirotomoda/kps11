@extends('layouts.contest')

@section('title')
コンテスト本番コース | ナレッジタイピングforコンテスト
@endsection
@section('description')
コンテスト本番コースです。このURLをブックマークに登録しないでください。
@endsection
@section('css')
<link rel="stylesheet" href="/css/typing.css?v=20240123">
<link rel="stylesheet" href="/css/style.css?v=20240122">
@endsection

@section('js')
<script>
  let typeWordsAll = @json($wordData);
  let courseData = @json($course);
  let courseList = @json($coursesArray);
  let sound = @json($soundData);
  let alphabet = @json($alphabetData);
  const routeLink = "{{asset('/')}}";
  const isFree = true;
  const scoretype = 'contest';
  const isContest = true;
  const honban = true;
</script>
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/typing-data.js?v=20240123"></script>
<script src="/js/typing-common.js?v=20240123?v=20240123"></script>
<script src="/js/typing-contest.js?v=20231216"></script>
<script src="/js/typing-contest-record.js"></script>
<script src="/js/typing-score.js"></script>
<script src="/js/typing-sound.js"></script>
<script src="/js/keyboard.js"></script>
<script src="/js/finger.js"></script>
<script src="/js/romanizer.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/common.js?v=20240123"></script>
<script src="/js/prevnextcourse.js?v=20240123"></script>
@endsection

@section('content')
@include('layouts.include.typingcoursenamecontest')
<section class="l-main acting">
  <div class="l-main__container freel-main">
    <div class="order-1">
      <div class="c-table w-4/4">
        <div class="c-table__container">
          <div class="c-table__container__space">
            <table class="contestTable">
              <thead>
                <tr>
                  <th colspan="2" scope="col">登録情報</th>
                </tr>
              </thead>
              @if($school->password_display){{--●●塾、教室--}}
              <tbody>
                <tr>
                  <td>ｺﾝﾃｽﾄ名</td>
                  <td>{{$school->name}}</td>
                </tr>
                <tr>
                  <td>教室名</td>
                  <td>{{$student->grade}}</td>
                </tr>
                <tr>
                  <td>生徒番号</td>
                  <td>{{$student->attendance_no}}</td>
                </tr>
                <tr>
                  <td>名前</td>
                  <td>{{$student->name}}</td>
                </tr>
                <tr>
                  <td colspan="2">
                    <div class="underBtn"><button class="btn btn-primary btnRankTable" type="button" data-toggle="modal" data-target="#modal-reset" id="contest_typingtest_caution_btn">もう一度、コースをえらぶ</button></div>
                  </td>
                </tr>
              </tbody>
              @else{{--●●小学校--}}
              <tbody>
                <tr>
                  <td>学校</td>
                  <td>{{$school->name}}</td>
                </tr>
                <tr>
                  <td>学年</td>
                  <td>{{$student->grade}}</td>
                </tr>
                <tr>
                  <td>クラス</td>
                  <td>{{$student->kumi}}</td>
                </tr>
                <tr>
                  <td>出席番号</td>
                  <td>{{$student->attendance_no}}</td>
                </tr>
                <tr>
                  <td>名前</td>
                  <td>{{$student->name}}</td>
                </tr>
                <tr>
                  <td colspan="2">
                    <div class="underBtn"><button class="btn btn-primary btnRankTable" type="button" data-toggle="modal" data-target="#modal-reset" id="contest_typingtest_caution_btn">もう一度、コースをえらぶ</button></div>
                  </td>
                </tr>
              </tbody>
              @endif
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="order-0 typingOrder">
      <div class="p-typingscreen" id="p-typingscreen__startscreen">
        <img id="btnarrow" src="/img/btnarrow.png" class="hidden">
        <div id="p-typingscreen__startscreen__upper__container">

          <div class="p-typingscreen__startscreen__upper__container_Inner">
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__roman" id="p-typingscreen__startscreen__roman" />
              <label for="p-typingscreen__startscreen__roman">ローマ字あり
              </label>
            </div>
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__roman" id="p-typingscreen__startscreen__noroman" />
              <label for="p-typingscreen__startscreen__noroman">なし</label>
            </div>
          </div>

          <div class="p-typingscreen__startscreen__upper__container_Inner soundRadio hidden">
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__sound" />
              <label for="p-typingscreen__startscreen__sound">音あり</label>
            </div>
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__nosound" />
              <label for="p-typingscreen__startscreen__nosound">なし</label>
            </div>
          </div>

          <div class="p-typingscreen__startscreen__upper__container_Inner romanRadio">
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__upper" value="{{$course->course_level}}" />
              <label for="p-typingscreen__startscreen__upper">大文字</label>
            </div>
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__lower" />
              <label for="p-typingscreen__startscreen__lower">ローマ字小文字</label>
            </div>
          </div>

        </div>
        <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingscreen__start" onclick="startBtnClick()">はじめる</button>

        <div class="p-typingscreen__count hidden" id="p-typingscreen__count"></div>
        <div class="p-typingscreen__kana">&nbsp;</div>
        <div class="p-typingscreen__mana">&nbsp;</div>
        <div class="p-typingscreen__romaji">
          <div class="p-typingscreen__romaji__typed"></div>
          <div class="p-typingscreen__romaji__untyped">&nbsp;</div>
        </div>
      </div>
      <div class="p-typingscreen hidden" id="p-typingscreen">
        <div class="p-typingscreen__kana" id="p-kana"></div>{{-- お題(仮名) --}}
        <div class="p-typingscreen__mana" id="p-mana"></div>{{-- お題(真名) --}}
        <div class="p-typingscreen__romaji">
          <div class="p-typingscreen__romaji__show" id="p-typing-show"></div>
          <span class="p-typingscreen__romaji__typed hidden" id="p-typed"></span><span class="p-typingscreen__romaji__untyped hidden" id="p-untyped"></span>
        </div>
        <div class="p-typingscreen__successtype" id="p-successtype"></div>
        <div class="p-typingscreen__misstype" id="p-misstype"></div>
      </div>
      <div class="p-typingresult hidden" id="p-typingresult">
        <!-- 結果表示画面 -->
        <div class="p-typingresult__bg"></div>
        <div class="p-typingresult__card">
          <h3>結果</h3>
          <ul>
            <li><span>スコア</span>
              <div id="p-typingresult--score"></div><span>点</span><span id="perfect"></span>
            </li>
            <li><span>時間</span>
              <div id="p-typingresult--time"></div><span>秒</span>
            </li>
            <li><span>打った数</span>
              <div id="p-typingresult--all"></div><span>回</span>
            </li>
            <li><span>間違った数</span>
              <div id="p-typingresult--miss"></div><span>回</span>
            </li>
          </ul>
          <form method="POST" action="{{url('/')}}/{{$contest_account}}/contest/store">
            @csrf
            <input type="hidden" name="school_int_id" value="{{$school->id}}">
            <input type="hidden" name="student_id" value="{{$student->id}}">
            <input type="hidden" name="typingresult--score" value="">
            <input type="hidden" name="typingresult--time" value="">
            <input type="hidden" name="typingresult--all" value="">
            <input type="hidden" name="typingresult--miss" value="">
            <input type="hidden" name="course_id" value="">
            <input type="hidden" name="course_name" value="">
            <input type="hidden" name="contest_account" value="{{$contest_account}}">
            <h4 class="mb-4"><strong>かならず、↓のボタンをおしてください。</strong></h4>
            @if($school->id ==config('configrations.DEMO_CONTEST_INFO.practicemode_school_id'))
            <button type="button" id="typing_store_close" class="c-button--sm" data-toggle="modal" data-target="#modal-reset3" data-dismiss="modal" onclick="location.href='/{{$contest_account}}/contest/'">スコアを保存してとじる</button>
            @else
            <button type="submit" id="typing_store_close" class="c-button--sm" data-toggle="modal" data-target="#modal-reset3" data-dismiss="modal">スコアを保存してとじる</button>
            @endif
          </form>
        </div>
      </div>
      @include('layouts.include.keyboard')
      @include('layouts.include.finger')
      <div class="underBtn leftRightBtn">
        <button type="button" class="prevCourse hidden" id="mae">前のコース</button>
        <button type="button" class="nextCourse hidden" id="tugi">次のコース</button>
      </div>
      <button type="button" class="btn btn-primary stopBtn hidden" id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
    </div>
  </div>
  {{-- (2)モーダル（名前の確認）--}}
  <div class="modal fade" id="modal-reset" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5>名前は正しいですか？<br>まちがっていたら、やり直そう。</h5>
        </div>
        <div>
          <div class="modalCell">
            @if($school->password_display){{--●●塾、教室--}}
            <ul class="modalCellList plusUnderBorder">
              <li><span class="nameTitle">ｺﾝﾃｽﾄ名</span><span class="nameInput">{{$school->name}}</span></li>
              <li><span class="nameTitle">教室名</span><span class="nameInput">{{$student->grade}}</span></li>
              <li><span class="nameTitle">生徒番号</span><span class="nameInput">{{$student->attendance_no}}</span></li>
              <li><span class="nameTitle">名前</span><span class="nameInput">{{$student->name}}</span></li>
            </ul>
            @else
            <ul class="modalCellList plusUnderBorder">
              <li><span class="nameTitle">学校名</span><span class="nameInput">{{$school->name}}</span></li>
              <li><span class="nameTitle">学年</span><span class="nameInput">{{$student->grade}}</span></li>
              <li><span class="nameTitle">クラス</span><span class="nameInput">{{$student->kumi}}</span></li>
              <li><span class="nameTitle">出席番号</span><span class="nameInput">{{$student->attendance_no}}</span></li>
              <li><span class="nameTitle">名前</span><span class="nameInput">{{$student->name}}</span></li>
            </ul>
            @endif
          </div>
        </div>
        <div class="modal-footer btnCours">
          <button type="button" class="btn btn-secondary" id="userinfo_cxl_btn" onclick="location.href='/{{$contest_account}}/contest/'">やりなおす</button>
          <button type="button" class="btn btn-danger" id="contest_typingtest_nextbtn" data-toggle="modal" data-target="#modal-reset2" data-dismiss="modal">次にすすむ</button>

        </div>
      </div>
    </div>
  </div>{{--//(2)モーダルここまで--}}
  {{-- (3)モーダル（コースの確認）--}}
  <div class="modal fade" id="modal-reset2" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5>コースをえらぼう。</h5>
        </div>
        <div class="modalCell">
          {{--コースリスト--}}
          <table class="modalCourse">
            <thead>
              <tr>
                <th colspan="2" scope="col">【本番】コンテストコース</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($tabCourseList as $tabCourse)
              <tr>
                <td name="tab_course_index">{{$loop->index + 1}}</td>
                <td><a href="javascript:getTabCourseLink('{{$loop->index}}');">{{$tabCourse->course_name}}</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="modalCell startTexts">
          <p>スタートしたら「はじめる」ボタンで始まるよ！</p>
        </div>
        <div class="modal-footer btnCours">
          <button type="button" class="btn btn-secondary" id="start_cxl_btn" onclick="location.href='/{{$contest_account}}/contest/'">やりなおす</button>
          <button type="button" class="btn btn-danger" id="contest_typingtest_ok" data-dismiss="modal">本番スタート</button>
        </div>
      </div>
    </div>
  </div>{{--//(3)モーダルここまで--}}
  {{-- (4)モーダル（結果モーダル閉じて、TOPページリダイレクトする間の処理待ちメッセージ）--}}
  <div class="modal fade" id="modal-reset3" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5>保存中・・・</h5>
        </div>
      </div>
    </div>
  </div>{{--モーダルここまで--}}
  @endsection