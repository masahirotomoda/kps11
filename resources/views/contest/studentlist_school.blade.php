@extends('layouts.base')

@section('js')
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset_contest.js"></script>
@endsection

@section('content')
<div class="menuSpaces underFix">
{{ Form::open(['url' => '/school/contest-studentlist-search', 'method' => 'post','id' => 'search_form']) }}
@include('layouts.include.alertmessage')

    <div class="menuSpacesCell spaceAll contestList wraps">
        @include('layouts.include.gradekumiselect')
        <div>
            <button class="btn btn-outline-primary searchIcon" type="submit" name="search">検索</button>
            <button class="btn btn-outline-primary" id="all_btn" type="button" onclick="location.href='./contest-studentlist'">すべて表示</button>
        </div>
    </div>
</div>
<div class="infotext">
        <p class="largeTexts"><span>全生徒数</span><strong>{{$student_num}}</strong>人</p>
        @if($student_num !==  $users->total())
        <p class="largeTexts"><span>検索後の生徒数</span><strong>{{$users->total()}}</strong>人</p>
        @endif
        <p><small>※生徒削除はできません。</small></p>
</div>
@include('layouts.include.pagination')
<div class="menuSpaces bottomMin">
    {{ Form::submit('データの一括ダウンロード(Excel)', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm rightBox','form'=>'search_form']) }}
</div>

@if($school->password_display){{--●●塾、教室--}}
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th scope="col">教室名</th>
            <th scope="col">生徒番号</th>
            <th scope="col">名前</th>
            <th scope="col">登録日</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{$user->grade}}</td>
            <td>{{$user->attendance_no}}</td>
            <td>{{$user->name}}</td>
            <td>{{date('Y/m/d', strtotime($user->created_at))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@else{{--■■小学校--}}
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th scope="col">学年</th>
            <th scope="col">組</th>
            <th scope="col">出席No</th>
            <th scope="col">生徒名</th>
            <th scope="col">登録日</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{$user->grade}}</td>
            <td>{{$user->kumi}}</td>
            <td>{{$user->attendance_no}}</td>
            <td>{{$user->name}}</td>
            <td>{{date('Y/m/d', strtotime($user->created_at))}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif
@endsection
