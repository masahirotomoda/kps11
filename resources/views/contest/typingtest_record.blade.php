@extends('layouts.contest')

@section('title')
コンテスト本番コース | ナレッジタイピングforコンテスト
@endsection
@section('description')
コンテスト本番コースです。このURLをブックマークに登録しないでください。
@endsection
@section('css')
<link rel="stylesheet" href="/css/typing.css?v=20240123">
<link rel="stylesheet" href="/css/style.css?v=20240122">
@endsection
@section('js')
<script>
  let courseData = @json($course);
  let typeWordsAll = @json($wordData);
  let courseList = @json($coursesArray);
  const routeLink = '/';
  const isFree = true;
  const isContest = true;
  const honban = true;
</script>
<script src="/js/typing-common.js?v=20240123?v=20240123"></script>
<script src="/js/typing-test-contest.js"></script>
<script src="/js/typing-contest-record.js?"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/common.js?v=20240123"></script>
@endsection
@section('content')
@include('layouts.include.typingcoursenamecontest')
<section class="l-main acting">
  <div class="l-main__container freel-main">
    <div class="order-1">
      <div class="c-table">
        <div class="c-table__container">
          <div class="c-table__container__space">
            <!--コースリスト-->
            <table class="contestTable">
              <thead>
                <tr>
                  <th colspan="2" scope="col">登録情報</th>
                </tr>
              </thead>
              @if($school->password_display){{--●●塾、教室--}}
              <tbody>
                <tr>
                  <td>ｺﾝﾃｽﾄ名</td>
                  <td>{{$school->name}}</td>
                </tr>
                <tr>
                  <td>教室名</td>
                  <td>{{$student->grade}}</td>
                </tr>
                <tr>
                  <td>生徒番号</td>
                  <td>{{$student->attendance_no}}</td>
                </tr>
                <tr>
                  <td>名前</td>
                  <td>{{$student->name}}</td>
                </tr>
                <tr>
                  <td colspan="2">
                    <div class="underBtn"><button class="btn btn-primary btnRankTable" type="button" data-toggle="modal" data-target="#modal-reset" id="contest_typingtest_caution_btn">もう一度、コースをえらぶ</button></div>
                  </td>
                </tr>
              </tbody>
              @else{{--■■小学校--}}
              <tbody>
                <tr>
                  <td>学校</td>
                  <td>{{$school->name}}</td>
                </tr>
                <tr>
                  <td>学年</td>
                  <td>{{$student->grade}}</td>
                </tr>
                <tr>
                  <td>クラス</td>
                  <td>{{$student->kumi}}</td>
                </tr>
                <tr>
                  <td>出席番号</td>
                  <td>{{$student->attendance_no}}</td>
                </tr>
                <tr>
                  <td>名前</td>
                  <td>{{$student->name}}</td>
                </tr>
                <tr>
                  <td colspan="2">
                    <div class="underBtn"><button class="btn btn-primary btnRankTable" type="button" data-toggle="modal" data-target="#modal-reset" id="contest_typingtest_caution_btn">もう一度、コースをえらぶ</button></div>
                  </td>
                </tr>
              </tbody>
              @endif
            </table>
          </div>
        </div>
      </div>
    </div><!--/order-1"-->
    <div class="order-0">
      <div class="p-typingtestscreenOut">
        <div class="p-typingtestscreen" id="p-typingtestscreen__startscreen">
          <img id="btnarrow" src="/img/btnarrow.png" class="hidden">

          <div class="p-typingtestscreen__flex">
            <div class="p-typingtestscreen__typenumber">入力： 0文字</div>
            <div class="p-typingtestscreen__time">残り時間： <span id="p-initial-time"></span></div>
          </div>
          <div class="p-typingtestscreen__typesbg">
            <div id="p-typingscreen__startscreen__optionbtn__container" class="romanRadio">
              <div class="radioOut">
                <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__upper" />
                <label for="p-typingscreen__startscreen__upper">大文字</label>
              </div>
              <div class="radioOut">
                <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__lower" checked />
                <label for="p-typingscreen__startscreen__lower">ローマ字小文字</label>
              </div>
            </div>
            <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingtestscreen__start" onclick="startTypingFunc()">はじめる</button>
            <div class="p-typingscreen__count hidden" id="p-typingscreen__count"></div>
            <div class="p-typingtestscreen__typesfield"></div>
          </div>
        </div>
        <div class="p-typingtestscreen hidden" id="p-typingtestscreen">
          <div class="p-typingtestscreen__flex">
            <div class="p-typingtestscreen__typenumber">入力： <span id="p-typenumber"></span>文字</div>
            <div class="p-typingtestscreen__time">残り時間： <span id="p-time"></span></div>
          </div>
          <div class="p-typingtestscreen__typesbg">
            <div class="p-typingtestscreen__typesfield" id="p-typefield"></div>
          </div>
        </div>
        <div class="p-typingresult hidden" id="p-typingresult">
          {{-- 結果表示画面--}}
          <div class="p-typingresult__bg"></div>
          <div class="p-typingresult__card">
            <h3>結果</h3>
            <ul>
              <li><span>スコア</span>
                <div id="p-typingresult--score"></div><span>文字</span><span id="perfect"></span>
              </li>
              <li><span>残り時間</span>
                <div id="p-typingresult--time"></div>
              </li>
            </ul>
            <div class="p-typingtestscreen__typesbg">
              <div class="p-typingtestresultscreen__typesfield" id="p-typeresultfield"></div>
            </div>

            <form method="POST" action="{{url('/')}}/{{$contest_account}}/contest/store">
              @csrf
              <input type="hidden" name="school_int_id" value="{{$school->id}}">
              <input type="hidden" name="student_id" value="{{$student->id}}">
              <input type="hidden" name="typingresult--score" value="">
              <input type="hidden" name="typingresult--time" value="">
              <input type="hidden" name="course_id" value="">
              <input type="hidden" name="course_name" value="">
              <input type="hidden" name="contest_account" value="{{$contest_account}}">
              <h4 class="mb-4"><strong>かならず、↓のボタンをおしてください。</strong></h4>
              @if($school->id ==config('configrations.DEMO_CONTEST_INFO.testmode_school_id'))
              <button type="button" id="typing_store_close" class="c-button--sm" data-toggle="modal" data-target="#modal-reset3" data-dismiss="modal" onclick="location.href='/{{$contest_account}}/contest/'">スコアを保存してとじる</button>
              @else
              <button type="submit" id="typing_store_close" class="c-button--sm" data-toggle="modal" data-target="#modal-reset3" data-dismiss="modal">スコアを保存してとじる</button>
              @endif
            </form>
          </div>{{--//結果モーダルここまで--}}
        </div>
        <div class="underBtn plusUnder">
          <div class="underBtnInner">
            <button type="button" class="prevCourse btn btn-primary hidden" id="mae">前のコース</button>
            <button type="button" class="btn btn-primary hidden" id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
            <button type="button" class="nextCourse btn btn-primary hidden" id="tugi">次のコース</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
{{-- (2)モーダル（名前の確認）--}}
<div class="modal fade" id="modal-reset" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5>名前は正しいですか？<br>まちがっていたら、やり直そう。</h5>
      </div>
      <div>
        <div class="modalCell">
        @if($school->password_display){{--●●塾、教室--}}
          <ul class="modalCellList plusUnderBorder">
            <li><span class="nameTitle">ｺﾝﾃｽﾄ名</span><span class="nameInput">{{$school->name}}</span></li>
            <li><span class="nameTitle">教室名</span><span class="nameInput">{{$student->grade}}</span></li>
            <li><span class="nameTitle">生徒番号</span><span class="nameInput">{{$student->attendance_no}}</span></li>
            <li><span class="nameTitle">名前</span><span class="nameInput">{{$student->name}}</span></li>
          </ul>
          @else
          <ul class="modalCellList plusUnderBorder">
            <li><span class="nameTitle">学校名</span><span class="nameInput">{{$school->name}}</span></li>
            <li><span class="nameTitle">学年</span><span class="nameInput">{{$student->grade}}</span></li>
            <li><span class="nameTitle">クラス</span><span class="nameInput">{{$student->kumi}}</span></li>
            <li><span class="nameTitle">出席番号</span><span class="nameInput">{{$student->attendance_no}}</span></li>
            <li><span class="nameTitle">名前</span><span class="nameInput">{{$student->name}}</span></li>
          </ul>
          @endif
        </div>
      </div>
      <div class="modal-footer btnCours">
        <button type="button" class="btn btn-secondary" id="userinfo_cxl_btn" onclick="location.href='/{{$contest_account}}/contest/'">やりなおす</button>
        <button type="button" class="btn btn-danger" id="contest_typingtest_nextbtn" data-toggle="modal" data-target="#modal-reset2" data-dismiss="modal">次にすすむ</button>

      </div>
    </div>
  </div>
</div>{{--//(2)モーダルここまで--}}
{{-- (3)モーダル（コースの確認）--}}
<div class="modal fade" id="modal-reset2" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5>コースをえらぼう。</h5>
      </div>
      <div class="modalCell">
        {{--コースリスト--}}
        <table class="modalCourse">
          <tbody>
            @foreach ($tabCourseList as $tabCourse)
            <tr>
              <td name="tab_course_index">{{$loop->index + 1}}</a></td>
              <td><a href="javascript:getTabCourseLink('{{$loop->index}}');">{{$tabCourse->course_name}}</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="modalCell startTexts">
        <p>スタートしたら「はじめる」ボタンで始まるよ！</p>
      </div>
      <div class="modal-footer btnCours">
        <button type="button" class="btn btn-secondary" id="start_cxl_btn" onclick="location.href='/{{$contest_account}}/contest/'">やりなおす</button>
        <button type="button" class="btn btn-danger" id="contest_typingtest_ok" data-dismiss="modal">本番スタート</button>
      </div>
    </div>
  </div>
</div>{{--//(3)モーダルここまで--}}
{{-- (4)モーダル（結果モーダル閉じて、TOPページリダイレクトする間の処理待ちメッセージ）--}}
<div class="modal fade" id="modal-reset3" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5>保存中・・・</h5>
      </div>
    </div>
  </div>
</div>{{--モーダルここまで--}}
@endsection