@extends('layouts.base')

@section('js')
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
<script src="/js/system/btn_excelexport.js"></script>
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/del_modal.js"></script>
@endsection

@section('content')
<div class="menuSpaces bottomMin">
{{ Form::open(['url' => '/contest-scorelist-search', 'method' => 'post','id' => 'search_form']) }}
@include('layouts.include.alertmessage')
    <div class="menuSpacesCell">
        {{ Form::text('school_id', $school_id, ['type' => 'text' , 'class' => 'form-control','id' => 'school_id', 'maxlength' => 8 ,'placeholder' => '学校ID']) }}
        {{ Form::text('school_name', $school_name, ['type' => 'text' , 'class' => 'form-control','id' => 'school_name', 'maxlength' => 15 ,'placeholder' => '学校名の一部']) }}
        {{Form::text('school_grade',  $grade_select, ['class' => 'form-select','id' => 'school_grade','placeholder' => '学年or教室名の一部']) }}
        {{Form::text('school_kumi', $kumi_select, ['class' => 'form-select','id' => 'school_kumi','placeholder' => '組']) }}
        {{Form::text('school_attendance_no', $attendance_no_select, ['class' => 'form-select','id' => 'school_attendance_no','placeholder' => '出席番号or生徒番号の一部']) }}
        {{Form::text('school_student_name', $user_name, ['class' => 'form-control','id' => 'school_student_name','placeholder' => '生徒名の一部','size' =>'20','maxlength' =>'20','autocomplete' =>'off']) }}
    </div>
    <div class="menuSpacesCell">
        <div class="menuSpacesOneCheck">
            <input type="checkbox" name="school_invalid" value=true" class="form-check-input" id="school_invalid" {{$school_invalid}}>
            <label class="form-check-label" for="school_invalid">学校無効</label>
        </div>
        <div class="menuSpacesOneCheck">
            <input type="checkbox" name="test_mode" value=true" class="form-check-input" id="test_mode" {{$test_mode}}>
            <label class="form-check-label" for="test_mode">ﾃｽﾄﾓｰﾄﾞ</label>
        </div>
        <div class="menuSpacesOneCheck">
            <input type="checkbox" name="practice_mode" value=true" class="form-check-input" id="practice_mode" {{$practice_mode}}>
            <label class="form-check-label" for="practice_mode">練習ﾓｰﾄﾞ</label>
        </div>
        <div class="menuSpacesOneCheck">
            <input type="checkbox" name="sort_point_desc" value=true" class="form-check-input" id="sort_point_desc" {{$sort_point_desc}}>
            <label class="form-check-label" for="sort_point_desc">ｽｺｱ高順</label>
        </div>   
        @include('layouts.include.searchbtn')
        <button class="btn btn-secondary btn-sm" id="excelbtn" name="excel" type="submit">Excel出力</button>
</form>
        @component('components.delButton')
        @slot('route', 'contestScoreDel')
        @slot('id', 'contest_scorelist-checks')
        @slot('name', 'スコアリスト')
        @slot('message','スコアを削除します。')
        @endcomponent
    </div>    
</div>
<div class="infotext">
        <p class="largeTexts"><span>全スコア数</span><strong>{{$student_num}}</strong>人</p>
        @if($student_num !==  $scoreHistory->total())
        <p class="largeTexts"><span>検索後のスコア数</span><strong>{{$scoreHistory->total()}}</strong>人</p>
        @endif
        
</div>
@include('layouts.include.pagination')
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th><input type="checkbox" id="checkAll" value="1">削除</th>
            <th scope="col">学校intId<br>学校ID</th>
            <th scope="col">学校名<br>コンテストアカウント</th>
            <th scope="col">生徒ID<br>生徒名</th>
            <th scope="col">学年or教室名<br>組</th>
            <th scope="col">出席N0,生徒NO</th>
            <th scope="col">コースID<br>コース名</th>
            <th scope="col">スコア<br>時間</th>
            <th scope="col">実施日<br>モード</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($scoreHistory as $sh)
        @if($sh->sc_invalid ==1)
        <tr class="bg-secondary text-white invalidity">
        @else
        <tr>
        @endif        
            <td>{{$sh->id}}
                {{ Form::open(['url' => '/contest-scorelist-delete', 'method' => 'post']) }}
                <input type="checkbox" name="checks[]" value="{{$sh->id}}" id="{{$sh->id}}】{{$sh->st_name}}/{{$sh->course_name}}/" form="contest_scorelist-checks">
            </td>
            <td name="edit">{{$sh->school_int_id}}<br>{{$sh->sc_school_id}}</td>
            <td>{{$sh -> sc_name}}<br>{{$sh->sc_school_id}}{{$sh->password_teacher_init}}</td>
            <td>{{$sh->st_id}}<br>{{$sh->st_name}}</td>
            <td>{{$sh->grade}}<br>{{$sh->kumi}}</td>
            <td>{{$sh->attendance_no}}</td>
            <td>{{$sh->course_id}}<br><font color="red">{{$sh->course_name}}</font></td>
            <td><font color="red">{{$sh->point}}</font><br>{{$sh->time}}</td>
            <td>{{date_format($sh->created_at,'Y/m/d　　H:i')}}<br>
                @if($sh->sc_contest==1)
                テストモード
                @elseif($sh->sc_contest==2)
                練習モード
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
