@php
	if($school->id==config('configrations.DEMO_CONTEST_INFO.testmode_school_id')){
		$metaTitle='【デモサイト】ナレッジタイピング for コンテスト';
		$topTitle='【デモ】タイピングコンテスト練習＆本番サイト';
		$modalScorelistFlg=true;
		$comment1='このサイトはデモサイトで、タイピングスコアは記録されません';
		$type='demo_test';

	} elseif($school->id==config('configrations.DEMO_CONTEST_INFO.practicemode_school_id')){
		$metaTitle='【デモサイト】ナレッジタイピング for コンテスト';
		$topTitle='【デモ】タイピングコンテスト練習＆本番サイト';
		$modalScorelistFlg=true;
		$comment1='このサイトはデモサイトで、タイピングスコアは記録されません';
		$type='demo_pra';
	} elseif($school->contest==1){
		$metaTitle='【'.$school->name.'】タイピングコンテスト';
		$topTitle='タイピングコンテスト練習＆本番サイト';
		$modalScorelistFlg=false;
		$comment1='';
		$type='honban_test';
	} else {
		$metaTitle='【'.$school->name.'】タイピングコンテスト';
		$topTitle='タイピングコンテスト練習＆本番サイト';
		$modalScorelistFlg=false;
		$comment1='';
		$type='honban_pra';
	}
@endphp

@extends('layouts.contest')

@section('content')
@section('title')
{{$metaTitle}}
@endsection
@section('description')
小学校で開催するタイピングコンテスト専用のプラットフォームです。生徒のアカウント管理もなく、専用URLを生徒に伝えるだけで開催できます。先生画面では全生徒のスコア結果を確認でき、ボタン一つで表彰状を印刷できます。
@endsection
@section('css')
<link href="/css/style.css?v=20240118" rel="stylesheet">
@endsection
@section('js')
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>
<script src="/js/bootstrap.min.js"></script>{{--bootstrapはCDNでなくサーバー設置--}}
<script src="/js/addnew_all.js"></script>
<script>
  const userinfo_entry_btn=document.getElementById("change");
	userinfo_entry_btn.addEventListener('click', () => {
	userinfo_entry_btn.disabled = true;
	userinfo_entry_btn.textContent = '保存中...';
		setTimeout(function () {
			userinfo_entry_btn.disabled = false;
		}, 5000);
  });
</script>

@endsection
@section('content')

<main class="py-4">
	<h2 class="ktTitle">{{$topTitle}}</h2>
	<div class="container teacher-top">
		<div id="typing" class="pageLinkBlock">
			<div class="mainBlock freeTop">
				<div class="menuSpaces">
					<div class="menuSpaces contestTitle">
						<div class="infotext">
							<p class="largeTexts"><span>学校名</span><strong>{{$school->name}}</strong></p>
						</div>
						<div class="startBtn">
							<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#modalreset" id="reset">本番スタートボタン</button>
							<img src="/img/startill.png" alt="">
						</div>
					</div>
					<div class="menuSpaces scoreMessage">
						{{-- 先生用のスコア一覧⇒デモのみ --}}
						@if($modalScorelistFlg)
						<button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#modal-reset" id="demo_contest_scorelist">【先生用】スコア一覧</button>
						{{-- (2)モーダル（デモ用スコアリスト） --}}
						<div class="modal fade" id="modal-reset" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5>スコア一覧リストはダミーデータです</h5>
									</div>
									<div>
										<div class="modalCell">
											<ul class="modalCellList">
												<li>●生徒のスコアデータをエクセル出力できます。</li>
												<li>●生徒のスコアを元に表彰状の印刷ができます。</li>
											</ul>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
										<button type="button" name="demo_contest_scorelist" class="btn btn-danger" id="demo_contest_scorelist_ok" onclick="location.href='/{{$contest_account}}/scorelist/'">一覧ページへ</button>
									</div>
								</div>
							</div>
						</div>{{--//(2)モーダルここまで--}}
						<div class="contest-demo-news">{{$school->memo1}}</div>
						@elseif( !empty($school->memo1))
								<div class="contest-news">{{$school->memo1}}</div>
						@endif
					</div>
				</div>
				<ul class="nav nav-pills nav-fill">
					<li class="nav-item">
						<a href="#tab1" class="nav-link active" data-toggle="tab">【コンテストコース】本番と同じ問題を練習しよう</a>
					</li>
					<li class="nav-item">
						<a href="#tab2" class="nav-link" data-toggle="tab">練習コース(1)<br>練習モード</a>
					</li>
					<li class="nav-item">
						<a href="#tab3" class="nav-link" data-toggle="tab">練習コース(2)<br>テストモード</a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="tab1" class="tab-pane active">
						{{Form::open(['route' => ['contestTyping', $contest_account],'method'=>'post','id'=>'typing_form'])}}
						<table class="table">
							<thead>
								<tr>
									<th scope="col">No.</th>
									<th scope="col">コース名</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($exam_courses as $index1 => $ex_course)
								<tr>
									<td>{{ $index1 + 1 }}</td>
									<td><input type="submit" name="{{$ex_course->id}}" value="{{$ex_course->course_name}}" /></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div id="tab2" class="tab-pane">
						<table class="table">
							<thead>
								<tr>
									<th scope="col">No.</th>
									<th scope="col">コース名</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($practice_courses as $index2 => $pt_course)
								<tr>
									<td>{{ $index2 + 1 }}</td>
									<td><input type="submit" name="{{$pt_course->id}}" value="{{$pt_course->course_name}}" /></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div id="tab3" class="tab-pane">
						<table class="table">
							<thead>
								<tr>
									<th scope="col">No.</th>
									<th scope="col">コース名</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($test_courses as $index3 => $ts_course)
								<tr>
									<td>{{ $index3 + 1 }}</td>
									<td><input type="submit" name="{{$ts_course->id}}" value="{{$ts_course->course_name}}" /></td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{{Form::close()}}
					</div>
				</div>
				<div class="typingText noUpper">
					<p>iPad、タブレットの場合はキーボードを接続してご利用ください。</p>
				</div>
			</div>
			<div id="typingSp" class="pageLinkBlock">
				<div class="mainBlock">
					<div class="typingText">
						<p>タイピングはPC・タブレット（キーボード接続）でご利用ください。スマートフォンは非対応です。</p>
					</div>
				</div>
			</div>
			<div id="about" class="pageLinkBlock">
				<div class="mainBlock">
					<div class="infoTitle">
						<span>タイピング練習とコンテストが両方できるサイトです</span>
					</div>
					<div class="infoBox">
						<div class="infoBoxText">
							<p>校内タイピング大会は、タイピング学習の励みになるものの、先生や生徒への周知、アカウント管理など面倒な作業が発生します。「ただでさえ忙しいのに・・・」こんな学校現場の声で生まれたのが「ナレッジタイピングfor コンテスト」です。</p>
							<p>生徒には「専用URL」を伝えるだけです。練習モードで、1か月の練習期間を設けたら、コンテスト当日は本番ボタンでタイピング開始。先生は、生徒のタイピング成績一覧を手元の端末で確認でき、ボタン一つで表彰状印刷ができます。</p>
							<p>表彰状も、優勝、金メダル、がんばったで賞、記録証など、全員に配布できるテンプレートを多数用意しています。</p>
							<div class="btnOuter">
							</div>
						</div>
						<div class="infoBoxImage">
							<img src="/img/contest_top1.jpg" alt="タイピング画面">
						</div>
					</div>
				</div>
			</div>

			<div id="contest" class="pageLinkBlock">
				<div class="mainBlock">
					<div class="infoTitle">
						<span>コンテスト実施方法<small>　{{$comment1}}</small></span>
					</div>
					<div class="infoBox">
						<div class="infoBoxText">
							<p class="starsLists"><strong class="stars">1.専用URLにアクセス</strong><span>学校ごとのコンテストURLをお知らせ。</span></p>
							<p class="starsLists"><strong class="stars">2.生徒は練習スタート</strong><span>コンテスト当日までタイピング練習。※練習期間のスコアは記録しません。</span></p>
							<p class="starsLists"><strong class="stars">3.コンテストの当日</strong><span>生徒は「本番」ボタンを押して、名前等を入力してタイピング開始。</span></p>
							<p class="starsLists"><strong class="stars">4.先生は管理画面でスコア確認</strong><span>管理画面の専用URLをお伝えします。</span></p>
							<p class="starsLists"><strong class="stars">5.ボタン一つで、表彰状を印刷</strong><span>表彰状のレイアウトも選べます。</span></p>
						</div>
						<div class="infoBoxImage">
							<img src="/img/contest_top2.jpg" alt="タイピング画面">
						</div>
					</div>
				</div>
			</div>
			<div id="faq" class="pageLinkBlock">
				<div class="mainBlock">
					<div class="infoTitle">
						<span>よくある質問</span>
					</div>
					<div class="qnaBlock">
						<div class="qnaCell">
							<div class="question">
								<span class="qnaIcon">Q</span>
								<div class="qnaText">
									<p>ナレッジタイピングforコンテストの利用環境は？</p>
								</div>
							</div>
							<div class="answer">
								<div class="answerInner">
									<span class="qnaIcon">A</span>
									<div class="qnaText">
										<p>主要GIGA端末に対応（iPad、Chromebook、Windowsタブレット、Windowsパソコン、MacBook）※スマートフォンは非対応。</p>
										<p>主要ブラウザ対応（Edge、Chrome、Firefox、Safari）</p>
										<p>日本語配列キーボードをご利用ください。</p>
									</div>
								</div>
							</div>
						</div>
						<div class="qnaCell">
							<div class="question">
								<span class="qnaIcon">Q</span>
								<div class="qnaText">
									<p>スコアの算出方法は？</p>
								</div>
							</div>
							<div class="answer">
								<div class="answerInner">
									<span class="qnaIcon">A</span>
									<div class="qnaText">
										<p>練習モード（キーボードや指が表示される）は、1点～100点満点となります。配点はスピード50%、タイプミス50%です。初心者は50点、中級者は80点を目指すイメージで設計しています。</p>
										<p>テストモード（黒い画面にアルファベット表示）は、3分間（又は5分間）の入力文字数です。（タイプミスは関係なし）</p>
									</div>
								</div>
							</div>
						</div>

						<div class="qnaCell">
							<div class="question">
								<span class="qnaIcon">Q</span>
								<div class="qnaText">
									<p>ローマ字表記は「ヘボン式」「訓令式」、どちらを採用していますか？</p>
								</div>
							</div>
							<div class="answer">
								<div class="answerInner">
									<span class="qnaIcon">A</span>
									<div class="qnaText">
										<p>どちらで入力しても、正解判定となります。</p>
										<p>（例）「し」はヘボン式だと「shi」、 訓令式だと「si」</p>
										<p>（例）「ち」はヘボン式だと「chi」、訓令式だと「ti」</p>
										<p>（例）「じゃ」はヘボン式だと「ja」、訓令式だと「zya」</p>
										@if($type=='demo_test' || $type=='honban_test')
										<p>※テストモードは、表示されたアルファベットをそのまま入力するタイプなので、上記の変換機能はありません。</p>
										@endif
									</div>
								</div>
							</div>
						</div>
						@if($type=='demo_test' || $type=='demo_pra')
						<div class="qnaCell">
							<div class="question">
								<span class="qnaIcon">Q</span>
								<div class="qnaText">
									<p>学校ではないのですが、イベントでタイピング大会を実施できますか？</p>
								</div>
							</div>
							<div class="answer">
								<div class="answerInner">
									<span class="qnaIcon">A</span>
									<div class="qnaText">
										<p>はい。プログラミング教室、塾、商業施設でのイベントなどでご利用できます。料金については、お問い合わせください。</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="price" class="pageLinkBlock">
					<div class="mainBlock">
						<div class="infoTitle">
							<span>利用料金<small></span>
						</div>
						<div class="infoBox">
							<div class="infoBoxText">
								<p>利用料金は、生徒1名1か月110円です。10名～で利用期間は、1か月単位です。</p>
								<p>（例）冬休み明けの1月15日にコンテストを実施する場合、12月20～1月19日を利用期間とし、授業や自宅でタイピング練習をしてから、コンテスト当日を迎えます。</p>
								<p>（例）夏休みに、タイピング学習の宿題を課した場合、コンテスト日時を設定せずに、各自で本番ボタンを押してスコアを記録する方法もあります。先生は、誰がいつタイピングをしたかを確認でき、あらかじめ表彰状の準備もできます。</p>
								<p>お申込み後、1週間でご利用可能です。ご希望によりオンライン説明会を実施します。</p>
							</div>
							<div class="infoBoxImage">
								<img src="/img/contest_top3.jpg" alt="先生の管理画面（印刷モード）">
							</div>
						</div>
					</div>
				</div>
				<div id="school" class="pageLinkBlock">
					<div class="mainBlock">
						<div class="infoTitle">
							<span>運営スクールの紹介</span>
						</div>
						<div class="infoBox reverces">
							<div class="infoBoxImage02">
								<a href="https://knowledge-p.jp" target="_blank"><img src="/img/knowledge-p.jpg" alt="ナレッジ・プログラミングスクールの公式ホームページのスクリーンショット"></a>
							</div>
							<div class="infoBoxText">
								<p>東京都世田谷区にある<a href="https://knowledge-p.jp" target="_blank">「ナレッジ・プログラミングスクール」</a>です。</p>
								<p>2020年に最初の「ナレッジタイピング」を作りました。</p>
								<p>「前回より1点でも高いスコアにしたい！」生徒たちは、ゲーム感覚で練習し、数か月でタッチタイピングを身に付けます。</p>
								<p>2023年に、全国版としてナレッジタイピングFreeと、生徒や成績管理ができるFor schoolをリリース。「これ以上アカウント管理は大変」という現場の声を受け、2024年にこの「Forコンテスト」をリリースしました。気軽にタイピング練習ができ、先生が生徒のスコア記録を見たい時だけ、成績を記録する仕様としました。</p>
								<div class="btnOuter">
									<a class="btn-link" href="https://n-typing.com" target="_blank">ナレッジタイピングFree</a>
								</div>
							</div>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
</main>
{{-- (1)モーダル（生徒登録） --}}
<form method="POST" action="/{{$contest_account}}/contest/exam-start" autocomplete="off" name="examStartBtnByMenu">
	@csrf
	{{--ここからアラートメッセージ（隠しボタン）bootstrapのモーダルがボタンイベントで発動するので、強制的にボタンイベントを作るため--}}
	<button type="button" data-toggle="modal" data-target="#alertmodal" id="alertbtn"></button>
	<div class="modal fade" id="alertmodal" tabindex="-1" role="dialog" aria-labelledby="flashmessage-label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5>未入力の項目があります。</h5>
				</div>
				<div class="modalCell">
					<ul class="modalCellList plusUnderBorder">
						@foreach ($errors->all() as $error)
						<li><span class="nameInput">{{$error}}</span></li>
						@endforeach
					</ul>
				</div>
				<div class="closeOuter btnCours">
					<button type="button" class="btn btn-secondary close" data-dismiss="modal" aria-label="Close">閉じる</button>
				</div>
			</div>
		</div>
	</div>{{--hiddenにバリデーションエラーの有無を持たせる⇒エラーがあれば、addnew_all.js モーダルメッセージ表示--}}
	@if($errors->all() == null)
	<input name="is_error" type="hidden" value="no" id="is_error">
	@else
	<input name="is_error" type="hidden" value="yes" id="is_error">
	@endif{{--ここまでアラートメッセージ--}}
	<input type="hidden" name="contest_account" value="{{$contest_account}}">
	@if($school->password_display){{--●●塾、教室--}}
	<div class="modal fade" id="modalreset" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5>教室名、名前、生徒番号を入力</h5>
				</div>
				<div>
					{{Form::hidden('kumi', 'なし')}}
					<div class="modalCell contestInput">
						<label for="contest_grade">教室名<span class="red">*</span></label>
						{{ Form::text('grade', null,['id' => 'contest_grade']) }}
					</div>
					<div class="modalCell contestInput">
						<label for="contest_attendance_no">生徒番号<small>（最大10文字<br>わからない<br>時は「なし」）</small><span class="red">*</span></label>
						{{ Form::text('attendance_no',null,['id' => 'contest_attendance_no','maxlength'=>'10']) }}
					</div>
					<div class="modalCell contestInput">
						<label for="contest_student_name">名前<small>（18文字まで）</small><span class="red">*</span></label>
						<input type="text" name="student_name" id="contest_student_name" size="18" maxlength="18" class="form-control" autocomplete="off">
						<input type="text" name="dummy" style="display:none;">
					</div>
					<div class="modalCell startTexts">
						<p>ボタンを押したら、タイピングコンテスト本番だよ！</p>
					</div>
				</div>
				<div class="modal-footer btnCours">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
					<button type="button" name="exam_start_btn" value="true" class="btn btn-danger" id="change" onclick="submit();">本番スタート</button>
				</div>
			</div>
		</div>
	</div>
	@else{{--小学校--}}
	<div class="modal fade" id="modalreset" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5>学年、クラス、出席番号、名前を入力</h5>
				</div>
				<div>
					<div class="modalCell contestInput">
						<label for="contest_grade">学年<span class="red">*</span></label>
						{{ Form::select('grade', config('configrations.SCHOOL_GRADE'),null,['id' => 'contest_grade']) }}
					</div>
					<div class="modalCell contestInput">
						<label for="contest_kumi">クラス<span class="red">*</span></label>
						{{ Form::select('kumi', config('configrations.SCHOOL_KUMI'),null,['id' => 'contest_kumi']) }}
					</div>
					<div class="modalCell contestInput">
						<label for="contest_attendance_no">出席番号<span class="red">*</span></label>
						{{ Form::select('attendance_no', config('configrations.SCHOOL_ATTENDANCE_NO'),null,['id' => 'contest_attendance_no',]) }}
					</div>
					<div class="modalCell contestInput">
						<label for="contest_student_name">名前<small>（18文字まで）</small><span class="red">*</span></label>
						<input type="text" name="student_name" id="contest_student_name" size="18" maxlength="18" class="form-control" autocomplete="off">
						<input type="text" name="dummy" style="display:none;">
					</div>
					<div class="modalCell startTexts">
						<p>ボタンを押したら、タイピングコンテスト本番だよ！</p>
					</div>
				</div>
				<div class="modal-footer btnCours">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
					<button type="button" name="exam_start_btn" value="true" class="btn btn-danger" id="change" onclick="submit();">本番スタート</button>
				</div>
			</div>
		</div>
	</div>
	@endif
</form>
@endsection