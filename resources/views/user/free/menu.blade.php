@extends('layouts.free')
@section('googletag')
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-QHPN820GKL"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'G-QHPN820GKL');
</script>
@endsection
@section('title')
ナレッジタイピング | 小学生の無料タイピング練習サイト
@endsection
@section('description')
小学生のための「無料タイピング練習サイト」です。ローマ字五十音を反復練習することで、初めてタイピングを練習する子どもでも正しい指使いが自然に身につきます。小学高学年用には、英単語、長文、ことわざコースもあります。商用可、広告非表示なので、学校や塾などで活用してください。
@endsection
@section('css')
<link href="/css/style.css?v=20240926" rel="stylesheet">
@endsection
@section('js')
<script>
  const nyumon=@json($nyumon);
  const cyukyu=@json($cyukyu);
  const jokyu=@json($jokyu);
  let isFree = true;
</script>
<script src="/js/lazyload.min.js"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>
<script src="/js/bootstrap.min.js"></script>{{--bootstrapはCDNでなくサーバー設置--}}
<script src="/js/alphabet.js?v=20240917"></script>
<script>
    lazyload();
</script>{{--画像遅延表示ライブラリlazyloadを実行、bodyの最後--}}
@endsection
@section('content')
<main class="py-4">
    <div class="container teacher-top">
        <div id="typing" class="pageLinkBlock">
            <div class="mainBlock freeTop">
                {{Form::open(['url'=>'/free/course','method'=>'post','id'=>'typing_form'])}}
                <div class="menuSpaces">
                    <div class="menuSpaces">
                        <div class="menuSpacesCell">
                            <div class="menuSpacesOne roSm">
                                <div class="radioOut">
                                    {{ Form::radio('sound','on',($sound == 'yes'? true:false),['id' => 'sound1']) }}
                                    <label for="sound1">音有り</label>
                                </div>
                                <div class="radioOut">
                                    {{ Form::radio('sound','off',($sound == 'no'? true:false),['id' => 'sound2']) }}
                                    <label for="sound2">音なし</label>
                                </div>
                            </div>
{{--                        <div class="menuSpacesOne roSm">
                                <div class="radioOut">
                                    {{ Form::radio('alpha','on',($alpha == 'yes' ? true:false),['id' => 'alpha1']) }}
                                    <label for="alpha1">アルファベットあり</label>
                                </div>
                                <div class="radioOut">
                                    {{ Form::radio('alpha','off',($alpha == 'no' ? true:false),['id' => 'alpha2']) }}
                                    <label for="alpha2">なし</label>
                                </div>
                            </div>
--}}
                        </div>
                    </div>                    
                    <div class="menuSpaces rightBox rightSpaces linkSmall">
                        <div class="manualSpace links bottomBox">
                            @component('components.modalimageTop2')
                            @slot('word', 'このサイトの使い方')
                            @slot('image_name', 'typing1')
                            @endcomponent
                        </div>
                        <div class="manualSpace links bottomBox">
                            @component('components.modalimageTop2')
                            @slot('word', 'テストモード')
                            @slot('image_name', 'typing2')
                            @endcomponent
                        </div>
                        <div class="manualSpace links bottomBox">
                            @component('components.modalimageTop2')
                            @slot('word', 'キータッチ2000練習')
                            @slot('image_name', 'typing3')
                            @endcomponent
                        </div>
                    </div>{{--今日のラッキーコースボタン--}}
                    <div class="randam-course-btn-outer">
                        <h2><span>{{$newstitle}}</span></h2>                    
                        <div class="randam-course-btn">
                            <button type="submit" id="luckybtn_entry" class="btn btn-outline-primary btn-sm" onclick="luckyBtnEntry()">入門</button>
                            <button type="submit" id="luckybtn_middle" class="btn btn-outline-primary btn-sm" onclick="luckyBtnMiddle()">中級</button>
                            <button type="submit" id="luckybtn_advance" class="btn btn-outline-primary btn-sm" onclick="luckyBtnAdvance()">上級</button>
                        </div>
                    </div>
                </div>
                <div class="typingText"></div>
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a href="#tab1" class="nav-link {{$currentmonth_tab_active}}" data-toggle="tab">今月の<br>タイピング</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab2" class="nav-link {{$star1_tab_active}}" data-toggle="tab">レベル<br>★</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab3" class="nav-link {{$star2_tab_active}}" data-toggle="tab">レベル<br>★★</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab4" class="nav-link {{$star3_tab_active}}" data-toggle="tab">レベル<br>★★★</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab5" class="nav-link {{$test_tab_active}}" data-toggle="tab">テスト<br>モード</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab6" class="nav-link {{$keytouch_tab_active}}" data-toggle="tab"> キータッチ<br>2000練習</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab7" class="nav-link {{$map_tab_active}}" data-toggle="tab">地図<br>世界遺産</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab8" class="nav-link {{$english1_tab_active}}" data-toggle="tab"> <span><font color="red">New</font></span>英語<br>１</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab9" class="nav-link {{$english2_tab_active}}" data-toggle="tab"> <span><font color="red">New</font></span>英語<br>２</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab10" class="nav-link {{$english3_tab_active}}" data-toggle="tab"> <span><font color="red">New</font></span>英語<br>３</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="tab1" class="tab-pane {{$currentmonth_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses1 as $index1 => $course1)
                                <tr>
                                    <td>{{ $index1 + 1 }}</td>
                                    <td><input type="submit" name="{{$course1->id}}" value="{{$course1->course_name}}" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="tab2" class="tab-pane {{$star1_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses2 as $index2 => $course2)
                                <tr>
                                    <td>{{ $index2 + 1 }}</td>
                                    <td><input type="submit" name="{{$course2->id}}" value="{{$course2->course_name}}" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="tab3" class="tab-pane {{$star2_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses3 as $index3 => $course3)
                                <tr>
                                    <td>{{ $index3 + 1 }}</td>
                                    <td><input type="submit" name="{{$course3->id}}" value="{{$course3->course_name}}" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="tab4" class="tab-pane {{$star3_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses4 as $index4 => $course4)
                                <tr>
                                    <td>{{ $index4 + 1 }}</td>
                                    <td><input type="submit" name="{{$course4->id}}" value="{{$course4->course_name}}" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="tab5" class="tab-pane {{$test_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses5 as $index5 => $course5)
                                <tr>
                                    <td>{{ $index5 + 1 }}</td>
                                    <td><input type="submit" name="{{$course5->id}}" value="{{$course5->course_name}}" onclick="courseListTest()" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="tab6" class="tab-pane {{$keytouch_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses6 as $index6 => $course6)
                                <tr>
                                    <td>{{ $index6 + 1 }}</td>
                                    <td><input type="submit" name="{{$course6->id}}" value="{{$course6->course_name}}" onclick="courseListKeytouch()" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="tab7" class="tab-pane {{$map_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses7 as $index7 => $course7)
                                <tr>
                                    <td>{{ $index7 + 1 }}</td>
                                    <td><input type="submit" name="{{$course7->id}}" value="{{$course7->course_name}}" onclick="courseListMap()" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="tab8" class="tab-pane {{$english1_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses8 as $index8 => $course8)
                                <tr>
                                    <td>{{ $index8 + 1 }}</td>
                                    <td><input type="submit" name="{{$course8->id}}" value="{{$course8->course_name}}" onclick="courseListEnglish1()" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="tab9" class="tab-pane {{$english2_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses9 as $index9 => $course9)
                                <tr>
                                    <td>{{ $index9 + 1 }}</td>
                                    <td><input type="submit" name="{{$course9->id}}" value="{{$course9->course_name}}" onclick="courseListEnglish2()" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="tab10" class="tab-pane {{$english3_tab_active}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">コース名</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses10 as $index10 => $course10)
                                <tr>
                                    <td>{{ $index10 + 1 }}</td>
                                    <td><input type="submit" name="{{$course10->id}}" value="{{$course10->course_name}}" onclick="courseListEnglish3()" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{Form::close()}}
                <div class="typingText noUpper">
                    <p>iPad、タブレットの場合はキーボードを接続してご利用ください。</p>
                </div>
            </div>
            <div id="typingSp" class="pageLinkBlock">
                <div class="mainBlock">
                    <div class="typingText">
                        <p>タイピングはPC・タブレット（キーボード接続）でご利用ください。スマートフォンは非対応です。</p>
                    </div>
                </div>
            </div>
            <div id="news" class="pageLinkBlock">
                <div class="mainBlock">
                    <div class="infoTitle">
                        <span>お知らせ</span>
                    </div>
                    <div class="infoBlock">
                        @foreach ($information as $info)
                        <div class="infoBlockCell">
                            <div class="infoBlockCellImage">
                                @if($info->image !== null)<img class="lazyload" data-src="storage/blogimages/{{$info->image}}" alt="{{$info->title}}の記事に関する画像">@endif
                            </div>
                            <div class="infoBlockCellText">
                                <time class="infoTime">{{ date_format($info->created_at, 'Y-m-d') }}</time>
                                @if(date("Y-m-d H:i:s",strtotime("-10 day")) < $info->created_at)<span class="infoNew">NEW</span>@endif
                                    <div class="infoBlockCellTitle">{{ $info->title }}</div>
                                    <p>{!! $info->article !!}</p>
                            </div>
                        </div>
                        @endforeach
                        <div class="btnOuter">
                            <a class="btn-link" href="./free-allinfo">すべてのお知らせ</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="about" class="pageLinkBlock">
                <div class="mainBlock">
                    <div class="infoTitle">
                        <span>このサイトについて</span>
                    </div>
                    <div class="infoBox">
                        <div class="infoBoxText">
                            <p>ナレッジタイピングは、「ナレッジ・プログラミングスクール」の子どもたちが授業で使っているアプリを、「誰でも、どこでも、無料で」（広告表示なし）使えるようにしたものです。 小学１年生でも、<font color="red">五十音から丁寧に学べて、「正しい指」と「スピード」が自然に身につく </font>がコンセプトです。</p>
                            <p>「多くの子どもたちに、タッチタイピングをマスターしてもらいたい！」<br>そのため、個人だけでなく、商用・非商用を問わず、塾、教室、学校でも無料でご利用いただけます。</p>
                            <p><span class="text-danger">※学校や塾には、成績や生徒管理ができる「for School」(1人110円/月)がおすすめ！<span></p>
                            <div class="btnOuter">
                                <a class="btn-link" href="https://n-typing-school.com" target="_blank">ナレッジタイピング for School</a>
                            </div>
                        </div>
                        <div class="infoBoxImage">
                            <img class="lazyload" data-src="/img/typingimage1.jpg" alt="タイピング画面">
                        </div>
                    </div>
                </div>
            </div>
            <div id="about02" class="pageLinkBlock">
                <div class="mainBlock">
                    <div class="infoTitle">
                        <span>レベル別コース＆テストモード　　<small>五十音、英単語、テストまで</small></span>
                    </div>
                    <div class="infoBox">
                        <div class="infoBoxText">
                            <p class="starsLists"><strong class="stars">今月のタイピング</strong><span>毎月、時節関連のコースを発表します。</span></p>
                            <p class="starsLists"><strong class="stars">★コース</strong><span>あいうえお...から学習。ローマ字五十音を覚えたら、まずは言葉2文字（雨、犬、ねこなど）の入力練習です。</span></p>
                            <p class="starsLists"><strong class="stars">★★コース</strong><span>言葉や、短い文章（学校へ行く）、英単語（Apple 発音付き）、都道府県名など。</span></p>
                            <p class="starsLists"><strong class="stars">★★★コース</strong><span>少し長めの文章、慣用句やことわざなど。</span></p>
                            <p class="starsLists"><strong class="stars">テストモード</strong><span>3分間、5分間に何文字打てるか？を記録します。前回より記録をあげようと、モチベーションUPになります。</span></p>
                            <p class="starsLists"><strong class="stars">キータッチ<br>2000検定練習</strong><span>日本商工会議所の公式タイピング検定の練習モードです。10分間に入力文字数を記録します。ナレッジプログラミングスクールでは、教室内で実施する自前のタイピング検定として活用しています。</span></p>
                            <p class="starsLists"><strong class="stars">地図モード</strong><span>日本地図や世界遺産等のイラストを見ながらタイピング。クイズモード搭載。</span></p>
                            <p class="starsLists"><strong class="stars">英語モード</strong><span>英検5級、4級に頻出する小学生向け約800単語を収録。全単語、イラスト、発音付き。スペルを非表示にできる確認モード搭載。</span></p>
                        </div>
                        <div class="infoBoxImage">
                            <img class="lazyload" data-src="/img/typingimage2.jpg" alt="練習モードとテストモードの画面説明">
                        </div>
                    </div>
                </div>
            </div>
            <div id="about03" class="pageLinkBlock">
                <div class="mainBlock">
                    <div class="infoTitle">
                        <span>「練習モード」⇒「テストモード」　　<small>上達を実感でき、モチベーションUP</small></span>
                    </div>
                    <div class="infoBox">
                        <div class="infoBoxText">
                            <p class="starsLists"><strong class="stars">練習モード</strong><span>ローマ字50音の基本コースから、ことわざ、時事ネタまで、約150コース。スピード、誤入力などを判定して、スコアがでます。</span></p>
                            <p class="starsLists"><strong class="stars">テストモード</strong><span>シンプルに、3分間（5分間）に、何文字入力できるか？を記録します。</span></p>
                            <p><span>初心者は、練習モードからスタート。テストモードでスキルチェックがおすすめ！</span></p>
                            <p><span>上達したら「キータッチ2000」の模擬練習モード」にも挑戦です。10分間に2000文字入力して「マスター」を目指そう！</span><small>（※キータッチ2000検定の公式練習サイトではありません）</small></p>
                        </div>
                        <div class="infoBoxImage">
                            <img class="lazyload" data-src="/img/typingimage3.jpg" alt="タイピング後のスコア表示画面">
                        </div>
                    </div>
                </div>
            </div>
            <div id="faq" class="pageLinkBlock">
                <div class="mainBlock">
                    <div class="infoTitle">
                        <span>よくある質問</span>
                    </div>
                    <div class="qnaBlock">
                        <div class="qnaCell">
                            <div class="question">
                                <span class="qnaIcon">Q</span>
                                <div class="qnaText">
                                    <p>ナレッジタイピングの利用環境は？</p>
                                </div>
                            </div>
                            <div class="answer">
                                <div class="answerInner">
                                    <span class="qnaIcon">A</span>
                                    <div class="qnaText">
                                        <p>端末は、iPad、Chromebook、Windowsタブレット、Windowsパソコン、MacBookです。※スマートフォンは非対応。</p>
                                        <p>対応ブラウザは、Edge、Chrome、Firefox、Safariです。（Internet Explorer（IE）は非対応）</p>
                                        <p>日本語配列(JIS)キーボードをご利用ください。英語配列（US）キーボードには対応していません。</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="qnaBlock">
                            <div class="qnaCell">
                                <div class="question">
                                    <span class="qnaIcon">Q</span>
                                    <div class="qnaText">
                                        <p>ローマ字表記は「ヘボン式」「訓令式」、どちらを採用していますか？</p>
                                    </div>
                                </div>
                                <div class="answer">
                                    <div class="answerInner">
                                        <span class="qnaIcon">A</span>
                                        <div class="qnaText">
                                            <p>どちらで入力しても、正解判定となります。</p>
                                            <p>（例）「し」はヘボン式だと「shi」、 訓令式だと「si」</p>
                                            <p>（例）「ち」はヘボン式だと「chi」、訓令式だと「ti」</p>
                                            <p>（例）「じゃ」はヘボン式だと「ja」、訓令式だと「zya」</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="qnaBlock">
                            <div class="qnaCell">
                                <div class="question">
                                    <span class="qnaIcon">Q</span>
                                    <div class="qnaText">
                                        <p>タイピングスコアの算出基準は？</p>
                                    </div>
                                </div>
                                <div class="answer">
                                    <div class="answerInner">
                                        <span class="qnaIcon">A</span>
                                        <div class="qnaText">
                                            <p>練習モード（キーボードや指が表示される）では、100点満点制です。</p>
                                            <p>スピード50%、タイプミス50%です。</p>
                                            <p>レベル感としては、100点満点はかなり難しくクラスで1割程度、70～80点を目指してください。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="answer">
                                    <div class="answerInner">
                                        <span class="qnaIcon">A</span>
                                        <div class="qnaText">
                                            <p>テストモード（黒い画面にタイピング文字が表示される）では、3分、5分間の入力文字数がスコアとなります。ミスタイプや、スピードは一切考慮しません。</p>
                                            <p>基本的に、3分コースは最大500文字。5分コースは最大1000文字。これら最大文字数を入力できるのは、高学年の場合、クラスで数名です。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="qnaCell">
                                <div class="question">
                                    <span class="qnaIcon">Q</span>
                                    <div class="qnaText">
                                        <p>ナレッジタイピングは、商用でも使えますか？</p>
                                    </div>
                                </div>
                                <div class="answer">
                                    <div class="answerInner">
                                        <span class="qnaIcon">A</span>
                                        <div class="qnaText">
                                            <p>はい、使えます。</p>
                                            <p>幼稚園、学校、各種学校だけでなく、学童、教室、塾、プログラミング教室の授業でもご利用いただけます。</p>
                                            <p>塾・学校向けプラン「ナレッジタイピング for School」（生徒1名110円/月）をご用意しています。生徒・成績管理、コース追加、印刷など、授業で活用できる機能があります。<a href="https://n-typing-school.com" target="_blank">詳しくはこちら</a>。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="school" class="pageLinkBlock">
                    <div class="mainBlock">
                        <div class="infoTitle">
                            <span>運営スクールの紹介</span>
                        </div>
                        <div class="infoBox reverces">
                            <div class="infoBoxImage02">
                                <a href="https://knowledge-p.jp" target="_blank"><img class="lazyload" data-src="/img/knowledge-p.jpg" alt="ナレッジ・プログラミングスクールの公式ホームページのスクリーンショット"></a>
                            </div>
                            <div class="infoBoxText">
                                <p>東京都世田谷区にある<a href="https://knowledge-p.jp" target="_blank">「ナレッジ・プログラミングスクール」</a>です。</p>
                                <p>2020年に最初の「ナレッジタイピング」を作りました。</p>
                                <p>「前回より1点でも高いスコアにしたい！」生徒たちは、ゲーム感覚で練習し、数か月でタッチタイピングを身に付けます。</p>
                                <p>飽きずに続けられるように、「オリジナルコース機能」や「テスト機能」を追加し、生徒と一緒にアプリの改良を重ねました。</p>
                                <p><span class="text-danger">同シリーズ「for コンテスト」は、アカウント管理なしで好きな時にスコアが記録できるサイトです。</span></p>
                                <div class="btnOuter">
                                    <a class="btn-link" href="/contest2pramode2/contest" target="_blank">ナレッジタイピング for コンテスト</a>
                                </div>
                            </div>
                        </div>
                        <div class="infoBox">
                            <div class="infoBoxText">
                                <p>「多くの子どもたちに、タッチタイピングをマスターしてもらいたい！」というのが、私たちの願いです。</p>
                                <p>そのため、ナレッジタイピングは、個人だけでなく、商用・非商用を問わず、塾、教室、学校でも無料でご利用いただけます。</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</main>
@endsection