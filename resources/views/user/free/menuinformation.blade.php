@extends('layouts.free')

@section('title')
お知らせ | ナレッジタイピング
@endsection

@section('description')
新しいタイピングコースや、タイピング練習のコツ、タイピングコンテストなどのご案内です。
@endsection

@section('css')
<link href="/css/style.css?v=20231216" rel="stylesheet">
@endsection

@section('js')
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js"></script>
@endsection

@section('content')
<main class="py-4">
<div class="container teacher-top">
    <h2 class="ktTitle">すべてのお知らせ</h2>
    <div id="news" class="pageLinkBlock">
        <div class="mainBlock">
            {{$information->links()}}
            @if(count($information)>0)
            <p>全{{ $information->total() }}件中
            {{($information->currentPage()-1) * $information->perPage()+1}} -
            {{(($information->currentPage()-1) * $information->perPage()+ 1) + (count($information)-1)}}件が表示されています。
            </p>
            @else
            <p>データがありません。</p>
            @endif
            <div class="infoBlock">
                @foreach ($information as $info)
                <div class="infoBlockCell">
                    <div class="infoBlockCellImage">
                    @if($info->image !== null)<img src="storage/blogimages/{{$info->image}}" alt="{{$info->title}}の記事に関する画像">@endif
                    </div>
                    <div class="infoBlockCellText">
                        <time class="infoTime">{{date_format($info->created_at,'Y-m-d')}}</time>
                        @if(date("Y-m-d H:i:s",strtotime("-10 day")) < $info->created_at)<span class="infoNew">NEW</span>@endif
                        <div class="infoBlockCellTitle">{{$info->title}}</div>
                        <p>{!! $info->article !!}</p>
                    </div>
                </div>
                @endforeach
                @if($information !== null)
                <div class = "pagination_wrap">
                    <div class = "pagination_link">
                        {{$information->links()}}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
</main>
<div class="pagetop"><a href="#top"><span><img src="/img/pagetop.svg" alt="PAGETOP"/></span></a> </div>
@endsection