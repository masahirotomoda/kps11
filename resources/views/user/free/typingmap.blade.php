@extends('layouts.free')

@section('title')
@if($is_isan)
世界遺産クイズタイピング（東武ワールドスクウェア編）
@else
日本地図クイズタイピング
@endif
@endsection

@section('description')
@if($is_isan)
東武ワールドスクウェアにあるミニチュア世界遺産をクイズタイピング
@else
都道府県名、地図記号、各県の自然や世界遺産などをクイズ形式で出題。
@endif
@endsection

@section('css')
<link rel="stylesheet" href="/css/typing.css?v=20240123">
<link rel="stylesheet" href="/css/style.css?v=20240123">
@endsection

@section('js')
<script>
  let typeWordsAll = @json($wordData);
  let courseData = @json($course);
  let courseList = @json($coursesArray);
  let sound = @json($soundData);
  const alphabet = @json($alphabetData);
  let scoretype='free';
  const routeLink = "{{asset('/')}}";
  const isFree = true;
</script>

<script src="/js/typing-data.js?v=20240123"></script>
<script src="/js/typing-common.js?v=20240123?v=20240123"></script>
<script src="/js/typing-score.js"></script>
<script src="/js/typing-map.js?v=20240123?v=20231216"></script>

<script src="/js/typing-sound.js"></script>
<script src="/js/romanizer.js"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>
<script src="/js/prevnextcourse.js?v=20240123"></script>
@endsection

@section('content')
@include('layouts.include.typingcoursename')
<section class="l-main">
  <div class="l-main__container freel-main">
    <div class="order-1">
      <div class="c-table">
        <div class="c-table__container">
          <div class="c-table__container__space">{{--ここからコースリスト--}}
            <table>
              <thead>
                <tr>
                  <th colspan="2" scope="col">地図コース</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tabCourseList as $tabCourse)
                <tr>
                  <td name="tab_course_index">{{$loop->index + 1}}</a></td>
                  <td><a href="javascript:getTabCourseLink('{{$loop->index}}');">{{$tabCourse->course_name}}</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="order-0 typingOrder typingOrderMap">
      <div class="p-typingscreen" id="p-typingscreen__startscreen">
        <div id="p-typingscreen__startscreen__option__container">
          <div id="p-typingscreen__startscreen__sound__container" class="soundRadio">
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__sound" />
              <label for="p-typingscreen__startscreen__sound">音あり</label>
            </div>
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__nosound" />
              <label for="p-typingscreen__startscreen__nosound">なし</label>
            </div>
          </div>
          <div id="p-typingscreen__startscreen__challenge__container">
            <input type="checkbox" id="p-typingscreen__startscreen__challenge" onclick="challengeBtnClick()" /><label for="p-typingscreen__startscreen__challenge"></label>
          </div>
        </div>
        <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingscreen__start" onclick="startBtnClick()">はじめる</button>
        <div class="p-typingscreen__count hidden" id="p-typingscreen__count"></div>
        <div class="p-typingscreen__kana">&nbsp;</div>
        <div class="p-typingscreen__mana">&nbsp;</div>
        <div class="p-typingscreen__romaji">
          <div class="p-typingscreen__romaji__typed"></div>
          <div class="p-typingscreen__romaji__untyped">&nbsp;</div>
        </div>
      </div>
      <div class="p-typingscreen hidden" id="p-typingscreen">
        <div class="p-typingscreen__kana" id="p-kana"></div>{{-- お題(仮名) --}}
        <div class="p-typingscreen__mana" id="p-mana"></div>{{-- お題(真名) --}}
        <div class="p-typingscreen__romaji">
          <div class="p-typingscreen__romaji__show" id="p-typing-show"></div>
          <span class="p-typingscreen__romaji__typed hidden" id="p-typed"></span><span class="p-typingscreen__romaji__untyped hidden" id="p-untyped"></span>
        </div>{{-- お題(ローマ字) --}}
        <div class="p-typingscreen__successtype" id="p-successtype"></div>{{-- 成功タイプ数 --}}
        <div class="p-typingscreen__misstype" id="p-misstype"></div>{{-- ミスタイプ数 --}}
      </div>
      <div class="p-typingresult hidden" id="p-typingresult">
        {{-- 結果表示画面 --}}
        <div class="p-typingresult__bg"></div>
        <div class="p-typingresult__card">
          <h3>結果</h3>
          <ul>
            <li><span>スコア</span>
              <div id="p-typingresult--score"></div><span>点</span><span id ="perfect"></span>
            </li>
            <li><span>時間</span>
              <div id="p-typingresult--time"></div><span>秒</span>
            </li>
            <li><span>打った数</span>
              <div id="p-typingresult--all"></div><span>回</span>
            </li>
            <li><span>間違った数</span>
              <div id="p-typingresult--miss"></div><span>回</span>
            </li>
          </ul>
          <h4 class="mb-4"><strong>スペースキーでウィンドウがとじます。</strong></h4>
          <button type="button" id="typing_store_close" class="c-button--sm" onclick="restartBtnClick()">とじる</button>
        </div>
      </div>
      <div id="map">
        <div id="map_img"></div>{{--各コースにある単語の地図、1コース複数--}}
        <div id="map_top"></div>{{--各コースの表示の地図、1コース1枚--}}
      </div>
      <div class="underBtn leftRightBtn">
        <button type="button" class="prevCourse" id="mae">前のコース</button>
        <button type="button" class="nextCourse" id="tugi">次のコース</button>
      </div>
      <button type="button" class="btn btn-primary stopBtn" id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
    </div>
  </div>
  @endsection