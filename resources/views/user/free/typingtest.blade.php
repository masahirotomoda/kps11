@extends('layouts.free')

@section('title')
@if($course->tab == 'key')
キータッチ2000検定練習問題
@else
3分、5分の学年別テストタイピング
@endif
@endsection

@section('description')
@if($course->tab == 'key')
10分間の入力数を証明する商工会議所「キータッチ2000」検定の模擬問題。
@else
3分、5分間に何文字入力できるかのテスト形式で、学年別テストもあります。
@endif
@endsection

@section('css')
<link rel="stylesheet" href="/css/typing.css?v=20240123">
<link rel="stylesheet" href="/css/style.css?v=20240123">
@endsection
@section('js')
<script>
  let courseData = @json($course);
  let typeWordsAll = @json($wordData);
  let courseList = @json($coursesArray);
  const routeLink = ' / ';
  let isFree = true;
</script>
<script src="/js/typing-common.js?v=20240123?v=20240123"></script>
<script src="/js/typing-test.js?v=20240123"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>
@endsection

@section('content')
@php
if($course->tab=='test'){
$TabCourseName='テストコース';
} else{
$TabCourseName='キータッチ2000検定';
}
@endphp
@include('layouts.include.typingcoursename')
<section class="l-main">
  <div class="l-main__container freel-main">
    <div class="order-1">
      <div class="c-table">
        <div class="c-table__container">
          <div class="c-table__container__space">
            <!--コースリスト-->
            <table>
              <thead>
                <tr>
                  <th colspan="2" scope="col">{{$TabCourseName}}</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tabCourseList as $tabCourse)
                <tr>
                  <td name="tab_course_index">{{$loop->index + 1}}</a></td>
                  <td><a href="javascript:getTabCourseLink('{{$loop->index}}');">{{$tabCourse->course_name}}</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div><!--/order-1"-->
    <div class="order-0">
      <div class="p-typingtestscreenOut">
        <div class="p-typingtestscreen" id="p-typingtestscreen__startscreen">
          <div class="p-typingtestscreen__flex">
            <div class="p-typingtestscreen__typenumber">入力： 0文字</div>
            <div class="p-typingtestscreen__time">残り時間： <span id="p-initial-time"></span></div>
          </div>
          <div class="p-typingtestscreen__typesbg">
            <div id="p-typingscreen__startscreen__optionbtn__container" class="romanRadio">
              <div class="radioOut">
                <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__upper"  />
                <label for="p-typingscreen__startscreen__upper">大文字</label>
              </div>
              <div class="radioOut">
                <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__lower" checked/>
                <label for="p-typingscreen__startscreen__lower">ローマ字小文字</label>
              </div>
            </div>
            <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingtestscreen__start" onclick="startTypingFunc()">はじめる</button>
            <div class="p-typingscreen__count hidden" id="p-typingscreen__count"></div>
            <div class="p-typingtestscreen__typesfield"></div>
          </div>
        </div>
        <div class="p-typingtestscreen hidden" id="p-typingtestscreen">
          <div class="p-typingtestscreen__flex">
            <div class="p-typingtestscreen__typenumber">入力： <span id="p-typenumber"></span>文字</div>
            <div class="p-typingtestscreen__time">残り時間： <span id="p-time"></span></div>
          </div>
          <div class="p-typingtestscreen__typesbg">
            <div class="p-typingtestscreen__typesfield" id="p-typefield"></div>
          </div>
        </div>
        <div class="p-typingresult hidden" id="p-typingresult">
          {{-- 結果表示画面--}}
          <div class="p-typingresult__bg"></div>
          <div class="p-typingresult__card">
            <h3>結果</h3>
            <ul>
              <li><span>スコア</span>
                <div id="p-typingresult--score"></div><span>文字</span>
              </li>
              @if ( $course->tab === 'key' )
              <li style="display:none"><span>級</span>
                <div id="p-typingresult--rank"></div>
              </li>
              @endif
              <li><span>残り時間</span>
                <div id="p-typingresult--time"></div>
              </li>
            </ul>
            <div class="p-typingtestscreen__typesbg">
              <div class="p-typingtestresultscreen__typesfield" id="p-typeresultfield"></div>
            </div>
            <button type="button" id="typing_store_close" class="c-button--sm" onclick="restartBtnClick()">とじる</button>
          </div>
        </div>
        <div class="underBtn plusUnder">
          <div class="underBtnInner">
            <button type="button" class="prevCourse btn btn-primary" id="mae">前のコース</button>
            <button type="button" class="btn btn-primary " id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
            <button type="button" class="nextCourse btn btn-primary" id="tugi">次のコース</button>
            @if ( $course->tab == 'key' )
            @component('components.keytouchcaution')
            @endcomponent
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection