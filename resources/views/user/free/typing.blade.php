@extends('layouts.free')

@section('title')
初級～上級レベル別タイピング
@endsection
@section('description')
レベル別の120コースには「ローマ字50音」「英単語」「ことわざ」などがあります。又、毎月、時節やイベントに応じた楽しいタイピングコースを追加しています。
@endsection
@section('css')
<link rel="stylesheet" href="/css/typing.css?v=20240123">
<link rel="stylesheet" href="/css/style.css?v=20240123">
@endsection

@section('js')
<script>
  let typeWordsAll = @json($wordData);
  let courseData = @json($course);
  let courseList = @json($coursesArray);
  let sound = @json($soundData);
  let alphabet = @json($alphabetData);
  const routeLink = "{{asset('/')}}";
  let isFree = true;
  let scoretype='free';
</script>
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/typing-data.js?v=20240123"></script>
<script src="/js/typing-common.js?v=20240123?v=20240123"></script>
<script src="/js/typing.js?v=20240123"></script>
<script src="/js/typing-score.js"></script>
<script src="/js/typing-sound.js"></script>
<script src="/js/keyboard.js"></script>
<script src="/js/finger.js"></script>
<script src="/js/romanizer.js"></script>
<script src="/js/common.js?v=20240123"></script>
<script src="/js/prevnextcourse.js?v=20240123"></script>
@endsection

@section('content')
@php
if($course->tab=='month'){
$TabCourseName='今月のタイピング';
} elseif($course->tab=='star1'){
$TabCourseName='★コース';
} elseif($course->tab=='star2'){
$TabCourseName='★★コース';
} elseif($course->tab=='star3'){
$TabCourseName='★★★コース';
}
@endphp

@include('layouts.include.typingcoursename')

<section class="l-main">
  <div class="l-main__container freel-main">
    <div class="order-1">
      <div class="c-table w-4/4">
        <div class="c-table__container">
          <div class="c-table__container__space">
            <!--コースリスト-->
            <table>
              <thead>
                <tr>
                  <th colspan="2" scope="col">{{$TabCourseName}}</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tabCourseList as $tabCourse)
                <tr>
                  <td name="tab_course_index">{{$loop->index + 1}}</td>
                  <td><a href="javascript:getTabCourseLink('{{$loop->index}}');">{{$tabCourse->course_name}}</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="order-0 typingOrder">
      <div class="p-typingscreen" id="p-typingscreen__startscreen">
        <div id="p-typingscreen__startscreen__upper__container">

          <div class="p-typingscreen__startscreen__upper__container_Inner">
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__roman" id="p-typingscreen__startscreen__roman" />
              <label for="p-typingscreen__startscreen__roman">ローマ字あり
              </label>
            </div>
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__roman" id="p-typingscreen__startscreen__noroman" />
              <label for="p-typingscreen__startscreen__noroman">なし</label>
            </div>
          </div>

          <div class="p-typingscreen__startscreen__upper__container_Inner soundRadio">
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__sound" />
              <label for="p-typingscreen__startscreen__sound">音あり</label>
            </div>
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__nosound" />
              <label for="p-typingscreen__startscreen__nosound">なし</label>
            </div>
          </div>

          <div class="p-typingscreen__startscreen__upper__container_Inner romanRadio">
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__upper" value="{{$course->course_level}}" />
              <label for="p-typingscreen__startscreen__upper">大文字</label>
            </div>
            <div class="radioOut">
              <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__lower" />
              <label for="p-typingscreen__startscreen__lower">ローマ字小文字</label>
            </div>
          </div>

        </div>
        <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingscreen__start" onclick="startBtnClick()">はじめる</button>
        <div class="p-typingscreen__count hidden" id="p-typingscreen__count"></div>
        <div class="p-typingscreen__kana">&nbsp;</div>
        <div class="p-typingscreen__mana">&nbsp;</div>
        <div class="p-typingscreen__romaji">
          <div class="p-typingscreen__romaji__typed"></div>
          <div class="p-typingscreen__romaji__untyped">&nbsp;</div>
        </div>
      </div>
      <div class="p-typingscreen hidden" id="p-typingscreen">
        <div class="p-typingscreen__kana" id="p-kana"></div>{{-- お題(仮名) --}}
        <div class="p-typingscreen__mana" id="p-mana"></div>{{-- お題(真名) --}}
        <div class="p-typingscreen__romaji">
          <div class="p-typingscreen__romaji__show" id="p-typing-show"></div>
          <span class="p-typingscreen__romaji__typed hidden" id="p-typed"></span><span class="p-typingscreen__romaji__untyped hidden" id="p-untyped"></span>
        </div>{{-- お題(真名) --}}
        <div class="p-typingscreen__successtype" id="p-successtype"></div>
        <div class="p-typingscreen__misstype" id="p-misstype"></div>
      </div>
      <div class="p-typingresult hidden" id="p-typingresult">
      {{-- 結果モーダル --}}
        <div class="p-typingresult__bg"></div>
        <div class="p-typingresult__card">
          <h3>結果</h3>
          <ul>
            <li><span>スコア</span>
              <div id="p-typingresult--score"></div><span>点</span><span id ="perfect"></span>
            </li>
            <li><span>時間</span>
              <div id="p-typingresult--time"></div><span>秒</span>
            </li>
            <li><span>打った数</span>
              <div id="p-typingresult--all"></div><span>回</span>
            </li>
            <li><span>間違った数</span>
              <div id="p-typingresult--miss"></div><span>回</span>
            </li>
          </ul>
          <input type="hidden" name="typingresult--score" value="">
          <input type="hidden" name="typingresult--time" value="">
          <input type="hidden" name="typingresult--all" value="">
          <input type="hidden" name="typingresult--miss" value="">
          <h4 class="mb-4"><strong>スペースキーでウィンドウがとじます。</strong></h4>
          <button type="button" id="typing_store_close" class="c-button--sm" onclick="restartBtnClick()">とじる</button>
        </div>
      </div>
      @include('layouts.include.keyboard')
      @include('layouts.include.finger')
      <div class="underBtn leftRightBtn">
        <button type="button" class="prevCourse" id="mae">前のコース</button>
        <button type="button" class="nextCourse" id="tugi">次のコース</button>
      </div>
      <button type="button" class="btn btn-primary stopBtn" id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
    </div>
  </div>

  @endsection