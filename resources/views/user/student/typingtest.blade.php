@extends('layouts.schooltyping')

@section('js')
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>

<script>
  let typeWordsAll = @json($wordData);
  let courseData = @json($course);
  let courseList = @json($coursesArray);
  const routeLink = "/";
  let isFree = false;
</script>
<script src="/js/typing-common.js?v=20240123"></script>
<script src="/js/typing-test.js?v=20240123"></script>
<script src="/js/keytouch-rank.js"></script>
<script>
  function getResutAjax() {
    AjaxInit(); 
    let data = {
      'typingScore': successType,
      'typingRank': scoreRank,
      'typingTime': resultModalTestTime,
      'courseId': currentCourseId
    };
    $.ajax({
      url: "/typing/typing-store",
      method: "POST",
      data: data,
      dataType: "json",
    }).done(function(typingDataJson) {
      $todayScoreDataJson = typingDataJson[0]
      $bestScoreDataJson = typingDataJson[1]

      var todaypoints = $("[id=todaypoint_tr]");
      for (var todaypoint of todaypoints) {
        $(todaypoint).remove()
      }
      $.each($todayScoreDataJson, function(index, value) { 
        if (value.rank == null) {
          value.rank = '';
        }
        $('#todaypoint').append('<tr id="todaypoint_tr"><td>' + value.point + '<span class="bestscore_rank">' + value.rank + '</span></td></tr>')
      })
      var bestpoints = $("[id=bestpoint_tr]");
      for (var bestpoint of bestpoints) {
        $(bestpoint).remove()
      }
      let bestPointCreatedAtFormat; 
      let bestpoint_no = 1;
      let addTodayIcon; 
      let typingTodayFormat = new Date().toLocaleDateString('sv-SE');
      $.each($bestScoreDataJson, function(index, value) {
        bestPointCreatedAtFormat = value.best_point_created_at.slice(0, 10); 
        if (typingTodayFormat == bestPointCreatedAtFormat) {
          addTodayIcon = '<span class="today">';
        } else {
          addTodayIcon = '';
        }
        if (value.rank == null) {
          value.rank = '';
        }
        $('#bestpoint').append('<tr id="bestpoint_tr"><td>' + bestpoint_no + '</td><td>' + value.best_point + '<span class="bestscore_rank">' + value.rank + '</span></td><td>' + bestPointCreatedAtFormat + addTodayIcon + '</td></tr>');
        bestpoint_no = bestpoint_no + 1;
      })
      AjaxEnd();
    }).fail(function() {
      alert('ログインの「有効期限切れ」か、通信エラーでスコアが保存できませんでした。ページを再読み込み（リロード）してください。');
    });
  }
  function maetugiBtnGetScoreData() {
    AjaxInit();
    let data = {
      'typingScore': null,
      'typingRank': null,
      'typingTime': null,
      'courseId': currentCourseId
    };
    $.ajax({
      url: "/typing/typing-store",
      method: "POST",
      data: data,
      dataType: "json",
    }).done(function(typingDataJson) {
      $todayScoreDataJson = typingDataJson[0] 
      $bestScoreDataJson = typingDataJson[1] 

      var todaypoints = $("[id=todaypoint_tr]"); 
      for (var todaypoint of todaypoints) {
        $(todaypoint).remove()
      }
      $.each($todayScoreDataJson, function(index, value) { 
        if (value.rank == null) {
          value.rank = '';
        }
        $('#todaypoint').append('<tr id="todaypoint_tr"><td>' + value.point + '<span class="bestscore_rank">' + value.rank + '</span></td></tr>')
      })
      var bestpoints = $("[id=bestpoint_tr]");
      for (var bestpoint of bestpoints) {
        $(bestpoint).remove()
      }
      let bestPointCreatedAtFormat; 
      let bestpoint_no = 1;
      let addTodayIcon; 
      let typingTodayFormat = new Date().toLocaleDateString('sv-SE');
      $.each($bestScoreDataJson, function(index, value) {
        bestPointCreatedAtFormat = value.best_point_created_at.slice(0, 10); 
        if (typingTodayFormat == bestPointCreatedAtFormat) {
          addTodayIcon = '<span class="today">';
        } else {
          addTodayIcon = '';
        }
        if (value.rank == null) {
          value.rank = '';
        }
        $('#bestpoint').append('<tr id="bestpoint_tr"><td>' + bestpoint_no + '</td><td>' + value.best_point + '<span class="bestscore_rank">' + value.rank + '</span></td><td>' + bestPointCreatedAtFormat + addTodayIcon + '</td></tr>');
        bestpoint_no = bestpoint_no + 1;
      })
      AjaxEnd(); 
    }).fail(function() {
      alert('ログインの「有効期限切れ」か、通信エラーでスコアが保存できませんでした。ページを再読み込み（リロード）してください。');
    });
  }
</script>
@endsection

@section('content')
<div class="l-main__container test__container">
  <!--ベストスコアと今日のスコア-->
  <div class="order-1">
    <div class="c-table w-3/4">
      <div class="c-table__container">
        <div class="c-table__container__space">
          <!--ベストスコア-->
          <table>
            <thead>
              <tr>
                <th colspan="3" scope="col">ベストスコア</th>
              </tr>
            </thead>
            <tbody id="bestpoint">
              @foreach ($scoreData as $score)
              <tr id="bestpoint_tr">
                <td id="bestpoint_td">{{$loop->index + 1}}</td>
                <td id="bestpoint_td">{{$score->best_point}}<span class="bestscore_rank">{{$score->rank}}</span></td>
                <td id="bestpoint_td">{{explode(' ',$score->best_point_created_at)[0]}}
                  @if ( explode(' ',$score->best_point_created_at)[0] == now()->format('Y-m-d') )
                  <span class="today">{{"今日" }}</span>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div><!--/c-table w-3/4-->
    <!--今日のスコア-->
    <div class="c-table w-1/4">
      <div class="c-table__container">
        <div class="c-table__container__space">
          <table>
            <thead style="position:sticky;top:0">
              <tr>
                <th scope="col" class="today" style="border:none">今日</th>
              </tr>
            </thead>
            <tbody id="todaypoint">
              @foreach ($todayScoreData as $score)
              <tr id="todaypoint_tr">
                <td id="todaypoint_td">{{$score->point}}<span class="bestscore_rank">{{$score->rank}}</span></td>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div><!--/c-table w-1/4"-->
  </div><!--/order-1"-->
  <div class="order-0">
    <div class="p-typingtestscreenOut">
      <div class="p-typingtestscreen" id="p-typingtestscreen__startscreen">
        <div class="p-typingtestscreen__flex">
          <div class="p-typingtestscreen__typenumber">入力： 0文字</div><!-- 入力タイプ数 -->
          <div class="p-typingtestscreen__time">残り時間： <span id="p-initial-time"></span></div><!-- 残り時間 -->
        </div>
        <div class="p-typingtestscreen__typesbg">
          {{--大文字小文字ボタン/ローマ字ありなしボタン追加--}}
          <div id="p-typingscreen__startscreen__optionbtn__container" class="romanRadio">
          <div class="radioOut">
                <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__upper"  />
                <label for="p-typingscreen__startscreen__upper">大文字</label>
              </div>
              <div class="radioOut">
                <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__lower" checked/>
                <label for="p-typingscreen__startscreen__lower">ローマ字小文字</label>
              </div>
          </div>
          <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingtestscreen__start" onclick="startTypingFunc()">はじめる</button>
          <div class="p-typingtestscreen__count hidden" id="p-typingscreen__count"></div>
          <div class="p-typingtestscreen__typesfield"></div>
        </div>
      </div>
      <div class="p-typingtestscreen hidden" id="p-typingtestscreen">
        <div class="p-typingtestscreen__flex">
          <div class="p-typingtestscreen__typenumber">入力： <span id="p-typenumber"></span>文字</div><!-- 入力タイプ数 -->
          <div class="p-typingtestscreen__time">残り時間： <span id="p-time"></span></div><!-- 残り時間 -->
        </div>
        <div class="p-typingtestscreen__typesbg">
          <div class="p-typingtestscreen__typesfield" id="p-typefield"></div>
        </div>
      </div>
      <div class="p-typingresult hidden" id="p-typingresult">
        {{-- 結果表示画面--}}
        <div class="p-typingresult__bg"></div>
        <div class="p-typingresult__card">
          <h3>結果</h3>
          <ul>
            <li><span>スコア</span>
              <div id="p-typingresult--score"></div><span>文字</span>
            </li>
            @if ( $course->course_category === 'キータッチ2000検定' )
            <li><span>級</span>
              <div id="p-typingresult--rank"></div>
            </li>
            @endif
            <li><span>残り時間</span>
              <div id="p-typingresult--time"></div>
            </li>
          </ul>
          <div class="p-typingtestscreen__typesbg">
            <div class="p-typingtestresultscreen__typesfield" id="p-typeresultfield"></div>
          </div>
          <button type="button" id="typing_store_close" class="c-button--sm" onclick="restartBtnClick()">とじる</button>
        </div>
      </div>
      @if ( $course->course_category === 'キータッチ2000検定' )
      @include('layouts.include.kyu-danlist')
      @endif
      <div class="underBtn plusUnder">
        <div class="underBtnInner">
          <button type="button" class="prevCourse btn btn-primary" id="mae">前のコース</button>
          <button type="button" class="btn btn-primary " id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
          <button type="button" class="nextCourse btn btn-primary" id="tugi">次のコース</button>
          <div id="keytouchRankInfoBtn">
            @if ($course->course_category === 'キータッチ2000検定')
            {{--級段の対応表\resources\views\components\rankTable.blade.php--}}
            @component('components.rankTable')
            @endcomponent
            @component('components.keytouchcaution')
            @endcomponent
            @endif
          </div>
        </div>
      </div>
    </div>
  </div><!--/order-0-->
</div>
@endsection