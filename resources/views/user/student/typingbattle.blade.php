@extends('layouts.schooltypingbattle')
@section('js')
<script>
  let typeWordsAll = @json($wordData);
  let courseData = @json($course);
  let courseList = @json($coursesArray);
  let sound = @json($soundData);
  let alphabet = @json($alphabetData);
  let scoretype=0;
  const routeLink ="/";
  let isFree = false;
</script>
<script src="/js/typing-data.js?v=20240529"></script>
<script src="/js/typing-common.js?v=20240529"></script>
<script src="/js/typing.js?v=20240529"></script>
<script src="/js/typing-score.js?v=20250308"></script>
<script src="/js/keyboard.js"></script>
<script src="/js/finger.js"></script>
<script src="/js/romanizer.js"></script>
<script src="/js/typing-sound.js"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script>

  function getResutAjax() {
    AjaxInit();
    let data = {
      'typingScore': resultypingScore,
      'typingTime': typingTime,
      'courseId': currentCourseId
    };
    $.ajax({
      url: "/typing/typing-store-battle",
      method: "POST",
      data: data,
      dataType: "json",
    }).done(function(typingDataJson) {
      let bestScoreDataJson = typingDataJson.scoreData;
       let totalScores = typingDataJson.totalScores;
       let myRank = typingDataJson.myRank;
       let myBestScore = typingDataJson.myBestScore;

      var bestpoints = $("[id=bestpoint_tr]");
      for (var bestpoint of bestpoints) {
        $(bestpoint).remove()
      }
      let day;
      let month;
      let parts;
      let originalData;
      let bestPointCreatedAtFormat;
      let rank = 1;
      let previousScore = null;
      $.each(bestScoreDataJson, function(index, value) {
          originalData = value.created_at;
          parts = originalData.split('T')[0].split('-');
          month = parts[1];
          day = parts[2];
          bestPointCreatedAtFormat = `${month}/${day}`;
          if (value.battle_score !== previousScore) {
              rank = index + 1;
          }
          $('#bestpoint').append('<tr id="bestpoint_tr"><td>' + rank + '</td><td>' + value.nickname + '</td><td>' + value.battle_score + '</td><td>' + bestPointCreatedAtFormat + '</td></tr>');
          previousScore = value.battle_score;
      });
      $('#myranking').html(`${myRank} 位（全スコア：${totalScores}件 ）`);
      var mypoints = $("[id=bestpoint_tr2]");
      for (var mypoint of mypoints) {
        $(mypoint).remove()
      }
      let day2;
      let month2;
      let parts2;
      let originalData2;
      let bestPointCreatedAtFormat2;
      let bestPointCreatedAtFormat3;
      let rank2 = 1;
      let previousScore2 = null;
      let typingTodayFormat = new Date().toLocaleDateString('sv-SE');
      $.each(myBestScore, function(index, value) {
          originalData2 = value.created_at;
          parts2 = originalData2.split('T')[0].split('-');
          month2 = parts2[1];
          day2 = parts2[2];
          bestPointCreatedAtFormat2 = `${month2}/${day2}`;
          bestPointCreatedAtFormat3 = value.created_at.slice(0, 10);
          if (typingTodayFormat == bestPointCreatedAtFormat3) {
            addTodayIcon = '<span class="today">';
          } else {
            addTodayIcon = '';
          }
          rank2 = index + 1;
          $('#bestpoint2').append('<tr id="bestpoint_tr2"><td>' + rank2 + '</td><td>' + value.battle_score + '</td><td>' + bestPointCreatedAtFormat2  + addTodayIcon +  '</td></tr>');
          previousScore2 = value.battle_score;
      });
      AjaxEnd();
    }).fail(function() {
      alert('ログインの「有効期限切れ」か、通信エラーでスコアが保存できませんでした。ページを再読み込み（リロード）してください。');
    });
  }
</script>
@endsection

@section('content')
<div class="l-main__container">
  <div class="order-2">
  <div class="c-table">
    <div class="c-table__container">
        <div class="c-table__container__space">
          <table>{{--マイランキング--}}
            <thead>
              <tr>
                <th colspan="4" scope="col"><span class="mybest">マイベストスコア</span></th>
              </tr>
              <tr>
                <th>順位</th>
                <th>スコア</th>
                <th>日付</th>
              </tr>
            </thead>
            <tbody id="bestpoint2">
              @foreach ($bestScore as $score)
              <tr id="bestpoint_tr2">
                <td id="bestpoint_td2">{{ $loop->index + 1 }}</td>
                <td id="bestpoint_td2">{{ $score->battle_score}}</td>
                <td id="bestpoint_td2">{{ \Carbon\Carbon::parse($score->created_at)->format('m/d') }}
                  @if ( explode(' ',$score->created_at)[0] == now()->format('Y-m-d') )
                  <span class="today">{{"今日"}}</span>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!--/order-2"-->
  <div class="order-0 typingOrder">
    <div class="p-typingscreen" id="p-typingscreen__startscreen">
      {{--大文字小文字ボタン/音ありなしボタン/ローマ字ありなしボタン追加--}}
      <div id="p-typingscreen__startscreen__upper__container">
        <div class="p-typingscreen__startscreen__upper__container_Inner">
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio__roman" id="p-typingscreen__startscreen__roman" />
          <label for="p-typingscreen__startscreen__roman">ローマ字あり
          </label>
        </div>
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio__roman" id="p-typingscreen__startscreen__noroman" />
          <label for="p-typingscreen__startscreen__noroman">なし</label>
        </div>
        </div>
        <div class="p-typingscreen__startscreen__upper__container_Inner soundRadio">
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__sound" />
          <label for="p-typingscreen__startscreen__sound">音あり</label>
        </div>
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__nosound" />
          <label for="p-typingscreen__startscreen__nosound">なし</label>
        </div>
        </div>
        <div class="p-typingscreen__startscreen__upper__container_Inner romanRadio">
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__upper" value="{{$course->course_level}}" />
          <label for="p-typingscreen__startscreen__upper">大文字</label>
        </div>
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__lower" />
          <label for="p-typingscreen__startscreen__lower">ローマ字小文字</label>
        </div>
        </div>
      </div>
      <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingscreen__start" onclick="startBtnClick()">はじめる</button>
      <div class="p-typingscreen__count hidden" id="p-typingscreen__count"></div>
      <div class="p-typingscreen__kana">&nbsp;</div>
      <div class="p-typingscreen__mana">&nbsp;</div>
      <div class="p-typingscreen__romaji">
        <div class="p-typingscreen__romaji__typed"></div>
        <div class="p-typingscreen__romaji__untyped">&nbsp;</div>
      </div>
    </div>
    <div class="p-typingscreen hidden" id="p-typingscreen">
      <div class="p-typingscreen__kana" id="p-kana"></div><!-- お題(仮名) -->
      <div class="p-typingscreen__mana" id="p-mana"></div><!-- お題(真名) -->
      <div class="p-typingscreen__romaji">
        <div class="p-typingscreen__romaji__show" id="p-typing-show"></div>
        <span class="p-typingscreen__romaji__typed hidden" id="p-typed"></span><span class="p-typingscreen__romaji__untyped hidden" id="p-untyped"></span>
      </div><!-- お題(ローマ字) -->
      <div class="p-typingscreen__successtype" id="p-successtype"></div><!-- 成功タイプ数 -->
      <div class="p-typingscreen__misstype" id="p-misstype"></div><!-- ミスタイプ数 -->
    </div>
    <div class="p-typingresult hidden" id="p-typingresult">
      <!-- 結果表示画面 -->
      <div class="p-typingresult__bg"></div>
      <div class="p-typingresult__card nextCourse">
        <h3>結果</h3>
        <ul>
          <li><span>ニックネーム：{{$user_nickname}}</span></li>
          <li><span>スコア</span>
            <div id="p-typingresult--score"></div><span>点</span><span id ="perfect"></span>
          </li>
          <li><span>時間</span>
            <div id="p-typingresult--time"></div><span>秒</span>
          </li>
          <li><span>打った数</span>
            <div id="p-typingresult--all"></div><span>回</span>
          </li>
          <li><span>間違った数</span>
            <div id="p-typingresult--miss"></div><span>回</span>
          </li>
        </ul>
        <h4 class="mb-4"><strong>スペースキーでウィンドウがとじます。</strong></h4>
        <button id="typing_store_close" class="c-button--sm" onclick="restartBtnClick()">とじる</button>
      </div>
      <!--/p-typingresult__card-->
    </div>
    <!--p-typingresult hidden-->
    <!-- タイピングキーボード -->
    <div id="keyboard-area-init"><img src="/img/keyboard-init.png" alt="タイピング初期画面"></div>
    <div class="p-keyboard hidden" id="keyboard-area">
      <!-- 1段目 -->
      <div class="p-keyboard__key--normal" id="p-keyboard__key--zh">
        <!-- 半角/全角 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--one">
        <!-- 1 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--two">
        <!-- 2 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--three">
        <!-- 3 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--four">
        <!-- 4 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--five">
        <!-- 5 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--six">
        <!-- 6 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--seven">
        <!-- 7 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--eight">
        <!-- 8 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--nine">
        <!-- 9 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--zero">
        <!-- 0 -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--hypen">
        <!-- - -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--tilde">
        <!-- ^ -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--en">
        <!-- ¥ -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--del">
        <!-- del -->
      </div>
      <!-- 2段目 -->
      <div class="p-keyboard__key--md" id="p-keyboard__key--tab">
        <!-- tab -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--q">
        <!-- q -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--w">
        <!-- w -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--e">
        <!-- e -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--r">
        <!-- r -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--t">
        <!-- t -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--y">
        <!-- y -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--u">
        <!-- u -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--i">
        <!-- i -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--o">
        <!-- o -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--p">
        <!-- p -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--atmark">
        <!-- @ -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--lbracket">
        <!-- [ -->
      </div>
      <div class="p-keyboard__key--enter" id="p-keyboard__key--enter">
        <!-- enter -->
      </div>
      <!-- 3段目 -->
      <div class="p-keyboard__key--lg" id="p-keyboard__key--control">
        <!-- control -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--a">
        <!-- a -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--s">
        <!-- s -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--d">
        <!-- d -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--f">
        <!-- f -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--g">
        <!-- g -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--h">
        <!-- h -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--j">
        <!-- j -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--k">
        <!-- k -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--l">
        <!-- l -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--semicolon">
        <!-- ; -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--colon">
        <!-- : -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--rbracket">
        <!-- ] -->
      </div>
      <div class="p-keyboard__margin--md"></div>
      <!-- 4段目 -->
      <div class="p-keyboard__key--xl" id="p-keyboard__key--lshift">
        <!-- lshift -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--z">
        <!-- z -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--x">
        <!-- x -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--c">
        <!-- c -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--v">
        <!-- v -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--b">
        <!-- b -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--n">
        <!-- n -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--m">
        <!-- m -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--comma">
        <!-- , -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--period">
        <!-- . -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--slash">
        <!-- / -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--underscore">
        <!-- _ -->
      </div>
      <div class="p-keyboard__key--lg" id="p-keyboard__key--rshift">
        <!-- rshift -->
      </div>
      <!-- 5段目 -->
      <div class="p-keyboard__margin--lg"></div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--loption">
        <!-- loption -->
      </div>
      <div class="p-keyboard__key--md" id="p-keyboard__key--lcommnad">
        <!-- lcommnad -->
      </div>
      <div class="p-keyboard__key--max" id="p-keyboard__key--space">
        <!-- space -->
      </div>
      <div class="p-keyboard__key--md" id="p-keyboard__key--rcommnad">
        <!-- rcommnad -->
      </div>
      <div class="p-keyboard__key--normal" id="p-keyboard__key--roption">
        <!-- roption -->
      </div>
      <div class="p-keyboard__margin--lg"></div>
    </div>
    <div class="p-hands mt-4">
      <!-- 手 -->
      <!-- 左手 -->
      <div class="p-fingers">
        <div class="p-fingers__finger--md" id="p-fingers__finger--llittle"></div><!-- 小指 -->
        <div class="p-fingers__finger--lg" id="p-fingers__finger--lthird"></div><!-- 薬指 -->
        <div class="p-fingers__finger--xl" id="p-fingers__finger--lsecond"></div><!-- 中指 -->
        <div class="p-fingers__finger--lg" id="p-fingers__finger--lfirst"></div><!-- 人差し指 -->
        <div class="p-fingers__finger--sm ml-2" id="p-fingers__finger--lthumb"></div><!-- 親指 -->
      </div>
      <!-- 右手 -->
      <div class="p-fingers">
        <div class="p-fingers__finger--sm mr-2" id="p-fingers__finger--rthumb"></div><!-- 親指 -->
        <div class="p-fingers__finger--lg" id="p-fingers__finger--rfirst"></div><!-- 人差し指 -->
        <div class="p-fingers__finger--xl" id="p-fingers__finger--rsecond"></div><!-- 中指 -->
        <div class="p-fingers__finger--lg" id="p-fingers__finger--rthird"></div><!-- 薬指 -->
        <div class="p-fingers__finger--md" id="p-fingers__finger--rlittle"></div><!-- 小指 -->
      </div>
    </div><!-- /手 -->
    <div class="underBtn leftRightBtn">
      <button type="button" class="prevCourse" id="mae">前のコース</button>
      <button type="button" class="nextCourse" id="tugi">次のコース</button>
    </div>
    <button class="btn btn-primary stopBtn" id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
  </div>
  <!--/order-0-->
  <div class="order-1">
    <div class="c-table">
      <div class="c-table__container">
        <div class="c-table__container__space">
          <table>{{--月間ランキング--}}
            <thead>
              <tr>
                <th colspan="4" scope="col">{{$month}}月のランキング</th>
              </tr>
              <tr>
                <th>順位</th>
                <th>ニックネーム</th>
                <th>スコア</th>
                <th>日付</th>
              </tr>
            </thead>
              <tbody id="bestpoint">
                @php
                    $myRank=null;
                    $myRankFlg=0;
                    $rank = 1; // 初期順位
                    $previousScore = null; // 前のスコアを保持する変数
                    $displayRank = 1; // 表示する順位
                @endphp

                @foreach ($battleScore as $score)
                <tr id="bestpoint_tr">
                  <td id="bestpoint_td">
                      @if ($score->battle_score === $previousScore)
                          {{ $displayRank }}
                      @else
                          {{ $rank }}
                          @php
                              $displayRank = $rank; // 表示する順位を更新
                          @endphp
                      @endif
                      @if ($score->user_id == $userId && $myRankFlg == 0)
                      @php
                      $myRank=$displayRank;
                      $myRankFlg++;
                      @endphp
                      @endif
                  </td>
                  <td id="bestpoint_td">{{ $score->nickname }}</td>
                  <td id="bestpoint_td">{{ $score->battle_score }}</td>
                  <td id="bestpoint_td">{{ \Carbon\Carbon::parse($score->created_at)->format('m/d') }}</td>
                </tr>
                  @php
                    $previousScore = $score->battle_score; // 現在のスコアを前のスコアとして保持
                    $rank++; // ループ内で常に順位をインクリメント
                  @endphp
                @endforeach
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  <!--/order-1"-->
  <div id="myranking" class="myranking">{{--順位表示--}}
    @if($myRank != null)
      <span>{{$myRank}}位<small>（全スコア：{{$battleScoreCount}}件）</small></span>
    @endif
  </div>
  <!--/myranking"-->
</div>
@endsection