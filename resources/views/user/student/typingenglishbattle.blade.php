@extends('layouts.schooltypingbattle')
@section('js')
<script>
  let typeWordsAll = @json($wordData);
  let courseData = @json($course);
  let courseList = @json($coursesArray);
  let sound = @json($soundData);
  let alphabet = @json($alphabetData);
  let scoretype="{{$scoretype}}";
  const routeLink ='/';
  const isFree = false;
</script>{{--順番変えない事--}}
<script src="/js/typing-data.js?v=20240123"></script>
<script src="/js/typing-common.js?v=20240123"></script>
<script src="/js/typing-english.js?v=20240917"></script>
<script src="/js/typing-score-english.js"></script>
<script src="/js/romanizer.js"></script>
<script src="/js/typing-sound.js"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script>
  function getResutAjax() {
    AjaxInit();
    let data = {
      'typingScore': resultypingScore,
      'typingTime': typingTime,
      'courseId': currentCourseId
    };
    $.ajax({
      url: "/typing/typing-store-battle",
      method: "POST",
      data: data,
      dataType: "json",
    }).done(function(typingDataJson) {
       let bestScoreDataJson = typingDataJson.scoreData;
       let totalScores = typingDataJson.totalScores;
       let myRank = typingDataJson.myRank;
       let myBestScore = typingDataJson.myBestScore;

      var bestpoints = $("[id=bestpoint_tr]");
      for (var bestpoint of bestpoints) {
        $(bestpoint).remove()
      }
      let day;
      let month;
      let parts;
      let originalData;
      let bestPointCreatedAtFormat;
      let rank = 1;
      let previousScore = null;
      $.each(bestScoreDataJson, function(index, value) {
          originalData = value.created_at;
          parts = originalData.split('T')[0].split('-');
          month = parts[1];
          day = parts[2];
          bestPointCreatedAtFormat = `${month}/${day}`;
          if (value.battle_score !== previousScore) {
              rank = index + 1;
          }
          $('#bestpoint').append('<tr id="bestpoint_tr"><td>' + rank + '</td><td>' + value.nickname + '</td><td>' + value.battle_score + '</td><td>' + bestPointCreatedAtFormat + '</td></tr>');
          previousScore = value.battle_score;
      });
      $('#myranking').html(`${myRank} 位（全スコア：${totalScores}件 ）`);

      var mypoints = $("[id=bestpoint_tr2]");
      for (var mypoint of mypoints) {
        $(mypoint).remove()
      }
      let day2;
      let month2;
      let parts2;
      let originalData2;
      let bestPointCreatedAtFormat2;
      let bestPointCreatedAtFormat3;
      let rank2 = 1;
      let previousScore2 = null;
      let typingTodayFormat = new Date().toLocaleDateString('sv-SE');
      $.each(myBestScore, function(index, value) {
          originalData2 = value.created_at;
          parts2 = originalData2.split('T')[0].split('-');
          month2 = parts2[1];
          day2 = parts2[2];
          bestPointCreatedAtFormat2 = `${month2}/${day2}`;
          bestPointCreatedAtFormat3 = value.created_at.slice(0, 10);
          if (typingTodayFormat == bestPointCreatedAtFormat3) {
            addTodayIcon = '<span class="today">';
          } else {
            addTodayIcon = '';
          }
          rank2 = index + 1;
          $('#bestpoint2').append('<tr id="bestpoint_tr2"><td>' + rank2 + '</td><td>' + value.battle_score + '</td><td>' + bestPointCreatedAtFormat2  + addTodayIcon +  '</td></tr>');
          previousScore2 = value.battle_score;
      });

      AjaxEnd();
    }).fail(function() {
      alert('ログインの「有効期限切れ」か、通信エラーでスコアが保存できませんでした。ページを再読み込み（リロード）してください。');
    });
  }
</script>
@endsection
@section('content')
<div class="l-main__container">
  <div class="order-2">
  <div class="c-table">
    <div class="c-table__container">
        <div class="c-table__container__space">
          <table>{{--マイランキング--}}
            <thead>
              <tr>
                <th colspan="4" scope="col"><span class="mybest">マイベストスコア</span></th>
              </tr>
              <tr>
                <th>順位</th>
                <th>スコア</th>
                <th>日付</th>
              </tr>
            </thead>
            <tbody id="bestpoint2">
              @foreach ($bestScore as $score)
              <tr id="bestpoint_tr2">
                <td id="bestpoint_td2">{{ $loop->index + 1 }}</td>
                <td id="bestpoint_td2">{{ $score->battle_score}}</td>
                <td id="bestpoint_td2">{{ \Carbon\Carbon::parse($score->created_at)->format('m/d') }}
                  @if ( explode(' ',$score->created_at)[0] == now()->format('Y-m-d') )
                  <span class="today">{{"今日"}}</span>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!--/order-2-->
  <div class="order-0 typingOrder typingOrderEnglish">
    <div class="p-typingscreen" id="p-typingscreen__startscreen">
      <div id="p-typingscreen__startscreen__option__container">
        <div id="p-typingscreen__startscreen__sound__container" class="soundRadio">
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__sound" />
          <label for="p-typingscreen__startscreen__sound">音あり</label>
        </div>
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__nosound" />
          <label for="p-typingscreen__startscreen__nosound">なし</label>
        </div>
        </div>
        <div id="p-typingscreen__startscreen__english__container">
          <input type="checkbox" id="p-typingscreen__startscreen__english" onclick="challengeBtnClick()"><label for="p-typingscreen__startscreen__english"></label>
        </div>
      </div>
      <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingscreen__start" onclick="startBtnClick()">はじめる</button>
      <div class="p-typingscreen__count hidden" id="p-typingscreen__count"></div>
      <div class="p-typingscreen__kana">&nbsp;</div>
      <div class="p-typingscreen__mana">&nbsp;</div>
      <div class="p-typingscreen__romaji">
        <div class="p-typingscreen__romaji__typed"></div>
        <div class="p-typingscreen__romaji__untyped">&nbsp;</div>
      </div>
    </div>
    <div class="p-typingscreen hidden" id="p-typingscreen">
      <div class="p-typingscreen__kana" id="p-kana"></div>
      <div class="p-typingscreen__mana" id="p-mana"></div>
      <div class="p-typingscreen__romaji">
        <div class="p-typingscreen__romaji__show" id="p-typing-show"></div>
        <span class="p-typingscreen__romaji__typed hidden" id="p-typed"></span><span class="p-typingscreen__romaji__untyped hidden" id="p-untyped"></span>
      </div>
      <div class="p-typingscreen__successtype" id="p-successtype"></div>
      <div class="p-typingscreen__misstype" id="p-misstype"></div>
    </div>
    <div class="p-typingresult hidden" id="p-typingresult">
      {{-- 結果表示画面 --}}
      <div class="p-typingresult__bg"></div>
      <div class="p-typingresult__card">
        <h3>結果</h3>
        <ul>
          <li><span>ニックネーム：{{$user_nickname}}</span></li>
          <li><span>スコア</span>
            <div id="p-typingresult--score"></div><span>点</span><span id ="perfect"></span>
          </li>
          <li><span>時間</span>
            <div id="p-typingresult--time"></div><span>秒</span>
          </li>
          <li><span>打った数</span>
            <div id="p-typingresult--all"></div><span>回</span>
          </li>
          <li><span>間違った数</span>
            <div id="p-typingresult--miss"></div><span>回</span>
          </li>
        </ul>
        <h4 class="mb-4"><strong>スペースキーでウィンドウがとじます。</strong></h4>
        <button type="button" id="typing_store_close" class="c-button--sm" onclick="restartBtnClick()">とじる</button>
      </div>
      <!--/p-typingresult__card-->
    </div>
    {{--このエリアが地図画像表示エリア、jsで画像切り替える--}}
    <div id="map">
      <div id="map_img"></div>
      <div id="map_top"></div>
    </div>
    <div class="underBtn leftRightBtn">
      <button type="button" class="prevCourse" id="mae">前のコース</button>
      <button type="button" class="nextCourse" id="tugi">次のコース</button>
    </div>
    <button type="button" class="btn btn-primary stopBtn" id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
  </div>
  <!--/order-0-->
  <div class="order-1">
    <div class="c-table">
      <div class="c-table__container">
        <div class="c-table__container__space">
          <table>{{--月間ランキング--}}
            <thead>
              <tr>
                <th colspan="4" scope="col">{{$month}}月のランキング</th>
              </tr>
              <tr>
                <th>順位</th>
                <th>ニックネーム</th>
                <th>スコア</th>
                <th>日付</th>
              </tr>
            </thead>
              <tbody id="bestpoint">
                @php
                    $myRank=null;
                    $myRankFlg=0;
                    $rank = 1; // 初期順位
                    $previousScore = null; // 前のスコアを保持する変数
                    $displayRank = 1; // 表示する順位
                @endphp

                @foreach ($battleScore as $score)
                <tr id="bestpoint_tr">
                  <td id="bestpoint_td">
                      @if ($score->battle_score === $previousScore)
                          {{ $displayRank }}
                      @else
                          {{ $rank }}
                          @php
                              $displayRank = $rank; // 表示する順位を更新
                          @endphp
                      @endif
                      @if ($score->user_id == $userId && $myRankFlg == 0)
                      @php
                      $myRank=$displayRank;
                      $myRankFlg++;
                      @endphp
                      @endif
                  </td>
                  <td id="bestpoint_td">{{ $score->nickname }}</td>
                  <td id="bestpoint_td">{{ $score->battle_score }}</td>
                  <td id="bestpoint_td">{{ \Carbon\Carbon::parse($score->created_at)->format('m/d') }}</td>
                </tr>
                  @php
                    $previousScore = $score->battle_score; // 現在のスコアを前のスコアとして保持
                    $rank++; // ループ内で常に順位をインクリメント
                  @endphp
                @endforeach
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  <!--/order-1-->
  <div id="myranking" class="myranking">{{--順位表示--}}
    @if($myRank != null)
      <span>{{$myRank}}位<small>（全スコア：{{$battleScoreCount}}件）</small></span>
    @endif
  </div>
  <!--/myranking"-->
</div>
@endsection