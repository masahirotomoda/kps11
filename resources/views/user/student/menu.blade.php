@extends('layouts.base')
@section('js')
<script>
  const nyumon=@json($nyumon);
  const cyukyu=@json($cyukyu);
  const jokyu=@json($jokyu);
  let userNickname=@json($userNickname);
  let isFree = false;
</script>
<script src="/js/alphabet.js?v=20240616"></script>
<script src="/js/bootstrap.min.js"></script>{{--bootstrapはCDNでなくサーバー設置--}}
@endsection
@section('content')

    <div class="container teacher-top">
        <div id="typing" class="pageLinkBlock">
            <div class="mainBlock freeTop">
            {{Form::open(['url' => '/school-typingcourse','method' => 'post']) }}
                <div class="menuSpaces">
                    <div class="menuSpaces">
                        <div class="menuSpacesCell">
                            <div class="menuSpacesOne roSm">
                                <div class="radioOut">
                                    {{Form::radio('sound','on',($sound == 'yes'? true:false),['id' =>'sound1'])}}
                                    <label for="sound1">音あり</label>
                                </div>
                                <div class="radioOut">
                                    {{ Form::radio('sound','off',($sound == 'no'? true:false),['id' =>'sound2'])}}
                                    <label for="sound2">音なし</label>
                                </div>
                            </div>
{{--                          <div class="menuSpacesOne roSm">
                                <div class="radioOut">
                                    {{ Form::radio('alpha','on',($alpha == 'yes' ? true:false),['id'=>'alpha1'])}}
                                    <label for="alpha1">アルファベットあり</label>
                                </div>
                            <div class="radioOut">
                                {{ Form::radio('alpha','off',($alpha == 'no' ? true:false),['id'=>'alpha2'])}}
                                <label for="alpha2">なし</label>
                            </div>
                        </div>
--}}
                    </div>
                </div>
                <div class="menuSpaces rightBox rightSpaces linkSmall">
                    <div class="manualSpace links bottomBox">
                        @component('components.modalimage')
                        @slot('word', '使い方')
                        @slot('image_name', 'typing1')
                        @endcomponent
                    </div>
                    <div class="manualSpace links bottomBox">
                        @component('components.modalimage')
                        @slot('word', 'テストモード')
                        @slot('image_name', 'typing2')
                        @endcomponent
                    </div>
                    <div class="manualSpace links bottomBox">
                        @component('components.modalimage')
                        @slot('word', 'キータッチ2000')
                        @slot('image_name', 'typing3')
                        @endcomponent
                    </div>
                    @if($school->trial==0)
                    <div class="battleBtnArea">
                        <button id="battle_Btn" class="battleBtn" type="submit" name="battleBtn">ランキングスタジアム</button>
                    </div>
                    @endif
                </div>{{--今日のラッキーコースボタン--}}            
                <div class="randam-course-btn-outer">
                    <h2><span>{{$newstitle}}</span></h2>
                    <div class="randam-course-btn">
                        <button type="submit" id="luckybtn_entry" class="btn btn-outline-primary btn-sm" onclick="luckyBtnEntry()">入門</button>
                        <button type="submit" id="luckybtn_middle" class="btn btn-outline-primary btn-sm" onclick="luckyBtnMiddle()">中級</button>
                        <button type="submit" id="luckybtn_advance" class="btn btn-outline-primary btn-sm" onclick="luckyBtnAdvance()">上級</button>
                    </div>
                </div>            
            </div>
            <div class="typingText"></div>
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a href="#tab1" class="nav-link {{$currentmonth_tab_active}}" data-toggle="tab"><font color="#ff9900">今月の<br>コース</font></a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab2" class="nav-link {{$star1_tab_active}}" data-toggle="tab">レベル<br>★</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab3" class="nav-link {{$star2_tab_active}}" data-toggle="tab">レベル<br>★★</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab4" class="nav-link {{$star3_tab_active}}" data-toggle="tab">レベル<br>★★★</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab5" class="nav-link {{$test_tab_active}}" data-toggle="tab"><font color="#ff9900">テスト<br>モード</font></a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab6" class="nav-link {{$tuika_tab_active}}" data-toggle="tab">追加<br>コース</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab7" class="nav-link {{$keytouch_tab_active}}" data-toggle="tab"><font color="#ff9900">ｷｰﾀｯﾁ<br>2000</font></a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab8" class="nav-link {{$long_tab_active}}" data-toggle="tab"><font color="#ff9900">長文</font></a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab9" class="nav-link {{$map_tab_active}}" data-toggle="tab"><font color="#ff9900">地図</font></a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab10" class="nav-link {{$english1_tab_active}}" data-toggle="tab">英語１</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab11" class="nav-link {{$english2_tab_active}}" data-toggle="tab">英語２</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab12" class="nav-link {{$english3_tab_active}}" data-toggle="tab">英語３</font></a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab14" class="nav-link {{$english4_tab_active}}" data-toggle="tab">英語４</a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab15" class="nav-link {{$english5_tab_active}}" data-toggle="tab">英語５</a>
                    </li>
                    @if($school->logo==1)
                    <li class="nav-item">
                        <a href="#tab13" class="nav-link {{$vld_tab_active}}" data-toggle="tab"><font color="#ff9900">VLD</font></a>
                    </li>
                    @endif
                </ul>            
            <div class="tab-content">{{--タブ全体--}}
                <div id="tab1" class="tab-pane {{$currentmonth_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses1 as $index => $course1)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course1->id}}" value="{{$course1->course_name}}" /></td>                         
                                <td>{{ $course1->best_point }}</td>
                                <td>{{ explode(' ',$course1->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="tab2" class="tab-pane {{$star1_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses2 as $index => $course2)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course2->id}}" value="{{$course2->course_name}}" /></td>
                                <td> {{ $course2->best_point }}</td>
                                <td>{{ explode(' ',$course2->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="tab3" class="tab-pane {{$star2_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses3 as $index => $course3)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course3->id}}" value="{{$course3->course_name}}" /></td>
                                <td>{{ $course3->best_point }}</td>
                                <td>{{ explode(' ',$course3->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="tab4" class="tab-pane {{$star3_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses4 as $index => $course4)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course4->id}}" value="{{$course4->course_name}}" /></td>
                                <td>{{ $course4->best_point }}</td>
                                <td>{{ explode(' ',$course4->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="tab5" class="tab-pane {{$test_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses5 as $index => $course5)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course5->id}}" value="{{$course5->course_name}}" /></td>
                                <td>{{ $course5->best_point }}</td>
                                <td>{{ explode(' ',$course5->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="tab6" class="tab-pane {{$tuika_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>@if(!empty($courses6)){{--追加コースの登録がない学校は、この処理はしない--}}
                            @foreach ($courses6 as $index=> $course6)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course6->id}}" value="{{$course6->course_name}}" /></td>
                                <td>{{ $course6->best_point }}</td>
                                <td>{{ explode(' ',$course6->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div id="tab7" class="tab-pane {{$keytouch_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses7 as $index => $course7)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course7->id}}" value="{{$course7->course_name}}" /></td>
                                <td>{{ $course7->best_point }}</td>
                                <td>{{ explode(' ',$course7->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
                <div id="tab8" class="tab-pane {{$long_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses8 as $index => $course8)  
                               {{-- @if(isset($rankCourses[$index]['id']) && $rankCourses[$index]['id']==$course8->id )                            
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td><input type="submit" name="{{$course8->id}}" value="{{$course8->course_name}}" /></td>
                                        @if($course8->best_point==null)
                                            <td>{{ $course8->best_point }}</td>
                                        @else
                                            <td>{{ $course8->best_point }}（{{$position[$index]}}位）</td>
                                        @endif
                                        <td>{{ explode(' ',$course8->best_point_created_at)[0] }}</td>
                                    </tr>
                                @else --}}                           
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td><input type="submit" name="{{$course8->id}}" value="{{$course8->course_name}}" /></td>
                                        <td>{{ $course8->best_point }}</td>
                                        <td>{{ explode(' ',$course8->best_point_created_at)[0] }}</td>
                                    </tr>
                                {{--@endif  --}}                          
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
                <div id="tab9" class="tab-pane {{$map_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses9 as $index => $course9)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course9->id}}" value="{{$course9->course_name}}" /></td>
                                <td>{{ $course9->best_point }}</td>
                                <td>{{ explode(' ',$course9->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="tab10" class="tab-pane {{$english1_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses10 as $index => $course10)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course10->id}}" value="{{$course10->course_name}}" /></td>
                                <td>{{ $course10->best_point }}</td>
                                <td>{{ explode(' ',$course10->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="tab11" class="tab-pane {{$english2_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses11 as $index => $course11)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course11->id}}" value="{{$course11->course_name}}" /></td>
                                <td>{{ $course11->best_point }}</td>
                                <td>{{ explode(' ',$course11->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> 
                <div id="tab12" class="tab-pane {{$english3_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses12 as $index => $course12)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course12->id}}" value="{{$course12->course_name}}" /></td>
                                <td>{{ $course12->best_point }}</td>
                                <td>{{ explode(' ',$course12->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="tab14" class="tab-pane {{$english4_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses14 as $index => $course14)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course14->id}}" value="{{$course14->course_name}}" /></td>
                                <td>{{ $course14->best_point }}</td>
                                <td>{{ explode(' ',$course14->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="tab15" class="tab-pane {{$english5_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses15 as $index => $course15)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course15->id}}" value="{{$course15->course_name}}" /></td>
                                <td>{{ $course15->best_point }}</td>
                                <td>{{ explode(' ',$course15->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if($school->logo==1)
                <div id="tab13" class="tab-pane {{$vld_tab_active}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">コース名</th>
                                <th scope="col">最高点</th>
                                <th scope="col">タイピング日</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses13 as $index => $course13)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><input type="submit" name="{{$course13->id}}" value="{{$course13->course_name}}" /></td>
                                <td>{{ $course13->best_point }}</td>
                                <td>{{ explode(' ',$course13->best_point_created_at)[0] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>{{--/tab-content タブ全体--}}
            <div class="typingText noUpper">
                <p>iPad、タブレットの場合はキーボードを接続してご利用ください。</p>
            </div>
        </div>{{--/mainBlock--}}
    </div> {{--/pageLinkBlock--}}
    {{Form::close()}}
    <div id="typingSp" class="pageLinkBlock">
        <div class="mainBlock">
            <div class="typingText">
                <p>タイピングの練習はPC・タブレット（キーボード接続）でご利用ください。</p>
            </div>
        </div>
    </div>
</div>{{--/container teacher-top--}}
@endsection