{{--トライアル専用の生徒マイメニュー（学年クラスの変更なし）--}}
@extends('layouts.base')

@section('js')
<script src="/js/password_btn_null.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_password_update.js"></script>
@endsection

@section('content')

{{--alertmessageファイルにhiddenがあるため、formはここからスタート--}}
{{ Form::open(['url' => '/student/userpas-change', 'method' => 'post','autocomplete' => 'off','autocapitalize' => 'none']) }}
{{ Form::hidden('id', $user->id) }}
@include('layouts.include.flashmessage')
<table class="table" id="twolineLeftth">
    <tbody>
        <tr>
            <th scope="row">生徒名</th>
            <td>
                {{ Form::text('student_name',$user->user_name, ['class' => 'form-control', 'maxlength' => '20','readonly' ]) }}
            </td>
        </tr>   
        <tr>
            <th scope="row">学年・クラス</th>
            <td>
                <span>※トライアルのため、学年、組は変更できません</span>
                <select name="grade" disabled><option value="select_gradename" >学年をえらぶ</option></select>
                <select name="kumi" disabled><option value="select_kuminame" >クラスをえらぶ</option></select>     
            </td>
        </tr>           
        <tr>
            <th scope="row">ログインID</th>
            <td>{{ $user->login_account }}</td>
        </tr>
        <tr>
            <th scope="row">パスワード<br>（セキュリティのため、非表示）</th>
            <td>
                @component('components.changeOnePaswd')
                @slot('passwordbtn', 'パスワードを変える')
                @slot('passwordcomment', 'パスワードは数字4～6文字です') 
                @slot('maxlength', '6')
                @endcomponent
                <span>※パスワードは数字の４～６文字です</span>            
            </td>
        </tr>
    </tbody>
</table>
@include('layouts.include.alertmessage')
{{ Form::close() }}
@endsection