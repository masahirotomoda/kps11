@extends('layouts.base')

@section('js')
<script src="/js/password_btn_null.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_student_save.js"></script>
@endsection

@section('content'){{--トライアルページは別--}}
@php
    //小学1年、2年生はひらがな表記
    if( $user->grade_name == "1年" ||  $user->grade_name == "2年" ){
        $Name = "なまえ";
        $ClassInfo = "がくねん・くみ・しゅっせきばんごう（数字でさいだい4もじ）";
        $PasswordComment = "パスワードはみえないようにしています";
        $SaveBtn = "ほぞんする";
        $passwordbtn = "パスワードをかえる";
        $modal_title = "パスワードは数字４～６ケタで入力しよう";
        $alert_title = "ほぞんできませんでした。もう一度、にゅうりょくしよう。";
    }else{
        $Name = "名前";
        $ClassInfo = "学年・組・出席番号(数字で最大4桁)";
        $PasswordComment = "セキュリティのため、パスワードは非表示";
        $SaveBtn = "保存する";
        $passwordbtn = "パスワードを変える";
        $modal_title = "パスワードの変更（数字４～６ケタ）";
        $alert_title = "保存できませんでした。もう一度、入力しよう。";
    }
    //アバター
    $avatar = $avatar ?? null; // 既定値は null
@endphp
@include('layouts.include.flashmessage'){{--保存成功時のフラッシュメッセージ--}}
<table class="table tstable" id="twolineLeftth">
    <tbody>
        <tr>
            <th scope="row">{{ $Name }}</th>
            <td><font color=#808080> {{ $user->user_name }}</font></td>
        </tr>
        <tr>
            <th scope="row">{{ $ClassInfo }}<br>公開ニックネーム<span class="red"><small>※最大10文字</small></span><br>アイコン</th>
            <td>
                {{ Form::open(['url' => '/student/profile-update', 'method' => 'post','autocomplete' => 'off','autocapitalize' => 'none','form' => 'studentinfo_form']) }}
                <div class="menuSpaces">
                    {{ Form::select('nd_grade_selection',$grade_selection,$user->grade_id,['class' => 'form-select','id' => 'nd_grade_selection']) }}
                    {{ Form::select('nd_kumi_selection',$kumi_selection,$user->kumi_id,['class' => 'form-select','id' => 'nd_kumi_selection']) }}
                    {{ Form::text('attendance_no',$user->attendance_no,  ['class' => 'form-control', 'id' => 'attendance_no', 'maxlength' => '4']) }}
                    <div class="nickname">
						<div class="nicknameInner">
                            <span class="nicknameText">公開ニックネーム</span>
                    {{ Form::text('user_nickname', $user->user_nickname, ['class' => 'form-control', 'id' => 'user_nickname', 'maxlength' => '10', 'placeholder' => 'ニックネーム']) }}
                        </div>
					</div>
                    <div class="radio-group selectChara">
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_green.webp" alt="アズ"></span>
                            <input type="radio" id="radio-green" name="selected_avatar" value="green" {{ $avatar == 'green' ? 'checked' : '' }}>
                            <label for="green">アズ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_blue.webp" alt="カイ"></span>
                            <input type="radio" id="radio-blue" name="selected_avatar" value="blue" {{ $avatar == 'blue' ? 'checked' : '' }}>
                            <label for="blue">カイ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_pink.webp" alt="ルナ"></span>
                            <input type="radio" id="radio-pink" name="selected_avatar" value="pink" {{ $avatar == 'pink' ? 'checked' : '' }}>
                            <label for="pink">ルナ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_red.webp" alt="サン"></span>
                            <input type="radio" id="radio-red" name="selected_avatar" value="red" {{ $avatar == 'red' ? 'checked' : '' }}>
                            <label for="red">サン</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_white.webp" alt="スイ"></span>
                            <input type="radio" id="radio-white" name="selected_avatar" value="white" {{ $avatar == 'white' ? 'checked' : '' }}>
                            <label for="white">スイ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_yellow.webp" alt="ジェイ"></span>
                            <input type="radio" id="radio-yellow" name="selected_avatar" value="yellow" {{ $avatar == 'yellow' ? 'checked' : '' }}>
                            <label for="yellow">ジェイ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_black.webp" alt="タイピン１号"></span>
                            <input type="radio" id="radio-black" name="selected_avatar" value="black" {{ $avatar == 'black' || is_null($avatar) ? 'checked' : '' }}>
                            <label for="black">タイピン１号</label>
                        </div>
                    </div>
                    {{Form::submit( $SaveBtn, ['name'=>'student_name_change_btn','class'=>'btn btn-secondary btn-sm','id'=>'studentinfo_update_btn'])}}
                </div>
                {{ Form::close() }}
            </td>
        </tr>
        <tr>
            <th scope="row">ログインID<span class="red"><small>（変えられません）</small></span></th>
            <td><font color=#808080>　{{ $user->login_account }}</font></td>
        </tr>
        <tr>
            <th scope="row">パスワード<br><small>（{{ $PasswordComment }}）</small></th>
            <td>
            {{ Form::open(['url' => '/student/userpas-change', 'method' => 'post','autocomplete' => 'off','autocapitalize' => 'none']) }}{{-- パスワード確認 onclickで入力欄クリア --}}
            <button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#modalreset" id="reset" onclick="clear_text()">{{$passwordbtn}}</button>
            {{-- 生徒用は、変則的なので、コンポーネントのモーダルウィンドウをつかわない --}}
            <div class="modal fade" id="modalreset" tabindex="-1" role="dialog" aria-labelledby="modalreset-label" aria-hidden="false">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5>
                            {{$modal_title}}
                            </h5>
                        </div>
                        <div>
                            <div class="modalCell">
                                {{ Form::text('change_password',null, ['class' => 'form-control', 'id' => 'change_password', 'autocomplete' => 'off' ,'maxlength' => 6 ,'placeholder' => 'あたらしいパスワード']) }}
                            </div>
                            <div class="modalCell">
                                {{ Form::text('change_password_confirmation',null, ['class' => 'form-control', 'id' => 'change_password_confirmation', 'autocomplete' => 'off' ,'maxlength' => 6 ,'placeholder' => 'あたらしいパスワード(確認)']) }}
                            </div>
                            <div class="modalCell">
                                <p>{{ $PasswordComment }}</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                キャンセル
                            </button>
                            <button type="submit" name="user_password_reset_btn" class="btn btn-danger" id="change">
                            {{ $passwordbtn }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>{{--★生徒用アラートモーダルは、変則的なので、コンポーネントつかわない。バリデーションエラー時、モーダル表示 addnew_all.js--}}
<button type="button"  data-toggle="modal" data-target="#alertmodal" id="alertbtn">　</button>
<div class="modal fade" id="alertmodal" tabindex="-1" role="dialog" aria-labelledby="flashmessage-label" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>
                    {{$alert_title}}
                </h5>
            </div>
            <div class="modalCell">
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
            </div>
            <div class="closeOuter">
                <button type="button" class="btn btn-secondary close" data-dismiss="modal" aria-label="Close">閉じる</button>
            </div>
        </div>
    </div>
</div>{{--hiddenにバリデーションエラーの有無を持たせる⇒エラーがあれば、addnew_all.js モーダルメッセージ表示--}}
@if($errors->all() == null)
{{form::hidden('is_error','no',['id'=>'is_error'])}}
@else
{{form::hidden('is_error','yes',['id'=>'is_error'])}}
@endif
@endsection