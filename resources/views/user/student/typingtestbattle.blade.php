@extends('layouts.schooltypingbattle')

@section('js')
<script src="/js/jquery-3.6.0.js"></script>
<script src="/js/common.js?v=20240123"></script>

<script>
  let typeWordsAll = @json($wordData);
  let courseData = @json($course);
  let courseList = @json($coursesArray);
  const routeLink = "/";
  let isFree = false;
</script>
<script src="/js/typing-common.js?v=20240123"></script>
<script src="/js/typing-test.js?v=20240123"></script>
<script src="/js/keytouch-rank.js"></script>
<script>
  function getResutAjax() {
    AjaxInit();
    let data = {
      'typingScore': successType,
      'courseId': currentCourseId
    };
    $.ajax({
      url: "/typing/typing-store-battle",
      method: "POST",
      data: data,
      dataType: "json",
    }).done(function(typingDataJson) {
       let bestScoreDataJson = typingDataJson.scoreData;
       let totalScores = typingDataJson.totalScores;
       let myRank = typingDataJson.myRank;
       let myBestScore = typingDataJson.myBestScore;

      var bestpoints = $("[id=bestpoint_tr]");
      for (var bestpoint of bestpoints) {
        $(bestpoint).remove()
      }
      let day;
      let month;
      let parts;
      let originalData;
      let bestPointCreatedAtFormat;
      let rank = 1;
      let previousScore = null;
      $.each(bestScoreDataJson, function(index, value) {
          originalData = value.created_at;
          parts = originalData.split('T')[0].split('-');
          month = parts[1];
          day = parts[2];
          bestPointCreatedAtFormat = `${month}/${day}`;
          if (value.battle_score !== previousScore) {
              rank = index + 1;
          }
          $('#bestpoint').append('<tr id="bestpoint_tr"><td>' + rank + '</td><td>' + value.nickname + '</td><td>' + value.battle_score + '</td><td>' + bestPointCreatedAtFormat + '</td></tr>');
          previousScore = value.battle_score;
      });
      $('#myranking').html(`${myRank} 位（全スコア：${totalScores}件 ）`);

      var mypoints = $("[id=bestpoint_tr2]");
      for (var mypoint of mypoints) {
        $(mypoint).remove()
      }
      let day2;
      let month2;
      let parts2;
      let originalData2;
      let bestPointCreatedAtFormat2;
      let bestPointCreatedAtFormat3;
      let rank2 = 1;
      let previousScore2 = null;
      let typingTodayFormat = new Date().toLocaleDateString('sv-SE');
      $.each(myBestScore, function(index, value) {
          originalData2 = value.created_at;
          parts2 = originalData2.split('T')[0].split('-');
          month2 = parts2[1];
          day2 = parts2[2];
          bestPointCreatedAtFormat2 = `${month2}/${day2}`;
          bestPointCreatedAtFormat3 = value.created_at.slice(0, 10);
          if (typingTodayFormat == bestPointCreatedAtFormat3) {
            addTodayIcon = '<span class="today">';
          } else {
            addTodayIcon = '';
          }
          rank2 = index + 1;
          $('#bestpoint2').append('<tr id="bestpoint_tr2"><td>' + rank2 + '</td><td>' + value.battle_score + '</td><td>' + bestPointCreatedAtFormat2  + addTodayIcon +  '</td></tr>');
          previousScore2 = value.battle_score;
      });
      AjaxEnd();
    }).fail(function() {
      alert('ログインの「有効期限切れ」か、通信エラーでスコアが保存できませんでした。ページを再読み込み（リロード）してください。');
    });
  }
</script>
@endsection
@section('content')
<div class="l-main__container">
  <div class="order-2">
  <div class="c-table">
    <div class="c-table__container">
        <div class="c-table__container__space">
          <table>{{--マイランキング--}}
            <thead>
              <tr>
                <th colspan="4" scope="col"><span class="mybest">マイベストスコア</span></th>
              </tr>
              <tr>
                <th>順位</th>
                <th>スコア</th>
                <th>日付</th>
              </tr>
            </thead>
            <tbody id="bestpoint2">
              @foreach ($bestScore as $score)
              <tr id="bestpoint_tr2">
                <td id="bestpoint_td2">{{ $loop->index + 1 }}</td>
                <td id="bestpoint_td2">{{ $score->battle_score}}</td>
                <td id="bestpoint_td2">{{ \Carbon\Carbon::parse($score->created_at)->format('m/d') }}
                  @if ( explode(' ',$score->created_at)[0] == now()->format('Y-m-d') )
                  <span class="today">{{"今日"}}</span>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!--/order-2-->
  <div class="order-0">
    <div class="p-typingtestscreenOut">
      <div class="p-typingtestscreen" id="p-typingtestscreen__startscreen">
        <div class="p-typingtestscreen__flex">
          <div class="p-typingtestscreen__typenumber">入力： 0文字</div><!-- 入力タイプ数 -->
          <div class="p-typingtestscreen__time">残り時間： <span id="p-initial-time"></span></div><!-- 残り時間 -->
        </div>
        <div class="p-typingtestscreen__typesbg">
          {{--大文字小文字ボタン/ローマ字ありなしボタン追加--}}
          <div id="p-typingscreen__startscreen__optionbtn__container" class="romanRadio">
          <div class="radioOut">
                <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__upper"  />
                <label for="p-typingscreen__startscreen__upper">大文字</label>
              </div>
              <div class="radioOut">
                <input type="radio" name="p-typingscreen__radio" id="p-typingscreen__startscreen__lower" checked/>
                <label for="p-typingscreen__startscreen__lower">ローマ字小文字</label>
              </div>
          </div>
          <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingtestscreen__start" onclick="startTypingFunc()">はじめる</button>
          <div class="p-typingtestscreen__count hidden" id="p-typingscreen__count"></div>
          <div class="p-typingtestscreen__typesfield"></div>
        </div>
      </div>
      <div class="p-typingtestscreen hidden" id="p-typingtestscreen">
        <div class="p-typingtestscreen__flex">
          <div class="p-typingtestscreen__typenumber">入力： <span id="p-typenumber"></span>文字</div><!-- 入力タイプ数 -->
          <div class="p-typingtestscreen__time">残り時間： <span id="p-time"></span></div><!-- 残り時間 -->
        </div>
        <div class="p-typingtestscreen__typesbg">
          <div class="p-typingtestscreen__typesfield" id="p-typefield"></div>
        </div>
      </div>
      <div class="p-typingresult hidden" id="p-typingresult">
        {{-- 結果表示画面--}}
        <div class="p-typingresult__bg"></div>
        <div class="p-typingresult__card">
          <h3>結果</h3>
          <ul>
            <li><span>ニックネーム：{{$user_nickname}}</span></li>
            <li><span>スコア</span>
              <div id="p-typingresult--score"></div><span>文字</span>
            </li>
            @if ( $course->course_category === 'キータッチ2000検定' )
            <li><span>級</span>
              <div id="p-typingresult--rank"></div>
            </li>
            @endif
            <li><span>残り時間</span>
              <div id="p-typingresult--time"></div>
            </li>
          </ul>
          <div class="p-typingtestscreen__typesbg">
            <div class="p-typingtestresultscreen__typesfield" id="p-typeresultfield"></div>
          </div>
          <button type="button" id="typing_store_close" class="c-button--sm" onclick="restartBtnClick()">とじる</button>
        </div>
      </div>
      @if ( $course->course_category === 'キータッチ2000検定' )
      @include('layouts.include.kyu-danlist')
      @endif
      <div class="underBtn plusUnder">
        <div class="underBtnInner">
          <button type="button" class="prevCourse btn btn-primary" id="mae">前のコース</button>
          <button type="button" class="btn btn-primary " id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
          <button type="button" class="nextCourse btn btn-primary" id="tugi">次のコース</button>
          <div id="keytouchRankInfoBtn">
            @if ($course->course_category === 'キータッチ2000検定')
            {{--級段の対応表\resources\views\components\rankTable.blade.php--}}
            @component('components.rankTable')
            @endcomponent
            @component('components.keytouchcaution')
            @endcomponent
            @endif
          </div>
        </div>
      </div>
    </div>
  </div><!--/order-0-->
  <div class="order-1">
    <div class="c-table">
      <div class="c-table__container">
        <div class="c-table__container__space">
          <table>{{--月間ランキング--}}
            <thead>
              <tr>
                <th colspan="4" scope="col">{{$month}}月のランキング</th>
              </tr>
              <tr>
                <th>順位</th>
                <th>ニックネーム</th>
                <th>スコア</th>
                <th>日付</th>
              </tr>
            </thead>
              <tbody id="bestpoint">
                @php
                    $myRank=null;
                    $myRankFlg=0;
                    $rank = 1; // 初期順位
                    $previousScore = null; // 前のスコアを保持する変数
                    $displayRank = 1; // 表示する順位
                @endphp

                @foreach ($battleScore as $score)
                <tr id="bestpoint_tr">
                  <td id="bestpoint_td">
                      @if ($score->battle_score === $previousScore)
                          {{ $displayRank }}
                      @else
                          {{ $rank }}
                          @php
                              $displayRank = $rank; // 表示する順位を更新
                          @endphp
                      @endif
                      @if ($score->user_id == $userId && $myRankFlg == 0)
                      @php
                      $myRank=$displayRank;
                      $myRankFlg++;
                      @endphp
                      @endif
                  </td>
                  <td id="bestpoint_td">{{ $score->nickname }}</td>
                  <td id="bestpoint_td">{{ $score->battle_score }}</td>
                  <td id="bestpoint_td">{{ \Carbon\Carbon::parse($score->created_at)->format('m/d') }}</td>
                </tr>
                  @php
                    $previousScore = $score->battle_score; // 現在のスコアを前のスコアとして保持
                    $rank++; // ループ内で常に順位をインクリメント
                  @endphp
                @endforeach
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  <!--/order-1-->
  <div id="myranking" class="myranking">{{--順位表示--}}
    @if($myRank != null)
      <span>{{$myRank}}位<small>（全スコア：{{$battleScoreCount}}件）</small></span>
    @endif
  </div>
  <!--/myranking"-->
</div>
@endsection