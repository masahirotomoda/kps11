@extends('layouts.schooltyping')
@section('js')
<script>
  let typeWordsAll = @json($wordData);
  let courseData = @json($course);
  let courseList = @json($coursesArray);
  let sound = @json($soundData);
  let alphabet = @json($alphabetData);
  let scoretype="{{$scoretype}}";
  const routeLink ='/';
  const isFree = false;
</script>{{--順番変えない事--}}
<script src="/js/typing-data.js?v=20240123"></script>
<script src="/js/typing-common.js?v=20240123"></script>
<script src="/js/typing-english.js?v=20240917"></script>
<script src="/js/typing-score-english.js"></script>
<script src="/js/romanizer.js"></script>
<script src="/js/typing-sound.js"></script>
<script src="/js/jquery-3.6.0.js"></script>
<script>
  function getResutAjax() {
    AjaxInit(); 
    let data = {
      'typingScore': resultypingScore,
      'typingTime': typingTime,
      //'keydownAllCnt': keydownAllCnt,
      //'missType': missType,
      'courseId': currentCourseId
    };
    $.ajax({
      url: "/typing/typing-store",
      method: "POST",
      data: data,
      dataType: "json",
    }).done(function(typingDataJson) {
      $todayScoreDataJson = typingDataJson[0] 
      $bestScoreDataJson = typingDataJson[1]
      var todaypoints = $("[id=todaypoint_tr]");
      for (var todaypoint of todaypoints) {
        $(todaypoint).remove() 
      }
      $.each($todayScoreDataJson, function(index, value) { 
        $('#todaypoint').append('<tr id="todaypoint_tr"><td>' + value.point + '</td></tr>')
      })
      var bestpoints = $("[id=bestpoint_tr]");
      for (var bestpoint of bestpoints) {
        $(bestpoint).remove()
      }
      let bestPointCreatedAtFormat; 
      let bestpoint_no = 1;
      let addTodayIcon; 
      let typingTodayFormat = new Date().toLocaleDateString('sv-SE');
      $.each($bestScoreDataJson, function(index, value) {
        bestPointCreatedAtFormat = value.best_point_created_at.slice(0, 10); 
        if (typingTodayFormat == bestPointCreatedAtFormat) {
          addTodayIcon = '<span class="today">';
        } else {
          addTodayIcon = '';
        }
        $('#bestpoint').append('<tr id="bestpoint_tr"><td>' + bestpoint_no + '</td><td>' + value.best_point + '</td><td>' + bestPointCreatedAtFormat + addTodayIcon + '</td></tr>');
        bestpoint_no = bestpoint_no + 1;
      })
      AjaxEnd();
    }).fail(function() {
      alert('ログインの「有効期限切れ」か、通信エラーでスコアが保存できませんでした。ページを再読み込み（リロード）してください。');
    });
  }
  function maetugiBtnGetScoreData() {
    AjaxInit();
    let data = {
      'typingScore': null,
      'typingTime': null,
      //'keydownAllCnt': null,
      //'missType': null,
      'courseId': currentCourseId
    };
    $.ajax({
      url: "/typing/typing-store",
      method: "POST",
      data: data,
      dataType: "json",
    }).done(function(typingDataJson) {
      $todayScoreDataJson = typingDataJson[0] 
      $bestScoreDataJson = typingDataJson[1] 
      var todaypoints = $("[id=todaypoint_tr]"); 
      for (var todaypoint of todaypoints) {
        $(todaypoint).remove() 
      }
      $.each($todayScoreDataJson, function(index, value) { 
        $('#todaypoint').append('<tr id="todaypoint_tr"><td>' + value.point + '</td></tr>')
      })
      var bestpoints = $("[id=bestpoint_tr]");
      for (var bestpoint of bestpoints) {
        $(bestpoint).remove()
      }
      let bestPointCreatedAtFormat; 
      let bestpoint_no = 1;
      let addTodayIcon; 
      let typingTodayFormat = new Date().toLocaleDateString('sv-SE');
      $.each($bestScoreDataJson, function(index, value) {
        bestPointCreatedAtFormat = value.best_point_created_at.slice(0, 10); 
        if (typingTodayFormat == bestPointCreatedAtFormat) {
          addTodayIcon = '<span class="today">';
        } else {
          addTodayIcon = '';
        }
        $('#bestpoint').append('<tr id="bestpoint_tr"><td>' + bestpoint_no + '</td><td>' + value.best_point + '</td><td>' + bestPointCreatedAtFormat + addTodayIcon + '</td></tr>');
        bestpoint_no = bestpoint_no + 1;
      })
      AjaxEnd();
    }).fail(function() {
      alert('ログインの「有効期限切れ」か、通信エラーでスコアが保存できませんでした。ページを再読み込み（リロード）してください。');
    });
  }
</script>
@endsection
@section('content')
<div class="l-main__container">
  <!--ベストスコアと今日のスコア-->
  <div class="order-1">
    <div class="c-table w-3/4">
      <div class="c-table__container">
        <div class="c-table__container__space">
          <!--ベストスコア-->
          <table>
            <thead>
              <tr>
                <th colspan="3" scope="col">ベストスコア</th>
              </tr>
            </thead>
            <tbody id="bestpoint">
              @foreach ($scoreData as $score)
              <tr id="bestpoint_tr">
                <td id="bestpoint_td">{{ $loop->index + 1 }}</td>
                <td id="bestpoint_td">{{ $score->best_point }}</td>
                <td id="bestpoint_td">{{ explode(' ',$score->best_point_created_at)[0] }}
                  @if ( explode(' ',$score->best_point_created_at)[0] == now()->format('Y-m-d') )
                  <span class="today">{{"今日" }}</span>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/c-table w-3/4-->
    <!--今日のスコア-->
    <div class="c-table w-1/4">
      <div class="c-table__container">
        <div class="c-table__container__space">
          <table>
            <thead style="position:sticky;top:0">
              <tr>
                <th scope="col" style="border:none">今日</th>
              </tr>
            </thead>
            <tbody id="todaypoint">
              @foreach ($todayScoreData as $score)
              <tr id="todaypoint_tr">
                <td id="todaypoint_td">{{ $score->point }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/c-table w-1/4"-->
  </div>
  <!--/order-1"-->
  <div class="order-0 typingOrder typingOrderEnglish">
    <div class="p-typingscreen" id="p-typingscreen__startscreen">
      {{--音ありなし/チャレンジボタン追加--}}
      <div id="p-typingscreen__startscreen__option__container">
        <div id="p-typingscreen__startscreen__sound__container" class="soundRadio">
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__sound" />
          <label for="p-typingscreen__startscreen__sound">音あり</label>
        </div>
        <div class="radioOut">
          <input type="radio" name="p-typingscreen__radio__sound" id="p-typingscreen__startscreen__nosound" />
          <label for="p-typingscreen__startscreen__nosound">なし</label>
        </div>
        </div>
        <div id="p-typingscreen__startscreen__english__container">
          <input type="checkbox" id="p-typingscreen__startscreen__english" onclick="challengeBtnClick()"><label for="p-typingscreen__startscreen__english"></label>
        </div>
      </div>
      <button class="p-typingscreen__start c-button--sm" type="button" id="p-typingscreen__start" onclick="startBtnClick()">はじめる</button>
      <div class="p-typingscreen__count hidden" id="p-typingscreen__count"></div>
      <div class="p-typingscreen__kana">&nbsp;</div>
      <div class="p-typingscreen__mana">&nbsp;</div>
      <div class="p-typingscreen__romaji">
        <div class="p-typingscreen__romaji__typed"></div>
        <div class="p-typingscreen__romaji__untyped">&nbsp;</div>
      </div>
    </div>
    <div class="p-typingscreen hidden" id="p-typingscreen">
      <div class="p-typingscreen__kana" id="p-kana"></div>
      <div class="p-typingscreen__mana" id="p-mana"></div>
      <div class="p-typingscreen__romaji">
        <div class="p-typingscreen__romaji__show" id="p-typing-show"></div>
        <span class="p-typingscreen__romaji__typed hidden" id="p-typed"></span><span class="p-typingscreen__romaji__untyped hidden" id="p-untyped"></span>
      </div>
      <div class="p-typingscreen__successtype" id="p-successtype"></div>
      <div class="p-typingscreen__misstype" id="p-misstype"></div>
    </div>
    <div class="p-typingresult hidden" id="p-typingresult">
      {{-- 結果表示画面 --}}
      <div class="p-typingresult__bg"></div>
      <div class="p-typingresult__card">
        <h3>結果</h3>
        <ul>
          <li><span>スコア</span>
            <div id="p-typingresult--score"></div><span>点</span><span id ="perfect"></span>
          </li>
          <li><span>時間</span>
            <div id="p-typingresult--time"></div><span>秒</span>
          </li>
          <li><span>打った数</span>
            <div id="p-typingresult--all"></div><span>回</span>
          </li>
          <li><span>間違った数</span>
            <div id="p-typingresult--miss"></div><span>回</span>
          </li>
        </ul>
        <h4 class="mb-4"><strong>スペースキーでウィンドウがとじます。</strong></h4>
        <button type="button" id="typing_store_close" class="c-button--sm" onclick="restartBtnClick()">とじる</button>
      </div>
      <!--/p-typingresult__card-->
    </div>
    {{--このエリアが地図画像表示エリア、jsで画像切り替える--}}
    <div id="map">
      <div id="map_img"></div>
      <div id="map_top"></div>
    </div>
    <div class="underBtn leftRightBtn">
      <button type="button" class="prevCourse" id="mae">前のコース</button>
      <button type="button" class="nextCourse" id="tugi">次のコース</button>
    </div>
    <button type="button" class="btn btn-primary stopBtn" id="restart_Btn" onclick="restartBtnClick()">途中でやめる</button>
  </div>
  <!--/order-0-->
</div>
@endsection