@extends('layouts.base')

@section('js')
    <script src="/js/dateedit.js"></script>
    <script src="/js/checkall-dim.js"></script>
    <script src="/js/addnew_all.js"></script>
    <script src="/js/btn_excelexport.js"></script>
    <script src="/js/btn_pdfprint.js"></script>
    <script src="/js/btn_all_reset.js"></script>
@endsection

@section('content')
    @include('layouts.include.transcriptprint')
@endsection


