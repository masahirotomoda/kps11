@extends('layouts.base')
{{--新規ボタン押下で新1行追加、出席番頭、名前にrequiedをつけると、保存ボタンが非アクティブになる⇒必須項目はバリデーションのみで対応 --}}
@section('js')
<script src="/js/user/teacher/addnew_student.js"></script>
<script src="/js/user/teacher/checkedit_student.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_all_reset.js"></script>
<script src="/js/btn_password_reset.js"></script>

@endsection

@section('content')
{{ Form::open(['url' => '/teacher/student-search', 'method' => 'post']) }}

@include('layouts.include.flashmessage')
<div class="menuSpaces">
    <div class="menuSpacesCell">
        {{ Form::select('grade', $grade_selection, $grade_select, ['class' => 'form-select','id' => 'grade','placeholder' => '学年']) }}
        {{ Form::select('kumi', $kumi_selection, $kumi_select, ['class' => 'form-select','id' => 'kumi','placeholder' => '組']) }}
        {{ Form::text('username', $user_name, ['class' => 'form-control', 'id' => 'user_name','maxlength' => '20', 'placeholder' => '生徒名の一部']) }}
        @include('layouts.include.searchbtn')
        <button class="btn btn-outline-primary" id="ｎ" type="button" onclick="location.href='./student-list'">操作のキャンセル</button>
    </div>
</div>
{{ Form::close() }}
<div class="menuSpaces">
<button class="btn btn-secondary btn-sm" type="button" name="add" id="add">生徒を登録する</button>
    <div class="infotext">
        <p class="largeTexts"><span>全生徒数</span><strong>{{ $student_num}}</strong>人</p>
        @if($student_num !==  $users->total())
        <p class="largeTexts"><span>検索後の生徒数</span><strong>{{ $users->total()}}</strong>人</p>
        @endif
        <p><small>※生徒削除はできません。</small></p>
        <div class="manualSpace links">
        @component('components.modalimage')
        @slot('word', '新入生の登録')
        @slot('image_name', 'teacher28')
        @endcomponent
        @component('components.modalimage')
        @slot('word', 'パスワードリセット')
        @slot('image_name', 'teacher29')
        @endcomponent
        </div>
    </div>
</div>
@include('layouts.include.alertmessage')
@include('layouts.include.pagination')
<table class="table studentlist" id="mainTable">
    <thead>
        <tr>
            <th scope="col">
                @component('components.tooltiprequired')
                    @slot('word', '学年')
                    @slot('message', '学年がない時は「なし」を選ぶ')
                @endcomponent

            </th>
            <th scope="col">
                @component('components.tooltiprequired')
                    @slot('word', '組')
                    @slot('message', '組がない時は「なし」を選ぶ')
                @endcomponent
            </th>
            <th scope="col">
                @component('components.tooltiprequired')
                    @slot('word', '出席No')
                    @slot('message', '数字（最大4桁、ない時は「0」を入力）')
                @endcomponent
            </th>
            <th scope="col">生徒名<span class="red">*</span><small>（最大20文字）</small></th>
            <th scope="col">編集・保存</th>
            <th scope="col">
                @component('components.tooltip')
                    @slot('word', 'ﾛｸﾞｲﾝID')
                    @slot('message', 'ログインIDは変更不可')
                @endcomponent
            </th>
            <th scope="col">パスワード初期値<br>{{$school->password_student_init}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        {{ Form::open(['url' => '/teacher/student-list-update','method'=>'post','class'=>'d-flex p-1','style'=>'width:82%'])}}
        {{ Form::hidden('user_id', $user->user_id)}}
        <tr>
            <td> {{ Form::select('grade_id',$grade_selection,$user->grade_id,['class' => 'form-select','id' => 's_grade' ,'disabled']) }}</td>
            <td>{{ Form::select('kumi_id',$kumi_selection,$user->kumi_id,['class' => 'form-select','id' => 's_kumi','disabled']) }}</td>
            <td>{{ Form::text('attendance_no',$user->attendance_no,['class' => 'form-control col-xs-2', 'id' => 'attendance_no','maxlength' => '4','readonly']) }}</td>
            <td>{{ Form::text('user_name', $user->user_name, ['class' => 'form-control col-xs-2', 'id' => 'user_name','maxlength' => '20','readonly']) }}</td>
            <td>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>
            </td>
            <td>{{ $user->login_account }}</td>
            <td>
                {{ Form::close() }}
                @if($school->password_display === 1)
                @component('components.displayPaswd')
                @slot('controller', 'posts')
                @slot('id', $user->user_id)
                @slot('password',$user->password)
                @slot('name', '')
                @endcomponent
                @endif

                @component('components.confirmReset')
                @slot('controller', 'posts')
                @slot('url', '/teacher/userpas-reset')
                @slot('id', $user->user_id)
                @slot('school_id', $user->school_id)
                @slot('init_password',$school->password_student_init)
                @slot('name', $user->user_name)
                @endcomponent
            </td>
        </tr>
        @endforeach
    </tbody>
</table>{{--新規作成で1行追加した時、学年、組のセレクトボックスはここをクローンして作成される--}}
<div class="hidden">
    {{ Form::select('nd_grade_selection',$grade_selection,null,['class' => 'form-select','id' => 'nd_grade_selection', 'placeholder' => '学年選択']) }}
    {{ Form::select('nd_kumi_selection',$kumi_selection,null,['class' => 'form-select','id' => 'nd_kumi_selection', 'placeholder' => '組選択']) }}
</div>
@endsection
