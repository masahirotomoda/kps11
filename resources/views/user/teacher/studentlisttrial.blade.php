@extends('layouts.base')

@section('js')
<script src="/js/flashmessage_display.js"></script>
<script src="/js/user/teacher/checkedit_studenttrial.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_password_reset.js"></script>
@endsection

@section('content')
{{ Form::open(['url' => '/teacher/students-search', 'method' => 'post']) }}
@include('layouts.include.flashmessage')
<div class="menuSpaces">
    <div class="menuSpaces textat">
        <p><strong>トライアル中は「クラス機能」「生徒の新規登録」「削除」が制限されています。</strong></p>
    </div>
    <div class="manualSpace links links-sm rightBox">
        @component('components.modalimage')
        @slot('word', 'この画面の使い方')
        @slot('image_name', 'trial1')
        @endcomponent
    </div>
</div>
<div class="menuSpaces">
    <div class="menuSpacesCell">
        <select name="grade" disabled><option value="select_gradename" >学年を選択</option></select>
        <select name="kumi" disabled><option value="select_kuminame" >クラスを選択</option></select>
        {{ Form::text('username', $user_name, ['class' => 'form-control','maxlength' => '20', 'placeholder' => '生徒名の一部']) }}
        @include('layouts.include.searchbtn')
        <button disabled="disabled" class="btn btn-outline-primary btn-sm" type="button" >生徒を登録する</button>
    </div>
</div>
{{ Form::close() }}
@include('layouts.include.alertmessage')
<div class="infotext">
    @if($aftersearch_user_count == 0)
    <p>データがありません。</p>
    @else
    <p>全{{$user_count}}人中　{{$aftersearch_user_count}}人を表示。</p>
    @endif
</div>
<table class="table studentlisttrial" id="mainTable">
    <thead>
        <tr>
            <th scope="col">
                @component('components.tooltiprequired')
                @slot('word', '生徒名')
                @slot('message', '必須、最大20文字')
                @endcomponent
            </th>
            <th scope="col">
                @component('components.tooltip')
                @slot('word', 'ログインID')
                @slot('message', 'ログインIDは変更できません')
                @endcomponent
            </th>
            <th scope="col">編集・保存</th>
            <th scope="col">
                @component('components.tooltip')
                @slot('word', 'パスワードリセット')
                @slot('message', 'パスワードの変更はは生徒自身（マイメニュー）でおこないます。先生はリセットのみ。')
                @endcomponent
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        {{ Form::open(['url' => '/teacher/students-update', 'method' => 'post']) }}
        {{ Form::hidden('user_id', $user->user_id)}}
        <tr>
            <td>{{ Form::text('user_name', $user->user_name, ['class' => 'form-control col-xs-2', 'id' => 'user_name','maxlength' => '20','readonly']) }}</td>
            <td>{{ $user->login_account }}</td>
            <td>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>
            </td>
            {{ Form::close() }}
            {{ Form::open(['url' => '/teacher/students-pas-reset', 'files' => true , 'method' => 'post']) }}
            <td>{{--トライアルはパスワード確認ボタンは非表示--}}
                @component('components.confirmReset')
                @slot('controller', 'posts')
                @slot('url', '/teacher/userpas-reset')
                @slot('id', $user->user_id)
                @slot('school_id', $user->school_id)
                @slot('init_password',$school->password_student_init)
                @slot('name', $user->user_name)
                @endcomponent
            </td>
        </tr>
        {{ Form::close() }}
        @endforeach
    </tbody>
</table>
@endsection
