@extends('layouts.base')

@section('js')
<script src="/js/password_btn_null.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_password_update.js"></script>
<script src="/js/user/teacher/btn_name_update.js"></script>
@endsection

@section('content')

@include('layouts.include.flashmessage')
@php_check_syntax//アバター
    $avatar = $avatar ?? null; // 既定値は null
@endphp
<table class="table tablethfead" id="mainTable">
    <tr>
        <th>ログインID<small>（変更不可）</small></th>
        <th>先生名<span class="red">*</span>　公開ニックネーム</th>
        <th>パスワード変更</th>
    </tr>
        <td>{{ $user->login_account }}</td>
        <td>{{--alertmessageファイルにhiddenがあるため、formはここからスタート--}}
            {{ Form::open(['url' => '/teacher/profile-update', 'method' => 'post','autocomplete' => 'off','autocapitalize' => 'none']) }}
            @include('layouts.include.alertmessage')
            {{--requiredで文字数制限したいが、ボタン押下でdisabledにすると、無入力時に、エラーなのにdisabledでボタンが押せなくなるため--}}
            <p>変更後の名前(最大20文字)を入力してください。</p>
            <input type="text" name="name" id="name" class ="form-control" value="{{ $user->name}}" autocomplete="off" maxlength="20">
            <div class="nickname">
                <div class="nicknameInner">
                    <span class="nicknameText">公開ニックネーム</span>
            {{ Form::text('user_nickname', $user->user_nickname, ['class' => 'form-control', 'id' => 'user_nickname', 'maxlength' => '10', 'placeholder' => '公開ニックネーム（最大10文字）例：たろう123']) }}
                </div>
            </div>
                    <div class="radio-group selectChara">
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_green.webp" alt="アズ"></span>
                            <input type="radio" id="radio-green" name="selected_avatar" value="green" {{ $avatar == 'green' ? 'checked' : '' }}>
                            <label for="green">アズ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_blue.webp" alt="カイ"></span>
                            <input type="radio" id="radio-blue" name="selected_avatar" value="blue" {{ $avatar == 'blue' ? 'checked' : '' }}>
                            <label for="blue">カイ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_pink.webp" alt="ルナ"></span>
                            <input type="radio" id="radio-pink" name="selected_avatar" value="pink" {{ $avatar == 'pink' ? 'checked' : '' }}>
                            <label for="pink">ルナ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_red.webp" alt="サン"></span>
                            <input type="radio" id="radio-red" name="selected_avatar" value="red" {{ $avatar == 'red' ? 'checked' : '' }}>
                            <label for="red">サン</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_white.webp" alt="スイ"></span>
                            <input type="radio" id="radio-white" name="selected_avatar" value="white" {{ $avatar == 'white' ? 'checked' : '' }}>
                            <label for="white">スイ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_yellow.webp" alt="ジェイ"></span>
                            <input type="radio" id="radio-yellow" name="selected_avatar" value="yellow" {{ $avatar == 'yellow' ? 'checked' : '' }}>
                            <label for="yellow">ジェイ</label>
                        </div>
                        <div class="radio-item">
                            <span><img src="../img/typingstudium/select_black.webp" alt="タイピン１号"></span>
                            <input type="radio" id="radio-black" name="selected_avatar" value="black" {{ $avatar == 'black' || is_null($avatar) ? 'checked' : '' }}>
                            <label for="black">タイピン１号</label>
                        </div>
                    </div>
            <button id="update_btn" class="btn btn-secondary btn-sm" type="submit">更新</button>
            {{ Form::close() }}
        </td>
        <td>
            {{ Form::open(['url' => '/teacher/userpas-change', 'method' => 'post','autocomplete' => 'off','autocapitalize' => 'none']) }}
            <p>現在のパスワードは非表示</p>
            @component('components.changeOnePaswd')
            @slot('passwordbtn', 'パスワードを変える')
            @slot('passwordcomment', '6～20文字で入力。英数字（アルファベットと数字）が必要です')
            @slot('maxlength', '20')
            @endcomponent
            <p>※パスワードは、①6～20文字 ②アルファベットが必要（大文字か小文字、どちらか1つ以上） ③数字が必要です。</p>
            {{ Form::close() }}
        </td>
    </tr>
</table>
<div>
<div class="manualSpace spaces links">
@component('components.modalimage')
    @slot('word', '4月の年度更新')
    @slot('image_name', 'teacher21')
@endcomponent
@component('components.modalimage')
    @slot('word', '生徒の「マイアカウント」画面')
    @slot('image_name', 'teacher1')
@endcomponent
@component('components.modalimage')
    @slot('word', '生徒のパスワードルール')
    @slot('image_name', 'teacher27')
@endcomponent
</div>
</div>
@endsection