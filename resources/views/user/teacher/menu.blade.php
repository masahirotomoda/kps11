@extends('layouts.base')

@section('head')
@endsection

@section('content')
<div class="infoTitle">
    <span>お知らせ</span>
</div>
@include('layouts.include.information')
<div class="btnOuter">
    <a class="btn-link" href="./menu-allinfo">すべてのお知らせ</a>
</div>

<div class="bannerAreaOuter">
    <div class="bannerArea">
        <div class="bannerCell"><a href="./movie-introduction" target="_blank"><img src="/img/banner_movie.png" alt="ナレッジタイピングとは？　動画でみてみよう！ 30秒"/></a></div>
        <div class="bannerCell"><a href="/pdf/first_typing.pdf" target="_blank"><img src="/img/banner_guide.png" alt="かんたんガイド！ はじめて操作する方はこちら　画面の説明、使い方の事例などご案内"/></a></div>
        <div class="bannerCell"><a href="/pdf/qa.pdf" target="_blank"><img src="/img/banner_faq.png" alt="よくある質問FAQ　転校生がきたときはどうするの？　生徒のスコアがみたいときは？"/></a></div>
    </div>
</div>
<div class="mainBlock"> {{--mainBlock start--}}
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block"
                onclick="location.href='./profile'">マイアカウント</button>
        </div>
        <div class="twoBlockRight">先生のパスワード変更など</div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
        @if($school->trial === 1)  {{--トライアル生徒一覧と学校版の生徒一覧でリンク先（コントローラー）が異なる--}}
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./studentlists'">生徒情報</button>
        @else
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./student-list'">生徒情報</button>
        @endif
        </div>
        <div class="twoBlockRight">生徒追加、パスワードリセット、クラス変更</div>
    </div>
    @if($school->trial === 1) {{--クラス一覧はトライアルはなし--}}
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" disabled>クラス一覧</button>
        </div>
        <div class="twoBlockRight">トライアルでは使用できません</div>
    </div>
    @else {{--学校の生徒一覧--}}
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block"
                onclick="location.href='./byclass'">クラス一覧</button>
        </div>
        <div class="twoBlockRight">全クラスの生徒一覧　<small>(例)1年2組の生徒一覧</small></div>
    </div>
    @endif
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./transcript-print'">スコア履歴<small>エクセル出力</small><small>記録証印刷</small></button>
        </div>
        <div class="twoBlockRight">生徒のタイピングスコア一覧<br>&nbsp;&nbsp;<small>(例)タイピングテスト後、生徒に記録証状を配布</small>&nbsp;&nbsp;<small>(例)宿題のタイピング履歴確認</small></div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block"
                onclick="location.href='./bestscore'">ベストスコア<small>記録証印刷</small></button>
        </div>
        <div class="twoBlockRight">コース別、生徒別のベストスコア<br>&nbsp;&nbsp;<small>(例)〇〇さんのベストスコア</small>&nbsp;&nbsp;<small>(例)〇〇コースの生徒のベストスコアを記録証として印刷</small></div>
    </div>
    @if($school->trial === 1)
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" disabled >コース追加</button>
        </div>
        <div class="twoBlockRight">トライアルでは使用できません</div>
    </div>
    @else
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block"
                onclick="location.href='./battlescore-history'">ランキングスタジアム スコア一覧</button>
        </div>
        <div class="twoBlockRight">ランキングスタジアムのスコア一覧（自教室の今月、先月のスコア履歴）</div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block"
                onclick="location.href='./add-course'">新しいコースをつくる</button>
        </div>
        <div class="twoBlockRight">オリジナルのタイピングコースをつくる</div>
    </div>
    @endif
</div>
@endsection
