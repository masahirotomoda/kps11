@extends('layouts.base')
@section('js')
{{--新規と更新は、システム管理と学校管理と共通Js--}}
<script src="/js/checkDelBtn.js"></script>
<script src="/js/checkall-dim.js"></script>
<script src="/js/del_modal.js"></script>
<script src="/js/addnew_all.js"></script>
<script src="/js/btn_addword.js"></script>
@if($school->word_sound === 1)
<script src="/js/addnew-word2.js"></script>
<script src="/js/checkedit-word2.js"></script>
@else
<script src="/js/addnew-word.js"></script>
<script src="/js/checkedit-word.js"></script>
@endif
@endsection
@section('content')
    @include('layouts.include.courseaddword')
@endsection


