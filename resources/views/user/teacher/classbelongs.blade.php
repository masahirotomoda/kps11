@extends('layouts.base')

@section('content')
{{--先生メニューと学校管理で共通--}}
<table class="table byclass byclassOne" id="mainTable">
    <thead>
        <tr>
            <th scope="col">学年</th>
            <th scope="col">組</th>
            <th scope="col">所属生徒数</th>
            <th scope="col">クラスの編集</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $class->grade_name }}</td>
            <td>{{ $class->kumi_name }}</td>
            <td>{{ $student_num_byclass}}</td>
            <td>
                <div class="bottomSpaceSmall"><strong>クラスに生徒がいない</strong>または、<strong>別のクラスの生徒がいる場合</strong></div>
                <ul type="disc">
                    <li>生徒自身が、「マイメニュー」でクラスを修正する</li>
                    <li>または、先生が下記「生徒一覧へ」ボタンを押して、該当生徒のクラス情報を修正。</li>
                    <li>生徒を新たに登録する場合も、「生徒一覧へ」ボタンを押す。</li>
                </ul>
                {{--生徒一覧へ学年と組をパラメータでもつ--}}
                @if(Auth()->user()->role ==='学校管理者')
                {{ Form::open(['url' => '/school/student-search', 'files' => true , 'method' => 'post']) }}
                @elseif(Auth()->user()->role ==='先生')
                {{ Form::open(['url' => '/teacher/student-search', 'files' => true , 'method' => 'post']) }}
                @endif
                {{ Form::hidden('grade', $class->grade_id)}}
                {{ Form::hidden('kumi', $class->kumi_id)}}
                <button name="search" class="btn btn-secondary" type="submit" >生徒一覧へ</button>
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

<table class="table byclass byclassTwo" id="mainTableTwo">
    <thead>
        <tr>
            <th scope="col">出席No</th>
            <th scope="col">生徒名</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->attendance_no }}</td>
            <td>{{ $user->name }}</td>
            {{ Form::close() }}
        </tr>
        @endforeach
    </tbody>
</table>
@endsection