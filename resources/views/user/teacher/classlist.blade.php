@extends('layouts.base')

@section('content'){{--先生メニューと学校メニュー共通--}}
<div class="menuSpaces">
<p class="conspicuous">
    @if(Auth()->user()->role === '先生')
    下記一覧にクラスがない時は、学校管理者にお問い合わせください。
    @elseif(Auth()->user()->role === '学校管理者')
    クラスの増減で、クラスが足りない、又は未使用の場合、ナレッジタイピング事務局までお知らせください。
    @endif
</p>
<div class="manualSpace links links-sm rightBox">
    @component('components.modalimage')
    @slot('word', 'この画面の使い方')
    @slot('image_name', 'teacher30')
    @endcomponent
</div>
</div>
<table class="table schoolbyclass" id="mainTable">
<thead>
    <tr>
        <th scope="col">学年</th>
        <th scope="col">生徒数</th>
    </tr>
</thead>
<tbody>
@foreach ($gradeinfo_toals as $gradeinfo_toal)
    <tr>
        <td>{{$gradeinfo_toal->grade_name}}</th>
        <td>{{$gradeinfo_toal->user_id_count}}人</th>
    </tr>
@endforeach
</tbody>
</table>

<table class="table schoolbyclass02" id="mainTable">
    <thead>
        <tr>
            <th scope="col">学年</th>
            <th scope="col">組</th>
            <th scope="col">生徒数</th>
            <th scope="col">生徒一覧</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($classes as $class)
        <tr>
            <td>{{$class->grade_name }}
            <td>{{$class->kumi_name }}
            <td>
            {{--クラス別生徒数--}}
            <?php $student_count_byclass=0; ?>
               @foreach($users as $user)
                    @if($user['class_id'] == $class['class_id'])
                    <?php $student_count_byclass++; ?>
                    @endif
                @endforeach
                {{$student_count_byclass}}

            <td>
                @if(Auth()->user()->role ==='学校管理者')
                {{ Form::open(['url' => '/school/byclass-belongs', 'files' => true , 'method' => 'post']) }}
                @elseif(Auth()->user()->role ==='先生')
                {{ Form::open(['url' => '/teacher/byclass-belongs', 'files' => true , 'method' => 'post']) }}
                @endif
                {{ Form::hidden('class_id', $class->class_id)}}
                <button class="btn btn-secondary" name="class_belongs_studentbtn" type="submit">生徒一覧</button>
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection