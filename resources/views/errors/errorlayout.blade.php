@php
    if(Cookie::has('ntyping_last_login_role') == 1){
        $tmp_txt = 'ログイン画面へ';
        $btn_uri = '/login';
    }else{
            $tmp_txt = 'トップへ';
            $btn_uri = '/';
    }
@endphp

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700;800&display=swap" rel="stylesheet">
        <link href="/css/style.css?v=20240123" rel="stylesheet">
    </head>
    <body class="antialiased">
        {{--dd(url()->previous());--}}
    <div class="container notfound">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card errors">
                    <h2><strong>@yield('code')</strong></h2>
                    <div class="card-body">
                        <div class="notfoundBox">
                            <p>@yield('message')</p>
                            <div class="notfoundImage">
                            <img src="/img/logo_fs.svg" alt="ナレッジタイピング" />
                            </div>
                            <p class="backToTop"><a href="{{asset($btn_uri)}}">{{$tmp_txt}}戻る</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
