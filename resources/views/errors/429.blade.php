@extends('errors::errorlayout')

@section('title', __('429エラー/ナレッジタイピング'))
@section('code', '429 Too Many Requests')
@section('message', __('一定期間内のリクエストが集中しているため、ページを表示できません。時間をおいてから再度操作してください。'))
