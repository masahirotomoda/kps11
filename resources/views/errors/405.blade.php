@extends('errors::errorlayout')

@section('title', __('405エラー/ナレッジタイピング'))
@section('code', 'このURLは直接リンクから開けません')
@section('message', __('このURLをブックマークに登録しないでください。トップページからのリンクでページを表示させてください。'))
