@extends('errors::errorlayout')

@section('title', __('404エラー/ナレッジタイピング'))
@section('code', '404 Not Found')
@section('message', __('お探しのページは見つかりませんでした。'))