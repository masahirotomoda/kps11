@extends('errors::errorlayout')

@section('title', __('503エラー/ナレッジタイピング'))
@section('code', '503 Service Unavailable')
@section('message', __('リクエストが集中しているため、ページを表示できません。時間をおいてから再度操作してください。'))
