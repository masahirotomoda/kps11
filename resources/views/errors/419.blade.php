@php
    if(Cookie::has('ntyping_last_login_role') == 1){
        $tmp_txt = 'ログイン画面へ';
        $btn_uri = '/login';
        $error_title='ログインの期限切れです';
        $error_msg='ブラウザを閉じて、ログインしなおしてください。このエラーには、次の原因が考えられます。(1)ログイン後、数時間操作しなかった。(2)別タブで他の人がログインしていた。(3)ブラウザの「戻る」ボタンを何度か押した。';
    }else{
        $Ruquest_Url=$_SERVER['REQUEST_URI'];
        $tmp_txt = '「ナレッジタイピング for コンテスト」へ';
        $error_title='セッションタイムアウト';
        $error_msg='';
        if($Ruquest_Url=='/contest2pramode2/contest-course'){            
            $btn_uri = '/contest2pramode2/contest';            
        } elseif($Ruquest_Url=='/contest1tesmode1/contest-course'){
            $btn_uri = '/contest1tesmode1/contest';
        } elseif($Ruquest_Url=='/contest2pramode2/contest/exam-start'){
            $btn_uri = '/contest2pramode2/contest';
        } elseif($Ruquest_Url=='/contest1tesmode1/contest/exam-start'){
            $btn_uri = '/contest1tesmode1/contest';
        } else {
            $tmp_txt = 'トップへ';
            $btn_uri = '/';
        }
    }
    //dd( $Ruquest_Url);
@endphp

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>セッションタイムアウト/ナレッジタイピング</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700;800&display=swap" rel="stylesheet">
        <link href="/css/style.css?v=20240123" rel="stylesheet">
    </head>
    <body class="antialiased">
    <div class="container notfound">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card errors">
                    <h2><strong>{{$error_title}}</strong></h2>
                    <div class="card-body">
                        <div class="notfoundBox">
                            <p>{{$error_msg}}</p>
                            <div class="notfoundImage">
                            <img src="/img/logo_fs.svg" alt="ナレッジタイピング" />
                            </div>
                            <p class="backToTop"><a href="{{asset($btn_uri)}}">{{$tmp_txt}}戻る</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
