@php
    if(Cookie::has('ntyping_last_login_role') == 1){
        $tmp_txt = 'ログイン画面へ';
        $btn_uri = '/logout';
    }else{
        $tmp_txt = 'トップへ';
        $btn_uri = '/';
    }
@endphp

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>403エラー/ナレッジタイピング</title>
        <link href="/css/style.css?v=20240123" rel="stylesheet">
        <META http-equiv="Refresh" content="3;URL={{ $btn_uri }}">
    </head>
    <body class="antialiased">
    <div class="container notfound">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card errors">
                    <h2><strong>403 Forbidden</strong></h2>
                    <div class="card-body">
                        <div class="notfoundBox">
                            <p>アクセスするには認証が必要です。</p>
                            <div class="notfoundImage">
                            <img src="/img/logo_fs.svg" alt="ナレッジタイピング" />
                            </div>
                            <p class="backToTop"><a href="{{$btn_uri}}">{{ $tmp_txt }}戻る</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
