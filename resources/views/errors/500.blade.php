@extends('errors::errorlayout')

@section('title', __('500エラー/ナレッジタイピング'))
@section('code', '500 Internal Server Error')
@section('message', __('サーバーで問題が発生しているため、ページを表示できません。時間をおいてから再度操作してください。'))
