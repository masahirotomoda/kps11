@extends('errors::errorlayout')

@section('title', __('401エラー/ナレッジタイピング'))
@section('code', '401 Unauthorized')
@section('message', __('アクセスするには認証が必要です。'))
