@extends('errors::errorlayout')

@section('title', __('400エラー/ナレッジタイピング'))
@section('code', '400 Bad Request')
@section('message', __('ページを表示できません。'))
