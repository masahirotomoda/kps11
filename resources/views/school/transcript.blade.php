@extends('layouts.base')

@section('js')
    <script src="{{ asset('/js/dateedit.js') }}"></script>
    <script src="{{ asset('/js/checkall-dim.js') }}"></script>
    <script src="{{ asset('/js/validation_modal.js') }}"></script>
    <script src="{{ asset('/js/btn_excelexport.js') }}"></script>
    <script src="{{ asset('/js/btn_pdfprint.js') }}"></script>
    <script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection

@section('content')
    @include('layouts.include.transcriptprint')
@endsection