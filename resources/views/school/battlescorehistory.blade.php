@extends('layouts.base')

@section('js')
@endsection

@section('content')
<div class="menuSpaces bottomMin">
    <div class="menuSpacesCell spaceAll formRight">
        {{Form::open(['url' => '/school/battlescore-history-search', 'method' => 'post'])}}
            <div class="modalCell contestInput">
                <label for="select_course">コース<br><small>（直近２か月のみ）</small></label>
                {{Form::select('select_course', $course_selection, $select_course, ['class' => 'form-select',]) }} 
            </div>
            {{ Form::text('user_name', $user_name, ['type' => 'text' , 'class' => 'form-control', 'maxlength' => 20 ,'id' => '','placeholder' => '名前の一部（ニックネーム不可）']) }}
            <button class="btn btn-outline-primary searchIcon" type="submit" name="search">検索</button>
            <button class="btn btn-outline-primary" id="all_btn" type="button" onclick="location.href='/school/battlescore-history'">リセット</button>
        {{Form::close()}}
    </div>   
</div>
<br>
<br>
<div class="menuSpaces bottomMin">コースの全履歴数：{{$battlescore_count}}件</div>
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th scope="col">順位</th>
            <th scope="col">日時</th>
            <th scope="col">コース名</th>
            <th scope="col">ニックネーム</th>
            <th scope="col">名前</th>
            <th scope="col">スコア</th>
        </tr>
    </thead>
    <tbody>
        @php
            $rank = 1; // 実際の順位カウント（行数）
            $display_rank = 1; // 表示用の順位
            $prev_score = null; // 前回のスコア
        @endphp

        @foreach ($BattleScoreHistory as $sh)
            @php
                // 前回のスコアと異なる場合、順位を更新
                if ($prev_score !== null && $sh->battle_score < $prev_score) {
                    $display_rank = $rank;
                }
            @endphp
            @if ($sh->school_int_id == $school_int_id) <!-- ここでフィルタリング -->
                @if ($user_name==null || strpos($sh->name, $user_name) !== false)
                    <tr>
                        <td>{{ $display_rank }}</td>
                        <td>{{ date_format($sh->created_at, 'Y/m/d') }}</td>
                        <td>{{ $sh->course_name }}</td>
                        <td>{{ $sh->nickname }}</td>
                        <td>{{ $sh->name }}</td>
                        <td>{{ $sh->battle_score }}</td>
                    </tr>
                @endif
            @endif
            @php
                $prev_score = $sh->battle_score;
                $rank++; // 次の順位
            @endphp
        @endforeach
    </tbody>
</table>
@endsection