@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/checkDelBtn.js') }}"></script>
<script src="{{ asset('/js/checkall-dim.js') }}"></script>
<script src="{{ asset('/js/school/addnew_student.js') }}"></script>
<script src="{{ asset('/js/school/checkedit_student.js') }}"></script>
<script src="{{ asset('/js/del_modal.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
<script src="{{ asset('/js/btn_password_reset.js') }}"></script>
@endsection


@section('content')
@include('layouts.include.flashmessage')

<div class="menuSpaces bottomMin">
{{ Form::open(['url' => '/school/student-search', 'method' => 'post','id' => 'search_form']) }}
    <div class="menuSpacesCell spaceAll">
        {{ Form::select('grade', $grade_selection, $grade_select, ['class' => 'form-select','id' => 'grade','placeholder' => '学年']) }}
        {{ Form::select('kumi', $kumi_selection, $kumi_select, ['class' => 'form-select','id' => 'kumi','placeholder' => '組']) }}
        {{ Form::text('user_name_search', $user_name, ['class' => 'form-control', 'id' => 'user_name_search','maxlength' => 20, 'placeholder' => '生徒名の一部']) }}
        @include('layouts.include.searchbtn')
        <div class="manualSpace links links-sm rightBox topBox">
        @component('components.modalimage')
        @slot('word', 'アップロード方法')
        @slot('image_name', 'school5')
        @endcomponent
        </div>
        <div class="manualSpace links links-sm rightBox topBox">
        @component('components.modalimage')
        @slot('word', 'アップロードエラー')
        @slot('image_name', 'school4')
        @endcomponent
        </div>
    </div>
    @include('layouts.include.alertmessage')
    {{ Form::close() }}
    {{ Form::open(['url' => '/school/student-import', 'files' => true , 'method' => 'post']) }}
    <div class="menuSpacesCell bgPlus font-sm">
        <input class="btn btn-outline-primary btn-sm" name="downloadbtn" id="downloadbtn" type="button" value="①入力用ひな形取得" onclick="location.href='/school-student-exceltemplate'">
        <label class="fileInputBox">
            <input type="file" id="file" name="file" class="inputFile" required>
            <span class="fileInputText">⇒②ファイルを選択</span>
        </label>
        {{ Form::submit('⇒③データのアップロード(Excel)', ['name'=>'import','id'=>'importbtn','class'=>'btn btn-secondary btn-sm']) }}
        {{ Form::close() }}
        <button class="btn btn-outline-primary" id="all_reset_btn" type="button" onclick="location.href='./studentlist'">操作キャンセル</button>
        <button class="btn btn-outline-primary" type="button" onclick="location.href='./today-student'">本日の登録生徒</button>
    </div>    
</div>
<div class="menuSpaces">
    <button class="btn btn-secondary btn-sm" type="button" name="add" id="add">生徒を登録する</button>
    @component('components.delButton')
    @slot('route', 'studentDelete')
    @slot('id', 'student-checks')
    @slot('name', '生徒')
    @slot('message','')
    @endcomponent
</div>
@include('layouts.include.pagination')
<div class="menuSpaces bottomMin">
    {{ Form::submit('データの一括ダウンロード(Excel)', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm rightBox','form'=>'search_form']) }}
    <div class="manualSpace links bottomBox">
        @component('components.modalimage')
        @slot('word', 'ダウンロード方法')
        @slot('image_name', 'school6')
        @endcomponent
    </div>
</div>
<table class="table tablestudentlist" id="mainTable">
    <thead>
        <tr>
            <th>
                @component('components.tooltip')
                    @slot('word', '選択して削除')
                    @slot('message', 'まとめて削除する時はここをチェック。個別に削除するときは、個別にチェック')
                @endcomponent
                <input type="checkbox" id="checkAll" value="1">
            </th>
            <th scope="col">
                @component('components.tooltiprequired')
                    @slot('word', '学年')
                    @slot('message', '学年がない時は「なし」')
                @endcomponent
            </th>
            <th scope="col">
                @component('components.tooltiprequired')
                    @slot('word', '組')
                    @slot('message', '組がない時は「なし」')
                @endcomponent
            </th>
            <th scope="col">出席No<span class="red">*</span><small>（数字）※ない時は「0]を入力</small></th>
            <th scope="col">生徒名<span class="red">*</span><small>（最大20文字）</small></th>
            <th scope="col">編集・保存</th>
            <th scope="col">ログインID</th>
            <th scope="col">登録日</th>
            <th scope="col">パスワード初期値<br>{{$school->password_student_init}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{--form openはtr内は不可--}}
            {{ Form::open(['url' => '/school/student-list-new', 'files' => true , 'method' => 'post']) }}
            {{ Form::hidden('school_int_id', $user->school_int_id)}}
            {{ Form::hidden('school_id', $user->school_id)}}
            {{ Form::hidden('class_id', $user->class_id)}}
            {{ Form::hidden('user_id', $user->user_id)}}
            <input type="checkbox" name="checks[]" id="{{ $user->user_name }}" value="{{ $user->user_id }}" form="student-checks"></td>
            <td> {{ Form::select('grade_id',$grade_selection,$user->grade_id,['class' => 'form-select','id' => 's_grade' ,'disabled']) }}</td>
            <td>{{ Form::select('kumi_id',$kumi_selection,$user->kumi_id,['class' => 'form-select','id' => 's_kumi','disabled']) }}</td>
            <td>{{ Form::text('attendance_no',$user->attendance_no,['class' => 'form-control col-xs-2', 'id' => 'attendance_no','maxlength' => '4','readonly']) }}</td>
            <td><input class="form-control col-xs-2" type="text" name="student_name" value="{{ $user->user_name }}" readonly></td>
            <td><button class="btn btn-outline-primary btn-sm" name="edit" type="button">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>
                {{ Form::close() }}
            </td>
            <td>{{ $user->login_account }}</td>
            <td>{{ $user->created_at->format('Y/m/d') }}</td>
            <td>
                @if($school->password_display === 1)
                @component('components.displayPaswd')
                @slot('controller', 'posts')
                @slot('id', $user->user_id)
                @slot('password',$user->password)
                @slot('name', '')
                @endcomponent
                @endif

                @component('components.confirmReset')
                @slot('controller', 'posts')
                @slot('url', '/school/student-pass-reset')
                @slot('id', $user->user_id)
                @slot('school_id', $user->school_id)
                @slot('init_password',$school->password_student_init)
                @slot('name', $user->user_name)
                @endcomponent
            </td>
        </tr>
        @endforeach
    </tbody>
</table>{{--新規で1行作るときにhiddenのデータをコピーしてつくるので必要--}}
<div hidden>
    {{ Form::select('nd_grade_selection',$grade_selection,null,['class' => 'form-select','id' => 'nd_grade_selection', 'placeholder' => '学年選択']) }}
    {{ Form::select('nd_kumi_selection',$kumi_selection,null,['class' => 'form-select','id' => 'nd_kumi_selection', 'placeholder' => '組選択']) }}
</div>
@endsection
