@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/checkDelBtn.js') }}"></script>
<script src="{{ asset('/js/checkall-dim.js') }}"></script>
<script src="{{ asset('/js/addnew-course.js') }}"></script>
<script src="{{ asset('/js/checkedit-course.js') }}"></script>
<script src="{{ asset('/js/del_modal.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection

@section('content')
    @include('layouts.include.addcourse')
@endsection


