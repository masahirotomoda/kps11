@extends('layouts.base')

@section('js')
{{--単語の新規作成と更新は先生メニュー、システム管理メニューと共通Jsを使用--}}
<script src="{{ asset('/js/checkDelBtn.js') }}"></script>
<script src="{{ asset('/js/checkall-dim.js') }}"></script>
<script src="{{ asset('/js/del_modal.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_addword.js') }}"></script>
{{--音声ファイルありかなしかの判定--}}
@if($school->word_sound === 1)
<script src="{{ asset('/js/addnew-word2.js') }}"></script>
<script src="{{ asset('/js/checkedit-word2.js') }}"></script>
@else
<script src="{{ asset('/js/addnew-word.js') }}"></script>
<script src="{{ asset('/js/checkedit-word.js') }}"></script>
@endif
@endsection

@section('content')
    @include('layouts.include.courseaddword')
@endsection