@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/password_btn_null.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_password_update.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection

@section('content')
@include('layouts.include.flashmessage')
<div class="manualSpace">
【{{ $school->name }}タイピングURL】:<a href={{$domain_name}}login target="blank">{{$domain_name}}login</a><br>
【{{ $school->name }}学校管理用URL】:<a href={{$domain_name}}school-login target="blank">{{$domain_name}}school-login</a>
</div>
{{ Form::open(['url' => '/school/userpas-change', 'method' => 'post']) }}
@include('layouts.include.alertmessage')
<table class="table" id="twolineLeftth03">
    <tr>
        <th scope="row">学校ID</th>
        <td>{{ $school_id }}</td>
    </tr>
    <tr>
        <th scope="row">学校名</th>
        <td> {{ $school->name }}</td>
    </tr>
    <tr>
        <th scope="row">学校管理者ログインID</th>
        <td> {{ $user->login_account }}</td>
    </tr>
    <tr>
        <th scope="row">パスワード<br><small>（セキュリティのため非表示）</small></th>
        <td><small>パスワードは10～20文字。アルファベット（大文字・小文字）、数字、記号（!#$%& ）の内、3種類以上必要</small><br>
            @component('components.changeOnePaswd')
                @slot('passwordbtn', 'パスワードを変更する')
                @slot('title', 'パスワードを変更する')
                @slot('passwordcomment', '10～20文字で入力。アルファベット（大文字・小文字）、数字、記号（!#$%& ）のうち、3種類以上必要です')
                @slot('maxlength', '20')
            @endcomponent
        </td>
    </tr>
    <tr>
        <th scope="row">学校管理者のパスワード初期値</th>
        <td>{{ $school->password_school_init }}</td>
    </tr>
    <tr>
        <th scope="row">先生のパスワード初期値</th>
        <td>{{ $school->password_teacher_init }}</td>
    </tr>
    <tr>
        <th scope="row">生徒のパスワード初期値</th>
        <td>{{ $school->password_student_init }}</td>
    </tr>
    <tr>
        <th scope="row">生徒パスワードの確認表示</th>
        <td>
            {{ Form::checkbox('password_display','display',$school->password_display,['class'=>'form-check-input']) }}
            <label class="form-check-label" for="school_adm">管理画面のパスワード確認で「生徒のパスワードを表示」する</label>
            <button class="btn btn-secondary btn-sm" type="submit" name="password_display_btn" id="update_btn">更新する</button>
            <div class="manualSpace links links-sm">
            @component('components.modalimage')
            @slot('word', '生徒のパスワード表示とは')
            @slot('image_name', 'school1')
            @endcomponent
            </div>
        </td>
    </tr>
    <tr>
        <th scope="row">利用者の最大数</th>
        <td>{{ $school->user_number }}人　<small>（先生・生徒）※先生1名と学校管理者1名は含まない</small><strong><strong><span class="red">　　現在の登録数：{{$user_num}}人</span><strong></td>
    </tr>
    <tr>
        <th scope="row">追加タイピングコース最大数</th>
        <td>{{ $school->course_number }}コース　<small>（学校独自のタイピングコース数）</small><strong><span class="red">　　現在のコース数：{{$course_num}}コース</span><strong></td>
    </tr>
    <tr>
        <th scope="row">追加タイピングコースの単語最大数</th>
        <td>{{ $school->word_number }}単語　<small>（追加タイピングコースに登録する単語数）</small> <strong><span class="red">　　現在の単語数：{{$word_num}}単語</span><strong></td>
    </tr>

</table>
{{ Form::close() }}

@endsection