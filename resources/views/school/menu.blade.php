@extends('layouts.base')
@section('content')
@if($school->contest > 0)
<div>{{--コンテスト用の管理画面（生徒一覧とスコア履歴のみ）--}}
    <div>
        <button class="btn btn-outline-primary" type="button" onclick="location.href='../{{$contest_account}}/contest'">生徒のタイピングページ</button>
    </div>
    <div class="mainBlock contestMenu">
        <div class="twoBlockCell">
            <div class="twoBlockLeft">
                <button type="button" class="btn btn-primary btn-block" onclick="location.href='./contest-studentlist'">生徒一覧</button>
            </div>
            <div class="twoBlockRight">登録済みの生徒一覧</div>
        </div>
        <div class="twoBlockCell">
            <div class="twoBlockLeft">
                <button type="button" class="btn btn-primary btn-block" onclick="location.href='./contest-scorelist'">スコア履歴<small>エクセル出力</small><small>記録証印刷</small></button>
            </div>
        <div class="twoBlockRight">生徒のタイピングスコア一覧<br>&nbsp;&nbsp;<small>スコアをエクセル出力、生徒の記録証状を印刷</small></div>
    </div>
</div>
@else
<div class="infoTitle">{{--ここから通常のナレッジタイピング学校管理メニュー--}}
    <span>お知らせ</span>
</div>
    @include('layouts.include.information')
    <div class="btnOuter">
        <a class="btn-link" href="./menu-allinfo">すべてのお知らせ</a>
    </div>
<div>{{--ここからmainBlock メインメニュー--}}
    <div class="mainBlock">
        <div class="twoBlockCell">
            <div class="twoBlockLeft">
                <button type="button" class="btn btn-primary btn-block" onclick="location.href='./profile'">学校アカウント情報</button>
            </div>
            <div class="twoBlockRight">パスワード変更、現在の登録人数、生徒パスワード表示オプション</div>
        </div>
        <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./teacherlist'">先生情報<small>エクセル登録</small></button>
        </div>
        <div class="twoBlockRight">先生追加・編集・削除、パスワードリセット、<span class="red"><strong>先生のエクセルアップロード</strong></span></div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./studentlist'">生徒情報<small>エクセル登録</small></button>
        </div>
        <div class="twoBlockRight">生徒追加・編集・削除、パスワードリセット、<span class="red"><strong>生徒のエクセルアップロード</strong></span></div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./byclass'">クラス一覧</button>
        </div>
        <div class="twoBlockRight">クラス一覧と、各クラスの生徒一覧<small>&nbsp;&nbsp;(例)1年2組の生徒一覧</small></div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./transcript-print'">スコア履歴<small>エクセル出力</small><small>記録証印刷</small></button>
        </div>
        <div class="twoBlockRight">生徒のタイピングスコア一覧<br>&nbsp;&nbsp;<small>(例)タイピングテスト後、生徒に記録証状を配布</small>&nbsp;&nbsp;<small>(例)宿題のタイピング履歴確認</small></div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./bestscore'">ベストスコア<small>記録証印刷</small></button>
        </div>
        <div class="twoBlockRight">コース別、生徒別のベストスコア<br>&nbsp;&nbsp;<small>(例)〇〇さんのベストスコア</small>&nbsp;&nbsp;<small>(例)〇〇コースの生徒のベストスコアを記録証として印刷</small></div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./battlescore-history'">ランキングバトル スコア一覧</button>
        </div>
        <div class="twoBlockRight">ランキングスタジアムのスコア一覧（自教室の今月、先月のスコア履歴）</div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block" onclick="location.href='./add-course'">新しいコースをつくる</button>
        </div>
        <div class="twoBlockRight">オリジナルのタイピングコース作成（全学年が利用可）</div>
    </div>
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block"
            onclick="location.href='./login-history'">管理画面ログイン履歴</button>
        </div>
        <div class="twoBlockRight">管理画面（この画面）と先生のログイン履歴</div>
    </div>
    @if($school->invoice == 1)
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block"
            onclick="location.href='./invoice'">請求書・受領書発行</button>
        </div>
        <div class="twoBlockRight">お支払い履歴の確認・請求書や受領書のPDF発行</div>
    </div>
    @endif
    @if($school->logo == 1)
    <div class="twoBlockCell">
        <div class="twoBlockLeft">
            <button type="button" class="btn btn-primary btn-block"
            onclick="location.href='./vld-test'">ICTタイピング検定管理</button>
        </div>
        <div class="twoBlockRight">タイピング検定問題の表示・非表示設定</div>
    </div>
    @endif

</div>
@endif
@endsection