<table>
    <tr>
    <th>日時</th>
    <th>ログインID</th>
    <th>氏名</th>
    <th>管理画面</th>
    <th>IPアドレス</th>
    <th>OS</th>
    <th>WEBブラウザ</th>
    </tr>
    <tbody>
        @foreach ($login_logs as $logs)
        <tr>
        <td>{{ $logs->created_at }}</td>
        <td>{{ $logs->login_id }}</td>
        <td>{{ $logs->login_username }}</td>
        <td>{{ $logs->login_from }}</td>
        <td>{{ $logs->ipaddress }}</td>
        <td>{{ $logs->os }}</td>
        <td>{{ $logs->browser }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
