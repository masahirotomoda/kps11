@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
<script src="{{ asset('/js/btn_excelexport.js') }}"></script>
@endsection

@section('content')
{{ Form::open(['url' => '/school/login-History-search', 'method' => 'post','id' => 'search_form']) }}
<div class="menuSpaces">
    <div class="menuSpacesCell underFix">
        <div class="plusText ptfull">
            <span class="inputText">開始日</span>
            {{ Form::date('start_date',$startDateTime,['id' => 'start_date','class' => 'form-control']) }}
        </div>
        <div class="plusText ptfull">
            <span class="inputText">終了日</span>
            {{ Form::date('end_date', $endDateTime, ['class' => 'form-control']) }}
        </div>
        {{ Form::text('user_name', $user_name, ['class' => 'form-control','maxlength' => 20 ,'placeholder' => '名前の一部']) }}
        {{ Form::text('ipaddress', $ipaddress, ['class' => 'form-control','maxlength' => 16, 'placeholder' => 'IPアドレス']) }}
    </div>
    <div class="menuSpacesCell">
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('schooladmin',$chkSchool, $chkSchool , ['class'=>'form-check-input']) }}
            <label class="form-check-label" for="valid">学校管理者</label>
        </div>
        <div class="menuSpacesOneCheck">
            {{Form::checkbox('schoolteacher',$chkTeacher, $chkTeacher , ['class'=>'form-check-input']) }}
            <label class="form-check-label" for="invalid">先生</label>
        </div>
        @include('layouts.include.searchbtn')
    </div>
</div>
{{ Form::close() }}
@include('layouts.include.pagination')
<div class="menuSpaces topMinusBox">
    {{ Form::submit('データの一括ダウンロード(Excel)', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm rightBox','form'=>'search_form']) }}
</div>
<table class="table loginhistory" id="mainTable">
    <thead>
        <tr>
            <th scope="col">日時</th>
            <th scope="col">ログインID</th>
            <th scope="col">名前</th>
            <th scope="col">管理画面</th>
            <th scope="col">IPアドレス</th>
            <th scope="col">OS</th>
            <th scope="col">
                @component('components.tooltip')
                    @slot('word', 'WEBブラウザ')
                    @slot('message', '動作保証をしている主要ブラウザのみ表示（Safari、Firefox、MicrosoftEdge、GoogleChrome）')
                @endcomponent
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($login_logs as $logs)
        <tr>
            <td>{{ $logs->created_at }}</td>
            <td>{{ $logs->login_id }}</td>
            <td>{{ $logs->login_username}}</td>
            <td>{{ $logs->login_from }}</td>
            <td>{{ $logs->ipaddress }}</td>
            <td>{{ $logs->os }}</td>
            <td>{{ $logs->browser }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection