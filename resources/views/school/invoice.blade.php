@extends('layouts.base')

@section('js')
    <script src="{{ asset('/js/validation_modal.js') }}"></script>
    <script src="{{ asset('/js/btn_pdfprint_invoice.js') }}"></script>
    <script src="{{ asset('/js/btn_all_reset.js') }}"></script>
@endsection

@section('content')
<div class="menuSpaces underFix">
    <div class="menuSpacesCell">
        {{Form::open(['url' => '/school/invoice-pdf', 'method' => 'post','id'=>'pdf'])}}
        @include('layouts.include.alertmessage')
        <button name="pdf1" id="pdf1" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">請求書PDF</button>
        <button name="pdf2" id="pdf2" type="submit" class="btn btn-outline-primary btn-sm pdfBtn">受領書PDF</button>
    </div>
</div>
<table class="table" id="mainTable">
    <thead>
        <tr>
            <th scope="col">請求書発行</th>
            <th scope="col">受領書発行</th>
            <th scope="col">利用期間</th>
            <th scope="col">サービス名</th>
            <th scope="col">金額</th>
            <th scope="col">請求書発行日</th>
            <th scope="col">お支払い期限</th>
            <th scope="col">お支払い日</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($invoices as $invoice)
        <tr>
            <td>
            @if($invoice->show_invoice==1)
                <input type="checkbox" class="checks" name="checks1[]" value="{{ $invoice->id }}" id="{{ 'pdfInvoice'.$invoice->id }}">
            @else
                準備中
            @endif
            </td>
            <td>
            @if($invoice->show_receipt==1)
                <input type="checkbox" class="checks" name="checks2[]" value="{{ $invoice->id }}" id="{{ 'pdfReceipt'.$invoice->id }}">
            @else
                準備中            
            @endif
            </td>
            <td>{{$invoice->service_period}}</td>
            <td>{{$invoice->service}}</td>
            <td text-align="right">{{number_format($invoice->price)}}</td>
            <td>{{$invoice->seikyu_day}} </td>
            <td>{{$invoice->kigen_day}}</td>
            <td>{{$invoice->juryo_day}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ Form::close() }}
@endsection