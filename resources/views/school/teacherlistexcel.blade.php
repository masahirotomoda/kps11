<table>
    <tr>
        <th>先生名</th>
        <th>ログインID</th>
    </tr>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->user_name }}</td>
            <td>{{ $user->login_account }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
