@extends('layouts.base')

@section('js')
<script src="{{ asset('/js/checkDelBtn.js') }}"></script>
<script src="{{ asset('/js/checkall-dim.js') }}"></script>
<script src="{{ asset('/js/school/addnew_teacher.js') }}"></script>
<script src="{{ asset('/js/school/checkedit_teacher.js') }}"></script>
<script src="{{ asset('/js/del_modal.js') }}"></script>
<script src="{{ asset('/js/addnew_all.js') }}"></script>
<script src="{{ asset('/js/btn_all_reset.js') }}"></script>
<script src="{{ asset('/js/btn_password_reset.js') }}"></script>
@endsection

@section('content')
@include('layouts.include.flashmessage')
<div class="menuSpaces bottomMin">
    <div class="menuSpacesCell spaceAll formRight">
        {{ Form::open(['url' => '/school/teacher-search', 'method' => 'post','id' => 'search_form']) }}
        {{ Form::text('user_name', $user_name, ['class' => 'form-control', 'id' => 'user_name', 'maxlength' => 20,'placeholder' => '先生名の一部']) }}
        @include('layouts.include.searchbtn')
        {{ Form::close() }}
        <div class="manualSpace links links-sm rightBox topBox">
        @component('components.modalimage')
        @slot('word', 'アップロード方法')
        @slot('image_name', 'school2')
        @endcomponent
        </div>
    </div>
    <div class="menuSpacesCell bgPlus font-sm">
        {{ Form::open(['url' => '/school/teacher-import', 'files' => true , 'method' => 'post']) }}
        <input name="downloadbtn" class="btn btn-outline-primary btn-sm"  type="button" id="downloadbtn" value="①入力用ひな型入手" onclick="location.href='/school-teacher-exceltemplate'" >

        <label class="fileInputBox">
            <input type="file" id="file" name="file" class="inputFile" required>
            <span class="fileInputText" id="excel_import_form">⇒②ファイルを選択</span>
        </label>
        {{ Form::submit('⇒③先生データのアップロード(Excel)', ['name'=>'import','id'=>'importbtn','class'=>'btn btn-secondary btn-sm']) }}
        <button class="btn btn-outline-primary" id="all_reset_btn" type="button" onclick="location.href='./teacherlist'">操作キャンセル</button>
        {{ Form::close() }}
    </div>

    <div class="manualSpace links links-sm rightBox topBox">
        @component('components.modalimage')
        @slot('word', 'アップロードエラー')
        @slot('image_name', 'school4')
        @endcomponent
    </div>
</div>
<div class="menuSpaces">
    <button type="button" class="btn btn-secondary btn-sm" name="add" id="add">先生を登録する</button>
    @component('components.delButton')
    @slot('route', 'teacherDelete')
    @slot('id', 'teacher-checks')
    @slot('name', '先生')
    @slot('message','※削除した先生情報（タイピング履歴やスコアを含む）は元に戻せません。')
    @endcomponent
</div>
@include('layouts.include.alertmessage')
<div class="menuSpaces bottomMin">
    @if ($current_teacher_num === 0){{--ページネーションがないため手動で件数表示--}}
    <div class = "pagination_no">
    <p class="text-danger">データがありません。</p>
    </div>
    @else
    <p>全{{$teacher_num}}件中、{{$current_teacher_num}}件を表示。</p>
    @endif
    {{ Form::submit('データの一括ダウンロード(Excel)', ['name'=>'excel','id'=>'excelbtn','class'=>'btn btn-secondary btn-sm rightBox','form'=>'search_form']) }}
    <div class="manualSpace links bottomBox">
        @component('components.modalimage')
        @slot('word', 'ダウンロード方法')
        @slot('image_name', 'school3')
        @endcomponent
    </div>
</div>
<table class="table tableteacher" id="mainTable">
    <thead>
        <tr>
            <th>
                @component('components.tooltip')
                    @slot('word', '選択して削除')
                    @slot('message', '表示データをまとめて削除する時はここをチェック。個別に削除するときは、個別にチェック')
                @endcomponent
                <input type="checkbox" id="checkAll" value="1">
            </th>
            <th scope="col">先生名<span class="red">*</span>（最大20文字）</th>
            <th scope="col">編集・保存&nbsp;</th>
            <th scope="col">ログインID</th>
            <th scope="col">
                @component('components.tooltip')
                    @slot('word', 'パスワード')
                    @slot('message', '「パスワードの変更は、先生自身でログイン後、「先生メニュー」でおこなう')
                @endcomponent
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{--★formタグはtrに設置できない。td内にすること--}}
                {{ Form::open(['url' => '/school/teacher-list-new', 'method' => 'post']) }}
                {{ Form::hidden('school_id', $user->school_id)}}{{--★Jsで使うのでhiddenは必要--}}
                {{ Form::hidden('id', $user->user_id)}}
                <input type="checkbox" name="checks[]" value="{{ $user->user_id }}" id="{{ $user->user_name }}" form="teacher-checks">
            </td>
            <td>{{--★requiredを設定しない理由⇒空欄で保存ボタンを押すとエラーメッセージがでて、処理が止まるため --}}
                {{ Form::text('name',$user->user_name,['class' => 'form-control col-xs-2', 'id' => 'user_name','maxlength' => '20','readonly']) }}
            </td>
            <td>
                <button class="btn btn-outline-primary btn-sm" type="button" name="edit">編集</button>
                {{ Form::submit('保存', ['name'=>'save','class'=>'btn btn-secondary btn-sm','disabled']) }}
                <button class="btn btn-outline-primary btn-sm" name="editcancel" type="button" disabled >ｷｬﾝｾﾙ</button>

            </td>
            {{ Form::close() }}{{--/form はtdの外に入れる--}}
            <td>
                {{ $user->login_account }}
            </td>
            <td>
                @component('components.confirmReset')
                @slot('controller', 'posts')
                @slot('url','/school/teacher-pass-reset')
                @slot('id', $user->user_id)
                @slot('school_id', $user->school_id)
                @slot('init_password',$school->password_teacher_init)
                @slot('name',$user->user_name)
                @endcomponent
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
