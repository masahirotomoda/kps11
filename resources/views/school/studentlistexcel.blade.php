<table>
    <thead>
    <tr>
        <th>学年</th>
        <th>組</th>
        <th>出席番号</th>
        <th>生徒名</th>
        <th>ログインID</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->grade_name }}</td>
            <td>{{ $user->kumi_name }}</td>
            <td>{{ $user->attendance_no }}</td>
            <td>{{ $user->user_name }}</td>
            <td>{{ $user->login_account }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
