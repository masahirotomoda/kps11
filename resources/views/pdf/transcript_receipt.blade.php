<!doctype html>
<html>
<head>
    <title>受領書 | ナレッジタイピング</title>
    <link rel="apple-touch-icon" href="{{asset('/img/icon/apple-touch-icon-student.png')}}">
    <link href="{{ asset('/img/icon/favicon-student.ico') }}" rel="shortcut icon" type="image/x-icon">
    <style>
        body {
            font-family: ipaexg, serif !important;
            word-wrap: break-word; /* IEやFirefox対応用 */
            overflow-wrap: break-word;            
            font-size: 16px;
            line-height: 1.6em;
            color: black;            
        }
        .container{
            position: relative;
            margin-right:auto; 
            margin-left:auto;
            width:90%;
        }
        td {
            border: none;
            vertical-align:bottom;
        }
        table {
            border: none;
            width:100%;
            margin-top:50px;
            margin-bottom:30px;
            font-style:bold;
        }
        .title{
            font-size:20px;
            margin:0 30px 0 0;
            padding:0;
        }
        td.log {
            background-position:top right;
            text-align:right;
            line-height: 1.3em;
            padding-top:50px;
            height: 110px;
            background-size: 180px;
            background-repeat: no-repeat; 
             background-image : url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR4AAADaCAYAAABwxy3AAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAyNDowMToxMiAwMTo1OTowNyQ47pcAAD6ZSURBVHhe7Z0HgFNV9sa/TGEavYMISheQqig2EKwgKOquYMO6KrJ2UNRddV0rxfZXEcWuiCugiKKggErvAtI7DH1gYHrL/d/v3RcIIZnJTN5khnB++sjkJXklefd755x77rkupYEgCEIYibIfBUEQwoYIjyAIYUeERxCEsCPCIwhC2BHhEQQh7IjwCIIQdkR4BEEIOyI8giCEHREeQRDCjgiPIAhhR4RHEISwI8IjCELYEeERBCHsiPAIghB2RHgEQQg7IjyCIIQdER5BEMKOCI8gCGFHhEcQhLAjwiMIQtgR4YkQpGK/cCIhs0xECMtS0zFy2z7E5uWgSkwMasbFomZCPKroW0sN/dihciJqVIi13x0aBaoA2fkZ9rNgUYiJqoC46AT7uXAyI8ITIczcm4q71mxDZmYWYmNj7LWG6Lg43FQtCc+3Pt1eExrJ6RsxesVQ86QYV0/FhEro3ehetKx+tr1GOFkRVytCiHLpxe1GtLsAKifnmCU3MxO7c3Ltd4aOW1s8aXkHi73sy9mBVSnz7K0Ivii4re+2pIsqxl2A9oZbXy++i8cOKew1JxCLJ0L4fZ+2eP7agiwtMlqDjkFp16tP7Wp4u30ze01obE9bh9eW3G+eFOPqiYmLxrk1r8K1zQbZawRyKCcFU3d8guTDG6Hc9spioxAVHY0zqndG9wb9tVt7rNXrS3p6OoaPGInU1ENwucwV06xpEwwceJ/199Rpv+Dnn6dagkOqVq2KIYMfRUKCM66yWDwnAdSGSjHR5olQ7vh+02gsSvkZ29LWYntGSZd12HJ4FX7fPw5L9v5qbzkw+fn5WL9+o17WH1m2bdtuvwrs378f69YdfW3Dhg1HRMgJRHhOAlyuKCRGy09dXtmbuQPuPP2HW1seISwudxRyc/KwT2+vKGjlxOibER89S7S2mDxERUUd81qMtpqdRK7GkwCa4HEMAgnlkih9Y3ASp7dXGojwnATwR67k8B1LEEJBhOckIeEkcrXy8wuwatUqLF26FEuXLTt+WboMK1asxOHDafYnjrJlyxb/n7GXP/9cjt27d9vvjlwYA6Lr5VlyHOwVJdKrFSH8sf8Q7ly52W+vVkxCAl5u1gB/b1DLXhMa5blXKy8vDyNfex2LFy8pMhhav359PDn0CdStW8d6/v33kzH2q3HIzS28kSUmJuHBBwahU6eO9prQGLn4fuzK3oCCvNCDtzFxUehW9wb0PP0Oe41/Dh06hPsHPYCDBw9aMRzSpnVrvPzyi9bfFOclS5bqn9f8wElJSbju2r6oUKGC9TxUxOIpI3hH4QXu1MIGF0gFeFnF6de8319QUGBejDA2btpkiU6mFuCcnJyAS3Z2NrZt24YZM2dan+PzSVp42CD9vd97OXjwAL6bNMn6nBNUiq0KRCu4lVv/SqEt0bFRenvV7C0HhsHjBg0aoGHDU+2loRbguvarQIcO7XHnnbfjrjvvsJb+/W5wTHSIWDxlxP+9/Q5WrvzLugBCJcpdgD2162N+l0sB/fdxaFP57HnTUW/3Nrit/A6FhIREXNv3Gpx//nnmPcWgPFs8y1eswH/+819LIIqCLkSvXj1x9113HrEA+FgU/M2aNmmCESOG2WtCY2fGJkzY+BYO5u6BcpesOfLmwptJ8xqd0Pf0+5EQW9G8UAi8AXk3f55XbKwzw2qKQoSnjOBd+ZVXhyErK8teU3JcBfk41LAJNvXpr3/Q4811xYYy8QtU2rEZKtoEmfmzV69eHU89+QRatGhhrQuWSBKeq67qZd3RKTiD/vkgUlNT7VcDwwbarGlTDB/+qr0mdJh5nOVOL3GujNXtrVxIiq1irynfiKtVRjA+cNlll1oXvxN4/PTj4AWp74RRWpz0E7NOw/enpKTgzbf+T7sORTc2IQgyk4H9C+De539ByiKo7P32m48lyhWNpOgqlptUkqViTNUTRnSICE8Z0r9fP8vPLm1c+i4alU/hORbeubdu3YZR7412NCv1ZMW99gNg7g2I+qOfn+UGYOEtUPsW2u8+lsWpaRi7Yy/Gbt9T4mXi7hRszci2t1i+EeEpQ5KSEi0zPz4+3tEBeL643AWWO0brxx8LFi7EuK+/tp8JJUXlHID28ZCXk+13QU4uXHFV7Xcf5fNtuzHgr214Yt0OvSSXeHlo1Tb0W7oeKw+l21suv4jwlDHt27dD9+4XOxJk9ofS7hWFx7J4/OgOXa683Fx8++0kLFjg/24sBIcr96D+l78jv+hjFw5byc/Rv0GF43ucvkneh0OZmdoNy9biFcKSnYUdOXn4aY8WwBKQlZOFvSl7sH7LOqzasBIr1i7HX/px596d9jucQ4SnHHDzTTfi1FMblJq7Y7laVozHPxQfjlZ+/4Mx2LUr8pPjSgcFd04q3PkB0hS0RatccUyqslccpUC/5lRDjNLHURzbOSV1PxaumI9vfhqHTyZ8iPFTv8aUPybjl3nTMGPhr/ht0Qys3vCX/W7nEOEpB1SqVAm333YbKlas6LzLpW+4rgI3XNriofUTCFpcu3btwjvvvmvnBJ2YFPf7835/cT573HvduUBeeiHb0N9+TKK1+BL4Vyk9tu3ahu+nf4f/acGZs2y29Tw9Mx3pGRnIzc1DrnYLubCLvl7t+vannEOEp5xw1lmd0K1bV8d6uY7iQnRert9udl9o+SxfvhKfffa5vab8kZ6ZZrkDh9L959vEx8VbuShFWY8UCJ5vZS36hMlxcXFxQVmd/CwzeY+hIMcSHgQcoKkFiaLjR3jCSerhVG3R/IhJ0ydiw7b1SEtP06523pHz5nfiDQec1qxW037mHCI85Qjjcp3quMvlytcWTJCJaQXaJft56jTMm1e+KgUeSjuEX+ZOw2fffYLx0/6HLyd/hunzfjkuA/v0009Dh/btLeuRQftAS2JiotWj2LXrRdbnWOCqT+/eqFq1it/3ey+VK1fG1Vf3tj53hPxsqHwKj3/7xSorEaPFyk/N6eDtrOAItL11W9ZpC+crrNm4ysrU5nfnKzS+xMXGoUql4wPioSIJhOWMFStX4vXX37RybIKFPVYHG5yOjb37HWfZKG1BJW3fghaTx9ltomjDnpdEo0YNMXLEcL91WMKZQOh2F2DZ6mVYuHI+srKzkEcRtfeZmJSIc9ueh06tzzIrbPLyWORqvXYbArs+FbRV1LhxY1SpcmzuCweJ7tu/37/4603x++AYr3r1jg4vsEjbBDX9KuRnHNRf8fH381htyOZX74KYS76x1xylz5zlWJZBi8kBF1cL4z8b1MTg5g3tFYa5y+Zg6erFyMxkkf6irwFCUTq17qn4e8/+9hrnEOEphyQn77TGEVkXfxDXiP4R8ae+ZocXxGu/POeYj7ijotApRmFookLQo7P0FVGzZk20aNHcXnEs4RKenXuTMXvpLOzat9PKRHb5fBlWBnGj5ujd/Wp7TdmhDiyHa9bfkZdxWD87/keLjXYjv15PxFw4xl5zlNGbkjEi+SBytKi6QmiNKkq7jvrG83GHZuhY1biQbN6/LZiBlRuWIysru0gLx5uY2Bi0b9ER3c652F7jHCI8EcLc1AwMWLb+uNHpbm3x9KxdDaM7+BeRklDawkPLZsHyeVZXLrt4A41fottzfocL0aGVM6PEQ0Gt/xBY/hzyc2m1+BGeGDcKGt6E6HOG22uOwrObsS8Vm7Ny4C4ouZvN2UXOqpSINlWOxp/mLp2NxasWWUNziiM6JC4+Dpd2uRwtG59hr3EOEZ4IIVCxd1dcPK6pWQlvtHOm0DspTeFhwHP24j+Qmp5q5Rf5a8SEjeL0+o1xxYXaiijDImcq9xCwaiRcmz9HnhbMQMcbG+dCQeN7Ed3+aXtN6bNuy1r8PGtKUOPWCIXJpa2mGI7n06cRGxOL6y+7AbWqO1NOxRsJLp8EnAhlTw+nH8JPf0yxGsq+A/usnhZ/jZj3SfZAtW7SBldeVMais28+1O83wLVxDPKswb6FfM/R+tUwjqVKy0jD7wtnIie3cNFh1g97Uhlcr129Npo3aoEzm7dFh5Yd0apxG1St7HxgmYjwRAiFSUui4130zrJy/QqM+3EsVm/8q9DR+mwk7FE6t10XdD/3Et1gykh03Llwr3oDrtkD4Er5U4ukNvsCuDEufcyxcVFQVc5BVMOe9trSZ+6y2cjIyuCXFhBaN0kJSZar+vcr++Omq2/FVRf3wcXn9MCFZ3VF187dLKunNBDhiRAO5uVbPeb+Lv/oYvr24SLl4H58++sEzFwwHalpqcd1jfuSEJ+Irmd1R+e259prwo9KXQ0161ZErR2B/KxDyHcHbkKxUW7ExFeEu8XDwAVfAJWa2K+ULvv197p+67pCE0EZmK9Tow76XnodunXujlrVaoW1SLwIT4Sw/FA6cgIMi6jsM6VxqLCEA5eSwJ4pt1bIJX8twrgpY7F5+0Yrp8S3x+oY9FVao1oN9O1xLVo2bmmvDDPaxVObvoB75t/g2v0b8rI5c2eg5uNGbAVt69Q4C0oLTlTrR+CKibdfK33+XLPUpB0EgLGcqpWroffFV6NeLeezkoNBhCcCyNeNYlbKIagAd7iqDgtPbFScXrQJXoJ+CcZkNm7dhEWrFyIzK1NbOYX34jAL+ZTaDXDNJdfhlLqlX0LEHypjBzD/frhWPA2VlYK8gsDNJtpVgNikynA3GwhXt3Fw1QrvPPHZOdnYpMU8X1vA/lFW8mQP7aqWRmJgsIjwRAC/7DmADbkFVj6PL7QjasU5VyuXJMQkIS4mUV/CxRcewuzoontalLYaYtG0YTP01aJTTd+hywK1YwrUzOuBHd8iLyOnECtHH6/WYleNdkCXTxDV9kmtsuEfHpG8Zwdy8gLX5InWwn9avdPR6JTT7DVlgwjPCc6qwxl4YUMysrKPTRwklIX46CjUrOCsxVMxtgqqxNaAK7p0YkcUtPj4BKtn5cqLelnjr8JOzkGopU/DtfQBqMNbkZenzzVArCzapV2rxCSopv8Aun6tlb7sYlBbk7dY0/v4w/pe9XfZsXUne03ZIcJTDtmYnoXFqelYfDDtuGWJXuYfOIwpu1Pwn7Xb0H/hamxOy0C0JTPHU0Gvr5sQZz9zBtaWaVS5FaJLYa4uxh8qVayErmdfjIvO7lZqdYoKQ+1fCPVHP7g2fYQ8/d26C7NytDHpqn4GcM77cLV/Bq4KZVd+lKkGu/fvsmar8Ae/y6oVq6F2DTOdT1kiCYTljN3Zuei7YDX26btWINeJ9VvcuvEX6AvMnZsbsFm4dSNunBiHny5o53iX+qbUFXh/zVDkaEsrWJhAeGHdvmhW0A3T5k857rNsGJUrVkaPLpfh9Aan22vDT8FvAxB94BcU4rFYVk5UvHY3G/UHWj0CV1x1+5WygzGzTyZ+aLrR/VBBu9zntTsfZ53Z2V5TdojFU874aMsu7MjN140yG9k5OcctWXphRm9Bjm4VhYgOcUXHoElSQqnk8ZxWpTWaVuwIxpidIEpbT3Vr1bO6d8tSdAxuqLzA92MO+ETlJlBnvwNXh/+WC9EhmdmZKPA3vZFNdFQ0GtQ91X5WtojwlCO2ZWbjf7sPQOWaeE2gJViiY6JxbvXSMf2Z83F1k/tQNbYOXDElN5qtuENCPJqc2tQSnRpVna/9Ulzc+Rwf5t9diebEezU6w9V9IlynXG6vLR/Q4omKCdykY6NjrW708oAITzlijLZ29ucHjigUB0pBUpQLXWuVXpdpzYR6GNDyX2hQsSkqJMRoVeRslkWLkLd3n5SYhI5nnIU+3a9BQtzxtWrCjj42F6sJBoAN2125OVzxzo9fCpWc3OyAtZwsNzapctkE6v0gwlNO2JCeifG2teMEKrYCulSMR8tKpdule2rlFrin9TBcccrtaKAbZMXEioiNjwHn8D5+iUa+O8+K4yTEJ5h4zjmX4rwO51tB5XKB0q4KqwkWYlu6+J5yiOVmBdB9S3gqlZ95tyS4XE4YvHwDvtqTClde4LttMPDHVDGxaJQYj086NEXTpPBZERSVPZlbsStzCw5k78LBrL3Izj8a6IyOjcYZVc5Fpzo9sHNPstV7VUnfhcsVBdnI/+kyRKVv0A35ePGxRpk36IfozseXtyhr1mxajV/mTrUywX2hO9um6ZnW8IjygAhPOWDl4Qz0+3MTDqSnF9sEZdOgce3Sd7So6Bgrb+eCahUxtMkpaFbK1k5Ekp+J/B+6IiZnu1/hiabw1NfCc85Ie035YeO2Dfhp9o/WnOgcgsJCXqxlxOEt1apURcdWZ5VKbZ2SIMJTDvhpdwqGb92rb7aF9N/6gT9crvbpk2JjUTO+AtpWTkKP2tXQST9GnwClMMoleenIn3kzovMPauE5vmnQZSyo3RPRbYfYa8oPLII/c+EMJMQlWpneNavXRJWKVSyXNr5CQilMJFByRHjKAbRYcvVFXtyfgu9mTk+cvqAqiM44hP5WtbsVGP1FcxR3lLPDUJyA1w//C+co85IiwiMIQtiRXi1BEMKOCI8gCGFHhEcQhLAjwiMIQtgR4TmBOBzCnEuCUJ6QXq0TgLS8fDy1eiuWZeagfVIcXjzjNFSMCU9OxrZt25FyIAUNT21oTQt86qkNcMopp9ivRjZ//bXKqpZYp04d7EhORrOmTa1ZLoTQEYvnBOCL7XuxPj0TrzdvgA3pWfh46277Fefg/YcDDL0XMm3aNGsu97/++gsvvfwKZs78zVofSfg7d677YMyHeOedUViwcCGGDRthzasuOINYPGVIRkEBXl673RIVzr/kC3+ZStqyqZsYjw2HM/BGu6YYuGw9zqtRBY80O1pXZdWq1Rg/YQKyWf40QCIhB2HGxcXj4YceRFLS0aEUTK//9LPPsXbtOt3gvAc/uqxC63Xr1MamzVtwy803YdSo99C37zW46qpe1jt+++13TJ8x07IKCoPnkZSUhEH333fEYkhJScEnn36G7KxsJCQmWBPKVaxYUR9jHOL1kpCYqNfFW4NJuY6v1a9fzzomsnLlSkyY+J0+/mMH1daoUR33DxxoTfpXFCkpB/DBB2Owd98+/ezo90/h4dzxbBqcWLBrt4swfvwEPPjAP9G8eXMo/T2p1W8Ce+fArQq/d8fEuFBQsQWiOz5nrznKjz9Owdx586z9BIK/W1NtaQ249RZ7Tehwf+/r8966ZSt3gC5dztW/cx18P3myde6JCYl46KEHrN+ktBDhKUO+3rEXT2zdj9zMTHuNH/SFMaBedezLycXG7FycFl8Br7ZujBpxpgGuXrMGw4ePwL59+62LlFPHMLnWgn8y0dYVZb0Wo0Xsg/dHo2rVo6OUV61ejaee/jdyc0wD5oXngduqV6+u1di5dD77bNx8843WTBEZGRm4+x/3Ij093Y9kHgtr29SoUQMjRwyzHsmHH36EyT/8aNUH5jHyMuR2ovQTjqTmwmPmI69Qpdx4beQIy9UjDz70iGWBuL0vX/13tWpV8f7o9yyxIpyriwu34zvr6DffjMfnX3x5ZP4pz7nzWChctWvXto6tQoU43HhjP3Q591xrO+598xG1SAtB+mHzBfvCQ7Kyh/VrsS7kJ7RDzGWTrZc87Nq1G48NHoI0/f1xXBXPzzoVbk4/RllDXvQrevtt2rTCf5//Dz/mCDy/Rx4djA0bNujtR+HKKy9Ho4YN8eFHH1u/R1xcBf0djipVt1JcrTKEjaxubBTqxUb7XepXiEZN/Qt9lbwXWbrxdKpeBU80b3hEdNasWWuJzu7de6yLKT4+Xt+pa6BG9erWwr85lQktElozFA9eyN5E64bkaZhcKAx169a14hr1teikpqYiOTkZO3YkW7N8ehov91dFX5i0MKpXq1boUqtWLZx91lmoUsWrLIM+jkRt6cTaU+9wHFGcbuzcvhFQt24E+dY+M7UwU0jYIAj3zfOiVXLkXPVxcD+XXnLJEato69Zt+Pe/n9Ui9TBeHTb8GFG1sL8LjyjV0dYd7/wUWx7rrl27sHPnLn3uO6xj4fsMWhkq1ALi6wNxfha9Pl8lakHT+2MlQz9lGrktzrTBY+LvQ4vO89vxkcfj+d18BdMJuE3umwu/ey68DjzXydFzLR3E4ilDsvWPnqmvzWNdnKOwWaxPy8TtyzbgsL4rx2hX6cM2jdCjlqki98qrwzB79hwjAlUqazfqITRr1tR6TtiAf/l1Oj7VLg0vMLo7o959x3ov2bZ9O37Vr0+Y+K1llbAxP/HE42jS+Gjp0X/9+xltWWiTXB/NlVdcjvvuu8daz31kalFw64u1qEuId9VKlSrazwzp6RlaVDKtxkeLI0dbdHT7Dqelafcr64jgcB8FutG3bdfWsrg80FLgMfvu21vcRr//geXOsEE1atQQb7z+2pGBksuXL8d3k77HwoWLrG1QcF995aUjltKevXst0aJFx0b68MMPoutFF1mvwa0tpIJ0/ej/d1OIhXvFK4ja9KE2fGKQX/VsxFwy0X7VuJlz583HmDEfWufOhj5o0EBLnD3n8/obb2Lx4iXW3x07dsCzz/zb+tsJuI8hjw/VLvoq67fp0+cqy+IZ9d5o67vid/jmG69h+vQZ1jVSqVIl3HRjf0ddLxGecg6DydcsXINDugFGxVbAR+2boLuX8MyZM9e6kNq3b4f/PPestd6bn36eitGj37fu2N7Cc+jQIQx98ml9R99pXWzcBu+6zz//nNV74+HhRx7Fpk2b9es4Rni4vW/GT7BiQz5G1PHoz7IM6xWXX45OnTpaqw4ePIivvvraEpu4+DjLWuPxMf7EGAMvcsZ+KAQVdMOku1O7dq0j1kwweAtPYy2mI0cMt4SHMbHhI4x7SijQ1bRl9uGY948IE+M+D2l37vDhw8cLTxAUzBmIqB0TtfBEI79yR+1qTbLWU2iefOpf+jvdZJ074TkNHvyo5cp5YCB/nhYn/i6+wsNg97SpvxgXsajvXhOrr5s+vXvhzDPPtJ5zm4UJDy2uvtdcY8X+srKyUVW7r2+/9cYxoh4q4mqVc0KtbhHovkKLg9YGGx0vfC68sKpVDa5U6ooVK6wYyaJFiyyrodBFv4d3773aivAw8dvvMHXaNPysF4rDt/r5F1+Mxfvvj8Ebb/2fJar/+tezGDr0KQweMlQ31qctl9IJdu/ejYyMzCPnzXjO6aedZn0XHo5zy4qBe7m2dnZORn6B3h51LEm7XzYUG0/vGPdNUauqv3O6WN4E+t34+VGjRmP+ggVYvGSJ9b0WtSzR79u734hsMPB7YAzPCCNHu4d4EfpBhKecM27HXqRr68LCvhs7AQPM/frdgDvvuB133HEb/nH3XRj6xBArbuKNd2P0hndGwkZboQIbr//FG++4ARsWrYuqWuxo3bARMqDK9XTfzPaV9cgGQAuruLGOQMfesmUL3HbbAOu877hdn/s/7rIsGifiGpbobHoX+bn5VvzMXf1CxHT6r/0qLAuO++N+uf8777wdDz1oesu8CXTsnpiMv+/a38LvjAH5Nq1a21sIjkD7dwpxtcoxr6zbjtHJdq+XdjUuqFoRb7drgur6grJe93K1OnRoj+eefcZa782PU37SVsQHVgP2jfEEw0MPP4LNm7fofUC7Spdh4MB7rfW0luiG6EvI+t8X9tQcTD2orZZnLOuKjeCef9yNy/U2SLq+o2Zad9U8ZGQa64vzbLFsJ2M7fM44Dt+Tpde11W5Ct25drc8Gy3vaxZyiz5/n7u1qBUNJXC2P6ORlaUtBf1+cXdR9zhhE1QveRfPwwosvYcGChdZv6+1q8TljRB7hD4akpIrajU6ynxkGD3kioKtFl/aSHj0w9qtx1vPq2hp7683XxdWKJA7qhrcjOxfJWTnHLC+t2apFJwW5WVmW6JxfNQnvtD0qOr4wOLtPm9NsMFz26YVxhKlTp1qxAF6wvFt638k4wT8zcrdv3+F3mTTpeyQn77Q+x8/TKvHAOze7W9lFzzu778Ld/PH7LEtA2Aq5X2+LgvkxdLcmTJiIWbNmW1nCyTt3WsLDIHe9evUssemhG8C1fa9B69atjmlsPJ79ugHu3WvO13vhuS9b9ifmz1+gLaWjwW/vc2eMy985W8uOHfj66/9Z7gahWPlaWyp7H5CxA8peCpa9aEQnMxfRLp4v64npzy99AurACvtTBn6f+/lbBTj22bNnY/XqNX7FhedAq5S9jsEuvqJD65GLP/hdHTp0GL9On37kd/f97ZxALJ4yZE1aJu5fuQm70jOPuQPwJ8nQawry8+CKrYDztOi8264ZqvnMge6xeDzmNwO03rDR5Whx4fb42i233Iw+va+yLiQ28OEjRloxgECXANdz21zOOKMlhgwZjNq1zLQujNe8/PKr2Llrl/XcF36WosMLnMdGi+zRRx62ekjIRx9/gu8n/2D1WBHPMfAC9114HpwuecTwYWjQwAzX+OLLsZY1k6+/I38mF8WWLhq3y30+8MCgI8FbdpG/+NJLlsh4i5E3/BwbfkxMLLp2vRD33XvPke9X7f5DC8pjKMhIZQSEaxCrMvQ+C8DsgPzKrbUY7YQrN0WLkH65ajOg6zdwJdSxtvnuu+9hzlz+bhSW4/fP4zbxFVgiM2TwY2jVyplaydz/22+/gxkzf7N+G37tvXv3wmmNGlkWj7cg8TtI1FbyAH3d9OrV017rDGLxlCFzDxzGhgIXDisXUr2WQ/pn0ZeENivicV41WjpNjxMd4n2R8ILiHdp7oegQNphb9cVzdZ/eRxra/v0HsH79BquBesTF38IL84yWLfHE40OOiA7ZuHEjNmqLynefnoXuEo8pOjpGuwod8dijjx4RHcJtM0OZFhPxBHopNLzgeW4Ux7S0dP2YZR23x+rg6zNn/m51vTNI7G//HtFhT91DDz5wTI/RmjVrrAQ+vu57vlwKCswjj6frRRfin4PuP0bU1c5pWlS2IEYd1oKTqpdDemU+YvVb3LXOQ0z3bxB93tuIqVhTH7e2GLBXm5cmsM5zmvnbb1Y3faBj52/C83VadAj3++fylUeEjd93gj43nq83/N3pmjNj2mnRIWLxlCG/70/FuL2pyPM3/7i+8GrEVcDgpqcEdK8mTvwW69avtxpKIGgptGvbFldcceysl+zO/uzzL5GuGy/35R823Ero3+/vVnKeN8x6Ha/dpML2za1Wq14NN990oyUA3tDVYXIiGyIto8zMLOtv5u1k2fk7fM4Gwpyedu3a4ZIe3a0GyUuWmc/796egwKfBeENR69a1Kzp3Ppr/QxjbYOyL8aVAcD/1tbvHjGVvF5OoLePh2vMzCpggaGN9hXHV4TrzcbjiTHa22jUDrm1f6QOJh2r5GFxJp1rnw6EnGfqcOAOEf4xb26tnT0dFh/CG8NFHHyNVf/88RyaB3nLzzdiwaaOV05WXZ25mdJc7dupgJWSWBiI8giCEHXG1BEEIOyI8giCEHREeQRDCjgiPIAhhR4RHEISwI8IjCELYEeERBCHsiPAIghB2RHgEQQg7IjyCIIQdER5BEMKOCI8gCGFHhEcQhLAjo9MFwQObAstssDQEaxn9tYoTqAPJyXzRvOdEg+fDOkjNmgGcZaJ2HSAhXpsc2uZgLQ+rnkf4EeERBDbO9HRWCAO+/RaYOZNFe1i85ujrZYFHCAkFgmJRUvhZLvXrA507A1deCVx8MVCvHgsXhV2ARHiEk5eCAmDPHmDKT8CYMcDixSwGbRpi5SpAw4ZAW20lcOYNVuzjhHYsCsaC8SVpNtwfF4qJR0g8i2/D5/OUFODLLwEWa+NkhpddZj7LfQcrFNz2oUOcwZATzgMZGSxdaY6D53X1NcBddwKtW5vzCxMiPMLJScoB4JdfgFdeAZYuMQ2ULgktAE5oyMcD+j2cC2znTmP9nHMOcKdupFdcYcSpNKHQffwx8OijwFlncbJ54PSjM7wWi08/BTgDyZZtwMMPAW3acKZHVuMH9unzS6wI3HIL8I9/AC1bcM4i+4Olh/62BeEkgnf7Zct0A3wYGHArsH4dwDmtuCQlGetg61bWdgU4wwfXcVoXWgp0w/79b2DaNHtjpQDtAArer7+a43zzTWDChJKLDuE0RIe11cS551u1Am6/HRj3FfDjj8DAQQAnExz1LnCNtn7G6vXcf2lDi0cQTgqyspSaMkWpNmcqFR2lVKdOSo0apdS6dUotX67UsmVKbd2qVFqaUrm5SuXnmyUvT6lnnqEkKNW1q1Lz5tkbdBC3W6nsbLPtiy9WqksXpXbtsl8MkRkzlDpTn3P79kotXmyv9GKpPu/bbleqenWl4uKUevAhpTZvtl8sHcTiEU4OaLF88w1w223A5k3AjTcBn30G3HPP0R6fdu1MXIeF6T2xHC50qzi1M62fxo2NK+YEjNdkZ3PKD2DOHBPP+e9/zTG8/jpQt679xhDh9nj8NWocG8fx7L+NtoJeGwm8rN3OJvrc3tT7HjwEWLXafqPzSIxHiHw4qeCkSSaGwVgOH5980sR0goE9XEOHAosWAW+8AVx/vf1CCWBQlz1ojBlt22a2zWNjjxqF74kngPbt7TcHAQPPmzebbdIlZHyGLmJeHpCYaASULtWrr5r1jFFdeinQoAEnkTfu3L595rvgTKl0y158EZg1C7j2OmDkCKBRI3tnziHCI0Q2jOnMmAHcfbeJ3zz0EPD445xszH5DEEyeDHz0EdC3L9C/v7GCSgoD1RQCihiFgBYIg73s2m7RonhBa4oYReull4yI8JworLRk+Fi5shEexqy4X8JpirifatWA7dvNQhHkZxj/oRWYqY/r0UeAP/4A/vlP4OmnzeccRIRHiFw4bxXv6HSn/lppLB020mC6jdksaAksXGgsJloD7H4OFVootCooEh5hKCm0an7+2fRaUTgoRBQcumjsfqf7SBfru++0G/UyULs28K9/AT16GPGki8fkSD5SfPi5Ll2M9cNcJgbgmUD5graA7rtPu6DHToUcEhQeQYhIUlKU6tdfqahope64Q6mDB+0XioAB5YULlbrrLhNwbdtWqZ9/VqqgwH5DOYTHxuP2d4zr1yt10UVKvfCCvSJIJk1SqnFjpSpXUWryZBMAdwgJLguRCS2Amb8B48cDnToaS4d3/2BgfGPwYJM7ExdntsW/33rLWAJ02cobtHRoxfDRF7pTPI/iWmy9ewMPateUOUUjXzOWkUOI8AiRye49wLBhAKd/vusu42YUBQVm7lzdyEaax8svB957D/jqK6CjFi/GPxicfU03Qmv8VgngPrh4YAyKrhzjPXR3uLAHju4YXT0u/JtuVElgtvILLxjxYa9WcbnpRi3cnYDftYj/8GPJj8MHifEIkQcb9ujRwP33A926GaunKGuHwVkmBjJbeP164I47TKKdd48O40XsDWNguFcv4LrrTGD41FODCzjTYmBMhpnI7JJftw7YscOIDsWHTdHTE+VJZGRMiD1VjL+w2/+MM4ITUUIBGzIE+OQT4NprgeefL1kP1adacO8fCDRuAkzR4sPxXiES/azG/lsQIoO0dNMbw8b73HOmoQeC72EODd0oulNsrBymwAbrK1Zs/OwRWr0a+P57k11Mi4JiQfEpbKgBBWT4cJOJzOAuc2sWLAA2bTIWTWqqCRZ73CX+TeuCFhDHWdHa4v4okB06mJyiwqD4fv65OSdaa888Y469JFAkf/8DWKiPt207M34t1EGlVqRHECIFBkAXLTYZuGefrdShw/YLPjAzecECpYYOVeqCC5Rq0UKpG280AVVmKhfGihVK3XuvUqedRhtFqe7dlZozx34xAJ99plTVqkpdf73JUC4Oh/U58Ni4r3POMVnWhcGg+ltvKdWokclWdiLT+qOPzXd6/d/MdxciEuMRIgs2z+8nmTs+Sz9UDpAkyIDp2rXArl3GAnn3XeD9901AtahcGrpXI0aYGBK76D1jubxjN77QzaI7RauI+y4uzKamBTZQuzzsJg8ELSgOfGUcivt67DHTtR4qvXqa/XNgqQNBZonxCJEF4yXdLtYu0Argf98APbX4lCYUGw47YK9RYXEeChwTEOmmsQQH/+b7+XkubIYUJIqTJx+H7hZHxzORj0Mo6CoxZ4eDOv1Bt4zu1RdfGPfo1ltN/lGobhFhELxnL2C6dvf+9z9z/CEgwiNEFix30aihycxdsVzfpfVjeYFxGgoI40G0rpYuBTZuNMLDpEZaWh4RovDQsmCNIC6MA3FkPBP5AsE4ErfJ7bCERzAB7+Lw4kvAM/oY7h+kz0NbVCEgrpYQWWzZrC0FfXdmPZ348BW2CgrWvGGAuWtXIzAMKLMEBQPctNQoGOzy5rEz54bjqS68EBgwwOQVUUwKwzPe6rzznBcd0rSJ3q4+Rva2hYhYPEJkwXFV7OZmbRm6HMUZ+yQUDoePMD3hzLbAvLn2ypIhFo8QWTCWQli+QkTHWZpoi4ffaUmTJ70Q4REiC8ZFGJxlsFdwFn6nDFSzXGqIiPAIkQV7hQhjKCWF2+AQAw5XKI+wtysQFF3W+GFiYyjfgT88URnPdxwCIjyC4A27tBkn4pCJd94BDh60XyglOAzj7bdNXZ3CmD4dmD/f9Fqxng/LW3BYCAPU3nB7rGLIHCNmPJdTRHgEwRtOd/P112YGCvbesFubjZvDGzhkgTkszKX57TeTNBgKzLthF/lTT5nBqCxYNm4cMGqUSQDkegoMa+lwvxxzxZwe9ngxmY+Wj2+ODuvn8Ng5nmzLFntl+UOERxC8YTlSlhIlTPajADBbmNnAzMGhGHCgKAeg8jkT/EoCLSuOn/rhB5MYePXVJobCLnFW++NAUA4W5ess6bFihcmwvuAC4PzzzXgyWmXMmvbmlFNMnhDHcvGxnCLCIwgemIFMS4YJexQDjkDnI2sU/+1vwN//Dlx1lbGCaFnQrSnO8AHGSJgwSMuF1f0YR/rPf4yosTQr82969jTpADfdZMSNI8EZU+GwB67ngE8P/iopsoQFBYlDOdq2tVeWP0R4BMEDBWfsWJP1zGS/Rx4xNZZZHqNfP7OOxRyYpEc4Kr2ochveMNhLa4ouEpMBmRTIsVTMjfE3rIFWC0thUEQ42j4YaEmxvjKDzKGUVS1lRHgEgULA4C6LmlMAGJxl8TAKiy98Ly0jwtkg6tQxfwcDLSV+hrNUcBwVrZNA3f4UKIoNZ59g8THmJQUDYzusscxAdHF7n+g2lmQAawkQ4RFObhg4ZqyGAVwWyeIjRSeQoLAx8zO0WCgcxRmaQFHzBIYDwcbvKWLGoRScB6xlS/vFIGBxMbqBHIrBQHkwUExZJ4iC68DI82AQ4RFObigctEIefND0ILGURmEZzxwZfsMNwKBBx8ZbSoon7vPTT6bh07ph7xmnu+FcWLSOipMM6XHZOIsFR5QXBYWK8SaW+aCLWVSBMYcQ4RFObtiLxEGbDBxzSIC/WIsHxmhYf5kJeixByho5JYHuzOzZpgYQYzzs3aKrx/o5jClxUj+OQqd7VdzBnhRGWlR0B+naBYKCxy56xo9Y7pVxJg5G9e0lKyVEeASBDb4wwfHAJD5aJUziCwUKGPfHwDELj7FrnAtdPAogS5uWRNQYUGYNZ7qCnLOrsOLuLMtBC4+9eOym5/6DjSM5gAiPIAQDk/H+7/+MBUKrhBZPSaHQUVxoaXHh37S2aK2EMrA1JcVUVSSMUdGa84WxH+YhsRufAsgANutSs6piGBHhEYSi4FQ37IX6808TD6FL4q/HK1joAjEHh93dwVhawcJMaBYMI/6q3TCIzAxs1v7hVM7MwGZ6AOdsL8wtKwVEeAQhEIzFcFwU83nYU8TEPIpOceZdDye0cDyT9rEbnpnXFCK6hnSrmI9Ei4piw14z9szRynJS/IJECoEJkQUbFeMwHneiJNBlmTDBBJJp5TBYy2ETFJ7iJAz6g82NwyA4IykbPF02ul7M2+HYLwoDKw8yPsPMZhZvZ7yGghfM9DQMGHNYB7fHusu0qthFfvrpwL33mlrJJQ2Kp6ebY2M3f4iyIRaPIBA2JOa9cB4qVi9kzxLLYrDBszEzFhKq6BCKDbfDsVgUF8aM2O3N55w9gm4P59ziHF7Nm5s5wbgEKvDuC60YlkvlNmn1MKbDoDXdKmZfl1R0HEYsHiGyKKnFw0bK9zOvhfk57F5mI+agS08BrFBg4iEHoDKgy1wZ9kBRHOi2sQkycZCV/Wj1UIzoNvG9rKjIY+P7eFwcJMpBpIXBoR9MQmQsh2LDnCB/47qKi4MWjwiPEFmUVHgoBJ65sej6cAmlh8kX1iumFUVhYyCXlgcbMUWIokaXiANCKXRcx/gSj4WwPAfzfDgGiwNWKSq0iALBzzGPh02bglXcXKBAiPAIQgCciPGUBhQ2igHFjU2OFoh3TxLFh8+58G9Ps6RoMEDMoRzffmtKZHCUelkgMR5BOMGggNCSYSyHOTYMGNPq8Sx8jWJEt47WFh9pBdE9Y/4Q50yfMqXsRMdhRHgEoTxBS4LWEbvvWYaDcaZZs8xgUSYZRggiPIJQllBoGARm9zpdRAoMs5n52L27GdPFXCL+HUGI8AhCIDwBZ0/9HSdhnIRJfaxyyJ4qDlvg4FNaNc8/bwSHI9U5nsvJIHc5QYRHEALBJEJ2YXPgJktXOAljPByV/v33pseLpTA+/9wUf2f3N+M8JNRu/HKKCI8gBIIjvZk5PG0aMHGivdIhKCi0ZBh09vRmefdyRTgiPIIQCObAcDAoXa0NGxyZyO4IjO1w0CbduZMQER5BCAS7uT2lJdjV7XF/nIDDM775xnSR043j0AbGfTjCnMmDFCVPAmEEIsIjCIFgHg1dIebUcPyUk/EWWlIMXDO2w0RHZiRzACcDzpzQ74svzAR/zN8JxSpi0p+/4DgtLgpcMOVRSwERHkEIBEtMMAhMq8dfUa1QoAXF2s0vvGBKoDKwzC50DiBlTIkZyhzcycGpHK8VDJ56PJ6sYg5yZQCbs5T6CgyHbrzxhqmq6KQLGSQiPIIQCI6H4oDMtDTn51BnjZwhQ4zATJ1qtn/uuabWMif8o7XDsqSc/SGYKXQoNuyFo9B4hIqWDhMROdSCPWfecBQ+38uZUoMVNgcR4RGEQLDXie6WZ0obJ2GpCxYYY51jVgUcOdKMJKcVxHFZkyebaYu5bx5DUdANpFtG8eGAUsIpjFu1MnNs0YrydrloATGGRFGlyxdmRHgEIRB0ezwV/ZyuOsjGTjeOcR0mD7L4Oq0QJg0yiM2gM12k8eODj8NwACfdLYoWXSnCLGhabbR6KHAeeF48J4pTsLV+HESERxACwYGbnO6FgWVW8HMS1syhm8UsZQoM6zpTcDg9Muf4GjPGxH1Y4znYzOXOnU0WNN00ig/hUAsWNqPIeFs8HHzK/XG+dicKnBUTER5BCAQDyhyywBKiHFXuJGzwDB5zhgkGhDlE4u23gaFDzVxXr7xi3CMKFGe44HiuoqDbduONpoqhJxjOR9YB+uwzY/0Q9mZRhDh5IafXcaJIWDGRejxCZOFkPR66ONwe3RTOq84YTGlCAWKtZHalMy7DvB5aOywOxlkuaLkUVdSL+T/MCWIKgD9LhoLD4DUtLFpTFKlgkXo8ghAGli83JSlYdN3p4LI/GPOhEHDqYg7R4JzuFBvGYRg4pqVSFAxEsxfMV3SYC0TLiVYVF27TafexGIjwCEIgmF3Mbm5PImE4YcCXdZ9pabEQGKejKalLRNFkwfoHHjC5O61bG5eMQldGiPAIQmHQpaDohFt4nIBxoT/+MEFsLiwaT8GhRRXKTKgOIMIjCIGg6HA5EUeOMx4zdqzpqmeQmpUM2V3PHCG6WWWMCI8gBIKxkkaNTM8WH51g5UoTv2FVwZ9+An7/3cz4yRiOk9C94tKjh8kR4rAMdrWXE6RXS4gsnOzVYpLfokXGzWKOjBOj0zkMggFrbptDFdj7RJeIjwwscz/Mag51QCqbtdNFxKRXSxDCABMImftCS8GpkhjcHqcYHjzYJApedhlw9tkmmExriIXHAsHXmNcTzGh1p0XHYcTiESKL8jqvVjCwu5zNkSLnLRysxfzLL6YYGacnvvbasgl2i8UjCBEIBYcuF0WHAzhZq4e9URRSjjJv08YMqTgRe9h8EOERIosTuW4xM445zopjt+iKsRD82rVGbDxDKYIpkVHaOODGiaslRBasL8M6N6zkxxkbyiP795seJw7JYFkKxm5o0XBudA6R4GPz5sDll5uAs9PjxLxhYNuTmEh3jkM2WrY0tYF8y3HwvfXqadXQ4n5Qi2QIiPAIkcVXXwEDBgB33AG8807x7s6eWscMKntITjbbZKlSTnNTo4b9Qglhjxa3R/HhAE6Wp+DCXi6OhGfxsdNOAzp1Cn5UOmEz9mybwWemAFx1FdCihf2GAHz5pYndcL8UQY5q59AKZkxT+LyPgbWh27cHGjbSQrnSXlkyxNUSIgvekcn6DUZEgoUuzWOPmeEJrGnjgXk2I0YAX39txmyFCocpsLucvVls2BwdfvvtxjpjLxcLgTHZrziiQyiwHN7BTOXRo832Bg4058NzC1Q4nmL70UcmlsTR7RRtj4Xla5MwuE1R46DVEBHhESKLUxqYBrNTWyrBwhHbLD/66afAnDnHlqDg3Z/5NhQCJ4qBtW1raivTIrv+elMew6lCXOyqp6vJIREUMPY+DRtm9sf6Pt6C6uHqq83rtGQoildcYcZzcZyYr6tF4aXw1NPWUYiI8AiRBd0WBmA5wHOjvkMHAxsTrQVPN7H3nZ4WFIcYMObiVPZyaUFXjZnKtNxYRJ41fR59FDjzTHNO/ioZVqtm6gLx/Rw8SujqebubhJ9fZOcQsXctRER4hMgiKRG4SN/50zOAWdp6CQZ2Y7M8KHvEvEWHMG/mlltMJb8yKJjlF1+3iTEaFnOfORPYu9eso2BShAYNMgL0t7+ZecL8we55ltFg9z0D2/6gaDHrmpZfz172ypIjwiNEFmwYDKqycXJwpK+Q+IMNj6LChX9758mwwa1fbwpnsVCX07DmD+vuFDVFMkWBM1Pw/Yw30ZJhtztFh+4hp8Ghy8SF7hbHfxGKKa0aLoWlGnCbjGUFGjPGjGm6WrR2aBGFSPSzGvtvQTjxYZC1qnY5GK9hjxTLe9KaKQx+hqLCxkV3ir1Xnt4wNnTO+MkGfsklztYnZiyJ1hR7lmhpsCdq3jwT0KZ1wcqHFByuozXDXiu6hBzCQZeHx0ghoLiyhjK76BmT4vs4AJXnRBfR123yhcFnDuNgLxvjTnTZfGHcaK4WuFtuNUFxz/dTQsTiESIPxnh6aXeAcR6OzA4G9uhwzBS7lb0tA26DXd+s1le/vr3SB1oiFIfiTBNDS4pd1hQZ7rt/f2OlMcBL8fPUeabg0FXirBEsvcrhEgxQs64ORYKiwi5z2g8cgc7qgiz4RauECYjMZ2KOUCC2bjVDS2ghUVD8JShymp3JPwCJel9/0/sMUXQsmMcjCBGF263U/PlKVauuVMOGSi1YYL9QCEuXKnX55UoNGmSvsHntNaVq1VJq+HB7hQ9btijVp49Szz+v1KFD9spC2LFDqbFjlbrrLqWuuUapl15SavZspbKylMrJUSo72zzm5iq1fbtSPXsqFRen1MMP2xsIAm6LxzV1qlKTJim1a5f9gg+rVil1/fVKJSUpdeedSm3bZr/gwz33KBUVZd6TkWGvDA2xeITIg3dk9uQwP4bu1nuj9R3Wfs0fdKOYOEerxXs6X+YB0QXiVMB0bXyhJURrhJ9n7k1RpUS5jSVLjEtE64JT2zA2wy51dtV7xmrxkV3Z7GZnT9Pdd5v3BQu3RauJ87H37Ok/85nVCDmQlm4kY2KcXNBfgbC5+jvhd0N39YEHTSKlA4jwCJEJA8W3DTBzYn33LTBeN7BAcPQ3GyAD074lQen+eIKz3jDxjrN/fvyx+UzHjvYLhcCgNeMzzLHp08fEZwLFXxizodhQqCigJS3Mzn16u448HwbKGZxmMTK6pJxtgjOO+sJ40ojhRnzZ5d6iuf1C6IjwCJELxxwxg5exF8ZAGHD1hVbNsmXGAqB4MKHOAxMLGaD1WCHeMCDMhRbJddcFP5TCI2K+2/PAXCJO5McZRWlRscg7rbdQYc8c52FnjIgBbfZQMVOaFhtjW76kaSuOA1N/+AHodrERP34PTmG7XIIQmRw8qNSTTykVHa1U53OUmjvPfsHmzz+V6tZNqcaNTTzEm5QUpW6/3Xz2lVeU2rdPqXHjlLr7bqWuuEKpwYNN/IjxmFDYs0epOXOUGjFCqUsuUeqii5QaNszEgxjvCZaCAhPf8cSJGHOaMUOpgQOVat5cqYQEpRo0UOrRR01MKy3N/qAP/PzQoUpVqqTUGWcoNWuWiZs5iAwSFSIfugqMZ3zyqUkufPkloMu5psua5SdoWbA7mXd131wXdiPzs0zIY3yD72VvEhPz2BvFdcXt5WGsh9UGWVaVrhStLXbTs7eKLg8tL/agBZuwyC50Fnbn3OiMN9G9YrNmmQ0ujFtxnxx4ynO8WFswgYZpsIufBeE5xitau56cgbS7fr/v8IkQEeERTg7WrAUefxyYNAk49xwzGJLTBjPQy+5nNkZ/AzPZlfzMMybBj/Eifo7xDv5dUjhmirk2FAO6XezCZqyHIkZ3priNfNw4M0xixw7znNvh2CvGkShmzFjmPhgg5t+Btn8gRbukz5kcKAowExuZ0+TEGDUfRHiEkwNe5cyFefFFE0OpU9vM0sneHObMFMaff5qALGNGLMYeas8OmxyD07SUfDOlSwJzjZgPxLIVFM8mTYzVRCuNz7mfoqyy6dOBl7Sl89tM/d3UNYHzXj1DP9cAiPAIJxc7d5mR2iNHANnarThbC8k99wJX99bWQCX7TT6wiTDhz7eHqLzA42NvFY+xuGK2Qrt8LKMxYYL+bpKBrt1MQP6C8wMHwB1AhEc4+aCrw7o1jGXwkbEUVvrjOCfWx2EPlT+3KxJgc6eLx0GlH38CTPnR5DpV1+d8zz3AnXeYHKBSFlgRHuHkhBYCR3Iz5vPuKH3nX27EhkFXihCT+po1Bzppl4UzK7CxnojQAmJwmblKy1dot3GZiW1xqAQDybTy+vY1wfLWrUolnuMPER7h5MatL39mJjO2wbKhjJUwl4bQXWEgNtQYTFnDJk43jDlL/JvWDMd39e4D3NjfxIToVoXRjRThEQTiiZHs01bQEm0VMJjMbu7Nm0wyXXmM7QQDmzeFk4NfOfq9XVvg4m6mbjIDxzyvogLPpYAIjyAIYecElXFBEE5kRHgEQQg7IjyCIIQdER5BEMKOCI8gCGFHhEcQhLAjwiMIQtgR4REEIeyI8AiCEHZEeARBCDsiPIIghB0RHkEQwo4IjyAIYUeERxCEsCPCIwhC2BHhEQQh7IjwCIIQdkR4BEEIOyI8giCEHREeQRDCjgiPIAhhR4RHEISwI8IjCELYEeERBCHsiPAIghB2RHgEQQg7IjyCIIQdER5BEMKOCI8gCGEG+H/1F7pwzlpiXAAAAABJRU5ErkJggg==');

        }
        .right{
            text-align:right;
        }
        .total{
            text-align:right;
        }
        .font_small{
            font-size:13px;
            line-height: 1.2em;
        }
        .box_sita{
            position: absolute;
            bottom: 0;
            width:100%;
        }
        div.bikoubox {
            border: 1px solid black;
            border-radius: 10px;
            padding: 10px;
            }
        div.bankbox {
            border: 1px solid black;
            padding: 10px;
            margin-bottom:30px;
            }
        h1 {
            text-align: center;
            font-size: 40px;
            margin-top: 0px;
            font-style: bold;
            letter-spacing: 0.2em;
        }               
    </style>
</head>
@foreach ($invoices as $invoice)
<body>
    <div class="container">
       
    <table>
        <tr>
            <td  colspan="3"><h1>受領書</h1></td>
        </tr>
        <tr>
            <td colspan="3" class="title">{{$invoice->name}}御中</td>
        </tr>
        <tr>
            <td colspan="3" class="right">受領日：{{$invoice->juryo_day}}<br>
                登録番号：T3810698883065<br>
                請求書No：{{1000+($invoice->id)}}
            </td>
        </tr>
        <tr>
            <td colspan="3" class="log">
                ナレッジ・プログラミングスクール<br>
                代表 市川 葉子<br>
                <span class="font_small">〒156-0052 東京都世田谷区経堂<br>
                1-19-6 清水ビル4F<br>
                電話:03-6432-6270</span>
            </td>
        </tr>
        <tr>
            <td height="100px" >受領金額（消費税込）</td>
            <td ><font size="23"><strong>￥{{number_format($invoice->price)}} -</strong></font></td>
            <td class="right">お支払方法：{{$invoice->howpay}}</td>
        </tr>
        <tr>
            <td  colspan="3"><hr></td>
        </tr>
    </table>
    <table>
        <tr>
            <th>取引期間</th>
            <th>サービス名</th>
            <th>単価</th>
            <th>数量</th>
            <th>金額</th>
        </tr>
        <tr>
            <td  colspan="5"><hr></td>
        </tr>
        <tr>
            <td>{{$invoice->service_period}}</td>
            <td>{{$invoice->service}}</td>
            <td class="right">{{number_format($invoice->tanka)}}</td>
            <td class="right">{{number_format($invoice->count)}}</td>
            <td class="right">{{number_format($invoice->price)}}</td>
        </tr>
        <tr>
            <td  colspan="5"><hr></td>
        </tr>
        <tr >
            <td colspan="5" class="total">小計：{{number_format($invoice->price)}}<br>
            （内消費税額 10% {{number_format($invoice->tax)}}）<br>
            合計：{{number_format($invoice->price)}}
            </td>
        </tr>        
    </table>
    <div class="box_sita">   
        <div class="bikoubox">【備考】
            <ul>
                <li>カード会社発行の利用明細書や銀行振り込みの控え（振込明細）が正式な領収書となりますので、当受領書と合わせて大切に保管してください。</li>
                <li>当受領書は電子文書であり印紙税は非課税となりますので、印紙は添付しません。（当受領書を印刷した場合も同様です。）</li>
            </ul>
    </div>    
    <div>
    </div>
    </body>
    @endforeach
</html>