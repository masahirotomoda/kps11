<table>
    <thead>
    <tr>
        <th>学年</th>
        <th>組</th>
        <th>出席番号</th>
        <th>氏名</th>
        <th>コース名</th>
        <th>最高点</th>
        <th>級（キータッチ2000検定コースのみ表示)</th>
        <th>日付</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($bestScore as $bs)
        <tr>
        <td>{{$bs->grade_name}}</td>
            <td>{{$bs->kumi_name}}</td>
            <td>{{$bs->attendance_no}}</td>
            <td>{{$bs->name}}</td>
            <td>{{$bs->course_name}}</td>
            <td>{{$bs->best_point}}</td>
            <td>{{$bs->bestpoint_rank}}</td>
            <td>{{date_format(new DateTime($bs->b_created_at),'Y年m月d日 H:i')}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
