
    <table>
        <thead>
        <tr>
            <th>日時</th>
            <th>コース名</th>
            <th>学年</th>
            <th>組</th>
            <th>出席番号</th>
            <th>氏名</th>
            <th>スコア</th>
            <th>級/段</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($scoreHistory as $sh)
            <tr>
                <td>{{ date_format($sh->created_at,'Y年m月d日 H:i') }}</td>
                <td>{{ $sh->course_name }}</td>
                <td>{{ $sh->grade_name }}</td>
                <td>{{ $sh->kumi_name }}</td>
                <td>{{ $sh->attendance_no }}</td>
                <td>{{ $sh->name }}</td>
                <td>{{ $sh->point }}</td>
                <td>{{ $sh->rank }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

