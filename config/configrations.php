<?php
return[
    'PAGENATION' => 200,
    //'SESSION_TIME' => 60*60*3, //もとは60*60*3　　※.envで設定
    'COPYRIGHT' => '© knowledge programming school.',
    'TRIAL_PERIOD_SCHOOL' => 92,
    'TRIAL_PERIOD_CRAM' => 31,
    'META_TITLE' => 'ナレッジタイピング',
    'TRIAL_SCHOOL_NO' => 1,
    'TRIAL_SCHOOL_NO_URL' => 'tri12345',
    'DOMAIN_NAME' => 'https://n-typing.com/',
    'ADMIN_ADDRESS' => '東京都世田谷区経堂1-19-6　清水ビル4F',
    'ADMIN_ZIP' => '156-0052',
    'ADMIN_TEL' => '03-6432-6270',
    'ADMIN_MAIL' => 'info-knowledge@knowledge-p.co.jp',
    'ADMIN_IPADDRESS' => '183.77.254.227',
    'ADMIN_SCHOOL' => 'ナレッジ・プログラミングスクール',
    'ADMIN_SCHOOL_NO' => '2',
    'ADMIN_SCHOOL_NO_URL' => 'Sv4EXsJQ',
    'SCHOOL_ACCOUNT_FOR_ADMIN' => 'b33259',

    //アバター
    'AVATAR' => [
        'green' => 'green',
        'blue' => 'blue',
        'pink' => 'pink',
        'red' => 'red',
        'white' => 'white',
        'yellow' => 'yellow',
        'black' => 'black',
    ],
    //学校カテゴリー　ひらがな表記、通常の漢字表記にするかのフラグ
    'SCHOOL_CATEGORY' => [
        1 => '漢字',
        2 => 'かな',
    ],
    //ロールの切り分けで使用
    'ROLE_LIST' => [
        1 => 'システム管理者',
        2 => '学校管理者',
        3 => '先生',
        4 => '生徒',
    ],

    'ROLE_LIST2' => [
        '学校管理者' => '学校管理者',
        '先生' => '先生',
        '生徒' => '生徒',
    ],
    //システム管理　ユーザー更新のドロップダウン値（生徒が一番上にくる）
    'ROLE_LIST3' => [  
        '生徒' => '生徒',
        '先生' => '先生',
        '学校管理者' => '学校管理者',
    ],
    'LEVEL_LIST' => [
        '★' => '★',
        '★★' => '★★',
        '★★★' => '★★★',
        'その他' => 'その他',
        '1月' => '1月',
        '2月' => '2月',
        '3月' => '3月',
        '4月' => '4月',
        '5月' => '5月',
        '6月' => '6月',
        '7月' => '7月',
        '8月' => '8月',
        '9月' => '9月',
        '10月' => '10月',
        '11月' => '11月',
        '12月' => '12月',
    ],
    'CATEGORY_LIST' => [
        'ホームポジション' => 'ホームポジション',
        'ローマ字50音' => 'ローマ字50音',
        'ことば' => 'ことば',
        'ことわざ' => 'ことわざ',
        '英単語' => '英単語',
        '短い文章' => '短い文章',
        '長い文章' => '長い文章',
        'テスト' => 'テスト',
        '今月のタイピング' => '今月のタイピング',
        'キータッチ2000検定' => 'キータッチ2000検定',
        'その他' => 'その他',
        'vld' => 'vld',
    ],
    'COURSE_TYPE' => [
        '組込' => '組込',
        'テスト' => 'テスト',
        '追加' => '追加',
        '長文' => '長文',
        '地図' => '地図',
        '世界遺産' => '世界遺産',
        '英語' => '英語',
    ],
    'COURSE_TAB' => [
        'month' => 'month',
        'star1' => 'star1',
        'star2' => 'star2',
        'star3' => 'star3',        
        'test' => 'test',
        'add' => 'add',
        'key' => 'key',
        'long' => 'long',
        'map' => 'map',
        'contest' => 'contest',
        'isan' => 'isan',
        'english1' => 'english1',
        'english2' => 'english2',
        'english3' => 'english3',
        'english4' => 'english4',
        'english5' => 'english5',
        'vld' => 'vld',
        'vldtest' => 'vldtest',
        'battle' => 'battle',
        'chg' => 'chg',
    ],  
    'DEMO_CONTEST_INFO' => [
        'testmode_school_id' => 4,//すみれ小学校
        'practicemode_school_id' => 3,//さくら小学校
    ],

    'ENTRY_TYPE' => [
        '小学校 ' => '小学校',
        '中学校' => '中学校',
        '高校' => '高校',
        '専門学校' => '専門学校',
        '大学' => '大学',
        'プログラミング教室' => 'プログラミング教室',
        '塾' => '塾',
        '各種教室' => '各種教室',
        'その他' => 'その他',
    ],
    'SCHOOL_GRADE' => [
        '' => '',
        'なし' => 'なし',
        '1年' => '1年',
        '2年' => '2年',
        '3年' => '3年',
        '4年' => '4年',
        '5年' => '5年',
        '6年' => '6年',
    ],
    'SCHOOL_KUMI' => [
        '' => '',
        'なし' => 'なし',
        '1組' => '1組',
        '2組' => '2組',
        '3組' => '3組',
        '4組' => '4組',
        '5組' => '5組',
        '6組' => '6組',
        '7組' => '7組',
        '8組' => '8組',
        '9組' => '9組',
        '10組' => '10組',
    ],
    'SCHOOL_ATTENDANCE_NO' => [
        '' => '',
        'なし' => 'なし',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
        '21' => '21',
        '22' => '22',
        '23' => '23',
        '24' => '24',
        '25' => '25',
        '26' => '26',
        '27' => '27',
        '28' => '28',
        '29' => '29',
        '30' => '30',
        '31' => '31',
        '32' => '32',
        '33' => '33',
        '34' => '34',
        '35' => '35',
        '36' => '36',
        '37' => '37',
        '38' => '38',
        '39' => '39',
        '40' => '40',
        '41' => '41',
        '42' => '42',
        '43' => '43',
        '44' => '44',
        '45' => '45',
        '46' => '46',
        '47' => '47',
        '48' => '48',
        '49' => '49',
        '50' => '50',
        'その他' => 'その他',
    ],



 //ROLE STYLE未使用⇒view/layout/schooltyping 
 //ROLE STYLE未使用⇒views/layouts/free
 //login(先生、学校、システム)のROLE STYLEは、ファイルに直接設定
 
 'PAGE_INFO' => [
    //◎生徒メニュー　＞　生徒プロフィール
    'student/profile' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => 'タイピングコースにもどる',
        'BACK_BTN' => '../user-menu',
        'KANRI_TITLE' => 'マイメニュー',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
        'ALERT_TITLE' => '保存できませんでした。もう一度、入力してみよう。',
    ], 
    //■先生メニュー　＞　メインメニュー
    'teacher/menu' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => 'コースメニューにもどる',
        'BACK_BTN' => '../user-menu',
        'KANRI_TITLE' => '先生メニュー',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　メインメニュー　＞　すべてのお知らせ
    'teacher/menu-allinfo' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'すべてのお知らせ',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'information',
    ],
    //■先生メニュー　＞　メインメニュー　＞　動画紹介
    'teacher/movie-introduction' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'ナレッジタイピング　動画で見てみよう！',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　プロフィール
    'teacher/profile' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'マイアカウント',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　生徒情報　＞　生徒一覧
    'teacher/student-list' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' => './student-list',
        'PAGINATION' => 'users',
    ],

    //■先生メニュー　＞　生徒情報　＞　更新
    'teacher/student-list-update' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '先生メニューにもどる',
        'SEARCH_BTN' => './student-list',
        'PAGINATION' => 'users',
    ],
    //■先生メニュー　＞　生徒情報　＞　検索
    'teacher/student-search' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' => './student-list',
        'PAGINATION' => 'users',
    ],

    //■先生メニュー　＞　生徒情報　＞　新規 
    'teacher/student-list-new' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' =>  './student-list',
        'PAGINATION' => 'users',
    ], 
    //■トライアル先生メニュー　＞　トライアル生徒情報　＞　トライアル生徒一覧
    'teacher/studentlists' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' => './studentlists',
        'PAGINATION' => null,
    ],
    //■トライアル先生メニュー　＞　トライアル生徒情報　＞　トライアル・検索
    'teacher/students-search' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' => './studentlists',
        'PAGINATION' => null,
    ],
    //■トライアル先生メニュー　＞　トライアル生徒情報　＞　トライアル生徒更新
    'teacher/students-update' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '先生メニューにもどる',
        'SEARCH_BTN' => './studentlists',
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　クラス一覧
    'teacher/byclass' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'クラス一覧',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　クラス一覧　＞　クラス別所属生徒
    'teacher/byclass-belongs' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => 'クラス一覧にもどる',
        'BACK_BTN' => './byclass',
        'KANRI_TITLE' => 'クラス所属生徒',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　スコア履歴 
    'teacher/transcript-print' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'スコア履歴（エクセル出力・記録証印刷）',
        'SEARCH_BTN' => './transcript-print',
        'PAGINATION' => 'scoreHistory',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
    //■先生メニュー　＞　スコア履歴　＞　検索
    'teacher/transcript-print-bt' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'スコア履歴（エクセル出力・記録証印刷）',
        'SEARCH_BTN' => './transcript-print',
        'PAGINATION' => 'scoreHistory',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
    //■先生メニュー　＞　ベストスコア　＞　一覧
    'teacher/bestscore' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒別ベストスコア',
        'SEARCH_BTN' => './bestscore',
        'PAGINATION' => 'bestScore',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
    //■先生メニュー　＞　ベストスコア　＞　検索
    'teacher/bestscore-search' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒別ベストスコア',
        'SEARCH_BTN' => './bestscore',
        'PAGINATION' => 'bestScore',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
     //■先生メニュー　＞　ランキングスタジアム　スコア一覧
     'teacher/battlescore-history' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'ランキングスタジアム スコア一覧',
        'SEARCH_BTN' => './battlescore-history',
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　ランキングスタジアム　スコア一覧 ＞検索
    'teacher/battlescore-history-search' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'ランキングスタジアム スコア一覧',
        'SEARCH_BTN' => './battlescore-history',
        'PAGINATION' => null,
    ],
     //■先生メニュー　＞　コース一覧
     'teacher/add-course' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'タイピングコース作成',
        'SEARCH_BTN' => './add-course',
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　コース一覧　＞　検索
    'teacher/addcourse-search' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => '先生メニューにもどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'タイピングコース作成',
        'SEARCH_BTN' => './add-course',
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　コース一覧　＞　単語一覧
    'teacher/addcourse-add-word' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => 'コース一覧にもどる',
        'BACK_BTN' => '../teacher/add-course',
        'KANRI_TITLE' => 'タイピングコース単語登録',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　コース追加　＞　単語一覧　＞　新規＆更新
    'teacher/add-word' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => 'コース一覧にもどる',
        'BACK_BTN' => '../teacher/add-course',
        'KANRI_TITLE' => 'タイピングコース単語登録',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //■先生メニュー　＞　コース追加　＞　単語一覧　＞　削除
    'teacher/addcourse-word-delete' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => 'コース一覧にもどる',
        'BACK_BTN' => './add-course',
        'KANRI_TITLE' => 'タイピングコース単語登録',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ], 
   //●学校管理メニュー　＞　メインメニュー　＞　全てのお知らせ
   'school/menu-allinfo' => [
    'ROLE_STYLE' => 'bgSchool' ,
    'BACK_BTN_NAME' => 'もどる',
    'BACK_BTN' => './menu',
    'KANRI_TITLE' => 'すべてのお知らせ',
    'SEARCH_BTN' => null,
    'PAGINATION' => 'information',
],
    //●学校管理メニュー　＞　メインメニュー
    'school/menu' => [
    'ROLE_STYLE' => 'bgSchool' ,
    'BACK_BTN_NAME' => 'もどる',
    'BACK_BTN' => null,
    'KANRI_TITLE' => '学校管理メニュー',
    'SEARCH_BTN' => null,
    'PAGINATION' => null,
],
    //●学校管理メニュー　＞　学校アカウント情報
    'school/profile' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '学校アカウント情報',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　学校アカウント情報　＞　パスワードリセット
    'school/userpas-change' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '学校管理情報',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　先生一覧
    'school/teacherlist' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '先生一覧',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　先生一覧　＞　検索＆エクセル出力
    'school/teacher-search' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '先生一覧',
        'SEARCH_BTN' => './teacherlist',
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　先生一覧　＞　エクセル一括登録
    'school/teacher-import' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '先生一覧',
        'SEARCH_BTN' => './teacherlist',
        'PAGINATION' =>  null,
    ],
    //●学校管理メニュー　＞　生徒一覧
    'school/studentlist' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' => './studentlist',
        'PAGINATION' => 'users',
    ],
    //●学校管理メニュー　＞　生徒一覧　＞　検索＆エクセル出力
    'school/student-search' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' => './studentlist',
        'PAGINATION' => 'users',
    ],
    //●学校管理メニュー　＞　生徒一覧　＞　新規＆更新
    'school/student-list-new' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' => './studentlist',
        'PAGINATION' => 'users',
    ],
    //●学校管理メニュー　＞　生徒一覧　＞　エクセル一括登録
    'school/student-import' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'users',
    ],
    //●学校管理メニュー　＞　生徒一覧　＞　本日登録の生徒(インポート後の確認)
    'school/today-student' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒一覧',
        'SEARCH_BTN' => './studentlist',
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　コンテスト　＞　生徒一覧
    'school/contest-studentlist' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '【コンテスト】生徒一覧',
        'SEARCH_BTN' => './contest-studentlist',
        'PAGINATION' => 'users',
    ],
    //●学校管理メニュー　＞　コンテスト　＞　生徒一覧　＞　検索＆エクセル出力
    'school/contest-studentlist-search' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '【コンテスト】生徒一覧',
        'SEARCH_BTN' => './contest-studentlist',
        'PAGINATION' => 'users',
    ],

    //●学校管理メニュー　＞　クラス一覧
    'school/byclass' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'クラス一覧',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　クラス一覧　＞　クラス別所属生徒
    'school/byclass-belongs' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'クラス一覧にもどる',
        'BACK_BTN' => './byclass',
        'KANRI_TITLE' => 'クラス所属生徒',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　スコア履歴一覧
    'school/transcript-print' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'スコア履歴（エクセル出力・記録証印刷）',
        'SEARCH_BTN' => './transcript-print',
        'PAGINATION' => 'scoreHistory',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
    //●学校管理メニュー　＞　コンテスト　＞スコア履歴一覧
    'school/contest-scorelist' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '【コンテスト】 スコア履歴 （エクセル出力・記録証印刷）',
        'SEARCH_BTN' => './contest-scorelist',
        'PAGINATION' => 'scoreHistory',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
    //●学校管理メニュー　＞　スコア履歴一覧　＞　検索＆エクセル出力
    'school/transcript-print-bt' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'スコア履歴（エクセル出力・記録証印刷）',
        'SEARCH_BTN' => './transcript-print',
        'PAGINATION' => 'scoreHistory',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
    //●学校管理メニュー　＞　コンテスト　＞　スコア履歴一覧　＞　検索＆エクセル出力
    'school/contest-scorelist-search' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '【コンテスト】 スコア履歴 （エクセル出力・記録証印刷）',
        'SEARCH_BTN' => './contest-scorelist',
        'PAGINATION' => 'scoreHistory',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
    //●学校管理メニュー　＞　ベストスコア
    'school/bestscore' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒別ベストスコア',
        'SEARCH_BTN' => './bestscore',
        'PAGINATION' => 'bestScore',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
    //●学校管理メニュー　＞　ベストスコア　＞　検索＆エクセル出力
    'school/bestscore-search' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '生徒別ベストスコア',
        'SEARCH_BTN' => './bestscore',
        'PAGINATION' => 'bestScore',
        'ALERT_TITLE' => '次の理由で、出力が停止しました',
    ],
    //●学校管理メニュー　＞　ランキングバトルスコア一覧
    'school/battlescore-history' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'ランキングスタジアムスコア一覧',
        'SEARCH_BTN' => './battlescore-history',
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　ランキングバトルスコア一覧　＞コース検索
    'school/battlescore-history-search' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'ランキングスタジアムスコア一覧',
        'SEARCH_BTN' => './battlescore-history',
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　コース一覧
    'school/add-course' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'タイピングコース作成・編集',
        'SEARCH_BTN' => './add-course',
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　コース一覧　＞　検索
    'school/addcourse-search' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'タイピングコース作成・編集',
        'SEARCH_BTN' => './add-course',
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　コース一覧　＞　単語一覧
    'school/addcourse-add-word' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'タイピングコース一覧にもどる',
        'BACK_BTN' => './add-course',
        'KANRI_TITLE' => '単語登録・編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　コース一覧　＞　単語一覧　＞　新規
    'school/add-word' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'タイピングコース一覧にもどる',
        'BACK_BTN' => './add-course',
        'KANRI_TITLE' => '単語登録・編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
     //●学校管理メニュー　＞　コース一覧　＞　単語一覧　＞　削除
    'school/addcourse-word-delete' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'タイピングコース一覧にもどる',
        'BACK_BTN' => './add-course',
        'KANRI_TITLE' => '単語登録・編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　ログイン履歴一覧
    'school/login-history' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '管理画面ログイン履歴',
        'SEARCH_BTN' => './login-history',
        'PAGINATION' => 'login_logs',
    ],
    //●学校管理メニュー　＞　ログイン履歴　＞　検索＆エクセル出力
    'school/login-History-search' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '管理画面ログイン履歴',
        'SEARCH_BTN' => './login-history',
        'PAGINATION' => 'login_logs',
    ],
    //●学校管理メニュー　＞　請求書受領書
    'school/invoice' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => '請求書・受領書発行',
        'SEARCH_BTN' => './menu',
        'PAGINATION' => null,
    ],
    //●学校管理メニュー　＞　バレッド専用ICTタイピング検定
    'school/vld-test' => [
        'ROLE_STYLE' => 'bgSchool' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './menu',
        'KANRI_TITLE' => 'ICTタイピング検定管理',
        'SEARCH_BTN' => './menu',
        'PAGINATION' => null,
    ],
    
    //■タイピングコース一覧
    'user-menu' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => null,
        'BACK_BTN' => null,
        'KANRI_TITLE' => 'タイピングコース',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //■タイピングコース一覧　＞　検索
    'usermenu-search' => [
        'ROLE_STYLE' => 'bgTeacherstudent' ,
        'BACK_BTN_NAME' => null,
        'BACK_BTN' => null,
        'KANRI_TITLE' => 'タイピングコース',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　メインメニュー
    'system-menu' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => null,
        'BACK_BTN' => null,
        'KANRI_TITLE' => 'システム管理メニュー',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　メインメニュー　＞　すべてのトライアル
    'menu-alltrial' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'トライアル学校',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'trials',
        //'PAGINATION' => 'trials',
    ],
    //◆システム管理メニュー　＞　メインメニュー　＞　すべてのお知らせ
    'menu-allinfo' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu',
        'KANRI_TITLE' => 'すべてのお知らせ',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'information',
    ],
    //◆システム管理メニュー　＞　トライアル管理
    'trialinfo' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'トライアル学校管理',
        'SEARCH_BTN' => './trialinfo',
        'PAGINATION' =>'trials',
    ],
    //◆システム管理メニュー　＞　トライアル管理 ＞　編集画面へ遷移
    'trialinfo-edit' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'トライアル一覧にもどる',
        'BACK_BTN' => './trialinfo',
        'KANRI_TITLE' => 'トライアル学校・編集',
        'SEARCH_BTN' => null,
        'PAGINATION' =>null,
    ],
    //◆システム管理メニュー　＞　トライアル管理　＞　更新
    'trialinfo-update' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'トライアル一覧にもどる',
        'BACK_BTN' => './trialinfo',
        'KANRI_TITLE' => 'トライアル学校・編集',
        'SEARCH_BTN' => null,
        'PAGINATION' =>null,
    ],
    //◆システム管理メニュー　＞　トライアル管理　＞　検索
    'trialinfo-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'トライアル学校管理',
        'SEARCH_BTN' => './trialinfo',
        'PAGINATION' => 'trials',
    ],
    //◆システム管理メニュー　＞　トライアル管理　＞　ユーザー編集
    'trialinfo-edituser' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'トライアル一覧にもどる',
        'BACK_BTN' => './trialinfo',
        'KANRI_TITLE' => 'トライアル学校・ユーザー一括編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'users',
    ],
    //◆システム管理メニュー　＞　トライアル管理　＞　ユーザー一括更新
    'trialinfo-allupdate' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'トライアル一覧にもどる',
        'BACK_BTN' => './trialinfo',
        'KANRI_TITLE' => 'トライアル学校・ユーザー一括編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'users',
    ],
    //◆システム管理メニュー　＞　システム管理者情報
    'profile' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'システム管理者情報',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　システム管理者情報
    'school-info' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '学校管理',
        'SEARCH_BTN' => './school-info',
        'PAGINATION' => 'schools',
    ],
    //◆システム管理メニュー　＞　学校管理　＞　検索
    'school-info-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '学校管理',
        'SEARCH_BTN' => './school-info',
        'PAGINATION' => 'schools',
    ],
    //◆システム管理メニュー　＞　学校管理　＞　編集画面表示＆新規登録画面表示
    'school-info-edit' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校管理へもどる',
        'BACK_BTN' => './school-info',
        'KANRI_TITLE' => '学校の新規登録・編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校管理　＞　更新
    'school-info-update' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校管理へもどる',
        'BACK_BTN' => './school-info',
        'KANRI_TITLE' => '学校管理　編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校管理　＞　新規
    'school-add' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './school-info',
        'KANRI_TITLE' => '学校管理　編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学年管理　＞　一覧   
    'grade' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
         'KANRI_TITLE' => '学年設定',
        'SEARCH_BTN' => './grade',
        'PAGINATION' => 'grades',
    ],
    //◆システム管理メニュー　＞　学年管理　＞　検索 
    'grade-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
         'KANRI_TITLE' => '学年設定',
        'SEARCH_BTN' => './grade',
        'PAGINATION' => 'grades',
    ],
    //◆システム管理メニュー　＞　学年管理　＞　学校選択
    'grade-select' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
         'KANRI_TITLE' => '学年設定',
        'SEARCH_BTN' => './grade',
        'PAGINATION' => 'grades',
    ],
    //◆システム管理メニュー　＞　学年管理　＞　新規
    'grade-add' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
         'KANRI_TITLE' => '学年設定',
        'SEARCH_BTN' => './grade',
        'PAGINATION' => 'grades',
    ],
    //◆システム管理メニュー　＞　学年管理　＞　更新
    'grade-save' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
         'KANRI_TITLE' => '学年設定',
        'SEARCH_BTN' => './grade',
        'PAGINATION' => 'grades',
    ],
    //◆システム管理メニュー　＞　学年管理　＞　削除
    'grade-delete' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
         'KANRI_TITLE' => '学年設定',
        'SEARCH_BTN' => './grade',
        'PAGINATION' => 'grades',
    ],
    //◆システム管理メニュー　＞　学校別・学年管理　＞　学校選択
    'grade2-school-select' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
         'KANRI_TITLE' => '【学校別】学年設定　学校を選ぶ',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・学年管理　＞　学年一覧
    'grade2' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './grade2-school-select',
         'KANRI_TITLE' => '【学校別】学年設定',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・学年管理　＞　新規＆更新
    'grade2-save' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './grade2-school-select',
         'KANRI_TITLE' => '【学校別】学年設定',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・学年管理　＞　削除
    'grade2-delete' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './grade2-school-select',
         'KANRI_TITLE' => '【学校別】学年設定',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    
    //◆システム管理メニュー　＞　学校別・クラス管理　＞　一覧
    'class2' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './class2-school-select',
        'KANRI_TITLE' => '【学校別】クラス管理（学年と組）',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・クラス管理　＞　学校選択
    'class2-school-select' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '【学校別】クラス管理　学校を選ぶ',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・クラス管理　＞　検索
    'class2-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './class2-school-select',
        'KANRI_TITLE' => '【学校別】クラス管理（学年と組）',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・クラス管理　＞　新規＆更新
    'class2-add' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './class2-school-select',
        'KANRI_TITLE' => '【学校別】クラス管理（学年と組）',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・クラス管理　＞　削除
    'class2-delete' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './class2-school-select',
        'KANRI_TITLE' => '【学校別】クラス管理（学年と組）',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　クラス管理　＞　一覧
    'class' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'クラス管理（学年と組）',
        'SEARCH_BTN' => './class',
        'PAGINATION' => 'classes',
    ],
    //◆システム管理メニュー　＞　クラス管理　＞　検索
    'class-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'クラス管理（学年と組）検索',
        'SEARCH_BTN' => './class',
        'PAGINATION' => 'classes',
    ],
    //◆システム管理メニュー　＞　クラス管理　＞　学校選択
    'class-select' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'クラス管理（学年と組）',
        'SEARCH_BTN' => './class',
        'PAGINATION' => 'classes',
    ],
    //◆システム管理メニュー　＞　クラス管理　＞　新規＆更新
    'class-add' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'クラス管理（学年と組）',
        'SEARCH_BTN' => './class',
        'PAGINATION' => 'classes',
    ],
    //◆システム管理メニュー　＞　クラス管理　＞　クラス一覧　＞クラス別生徒一覧　※共通（学校別クラス管理のクラス別生徒一覧と）
    'class-student' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'クラス一覧',
        'BACK_BTN' => './class',
        'KANRI_TITLE' => 'クラス管理（学年と組）　クラス別生徒一覧',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　組管理　＞　一覧
    'kumi' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '組設定',
        'SEARCH_BTN' => './kumi',
        'PAGINATION' => 'kumi',
    ],
    //◆システム管理メニュー　＞　組管理　＞　新規＆更新
    'kumi-save' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '組設定',
        'SEARCH_BTN' => './kumi',
        'PAGINATION' => 'kumi',
    ],
    //◆システム管理メニュー　＞　組管理　＞　学校選択
    'kumi-select' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '組設定',
        'SEARCH_BTN' => './kumi',
        'PAGINATION' => 'kumi',
    ],
    //◆システム管理メニュー　＞　組管理　＞　検索
    'kumi-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '組設定',
        'SEARCH_BTN' => './kumi',
        'PAGINATION' => 'kumi',
    ],
    //◆システム管理メニュー　＞　学校別・組管理　＞　学校選択
    'kumi2-school-select' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '【学校別】組設定',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・組管理　＞　新規＆更新
    'kumi2-save' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './kumi2-school-select',
        'KANRI_TITLE' => '【学校別】組設定',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・組管理　＞　削除
    'kumi2-delete' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './kumi2-school-select',
        'KANRI_TITLE' => '【学校別】組設定',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・組管理　＞　一覧
    'kumi2' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択へもどる',
        'BACK_BTN' => './kumi2-school-select',
        'KANRI_TITLE' => '【学校別】組設定',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　ユーザー管理　＞　一覧
    'user' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'ユーザー管理',
        'SEARCH_BTN' => './user',
        'PAGINATION' => 'users',
    ],
    //◆システム管理メニュー　＞　ユーザー管理　＞　学校選択
    'user-select' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'ユーザー管理',
        'SEARCH_BTN' => './user',
        'PAGINATION' => 'users',
    ],
    //◆システム管理メニュー　＞　ユーザー管理　＞　新規＆更新
    'user-save' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'ユーザー管理',
        'SEARCH_BTN' => './user',
        'PAGINATION' => 'users',
    ],
    //◆システム管理メニュー　＞　ユーザー管理　＞　検索
    'user-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'ユーザー管理',
        'SEARCH_BTN' => './user',
        'PAGINATION' => 'users',
    ],
    //◆システム管理メニュー　＞　ユーザー管理　＞　エクセル一括登録
    'user-import' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'ユーザー管理',
        'SEARCH_BTN' => './user',
        'PAGINATION' => 'users',
    ],
     //◆システム管理メニュー　＞　学校別・ユーザー管理検索　＞　学校選択
     'user2-school-select' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '【学校別】ユーザー管理　学校を選ぶ',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
     //◆システム管理メニュー　＞　学校別・ユーザー管理一覧
     'user2' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択にもどる',
        'BACK_BTN' => './user2-school-select',
        'KANRI_TITLE' => '【学校別】ユーザー管理',
        'SEARCH_BTN' => './user2',
        'PAGINATION' => 'users',
    ],
     //◆システム管理メニュー　＞　学校別・ユーザー管理検索　＞　検索
     'user2-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択にもどる',
        'BACK_BTN' => './user2-school-select',
        'KANRI_TITLE' => '【学校別】ユーザー管理',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'users',
    ],
     //◆システム管理メニュー　＞　学校別・ユーザー管理検索　＞　新規・更新保存
     'user2-save' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択にもどる',
        'BACK_BTN' => './user2-school-select',
        'KANRI_TITLE' => '【学校別】ユーザー管理',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'users',
    ],
     //◆システム管理メニュー　＞　学校別・ユーザー管理検索　＞　削除
     'user2-delete' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択にもどる',
        'BACK_BTN' => './user2-school-select',
        'KANRI_TITLE' => '【学校別】ユーザー管理',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'users',
    ],
     //◆システム管理メニュー　＞　学校別・ユーザー管理検索　＞　特別学校管理アカウント作成
     //戻るボタンでユーザー一覧に戻るのが理想だが、学校情報を保持して遷移できない(getパラメータもダメでした）ので、学校選択まで戻る。
     'user2-addschoolaccount' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択にもどる',
        'BACK_BTN' => './user2-school-select',
        'KANRI_TITLE' => '【学校別】特別学校アカウント発行（新規作成のみ）',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・ユーザー管理検索　＞　特別学校管理アカウント保存
    'user2-saveschoolaccount' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択にもどる',
        'BACK_BTN' => './user2-school-select',
        'KANRI_TITLE' => '【学校別】ユーザー管理',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・ユーザー管理検索　＞　ユーザー編集画面へ遷移「＊」ボタン押下
    'user2-edituser' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択にもどる',
        'BACK_BTN' => './user2-school-select',
        'KANRI_TITLE' => '【学校別】ユーザー管理 ユーザー編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　学校別・ユーザー管理検索　＞　特別学校管理アカウント保存
    'user2-saveuser' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => '学校選択にもどる',
        'BACK_BTN' => './user2-school-select',
        'KANRI_TITLE' => '【学校別】ユーザー管理',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　コース一覧
    'course' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'コース管理',
        'SEARCH_BTN' => './course',
        'PAGINATION' => 'courses',
    ],
    //◆システム管理メニュー　＞　コース一覧　＞　検索
    'course-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'コース管理',
        'SEARCH_BTN' =>'./course',
        'PAGINATION' => 'courses',
    ],
    //◆システム管理メニュー　＞　コース一覧　＞　新規＆更新
    'course-save' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'コース管理',
        'SEARCH_BTN' =>'./course',
        'PAGINATION' => 'courses',
    ],
    //◆システム管理メニュー　＞　コース一覧　＞　【管理用】全単語一覧
    'course-wordlist' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'コース一覧にもどる',
        'BACK_BTN' => './course',
        'KANRI_TITLE' => '【管理用】ALL単語リスト',
        'SEARCH_BTN' => './course-wordlist',
        'PAGINATION' => 'courseWords',
    ],
    'add-course-new' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'コース管理',
        'SEARCH_BTN' =>'./course',
        'PAGINATION' => 'courses',
    ],
    //◆システム管理メニュー　＞　コース一覧　＞　単語一覧
    'course-addword' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'コース一覧にもどる',
        'BACK_BTN' => './course',
        'KANRI_TITLE' => '単語一覧',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    'add-word' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './course',
        'KANRI_TITLE' => '単語一覧',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    'course-addwordtest' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './course',
        'KANRI_TITLE' => 'テスト単語追加',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　コース一覧　＞　単語一覧　＞　テストモード編集
    'wordtest' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'コース一覧にもどる',
        'BACK_BTN' => './course',
        'KANRI_TITLE' => 'テスト単語追加',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    'addword-add' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './course',
        'KANRI_TITLE' => '単語追加',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    'addword-save' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './course',
        'KANRI_TITLE' => '単語追加',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    'addword-delete' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './course',
        'KANRI_TITLE' => '単語追加',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　ランキングスタジアムスコア履歴一覧
    'battle' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'ランキングスタジアムスコア一覧',
        'SEARCH_BTN' =>'./battle-search' ,
        'PAGINATION' => 'battlescoreHistory', 
    ],
    //◆システム管理メニュー　＞　ランキングスタジアムスコア履歴検索
    'battle-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'ランキングスタジアムスコア一覧',
        'SEARCH_BTN' => './battle-search' ,
        'PAGINATION' => 'battlescoreHistory', 
    ],

    //◆システム管理メニュー　＞　スコア履歴一覧
    'typing-history' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '成績履歴・エクセル出力・記録証印刷',
        'SEARCH_BTN' => './typing-history' ,
        'PAGINATION' => 'scoreHistory', 
    ],
    //◆システム管理メニュー　＞　スコア履歴一覧　＞　検索
    'typing-history-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '成績履歴・エクセル出力・記録証印刷',
        'SEARCH_BTN' => './typing-history' ,
        'PAGINATION' => 'scoreHistory', 
    ],
    //◆システム管理メニュー　＞　ベストスコア一覧
    'bestscore' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'ベストスコアの記録証印刷',
        'SEARCH_BTN' => './bestscore' ,
        'PAGINATION' => 'bestScore', 
    ],
    //◆システム管理メニュー　＞　バレッド専用検定管理　＞生徒一覧一覧
    'vldtest' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'バレッド専用検定管理',
        'SEARCH_BTN' => './vldtest-search',
        'PAGINATION' => null, 
    ],
    //◆システム管理メニュー　＞　バレッド専用検定管理　＞検索
    'vldtest-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'バレッド専用検定管理',
        'SEARCH_BTN' => './vldtest-search',
        'PAGINATION' => null, 
    ],
    //◆システム管理メニュー　＞　バレッド専用検定管理　＞　試験選択
    'vldtest-update' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'バレッド専用検定管理',
        'SEARCH_BTN' => './vldtest-update',
        'PAGINATION' => null, 
    ],
    //◆システム管理メニュー　＞　ベストスコア一覧　＞　検索
    'bestscore-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'ベストスコアの記録証印刷',
        'SEARCH_BTN' => './bestscore' ,
        'PAGINATION' => 'bestScore', 
    ],

    //◆システム管理メニュー　＞　メンテナンス情報一覧
    'maintenance-info' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'メンテナンス情報管理',
        'SEARCH_BTN' => './maintenance-info',
        'PAGINATION' => 'information',
    ],
    //◆システム管理メニュー　＞　メンテナンス情報　＞　検索
    'maintenance-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => 'メンテナンス情報管理',
        'SEARCH_BTN' => './maintenance-info',
        'PAGINATION' => 'information',
    ],
    //◆システム管理メニュー　＞　メンテナンス情報　＞　編集画面へ遷移
    'maintenance-add' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'メンテナンス情報にもどる',
        'BACK_BTN' => './maintenance-info',
        'KANRI_TITLE' => 'メンテナンス情報管理 編集・保存',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　メンテナンス情報　＞　新規＆更新
    'maintenance-new' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'メンテナンス情報にもどる',
        'BACK_BTN' => './maintenance-info',
        'KANRI_TITLE' => 'メンテナンス情報管理 編集・保存',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　ログイン履歴
    'login-history' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '管理画面ログイン履歴',
        'SEARCH_BTN' => './login-history',
        'PAGINATION' => 'login_logs',
    ],
    //◆システム管理メニュー　＞　ログイン履歴　＞　検索
    'login-history-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '管理画面ログイン履歴',
        'SEARCH_BTN'=>'./login-history',
        'PAGINATION' => 'login_logs',
    ],

    //◆システム管理メニュー　＞　ブログ一覧　
    'bloglist' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' =>'./system-menu#menu-top',
        'KANRI_TITLE' => 'ブログ一覧',
        'SEARCH_BTN' => null,
        'PAGINATION' => 'blogs',
    ],
    //◆システム管理メニュー　＞　ブログ一覧　＞　検索
    'blog-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' =>'./bloglist',
        'KANRI_TITLE' => 'ブログ一覧',
        'SEARCH_BTN' =>'./bloglist',
        'PAGINATION' =>'blogs',
    ],
    //◆システム管理メニュー　＞　ブログ一覧　　＞　編集画面へ遷移
    'blog-edit' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'ブログ一覧にもどる',
        'BACK_BTN' =>'./bloglist',
        'KANRI_TITLE' => 'ブログ編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　ブログ一覧　　＞　編集・保存・削除
    'blog-save' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'ブログ一覧にもどる',
        'BACK_BTN' =>'./bloglist',
        'KANRI_TITLE' => 'ブログ編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　メンテナンス情報　＞　新規＆更新
    'setting' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' =>'./system-menu#menu-top',
        'KANRI_TITLE' => '【重要】基本設定',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　ノート一覧
    'note' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' =>'./system-menu#menu-top',
        'KANRI_TITLE' => 'システム開発ノート',
        'SEARCH_BTN' => './note',
        'PAGINATION' => 'notes',
    ],
    //◆システム管理メニュー　＞　ノート一覧 ＞　検索
    'note-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' =>'./system-menu#menu-top',
        'KANRI_TITLE' => 'システム開発ノート',
        'SEARCH_BTN' => './note',
        'PAGINATION' => 'notes',
    ],
    //◆システム管理メニュー　＞　ノート一覧 ＞　編集画面
    'note-edit' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'ノート一覧にもどる',
        'BACK_BTN' =>'./note',
        'KANRI_TITLE' => 'システム開発ノートの編集',
        'SEARCH_BTN' => null,
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　使っていない音声ファイル一覧
    'file' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' =>'./system-menu#menu-top',
        'KANRI_TITLE' => '使っていないファイルの整理',
        'SEARCH_BTN' => './file',
        'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　使っていない音声ファイル一覧　＞　ラジオボタン選択（音声ファイルかブログ画像ファイルか）
    'file-select' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' =>'./system-menu#menu-top',
        'KANRI_TITLE' => '使っていないファイルの整理',
        'SEARCH_BTN' => './file',
        'PAGINATION' => null,
    ],
   //◆システム管理メニュー　＞　検証一覧
   'check' => [
    'ROLE_STYLE' => 'bgSystem' ,
    'BACK_BTN_NAME' => 'もどる',
    'BACK_BTN' =>'./system-menu#menu-top',
    'KANRI_TITLE' => 'サーバーエラーの確認一覧',
    'SEARCH_BTN' => null,
    'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　画像アップロード
   'fileupload' => [
    'ROLE_STYLE' => 'bgSystem' ,
    'BACK_BTN_NAME' => 'もどる',
    'BACK_BTN' =>'./system-menu#menu-top',
    'KANRI_TITLE' => '画像アップロード',
    'SEARCH_BTN' => './fileupload',
    'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　請求書
   'invoice' => [
    'ROLE_STYLE' => 'bgSystem' ,
    'BACK_BTN_NAME' => 'もどる',
    'BACK_BTN' =>'./system-menu#menu-top',
    'KANRI_TITLE' => '請求書',
    'SEARCH_BTN' => './invoice',
    'PAGINATION' => 'invoices',
    ],
    //◆システム管理メニュー　＞　請求書 　＞　追加
   'invoice-add/add' => [
    'ROLE_STYLE' => 'bgSystem' ,
    'BACK_BTN_NAME' => 'もどる',
    'BACK_BTN' =>'./invoice',
    'KANRI_TITLE' => '請求書の新規作成',
    'SEARCH_BTN' => null,
    'PAGINATION' => null,
    ],
    //◆システム管理メニュー　＞　請求書 　＞　編集
   'invoice-edit/' => [
    'ROLE_STYLE' => 'bgSystem' ,
    'BACK_BTN_NAME' => 'もどる',
    'BACK_BTN' =>'./invoice',
    'KANRI_TITLE' => '請求書の編集・削除',
    'SEARCH_BTN' => null,
    'PAGINATION' => null,
    ],
    //【コンテスト】システム　＞　生徒一覧
    'contest-studentlist' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '【コンテスト】生徒一覧',
        'SEARCH_BTN' => './contest-studentlist',
        'PAGINATION' => 'users',
    ],
    //【コンテスト】システム　＞　生徒一覧 ＞　検索
    'contest-studentlist-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '【コンテスト】生徒一覧',
        'SEARCH_BTN' => './contest-studentlist',
        'PAGINATION' => 'users',
    ],
    //【コンテスト】システム　＞　スコア一覧
    'contest-scorelist' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '【コンテスト】スコア一覧',
        'SEARCH_BTN' => './contest-scorelist',
        'PAGINATION' => 'scoreHistory',
    ],
    //【コンテスト】システム　＞　スコア一覧 ＞　検索
    'contest-scorelist-search' => [
        'ROLE_STYLE' => 'bgSystem' ,
        'BACK_BTN_NAME' => 'もどる',
        'BACK_BTN' => './system-menu#menu-top',
        'KANRI_TITLE' => '【コンテスト】スコア一覧',
        'SEARCH_BTN' => './contest-scorelist',
        'PAGINATION' => 'scoreHistory',
    ],
    


    
    
    ],

];
